Threat	1	Product Contamination	C(C)	Chemical	Man-Made Hazard
Threat	2	Product Contamination	C(R)	Radionuclide	Man-Made Hazard
Threat	3	Product Contamination	C(B)	Bio-toxin	Man-Made Hazard
Threat	4	Product Contamination	C(P)	Pathogen	Man-Made Hazard
Threat	5	Product Contamination	C(W)	Weaponization	Man-Made Hazard
Threat	6	Sabotage	S(PI)	Physical - Insider	Man-Made Hazard
Threat	7	Sabotage	S(PU)	Physical - Outsider	Man-Made Hazard
Threat	8	Sabotage	S(CI)	Cyber - Insider	Man-Made Hazard
Threat	9	Sabotage	S(CU)	Cyber - Outsider	Man-Made Hazard
Threat	10	Theft or Diversion	T(PI)	Physical - Insider	Man-Made Hazard
Threat	11	Theft or Diversion	T(PU)	Physical - Outsider	Man-Made Hazard
Threat	12	Theft or Diversion	T(CI)	Cyber - Insider	Man-Made Hazard
Threat	13	Theft or Diversion	T(CU)	Cyber - Outsider	Man-Made Hazard
Threat	14	Attack: Marine	(M1)	Small Boat	Man-Made Hazard
Threat	15	Attack: Marine	(M2)	Fast Boat	Man-Made Hazard
Threat	16	Attack: Marine	(M3)	Barge	Man-Made Hazard
Threat	17	Attack: Marine	(M4)	Ocean Ship	Man-Made Hazard
Threat	18	Attack: Aircraft	(A1)	Helicopter	Man-Made Hazard
Threat	19	Attack: Aircraft	(A2)	Small Plane	Man-Made Hazard
Threat	20	Attack: Aircraft	(A3)	Medium - Regional Jet	Man-Made Hazard
Threat	21	Attack: Aircraft	(A4)	Long - Flight Jet	Man-Made Hazard
Threat	22	Attack: Automotive	(V1)	Car	Man-Made Hazard
Threat	23	Attack: Automotive	(V2)	Van	Man-Made Hazard
Threat	24	Attack: Automotive	(V3)	Mid-size Truck	Man-Made Hazard
Threat	25	Attack: Automotive	(V4)	Large Truck (18 Wheeler)	Man-Made Hazard
Threat	26	Attack: Assault Team	(AT1)	1 Assailant	Man-Made Hazard
Threat	27	Attack: Assault Team	(AT2)	2-4 Assailants	Man-Made Hazard
Threat	28	Attack: Assault Team	(AT3)	5-8 Assailants	Man-Made Hazard
Threat	29	Attack: Assault Team	(AT4)	9-16 Assailants	Man-Made Hazard
Threat	30	Natural	N(H)	Hurricane	Natural Hazard
Threat	31	Natural	N(E)	Earthquake	Natural Hazard
Threat	32	Natural	N(T)	Tornado	Natural Hazard
Threat	33	Natural	N(F)	Flood	Natural Hazard
Threat	34	Natural	N(I)	Ice Storms	Natural Hazard
Threat	35	Natural	N(W)	Wildfire	Natural Hazard
Threat	36	Dependency and Proximity	D(U)	Utilities	Dependency and Proximity
Threat	37	Dependency and Proximity	D(S)	Key Suppliers	Dependency and Proximity
Threat	38	Dependency and Proximity	D(E)	Key Employees	Dependency and Proximity
Threat	39	Dependency and Proximity	D(C)	Key Customers	Dependency and Proximity
Threat	40	Dependency and Proximity	D(T)	Transportation	Dependency and Proximity
Threat	41	Dependency and Proximity	D(P)	Proximity	Dependency and Proximity
Org	Admin Admin Admin	This is Admin Service	1
UserPreferences	true	1