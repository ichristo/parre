/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.util;

import static org.junit.Assert.*;

import java.math.BigDecimal;

import org.apache.log4j.Logger;
import org.junit.Test;


/**
 * The Class ViewUtilTest.
 */
public class ViewUtilTest {
	private static final transient Logger log = Logger.getLogger(ViewUtilTest.class);
	
	@Test
	public void testDisplayValue() {
		BigDecimal largeValue = new BigDecimal("99999999999999.99");
		String expectedFormattedValue = "99,999,999,999,999";
		
		String formattedValue = toDisplayValueWithCommas(largeValue);
		
		log.info("Formatted value: " + formattedValue);
		assertEquals("Incorrect format", expectedFormattedValue, formattedValue);
	}
	
	private static String toDisplayValueWithCommas(BigDecimal quantity) {
        boolean isNegative = false;
        if(quantity.longValue() < 0) {
            isNegative = true;
        }
        return addCommas(Long.toString(quantity.abs().longValue()), Long.toString(quantity.abs().longValue()).length(), isNegative);
    }
	
	private static String addCommas(String str, int length, boolean isNegative) {
        if(length < 4) {
            if(isNegative) {
                str = "-" + str;
            }
            return str;
        }
        return addCommas(str.subSequence(0, (length - 3)) + "," + str.subSequence(length - 3, str.length()), length-3, isNegative);
    }

}
