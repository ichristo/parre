package org.parre.client.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.apache.log4j.Logger;
import org.junit.Test;

public class ComparableTest {
	private final static transient Logger log = Logger.getLogger(ComparableTest.class);
	
	@Test
	public void testBooleanCompare() {
		List<Boolean> boolVals = Arrays.asList(Boolean.TRUE, Boolean.FALSE, Boolean.TRUE, Boolean.FALSE, Boolean.FALSE, Boolean.TRUE);
		
		List<Widget> widgets = new ArrayList<Widget>();
		
		int count = 0;
		for (Boolean boolVal : boolVals) {
			widgets.add(new Widget(boolVal, count++));
			
			if (count % 3 == 0) {
				widgets.add(new Widget(null, count++));
			}
		}
		
		log.info(">>>>>> Before");
		printWidgetList(widgets);
		Collections.sort(widgets);
		log.info(">>>>>> After");
		printWidgetList(widgets);
	}
	
	private void printWidgetList(List<Widget> widgets) {
		for (Widget widget : widgets) {
			log.info("Widget: " + widget.toString());
		}
	}
	
	protected class Widget implements Comparable<Widget> {
		private Boolean boolValue;
		private Integer numValue;
		
		public Widget() { }
		public Widget(Boolean boolValue, Integer numValue) { 
			this.boolValue = boolValue;
			this.numValue = numValue;
		}
		
		public Boolean getBoolValue() { return boolValue; }
		public void setBoolValue(Boolean boolValue) { this.boolValue = boolValue; }
		
		public Integer getNumValue() { return numValue; }
		public void setNumValue(Integer numValue) { this.numValue = numValue; }
		
		@Override
		public int compareTo(Widget other) {
			return -1 * String.valueOf(boolValue).compareTo(String.valueOf(other.boolValue));
		}
		
		@Override
		public String toString() {
			return "Widget [boolValue=" + boolValue + ", numValue=" + numValue
					+ "]";
		}
		
	}
}
