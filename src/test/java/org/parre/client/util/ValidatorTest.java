/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.util;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.List;

import org.apache.log4j.Logger;
import org.junit.Test;
import org.parre.client.util.CoordinateValidator;
import org.parre.client.util.NumericRangeValidator;
import org.parre.client.util.PostalCodeValidator;

import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.GwtEvent;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.ui.HasValue;

/**
 * The Class ValidatorTest.
 */
public class ValidatorTest {
	private static transient final Logger log = Logger.getLogger(ValidatorTest.class);
	
	@Test
	public void testCoordinateValidator() {
		List<String> goodCoordinates = Arrays.asList("40.689060", "-74.044636");
		List<String> badCoordinates = Arrays.asList("Fred", "-1.2.2.", "1j");
		
		CoordinateValidator validator = new CoordinateValidator();
		
		for (String goodCoordinate : goodCoordinates) {
			boolean isValid = validator.validate(new ValidatorTestValue(goodCoordinate));
			log.debug(goodCoordinate + " is valid: " + isValid);
			assertTrue(isValid);
		}
		
		for (String badCoordinate : badCoordinates) {
			boolean isValid = validator.validate(new ValidatorTestValue(badCoordinate));
			log.debug(badCoordinate + " is valid: " + isValid);
			assertFalse(isValid);
		}
	}
	
	@Test
	public void testPostalCodeValidator() {
		List<String> goodPostalCodes = Arrays.asList("22030", "90210", "00001");
		List<String> badPostalCodes = Arrays.asList("Fred", "-1.2.2.", "1j", "1234", "123456");
		
		PostalCodeValidator validator = new PostalCodeValidator();
		
		for (String goodPostalCode : goodPostalCodes) {
			boolean isValid = validator.validate(new ValidatorTestValue(goodPostalCode));
			log.debug(goodPostalCode + " is valid: " + isValid);
			assertTrue(isValid);
		}
		
		for (String badPostalCode : badPostalCodes) {
			boolean isValid = validator.validate(new ValidatorTestValue(badPostalCode));
			log.debug(badPostalCode + " is valid: " + isValid);
			assertFalse(isValid);
		}
	}
	
	@Test
	public void testMatchEmptyString() {
		List<String> originals = Arrays.asList("blah '' blah '' blah", "blah", "''", "xy''''z");
		String regex = "''";
		String replacement = "NULL";
		
		
		for (String original : originals) {
			log.debug("Original: " + original);
			log.debug("Changed: " + original.replaceAll(regex, replacement));
		}
	}

    @Test
    public void testNumericRangeValidator() {
        ValidatorTestValue validatorValue = new ValidatorTestValue("-1");

        List<String> values = Arrays.asList("12[34", "$1\\234.34", "1|23,456", "$1,?23,468.99", "asdf");
        List<Boolean> results = Arrays.asList(false, false, true, true, false);
        NumericRangeValidator numericRangeValidator = new NumericRangeValidator(1500d, 1500000d);

        int index = 0;
        for (String value : values ) {
            validatorValue.setValue(value);
            boolean actualResult = numericRangeValidator.validate(validatorValue);
            assertEquals("Validation result did not match expected value", results.get(index++), actualResult);
        }
    }
	
	/**
	 * The Class ValidatorTestValue.
	 */
	private class ValidatorTestValue implements HasValue<String> {
		private String value = "";
		
		public ValidatorTestValue(String value) {
			this.value = value;
		}
		
		@Override
		public HandlerRegistration addValueChangeHandler(ValueChangeHandler<String> handler) {
			return null;
		}

		@Override
		public void fireEvent(GwtEvent<?> event) {
		}

		@Override
		public String getValue() {
			return value;
		}

		@Override
		public void setValue(String value) {
			this.value = value;
		}

		@Override
		public void setValue(String value, boolean fireEvents) {
			setValue(value);
		}
		
	}
	
}
