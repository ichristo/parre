/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.shared;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

import org.apache.log4j.Logger;
import org.junit.Test;
import org.parre.shared.types.CounterMeasureType;


/**
 * The Class TypesTest.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 1/31/12
 */
public class TypesTest {
    private static transient final Logger log = Logger.getLogger(TypesTest.class);

    @Test
    public void printCounterMeasureTypes() {
        CounterMeasureType[] values = CounterMeasureType.values();
        log.info("Number of Counter Measure Types : " + values.length);
        for (CounterMeasureType value : values) {
            log.info(value.name() + ", " + value.getValue());
        }
    }
    
    @Test
    public void testBooleanEvaluations() {
    	Boolean truth = Boolean.TRUE;
    	Boolean falsity = Boolean.FALSE;
    	
    	if (!truth)  { log.debug("No truth"); }
    	if (!falsity) { log.debug("No falsity"); }
    }
    
    @Test
    public void testBigDecimalConstructor() {
    	List<String> values = Arrays.asList("210000", "210100.000532", "210,000.99");
    	
    	for (String value : values) {
    		BigDecimal converted = new BigDecimal(removeCommas(value));
    		log.info(value + " converts to " + converted);
    	}
    }
    
    private String removeCommas(String str) {
    	return str == null ? null : str.replaceAll(",", "");
    }
}
