/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.server;

import java.util.ArrayList;
import java.util.List;

import org.parre.server.domain.ReportSectionLookup;

public class ReportLookupDataload {

	public static List<ReportSectionLookup> getReportSections() {
		List<ReportSectionLookup> sections = new ArrayList<ReportSectionLookup>();

		sections.add(
				new ReportSectionLookup(
						"Executive Summary",
						"NA",
						"",
						"Please enter Executive Summary information",
						1,
						null,
						false,
						false,
						false
						)	
				);

		sections.add(
				new ReportSectionLookup(
						"Assessment Team",
						"NA",
						"",
						"Please enter Assessment Team information",
						2,
						null,
						false,
						false,
						false
						)	
				);

		sections.add(
				new ReportSectionLookup(
						"Assessment Team Members",
						"Successful assessments are the result of collaboration between the key information centers of the utility.  These team members may be part of the senior leadership, but in many cases, individuals with specific expertise and in-depth information of the operations of the utility are included.  The broad and diverse viewpoints of the team provide the richness and insight needed to achieve understanding of risk and the creativeness needed to reduce that risk while maintaining restricted operating budgets.  Not all members of the team needed to be involved in every phase of the assessment, but their reviews and reactions to the team’s collective decisions is key to producing an effective, actionable plan.",
						"",
						"Please enter Risk & Resilience Team Members information",
						3,
						null,
						false,
						false,
						false
						)	
				);

		sections.add(
				new ReportSectionLookup(
						"Assessment Approach",
						"NA",
						"",
						"Please enter Assessment Approach information",
						4,
						null,
						false,
						false,
						false
						)	
				);

		sections.add(
				new ReportSectionLookup(
						"Assessment Tasks",
						"NA",
						"",
						"Please enter Assessment Task details by clicking on the Data Table tab",
						5,
						null,
						false,
						true,
						false
						)	
				);

		sections.add(
				new ReportSectionLookup(
						"Work Sessions",
						"NA",
						"",
						"Please enter Work Sessions information",
						6,
						null,
						false,
						false,
						false
						)	
				);

		sections.add(
				new ReportSectionLookup(
						"Description of Utility",
						"NA",
						"",
						"Please enter Description of Utility information",
						7,
						null,
						false,
						false,
						false
						)	
				);

		sections.add(
				new ReportSectionLookup(
						"Mission Statement",
						"The focus of any utility system assessment starts by achieving team understanding the mission of that utility.  This concise statement of the goals and aspirations provides the direction for decision-making, especially the classification of critical assets and probable threats.    The assessment team identified missions as follows (in no particular order):",
						"",
						"Please enter Mission Statement information",
						8,
						null,
						false,
						false,
						false
						)	
				);

		sections.add(
				new ReportSectionLookup(
						"Critical Assets",
						"A risk assessment is conducted to better understand the risks to a utility’s assets.  Assets can be physical, human, cyber or knowledge (information), all of which are exposed to a degree of risk to their integrity and sustainability.  Through a process of evaluating those risks whose loss or impairment would prevent the utility from meeting its mission(s) in a meaningful way, the team identified a series of key assets that were critical to the operation.  ",
						"",
						"Please enter Critical Assets information",
						9,
						null,
						true,
						false,
						false
						)	
				);

		sections.add(
				new ReportSectionLookup(
						"Asset Threat Pairs",
						"Even for a moderate sized utility, the number of threat-asset pairs that are identified for analysis can rapidly become excessive.  It is not unusual to have several thousand pairs.  In order to focus attention to the most critical pairs, the J-100 recommends prioritizing them.  The PARRE tool helps the team by allowing the pairs to be evaluated on a scale of 1-5 as to the impact on the outcomes should the specified threat be successful in removing the critical asset from service.  ",
						"",
						"Please enter Asset Threat Pairs information",
						10,
						null,
						false,
						false,
						false
						)	
				);

		sections.add(
				new ReportSectionLookup(
						"Calculation of Consequences",
						"A successful attack by a threat on an asset produces consequences.  The J-100 approach is to evaluate these consequences in terms of the loss of life, the number of serious injuries, the financial cost to the utility, and finally, the economic impact on the community.  The financial impacts frequently have two components, the direct cost to repair / replace the damaged asset, and the potential loss of commodity sales as a result of the damaged asset.",
						"",
						"Please enter Calculation Consequences information",
						11,
						null,
						true,
						false,
						false
						)	
				);

		sections.add(
				new ReportSectionLookup(
						"Expert Elicitation",
						"NA",
						"",
						"Please enter Expert Elicitation information",
						12,
						"Vulnerability",
						true,
						false,
						false
						)	
				);

		sections.add(
				new ReportSectionLookup(
						"Path Analysis Discussion",
						"Path analysis compares the effectiveness of the countermeasures by evaluating the time required of an attacker (threat) to arrive at the asset and inflict damage along the most probable route, against the time required for that attacker to be detected and intercepted to prevent the attack from being successful.  Single, fixed, high-value targets already protected by a variety of countermeasures tend to be the most amenable for this type of analysis. ",
						"",
						"Please enter Path Analysis information",
						13,
						"Vulnerability",
						true,
						false,
						false
						)	
				);

		sections.add(
				new ReportSectionLookup(
						"Event Tree Discussion",
						"An event tree is a probability based decision matrix that breaks an attack into its component parts and then evaluates the probability that each of the parts will achieve success against the asset.  A well-defined event tree can be used to evaluate similarly situated assets",
						"",
						"Please enter Event Tree Discussion information",
						14,
						"Vulnerability",
						true,
						false,
						false
						)	
				);

		sections.add(
				new ReportSectionLookup(
						"Best Estimate",
						"For many directed threats, the best information concerning the likelihood of a specific threat is obtained from utility personnel or their governmental colleagues.  Called the “Best Estimate” in J 100, this method of evaluating threat likelihood is very similar to the expert elicitation approach in vulnerability determination",
						"",
						"Please enter Best Estimate information",
						15,
						"Threat Likelihood",
						true,
						false,
						false
						)	
				);

		sections.add(
				new ReportSectionLookup(
						"Proxy Analysis",
						"The proxy indicator was developed and included in the J-100 tools to fill a very large gap in the ability of the local and national intelligence/ policing agencies to accurately predict or share their information with local utility owners/operators.  The proxy indicator is based on the 2007 work of the RAND Corporation and the 2008 work of the Risk Management Solutions Corporation.  These publically available summaries detail directed threats recorded up to the time of their publication around the world.  The proxy is an attempt to characterize the relatively infrequent attacks, parsed by country and type of infrastructure",
						"",
						"Please enter Proxy Analysis information",
						16,
						"Threat Likelihood",
						true,
						false,
						false
						)	
				);

		sections.add(
				new ReportSectionLookup(
						"Conditional Likelihood",
						"NA",
						"",
						"Please enter Conditional Likelihood information",
						17,
						"Threat Likelihood",
						true,
						false,
						false
						)	
				);

		sections.add(
				new ReportSectionLookup(
						"Risk Management",
						"The concluding step of a J-100 risk assessment is the development of a risk reduction plan for those risks that exceed the tolerance level of the utility",
						"",
						"Please enter Risk Mangement information",
						18,
						null,
						true,
						false,
						false
						)	
				);

		sections.add(
				new ReportSectionLookup(
						"Risk Reduction Options",
						"NA",
						"",
						"Please enter Risk Reduction Options information and enter Investment Options by clicking on the Data Table tab",
						19,
						null,
						true,
						true,
						false
						)	
				);

		sections.add(
				new ReportSectionLookup(
						"Introduction",
						"Since the middle of the last decade, the US water sector has slowly become aware of the need to better understand the reliability of their systems.  Increased world tensions made owners/operators concerned that publically accessible water systems were inviting targets that garner significant public attention and concern when they are maliciously disturbed.  Tools began appearing in the late 1990s to measure the vulnerability of water systems to directed attacks against them.  Since that time, research and substantial practice has led to the development of new tools to support whole-system evaluations.  At the same time, the leadership of public water systems has evolved to better understand how to interpret an assessment and how to make wise decisions concerning its protection.  In the past 20-years, public utility systems have expanded their thinking from looking only at the vulnerability of their system to directed attacks to a more robust understanding of the “Risks” their systems face from directed as well as natural hazards.  At the end of the day however, the real goal is to increase the resilience of the system to be able to endure the attack and be able to promptly restore service to the customer when the threat has passed.",
						"",
						"",
						20,
						null,
						false,
						false,
						true
						)	
				);


		sections.add(
				new ReportSectionLookup(
						"RiskAssessment",
						"The ANSI/AWWA J-100-10(1) standard is the national standard for conducting risk and resilience assessments in the water sector. J-100 prescribes the RAMCAP approach of risk-based vulnerability assessments against a set of standardized directed and natural threats. The resulting assessment is an actionable program that describes the threat-concerns in terms of risk to the utility rather than simply looking at how vulnerable the utility is to that threat.",
						"",
						"",
						21,
						null,
						false,
						false,
						true
						)	
				);

		sections.add(
				new ReportSectionLookup(
						"ComplaintAssessment",
						"Unlike earlier water sector assessments, the J-100 uses a strict process of probability-driven determinations that consider the likelihood that a threat will occur, the most probable consequences of that threat, as well as the vulnerability of the critical assets to that threat. The resulting risk assessment is then used as a baseline against which countermeasures can be tested in an attempt to lower the risk and improve the resilience of the asset and/or system. The ability to measure the cost of the countermeasure(s) against the reduction in risk that results produces a return on investment (ROI) analysis of the system. The frequent outcome of such analysis is the awareness of low cost, but highly effective measures that can be taken (the proverbial “low hanging fruit”) to reduce risk. More costly but important investments can be programmed as capital investment opportunities avail themselves.",
						"",
						"",
						22,
						null,
						false,
						false,
						true
						)	
				);

		sections.add(
				new ReportSectionLookup(
						"SafetyAct",
						"The SAFETY Act is a US Department of Homeland Security program designed to encourage the development of anti- terrorism tools and processes without extraordinary liability exposure. The act is described by DHS as: The SAFETY Act provides important legal liability protections for providers of Qualified Anti-Terrorism Technologies - whether they are products or services. The goal of the SAFETY Act is to encourage the development and deployment of new and innovative anti-terrorism products and services by providing liability protections. (2) The SAFETY Act limits the liability exposure of utilities that use designated product in the event that they are the victims of a directed attack. This coverage limit typically coincides with the insurance coverage of most utilities, picking up liability protection above existing insurance limits.",
						"The J-100 standard has already been granted SAFETY Act designation, implying that utilities that complete a risk and resilience assessment in accordance with the standard and put into place a program consistent with the findings of that assessment, can benefit from the flow-down protection provisions of the act.",
						"",
						23,
						null,
						false,
						false,
						true
						)	
				);

		sections.add(
				new ReportSectionLookup(
						"SupportTool",
						"The J-100 standard requires the methodical application of the principles of probability and statistics to calculate risk- based assessments of every critical asset against every probable directed and random (natural) threat. While no single threat-asset pair analysis is difficult or exceptionally time consuming, a complete system analysis can entail thousands or even hundreds or thousands of threat-asset pairs. The Program to Assist Risk and Resilience Examination (PARRE) is an open-source, database tool written in Java-Code intended for use by a utility’s risk assessment team. PARRE stores the data as well as completes the numerous calculations in the background, freeing the team to understand the results and formulate options for reducing risk to the critical assets. A PARRE assessment will assure that the team has completed all required steps required by the J-100 standard. Once completed, the assessment, together with the notes, decisions, and options of the team, is contained in an accessible database, available for running “what-if” scenario drills or determining current risk status.",
						"The PARRE tool has been submitted for DHS SAFETY Act designation; further reinforcing the coverage and protections afforded the utility that uses the tool to conduct their assessments.",
						"",
						24,
						null,
						false,
						false,
						true
						)	
				);

		sections.add(
				new ReportSectionLookup(
						"AssessmentProcess",
						"The J-100 standard describes a stepped process for conducting risk and resilience assessments based on the RAMCAP approach.  While these steps can be done in any order, the standard does require that each step must be considered in order to complete a compliant analysis.  The  assessment followed the standard using the PARRE tool to record information and to support the decision making of the team.  The steps and key findings of each step are described in this section.",
						"",
						"",
						25,
						null,
						false,
						false,
						true
						)	
				);

		sections.add(
				new ReportSectionLookup(
						"ProbableThreats",
						"Threats to the critical assets are typically either directed or random.  The J-100 specifies the suite of 35 reference and six natural threats that, at a minimum, are to be considered (others can be added if desired).  The directed threats take the form of a physical or cyber assault, an explosion brought to the target by a number of conveyances, or the theft or sabotage of critical assets.  The natural threats included the six specified in the J-100. After evaluating all of the potential threats, the team selected the 18 threats to use in the assessment as shown in Table 3",
						"",
						"",
						26,
						null,
						false,
						false,
						true
						)	
				);

		sections.add(
				new ReportSectionLookup(
						"Countermeasure",
						"Countermeasures are the collection of structural and non-structural systems put in place to protect the critical assets by lessening the severity of the consequences from a successful attack by the threat.  The system has a wide variety of countermeasures already in place as detailed in Table 4",
						"",
						"",
						27,
						null,
						false,
						false,
						true
						)	
				);

		//For 28: Get the Stat Value from DB and append in b/w Description & Additional Description.
		//As a Sentence: $XXX million for a life and $XXX for a serious injury.  

		sections.add(
				new ReportSectionLookup(
						"StatisticalValue",
						"The team chose to include the statistical values of life and serious injuries in this assessment.  The values used are the 2011 US Department of Transportation values of: ",
						"The consequences for a threat-asset pair will therefore be displayed as one total value, combining the dollar values of lives and serious injuries with the financial consequences to the utility.",
						"",
						28,
						null,
						false,
						false,
						true
						)	
				);

		sections.add(
				new ReportSectionLookup(
						"AssetVulnerabilities",
						"Vulnerability is a measure of the effectiveness of the attacker and of the countermeasures that are in place.  Vulnerability is a calculated value, expressed as a probability ranging from very low vulnerabilities (ineffective attackers and/or highly effective countermeasures), to the other extreme of very high vulnerabilities (very effective attackers and/or very ineffective countermeasures).  The J-100 permits the team to evaluate the vulnerabilities using a variety of approaches ranging from expert elicitation to the logic-based tools of Path Analysis, or Decision Tree Analysis.  The characteristics of each threat-asset pair tend to dictate the best approach to use.  The team used all three approaches in arriving at the vulnerabilities for the identified threat-asset pairs.",
						"",
						"",
						29,
						null,
						false,
						false,
						true
						)	
				);


		sections.add(
				new ReportSectionLookup(
						"ThreatLikelihood",
						"The element that makes a risk assessment different from a vulnerability assessment is the inclusion of threat likelihood.  Threat likelihood seeks to understand the probability that the specified threat will even attack the critical asset.  For natural threats, the likelihood is embodied in the concept of its “return period”, thus the likelihood of the threat such as flood, can be seen in the 100-year rain event which by definition has a threat likelihood in any given year of 1/100 = 1%.  Threat likelihood for directed threats is a bit more complicated.  In these cases, the J-100 permits the use of at least three approaches; Proxy Analysis, Expert Elicitation, and/or Conditional Likelihood.",
						"",
						"",
						30,
						null,
						false,
						false,
						true
						)	
				);

		sections.add(
				new ReportSectionLookup(
						"RiskDetermination",
						"Risk, as developed by the RAMCAP process and detailed in the J-100 standard, is defined as the product of the consequences suffered by the critical asset by a threat, times the vulnerability probability, times the threat likelihood probability. R = C x V x T : ",
						"Since the consequence is dollar denominated, while vulnerability and threat likelihood are probabilities, the value of risk using this approach is a dollar-valued amount.  The physical meaning of these risk dollars may or may not be of great significance when used to compare risks between assets and prepare prioritized listings of asset-threat pairs.  The dollar value of risk may be interpreted as the annual amount that a utility would have to put into savings (sinking fund) at zero interest to reconstruct the asset after it has been successfully attacked by the threat.",
						"",
						31,
						null,
						false,
						false,
						true
						)	
				);

		sections.add(
				new ReportSectionLookup(
						"Baseline",
						"The baseline is an in-depth assessment of the utility, as it currently exists.  After starting with 92 critical assets and 41 possible threats (39 J-100 reference threats and the addition of the natural threats of ice storms and wildfires), the assessment team evaluated 3,772 asset-threat pairs of which 890 pairs were determined to be critical to the continuous operations of the system.  Table 11 is a synopsis of the threat-asset pairs of greatest concern as measured individually by all three consequence measurements, their risk, and the level of resilience of the asset-threat pair.",
						"",
						"",
						32,
						null,
						false,
						false,
						true
						)	
				);

		sections.add(
				new ReportSectionLookup(
						"Resilience",
						"Resilience is the ability of an asset or the utility system to promptly recover from the consequences of a successful attack and restore service to some meaningful level.  The J-100 standard describes the approach to be applied for both the asset-threat resilience and also for the system resiliency determinations.",
						"",
						"",
						33,
						null,
						false,
						false,
						true
						)	
				);

		sections.add(
				new ReportSectionLookup(
						"PairResilience",
						"The resilience of a single critical asset to a defined threat carries with it much of the same considerations as the risk to that asset.  There may be situations where the utility is particularly interested in the resilience of a specifically critical asset and would want to investigate ways to further enhance the robustness of this asset so as to make it better able to recover from an attack.  The J-100 describes the development of an asset resilience metric that is service denial due to the loss of the asset, weighted by the vulnerability and threat likelihood.  If the asset is capable of continuing to support the mission throughout the attack and restore service with little or no loss in service, then the metric would return a zero as there are no service outage consequences.  The greater this metric, the less resilient the asset is to the defined threat.  The resilience of the 50 least-resilience asset-threat pairs are shown in Table 14",
						"",
						"",
						34,
						null,
						false,
						false,
						true
						)	
				);

		sections.add(
				new ReportSectionLookup(
						"SystemResilience",
						"Another useful metric for evaluating the robustness of the utility is the “System Resilience”.  This indicator attempts to evaluate the ability of the utility system to recover from an attack and reestablish service in a timely manner.  To understand the ability of the system to accomplish this, two areas are evaluated.  First, the system’s operational robustness (called its Operational Resilience Index -- ORI) to deal with the aftermath of an attack is measured through such efforts as the level of emergency response plan implementation, NIMS compliance, WARN program involvement, critical parts inventory, and emergency power.  It should be noted that specific critical assets are not part of this system analysis nor are the specific threats.  The second factor in determining a system’s resilience (called its financial resilience index – FRI) is its financial ability to recover.  This ability is informed by such indicators as its Business Continuity Plan, Bond rating, and level of GASB Assessment.  In addition, two indices are included in the financial resilience including the general level of unemployment in the area, and the median household income.  The purpose of these last two indicators is to weight the outcome by the ability of the served community to support the extraordinary costs that inevitably result from a major attack on the utility system",
						"",
						"",
						35,
						null,
						false,
						false,
						true
						)	
				);


		sections.add(
				new ReportSectionLookup(
						"SystemResilienceSummary",
						"Evaluated 12 questions regarding their system and the community that they serve to determine the level of their overall system resilience as defined by the J-100 standard .  In this measure, the system resilience is a combination of its Operational Resilience index (ORI) and its Financial Resilience Index (FEI).  The system has a resilience score of 56.25 on a 100 scale, assessed as follows:",
						"",
						"",
						36,
						null,
						false,
						false,
						true
						)	
				);

		sections.add(
				new ReportSectionLookup(
						"Appendix",
						"Summary tables of the Top-20 Risks as well as the Top 10 natural threat risks are provided in the following tables.  For complete information on all asset-threat pairs and the results of their assessments, the reader is referred to the PARRE tool database. ",
						"",
						"",
						37,
						null,
						false,
						false,
						true
						)	
				);

		return sections;
	}

}
