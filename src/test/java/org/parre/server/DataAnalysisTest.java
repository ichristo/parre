/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.server;

import java.math.BigDecimal;

import junit.framework.Assert;

import org.apache.log4j.Logger;
import org.junit.Test;
import org.parre.server.domain.ProxyTier;
import org.parre.server.domain.manager.LotManager;
import org.parre.server.messaging.invocation.ServiceTest;
import org.parre.server.messaging.service.CalculationUtil;
import org.parre.shared.ProxyCityDTO;
import org.parre.shared.ProxyIndicationDTO;
import org.parre.shared.ProxyTargetTypeDTO;


/**
 * User: Daniel
 * Date: 1/12/12
 * Time: 9:18 AM
*/
public class DataAnalysisTest extends ServiceTest {
    private static transient final Logger log= Logger.getLogger(DataAnalysisTest.class);
    private static final Double variance = 0.1d;  // TODO: get this to 0.0001?

    @Test
    public void testDataAnalysisDC() {
        ProxyCityDTO cityDTO = new ProxyCityDTO("", 1, 2);  //"Washington-Arlington-Alexandria, DC-VA-MD-WV MSA"
        ProxyTargetTypeDTO targetTypeDTO = new ProxyTargetTypeDTO(2, "", new BigDecimal("0.05555555600000000000")); //"Water reservoirs and distribution, passenger trains, airspace zones"
        BigDecimal expectedValue = BigDecimal.valueOf(0.002231);
        BigDecimal calculatedValue = CalculationUtil.calculateProbability(
                new ProxyIndicationDTO(4, cityDTO, targetTypeDTO, new Integer(6), new Integer(60), new Integer(200), new Integer(500), new Integer(0), BigDecimal.ONE));
        log.info("Calculated Value : " + calculatedValue);
        compareResults(calculatedValue.doubleValue(), expectedValue.doubleValue());
    }

    @Test
    public void testDataAnalysisSeattle() {
        ProxyCityDTO cityDTO = new ProxyCityDTO("", 4, 4);//"Seattle-Tacoma-Bellevue, WA MSA"
        ProxyTargetTypeDTO targetTypeDTO = new ProxyTargetTypeDTO(6, "", new BigDecimal("0.16666666700000000000")); //"Military, train and subway stations, stadiums, bridges and tunnels"
        double test2Value = 0.000890;
        BigDecimal test2 = CalculationUtil.calculateProbability(
                new ProxyIndicationDTO(12, cityDTO, targetTypeDTO, new Integer(13), new Integer(55), new Integer(90), new Integer(750), new Integer(0), BigDecimal.ONE));

        compareResults(test2.doubleValue(), test2Value);
    }

    @Test
    public void testDataAnalysisOhio() {
        ProxyCityDTO cityDTO = new ProxyCityDTO("", 4, 4);//"Cleveland-Elyria-Mentor, OH MSA"
        ProxyTargetTypeDTO targetTypeDTO = new ProxyTargetTypeDTO(8, "", new BigDecimal("0.22222222200000000000")); //"Government Buildings"
        double test3Value = 0.000303;
        BigDecimal test3 = CalculationUtil.calculateProbability(
                new ProxyIndicationDTO(2, cityDTO, targetTypeDTO, new Integer(5), new Integer(23), new Integer(1000), new Integer(5000), new Integer(0), BigDecimal.ONE));

        compareResults(test3.doubleValue(), test3Value);
    }

    @Test
    public void testDataAnalysisCalifornia() {
        ProxyCityDTO cityDTO = new ProxyCityDTO("", 4, 4);//"San Diego-Carlsbad-San Marcos, CA MSA"
        ProxyTargetTypeDTO targetTypeDTO = new ProxyTargetTypeDTO(1, "", new BigDecimal("0.02777777800000000000")); // "Power plants, dams, railway networks. "
        double test4Value = 0.000009;
        BigDecimal test4 = CalculationUtil.calculateProbability(
                new ProxyIndicationDTO(1, cityDTO, targetTypeDTO, new Integer(1), new Integer(50), new Integer(10), new Integer(10), new Integer(0), BigDecimal.ONE));

        compareResults(test4.doubleValue(), test4Value);
    }

    @Test
    public void testDataAnalysisNewYork() {
        ProxyCityDTO cityDTO = new ProxyCityDTO("", 1, 2);// "New York"
        ProxyTargetTypeDTO targetTypeDTO = new ProxyTargetTypeDTO(8, "", new BigDecimal(0.22222222200000000000)); //"Government Buildings"
        double test5Value = 0.697166;
        BigDecimal test5 = CalculationUtil.calculateProbability(
                new ProxyIndicationDTO(50, cityDTO, targetTypeDTO, new Integer(5), new Integer(10), new Integer(5000), new Integer(10000), new Integer(0), BigDecimal.ONE));

        compareResults(test5.doubleValue(), test5Value);
    }

    @Test
    public void testDataAnalysisOther() {
        Integer population = new Integer(256137);//"Other  (Please Input Approximate Population)"
        Integer tierNumber = CalculationUtil.calculateTierFromPopulation(population);
        log.info("Calculated Tier Number = " + tierNumber);
        ProxyTier proxyTier = LotManager.findProxyTier(tierNumber, entityManager());
        ProxyCityDTO cityDTO = new ProxyCityDTO("", proxyTier.getTierNumber(), proxyTier.getNumberOfCities());

        ProxyTargetTypeDTO targetTypeDTO = new ProxyTargetTypeDTO(8, "", new BigDecimal(0.22222222200000000000)); //"Government Buildings"
        double test6Value = 0.000267;
        BigDecimal test6 = CalculationUtil.calculateProbability(
                new ProxyIndicationDTO(50, cityDTO, targetTypeDTO, new Integer(5), new Integer(10), new Integer(5000), new Integer(10000), population, BigDecimal.ONE));

        compareResults(test6.doubleValue(), test6Value);
    }

    @Test
    public void testDataAnalysisOther2() {
        Integer population = new Integer(750000); //"Other  (Please Input Approximate Population)"
        Integer tierNumber = CalculationUtil.calculateTierFromPopulation(population);
        log.info("Calculated Tier Number = " + tierNumber);
        ProxyTier proxyTier = LotManager.findProxyTier(tierNumber, entityManager());
        ProxyCityDTO cityDTO = new ProxyCityDTO("", proxyTier.getTierNumber(), proxyTier.getNumberOfCities());
        ProxyTargetTypeDTO targetTypeDTO = new ProxyTargetTypeDTO(3, "", new BigDecimal("0.08333333300000000000")); //"Cruise ships, apartment buildings, foreign consulates, United Nations"
        double test7Value = 0.000021;
        BigDecimal test7 = CalculationUtil.calculateProbability(
                new ProxyIndicationDTO(5, cityDTO, targetTypeDTO, new Integer(2), new Integer(4), new Integer(1000), new Integer(3000), population, BigDecimal.ONE));

        compareResults(test7.doubleValue(), test7Value);
    }

    private void compareResults(double calculatedValue, double expectedValue) {
    	Assert.assertEquals(calculatedValue, expectedValue, expectedValue * variance);
    }


}