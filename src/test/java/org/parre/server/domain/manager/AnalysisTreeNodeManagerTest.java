/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.server.domain.manager;

import org.apache.log4j.Logger;
import org.junit.Test;
import org.parre.server.domain.AnalysisTreeNode;
import org.parre.server.domain.manager.AnalysisTreeNodeManager;
import org.parre.server.persistence.PersistenceTest;
import org.parre.shared.types.AnalysisTreeType;

import java.math.BigDecimal;
import java.util.List;
import java.util.Set;

/**
 * The Class AnalysisTreeNodeManagerTest.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 1/17/12
 */
public class AnalysisTreeNodeManagerTest extends PersistenceTest {
    private static transient final Logger log = Logger.getLogger(AnalysisTreeNodeManagerTest.class);

    @Test
    public void saveTree() {
        AnalysisTreeNode root = new AnalysisTreeNode();
        //root.setAnalysisTreeType(AnalysisTreeType.EVENT_TREE);
        root.setName("Another Tree Root");
        root.setDescription("This is the root node for the Test Tree");

        entityManager().persist(root);
        entityManager().flush();

        AnalysisTreeNode node;
        node = new AnalysisTreeNode();
        node.setName("Left Branch");
        node.setProbability(BigDecimal.valueOf(0.5));
        //node.setAnalysisTreeType(AnalysisTreeType.EVENT_TREE);
        root.addChild(node);

        node = new AnalysisTreeNode();
        node.setName("Right Branch");
        node.setProbability(BigDecimal.valueOf(0.5));
        //node.setAnalysisTreeType(AnalysisTreeType.EVENT_TREE);
        root.addChild(node);

        entityManager().persist(root);
        entityManager().flush();
    }

    @Test
    public void loadTrees() {
        List<AnalysisTreeNode> analysisTrees = AnalysisTreeNodeManager.getAnalysisTrees(entityManager());
        log.info("Number of trees : " + analysisTrees.size());
        for (AnalysisTreeNode analysisTree : analysisTrees) {
            log.info(analysisTree.getName());
            Set<AnalysisTreeNode> children = analysisTree.getChildren();
            log.info("Number of Children : " + children.size());
            for (AnalysisTreeNode child : children) {
                log.info("\t\t" + child.getName() + "(" + child.getProbability() + ")");
            }
        }
    }
}
