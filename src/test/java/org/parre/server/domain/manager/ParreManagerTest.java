/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.server.domain.manager;


import org.apache.log4j.Logger;
import org.junit.Test;
import org.parre.server.domain.*;
import org.parre.server.domain.manager.ParreManager;
import org.parre.server.persistence.PersistenceTest;
import org.parre.shared.AssetThreatLevelDTO;
import org.parre.shared.LabelValueDTO;
import org.parre.shared.types.LookupType;

import java.util.List;


/**
 * The Class ParreManagerTest.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 11/18/11
 */
public class ParreManagerTest extends PersistenceTest {
    private static transient final Logger log = Logger.getLogger(ParreManagerTest.class);
    @Test
    public void findUserByEmail() {
        User user = ParreManager.findUser("SDhaliwal@aemcorp.com", entityManager());
        log.info(user);
        log.info("OrgName= " + user.getOrg().getName());
    }

    @Test
    public void getOrgContacts() {
        List<Contact> orgContacts = ParreManager.getOrgContacts(1L, entityManager());
        log.info("Number Of Contacts = " + orgContacts.size());
    }

    @Test
    public void updateAssetThreatLevel() {
        AssetThreatLevel records = ParreManager.findAssetThreatLevel(1L, new AssetThreatLevelDTO(1L, 1L, 3), entityManager());
        log.info("Updated = " + records);
    }

    @Test
    public void getAllAssetThreatCount() {
        Long count = ParreManager.getAllAssetThreatCount(1L, entityManager());
        log.info("Count = " + count);
    }

    @Test
    public void getAllAssetThreats() {
        List<AssetThreat> assetThreats = ParreManager.getAllAssetThreats(1L, entityManager());
        log.info("Count = " + assetThreats.size());
    }

    @Test
    public void getProposedMeasures() {
        List<ProposedMeasure> proposedMeasures = ParreManager.getProposedMeasures(5L, entityManager());
        log.info("Number of Proposed Measures : " + proposedMeasures.size());
    }

    @Test
    public void getBaselineProposedMeasures() {
        List<ProposedMeasure> proposedMeasures = ParreManager.getBaselineProposedMeasures(1L, entityManager());
        log.info("Number of Baseline Proposed Measures : " + proposedMeasures.size());
    }

    @Test
    public void getAssetThreatAnalyses() {
        List<AssetThreatAnalysis> assetThreatAnalyses = ParreManager.getAssetThreatAnalyses(1L, entityManager());
        for (AssetThreatAnalysis assetThreatAnalyse : assetThreatAnalyses) {
            log.info(assetThreatAnalyse);
        }
    }

    @Test
    public void getCounterMeasures() {
        List<CounterMeasure> counterMeasures = ParreManager.getCounterMeasures(1L, entityManager());
        log.info("Number Of Counter Measures=" + counterMeasures.size());
    }

    @Test
    public void getStateCodeList() {
    	List<LabelValueDTO> states = LookupValue.createDTOs(ParreManager.getLookupValues(LookupType.STATE_CODES, entityManager()));
    	log.info("Found " + states.size() + " states.");
    	for (LabelValueDTO state : states) {
    		log.info("> " + state.toString());
    	}
    }
    
	@Test
	@SuppressWarnings("unchecked")
    public void getAssetStatus() {
    	List<Asset> assets = entityManager().createQuery("select pr from PhysicalResource pr").getResultList();
    	for (Asset asset : assets) {
    		ParreManager.isAssetInUse(asset, entityManager());
    	}
    }

}
