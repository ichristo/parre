/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.server.domain.manager;

import org.apache.log4j.Logger;
import org.junit.Test;
import org.parre.server.domain.ParreAnalysis;
import org.parre.server.domain.manager.ParreAnalysisManager;
import org.parre.server.persistence.PersistenceTest;

import java.util.List;

/**
 * The Class ParreAnalysisManagerTest.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 11/23/11
 */
public class ParreAnalysisManagerTest extends PersistenceTest {
    private static transient final Logger log = Logger.getLogger(ParreAnalysisManagerTest.class);

    @Test
    public void getActive() throws Exception {
        List<ParreAnalysis> currentParreAnalysis = ParreAnalysisManager.getNotArchivedAnalyses(1L, entityManager());
        log.info("Active Count=" + currentParreAnalysis.size());
        
        for (ParreAnalysis analysis : currentParreAnalysis) {
        	log.info("Stat values: " + (analysis.getStatValues() == null ? "null!" : analysis.getStatValues().toString()));
        }
    }
    @Test
    public void getCurrent() throws Exception {
        List<ParreAnalysis> currentParreAnalysis = ParreAnalysisManager.getActiveParreAnalyses(1L, entityManager());
        log.info("current=" + currentParreAnalysis.size());
    }
}
