/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.server.domain.manager;

import org.apache.log4j.Logger;
import org.junit.Test;
import org.parre.server.domain.ProxyIndication;
import org.parre.server.domain.manager.AssetThreatAnalysisManager;
import org.parre.server.messaging.invocation.ServiceTest;

import java.util.List;

/**
 * The Class AssetThreatAnalysisManagerTest.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 5/2/12
 */
public class AssetThreatAnalysisManagerTest extends ServiceTest {
    private static transient final Logger log = Logger.getLogger(AssetThreatAnalysisManagerTest.class);

    @Test
    public void testGetParreAnalysisProxies() throws Exception {
        List<ProxyIndication> parreAnalysisProxies = AssetThreatAnalysisManager.getParreAnalysisProxies(1L, entityManager());
        log.info("Number of Proxies : " + parreAnalysisProxies.size());
    }
}
