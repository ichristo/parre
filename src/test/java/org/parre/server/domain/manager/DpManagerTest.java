/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.server.domain.manager;

import java.util.List;

import org.apache.log4j.Logger;
import org.junit.Test;
import org.parre.server.domain.Threat;
import org.parre.server.domain.manager.DpManager;
import org.parre.server.domain.manager.ThreatManager;
import org.parre.server.persistence.PersistenceTest;
import org.parre.shared.DpDTO;
import org.parre.shared.ThreatDTO;
import org.parre.shared.types.ThreatCategoryType;


/**
 * The Class DpManagerTest.
 */
public class DpManagerTest extends PersistenceTest {
	private static transient final Logger log = Logger.getLogger(DpManagerTest.class);
	
	@Test
	public void testSearch() {
		ThreatDTO searchDTO = new ThreatDTO();
		List<DpDTO> results = DpManager.search(searchDTO, 1L, entityManager());
		
		log.debug("Found: " + results.size() + " threats!");
		for (DpDTO result : results) {
			log.debug(result.toString());
		}
		
	}
	
	
	@Test
	public void testGetManMadeThreats() {
		List<Threat> results = ThreatManager.getActiveThreats(1L, ThreatCategoryType.MAN_MADE_HAZARD, entityManager());
		
		log.debug("Found: " + results.size() + " threats!");
		for (Threat result : results) {
			log.debug(result.toString());
		}
	}
}
