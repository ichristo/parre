/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.server.domain.manager;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.junit.Test;
import org.parre.server.domain.Asset;
import org.parre.server.domain.AssetNote;
import org.parre.server.domain.PhysicalResource;
import org.parre.server.domain.manager.AssetManager;
import org.parre.server.persistence.PersistenceTest;
import org.parre.shared.AssetDTO;
import org.parre.shared.AssetSearchDTO;


/**
 * The Class AssetManagerTest.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 11/23/11
 */
public class AssetManagerTest extends PersistenceTest {
    private static transient final Logger log = Logger.getLogger(AssetManagerTest.class);

    @Test
    public void searchAssets() throws Exception {
        AssetSearchDTO searchDTO = new AssetSearchDTO("", "");
        searchDTO.setParreAnalysisId(1L);
        List<Asset> assets = AssetManager.searchAssets(searchDTO, entityManager());
        log.info("Number of Assets=" + assets.size());
    }
    @Test
    public void getAssets() throws Exception {
        List<Asset> assets = AssetManager.getAssets(1L, entityManager());
        log.info("Number of Assets=" + assets.size());
    }
    
    @Test
    public void getSystemTypes() throws Exception {
    	List<String> assetSystemTypes = AssetManager.getAssetSystemTypes(10L, entityManager());
    	log.info("Found: " + assetSystemTypes.size() + " asset system types...");
    	for (String systemType : assetSystemTypes) {
    		log.info(">>> " + systemType);
    	}
    }
    
    @Test
    public void testSaveAsset() throws Exception {
    	//entityManager().getTransaction().begin();
    	Asset asset = new PhysicalResource();
    	asset.setDescription("Delete me");
    	asset.setName("DeleteMe");
    	
    	Set<AssetNote> notes = new HashSet<AssetNote>();
    	for (int i = 0; i < 2; i++) {
    		AssetNote note = new AssetNote();
    		note.setDateCreated(new Date());
    		note.setMessage("DeleteMe note: " + i);
    		note.setUserName("Arthur Dent");
    		notes.add(note);
    	}
    	asset.setAssetNotes(notes);
    	entityManager().persist(asset);
    	entityManager().flush();
    	entityManager().getTransaction().commit();
    	entityManager().getTransaction().begin();
    	
    	Asset copy = entityManager().find(Asset.class, asset.getId());
    	
    	log.info("Copy has " + copy.getAssetNotes().size() + " notes.");
    	Long assetId = copy.getId();
    	log.info("Copy: " + copy.getId());
    	
    	entityManager().remove(copy);
    	entityManager().flush();
    	entityManager().getTransaction().commit();
    	
    	Asset afterDelete = entityManager().find(Asset.class, assetId);
    	log.info("After delete, found by id: " + String.valueOf(afterDelete));
    }
}
