/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.server.domain.manager;

import org.apache.log4j.Logger;
import org.junit.Test;
import org.parre.server.domain.ProxyTier;
import org.parre.server.domain.manager.LotManager;
import org.parre.server.persistence.PersistenceTest;

/**
 * The Class LotManagerTest.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 2/7/12
 */
public class LotManagerTest extends PersistenceTest {
    private static transient final Logger log = Logger.getLogger(LotManagerTest.class);

    @Test
    public void findProxyTier() {
        ProxyTier proxyTier = LotManager.findProxyTier(5, entityManager());
        log.info("ProxyTier = " + proxyTier);
    }
}
