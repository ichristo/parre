/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.server.domain.manager;

import static org.junit.Assert.*;

import org.apache.log4j.Logger;
import org.junit.Test;
import org.parre.server.domain.DetectionLikelihood;
import org.parre.server.domain.InactiveThreat;
import org.parre.server.domain.Threat;
import org.parre.server.domain.manager.ThreatManager;
import org.parre.server.persistence.PersistenceTest;
import org.parre.shared.ThreatDTO;
import org.parre.shared.types.ThreatCategoryType;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;
import java.util.Map;


/**
 * The Class ThreatManagerTest.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 11/18/11
 */
public class ThreatManagerTest extends PersistenceTest {
    private static transient final Logger log = Logger.getLogger(ThreatManagerTest.class);
    @Test
    public void searchThreats() {
        ThreatDTO dto = new ThreatDTO();
        dto.setThreatCategory(ThreatCategoryType.NONE);
        dto.setHazardType("Product*");
        dto.setName("c*");
        List<Threat> threats = ThreatManager.searchThreats(dto, entityManager());
        log.info("Number Of Threats = " + threats.size());
        for (Threat threat : threats) {
            log.info(threat);
        }
    }
    @Test
    public void getActiveThreats() {
        List<Threat> activeThreats = ThreatManager.getActiveThreats(0L, ThreatCategoryType.MAN_MADE_HAZARD, entityManager());
        for (Threat activeThreat : activeThreats) {
            log.info(activeThreat);
        }
    }

    @Test
    public void getInactiveThreats() throws Exception {
        List<InactiveThreat> inactiveThreats = ThreatManager.getInactiveThreats(0L, entityManager());
        for (InactiveThreat inactiveThreat : inactiveThreats) {
            log.info(inactiveThreat);
        }
    }
    @Test
    public void getInactiveThreatIds() throws Exception {
        List<Long> inactiveThreats = ThreatManager.getInactiveThreatIds(0L, entityManager());
        log.info("Ids=" + inactiveThreats);
    }

    @Test
    public void findInactiveThreat() throws Exception {
        InactiveThreat inactiveThreat = ThreatManager.findInactiveThreat(1L, 1L, entityManager());
        log.info(inactiveThreat);
    }

    @Test
    public void getInactiveReasons() throws Exception {
        Map<Long,String> inactiveReasons = ThreatManager.getInactiveReasons(1L, Collections.singletonList(14L), entityManager());
        log.info(inactiveReasons);
    }

    @Test
    public void findThreat() {
        Threat threat = ThreatManager.findThreat("C(C)", entityManager());
        log.info("Threat = " + threat);
    }
    
    @Test
    public void updateUserDefinedThreatLikelihood() {
		Threat threat = new Threat();
		threat.setName("(TEST)");
		threat.setDescription("Threat used for unit testing...");
		threat.setHazardType("Test Hazard Type");
		threat.setThreatCategory(ThreatCategoryType.MAN_MADE_HAZARD);
		
		entityManager().persist(threat);
		entityManager().flush();
    	ThreatManager.updateUserDefinedThreatLikelihood(threat, BigDecimal.valueOf(.5), entityManager());
    	
    	List<DetectionLikelihood> detectionLikelihoods = entityManager().createQuery("select dl from DetectionLikelihood dl").getResultList();
    	
    	boolean foundDetectionLikelihood = false;
    	for (DetectionLikelihood detectionLikelihood : detectionLikelihoods) {
    		log.info("DetectionLikelihood: " + detectionLikelihood);
    		if (detectionLikelihood.getThreat().getId().equals(threat.getId())) {
    			entityManager().remove(detectionLikelihood);
    			log.info("Found it!");
    			foundDetectionLikelihood = true;
    		}
    	}
    	
    	entityManager().remove(threat);
    	
    	assertTrue(foundDetectionLikelihood);
    }
}
