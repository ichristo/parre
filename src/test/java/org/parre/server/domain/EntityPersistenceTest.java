/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.server.domain;

import org.apache.log4j.Logger;
import org.junit.Test;
import org.parre.server.domain.Address;
import org.parre.server.domain.Asset;
import org.parre.server.domain.BaseEntity;
import org.parre.server.domain.ExternalResource;
import org.parre.server.domain.HumanResource;
import org.parre.server.domain.PhysicalResource;
import org.parre.server.persistence.PersistenceTest;
import org.parre.shared.AmountDTO;
import org.parre.shared.types.MoneyUnitType;

import javax.persistence.Query;
import java.math.BigDecimal;
import java.util.List;

/**
 * The Class EntityPersistenceTest.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 11/10/11
 */
public class EntityPersistenceTest extends PersistenceTest {
    private static transient final Logger log = Logger.getLogger(EntityPersistenceTest.class);

    @Test
    public void createAsset() {
        HumanResource hr = new HumanResource();
        hr.setAssetId("HR-1");
        hr.setName("Human Resource 1");
        hr.setDescription("Description for the Human Resource");
        hr.setCritical(true);
        hr.setEconomicPl(0);
        hr.setFinancialPl(0);
        hr.setHumanPl(0);
        saveEntity(hr);
    }

    private void saveEntity(BaseEntity hr) {
        entityManager().persist(hr);
        entityManager().flush();
        log.info("hr id = " + hr.getId());
    }

    @Test
    public void createExternalResource() {
        log.info("Creating External Resource");
        ExternalResource hr = new ExternalResource();
        hr.setAssetId("ER-1");
        hr.setName("ExternalResource 1");
        hr.setDescription("Description for the ExternalResource");
        hr.setCritical(true);
        hr.setEconomicPl(0);
        hr.setFinancialPl(0);
        hr.setHumanPl(0);
        saveEntity(hr);
    }

    @Test
    public void createPhysicalResource() {
        log.info("Creating External Resource");
        PhysicalResource hr = new PhysicalResource();
        hr.setAssetId("PR-1");
        hr.setName("PhysicalResource 1");
        hr.setDescription("Description for the PhysicalResource");
        hr.setCritical(true);
        hr.setEconomicPl(0);
        hr.setFinancialPl(0);
        hr.setHumanPl(0);
        hr.setYearBuilt(1970);
        hr.setPostalCode("20171");
        hr.setReplacementCostDTO(new AmountDTO(BigDecimal.valueOf(1.5), MoneyUnitType.MILLION));
        saveEntity(hr);
    }

    @Test
    public void getAllAssets() {
        String queryString = "select asset from Asset asset";
        printAssets(queryString);
    }

    private void printAssets(String queryString) {
        Query query = entityManager().createQuery(queryString);
        List<Asset> resultList = query.getResultList();
        log.info("Number of records : " + resultList.size());
        for (Asset asset : resultList) {
            log.info(asset.getAssetType() + " - " + asset.getName());
        }
    }

    @Test
    public void getHumanResources() {
        String queryString = "select asset from HumanResource asset";
        printAssets(queryString);
    }
    @Test
    public void getExternalResources() {
        String queryString = "select asset from ExternalResource asset";
        printAssets(queryString);
    }
    @Test
    public void getPhysicalResources() {
        String queryString = "select asset from PhysicalResource asset";
        printAssets(queryString);
    }
    @Test
    public void persistAddress() {
        Address address = new Address();
        address.setStreetAddress("14030 Thunderbolt Place");
        address.setUnit("Suite 900");
        address.setCity("Chantilly");
        address.setState("VA");
        address.setPostalCode("20151");
        address.setCountry("US");
        saveEntity(address);
    }
}
