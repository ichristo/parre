/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.server.rpt;

import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.engine.xml.JRXmlWriter;

import org.apache.log4j.Logger;
import org.junit.Test;

public class JasperReportTest {
	private final static transient Logger log = Logger.getLogger(JasperReportTest.class);
	
	@Test
	public void testReverseEngineerJasper() throws Exception {
		//String sourcePath = "jasperreports/ParreRptPDF.jasper";
		String outputPath = "ParreRptPDF.jrxml";
//		JasperReport report = (JasperReport) JRLoader.loadObject(sourcePath);
		JasperReport report = (JasperReport) JRLoader.loadObject(JasperReport.class.getResourceAsStream("/jasperreports/ParreRptPDF.jasper"));
		JRXmlWriter.writeReport(report, outputPath, "UTF-8");
		log.info("ouch!");
	}
}
