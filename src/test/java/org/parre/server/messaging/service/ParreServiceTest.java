/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.server.messaging.service;

import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.junit.Test;
import org.parre.client.messaging.ParreService;
import org.parre.server.domain.DirectedThreat;
import org.parre.server.domain.DpAnalysis;
import org.parre.server.domain.ParreAnalysis;
import org.parre.server.domain.manager.DpManager;
import org.parre.server.domain.manager.ParreAnalysisManager;
import org.parre.server.messaging.invocation.ServiceTest;
import org.parre.server.messaging.service.ParreBaseService;
import org.parre.server.messaging.service.ParreServiceImpl;
import org.parre.shared.AppInitData;
import org.parre.shared.LabelValueDTO;
import org.parre.shared.LoginInfo;
import org.parre.shared.ParreAnalysisDTO;
import org.parre.shared.ThreatDTO;


/**
 * The Class ParreServiceTest.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 11/29/11
 */
public class ParreServiceTest extends ServiceTest {
    private static transient final Logger log = Logger.getLogger(ParreServiceTest.class);
    private ParreService service = (ParreService)ParreServiceImpl.getInstance();

    @Test
    public void getAppInitData() {
        AppInitData appInitData = ParreServiceImpl.getInstance().getAppInitData();
        List<ParreAnalysisDTO> activeParreAnalyses = appInitData.getActiveParreAnalyses();
        for (ParreAnalysisDTO activeParreAnalyse : activeParreAnalyses) {
            log.info(activeParreAnalyse.getName());
        }
        
        // Checking for stat values
        log.info("Statistical values: " + appInitData.getStatisticalValuesDTO().toString());
    }
    /*@Test
    public void getAssetsForAnalysis() {
        List<Asset> assetsForAnalysis = ParreServiceImpl.getInstance().getAssetsForAnalysis(1L);
        
        log.info("Assets For Analysis : " + assetsForAnalysis != null ? assetsForAnalysis.size() : "Not Found");
    }
*/
    @Test
    public void saveParreAnalysis() {
        List<ParreAnalysis> parreAnalysis = ParreAnalysisManager.getActiveParreAnalyses(1L, entityManager());
        for (ParreAnalysis parreAnalysi : parreAnalysis) {
        	ParreAnalysisDTO dto = parreAnalysi.createDTO();
            dto.setCreatedTime(new Date());
            parreAnalysi.copyFrom(dto);
            entityManager().persist(parreAnalysi);
        }
    }
    @Override
    protected LoginInfo createLoginInfo() {
        LoginInfo info = new LoginInfo();
        info.setEmailAddress("sdhaliwal@aemcorp.com");
        return info;
    }
    @Test
    public void getContacts() {
        List<LabelValueDTO> contacts = service.getContacts();
        for (LabelValueDTO contact : contacts) {
            log.info(contact);
        }
    }

    @Test
    public void getProductServices() {
        List<LabelValueDTO> productServices = service.getProductServices();
        log.info("Number Of Product Services : " + productServices.size());
    }
    
    @Test
    public void testGetDirectedThreatAnalysisDTOs() {
    	List<ThreatDTO> results = ((ParreBaseService)service).getDirectedThreatAnalysisDTOs(2L);
    	log.debug("Found " + results.size() + " directed threats.");
    	for (ThreatDTO result : results) {
    		log.debug("threat: " + result.toString());
    	}
    }
    
    @Test
    public void testGetCurrentDpAnalysis() {
    	DpAnalysis dp = DpManager.getCurrentAnalysis(1L, entityManager());
    	log.debug("Found: " + String.valueOf(dp));
    }

    @Test
    public void testDeleteParreAnalysis() {
        ParreServiceImpl.getInstance().deleteParreAnalysis(34L);
    }
    
}
