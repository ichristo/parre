/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.server.messaging.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.junit.Test;
import org.parre.server.messaging.invocation.ServiceTest;
import org.parre.server.messaging.service.AssetServiceImpl;
import org.parre.shared.AssetDTO;
import org.parre.shared.AssetSearchDTO;
import org.parre.shared.NoteDTO;
import org.parre.shared.PhysicalResourceDTO;
import org.parre.shared.ThreatDTO;
import org.parre.shared.types.AssetType;


/**
 * The Class AssetServiceTest.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 5/4/12
 */
public class AssetServiceTest extends ServiceTest {
    private static transient final Logger log = Logger.getLogger(AssetServiceTest.class);

    private AssetServiceImpl service = (AssetServiceImpl) AssetServiceImpl.getInstance();
    @Test
    public void testGetAssetSearchResults() throws Exception {
    	
    	AssetSearchDTO searchDTO = new AssetSearchDTO();
    	searchDTO.setParreAnalysisId(1L);
    	
		List<AssetDTO> results = service.getAssetSearchResults(searchDTO );
		
		log.debug("Found: " + results.size() + " assets!");
		for (AssetDTO result : results) {
			log.debug(result.toString());
		}
    }

    @Test
    public void testGetStateList() throws Exception {

    }

    @Test
    public void testSaveAsset() throws Exception {
        AssetDTO newAssetDTO = new AssetDTO() {
            @Override
            public AssetType getAssetType() {
                return AssetType.HUMAN_ASSET;
            }

            @Override
            protected AssetDTO createClone() {
                return null;
            }
        };

        newAssetDTO.setAssetId("5");
        newAssetDTO.setCritical(true);
        newAssetDTO.setEconomicPl(3);
        newAssetDTO.setFinancialPl(2);
        newAssetDTO.setHumanPl(5);
        newAssetDTO.setParreAnalysisId(3l);
        newAssetDTO.setRemoved(false);
        newAssetDTO.setName("Some asset");

        List<NoteDTO> noteDTOs = new ArrayList<NoteDTO>();
        noteDTOs.add(new NoteDTO());

        noteDTOs.get(0).setDateCreated(new Date());
        noteDTOs.get(0).setMessage("This is a message.");
        noteDTOs.get(0).setUserName("kjones");

        newAssetDTO.setNoteDTOs(noteDTOs);

        newAssetDTO = service.saveAsset(newAssetDTO, newAssetDTO.getParreAnalysisId());
        log.info("Saved " + newAssetDTO.getName());
        for(NoteDTO noteDTO : newAssetDTO.getNoteDTOs()) {
            log.info("AssetNoteDTO = " + noteDTO.getMessage());
        }

    }

    @Test
    public void testGetActiveThreats() throws Exception {
        List<ThreatDTO> activeThreats = service.getActiveThreats(10L);
    }

    @Test
    public void testDeleteAsset() throws Exception { 
    	AssetDTO assetDTO = null;
    	try {
	    	assetDTO = new PhysicalResourceDTO();
	    	assetDTO.setDescription("Delete me");
	    	assetDTO.setName("DeleteMe");
	    	
	    	List<NoteDTO> notes = new ArrayList<NoteDTO>();
	    	for (int i = 0; i < 5; i++) {
	    		NoteDTO note = new NoteDTO();
	    		note.setDateCreated(new Date());
	    		note.setMessage("DeleteMe note: " + i);
	    		note.setUserName("Arthur Dent");
	    		note.setNew(true);
	    		notes.add(note);
	    	}
	    	assetDTO.setNoteDTOs(notes);
	    	
	    	assetDTO.setAssetId("test" + String.valueOf(new java.util.Date()));
	    	assetDTO.setParreAnalysisId(1L);
	    	assetDTO = service.saveAsset(assetDTO, 1L);
	    	log.info("Saved asset, new id is: " + assetDTO.getId());
	    	
	    	
    	} catch (Exception ex) {
    		log.error("Error while persisting dummy record", ex);
    		assetDTO = null;
    	}
    	
    	try {
    		if (assetDTO != null) {
    			service.deleteAsset(assetDTO, assetDTO.getParreAnalysisId());
    	    	entityManager().flush();
    		}
    	} catch (Exception ex) {
    		log.error("Error while removing dummy record", ex);
    	}
    	
    	try {
    		entityManager().getTransaction().rollback();
    	} catch (Exception ex) {
    		log.error("Error while rolling back transaction!", ex);
    	}
	    	
    }
    
    @Test
    public void testHasActiveThreats() {
    	boolean hasActiveThreats = service.hasActiveThreats(30L);
    	log.info("Has active threats: " + hasActiveThreats);
    }

    @Test
    public void testGetAssetThreatLevels() throws Exception {

    }
}
