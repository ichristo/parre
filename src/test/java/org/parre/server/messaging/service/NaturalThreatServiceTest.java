/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.server.messaging.service;

import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

import org.apache.log4j.Logger;
import org.junit.Test;
import org.parre.client.messaging.NaturalThreatService;
import org.parre.server.domain.ParreAnalysis;
import org.parre.server.messaging.invocation.ServiceTest;
import org.parre.server.messaging.service.NaturalThreatServiceImpl;
import org.parre.shared.EarthquakeProfileDTO;
import org.parre.shared.HurricaneLookupDataDTO;



/**
 * The Class NaturalThreatServiceTest.
 */
public class NaturalThreatServiceTest extends ServiceTest {
	private final static transient Logger log = Logger.getLogger(NaturalThreatServiceTest.class);
	
	private NaturalThreatService service = (NaturalThreatService)NaturalThreatServiceImpl.getInstance();
	
	@Test
	public void testEarthquakeProfileLookup() {
		EarthquakeProfileDTO profile = service.getEarthquakeProfile(16L);
		log.debug("Found profile: " + profile);
	}
	
	@Test
	public void testHurricaneLookupData() {
		List<HurricaneLookupDataDTO> results = service.getHurricaneLookupData();
		
		if (log.isDebugEnabled()) {
			for (HurricaneLookupDataDTO dto : results) {
				log.debug("Found hurricane profile: " + dto.toString());
			}
		}
		
		if (results.size() > 0) {
			assertEquals(7, results.size());
		}
	}
	
	//@Test
	@SuppressWarnings("unchecked")
	public void testRecalculateAllNaturals() {
		List<ParreAnalysis> results = entityManager().createQuery("select p from ParreAnalysis p").getResultList();
		
		for (ParreAnalysis parreAnalysis : results) {
			log.info("Processing parreAnalysis: " + String.valueOf(parreAnalysis));
			if (parreAnalysis.getId().intValue() == 23) {
				Boolean success = service.recalculateAllNaturalThreats(parreAnalysis.getId());
				log.info("Success? " + success);
			}
		}
		
		entityManager().getTransaction().commit();
		
	}
	
	@Test
	public void bigDecimalRoundingTest() throws Exception {
		BigDecimal returnSpeed = new BigDecimal("110.00");
		BigDecimal probability = BigDecimal.ZERO;
		probability = BigDecimal.ONE.divide(returnSpeed, 10, RoundingMode.HALF_UP);
		log.info("110.00 result: " + String.valueOf(probability));
		
		returnSpeed = new BigDecimal("110");
		probability = BigDecimal.ZERO;
		probability = BigDecimal.ONE.divide(returnSpeed, 10, RoundingMode.HALF_UP);
		log.info("110.00 result: " + String.valueOf(probability));
		
	}
	
	@Test
	public void bigDecimalCompareTest() {
		BigDecimal test = new BigDecimal("0.00");
		
		if (test.equals(BigDecimal.ZERO)) {
			log.info("Equals!");
		} else {
			log.info("Not equals!");
		}
		
		if (test.intValue() == 0) {
			log.info("Int value equals 0!");
		} else {
			log.info("Int value <> 0!");
		}
	}
}
