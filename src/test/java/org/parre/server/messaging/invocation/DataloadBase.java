/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.server.messaging.invocation;

import javax.persistence.EntityManager;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.parre.server.messaging.invocation.ServiceInvocationContext;
import org.parre.server.persistence.EntityManagerRegistry;
import org.parre.server.persistence.PersistenceContextRegistry;
import org.parre.shared.LoginInfo;


/**
 * The Class DataloadBase.
 */
public class DataloadBase {

	private static transient final Logger log = Logger.getLogger(ServiceTest.class);
	
    @Before
    public void initServiceContext() {
        ServiceInvocationContext context = new ServiceInvocationContext(createLoginInfo());
        context.setEntityManager(entityManager());
        ServiceInvocationContext.init(context);
        getEntityManagerRegistry().beginTransaction();
    }

    protected LoginInfo createLoginInfo() {
        LoginInfo loginInfo = new LoginInfo();
        loginInfo.setEmailAddress("sdhaliwal@aemcorp.com");
        return loginInfo;
    }

    @After
    public void closeServiceContext() {
    	getEntityManagerRegistry().commitTransaction();
        getEntityManagerRegistry().closeEntityManager();
        ServiceInvocationContext.destroy();
    }
    
    @AfterClass
    public static void shutdown() {
        PersistenceContextRegistry.shutdown();
    }

    protected EntityManager entityManager() {
        return getEntityManagerRegistry().entityManager();
    }

    private EntityManagerRegistry getEntityManagerRegistry() {
    	String persistenceUnitName = System.getProperty("parre.persistenceUnit", "parre-testing");
    	System.setProperty("parre.persistenceUnit", persistenceUnitName);
    	if (log.isDebugEnabled()) { log.debug("Getting EntityManagerRegistry with key: " + persistenceUnitName); }
        return PersistenceContextRegistry.getEntityManagerAdaptor(persistenceUnitName);
    }
}
