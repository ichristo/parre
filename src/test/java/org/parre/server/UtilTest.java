/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.server;

import static org.junit.Assert.*;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import junit.framework.Assert;

import org.apache.log4j.Logger;
import org.junit.Test;
import org.parre.server.domain.CounterMeasure;
import org.parre.server.domain.ParreAnalysis;
import org.parre.server.domain.StatValues;
import org.parre.shared.AmountDTO;
import org.parre.shared.ParreAnalysisDTO;
import org.parre.shared.StatValuesDTO;
import org.parre.shared.types.ParreAnalysisStatusType;


/**
 * The Class UtilTest.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 1/17/12
 */
public class UtilTest {
    private static transient final Logger log = Logger.getLogger(UtilTest.class);
    @Test
    public void testBigDecimal() {
        BigDecimal bd = new BigDecimal("0.000000000525");
        DecimalFormat format = new DecimalFormat("'0'.000E00");
        log.info(format.format(bd));
        try {
            Number parse = format.parse("0.525E-9");
            log.info(parse);
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void copyProperties() {
        ParreAnalysisDTO dto = new ParreAnalysisDTO();
        dto.setStatus(ParreAnalysisStatusType.ACTIVE);
        dto.setCreatedTime(new Date());
        ParreAnalysis analysis = new ParreAnalysis();
        analysis.copyFrom(dto);
        log.info(analysis.getStatus());
        log.info(analysis.getCreatedTime());
    }
    @Test
    public void statValuesChanged() {
        StatValuesDTO statValuesDTO = new StatValuesDTO();
        statValuesDTO.setFatalityDTO(new AmountDTO(BigDecimal.valueOf(5.3)));
        statValuesDTO.setSeriousInjuryDTO(new AmountDTO(BigDecimal.valueOf(1.2)));
        StatValues statValues = new StatValues(statValuesDTO);
        Assert.assertFalse(statValues.isChanged(statValuesDTO));
        statValuesDTO.setSeriousInjuryDTO(new AmountDTO(BigDecimal.valueOf(1.201)));
        Assert.assertTrue(statValues.isChanged(statValuesDTO));
    }
    @Test
    public void emptySetTest() {
    	boolean failed = false;
    	
    	try {
	    	Set<CounterMeasure> test = Collections.<CounterMeasure>emptySet();
	    	test.add(new CounterMeasure());
    	} catch (UnsupportedOperationException uoe) {
    		failed = true;
    	}
    	
    	assertTrue(failed);
    	
    	failed = false;
    	try {
    		Set<CounterMeasure> test = new HashSet<CounterMeasure>();
    		test.add(new CounterMeasure());
    	} catch (Exception ex) {
    		failed = true;
    	}
    	
    	assertFalse(failed);
    }
}
