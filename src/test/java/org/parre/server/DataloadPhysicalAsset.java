/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.server;

import java.util.List;

import org.junit.Test;
import org.parre.server.domain.PhysicalResource;
import org.parre.server.messaging.invocation.DataloadBase;
import org.parre.shared.types.PhysicalAssetType;


/**
 * This junit test updates the asset table to fill in an appropriate physical asset type.
 *
 * @author kspokas
 */
public class DataloadPhysicalAsset extends DataloadBase {

	@SuppressWarnings("unchecked")
	@Test
	public void populatePhysicalAsset() {
		List<PhysicalResource> physicalAssets = entityManager().createQuery("select pr from PhysicalResource pr").getResultList();
		
		for (PhysicalResource physicalAsset : physicalAssets) {
			if (physicalAsset.getPhysicalAssetType() == null && physicalAsset.getDamageFactor() != null) {
				String cleanDamageFactor = physicalAsset.getDamageFactor().stripTrailingZeros().toString();
				for (PhysicalAssetType type : PhysicalAssetType.values()) {
					if (type.getDamageFactor().equals(cleanDamageFactor)) {
						physicalAsset.setPhysicalAssetType(type);
						entityManager().persist(physicalAsset);
						entityManager().flush();
						break;
					}
				}
			}
		}
		
	}
}
