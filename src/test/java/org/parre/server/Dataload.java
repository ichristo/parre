/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */

/* TODO: Merge this back into the LGPL version */
package org.parre.server;

import java.io.File;
import java.util.List;

import org.apache.log4j.Logger;
import org.junit.Test;
import org.parre.server.ReportLookupDataload;
import org.parre.server.domain.ReportSectionLookup;
import org.parre.server.NaturalThreatLookupData;
import org.parre.server.ParreDatabase;
import org.parre.server.messaging.invocation.DataloadBase;
import org.parre.shared.DetectionLikelihoodDTO;
import org.parre.shared.EarthquakeLookupDataDTO;
import org.parre.shared.HurricaneLookupDataDTO;
import org.parre.shared.ProxyCityDTO;
import org.parre.shared.ProxyTargetTypeDTO;
import org.parre.shared.ProxyTierDTO;
import org.parre.shared.TornadoLookupDataDTO;
import org.parre.shared.ZipLookupDataDTO;


/**
 * User: Jagmeet
 * Date: 7/10/11
 * Time: 2:28 PM
 */
public class Dataload extends DataloadBase {
    private static transient final Logger log= Logger.getLogger(Dataload.class);
    private static final String DATA_DIR = "src/test/java/data";
    @org.junit.Test
    public void initData() {
        ParreDatabase db = ParreDatabase.get();
        db.initData(new File(DATA_DIR + "/parreData.txt"));
    }
    @org.junit.Test
    public void initUsers() {
        ParreDatabase db = ParreDatabase.get();
        db.initUsers(new File(DATA_DIR + "/parreUsers.txt"));
        log.info(db.getUser("admin@aemcorp.com"));
    }
    @org.junit.Test
    public void initStates() {
        ParreDatabase db = ParreDatabase.get();
        db.initLookupValues(new File(DATA_DIR + "/lookupValues.txt"));
        log.info(db.getStateList());
    }

    @org.junit.Test
    public void initQuestions() {
        ParreDatabase db = ParreDatabase.get();
        db.initQuestions(new File(DATA_DIR + "/uriQuestions.txt"));
    }
    @org.junit.Test
    public void initCityTierTotals() {
        ParreDatabase db = ParreDatabase.get();
        db.initCityTierTotals(new File(DATA_DIR + "/cityTierTotals.txt"));
        List<ProxyTierDTO> cityTierTotalDTOs = db.getCityTierTotalsDTOs();
        for (ProxyTierDTO cityTierDTO : cityTierTotalDTOs) {
            log.info(cityTierDTO);
        }
    }

    @org.junit.Test
    public void initCityTiers() {
        ParreDatabase db = ParreDatabase.get();
        db.initCityTiers(new File(DATA_DIR + "/cityTiers.txt"));
        List<ProxyCityDTO> cityTierDTOs = db.getCityTierDTOs();
        for (ProxyCityDTO cityTierDTO : cityTierDTOs) {
            log.info(cityTierDTO);
        }
    }

    @org.junit.Test
    public void initTargetTypes() {
        ParreDatabase db = ParreDatabase.get();
        db.initTargetTypes(new File(DATA_DIR + "/targetTypes.txt"));
        List<ProxyTargetTypeDTO> targetTypesDTOs = db.getTargetTypesDTOs();
        for(ProxyTargetTypeDTO targetTypesDTO : targetTypesDTOs) {
            log.info(targetTypesDTO);
        }
    }

    @org.junit.Test
    public void initDetectionLikelihood() {
        ParreDatabase db = ParreDatabase.get();
        db.initDetectionLikelihood(new File(DATA_DIR + "/detectionLikelihood.txt"));
        List<DetectionLikelihoodDTO> detectionLikelihoodDTOs = db.getDetectionLikelihoodDTOs();
        for(DetectionLikelihoodDTO detectionLikelihoodDTO : detectionLikelihoodDTOs) {
            log.info(detectionLikelihoodDTO);
        }
    }

    @Test
    public void initEarthquakeLookupData() {
        NaturalThreatLookupData ntl = NaturalThreatLookupData.get();
        ntl.initEarthquakeLookupData(new File(DATA_DIR + "/earthquakeLookupData.txt"));
        List<EarthquakeLookupDataDTO> earthquakeLookupDataDTOs = ntl.getEarthquakeLookupDataDTOs();
        for (EarthquakeLookupDataDTO earthquakeLookupDataDTO : earthquakeLookupDataDTOs) {
            log.info(earthquakeLookupDataDTO);
        }
    }

    @Test
    public void initHurricaneLookupData() {
    	NaturalThreatLookupData ntl = NaturalThreatLookupData.get();
        ntl.initHurricaneCalc(new File(DATA_DIR + "/hurricaneLookupData.txt"));
        List<HurricaneLookupDataDTO> hurricaneLookupDataDTOs = ntl.getHurricaneLookupDataDTOs();
        for (HurricaneLookupDataDTO hurricaneLookupDataDTO : hurricaneLookupDataDTOs) {
            log.info(hurricaneLookupDataDTO);
        }
    }

    @Test
    public void initIceworkLookupData() {
    	NaturalThreatLookupData ntl = NaturalThreatLookupData.get();
        ntl.initIceworkCalc(new File(DATA_DIR + "/iceworkLookupData.txt"));
    }

    @Test
    public void initTornadoLookupData() {
    	NaturalThreatLookupData ntl = NaturalThreatLookupData.get();
        ntl.initTornadoCalc(new File(DATA_DIR + "/tornadoLookupData.txt"));
        List<TornadoLookupDataDTO> tornadoLookupDataDTOs = ntl.getTornadoLookupDataDTOs();
        for (TornadoLookupDataDTO tornadoLookupDataDTO : tornadoLookupDataDTOs) {
            log.info(tornadoLookupDataDTO);
        }
    }

    @Test
    public void initWindLookupData() {
    	NaturalThreatLookupData ntl = NaturalThreatLookupData.get();
        ntl.initWindLookupData(new File(DATA_DIR + "/windLookupData.txt"));
    }

    @Test
    public void initZipLookupData() {
    	ParreDatabase db = ParreDatabase.get();
    	db.initZipCodes(new File(DATA_DIR + "/zipCodeCounty.txt"));
    	List<ZipLookupDataDTO> zipCodes = db.getZipLookupDataDTOs();
    	
    	log.info("Loaded: " + zipCodes.size() + " zip codes from the zip code data file.");
    }
    
    @Test
    public void getPersistedZipData() {
    	ParreDatabase db = ParreDatabase.get();
    	List<ZipLookupDataDTO> zipCodes = db.getZipLookupDataDTOs();
    	log.info("Found: " + zipCodes.size() + " zip codes from the database.");

    }

    @Test
    public void initReportSectionLookup() {
    	for (ReportSectionLookup section : ReportLookupDataload.getReportSections()) {
    		entityManager().persist(section);
    	}
    }

}
