/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.shared;


import java.math.BigDecimal;

/**
 * The Class NaturalThreatDTO.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 12/5/11
 */
public class NaturalThreatDTO extends AssetThreatDTO implements HasFinancialTotal {
    private Integer fatalities = 0;
    private Integer seriousInjuries = 0;
    private AmountDTO financialImpactDTO = AmountDTO.createZero();
    private AmountDTO economicImpactDTO = AmountDTO.createZero();
    private BigDecimal vulnerability = BigDecimal.ZERO;
    private BigDecimal lot = BigDecimal.ZERO;
    private AmountDTO operationalLossDTO = AmountDTO.createZero();
    private AmountDTO financialTotalDTO = AmountDTO.createZero();
    private AmountDTO totalRiskDTO = AmountDTO.createZero();
    private BigDecimal durationDays = BigDecimal.ZERO;
    private BigDecimal severityMgd = BigDecimal.ZERO;
    
    private Boolean manuallyEntered = Boolean.FALSE;
    private Boolean useDefaultValues = Boolean.TRUE;
    private BigDecimal latitudeOverride;
    private BigDecimal longitudeOverride;
    private String countyOverride;
    private String stateCodeOverride;
    private Boolean floodRisk = Boolean.FALSE;
    
    private Long earthquakeProfileId;
    private Long hurricaneProfileId;
    
    private BigDecimal designSpeed = BigDecimal.ZERO;
    
    private IceStormWorkDTO iceStormWorkDTO;
    
    public Integer getFatalities() {
        return fatalities;
    }

    public void setFatalities(Integer fatalities) {
        this.fatalities = fatalities;
    }

    public Integer getSeriousInjuries() {
        return seriousInjuries;
    }

    public void setSeriousInjuries(Integer seriousInjuries) {
        this.seriousInjuries = seriousInjuries;
    }

    public AmountDTO getFinancialImpactDTO() {
        return financialImpactDTO;
    }

    public void setFinancialImpactDTO(AmountDTO financialImpactDTO) {
        this.financialImpactDTO = financialImpactDTO;
    }

    public AmountDTO getEconomicImpactDTO() {
        return economicImpactDTO;
    }

    public void setEconomicImpactDTO(AmountDTO economicImpactDTO) {
        this.economicImpactDTO = economicImpactDTO;
    }

    public BigDecimal getVulnerability() {
        return vulnerability;
    }

    public void setVulnerability(BigDecimal vulnerability) {
        this.vulnerability = vulnerability;
    }

    public BigDecimal getLot() {
        return lot;
    }

    public void setLot(BigDecimal lot) {
        this.lot = lot;
    }

    public AmountDTO getOperationalLossDTO() {
		return operationalLossDTO;
	}

	public void setOperationalLossDTO(AmountDTO operationalLoss) {
		this.operationalLossDTO = operationalLoss;
	}

	public AmountDTO getFinancialTotalDTO() {
        return financialTotalDTO;
    }

    public void setFinancialTotalDTO(AmountDTO financialTotalDTO) {
        this.financialTotalDTO = financialTotalDTO;
    }
    
    public AmountDTO getTotalRiskDTO() {
        return totalRiskDTO;
    }

    public void setTotalRiskDTO(AmountDTO totalRiskDTO) {
        this.totalRiskDTO = totalRiskDTO;
    }
    
	public BigDecimal getDurationDays() {
		return durationDays;
	}

	public void setDurationDays(BigDecimal durationDays) {
		this.durationDays = durationDays;
	}

	public BigDecimal getSeverityMgd() {
		return severityMgd;
	}

	public void setSeverityMgd(BigDecimal severityMgd) {
		this.severityMgd = severityMgd;
	}
	
	public Boolean getManuallyEntered() {
		return manuallyEntered != null ? manuallyEntered : Boolean.FALSE;
	}

	public void setManuallyEntered(Boolean manuallyEntered) {
		this.manuallyEntered = manuallyEntered;
	}

	public Boolean getUseDefaultValues() {
		return useDefaultValues;
	}

	public void setUseDefaultValues(Boolean useDefaultValues) {
		this.useDefaultValues = useDefaultValues == null ? Boolean.TRUE : useDefaultValues;
	}

	public BigDecimal getLatitudeOverride() {
		return latitudeOverride;
	}

	public void setLatitudeOverride(BigDecimal latitudeOverride) {
		this.latitudeOverride = latitudeOverride;
	}

	public BigDecimal getLongitudeOverride() {
		return longitudeOverride;
	}

	public void setLongitudeOverride(BigDecimal longitudeOverride) {
		this.longitudeOverride = longitudeOverride;
	}

	public String getCountyOverride() {
		return countyOverride;
	}

	public void setCountyOverride(String countyOverride) {
		this.countyOverride = countyOverride;
	}

	public String getStateCodeOverride() {
		return stateCodeOverride;
	}

	public void setStateCodeOverride(String stateCodeOverride) {
		this.stateCodeOverride = stateCodeOverride;
	}

	public Boolean getFloodRisk() {
		return floodRisk;
	}

	public void setFloodRisk(Boolean floodRisk) {
		this.floodRisk = floodRisk == null ? Boolean.FALSE : floodRisk;
	}

	public Long getEarthquakeProfileId() {
		return earthquakeProfileId;
	}

	public void setEarthquakeProfileId(Long earthquakeProfileId) {
		this.earthquakeProfileId = earthquakeProfileId;
	}

	public Long getHurricaneProfileId() {
		return hurricaneProfileId;
	}

	public void setHurricaneProfileId(Long hurricaneProfileId) {
		this.hurricaneProfileId = hurricaneProfileId;
	}
	
	public BigDecimal getDesignSpeed() {
		return designSpeed;
	}

	public void setDesignSpeed(BigDecimal designSpeed) {
		this.designSpeed = designSpeed == null ? BigDecimal.ZERO : designSpeed;
	}

	public IceStormWorkDTO getIceStormWorkDTO() {
		if (iceStormWorkDTO == null) {
			iceStormWorkDTO = new IceStormWorkDTO();
		}
		return iceStormWorkDTO;
	}

	public void setIceStormWorkDTO(IceStormWorkDTO iceStormWorkDTO) {
		this.iceStormWorkDTO = iceStormWorkDTO;
	}

	@Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("NaturalThreatDTO");
        sb.append(", fatalities=").append(fatalities);
        sb.append(", seriousInjuries=").append(seriousInjuries);
        sb.append(", operationalLossDTO=").append(operationalLossDTO);
        sb.append(", financialImpactDTO=").append(financialImpactDTO);
        sb.append(", financialTotalDTO=").append(financialTotalDTO);
        sb.append(", totalRiskDTO=").append(totalRiskDTO);
        sb.append(", vulnerability=").append(vulnerability);
        sb.append(", lot=").append(lot);
        sb.append(", duration(days)=").append(durationDays);
        sb.append(", severity=").append(severityMgd);
        sb.append(", manuallyEntered=").append(manuallyEntered);
        sb.append(", useDefaultValues=").append(useDefaultValues);
        sb.append(", latitudeOverride=").append(latitudeOverride);
        sb.append(", longitudeOverride=").append(longitudeOverride);
        sb.append(", countyOverride=").append(countyOverride);
        sb.append(", stateCodeOverride").append(stateCodeOverride);
        sb.append(", floodRisk=").append(floodRisk);
        sb.append(", earthquakeProfileId=").append(earthquakeProfileId);
        sb.append(", hurricaneProfileId=").append(hurricaneProfileId);
        sb.append(", designSpeed=").append(designSpeed);
        sb.append('}');
        return sb.toString();
    }
}
