/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.shared;


import java.math.BigDecimal;
import java.util.*;

import org.parre.shared.types.AnalysisTreeType;

/**
 * User: keithjones
 * Date: 6/18/13
 * Time: 11:48 AM
*/
public class DiagramAnalysisDTO extends BaseDTO {

    private Set<CounterMeasureDTO> counterMeasureDTOs = Collections.emptySet();

    private AnalysisTreeNodeDTO analysisTreeRoot;

    private AnalysisTreeType analysisTreeType;

    public DiagramAnalysisDTO(AnalysisTreeType analysisTreeType) {
        this.analysisTreeType = analysisTreeType;
        analysisTreeRoot = new AnalysisTreeNodeDTO();
    }

    public DiagramAnalysisDTO() {
        analysisTreeRoot = new AnalysisTreeNodeDTO();
    }

    public DiagramAnalysisDTO(AnalysisTreeNodeDTO selectedObject) {
        analysisTreeRoot = selectedObject;
    }

    public Set<CounterMeasureDTO> getCounterMeasureDTOs() {
        return counterMeasureDTOs;
    }

    public void setCounterMeasureDTOs(Set<CounterMeasureDTO> counterMeasureDTOs) {
        this.counterMeasureDTOs = counterMeasureDTOs;
    }

    public AnalysisTreeNodeDTO getAnalysisTreeRootDTO() {
        return analysisTreeRoot;
    }

    public void setAnalysisTreeRootDTO(AnalysisTreeNodeDTO analysisTreeRoot) {
        this.analysisTreeRoot = analysisTreeRoot;
    }

    public AnalysisTreeType getAnalysisTreeType() {
        return analysisTreeType;
    }

    public void setAnalysisTreeType(AnalysisTreeType analysisTreeType) {
        this.analysisTreeType = analysisTreeType;
    }

    public BigDecimal calculateProbability() {
        if (isEventTree()) {
            return calculateEventTreeProbability(analysisTreeRoot.getChildDTOs(), BigDecimal.ONE);
        }
        else {
            List<BigDecimal> probabilities = new ArrayList<BigDecimal>();
            calculateLogicDiagramProbability(analysisTreeRoot, probabilities);
            BigDecimal sum = BigDecimal.ZERO;
            for (BigDecimal bigDecimal : probabilities) {
                sum = sum.add(bigDecimal);
            }
            return sum;
        }
    }

    private void calculateLogicDiagramProbability(AnalysisTreeNodeDTO parentNodeDTO, List<BigDecimal> probabilities) {

        if (!parentNodeDTO.hasChildren()) {
            BigDecimal calculatedProbability = BigDecimal.ONE;
            while (!parentNodeDTO.isRootNode()) {
                calculatedProbability = calculatedProbability.multiply(parentNodeDTO.getProbability());
                parentNodeDTO = parentNodeDTO.getParentDTO();
            }
            probabilities.add(calculatedProbability);
            return;
        }
        List<AnalysisTreeNodeDTO> children = parentNodeDTO.getChildDTOs();
        for (AnalysisTreeNodeDTO child : children) {
            if (isLogicDiagram() || child.getSuccess()) {
                calculateLogicDiagramProbability(child, probabilities);
            }
        }
    }

    private BigDecimal calculateEventTreeProbability(List<AnalysisTreeNodeDTO> childDTOs, BigDecimal calculatedProbability) {
    	
    	// First calculate the current success node
        for (AnalysisTreeNodeDTO childDTO : childDTOs) {
            if (childDTO.getSuccess()) {
                calculatedProbability = calculatedProbability.multiply(childDTO.getProbability());
            }
        }
        // Then recursively recalculate the children, whether they hang off the success or failure child
        for (AnalysisTreeNodeDTO childDTO : childDTOs) {
        	if (childDTO.hasChildren()) {
        		calculatedProbability = calculateEventTreeProbability(childDTO.getChildDTOs(), calculatedProbability);
        	}
        }
        return calculatedProbability;
    }

    public boolean isEventTree() {
        return analysisTreeType.isEventTree();
    }

    public boolean isLogicDiagram() {
        return analysisTreeType.isLogicDiagram();
    }

    public void addCounterMeasure(CounterMeasureDTO counterMeasureDTO) {
        if(counterMeasureDTOs.isEmpty()) {
            counterMeasureDTOs = new HashSet<CounterMeasureDTO>();
        }
        counterMeasureDTOs.add(counterMeasureDTO);
    }

    // Making a copy, want to reset all ids to null so we can later save
	public void clearIds() {
		setId(null);
		if (analysisTreeRoot != null) {
			clearNodeIds(analysisTreeRoot);
		}
	}
	
	// This stop gap functionality will return a version of the tree with children hanging of success rather than root
	public AnalysisTreeNodeDTO getSuccessTree() {
		AnalysisTreeNodeDTO alternateRoot = (AnalysisTreeNodeDTO) analysisTreeRoot.copyInto(new AnalysisTreeNodeDTO());
		updateTier(alternateRoot, analysisTreeRoot);
		return alternateRoot;
	}
	
	private void updateTier(AnalysisTreeNodeDTO alternateRoot, AnalysisTreeNodeDTO originalRoot) {
		AnalysisTreeNodeDTO alternateSuccessNode = null;
		
		for (AnalysisTreeNodeDTO child : originalRoot.getChildDTOs()) {
			AnalysisTreeNodeDTO alternateChild = (AnalysisTreeNodeDTO)child.copyInto(new AnalysisTreeNodeDTO()); 
			alternateRoot.addChildDTO(alternateChild);
			if (alternateChild.getSuccess()) {
				alternateSuccessNode = alternateChild;
			}
		}
		
		for (AnalysisTreeNodeDTO child : originalRoot.getChildDTOs()) {
			if (child.hasChildren()) {
				updateTier(alternateSuccessNode, child);
			}
		}
	}
	
	private void clearNodeIds(AnalysisTreeNodeDTO node) {
		node.setSalt(node.getId());  // Will use salt instead of id when necessary for hashcode/equals
		node.setId(null);
		for (AnalysisTreeNodeDTO child : node.getChildDTOs()) {
			clearNodeIds(child);
		}
	}
}
