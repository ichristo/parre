/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.shared;



import java.math.BigDecimal;

import org.parre.shared.types.LotAnalysisType;

/**
 * User: Swarn S. Dhaliwal
 * Date: 7/14/11
 * Time: 11:20 AM
*/
public class LotDTO extends AssetThreatDTO {
    private LotAnalysisType lotAnalysisType;
    private BigDecimal lot;
    private ProxyIndicationDTO proxyIndicationDTO;

    public LotDTO() {
    }

    public LotDTO(AssetDTO assetDTO, ThreatDTO threatDTO) {
        super(assetDTO, threatDTO);
    }


    public LotAnalysisType getLotAnalysisType() {
        return lotAnalysisType;
    }

    public void setLotAnalysisType(LotAnalysisType lotAnalysisType) {
        this.lotAnalysisType = lotAnalysisType;
    }

    public BigDecimal getLot() {
        return lot;
    }

    public void setLot(BigDecimal lot) {
        this.lot = lot;
    }

    public ProxyIndicationDTO getProxyIndicationDTO() {
        return proxyIndicationDTO;
    }

    public void setProxyIndicationDTO(ProxyIndicationDTO proxyIndicationDTO) {
        this.proxyIndicationDTO = proxyIndicationDTO;
        setLot(proxyIndicationDTO.getCalculatedProbability());
    }

    public boolean hasProxyIndication() {
        return proxyIndicationDTO != null;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("VulnerabilityDTO");
        sb.append(", analysisType=").append(lotAnalysisType);
        sb.append(", probability=").append(lot);
        sb.append('}');
        return sb.toString();
    }
}
