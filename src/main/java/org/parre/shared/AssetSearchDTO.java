/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.shared;

/**
 * User: Swarn S. Dhaliwal
 * Date: 7/14/11
 * Time: 11:15 AM
*/
public class AssetSearchDTO extends BaseDTO {
    private Long parreAnalysisId;
    private String assetId;
    private String assetName;
    private Boolean removed = false;

    public AssetSearchDTO() {
    }

    public AssetSearchDTO(String assetId, String assetName) {
        this.assetId = assetId;
        this.assetName = assetName;
    }

    public AssetSearchDTO(Long parreAnalysisId) {
        this.parreAnalysisId = parreAnalysisId;
    }

    public Long getParreAnalysisId() {
        return parreAnalysisId;
    }

    public void setParreAnalysisId(Long parreAnalysisId) {
        this.parreAnalysisId = parreAnalysisId;
    }

    public String getAssetId() {
        return assetId;
    }

    public void setAssetId(String assetId) {
        this.assetId = assetId;
    }

    public String getAssetName() {
        return assetName;
    }

    public void setAssetName(String assetName) {
        this.assetName = assetName;
    }

    public Boolean getRemoved() {
        return removed;
    }

    public void setRemoved(Boolean removed) {
        this.removed = removed;
    }

	@Override
	public String toString() {
		return "AssetSearchDTO [parreAnalysisId=" + parreAnalysisId
				+ ", assetId=" + assetId + ", assetName=" + assetName
				+ ", removed=" + removed + "]";
	}


   
}
