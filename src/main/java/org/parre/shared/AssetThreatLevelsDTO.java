/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.shared;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * The Class AssetThreatLevelsDTO.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 10/6/11
 */
public class AssetThreatLevelsDTO extends BaseDTO {
    private String assetName;
    private String assetId;
    private Map<Long, Integer> threatLevelMap = new HashMap<Long, Integer>(50);

    public String getAssetName() {
        return assetName;
    }

    public void setAssetName(String assetName) {
        this.assetName = assetName;
    }

    public String getAssetId() {
        return assetId;
    }

    public void setAssetId(String assetId) {
        this.assetId = assetId;
    }

    public Integer getThreatLevel(Long threatId) {
        return threatLevelMap.get(threatId);
    }

    public Integer putThreatLevel(Long threatId, Integer level) {
        return  threatLevelMap.put(threatId, level);
    }

    public Collection<Integer> getThreatLevels() {
        return threatLevelMap.values();
    }
}
