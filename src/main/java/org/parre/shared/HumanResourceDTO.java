/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.shared;

import org.parre.shared.types.AssetType;

/**
 * The Class HumanResourceDTO.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 9/23/11
 */
public class HumanResourceDTO extends AssetDTO {

    @Override
    public AssetType getAssetType() {
        return AssetType.HUMAN_ASSET;
    }

    @Override
    protected AssetDTO createClone() {
        return new HumanResourceDTO();
    }
}
