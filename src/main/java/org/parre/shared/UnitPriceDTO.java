/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.shared;

import com.google.gwt.user.client.rpc.IsSerializable;

import java.math.BigDecimal;

import org.parre.shared.types.MoneyUnitType;

/**
 * The Class UnitPriceDTO.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 9/22/11
 */
public class UnitPriceDTO implements IsSerializable {
    private BigDecimal price;
    private Integer quantity;

    public UnitPriceDTO() {
    }

    public UnitPriceDTO(BigDecimal price, Integer quantity) {
        this.price = price;
        this.quantity = quantity;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public Integer getQuantity() {
        return quantity;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UnitPriceDTO that = (UnitPriceDTO) o;

        if (!price.equals(that.price)) return false;
        if (!quantity.equals(that.quantity)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = price.hashCode();
        result = 31 * result + quantity.hashCode();
        return result;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("Amount");
        sb.append("{quantity=").append(price);
        sb.append(", unit=").append(quantity);
        sb.append('}');
        return sb.toString();
    }

}
