/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.shared;

import java.io.Serializable;

/**
 * The Class AssetThreatLevelDTO.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 10/12/11
 */
public class AssetThreatLevelDTO implements Serializable {
    private Long assetId;
    private Long threatId;
    private Integer level;

    public AssetThreatLevelDTO() {
    }

    public AssetThreatLevelDTO(Long assetId, Long threatId, Integer level) {
        this.assetId = assetId;
        this.threatId = threatId;
        this.level = level;
    }

    public Long getAssetId() {
        return assetId;
    }

    public void setAssetId(Long assetId) {
        this.assetId = assetId;
    }

    public Long getThreatId() {
        return threatId;
    }

    public void setThreatId(Long threatId) {
        this.threatId = threatId;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AssetThreatLevelDTO that = (AssetThreatLevelDTO) o;

        if (!assetId.equals(that.assetId)) return false;
        if (!threatId.equals(that.threatId)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = assetId.hashCode();
        result = 31 * result + threatId.hashCode();
        return result;
    }
}
