/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.shared;

import java.util.HashSet;
import java.util.Set;

/**
 * @author Raju Kanumuri
 * @date 10/30/2013
 */
public class RptCountermeasureDTO extends BaseDTO {
	private String assetName;
	private Set<String> counterMeasures = new HashSet<String>();

	public RptCountermeasureDTO() {
	}

	public RptCountermeasureDTO(String assetName) {
		this.assetName = assetName;
	}

	public String getAssetName() {
		return assetName;

	}

	public void setAssetName(String assetName) {
		this.assetName = assetName;

	}

	public Set<String> getCounterMeasures() {
		return counterMeasures;

	}

	public void setCounterMeasures(Set<String> counterMeasures) {
		this.counterMeasures = counterMeasures;

	}

	@Override
	public String toString() {
		return "RptCountermeasureDTO [assetName=" + assetName
				+ ", counterMeasures=" + counterMeasures + "]";
	}

}
