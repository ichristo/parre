/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.shared;


import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * The Class AnalysisTreeNodeDTO.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 1/12/12
 */
public class AnalysisTreeNodeDTO extends NameDescriptionDTO {
    private Boolean success = false;
    private Boolean leaf = false;
    private BigDecimal probability;
    private AnalysisTreeNodeDTO parentDTO;
    private List<AnalysisTreeNodeDTO> childDTOs = Collections.emptyList();
    
    private Long salt; // Used for hash code and equals when Ids are not available

    public AnalysisTreeNodeDTO() {
        super("", "");
    }

    public AnalysisTreeNodeDTO(AnalysisTreeNodeDTO parentDTO, String name, BigDecimal probability) {
        super(name, name);
        this.parentDTO = parentDTO;
        this.probability = probability;
    }

    public AnalysisTreeNodeDTO(NameDescriptionDTO dto) {
        super(dto);
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public BigDecimal getProbability() {
        return probability;
    }

    public void setProbability(BigDecimal probability) {
        this.probability = probability;
    }

    public Boolean getLeaf() {
        return !isRootNode() && leaf;
    }

    public void setLeaf(Boolean leaf) {
        this.leaf = leaf;
    }

    public AnalysisTreeNodeDTO getParentDTO() {
        return parentDTO;
    }

    public void setParentDTO(AnalysisTreeNodeDTO parentDTO) {
        this.parentDTO = parentDTO;
    }

    public List<AnalysisTreeNodeDTO> getChildDTOs() {
        return childDTOs;
    }

    public void setChildDTOs(List<AnalysisTreeNodeDTO> childDTOs) {
        this.childDTOs = childDTOs;
        for (AnalysisTreeNodeDTO childDTO : childDTOs) {
            childDTO.setParentDTO(this);
        }
    }

    public void addChildDTO(AnalysisTreeNodeDTO childDTO) {
        if (childDTOs.isEmpty()) {
            childDTOs = new ArrayList<AnalysisTreeNodeDTO>();
        }
        childDTO.setParentDTO(this);
        childDTOs.add(childDTO);
    }

    public boolean isRootNode() {
        return parentDTO == null;
    }

    public AnalysisTreeNodeDTO getRootNode() {
        if (isRootNode()) {
            return this;
        }
        return getParentDTO().getRootNode();
    }
    public Long getRootNodeId() {
        return getRootNode().getId();
    }

    public boolean hasChildren() {
        return !childDTOs.isEmpty();
    }
    
    public Long getSalt() {
    	return salt;
    }
    
    public void setSalt(Long salt) {
    	this.salt = salt;
    }
    
    public Long getSaltOrId() {
    	return salt == null ? getId() : salt;
    }

	@Override
	public int hashCode() {
		return salt == null ? super.hashCode() : salt.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (salt != null) {
			AnalysisTreeNodeDTO other = (AnalysisTreeNodeDTO) obj;
			if (salt == null) {
				if (other.salt != null)
					return false;
			} else if (!salt.equals(other.salt)) {
				return false;
			}
			return true;
		}
		return super.equals(obj);
	}
	
	public void sort(List<AnalysisTreeNodeDTO> values) {
		Collections.sort(values, new Comparator<AnalysisTreeNodeDTO>(){
				@Override
				public int compare(AnalysisTreeNodeDTO o1, AnalysisTreeNodeDTO o2){
					return -1 * String.valueOf(o1.success).compareTo(String.valueOf(o2.success));
				}
			});
	}
	
	public AnalysisTreeNodeDTO copyInto(AnalysisTreeNodeDTO dto) {
		super.copyInto(dto);
		dto.setLeaf(leaf);
		dto.setProbability(probability);
		dto.setSuccess(success);
		dto.setSalt(getSaltOrId());
		return dto;
	}

	@Override
	public String toString() {
		return "AnalysisTreeNodeDTO [success=" + success + ", leaf=" + leaf
				+ ", probability=" + probability
				+ ", salt=" + salt  + ", parentDTOId=" + (parentDTO == null ? "null" : parentDTO.getSaltOrId()) + "]";
	}
	
	
}
