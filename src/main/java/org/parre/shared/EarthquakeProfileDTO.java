/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.shared;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * The Class EarthquakeProfileDTO.
 */
public class EarthquakeProfileDTO extends ProfileDTO {
	public EarthquakeProfileDTO() {
	}

	private BigDecimal percentG;
	private BigDecimal returnValue;
	private Set<EarthquakeMagnitudeProbabilityDTO> magnitudeProbabilityDTOs = new HashSet<EarthquakeMagnitudeProbabilityDTO>();
	
	public BigDecimal getPercentG() {
		return percentG;
	}

	public void setPercentG(BigDecimal percentG) {
		this.percentG = percentG;
	}

	public BigDecimal getReturnValue() {
		return returnValue;
	}

	public void setReturnValue(BigDecimal returnValue) {
		this.returnValue = returnValue;
	}

	public Set<EarthquakeMagnitudeProbabilityDTO> getMagnitudeProbabilityDTOs() {
		return magnitudeProbabilityDTOs;
	}

	public void setMagnitudeProbabilityDTOs(
			Set<EarthquakeMagnitudeProbabilityDTO> magnitudeProbabilityDTOs) {
		this.magnitudeProbabilityDTOs = magnitudeProbabilityDTOs;
	}
	
	public void addMagnitudeProbabilityDTO(EarthquakeMagnitudeProbabilityDTO dto) {
		magnitudeProbabilityDTOs.add(dto);
	}
	
	@Override
	public String toString() {
		return "EarthquakeProfileDTO [name=" + super.getName() +
				", earthquakePercentG=" + percentG
				+ ", earthquakeReturnValue=" + returnValue
				+ ", earthquakeMagnitudeProbabilities="
				+ magnitudeProbabilityDTOs + "]";
	}
	
	public static List<EarthquakeMagnitudeProbabilityDTO> getMagnitudeProbabilitiesReverseOrder(EarthquakeProfileDTO dto) {
		List<EarthquakeMagnitudeProbabilityDTO> results = getMagnitudeProbabilitiesInOrder(dto);
		Collections.reverse(results);
		return results;
	}
	
	public static List<EarthquakeMagnitudeProbabilityDTO> getMagnitudeProbabilitiesInOrder(EarthquakeProfileDTO dto) {
		List<EarthquakeMagnitudeProbabilityDTO> results = new ArrayList<EarthquakeMagnitudeProbabilityDTO>();
		if (dto != null && dto.getMagnitudeProbabilityDTOs() != null) {
			results.addAll(dto.getMagnitudeProbabilityDTOs());
			Collections.sort(results);
		}
		return results;
	}

	public static List<ProfileDTO> narrowListToProfile(List<EarthquakeProfileDTO> earthquakeProfileDTOs) {
		List<ProfileDTO> narrowList = new ArrayList<ProfileDTO>();
		for (EarthquakeProfileDTO profile : earthquakeProfileDTOs) {
			narrowList.add(new ProfileDTO(profile.getId(), profile.getName(), profile.getAnalysisDefault()));
		}
		return narrowList;
	}
}
