/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.shared;


import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.parre.shared.types.ThreatCategoryType;

/**
 * User: Swarn S. Dhaliwal
 * Date: 7/14/11
 * Time: 11:20 AM
*/
public class ThreatDTO extends NameDescriptionDTO {
    private String hazardType = "";
    private ThreatCategoryType threatCategory = ThreatCategoryType.NONE;
    private Boolean active = Boolean.TRUE;
    private String inactiveReason = "";
    private Boolean userDefined = Boolean.FALSE;
    private Long baselineId;
    private List<NoteDTO> noteDTOs = new ArrayList<NoteDTO>();
    private BigDecimal detectionLikelihood;

    public String getHazardType() {
        return hazardType;
    }

    public void setHazardType(String hazardType) {
        this.hazardType = hazardType;
    }

    public ThreatCategoryType getThreatCategory() {
        return threatCategory;
    }

    public void setThreatCategory(ThreatCategoryType threatCategory) {
        this.threatCategory = threatCategory;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public String getInactiveReason() {
        return inactiveReason;
    }

    public void setInactiveReason(String inactiveReason) {
        this.inactiveReason = inactiveReason;
    }


    public Boolean getUserDefined() {
        return userDefined;
    }

    public void setUserDefined(Boolean userDefined) {
        this.userDefined = userDefined;
    }


    @Override
    public String toString() {
        return "ThreatDTO{" +
                "hazardType='" + hazardType + '\'' +
                ", threatCategory=" + threatCategory +
                ", active=" + active +
                ", inactiveReason='" + inactiveReason + '\'' +
                ", userDefined=" + userDefined +
                ", baselineId=" + baselineId +
                '}';
    }

    public Long getBaselineId() {
        return baselineId;
    }

    public void setBaselineId(Long baselineId) {
        this.baselineId = baselineId;
    }

    public List<NoteDTO> getNoteDTOs() {
        return noteDTOs;
    }

    public void setNoteDTOs(List<NoteDTO> noteDTOs) {
        this.noteDTOs = noteDTOs;
    }

    public BigDecimal getDetectionLikelihood() {
        if(detectionLikelihood == null) {
            return BigDecimal.ZERO;
        }
        return detectionLikelihood;
    }

    public void setDetectionLikelihood(BigDecimal detectionLikelihood) {
        this.detectionLikelihood = detectionLikelihood;
    }
}
