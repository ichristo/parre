/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.shared;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The Class RiskBenefitDTO.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 2/16/12
 */
public class RiskBenefitDTO extends AssetThreatDTO {
    private AmountDTO baselineAmountDTO;
    private List<RiskBenefitOptionDTO> optionDTOs = Collections.emptyList();
    private List<BigDecimal> optionDeltaList = new ArrayList<java.math.BigDecimal>();

    public RiskBenefitDTO() {
    }

    public AmountDTO getBaselineAmountDTO() {
        return baselineAmountDTO;
    }

    public void setBaselineAmountDTO(AmountDTO baselineAmountDTO) {
        this.baselineAmountDTO = baselineAmountDTO;
    }

    public List<RiskBenefitOptionDTO> getOptionDTOs() {
        return optionDTOs;
    }

    public void setOptionDTOs(List<RiskBenefitOptionDTO> optionDTOs) {
        this.optionDTOs = optionDTOs;
    }

    public BigDecimal getBaselineQuantity() {
    	return baselineAmountDTO != null ? baselineAmountDTO.getQuantity() : BigDecimal.ZERO;
    }

    public BigDecimal getOptionQuantity(int optionNumber) {
        try {
        	if (optionDTOs != null && optionDTOs.size() > optionNumber && optionDTOs.get(optionNumber).getOptionAmountDTO() != null) {
        		return optionDTOs.get(optionNumber).getOptionAmountDTO().getQuantity();
        	}
        	
        } catch(Exception e) {}
        return BigDecimal.ZERO;
    }

    public BigDecimal getOptionDelta(int optionNumber) {
        return optionDeltaList.get(optionNumber);
    }

    public void populateOptionDeltas() {
        int numOptions = optionDTOs.size();
        for (int i = 0; i < numOptions; i++) {
            //TODO Make sure this is correct
            //BigDecimal quantity = baselineAmountDTO.getQuantity();
            BigDecimal quantity = getBaselineQuantity();
            if(quantity == null) {
                quantity = BigDecimal.ZERO;
            }
            BigDecimal optionQuantity = getOptionQuantity(i);
            if(optionQuantity == null) {
                optionQuantity = BigDecimal.ZERO;
            }

            optionDeltaList.add(quantity.subtract(optionQuantity));
        }
    }

    public void addRiskBenefitOptionDTO(RiskBenefitOptionDTO dto) {
        optionDTOs = add(dto, optionDTOs);
        dto.setRiskBenefitId(getId());
    }

    public void sort() {
        Collections.sort(optionDTOs);
    }

    public void updateId() {
        setId(getAssetThreatId());
    }
}
