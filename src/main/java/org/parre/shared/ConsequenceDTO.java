/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.shared;


import java.math.BigDecimal;

/**
 * User: Swarn S. Dhaliwal
 * Date: 7/14/11
 * Time: 11:20 AM
*/
public class ConsequenceDTO extends AssetThreatDTO implements HasFinancialTotal {
    private Integer fatalities = Integer.valueOf(0);
    private Integer seriousInjuries = Integer.valueOf(0);
    private AmountDTO financialImpactDTO = AmountDTO.createZero();
    private AmountDTO economicImpactDTO = AmountDTO.createZero();
    private AmountDTO financialTotalDTO = AmountDTO.createZero();
    private BigDecimal durationDays = BigDecimal.ZERO;
    private BigDecimal severityMgd = BigDecimal.ZERO;
    
    public ConsequenceDTO() {
    }

    public ConsequenceDTO(AssetDTO assetDTO, ThreatDTO threatDTO) {
        super(assetDTO, threatDTO);
    }


    public Integer getFatalities() {
    	return fatalities;
    }
    
    public void setFatalities(Integer fatalities) {
    	this.fatalities = fatalities;
    }

    public void setSeriousInjuries(Integer seriousInjuries) {
    	this.seriousInjuries = seriousInjuries;
    }
    
    public Integer getSeriousInjuries() {
    	return seriousInjuries;
    }
    
    public void setFinancialImpactDTO(AmountDTO financialImpactDTO) {
    	this.financialImpactDTO = financialImpactDTO;
    }
    
    public AmountDTO getFinancialImpactDTO() {
    	return financialImpactDTO;
    }
    
    public void setEconomicImpactDTO(AmountDTO economicImpactDTO) {
    	this.economicImpactDTO = economicImpactDTO;
    }

    public AmountDTO getEconomicImpactDTO() {
    	return economicImpactDTO;
    }


    public void setFinancialTotalDTO(AmountDTO financialTotalDTO) {
        this.financialTotalDTO = financialTotalDTO;
    }

    public AmountDTO getFinancialTotalDTO() {
        return financialTotalDTO;
    }


    @Override
    public String toString() {
        return "ConsequenceDTO{" +
                "fatalities=" + fatalities +
                ", seriousInjuries=" + seriousInjuries +
                ", financialImpactDTO=" + financialImpactDTO +
                ", economicImpactDTO=" + economicImpactDTO +
                ", financialTotalDTO=" + financialTotalDTO +
                ", durationDays=" + durationDays +
                ", severityMgd=" + severityMgd +
                '}';
    }

    public BigDecimal getDurationDays() {
        return durationDays;
    }

    public void setDurationDays(BigDecimal durationDays) {
        this.durationDays = durationDays;
    }

    public BigDecimal getSeverityMgd() {
        return severityMgd;
    }

    public void setSeverityMgd(BigDecimal severityMgd) {
        this.severityMgd = severityMgd;
    }
}
