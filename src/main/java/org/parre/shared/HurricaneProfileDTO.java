/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.shared;

import java.math.BigDecimal;

/**
 * The Class HurricaneProfileDTO.
 */
public class HurricaneProfileDTO extends ProfileDTO {
	private static final long serialVersionUID = 8615325655178972612L;
	private BigDecimal defaultDesignSpeed;
	private BigDecimal cat0ReturnPeriod = BigDecimal.ZERO;
	private BigDecimal cat1ReturnPeriod = BigDecimal.ZERO;
	private BigDecimal cat2ReturnPeriod = BigDecimal.ZERO;
	private BigDecimal cat3ReturnPeriod = BigDecimal.ZERO;
	private BigDecimal cat4ReturnPeriod = BigDecimal.ZERO;
	private BigDecimal cat5ReturnPeriod = BigDecimal.ZERO;
	
	public BigDecimal getDefaultDesignSpeed() {
		return defaultDesignSpeed;
	}
	public void setDefaultDesignSpeed(BigDecimal defaultDesignSpeed) {
		this.defaultDesignSpeed = defaultDesignSpeed;
	}
	public BigDecimal getCat0ReturnPeriod() {
		return cat0ReturnPeriod;
	}
	public void setCat0ReturnPeriod(BigDecimal cat0ReturnPeriod) {
		this.cat0ReturnPeriod = cat0ReturnPeriod;
	}
	public BigDecimal getCat1ReturnPeriod() {
		return cat1ReturnPeriod;
	}
	public void setCat1ReturnPeriod(BigDecimal cat1ReturnPeriod) {
		this.cat1ReturnPeriod = cat1ReturnPeriod;
	}
	public BigDecimal getCat2ReturnPeriod() {
		return cat2ReturnPeriod;
	}
	public void setCat2ReturnPeriod(BigDecimal cat2ReturnPeriod) {
		this.cat2ReturnPeriod = cat2ReturnPeriod;
	}
	public BigDecimal getCat3ReturnPeriod() {
		return cat3ReturnPeriod;
	}
	public void setCat3ReturnPeriod(BigDecimal cat3ReturnPeriod) {
		this.cat3ReturnPeriod = cat3ReturnPeriod;
	}
	public BigDecimal getCat4ReturnPeriod() {
		return cat4ReturnPeriod;
	}
	public void setCat4ReturnPeriod(BigDecimal cat4ReturnPeriod) {
		this.cat4ReturnPeriod = cat4ReturnPeriod;
	}
	public BigDecimal getCat5ReturnPeriod() {
		return cat5ReturnPeriod;
	}
	public void setCat5ReturnPeriod(BigDecimal cat5ReturnPeriod) {
		this.cat5ReturnPeriod = cat5ReturnPeriod;
	}
	public String toString() {
		return "HurricaneProfileDTO [defaultDesignSpeed=" + defaultDesignSpeed
				+ ", cat0ReturnPeriod=" + cat0ReturnPeriod
				+ ", cat1ReturnPeriod=" + cat1ReturnPeriod
				+ ", cat2ReturnPeriod=" + cat2ReturnPeriod
				+ ", cat3ReturnPeriod=" + cat3ReturnPeriod
				+ ", cat4ReturnPeriod=" + cat4ReturnPeriod
				+ ", cat5ReturnPeriod=" + cat5ReturnPeriod
				+ "]";
	}
}
