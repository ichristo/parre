/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.shared;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.user.client.rpc.IsSerializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.parre.shared.types.ParreAnalysisStatusType;

/**
 * The Class AppInitData.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 10/6/11
 */
public class AppInitData implements IsSerializable {
	private boolean reportSubSystemAvailable = false;
	private List<ThreatDTO> consequenceAnalysisThreats;
    private StatValuesDTO statisticalValuesDTO;
    private ParreAnalysisDTO parreAnalysisDTO;
    private List<ParreAnalysisDTO> activeParreAnalyses = Collections.emptyList();
    private UserPreferencesDTO userPreferencesDTO;
    private List<LabelValueDTO> stateList = Collections.emptyList();
    private List<ReportSectionLookupDTO> reportSectionDTOs = Collections.emptyList();

    public List<ThreatDTO> getConsequenceAnalysisThreats() {
        return consequenceAnalysisThreats;
    }

    public void setConsequenceAnalysisThreats(List<ThreatDTO> consequenceAnalysisThreats) {
        this.consequenceAnalysisThreats = consequenceAnalysisThreats;
    }

    public StatValuesDTO getStatisticalValuesDTO() {
        return statisticalValuesDTO;
    }

    public void setStatisticalValuesDTO(StatValuesDTO statisticalValuesDTO) {
        this.statisticalValuesDTO = statisticalValuesDTO;
    }

    public void sortParreAnalysisDTOs() {
        Collections.sort(activeParreAnalyses);
    }

    public void setParreAnalysisDTO(ParreAnalysisDTO parreAnalysisDTO) {
        this.parreAnalysisDTO = parreAnalysisDTO;
    }

    public ParreAnalysisDTO getParreAnalysisDTO() {
        return parreAnalysisDTO;
    }

    public void setActiveParreAnalyses(List<ParreAnalysisDTO> activeParreAnalyses) {
        this.activeParreAnalyses = activeParreAnalyses;
    }

    public List<ParreAnalysisDTO> getActiveParreAnalyses() {
        return activeParreAnalyses;
    }

    public void addActiveParreAnalysis(ParreAnalysisDTO dto) {
        if (activeParreAnalyses.isEmpty()) {
            activeParreAnalyses = new ArrayList<ParreAnalysisDTO>();
        }
        addActiveParreAnalysis(dto);
    }

    public boolean removeActiveParreAnalysis(ParreAnalysisDTO dto) {
        return activeParreAnalyses.remove(dto);
    }

    public boolean hasBaseline() {
        return parreAnalysisDTO != null && (parreAnalysisDTO.isOption() || parreAnalysisDTO.isBaseline());
    }
    
    public boolean isOption() {
        return parreAnalysisDTO != null && parreAnalysisDTO.isOption();
    }
    
    public Integer getDisplayOrder() {
        return parreAnalysisDTO.getDisplayOrder();
    }
    
    public boolean hasOptionAnalyses() {
        return parreAnalysisDTO.hasOptionAnalyses();
    }

    public boolean isLocked() {
        return parreAnalysisDTO != null && parreAnalysisDTO.isLocked();
    }

    public void lockCurrentAnalysis() {
        parreAnalysisDTO.setStatus(ParreAnalysisStatusType.LOCKED);
    }
    public void unlockCurrentAnalysis() {
        parreAnalysisDTO.setStatus(ParreAnalysisStatusType.ACTIVE);
    }

    public boolean hasParreAnalysis() {
        return parreAnalysisDTO != null;
    }

    public UserPreferencesDTO getUserPreferencesDTO() {
        return userPreferencesDTO;
    }

    public void setUserPreferencesDTO(UserPreferencesDTO userPreferencesDTO) {
        this.userPreferencesDTO = userPreferencesDTO;
    }

	public List<LabelValueDTO> getStateList() {
		return stateList;
	}

	public void setStateList(List<LabelValueDTO> stateList) {
		this.stateList = stateList;
	}

	public List<ReportSectionLookupDTO> getReportSectionDTOs() {
		return reportSectionDTOs;
	}

	public void setReportSectionDTOs(List<ReportSectionLookupDTO> reportSectionDTOs) {
		this.reportSectionDTOs = reportSectionDTOs;
	}

	public boolean isReportSubSystemAvailable() {
		return reportSubSystemAvailable;
	}

	public void setReportSubSystemAvailable(boolean reportSubSystemAvailable) {
		this.reportSubSystemAvailable = reportSubSystemAvailable;
	}
}
