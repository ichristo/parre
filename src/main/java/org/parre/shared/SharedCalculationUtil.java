/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.shared;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;

import org.parre.shared.types.MoneyUnitType;


/**
 * The Class SharedCalculationUtil.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 2/27/12
 */
public class SharedCalculationUtil implements Serializable {
    public static final int DIVISION_PRECISION = 21;
    public static final String ONE_MILLION = "1000000";
    
    public static void calculateFinancialTotal(NaturalThreatDTO dto, StatValueQuantities statValues) {
    	if (statValues != null) {
	    	BigDecimal financialTotalQuantity = calculateFinancialTotal(
	    			dto.getFatalities(), 
	    			dto.getSeriousInjuries(), 
	    			dto.getFinancialImpactDTO() != null ? dto.getFinancialImpactDTO().getQuantity() : BigDecimal.ZERO,
	    			statValues);
	    	
	    	dto.setFinancialTotalDTO(new AmountDTO(financialTotalQuantity, MoneyUnitType.DOLLAR));
    	} else if (dto.getFinancialImpactDTO() != null) {
    		dto.setFinancialTotalDTO(new AmountDTO(dto.getFinancialImpactDTO().getQuantity(), dto.getFinancialImpactDTO().getUnit()));
    	} else {
    		dto.setFinancialTotalDTO(AmountDTO.createZero());
    	}
    }

    public static BigDecimal calculateFinancialTotal(Integer fatalities, Integer seriousInjuries, BigDecimal financialImpactQuantity, StatValueQuantities statValueQuantities) {
    	
        try {
            BigDecimal fatalityStat = statValueQuantities.getFatalityQuantity();
            BigDecimal seriousInjuryValue = statValueQuantities.getSeriousInjuryQuantity();
            BigDecimal calculatedFatality;
            
            try {
            	BigDecimal fatalityStatDollars = fatalityStat.multiply(new BigDecimal(ONE_MILLION));
                calculatedFatality = fatalityStatDollars.multiply(new BigDecimal(fatalities));
            } catch (NullPointerException e) {
                calculatedFatality = BigDecimal.valueOf(0);
            }
            
            BigDecimal calculatedSI;
            try {
            	BigDecimal seriousInjuryDollars = seriousInjuryValue.multiply(new BigDecimal(ONE_MILLION));
                calculatedSI = seriousInjuryDollars.multiply(new BigDecimal(seriousInjuries));
            } catch (NullPointerException e) {
                calculatedSI = new BigDecimal(0);
            }
            BigDecimal financialTotal = calculatedFatality.add(calculatedSI);
            
            return financialTotal.add(financialImpactQuantity);
        } catch (Exception e) {
            return BigDecimal.ZERO;
        } 
    }

    public static BigDecimal scaleValue(BigDecimal value) {
        return value.setScale(DIVISION_PRECISION, BigDecimal.ROUND_HALF_EVEN);
    }

    public static BigDecimal divideSafely(BigDecimal numerator, BigDecimal denominator) {
        return divideSafely(numerator, denominator, DIVISION_PRECISION, RoundingMode.HALF_EVEN);
    }

    public static BigDecimal divideSafely(BigDecimal numerator, BigDecimal denominator, int precision, RoundingMode roundingMode) {
        try {
            return numerator.divide(denominator, precision, roundingMode);
        }
        catch (Exception e) {
            return BigDecimal.ZERO;
        }
    }

    public static int compare(BigDecimal val1, BigDecimal val2) {
        double dVal1 = val1 != null ? val1.doubleValue() : 0d;
        double dVal2 = val2 != null ? val2.doubleValue() : 0d;
        if (dVal1 == dVal2) {
            return 0;
        }
        else if (dVal1 < dVal2) {
            return -1;
        }
        else {
            return 1;
        }
    }
    
	public static AmountDTO calculateNaturalThreatTotalRisk(NaturalThreatDTO dto) {
		AmountDTO originalRisk = dto.getFinancialTotalDTO();
		BigDecimal adjustedRisk = originalRisk.getQuantity().multiply(dto.getLot());
		adjustedRisk = adjustedRisk.multiply(dto.getVulnerability());
		return new AmountDTO(adjustedRisk, originalRisk.getUnit());
	}
}
