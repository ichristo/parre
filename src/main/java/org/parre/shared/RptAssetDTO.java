/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.shared;

import java.io.Serializable;

/**
 * @author Raju Kanumuri
 * @date 10/28/2013
 */
public class RptAssetDTO extends BaseDTO implements Serializable{
  
	private static final long serialVersionUID = -3981393616095907461L;
	
	private String assetSystem;
    private String criticalAsset;
    
    public RptAssetDTO(String assetSystem, String criticalAsset){
		this.assetSystem=assetSystem;
		this.criticalAsset=criticalAsset;
	}
    
 	public RptAssetDTO() {	
	}
 	
	public String getAssetSystem() {
		return assetSystem;
	}
	public void setAssetSystem(String assetSystem) {
		this.assetSystem = assetSystem;
	}
	public String getCriticalAsset() {
		return criticalAsset;
	}
	public void setCriticalAsset(String criticalAsset) {
		this.criticalAsset = criticalAsset;
	}
	
   

   
}
