/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.shared;

import java.math.BigDecimal;
import java.util.*;

/**
 * The Class PathAnalysisDTO.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 1/12/12
 */
public class PathAnalysisDTO extends NameDescriptionDTO {
    private Long vulnerabilityId;
    private Integer responseTime;
    private List<AttackStepDTO> attackStepDTOs = Collections.emptyList();
    private List<ResponseStepDTO> responseStepDTOs = Collections.emptyList();
    private Set<CounterMeasureDTO> counterMeasureDTOs = Collections.emptySet();
    private BigDecimal probability;

    public PathAnalysisDTO() {
    }

    public PathAnalysisDTO(Long vulnerabilityId) {
        this.vulnerabilityId = vulnerabilityId;
    }

    public Long getVulnerabilityId() {
        return vulnerabilityId;
    }

    public void setVulnerabilityId(Long vulnerabilityId) {
        this.vulnerabilityId = vulnerabilityId;
    }

    public Integer getResponseTime() {
        return responseTime;
    }

    public void setResponseTime(Integer responseTime) {
        this.responseTime = responseTime;
    }

    public List<AttackStepDTO> getAttackStepDTOs() {
        return attackStepDTOs;
    }

    public void setAttackStepDTOs(List<AttackStepDTO> attackStepDTOs) {
        this.attackStepDTOs = attackStepDTOs;
    }

    public void updateStepDTO(AttackStepDTO stepDTO) {
        if (attackStepDTOs.isEmpty()) {
            attackStepDTOs = new LinkedList<AttackStepDTO>();
        }

        updateAnalysisStep(stepDTO, attackStepDTOs);
        updateAttackSteps();
    }

    public void updateStepDTO(ResponseStepDTO stepDTO) {
        if (responseStepDTOs.isEmpty()) {
            responseStepDTOs = new LinkedList<ResponseStepDTO>();
        }

        updateAnalysisStep(stepDTO, responseStepDTOs);
    }

    private <T> void updateAnalysisStep(T stepDTO, List<T> dtos) {
        int index = dtos.indexOf(stepDTO);
        if (index >= 0) {
            dtos.set(index, stepDTO);
        } else {
            dtos.add(stepDTO);
            ((AnalysisStep) stepDTO).setPathAnalysisId(getId());
        }
    }

    public List<ResponseStepDTO> getResponseStepDTOs() {
        return responseStepDTOs;
    }

    public void setResponseStepDTOs(List<ResponseStepDTO> responseStepDTOs) {
        this.responseStepDTOs = responseStepDTOs;
    }

    public void sortAttackStepDTOs() {
        Collections.sort(attackStepDTOs, AnalysisStep.COMPARATOR);
    }

    public void sortResponseStepDTOs() {
        Collections.sort(responseStepDTOs, AnalysisStep.COMPARATOR);
    }

    public void updateAttackSteps() {
        sortAttackStepDTOs();
        Boolean detected = Boolean.FALSE;
        for (AttackStepDTO attackStepDTO : getAttackStepDTOs()) {
            boolean isDetectionPoint = !detected && attackStepDTO.getDetected();
            if (isDetectionPoint) {
                detected = Boolean.TRUE;
                attackStepDTO.setDetectionPoint(Boolean.TRUE);
            }
            else if (detected) {
                attackStepDTO.setDetected(Boolean.TRUE);
                attackStepDTO.setDetectionPoint(Boolean.FALSE);
            }
        }
    }

    public Set<CounterMeasureDTO> getCounterMeasureDTOs() {
        return counterMeasureDTOs;
    }

    public void setCounterMeasureDTOs(Set<CounterMeasureDTO> counterMeasureDTOs) {
        this.counterMeasureDTOs = counterMeasureDTOs;
    }

    public void addCounterMeasure(CounterMeasureDTO counterMeasureDTO) {
        if(this.counterMeasureDTOs.isEmpty()) {
            this.counterMeasureDTOs = new HashSet<CounterMeasureDTO>();
        }
        this.counterMeasureDTOs.add(counterMeasureDTO);
    }

	public BigDecimal getProbability() {
		return probability;
	}

	public void setProbability(BigDecimal probability) {
		this.probability = probability;
	}
}
