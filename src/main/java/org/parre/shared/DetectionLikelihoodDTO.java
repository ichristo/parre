/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.shared;

import java.math.BigDecimal;

/**
 * User: Daniel
 * Date: 1/10/12
 * Time: 4:05 PM
*/
public class DetectionLikelihoodDTO extends NameDescriptionDTO {
    private BigDecimal likelihood;

    public DetectionLikelihoodDTO() {
    }

    public DetectionLikelihoodDTO(String name, String description, BigDecimal likelihood) {
        super(name, description);
        this.likelihood = likelihood;
    }

    public BigDecimal getLikelihood() {
        return likelihood;
    }

    public void setLikelihood(BigDecimal likelihood) {
        this.likelihood = likelihood;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append(super.toString());
        sb.append("DetectionLikelihoodDTO");
        sb.append("{likelihood=").append(likelihood);
        sb.append('}');
        return sb.toString();
    }
}
