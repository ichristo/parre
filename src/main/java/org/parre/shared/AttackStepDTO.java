/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.shared;


import java.math.BigDecimal;

import org.parre.shared.types.AnalysisPathStepType;

/**
 * The Class AttackStepDTO.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 1/26/12
 */
public class AttackStepDTO extends BaseDTO implements AnalysisStep {
    private Long pathAnalysisId;
    private AnalysisPathStepType type;
    private CounterMeasureDTO counterMeasureDTO;
    private BigDecimal travelTime = BigDecimal.ZERO;
    private BigDecimal overcomeTime;
    private Integer stepNumber;
    private Boolean detected = false;
    private Boolean detectionPoint = false;

    public AttackStepDTO() {
    }

    public AttackStepDTO(Long pathAnalysisId) {
        this.pathAnalysisId = pathAnalysisId;
    }

    public Long getPathAnalysisId() {
        return pathAnalysisId;
    }

    public void setPathAnalysisId(Long pathAnalysisId) {
        this.pathAnalysisId = pathAnalysisId;
    }

    public AnalysisPathStepType getType() {
        return type;
    }

    public void setType(AnalysisPathStepType type) {
        this.type = type;
    }

    public BigDecimal getTravelTime() {
        return travelTime;
    }

    public void setTravelTime(BigDecimal travelTime) {
        this.travelTime = travelTime;
    }

    public BigDecimal getOvercomeTime() {
        return overcomeTime;
    }

    public void setOvercomeTime(BigDecimal overcomeTime) {
        this.overcomeTime = overcomeTime;
    }

    public BigDecimal getTotalTime() {
        return travelTime.add(overcomeTime);
    }

    public Integer getStepNumber() {
        return stepNumber;
    }

    public void setStepNumber(Integer stepNumber) {
        this.stepNumber = stepNumber;
    }

    public CounterMeasureDTO getCounterMeasureDTO() {
        return counterMeasureDTO;
    }

    public void setCounterMeasureDTO(CounterMeasureDTO counterMeasureDTO) {
        this.counterMeasureDTO = counterMeasureDTO;
    }

    public Boolean getDetected() {
        return detected;
    }

    public void setDetected(Boolean detected) {
        this.detected = detected;
    }

    public Boolean getDetectionPoint() {
        return detectionPoint;
    }

    public void setDetectionPoint(Boolean detectionPoint) {
        this.detectionPoint = detectionPoint;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("AttackStepDTO");
        sb.append("{pathAnalysisId=").append(pathAnalysisId);
        sb.append(", type=").append(type);
        sb.append(", counterMeasureDTO=").append(counterMeasureDTO);
        sb.append(", travelTime=").append(travelTime);
        sb.append(", overcomeTime=").append(overcomeTime);
        sb.append(", stepNumber=").append(stepNumber);
        sb.append(", detected=").append(detected);
        sb.append('}');
        return sb.toString();
    }
}
