/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.shared;


import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;

import org.parre.shared.types.MoneyUnitType;

/**
 * The Class RiskBenefitAnalysisDTO.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 2/15/12
 */
public class RiskBenefitAnalysisDTO extends AnalysisDTO {
    private List<ParreAnalysisDTO> optionAnalysisDTOs = Collections.emptyList();
    private List<RiskBenefitDTO> riskBenefitDTOs = Collections.emptyList();
    private List<BenefitCostDTO> benefitCostDTOs = Collections.emptyList();
    private AmountDTO budgetDTO;

    public RiskBenefitAnalysisDTO() {
    }

    public RiskBenefitAnalysisDTO(NameDescriptionDTO dto) {
        super(dto);
        budgetDTO = new AmountDTO(BigDecimal.ZERO, MoneyUnitType.MILLION);
    }

    public List<RiskBenefitDTO> getRiskBenefitDTOs() {
        return riskBenefitDTOs;
    }

    public void setRiskBenefitDTOs(List<RiskBenefitDTO> riskBenefitDTOs) {
        this.riskBenefitDTOs = riskBenefitDTOs;
    }

    public AmountDTO getBudgetDTO() {
        return budgetDTO;
    }

    public void setBudgetDTO(AmountDTO budgetDTO) {
        this.budgetDTO = budgetDTO;
    }

    public void addRiskBenefitDTO(RiskBenefitDTO dto) {
        riskBenefitDTOs = add(dto, riskBenefitDTOs);
    }

    public List<ParreAnalysisDTO> getOptionAnalysisDTOs() {
        return optionAnalysisDTOs;
    }

    public void setOptionAnalysisDTOs(List<ParreAnalysisDTO> optionAnalysisDTOs) {
        this.optionAnalysisDTOs = optionAnalysisDTOs;
    }

    public List<BenefitCostDTO> getBenefitCostDTOs() {
        return benefitCostDTOs;
    }

    public void setBenefitCostDTOs(List<BenefitCostDTO> benefitCostDTOs) {
        this.benefitCostDTOs = benefitCostDTOs;
    }

    public boolean hasOptions() {
        return !optionAnalysisDTOs.isEmpty();
    }
}
