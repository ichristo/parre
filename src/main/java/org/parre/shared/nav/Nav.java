/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.shared.nav;

import com.google.gwt.user.client.rpc.IsSerializable;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * User: Swarn S. Dhaliwal
 * Date: 7/30/11
 * Time: 2:38 PM
*/
public class Nav implements IsSerializable {
    private StartStep startStep;
    private EndStep endStep;
    private Map<String, NavStep> navSteps = new HashMap<String, NavStep>();

    public StartStep getStartStep() {
        return startStep;
    }

    public void setStartStep(StartStep startStep) {
        this.startStep = startStep;
    }

    public EndStep getEndStep() {
        return endStep;
    }

    public void setEndStep(EndStep endStep) {
        this.endStep = endStep;
    }

    public Collection<NavStep> getNavSteps() {
        return navSteps.values();
    }

    public void addNavStep(NavStep navStep) {
        if (navSteps.containsKey(navStep.getName())) {
            throw new IllegalArgumentException("Duplicate NavStep : " + navStep.getName());
        }
        if (navStep.isStartStep()) {
            startStep = (StartStep) navStep;
        }
        if (navStep.isEndStep()) {
            endStep = (EndStep) navStep;
        }
        navSteps.put(navStep.getName(), navStep);
    }

    public NavStep getNavStep(String stepName) {
        return navSteps.get(stepName);
    }

    @Override
    public String toString() {
        return "Nav{" +
                "startStep=" + startStep +
                ", endStep=" + endStep +
                ", navSteps=" + navSteps +
                '}';
    }
}
