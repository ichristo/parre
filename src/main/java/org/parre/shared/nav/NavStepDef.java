/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.shared.nav;

import java.util.Collections;
import java.util.List;

/**
 * User: Swarn S. Dhaliwal
 * Date: 7/30/11
 * Time: 4:40 PM
*/
public class NavStepDef {
    public static final String START_STEP_NAME = "START";
    public static final String END_STEP_NAME = "END";
    private String name;
    private String label;
    private List<String> nextSteps;


    public NavStepDef(String name, String label, List<String> nextSteps) {
        this.name = name;
        this.label = label;
        this.nextSteps = Collections.unmodifiableList(nextSteps);
    }

    public boolean isStartStep() {
        return START_STEP_NAME.equals(name);
    }

    public boolean isEndStep() {
        return END_STEP_NAME.equals(name);
    }

    public String getName() {
        return name;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public List<String> getNextSteps() {
        return nextSteps;
    }

    @Override
    public String toString() {
        return "NavStepDef{" +
                "name='" + name + '\'' +
                ", label='" + label + '\'' +
                ", nextSteps=" + nextSteps +
                '}';
    }

    public NavStep createNavStep() {
        return new NavStep(this);
    }
}
