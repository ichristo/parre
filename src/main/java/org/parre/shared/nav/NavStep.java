/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.shared.nav;

import com.google.gwt.user.client.rpc.IsSerializable;

import java.util.Collections;
import java.util.List;

/**
 * User: Swarn S. Dhaliwal
 * Date: 7/30/11
 * Time: 2:16 PM
*/
public class NavStep implements IsSerializable {
    private String name;
    private String label;
    private List<NavStep> nextSteps = Collections.emptyList();
    private boolean currentStep;
    private boolean complete;

    public NavStep() {

    }

    public NavStep(NavStepDef def) {
        this.name = def.getName();
        this.label = def.getLabel();
    }

    public String getName() {
        return name;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public boolean isCurrentStep() {
        return currentStep;
    }

    public void setCurrentStep(boolean currentStep) {
        this.currentStep = currentStep;
    }

    public boolean isComplete() {
        return complete;
    }

    public void setComplete(boolean complete) {
        this.complete = complete;
    }

    public List<NavStep> getNextSteps() {
        return nextSteps;
    }

    public void setNextSteps(List<NavStep> nextSteps) {
        this.nextSteps = nextSteps;
    }

    public boolean isStartStep() {
        return false;
    }

    public boolean isEndStep() {
        return false;
    }

    @Override
    public String toString() {
        String message = "NavStep{" +
                "name='" + name + '\'' +
                ", label='" + label + '\'' +
                ", nextSteps=";
        for (NavStep nextStep : nextSteps) {
            message += nextStep.getName() + ", ";
        }
        message += "currentStep=" + currentStep +
                ", complete=" + complete +
                '}';
        return message;
    }
}
