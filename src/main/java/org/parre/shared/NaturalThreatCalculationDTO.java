/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.shared;

import java.io.Serializable;

/**
 * The Class NaturalThreatCalculationDTO.
 */
public class NaturalThreatCalculationDTO implements Serializable {
	private static final long serialVersionUID = 1952437308195515760L;
	
	private Boolean calculationSuccess = Boolean.TRUE;
	private String resultMessage;
	
	private PhysicalResourceDTO asset;
	private NaturalThreatDTO analysis;
	
	private EarthquakeProfileDTO earthquakeProfile;
	private HurricaneProfileDTO hurricaneProfile;
	
	public NaturalThreatCalculationDTO() {
	}

	public NaturalThreatCalculationDTO(PhysicalResourceDTO asset,
			NaturalThreatDTO analysis) {
		super();
		this.asset = asset;
		this.analysis = analysis;
	}
	
	public NaturalThreatCalculationDTO(PhysicalResourceDTO asset,
			NaturalThreatDTO analysis, EarthquakeProfileDTO earthquakeProfile) {
		super();
		this.asset = asset;
		this.analysis = analysis;
		this.earthquakeProfile = earthquakeProfile;
	}
	
	public NaturalThreatCalculationDTO(PhysicalResourceDTO asset,
			NaturalThreatDTO analysis, HurricaneProfileDTO hurricaneProfile) {
		super();
		this.asset = asset;
		this.analysis = analysis;
		this.hurricaneProfile = hurricaneProfile;
	}
	
	public NaturalThreatCalculationDTO(PhysicalResourceDTO asset, 
			NaturalThreatDTO analysis, IceStormWorkDTO iceStormWorkDTO) {
		super();
		this.asset = asset;
		this.analysis = analysis;
		this.analysis.setIceStormWorkDTO(iceStormWorkDTO);
	}

	public PhysicalResourceDTO getAsset() {
		return asset;
	}

	public void setAsset(PhysicalResourceDTO asset) {
		this.asset = asset;
	}

	public NaturalThreatDTO getAnalysis() {
		return analysis;
	}

	public void setAnalysis(NaturalThreatDTO analysis) {
		this.analysis = analysis;
	}
	
	public Boolean getCalculationSuccess() {
		return calculationSuccess;
	}

	public void setCalculationSuccess(Boolean calculationSuccess) {
		this.calculationSuccess = calculationSuccess;
	}

	public String getResultMessage() {
		return resultMessage;
	}

	public void setResultMessage(String resultMessage) {
		this.resultMessage = resultMessage;
	}
	
	public EarthquakeProfileDTO getEarthquakeProfile() {
		return earthquakeProfile;
	}

	public void setEarthquakeProfile(EarthquakeProfileDTO earthquakeProfile) {
		this.earthquakeProfile = earthquakeProfile;
	}

	public HurricaneProfileDTO getHurricaneProfile() {
		return hurricaneProfile;
	}

	public void setHurricaneProfile(HurricaneProfileDTO hurricaneProfile) {
		this.hurricaneProfile = hurricaneProfile;
	}

	public IceStormWorkDTO getIceStormWorkDTO() {
		return this.analysis.getIceStormWorkDTO();
	}

	public void setIceStormWorkDTO(IceStormWorkDTO iceStormWorkDTO) {
		this.analysis.setIceStormWorkDTO(iceStormWorkDTO);
	}

	@Override
	public String toString() {
		return "NaturalThreatCalculationDTO [calculationSuccess="
				+ calculationSuccess + ", resultMessage=" + resultMessage
				+ ", asset=" + asset + ", analysis=" + analysis
				+ ", earthquakeProfile=" + earthquakeProfile
				+ ", hurricaneProfile=" + hurricaneProfile + "]";
	}
}
