/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.shared;

import java.math.BigDecimal;

import org.parre.server.domain.Threat;


/**
 * The Class RiskResilienceDTO.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 2/16/12
 */
public class RiskResilienceDTO extends AssetThreatDTO {
    private BigDecimal vulnerability = BigDecimal.ZERO;
    private BigDecimal lot = BigDecimal.ZERO;
    private AmountDTO financialTotal = AmountDTO.createZero();
    private BigDecimal risk = BigDecimal.ZERO;
    private AmountDTO economicResilience = AmountDTO.createZero();
    private AmountDTO ownerResilience = null;
    private UnitPriceDTO unitPriceDTO;
    private BigDecimal durationDays = BigDecimal.ZERO;
    private BigDecimal severityMgd = BigDecimal.ZERO;
    private BigDecimal fatalities = BigDecimal.ZERO;
    private BigDecimal seriousInjuries = BigDecimal.ZERO;
    private BigDecimal ownersFinancialImpact;


    public BigDecimal getDurationDays() {
        if(durationDays == null) {
            durationDays = BigDecimal.ZERO;
        }
        return durationDays;
    }

    public void setDurationDays(BigDecimal durationDays) {
        this.durationDays = durationDays;
    }

    public BigDecimal getSeverityMgd() {
        return severityMgd;
    }

    public void setSeverityMgd(BigDecimal severityMgd) {
        this.severityMgd = severityMgd;
    }

    public BigDecimal getVulnerability() {
        return vulnerability;
    }

    public void setVulnerability(BigDecimal vulnerability) {
        this.vulnerability = vulnerability;
    }

    public BigDecimal getLot() {
        return lot;
    }

    public void setLot(BigDecimal lot) {
        this.lot = lot;
    }

    public AmountDTO getEconomicResilience() {
        return economicResilience;
    }

    public void setEconomicResilience(AmountDTO economicResilience) {
        this.economicResilience = economicResilience;
    }

    public BigDecimal getEconomicResilienceQuantity() {
        return economicResilience.getQuantity();
    }

    public AmountDTO getOwnerResilience() {
        if (ownerResilience == null) {
            calculateOwnerResilience();
        }
        return ownerResilience;
    }

    public void calculateOwnerResilience() {
        BigDecimal numerator = durationDays.multiply(severityMgd)
                .multiply(unitPriceDTO.getPrice()).multiply(vulnerability).multiply(lot);

        BigDecimal divisor = new BigDecimal(unitPriceDTO.getQuantity());
        BigDecimal financialImpact = SharedCalculationUtil.divideSafely(numerator, divisor);

        ownerResilience = new AmountDTO(financialImpact, economicResilience.getUnit());
    }

    public BigDecimal getRisk() {
        return risk;
    }

    public void calculateRisk() {
        risk = SharedCalculationUtil.scaleValue
                (getFinancialTotalQuantity().multiply(vulnerability).multiply(lot));
    }

    public void setRisk(BigDecimal risk) {
        this.risk = risk;
    }

    public BigDecimal getOwnerResilienceQuantity() {
        return getOwnerResilience().getQuantity();
    }

    public AmountDTO getFinancialTotal() {
        return financialTotal;
    }

    public void setFinancialTotal(AmountDTO financialTotal) {
        this.financialTotal = financialTotal;
    }

    public BigDecimal getFinancialTotalQuantity() {
        return financialTotal.getQuantity();
    }

    public void setUnitPriceDTO(UnitPriceDTO unitPriceDTO) {
        this.unitPriceDTO = unitPriceDTO;
    }

    public UnitPriceDTO getUnitPriceDTO() {
        return unitPriceDTO;
    }

    @Override
    public String toString() {
        return "RiskResilienceDTO{" +
        		"threat name: " + getThreatName() +
                ", vulnerability=" + vulnerability +
                ", lot=" + lot +
                ", financialTotal=" + financialTotal +
                ", risk=" + risk +
                ", economicResilience=" + economicResilience +
                ", ownerResilience=" + ownerResilience +
                ", unitPriceDTO=" + unitPriceDTO +
                ", durationDays=" + durationDays +
                ", severityMgd=" + severityMgd +
                ", fatalities=" + fatalities +
                ", seriousInjuries=" + seriousInjuries +
                ", ownersFinancialImpact=" + ownersFinancialImpact +
                '}';
    }

    public BigDecimal getFatalities() {
        return fatalities;
    }

    public void setFatalities(BigDecimal fatalities) {
        this.fatalities = fatalities != null ? fatalities : BigDecimal.ZERO;
    }

    public BigDecimal getSeriousInjuries() {
        return seriousInjuries;
    }

    public void setSeriousInjuries(BigDecimal seriousInjuries) {
        this.seriousInjuries = seriousInjuries != null ? seriousInjuries : BigDecimal.ZERO;
    }

    public BigDecimal getOwnersFinancialImpact() {
        return ownersFinancialImpact != null ? ownersFinancialImpact : BigDecimal.ZERO;
    }

    public void setOwnersFinancialImpact(BigDecimal ownersFinancialImpact) {
        this.ownersFinancialImpact = ownersFinancialImpact;
    }
}
