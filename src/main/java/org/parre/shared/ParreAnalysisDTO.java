/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.shared;


import java.math.BigDecimal;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.parre.shared.types.ParreAnalysisStatusType;


/**
 * The Class ParreAnalysisDTO.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 11/23/11
 */
public class ParreAnalysisDTO extends NameDescriptionDTO {
    private Long baselineId;
    private ParreAnalysisStatusType status;
    private Integer assetThreshold = 0;
    private Integer assetThreatThreshold = 0;
    private NameDescriptionDTO contactDTO;
    private NameDescriptionDTO siteLocationDTO;
    private String orgName;
    private Boolean useStatValues = false;
    private StatValuesDTO statValuesDTO = StatValuesDTO.createEmptyStatValues();
    private Boolean overrideFatality = false;
    private Boolean overrideSeriousInjury = false;
    private Date createdTime;
    private Integer displayOrder = 0;
    
    private Boolean useDefaultLocation = false;
    private String defaultPostalCode;
    private BigDecimal defaultLatitude;
    private BigDecimal defaultLongitude;

    private List<ParreAnalysisDTO> optionParreAnalysisDTOs = Collections.emptyList();
    private List<ProposedMeasureDTO> proposedMeasureDTOs = Collections.emptyList();
    
    private EarthquakeProfileDTO defaultEarthquakeProfileDTO;
    private HurricaneProfileDTO defaultHurricaneProfileDTO;
    
    private List<EarthquakeProfileDTO> earthquakeProfileDTOs = Collections.emptyList();
    private List<HurricaneProfileDTO> hurricaneProfileDTOs = Collections.emptyList();

    @Override
    public int compareTo(NameDescriptionDTO o) {
        if (!displayOrder.equals(((ParreAnalysisDTO)o).displayOrder)) {
            return displayOrder.compareTo(((ParreAnalysisDTO)o).displayOrder);
        }
        else {
            return super.compareTo(o);
        }
    }

    public boolean isBaseline() {
        return isLocked() && baselineId == null;
    }

    public boolean isOption() {
        return isLocked() && baselineId != null;
    }

    public Long getBaselineId() {
        if(baselineId == null) {
            return getId();
        }
        return baselineId;
    }

    public void setBaselineId(Long baselineId) {
        this.baselineId = baselineId;
    }

    public ParreAnalysisStatusType getStatus() {
        return status;
    }

    public void setStatus(ParreAnalysisStatusType status) {
        this.status = status;
    }

    public Integer getAssetThreshold() {
        return assetThreshold;
    }

    public void setAssetThreshold(Integer assetThreshold) {
        this.assetThreshold = assetThreshold;
    }

    public Integer getAssetThreatThreshold() {
        return assetThreatThreshold;
    }

    public void setAssetThreatThreshold(Integer assetThreatThreshold) {
        this.assetThreatThreshold = assetThreatThreshold;
    }

    public NameDescriptionDTO getContactDTO() {
        return contactDTO;
    }

    public void setContactDTO(NameDescriptionDTO contactDTO) {
        this.contactDTO = contactDTO;
    }

    public String getOrgName() {
        return orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    public Boolean getUseStatValues() {
        return useStatValues;
    }

    public void setUseStatValues(Boolean useStatValues) {
        this.useStatValues = useStatValues;
    }

    public StatValuesDTO getStatValuesDTO() {
        return statValuesDTO;
     }

    public void setStatValuesDTO(StatValuesDTO statValuesDTO) {
        this.statValuesDTO = statValuesDTO != null ? statValuesDTO : StatValuesDTO.createEmptyStatValues();
    }

    public Boolean getOverrideFatality() {
        return overrideFatality;
    }

    public void setOverrideFatality(Boolean overrideFatality) {
        this.overrideFatality = overrideFatality;
    }

    public Boolean getOverrideSeriousInjury() {
        return overrideSeriousInjury;
    }

    public void setOverrideSeriousInjury(Boolean overrideSeriousInjury) {
        this.overrideSeriousInjury = overrideSeriousInjury;
    }

    public Date getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(Date createdTime) {
        this.createdTime = fixDate(createdTime);
    }

    public boolean isLocked() {
        return ParreAnalysisStatusType.LOCKED == status;
    }

    public Integer getDisplayOrder() {
        return displayOrder;
    }

    public void setDisplayOrder(Integer displayOrder) {
        this.displayOrder = displayOrder;
    }

    public List<ParreAnalysisDTO> getOptionParreAnalysisDTOs() {
        return optionParreAnalysisDTOs;
    }

    public void setOptionParreAnalysisDTOs(List<ParreAnalysisDTO> optionParreAnalysisDTOs) {
        this.optionParreAnalysisDTOs = optionParreAnalysisDTOs;
    }

    public void sort() {
        Collections.sort(optionParreAnalysisDTOs);
    }

    public List<ProposedMeasureDTO> getProposedMeasureDTOs() {
        return proposedMeasureDTOs;
    }

    public void setProposedMeasureDTOs(List<ProposedMeasureDTO> proposedMeasureDTOs) {
        this.proposedMeasureDTOs = proposedMeasureDTOs;
    }

    public NameDescriptionDTO getSiteLocationDTO() {
        return siteLocationDTO;
    }

    public void setSiteLocationDTO(NameDescriptionDTO siteLocationDTO) {
        this.siteLocationDTO = siteLocationDTO;
    }
    
    public Boolean getUseDefaultLocation() {
		return useDefaultLocation == null ? false : useDefaultLocation;
	}

	public void setUseDefaultLocation(Boolean useDefaultLocation) {
		this.useDefaultLocation = useDefaultLocation;
	}

	public String getDefaultPostalCode() {
		return defaultPostalCode;
	}

	public void setDefaultPostalCode(String defaultPostalCode) {
		this.defaultPostalCode = defaultPostalCode;
	}

	public BigDecimal getDefaultLatitude() {
		return defaultLatitude;
	}

	public void setDefaultLatitude(BigDecimal defaultLatitude) {
		this.defaultLatitude = defaultLatitude;
	}

	public BigDecimal getDefaultLongitude() {
		return defaultLongitude;
	}

	public void setDefaultLongitude(BigDecimal defaultLongitude) {
		this.defaultLongitude = defaultLongitude;
	}
	
	public EarthquakeProfileDTO getDefaultEarthquakeProfileDTO() {
		return defaultEarthquakeProfileDTO;
	}

	public void setDefaultEarthquakeProfileDTO(
			EarthquakeProfileDTO defaultEarthquakeProfileDTO) {
		this.defaultEarthquakeProfileDTO = defaultEarthquakeProfileDTO;
	}

	public HurricaneProfileDTO getDefaultHurricaneProfileDTO() {
		return defaultHurricaneProfileDTO;
	}

	public void setDefaultHurricaneProfileDTO(
			HurricaneProfileDTO defaultHurricaneProfileDTO) {
		this.defaultHurricaneProfileDTO = defaultHurricaneProfileDTO;
	}
	
	public List<EarthquakeProfileDTO> getEarthquakeProfileDTOs() {
		return earthquakeProfileDTOs;
	}

	public void setEarthquakeProfileDTOs(
			List<EarthquakeProfileDTO> earthquakeProfileDTOs) {
		this.earthquakeProfileDTOs = earthquakeProfileDTOs;
	}

	public List<HurricaneProfileDTO> getHurricaneProfileDTOs() {
		return hurricaneProfileDTOs;
	}

	public void setHurricaneProfileDTOs(List<HurricaneProfileDTO> hurricaneProfileDTOs) {
		this.hurricaneProfileDTOs = hurricaneProfileDTOs;
	}

	public void updateOptionDTO(ParreAnalysisDTO optionDTO) {
        int index = optionParreAnalysisDTOs.indexOf(optionDTO);
        if (index >= 0) {
            optionParreAnalysisDTOs.set(index, optionDTO);
        }
        else {
            optionParreAnalysisDTOs = add(optionDTO, optionParreAnalysisDTOs);
        }
    }

    public boolean hasOptionAnalyses() {
        return optionParreAnalysisDTOs != null && !optionParreAnalysisDTOs.isEmpty();
    }

    public ParreAnalysisDTO findOptionAnalysis(Long parreAnalysisId) {
        for (ParreAnalysisDTO dto : optionParreAnalysisDTOs) {
            if (dto.hasId(parreAnalysisId)) {
                return dto;
            }
        }
        return null;
    }

    public static ParreAnalysisDTO findParreAnalysis(Long parreAnalysisId, Collection<ParreAnalysisDTO> dtos) {
        for (ParreAnalysisDTO dto : dtos) {
            if (dto.hasId(parreAnalysisId)) {
                return dto;
            }
            ParreAnalysisDTO optionAnalysis = dto.findOptionAnalysis(parreAnalysisId);
            if (optionAnalysis != null) {
                return optionAnalysis;
            }
        }
        return null;
    }
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("ParreAnalysisDTO");
        sb.append("{baselineId=").append(baselineId);
        sb.append(", status=").append(status);
        sb.append(", assetThreshold=").append(assetThreshold);
        sb.append(", assetThreatThreshold=").append(assetThreatThreshold);
        sb.append(", contactDTO=").append(contactDTO);
        sb.append(", siteLocationDTO=").append(siteLocationDTO);
        sb.append(", orgName='").append(orgName).append('\'');
        sb.append(", useStatValues=").append(useStatValues);
        sb.append(", statValuesDTO=").append(statValuesDTO);
        sb.append(", overrideFatality=").append(overrideFatality);
        sb.append(", overrideSeriousInjury=").append(overrideSeriousInjury);
        sb.append(", createdTime=").append(createdTime);
        sb.append(", displayOrder=").append(displayOrder);
        sb.append(", optionParreAnalysisDTOs=").append(optionParreAnalysisDTOs);
        sb.append(", proposedMeasureDTOs=").append(proposedMeasureDTOs);
        sb.append(", useDefaultLocation=").append(useDefaultLocation);
        sb.append(", defaultPostalCode=").append(defaultPostalCode);
        sb.append(", defaultLatitude=").append(defaultLatitude);
        sb.append(", defaultLongitude=").append(defaultLongitude);
        sb.append(", defaultEarthquakeProfileDTO=").append(defaultEarthquakeProfileDTO);
        sb.append(", defaultHurricaneProfileDTO=").append(defaultHurricaneProfileDTO);
        sb.append(", earthquakeProfile.size=" + (earthquakeProfileDTOs == null ? 0 : earthquakeProfileDTOs.size()));
        sb.append(", hurricaneProfile.size=" + (hurricaneProfileDTOs == null ? 0 : hurricaneProfileDTOs.size()));
        sb.append('}');
        return sb.toString();
    }

	public Boolean isDefaultEarthquakeProfile(EarthquakeProfileDTO profile) {
		if (profile != null && profile.getId() != null && defaultEarthquakeProfileDTO != null && defaultEarthquakeProfileDTO.getId() != null) {
			return defaultEarthquakeProfileDTO.getId().equals(profile.getId());
		}
		return false;
	}
	
	public Boolean isDefaultHurricaneProfile(HurricaneProfileDTO profile) {
		if (profile != null && profile.getId() != null && defaultHurricaneProfileDTO != null && defaultHurricaneProfileDTO.getId() != null) {
			return defaultHurricaneProfileDTO.getId().equals(profile.getId());
		}
		return false;
	}
}
