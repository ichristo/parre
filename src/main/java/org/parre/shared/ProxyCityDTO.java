/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.shared;

/**
 * User: Daniel
 * Date: 1/10/12
 * Time: 12:26 PM
*/
public class ProxyCityDTO extends BaseDTO {
    private String city;
    private Integer tierNumber;
    private Integer numberOfCities;

    public ProxyCityDTO() {
    }

    public ProxyCityDTO(Long id) {
        setId(id);
    }

    public ProxyCityDTO(String city, Integer tierNumber, Integer numberOfCities) {
        this.city = city;
        this.tierNumber = tierNumber;
        this.numberOfCities = numberOfCities;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public Integer getTierNumber() {
        return tierNumber;
    }

    public void setTierNumber(Integer tierNumber) {
        this.tierNumber = tierNumber;
    }

    public Integer getNumberOfCities() {
        return numberOfCities;
    }

    public void setNumberOfCities(Integer numberOfCities) {
        this.numberOfCities = numberOfCities;
    }

    public boolean isOther() {
        return (-1 == getId());
    }

    @Override
    public String toString() {
        return "ProxyCityDTO{" +
                "cityName='" + city + '\'' +
                ", tier=" + tierNumber +
                '}';
    }
}
