/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.shared;

/**
 * The Class AssetThreatDTO.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 2/22/12
 */
public class AssetThreatDTO extends BaseDTO {
    private Long assetThreatId;
    private String assetId;
    private String assetName;
    private String threatName;
    private String threatDescription;

    public AssetThreatDTO() {
    }

    public AssetThreatDTO(AssetDTO assetDTO, ThreatDTO threatDTO) {
        setAssetDTO(assetDTO);
        setThreatDTO(threatDTO);
    }

    public void setThreatDTO(ThreatDTO threatDTO) {
        this.threatName = threatDTO.getName();
        this.threatDescription = threatDTO.getDescription();
    }

    public void setAssetDTO(AssetDTO assetDTO) {
        this.assetId = assetDTO.getAssetId();
        this.assetName = assetDTO.getName();
    }

    public String getAssetId() {
        return assetId;
    }

    public void setAssetId(String assetId) {
        this.assetId = assetId;
    }

    public String getAssetName() {
        return assetName;
    }

    public void setAssetName(String assetName) {
        this.assetName = assetName;
    }

    public String getThreatName() {
        return threatName;
    }

    public void setThreatName(String threatName) {
        this.threatName = threatName;
    }

    public String getThreatDescription() {
        return threatDescription;
    }

    public void setThreatDescription(String threatDescription) {
        this.threatDescription = threatDescription;
    }

    public Long getAssetThreatId() {
        return assetThreatId;
    }

    public void setAssetThreatId(Long assetThreatId) {
        this.assetThreatId = assetThreatId;
    }
}
