/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.shared;

public class ReportInvestmentOptionDTO extends NameDescriptionDTO {
	private static final long serialVersionUID = 8969555408628788567L;
	private String investOption;
	private String countermeasure;
	private String sitesApplied;
	private String siteCost;
	private String totalInvestment;
	private Long reportId;
	private Long optionId;
	
	public ReportInvestmentOptionDTO() {
	}
	
	public ReportInvestmentOptionDTO(String investOption, String countermeasure,
			String sitesApplied, String siteCost, String totalInvestment, Long reportId, Long optionId) {
		super();
		this.investOption = investOption;
		this.countermeasure = countermeasure;
		this.sitesApplied = sitesApplied;
		this.siteCost = siteCost;
		this.totalInvestment = totalInvestment;
		this.reportId = reportId;
		this.optionId = optionId;
	}

	public String getInvestOption() {
		return investOption;
	}

	public void setInvestOption(String investOption) {
		this.investOption = investOption;
	}

	public String getCountermeasure() {
		return countermeasure;
	}

	public void setCountermeasure(String countermeasure) {
		this.countermeasure = countermeasure;
	}

	public String getSitesApplied() {
		return sitesApplied;
	}

	public void setSitesApplied(String sitesApplied) {
		this.sitesApplied = sitesApplied;
	}

	public String getSiteCost() {
		return siteCost;
	}

	public void setSiteCost(String siteCost) {
		this.siteCost = siteCost;
	}

	public String getTotalInvestment() {
		return totalInvestment;
	}

	public void setTotalInvestment(String totalInvestment) {
		this.totalInvestment = totalInvestment;
	}

	public Long getReportId() {
		return reportId;
	}

	public void setReportId(Long reportId) {
		this.reportId = reportId;
	}

	public Long getOptionId() {
		return optionId;
	}

	public void setOptionId(Long optionId) {
		this.optionId = optionId;
	}

	@Override
	public String toString() {
		return "ReportInvestmentOptionDTO [investOption=" + investOption
				+ ", countermeasure=" + countermeasure + ", sitesApplied="
				+ sitesApplied + ", siteCost=" + siteCost
				+ ", totalInvestment=" + totalInvestment + ", reportId="
				+ reportId + ", optionId=" + optionId + "]";
	}

	
}
