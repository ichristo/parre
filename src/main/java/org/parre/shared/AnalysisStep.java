/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.shared;

import java.util.Comparator;

/**
 * The Interface AnalysisStep.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 2/1/12
 */
public interface AnalysisStep {
    public static final Comparator<AnalysisStep> COMPARATOR = new Comparator<AnalysisStep>() {
        public int compare(AnalysisStep o1, AnalysisStep o2) {
            return o1.getStepNumber().compareTo(o2.getStepNumber());
        }
    };
    Integer getStepNumber();
    void setPathAnalysisId(Long id);
}
