/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.shared;



import java.math.BigDecimal;

import org.parre.server.domain.Amount;
import org.parre.shared.types.DpType;

/**
 * The Class DpDTO.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 12/5/11
 */
public class DpDTO extends NameDescriptionDTO implements HasFinancialTotal {
    private Long parentId;
    private Long threatId;
    private String assetId;
    private String assetName;
    private DpType type;
    private Integer fatalities;
    private Integer seriousInjuries;
    private AmountDTO financialImpactDTO;
    private AmountDTO economicImpactDTO = AmountDTO.createZero();
    private BigDecimal vulnerability;
    private BigDecimal lot;
    private AmountDTO financialTotalDTO = AmountDTO.createZero();
    private AmountDTO totalRiskDTO = AmountDTO.createZero();
    private BigDecimal durationDays = BigDecimal.ZERO;
    private BigDecimal severityMgd = BigDecimal.ZERO;

    public String getAssetId() {
        return assetId;
    }

    public void setAssetId(String assetId) {
        this.assetId = assetId;
    }

    public String getAssetName() {
        return assetName;
    }

    public void setAssetName(String assetName) {
        this.assetName = assetName;
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public DpType getType() {
        return type;
    }

    public void setType(DpType type) {
        this.type = type;
    }

    public Integer getFatalities() {
        return fatalities;
    }

    public void setFatalities(Integer fatalities) {
        this.fatalities = fatalities;
    }

    public Integer getSeriousInjuries() {
        return seriousInjuries;
    }

    public void setSeriousInjuries(Integer seriousInjuries) {
        this.seriousInjuries = seriousInjuries;
    }

    public AmountDTO getFinancialImpactDTO() {
        return financialImpactDTO;
    }

    public void setFinancialImpactDTO(AmountDTO financialImpactDTO) {
        this.financialImpactDTO = financialImpactDTO;
    }

    public AmountDTO getEconomicImpactDTO() {
        return economicImpactDTO;
    }

    public void setEconomicImpactDTO(AmountDTO economicImpactDTO) {
        this.economicImpactDTO = economicImpactDTO;
    }

    public void setFinancialTotalDTO(AmountDTO financialTotalDTO) {
        this.financialTotalDTO = financialTotalDTO;
    }

    public AmountDTO getFinancialTotalDTO() {
        return financialTotalDTO;
    }

    public BigDecimal getVulnerability() {
        return vulnerability;
    }

    public void setVulnerability(BigDecimal vulnerability) {
        this.vulnerability = vulnerability;
    }

    public BigDecimal getLot() {
        return lot;
    }

    public void setLot(BigDecimal lot) {
        this.lot = lot;
    }

    public Long getThreatId() {
        return threatId;
    }

    public void setThreatId(Long threatId) {
        this.threatId = threatId;
    }

    public boolean isParentOf(DpDTO dpDTO) {
        return getId().equals(dpDTO.getParentId());
    }

    public AmountDTO getTotalRiskDTO() {
        return totalRiskDTO;
    }

    public void setTotalRiskDTO(AmountDTO totalRiskDTO) {
        this.totalRiskDTO = totalRiskDTO;
    }

    @Override
    public String toString() {
        return "DpDTO{" +
                "parentId=" + parentId +
                ", threatId=" + threatId +
                ", assetId='" + assetId + '\'' +
                ", assetName='" + assetName + '\'' +
                ", type=" + type +
                ", fatalities=" + fatalities +
                ", seriousInjuries=" + seriousInjuries +
                ", financialImpactDTO=" + financialImpactDTO +
                ", economicImpactDTO=" + economicImpactDTO +
                ", vulnerability=" + vulnerability +
                ", lot=" + lot +
                ", financialTotalDTO=" + financialTotalDTO +
                ", totalRiskDTO=" + totalRiskDTO +
                ", durationDays=" + durationDays +
                ", severityMgd=" + severityMgd +
                '}';
    }

    public BigDecimal getDurationDays() {
        return durationDays;
    }

    public void setDurationDays(BigDecimal durationDays) {
        this.durationDays = durationDays;
    }

    public BigDecimal getSeverityMgd() {
        return severityMgd;
    }

    public void setSeverityMgd(BigDecimal severityMgd) {
        this.severityMgd = severityMgd;
    }
}
