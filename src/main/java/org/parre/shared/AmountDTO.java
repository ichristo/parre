/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.shared;


import java.io.Serializable;
import java.math.BigDecimal;

import org.parre.shared.types.MoneyUnitType;

/**
 * The Class AmountDTO.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 9/22/11
 */
public class AmountDTO implements Serializable {
    public static AmountDTO createZero() {
        return new AmountDTO(BigDecimal.ZERO, MoneyUnitType.DOLLAR);
    }

    private BigDecimal quantity;

    private MoneyUnitType unit;

    public AmountDTO() {
    }
    public AmountDTO(BigDecimal quantity) {
        this(quantity, MoneyUnitType.DOLLAR);
    }

    public AmountDTO(BigDecimal quantity, MoneyUnitType unit) {
        this.quantity = quantity;
        this.unit = MoneyUnitType.DOLLAR;
    }

    public BigDecimal getQuantity() {
        return quantity;
    }

    public MoneyUnitType getUnit() {
        return unit;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AmountDTO costDTO = (AmountDTO) o;

        if (!quantity.equals(costDTO.quantity)) return false;
        if (unit != costDTO.unit) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = quantity.hashCode();
        result = 31 * result + unit.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return quantity + " " + unit;
    }
}
