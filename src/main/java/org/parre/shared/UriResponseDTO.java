/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.shared;

import java.math.BigDecimal;
import java.util.List;

/**
 * User: Swarn S. Dhaliwal
 * Date: 7/14/11
 * Time: 11:20 AM
*/
public class UriResponseDTO extends BaseDTO {
    private Long riskResilienceAnalysisId;
    private Long uriQuestionId;
    private Long uriOptionId;
    private String questionName;
    private String optionName;
    private BigDecimal value;
    private BigDecimal weight;
    private UriQuestionDTO uriQuestionDTO;

    public UriResponseDTO() {
    }

    public UriResponseDTO(Long riskResilienceAnalysisId, UriQuestionDTO uriQuestionDTO) {
        this.riskResilienceAnalysisId = riskResilienceAnalysisId;
        this.uriQuestionDTO = uriQuestionDTO;
        this.uriQuestionId = uriQuestionDTO.getId();
        this.questionName = uriQuestionDTO.getName();
        UriOptionDTO uriOptionDTO = uriQuestionDTO.getOptionDTOs().get(0);
        uriOptionId = uriOptionDTO.getId();
        optionName = uriOptionDTO.getName();
        value = uriOptionDTO.getValue();
        weight = uriOptionDTO.getWeight();
    }

    public UriResponseDTO(String questionName, String optionName, BigDecimal value, BigDecimal weight) {
        this.questionName = questionName;
        this.optionName = optionName;
        this.value = value;
        this.weight = weight;
    }

    public Long getUriOptionId() {
        return uriOptionId;
    }

    public void setUriOptionId(Long uriOptionId) {
        this.uriOptionId = uriOptionId;
    }

    public String getQuestionName() {
        return questionName;
    }

    public void setQuestionName(String questionName) {
        this.questionName = questionName;
    }

    public String getOptionName() {
        return optionName;
    }

    public void setOptionName(String optionName) {
        this.optionName = optionName;
    }

    public BigDecimal getValue() {
        return value;
    }

    public void setValue(BigDecimal value) {
        this.value = value;
    }

    public BigDecimal getWeight() {
        return weight;
    }

    public void setWeight(BigDecimal weight) {
        this.weight = weight;
    }

    public UriQuestionDTO getUriQuestionDTO() {
        return uriQuestionDTO;
    }

    public void setUriQuestionDTO(UriQuestionDTO uriQuestionDTO) {
        this.uriQuestionDTO = uriQuestionDTO;
    }

    public Long getRiskResilienceAnalysisId() {
        return riskResilienceAnalysisId;
    }

    public void setRiskResilienceAnalysisId(Long riskResilienceAnalysisId) {
        this.riskResilienceAnalysisId = riskResilienceAnalysisId;
    }

    public Long getUriQuestionId() {
        return uriQuestionId;
    }

    public void setUriQuestionId(Long uriQuestionId) {
        this.uriQuestionId = uriQuestionId;
    }

    public void populateReferenceValues() {
        setQuestionName(uriQuestionDTO.getName());
        List<UriOptionDTO> optionDTOs = uriQuestionDTO.getOptionDTOs();
        for (UriOptionDTO optionDTO : optionDTOs) {
            if (optionDTO.hasId(uriOptionId)) {
                setOptionName(optionDTO.getName());
            }
        }
    }

	@Override
	public String toString() {
		return "UriResponseDTO [riskResilienceAnalysisId="
				+ riskResilienceAnalysisId + ", uriQuestionId=" + uriQuestionId
				+ ", uriOptionId=" + uriOptionId + ", questionName="
				+ questionName + ", optionName=" + optionName + ", value="
				+ value + ", weight=" + weight + ", uriQuestionDTO="
				+ uriQuestionDTO + "]";
	}
}
