/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.shared;

import java.math.BigDecimal;

/**
 * The Class RiskBenefitOptionDTO.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 3/2/12
 */
public class RiskBenefitOptionDTO extends BaseDTO implements Comparable<RiskBenefitOptionDTO> {
    private Long parreAnalysisId;
    private Integer displayOrder;
    private AmountDTO optionAmountDTO = new AmountDTO(BigDecimal.ZERO);
    private Long riskBenefitId;

    public Long getParreAnalysisId() {
        return parreAnalysisId;
    }

    public void setParreAnalysisId(Long parreAnalysisId) {
        this.parreAnalysisId = parreAnalysisId;
    }

    public Integer getDisplayOrder() {
        return displayOrder;
    }

    public void setDisplayOrder(Integer displayOrder) {
        this.displayOrder = displayOrder;
    }

    public AmountDTO getOptionAmountDTO() {
        return optionAmountDTO;
    }

    public void setOptionAmountDTO(AmountDTO optionAmountDTO) {
        this.optionAmountDTO = optionAmountDTO;
    }

    public Long getRiskBenefitId() {
        return riskBenefitId;
    }

    public void setRiskBenefitId(Long riskBenefitId) {
        this.riskBenefitId = riskBenefitId;
    }

    public int compareTo(RiskBenefitOptionDTO o) {
        return displayOrder.compareTo(o.displayOrder);
    }

    public void updateId() {
        setId(getParreAnalysisId());
    }

	@Override
	public String toString() {
		return "RiskBenefitOptionDTO [parreAnalysisId=" + parreAnalysisId
				+ ", displayOrder=" + displayOrder + ", optionAmountDTO="
				+ optionAmountDTO + ", riskBenefitId=" + riskBenefitId + "]";
	}
    
    
}
