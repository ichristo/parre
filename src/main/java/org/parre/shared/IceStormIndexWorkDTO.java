/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.shared;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * The Class IceStormIndexWorkDTO.
 */
public class IceStormIndexWorkDTO implements Serializable {
	private static final long serialVersionUID = 1L;

	private BigDecimal iceDamageProbability = BigDecimal.ZERO;
	private BigDecimal powerOutageProbability = BigDecimal.ZERO;
	
	private BigDecimal recoveryTime = BigDecimal.ZERO;
	private Integer fatalities = 0;
	private Integer seriousInjuries = 0;
	
	private BigDecimal consequence = BigDecimal.ZERO;
	private BigDecimal vulnerability = BigDecimal.ZERO;
	private BigDecimal lot = BigDecimal.ZERO;
	
	private BigDecimal financialImpact = BigDecimal.ZERO; // This is consequence without statistical life adjustments 
	
	public IceStormIndexWorkDTO() { }
	
	public IceStormIndexWorkDTO(BigDecimal recoveryTime, Integer fatalities, Integer seriousInjuries) {
		super();
		setRecoveryTime(recoveryTime);
		setFatalities(fatalities);
		setSeriousInjuries(seriousInjuries);
	}

	public BigDecimal getIceDamageProbability() {
		return iceDamageProbability;
	}

	public void setIceDamageProbability(BigDecimal iceDamageProbability) {
		this.iceDamageProbability = iceDamageProbability;
	}

	public BigDecimal getPowerOutageProbability() {
		return powerOutageProbability;
	}

	public void setPowerOutageProbability(BigDecimal powerOutageProbability) {
		this.powerOutageProbability = powerOutageProbability;
	}

	public BigDecimal getRecoveryTime() {
		return recoveryTime;
	}

	public void setRecoveryTime(BigDecimal recoveryTime) {
		this.recoveryTime = recoveryTime;
	}

	public Integer getFatalities() {
		return fatalities;
	}

	public void setFatalities(Integer fatalities) {
		this.fatalities = fatalities;
	}

	public Integer getSeriousInjuries() {
		return seriousInjuries;
	}

	public void setSeriousInjuries(Integer seriousInjuries) {
		this.seriousInjuries = seriousInjuries;
	}

	public BigDecimal getConsequence() {
		return consequence;
	}

	public void setConsequence(BigDecimal consequence) {
		this.consequence = consequence;
	}

	public BigDecimal getVulnerability() {
		return vulnerability;
	}

	public void setVulnerability(BigDecimal vulnerability) {
		this.vulnerability = vulnerability;
	}

	public BigDecimal getLot() {
		return lot;
	}

	public void setLot(BigDecimal lot) {
		this.lot = lot;
	}

	public BigDecimal getFinancialImpact() {
		return financialImpact;
	}

	public void setFinancialImpact(BigDecimal financialImpact) {
		this.financialImpact = financialImpact;
	}

	@Override
	public String toString() {
		return "IceStormIndexWorkDTO [iceStormProbability="
				+ iceDamageProbability + ", powerOutageProbability="
				+ powerOutageProbability + ", recoveryTime=" + recoveryTime
				+ ", fatalities=" + fatalities + ", seriousInjuries="
				+ seriousInjuries + ", consequence=" + consequence
				+ ", vulnerability=" + vulnerability + ", lot=" + lot + ", financialImpact=" + financialImpact +"]";
	}
	
	
	
	
}
