/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.shared;

import java.math.BigDecimal;

/**
 * The Class BenefitCostDTO.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 5/4/12
 */
public class BenefitCostDTO extends BaseDTO implements Comparable<BenefitCostDTO> {
    private String optionName;
    private Integer displayOrder;
    private BigDecimal totalGrossBenefits;
    private BigDecimal presentValueCost;
    private BigDecimal netBenefits;
    private BigDecimal benefitCostRatio;

    public BenefitCostDTO() {
    }

    public BenefitCostDTO(Long optionId, String optionName, Integer displayOrder) {
        this.displayOrder = displayOrder;
        setId(optionId);
        this.optionName = optionName;
    }

    public BigDecimal getTotalGrossBenefits() {
        return totalGrossBenefits;
    }

    public void setTotalGrossBenefits(BigDecimal totalGrossBenefits) {
        this.totalGrossBenefits = totalGrossBenefits;
    }

    public BigDecimal getPresentValueCost() {
        return presentValueCost;
    }

    public void setPresentValueCost(BigDecimal presentValueCost) {
        this.presentValueCost = presentValueCost;
    }

    public BigDecimal getNetBenefits() {
        return netBenefits;
    }

    public void setNetBenefits(BigDecimal netBenefits) {
        this.netBenefits = netBenefits;
    }

    public BigDecimal getBenefitCostRatio() {
        return benefitCostRatio;
    }

    public void setBenefitCostRatio(BigDecimal benefitCostRatio) {
        this.benefitCostRatio = benefitCostRatio;
    }

    public Integer getDisplayOrder() {
        return displayOrder;
    }

    public void setDisplayOrder(Integer displayOrder) {
        this.displayOrder = displayOrder;
    }

    public int compareTo(BenefitCostDTO o) {
        return displayOrder.compareTo(o.displayOrder);
    }

    public String getOptionName() {
        return optionName;
    }

    public void setOptionName(String optionName) {
        this.optionName = optionName;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("BenefitCostDTO");
        sb.append("{totalGrossBenefits=").append(totalGrossBenefits);
        sb.append(", presentValueCost=").append(presentValueCost);
        sb.append(", netBenefits=").append(netBenefits);
        sb.append(", benefitCostRatio=").append(benefitCostRatio);
        sb.append('}');
        return sb.toString();
    }
}
