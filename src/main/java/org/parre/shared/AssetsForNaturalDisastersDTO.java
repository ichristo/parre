/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.shared;


import java.math.BigDecimal;

import org.parre.shared.types.AssetType;

/**
 * User: keithjones
 * Date: 1/23/12
 * Time: 4:33 PM
*/
public class AssetsForNaturalDisastersDTO extends BaseDTO {

    private String assetTitle;
    private String assetID;
    private Integer yearBuilt;
    private BigDecimal costToReplace;

    public String getAssetTitle() {
        return assetTitle;
    }

    public void setAssetTitle(String assetTitle) {
        this.assetTitle = assetTitle;
    }

    public String getAssetID() {
        return assetID;
    }

    public void setAssetID(String assetID) {
        this.assetID = assetID;
    }

    public Integer getYearBuilt() {
        return yearBuilt;
    }

    public void setYearBuilt(Integer yearBuilt) {
        this.yearBuilt = yearBuilt;
    }

    public BigDecimal getCostToReplace() {
        return costToReplace;
    }

    public void setCostToReplace(BigDecimal costToReplace) {
        this.costToReplace = costToReplace;
    }

    @Override
    public String toString() {
        return "AssetsForNaturalDisastersDTO{" +
                "assetTitle='" + assetTitle + '\'' +
                ", assetID='" + assetID + '\'' +
                ", yearBuilt=" + yearBuilt +
                ", costToReplace=" + costToReplace +
                '}';
    }


}
