/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.shared;


import java.math.BigDecimal;

import org.parre.shared.types.AnalysisPathStepType;

/**
 * The Class ResponseStepDTO.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 1/31/12
 */
public class ResponseStepDTO extends NameDescriptionDTO implements AnalysisStep {
    private Long pathAnalysisId;
    private AnalysisPathStepType type;
    private BigDecimal duration;
    private Integer stepNumber;

    public ResponseStepDTO() {
    }

    public ResponseStepDTO(Long pathAnalysisId) {
        this.pathAnalysisId = pathAnalysisId;
    }

    public Long getPathAnalysisId() {
        return pathAnalysisId;
    }

    public void setPathAnalysisId(Long pathAnalysisId) {
        this.pathAnalysisId = pathAnalysisId;
    }

    public AnalysisPathStepType getType() {
        return type;
    }

    public void setType(AnalysisPathStepType type) {
        this.type = type;
    }

    public BigDecimal getDuration() {
        return duration;
    }

    public void setDuration(BigDecimal duration) {
        this.duration = duration;
    }

    public Integer getStepNumber() {
        return stepNumber;
    }

    public void setStepNumber(Integer stepNumber) {
        this.stepNumber = stepNumber;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("ResponseStepDTO");
        sb.append("{pathAnalysisId=").append(pathAnalysisId);
        sb.append(", type=").append(type);
        sb.append(", duration=").append(duration);
        sb.append(", stepNumber=").append(stepNumber);
        sb.append('}');
        return sb.toString();
    }
}
