/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.shared;

public class ReportAssessmentTaskDTO extends NameDescriptionDTO {

	private static final long serialVersionUID = -2322554949646471628L;
	
	private Long reportId;
	private Integer taskNumber;
	private String title;
	private String comments;
	private Long taskId;
	
	public ReportAssessmentTaskDTO() {
	}
	
	public ReportAssessmentTaskDTO(Long reportId, Integer taskNumber,
			String title, String comments, Long taskId) {
		super();
		this.reportId = reportId;
		this.taskNumber = taskNumber;
		this.title = title;
		this.comments = comments;
		this.taskId = taskId;
	}

	public Long getReportId() {
		return reportId;
	}
	public void setReportId(Long reportId) {
		this.reportId = reportId;
	}
	public Integer getTaskNumber() {
		return taskNumber;
	}
	public void setTaskNumber(Integer taskNumber) {
		this.taskNumber = taskNumber;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	public Long getTaskId() {
		return taskId;
	}

	public void setTaskId(Long taskId) {
		this.taskId = taskId;
	}

	@Override
	public String toString() {
		return "ReportAssessmentTaskDTO [reportId=" + reportId
				+ ", taskNumber=" + taskNumber + ", title=" + title
				+ ", comments=" + comments + ", taskId=" + taskId + "]";
	}

	
}
