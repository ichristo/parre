/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.shared;


import java.math.BigDecimal;

import org.parre.shared.types.AssetType;
import org.parre.shared.types.PhysicalAssetType;

/**
 * The Class PhysicalResourceDTO.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 9/22/11
 */
public class PhysicalResourceDTO extends AssetDTO {
    private Integer yearBuilt;
    private String postalCode;
    private AmountDTO replacementCostDTO = AmountDTO.createZero();
    private BigDecimal damageFactor = BigDecimal.ZERO;
    private BigDecimal latitude;
    private BigDecimal longitude;
    
    private String countyName;
    private String stateCode;
    
    private PhysicalAssetType physicalAssetType;

    public PhysicalResourceDTO() {
    }

    public PhysicalResourceDTO(Integer yearBuilt, AmountDTO replacementCostDTO, String postalCode, BigDecimal damageFactor) {
        this.yearBuilt = yearBuilt;
        this.replacementCostDTO = replacementCostDTO;
        this.postalCode = postalCode;
        this.damageFactor = damageFactor;
    }

    @Override
    public AssetType getAssetType() {
        return AssetType.PHYSICAL_ASSET;
    }

    @Override
    protected AssetDTO createClone() {
        return new PhysicalResourceDTO(getYearBuilt(), getReplacementCostDTO(), getPostalCode(), getDamageFactor());
    }

    public AmountDTO getReplacementCostDTO() {
        return replacementCostDTO;
    }

    public void setReplacementCostDTO(AmountDTO replacementCostDTO) {
        this.replacementCostDTO = replacementCostDTO;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public Integer getYearBuilt() {
        return yearBuilt;
    }

    public void setYearBuilt(Integer yearBuilt) {
        this.yearBuilt = yearBuilt;
    }

    public BigDecimal getDamageFactor() {
        return damageFactor;
    }

    public void setDamageFactor(BigDecimal damageFactor) {
        this.damageFactor = damageFactor;
    }

    public BigDecimal getLatitude() {
        return latitude;
    }

    public void setLatitude(BigDecimal latitude) {
        this.latitude = latitude;
    }

    public BigDecimal getLongitude() {
        return longitude;
    }

    public void setLongitude(BigDecimal longitude) {
        this.longitude = longitude;
    }

	public String getCountyName() {
		return countyName;
	}

	public void setCountyName(String countyName) {
		this.countyName = countyName;
	}

	public String getStateCode() {
		return stateCode;
	}

	public void setStateCode(String stateCode) {
		this.stateCode = stateCode;
	}

	public PhysicalAssetType getPhysicalAssetType() {
		return physicalAssetType;
	}

	public void setPhysicalAssetType(PhysicalAssetType physicalAssetType) {
		this.physicalAssetType = physicalAssetType;
	}
	
}
