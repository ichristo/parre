/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.shared.types;

/**
 * The Enum PhysicalAssetType.
 */
public enum PhysicalAssetType {
	SLAB_MOUNTED_EQUIPMENT("Slab Mounted Equipment-pumps", "0.2"),
	VALVES("Valves", "0.2"),
	COMPRESSORS("Compressors", "0.2"),
	METERS("Meters", "0.2"),
	ELECTRIC_MOTORS("Electric Motors", "0.2"),
	CONSOLES("Consoles", "0.2"),
	BURIED_PIPING("Buried Piping", "0.2"),
	HEATERS_SEISMIC_TRAPS("Hot Water Heaters with Seismic Traps", "0.2"),
	AUTOMOBILES("Automobiles", "0.2"),
	TRUCKS("Trucks", "0.2"),
	HEAVY_EQUIPMENT("Heavy Equipment", "0.2"),
	ABOVE_GROUND_PIPING("Above Ground Piping", "0.3"),
	PRESSURE_VESSELS("Pressure Vessels", "0.3"),
	BUILDING_TO_CODE("Building Designed to code", "0.5"),
	BUILDING_NOT_TO_CODE("Building not designed to code", "0.75"),
	PORTABLE_BUILDINGS("Portable buildings" , "1"),
	TRAILER("Trailer", "1");
	
	private final String displayName;
	private final String damageFactor;
	
	PhysicalAssetType(String displayName, String damageFactor) {
		this.displayName = displayName;
		this.damageFactor = damageFactor;
	}
	
	public String getDisplayName() {
		return displayName;
	}
	
	public String getDamageFactor() {
		return damageFactor;
	}

	public static PhysicalAssetType lookupByDisplayName(String selectedPhysicalAssetType) {
		for (PhysicalAssetType pat : values()) {
			if (pat.getDisplayName().equalsIgnoreCase(selectedPhysicalAssetType)) {
				return pat;
			}
		}
		return null;
	}
}
