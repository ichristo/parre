/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.shared.types;


/**
 * The Enum UriQuestionType.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 2/15/12
 */
public enum UriQuestionType {
    FRI("Financial Risk Index"), ORI("Operational Risk Index");
    private String value;

    UriQuestionType(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public boolean isFri() {
        return FRI == this;
    }

    public boolean isOri() {
        return ORI == this;
    }
}
