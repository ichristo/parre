/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.shared.types;

import org.parre.shared.LabelValue;

/**
 * The Enum RiskBenefitMetricType.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 4/17/12
 */
public enum RiskBenefitMetricType implements LabelValue {
    RISK("Risk"), FATALITIES("Lives Lost"), SERIOUS_INJURIES("Serious Injuries"),
    FINANCIAL_IMPACT("Owner Financial Loss"), FINANCIAL_TOTAL("Total Financial Loss"),
    ECONOMIC_IMPACT("Community Financial Loss"), VULNERABILITY("Vulnerability"),
    LOT("Threat Likelihood"), OWNER_RESILIENCE("Owner Resilience"),
    ECONOMIC_RESILIENCE("Economic Resilience");

    private String label;

    RiskBenefitMetricType(String label) {
        this.label = label;
    }

    public String getValue() {
        return name();
    }

    public String getLabel() {
        return label;
    }
}
