/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.shared.types;

/**
 * The Enum CounterMeasureType.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 1/31/12
 */
public enum CounterMeasureType {
    ACCESS_CONTROL_SYSTEMS("Access Control Systems"), ALARMS("Alarms"),
    CAMERAS("Cameras/Closed Circuit TV"), DOORS("Doors"),
    FENCE("Fence"), LIGHTING("Lighting"), LOCKS("Locks"),
    OPEN_SPACE_SETBACK("Open Space/Setback"), OTHER("Other"), SECURITY_GUARDS("Security Guards"),
    SENSORS("Sensors"), SIGNAGE("Signage"), TARGET("Target"), TRAINING("Training"),
    VEHICLE_BARRIERS("Vehicle Barriers"), WALLS("Walls"), WINDOWS("Windows");

    private String value;

    CounterMeasureType(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
