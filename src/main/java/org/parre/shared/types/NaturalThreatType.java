/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.shared.types;

/**
 * The Enum NaturalThreatType.
 */
public enum NaturalThreatType {
	TORNADO("Tornado"), ICE_STORM("Ice Storm"), EARTHQUAKE("Earthquake"), HURRICANE(
			"Hurricane"), FLOOD("Flood"), WILDFIRE("Wildfire"), UNKNOWN("unknown");
	private String value;

	NaturalThreatType(String value) {
		this.value = value;
	}

	public String value() {
		return value;
	}
	
	public static NaturalThreatType getNaturalThreatType(String type) {
		if (type == null) { return NaturalThreatType.UNKNOWN; }
		if ("tornado".equalsIgnoreCase(type)) { return TORNADO; }
		if ("Ice Storms".equalsIgnoreCase(type)) { return ICE_STORM; }
		if ("Earthquake".equalsIgnoreCase(type)) { return EARTHQUAKE; }
		if ("Hurricane".equalsIgnoreCase(type)) { return HURRICANE; }
		if ("Flood".equalsIgnoreCase(type)) { return FLOOD; }
		if ("Wildfire".equalsIgnoreCase(type)) { return WILDFIRE; }
		return UNKNOWN;
	}

}
