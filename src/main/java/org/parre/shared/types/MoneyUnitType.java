/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.shared.types;

import java.math.BigDecimal;

/**
 * The Enum MoneyUnitType.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 9/22/11
 */
public enum MoneyUnitType {
    DOLLAR("Dollar", BigDecimal.ONE), HUNDRED("Hundred", new BigDecimal(100)),
    THOUSAND("Thousand", new BigDecimal(1000)), HUNDRED_THOUSAND("100 Thousand", new BigDecimal(100 * 1000)),
    MILLION("Million", new BigDecimal(1000 * 1000)),
    BILLION("Billion", new BigDecimal(1000 * 1000 * 1000)), NONE("Not Set", BigDecimal.ONE);

    private String value;
    private BigDecimal dollarEquivalent;
    MoneyUnitType(String value, BigDecimal dollarEquivalent) {
        this.value = value;
        this.dollarEquivalent = dollarEquivalent;
    }

    public String getValue() {
        return value;
    }

    public BigDecimal getDollarEquivalent() {
        return dollarEquivalent;
    }
}
