/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.shared.types;


import java.util.HashMap;
import java.util.Map;

/**
 * The Enum ThreatCategoryType.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 9/30/11
 */
public enum ThreatCategoryType {
    DEPENDENCY_PROXIMITY("Dependency and Proximity"), MAN_MADE_HAZARD ("Man-Made Hazard"),
    NATURAL_HAZARD("Natural Hazard"), NONE("");

    private static final Map<String, ThreatCategoryType> VALUE_MAP =
            new HashMap<String, ThreatCategoryType>(ThreatCategoryType.values().length);
    static {
        ThreatCategoryType[] values = ThreatCategoryType.values();
        for (ThreatCategoryType value : values) {
            VALUE_MAP.put(value.getValue(), value);
        }
    }
    private String value;

    private ThreatCategoryType(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public static ThreatCategoryType fromValue(String value) {
        return VALUE_MAP.get(value);
    }

    public boolean isNone() {
        return NONE == this;
    }

    public boolean isManMadeHazard() {
        return MAN_MADE_HAZARD == this;
    }
}
