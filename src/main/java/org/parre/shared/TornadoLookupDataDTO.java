/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.shared;

import java.math.BigDecimal;

/**
 * User: keithjones
 * Date: 1/30/12
 * Time: 1:45 PM
*/
public class TornadoLookupDataDTO extends BaseDTO {
    private String state;
    private String county;
    private Integer numberOfTornadoes;
    private BigDecimal areaOfCounty;
    private BigDecimal estimatedTornadoFrequency;

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCounty() {
        return county;
    }

    public void setCounty(String county) {
        this.county = county;
    }

    public Integer getNumberOfTornadoes() {
        return numberOfTornadoes;
    }

    public void setNumberOfTornadoes(Integer numberOfTornadoes) {
        this.numberOfTornadoes = numberOfTornadoes;
    }

    public BigDecimal getAreaOfCounty() {
        return areaOfCounty;
    }

    public void setAreaOfCounty(BigDecimal areaOfCounty) {
        this.areaOfCounty = areaOfCounty;
    }

    public BigDecimal getEstimatedTornadoFrequency() {
        return estimatedTornadoFrequency;
    }

    public void setEstimatedTornadoFrequency(BigDecimal estimatedTornadoFrequency) {
        this.estimatedTornadoFrequency = estimatedTornadoFrequency;
    }

    @Override
    public String toString() {
        return "TornadoCalcDTO{" +
                "state='" + state + '\'' +
                ", county='" + county + '\'' +
                ", numberOfTornadoes=" + numberOfTornadoes +
                ", areaOfCounty=" + areaOfCounty +
                ", estimatedTornadoFrequency=" + estimatedTornadoFrequency +
                '}';
    }
}
