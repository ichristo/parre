/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.shared;

/**
 * @author Raju Kanumuri
 * @date 10/30/2013
 */
public class RptFatalitiesDTO extends BaseDTO {
    private String asset;
    private String threat;
    private String fatalities;
    
    public RptFatalitiesDTO(String asset, String threat, String fatalities){
		this.asset=asset;
		this.threat=threat;
		this.fatalities=fatalities;
	}

	public String getAsset() {
		return asset;
	}

	public void setAsset(String asset) {
		this.asset = asset;
	}

	public String getThreat() {
		return threat;
	}

	public void setThreat(String threat) {
		this.threat = threat;
	}

	public String getFatalities() {
		return fatalities;
	}

	public void setFatalities(String fatalities) {
		this.fatalities = fatalities;
	}

	
	
}
