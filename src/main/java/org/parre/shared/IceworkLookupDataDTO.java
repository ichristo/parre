/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.shared;

import java.math.BigDecimal;

/**
 * User: keithjones
 * Date: 2/22/12
 * Time: 2:11 PM
*/
public class IceworkLookupDataDTO extends BaseDTO {

    private String state;
    private String initials;
    private BigDecimal interval1 = BigDecimal.ZERO;   //.1  <= X < .25
    private BigDecimal interval2 = BigDecimal.ZERO;   //.25 <= X < .5
    private BigDecimal interval3 = BigDecimal.ZERO;   //.5  <= X < .75
    private BigDecimal interval4 = BigDecimal.ZERO;   //.75 <= X < 1
    private BigDecimal interval5 = BigDecimal.ZERO;   //1   <= X < 1.5
    private BigDecimal interval6 = BigDecimal.ZERO;   //1.5 <= X


    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getInitials() {
        return initials;
    }

    public void setInitials(String initials) {
        this.initials = initials;
    }

    public BigDecimal getInterval1() {
        return interval1;
    }

    public void setInterval1(BigDecimal interval1) {
        this.interval1 = interval1;
    }

    public BigDecimal getInterval2() {
        return interval2;
    }

    public void setInterval2(BigDecimal interval2) {
        this.interval2 = interval2;
    }

    public BigDecimal getInterval3() {
        return interval3;
    }

    public void setInterval3(BigDecimal interval3) {
        this.interval3 = interval3;
    }

    public BigDecimal getInterval4() {
        return interval4;
    }

    public void setInterval4(BigDecimal interval4) {
        this.interval4 = interval4;
    }

    public BigDecimal getInterval5() {
        return interval5;
    }

    public void setInterval5(BigDecimal interval5) {
        this.interval5 = interval5;
    }

    public BigDecimal getInterval6() {
        return interval6;
    }

    public void setInterval6(BigDecimal interval6) {
        this.interval6 = interval6;
    }
    
    public BigDecimal getIntervalByIndex(int index) {
    	switch (index) {
    	case 1: return getInterval1();
    	case 2: return getInterval2();
    	case 3: return getInterval3();
    	case 4: return getInterval4();
    	case 5: return getInterval5();
    	case 6: return getInterval6();
    	}
    	throw new IllegalArgumentException("Ice thickness index for calculations should be between 1 and 6 inclusive.");
    }

    @Override
    public String toString() {
        return "IceworkStatesDTO{" +
                "state='" + state + '\'' +
                ", initials='" + initials + '\'' +
                ", interval1=" + interval1 +
                ", interval2=" + interval2 +
                ", interval3=" + interval3 +
                ", interval4=" + interval4 +
                ", interval5=" + interval5 +
                ", interval6=" + interval6 +
                '}';
    }
}
