/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.shared;

/**
 * The Class NameDescriptionDTO.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 9/21/11
 */
public class NameDescriptionDTO extends BaseDTO implements LabelValue, Comparable<NameDescriptionDTO> {
    private String name = "";
    private String description = "";

    public NameDescriptionDTO() {
    }

    public NameDescriptionDTO(Long id) {
        setId(id);
    }

    public NameDescriptionDTO(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public NameDescriptionDTO(NameDescriptionDTO dto) {
        this(dto.getName(), dto.getDescription());
    }

    public NameDescriptionDTO copyInto(NameDescriptionDTO dto) {
        super.copyInto(dto);
        dto.setName(getName());
        dto.setDescription(getDescription());
        return dto;
    }

    public void copyFrom(NameDescriptionDTO dto) {
        super.copyFrom(dto);
        setName(dto.getName());
        setDescription(dto.getDescription());
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int compareTo(NameDescriptionDTO o) {
        return name.compareTo(o.getName());
    }

    public String getValue() {
        return String.valueOf(getId());
    }

    public String getLabel() {
        return getName();
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("NameDescriptionDTO");
        sb.append("{name='").append(name).append('\'');
        sb.append(", description='").append(description).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
