/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.shared;

import java.util.ArrayList;
import java.util.List;

import org.parre.shared.types.AssetType;


/**
 * User: Swarn S. Dhaliwal
 * Date: 7/14/11
 * Time: 11:19 AM
*/
public abstract class AssetDTO extends NameDescriptionDTO {
    private Long parreAnalysisId;
    private Boolean removed = Boolean.FALSE;
    private String assetId = "";
    private Boolean critical;
    private Integer humanPl = Integer.valueOf(0);
    private Integer financialPl = Integer.valueOf(0);
    private Integer economicPl = Integer.valueOf(0);
    private List<NoteDTO> noteDTOs = new ArrayList<NoteDTO>();
    private String systemType;
    

    public abstract AssetType getAssetType();

    protected abstract  AssetDTO createClone();

    public AssetDTO cloneAsset() {
        AssetDTO dto = createClone();
        super.copyInto(dto);
        dto.setRemoved(Boolean.FALSE);
        dto.setAssetId(getAssetId());
        dto.setCritical(getCritical());
        dto.setHumanPl(getHumanPl());
        dto.setFinancialPl(getFinancialPl());
        dto.setEconomicPl(getEconomicPl());
        dto.setNoteDTOs(getNoteDTOs());
        return dto;
    }

    public Long getParreAnalysisId() {
        return parreAnalysisId;
    }

    public void setParreAnalysisId(Long parreAnalysisId) {
        this.parreAnalysisId = parreAnalysisId;
    }

    public Boolean getRemoved() {
        return removed;
    }

    public void setRemoved(Boolean removed) {
        this.removed = removed;
    }

    public String getAssetId() {
        return assetId;
    }

    public void setAssetId(String assetId) {
        this.assetId = assetId;
    }

    public Boolean getCritical() {
        return critical;
    }

    public void setCritical(Boolean critical) {
        this.critical = critical;
    }

    public Integer getHumanPl() {
        return humanPl;
    }

    public void setHumanPl(Integer humanPl) {
        this.humanPl = humanPl;
    }

    public Integer getEconomicPl() {
        return economicPl;
    }

    public void setEconomicPl(Integer economicPl) {
        this.economicPl = economicPl;
    }

    public Integer getFinancialPl() {
        return financialPl;
    }

    public void setFinancialPl(Integer financialPl) {
        this.financialPl = financialPl;
    }

    public Integer getTotalPl() {
        return humanPl + financialPl + economicPl;
    }

    public String getName() {
        return super.getName();
    }
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("AssetDTO");
        sb.append("{assetId='").append(assetId).append('\'');
        sb.append(", critical=").append(critical);
        sb.append(", humanPl=").append(humanPl);
        sb.append(", financialPl=").append(financialPl);
        sb.append(", economicPl=").append(economicPl);
        sb.append(", systemType=").append(systemType);
        sb.append('}');
        return sb.toString();
    }

    public List<NoteDTO> getNoteDTOs() {
        return noteDTOs;
    }

    public void setNoteDTOs(List<NoteDTO> noteDTOs) {
        this.noteDTOs = noteDTOs;
    }

	public String getSystemType() {
		return systemType;
	}

	public void setSystemType(String systemType) {
		this.systemType = systemType;
	}
}
