/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.shared;

import java.util.Date;
import java.util.List;


/**
 * @author Raju Kanumuri
 * @date 10/28/2013
 */
public class RptMainDTO extends BaseDTO {	
	
	private String name;
	private String description;
	private Date createdTime;
	private String exeSummary;
	private String assesTeam;
	private String assesTeamOf;
	private String assesApproach;
	private String assesTasks;
	private String workSessions;
	private String descUtility;
	private String missionStmt;
	private String criticalAssets;
	private String assetThreatPairs;
	private String calConsequences;
	private String expertElicitation;
	private String pathAnalysisDisc;
	private String treeEventDisc;
	private String bestEstimate;
	private String proxyAnalysis;
	private String condLikelihood;
	private String riskManagement;
	private String riskReductionOpt;
	
	private String sIntroduction;
	private String sRiskAssessment;
	private String sComplaintAssessment;
	private String sSafetyAct;
	private String sSupportTool;
	private String sAssessmentTeamMembers;
	private String sAssessmentProcess;
	private String sMissionStatement;
	private String sCriticalAssets;
	private String sProbableThreats;
	private String sAssetThreatPairs;
	private String sCountermeasure;
	private String sCalOfConsequences;
	private String sStatisticalValue;
	private String sAssetVulnerabilities;
	private String sPathAnalysisDiscussion;
	private String sEventTreeDiscussion;
	private String sThreatLikelihood;
	private String sBestEstimate;
	private String sProxyAnalysis;
	private String sRiskDetermination;
	private String sBaseline;
	private String sRiskManagement;
	private String sResilience;
	private String sPairResilience;
	private String sSystemResilience;
	private String sSystemResilienceSummary;
	private String sFRI1;
	private String sFRI2;
	private String sFRI3;
	private String sFRI4;
	private String sFRI5;
	private String sORI1;
	private String sORI2;
	private String sORI3;
	private String sORI4;
	private String sORI5;
	private String sORI6;
	private String sORI7;
	private String sAppendix;
	
	private List<ReportAssessmentTaskDTO> tasks;
	private List<ReportInvestmentOptionDTO> investOptions;
	
	private List<RptAssetDTO> assets;
	private List<RptThreatDTO> threats;
	private List<RptCountermeasureDTO> countermeasures;
	private List<RptConsequenceDTO> consequences;
	private List<RptFatalitiesDTO> fatalities;
	private List<RptInjuriesDTO> injuries;
	private List<RptVulnerabilityDTO> vulnerabilities;
	private List<RptThreatLikelihoodDTO> threatLikehoods;
	private List<RptRiskDTO> risks;
	
	private List<RptRankingsDTO> rankings;
	private List<RptAssessmentDTO> assessments;
	private List<RptResilienceDTO> resilience;
	private List<RptTop20Risks> topRisks;
	private List<RptNaturalThreatsDTO> naturalThreats;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Date getCreatedTime() {
		return createdTime;
	}
	public void setCreatedTime(Date createdTime) {
		this.createdTime = createdTime;
	}
	
	
	public String getExeSummary() {
		return exeSummary;
	}
	public void setExeSummary(String exeSummary) {
		this.exeSummary = exeSummary;
	}
	public String getAssesTeam() {
		return assesTeam;
	}
	public void setAssesTeam(String assesTeam) {
		this.assesTeam = assesTeam;
	}
	public String getAssesTeamOf() {
		return assesTeamOf;
	}
	public void setAssesTeamOf(String assesTeamOf) {
		this.assesTeamOf = assesTeamOf;
	}
	public String getAssesApproach() {
		return assesApproach;
	}
	public void setAssesApproach(String assesApproach) {
		this.assesApproach = assesApproach;
	}
	public String getAssesTasks() {
		return assesTasks;
	}
	public void setAssesTasks(String assesTasks) {
		this.assesTasks = assesTasks;
	}
	public String getWorkSessions() {
		return workSessions;
	}
	public void setWorkSessions(String workSessions) {
		this.workSessions = workSessions;
	}
	public String getDescUtility() {
		return descUtility;
	}
	public void setDescUtility(String descUtility) {
		this.descUtility = descUtility;
	}
	public String getMissionStmt() {
		return missionStmt;
	}
	public void setMissionStmt(String missionStmt) {
		this.missionStmt = missionStmt;
	}
	public String getCriticalAssets() {
		return criticalAssets;
	}
	public void setCriticalAssets(String criticalAssets) {
		this.criticalAssets = criticalAssets;
	}
	public String getAssetThreatPairs() {
		return assetThreatPairs;
	}
	public void setAssetThreatPairs(String assetThreatPairs) {
		this.assetThreatPairs = assetThreatPairs;
	}
	public String getCalConsequences() {
		return calConsequences;
	}
	public void setCalConsequences(String calConsequences) {
		this.calConsequences = calConsequences;
	}
	public String getExpertElicitation() {
		return expertElicitation;
	}
	public void setExpertElicitation(String expertElicitation) {
		this.expertElicitation = expertElicitation;
	}
	public String getPathAnalysisDisc() {
		return pathAnalysisDisc;
	}
	public void setPathAnalysisDisc(String pathAnalysisDisc) {
		this.pathAnalysisDisc = pathAnalysisDisc;
	}
	public String getTreeEventDisc() {
		return treeEventDisc;
	}
	public void setTreeEventDisc(String treeEventDisc) {
		this.treeEventDisc = treeEventDisc;
	}
	public String getBestEstimate() {
		return bestEstimate;
	}
	public void setBestEstimate(String bestEstimate) {
		this.bestEstimate = bestEstimate;
	}
	public String getProxyAnalysis() {
		return proxyAnalysis;
	}
	public void setProxyAnalysis(String proxyAnalysis) {
		this.proxyAnalysis = proxyAnalysis;
	}
	public String getCondLikelihood() {
		return condLikelihood;
	}
	public void setCondLikelihood(String condLikelihood) {
		this.condLikelihood = condLikelihood;
	}
	public String getRiskManagement() {
		return riskManagement;
	}
	public void setRiskManagement(String riskManagement) {
		this.riskManagement = riskManagement;
	}
	public String getRiskReductionOpt() {
		return riskReductionOpt;
	}
	public void setRiskReductionOpt(String riskReductionOpt) {
		this.riskReductionOpt = riskReductionOpt;
	}
	
	public String getsIntroduction() {
		return sIntroduction;
	}
	public void setsIntroduction(String sIntroduction) {
		this.sIntroduction = sIntroduction;
	}
	public String getsRiskAssessment() {
		return sRiskAssessment;
	}
	public void setsRiskAssessment(String sRiskAssessment) {
		this.sRiskAssessment = sRiskAssessment;
	}
	public String getsComplaintAssessment() {
		return sComplaintAssessment;
	}
	public void setsComplaintAssessment(String sComplaintAssessment) {
		this.sComplaintAssessment = sComplaintAssessment;
	}
	public String getsSafetyAct() {
		return sSafetyAct;
	}
	public void setsSafetyAct(String sSafetyAct) {
		this.sSafetyAct = sSafetyAct;
	}
	public String getsSupportTool() {
		return sSupportTool;
	}
	public void setsSupportTool(String sSupportTool) {
		this.sSupportTool = sSupportTool;
	}
	public String getsAssessmentTeamMembers() {
		return sAssessmentTeamMembers;
	}
	public void setsAssessmentTeamMembers(String sAssessmentTeamMembers) {
		this.sAssessmentTeamMembers = sAssessmentTeamMembers;
	}
	public String getsAssessmentProcess() {
		return sAssessmentProcess;
	}
	public void setsAssessmentProcess(String sAssessmentProcess) {
		this.sAssessmentProcess = sAssessmentProcess;
	}
	public String getsMissionStatement() {
		return sMissionStatement;
	}
	public void setsMissionStatement(String sMissionStatement) {
		this.sMissionStatement = sMissionStatement;
	}
	public String getsCriticalAssets() {
		return sCriticalAssets;
	}
	public void setsCriticalAssets(String sCriticalAssets) {
		this.sCriticalAssets = sCriticalAssets;
	}
	public String getsProbableThreats() {
		return sProbableThreats;
	}
	public void setsProbableThreats(String sProbableThreats) {
		this.sProbableThreats = sProbableThreats;
	}
	public String getsAssetThreatPairs() {
		return sAssetThreatPairs;
	}
	public void setsAssetThreatPairs(String sAssetThreatPairs) {
		this.sAssetThreatPairs = sAssetThreatPairs;
	}
	public String getsCountermeasure() {
		return sCountermeasure;
	}
	public void setsCountermeasure(String sCountermeasure) {
		this.sCountermeasure = sCountermeasure;
	}
	public String getsCalOfConsequences() {
		return sCalOfConsequences;
	}
	public void setsCalOfConsequences(String sCalOfConsequences) {
		this.sCalOfConsequences = sCalOfConsequences;
	}
	public String getsStatisticalValue() {
		return sStatisticalValue;
	}
	public void setsStatisticalValue(String sStatisticalValue) {
		this.sStatisticalValue = sStatisticalValue;
	}
	public String getsAssetVulnerabilities() {
		return sAssetVulnerabilities;
	}
	public void setsAssetVulnerabilities(String sAssetVulnerabilities) {
		this.sAssetVulnerabilities = sAssetVulnerabilities;
	}
	public String getsPathAnalysisDiscussion() {
		return sPathAnalysisDiscussion;
	}
	public void setsPathAnalysisDiscussion(String sPathAnalysisDiscussion) {
		this.sPathAnalysisDiscussion = sPathAnalysisDiscussion;
	}
	public String getsEventTreeDiscussion() {
		return sEventTreeDiscussion;
	}
	public void setsEventTreeDiscussion(String sEventTreeDiscussion) {
		this.sEventTreeDiscussion = sEventTreeDiscussion;
	}
	public String getsThreatLikelihood() {
		return sThreatLikelihood;
	}
	public void setsThreatLikelihood(String sThreatLikelihood) {
		this.sThreatLikelihood = sThreatLikelihood;
	}
	public String getsBestEstimate() {
		return sBestEstimate;
	}
	public void setsBestEstimate(String sBestEstimate) {
		this.sBestEstimate = sBestEstimate;
	}
	public String getsProxyAnalysis() {
		return sProxyAnalysis;
	}
	public void setsProxyAnalysis(String sProxyAnalysis) {
		this.sProxyAnalysis = sProxyAnalysis;
	}
	public String getsRiskDetermination() {
		return sRiskDetermination;
	}
	public void setsRiskDetermination(String sRiskDetermination) {
		this.sRiskDetermination = sRiskDetermination;
	}
	public String getsBaseline() {
		return sBaseline;
	}
	public void setsBaseline(String sBaseline) {
		this.sBaseline = sBaseline;
	}
	public String getsRiskManagement() {
		return sRiskManagement;
	}
	public void setsRiskManagement(String sRiskManagement) {
		this.sRiskManagement = sRiskManagement;
	}
	public String getsResilience() {
		return sResilience;
	}
	public void setsResilience(String sResilience) {
		this.sResilience = sResilience;
	}
	public String getsPairResilience() {
		return sPairResilience;
	}
	public void setsPairResilience(String sPairResilience) {
		this.sPairResilience = sPairResilience;
	}
	public String getsSystemResilience() {
		return sSystemResilience;
	}
	public void setsSystemResilience(String sSystemResilience) {
		this.sSystemResilience = sSystemResilience;
	}
	public String getsSystemResilienceSummary() {
		return sSystemResilienceSummary;
	}
	public void setsSystemResilienceSummary(String sSystemResilienceSummary) {
		this.sSystemResilienceSummary = sSystemResilienceSummary;
	}
	public String getsFRI1() {
		return sFRI1;
	}
	public void setsFRI1(String sFRI1) {
		this.sFRI1 = sFRI1;
	}
	public String getsFRI2() {
		return sFRI2;
	}
	public void setsFRI2(String sFRI2) {
		this.sFRI2 = sFRI2;
	}
	public String getsFRI3() {
		return sFRI3;
	}
	public void setsFRI3(String sFRI3) {
		this.sFRI3 = sFRI3;
	}
	public String getsFRI4() {
		return sFRI4;
	}
	public void setsFRI4(String sFRI4) {
		this.sFRI4 = sFRI4;
	}
	public String getsFRI5() {
		return sFRI5;
	}
	public void setsFRI5(String sFRI5) {
		this.sFRI5 = sFRI5;
	}
	public String getsORI1() {
		return sORI1;
	}
	public void setsORI1(String sORI1) {
		this.sORI1 = sORI1;
	}
	public String getsORI2() {
		return sORI2;
	}
	public void setsORI2(String sORI2) {
		this.sORI2 = sORI2;
	}
	public String getsORI3() {
		return sORI3;
	}
	public void setsORI3(String sORI3) {
		this.sORI3 = sORI3;
	}
	public String getsORI4() {
		return sORI4;
	}
	public void setsORI4(String sORI4) {
		this.sORI4 = sORI4;
	}
	public String getsORI5() {
		return sORI5;
	}
	public void setsORI5(String sORI5) {
		this.sORI5 = sORI5;
	}
	public String getsORI6() {
		return sORI6;
	}
	public void setsORI6(String sORI6) {
		this.sORI6 = sORI6;
	}
	public String getsORI7() {
		return sORI7;
	}
	public void setsORI7(String sORI7) {
		this.sORI7 = sORI7;
	}
	public String getsAppendix() {
		return sAppendix;
	}
	public void setsAppendix(String sAppendix) {
		this.sAppendix = sAppendix;
	}
	public List<ReportAssessmentTaskDTO> getTasks() {
		return tasks;
	}
	public void setTasks(List<ReportAssessmentTaskDTO> tasks) {
		this.tasks = tasks;
	}
	public List<ReportInvestmentOptionDTO> getInvestOptions() {
		return investOptions;
	}
	public void setInvestOptions(List<ReportInvestmentOptionDTO> investOptions) {
		this.investOptions = investOptions;
	}
	public List<RptAssetDTO> getAssets() {
		return assets;
	}
	public void setAssets(List<RptAssetDTO> assets) {
		this.assets = assets;
	}
	public List<RptThreatDTO> getThreats() {
		return threats;
	}
	public void setThreats(List<RptThreatDTO> threats) {
		this.threats = threats;
	}
	public List<RptCountermeasureDTO> getCountermeasures() {
		return countermeasures;
	}
	public void setCountermeasures(List<RptCountermeasureDTO> countermeasures) {
		this.countermeasures = countermeasures;
	}
	public List<RptConsequenceDTO> getConsequences() {
		return consequences;
	}
	public void setConsequences(List<RptConsequenceDTO> consequences) {
		this.consequences = consequences;
	}
	public List<RptFatalitiesDTO> getFatalities() {
		return fatalities;
	}
	public void setFatalities(List<RptFatalitiesDTO> fatalities) {
		this.fatalities = fatalities;
	}
	public List<RptInjuriesDTO> getInjuries() {
		return injuries;
	}
	public void setInjuries(List<RptInjuriesDTO> injuries) {
		this.injuries = injuries;
	}
	public List<RptVulnerabilityDTO> getVulnerabilities() {
		return vulnerabilities;
	}
	public void setVulnerabilities(List<RptVulnerabilityDTO> vulnerabilities) {
		this.vulnerabilities = vulnerabilities;
	}
	public List<RptThreatLikelihoodDTO> getThreatLikehoods() {
		return threatLikehoods;
	}
	public void setThreatLikehoods(List<RptThreatLikelihoodDTO> threatLikehoods) {
		this.threatLikehoods = threatLikehoods;
	}
	public List<RptRiskDTO> getRisks() {
		return risks;
	}
	public void setRisks(List<RptRiskDTO> risks) {
		this.risks = risks;
	}
	public List<RptAssessmentDTO> getAssessments() {
		return assessments;
	}
	public void setAssessments(List<RptAssessmentDTO> assessments) {
		this.assessments = assessments;
	}
	public List<RptRankingsDTO> getRankings() {
		return rankings;
	}
	public void setRankings(List<RptRankingsDTO> rankings) {
		this.rankings = rankings;
	}
	public List<RptResilienceDTO> getResilience() {
		return resilience;
	}
	public void setResilience(List<RptResilienceDTO> resilience) {
		this.resilience = resilience;
	}
	public List<RptTop20Risks> getTopRisks() {
		return topRisks;
	}
	public void setTopRisks(List<RptTop20Risks> topRisks) {
		this.topRisks = topRisks;
	}
	public List<RptNaturalThreatsDTO> getNaturalThreats() {
		return naturalThreats;
	}
	public void setNaturalThreats(List<RptNaturalThreatsDTO> naturalThreats) {
		this.naturalThreats = naturalThreats;
	}
	@Override
	public String toString() {
		return "RptMainDTO [exeSummary=" + exeSummary + ", assesTeam="
				+ assesTeam + ", assesTeamOf=" + assesTeamOf
				+ ", assesApproach=" + assesApproach + ", assesTasks="
				+ assesTasks + ", workSessions=" + workSessions
				+ ", descUtility=" + descUtility + "]";
	}
   
	/*@Override
	public String toString() {
		return "RptMainDTO [sIntroduction=" + sIntroduction
				+ ", sRiskAssessment=" + sRiskAssessment
				+ ", sComplaintAssessment=" + sComplaintAssessment
				+ ", sSafetyAct=" + sSafetyAct + ", sSupportTool="
				+ sSupportTool + ", sAssessmentTeamMembers="
				+ sAssessmentTeamMembers + ", sAssessmentProcess="
				+ sAssessmentProcess + ", sMissionStatement="
				+ sMissionStatement + "]";
	}*/
	
	
	
}
