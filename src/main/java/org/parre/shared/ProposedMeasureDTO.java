/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.shared;


import java.math.BigDecimal;

import org.parre.shared.types.CounterMeasureType;

/**
 * The Class ProposedMeasureDTO.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 1/19/12
 */
public class ProposedMeasureDTO extends NameDescriptionDTO {
    private Long parreAnalysisId;
    private CounterMeasureType type;
    private AmountDTO capitalCostDTO = AmountDTO.createZero();
    private AmountDTO oAndMCostPerYearDTO = AmountDTO.createZero();
    private Integer effectiveLife = 0;
    private AmountDTO salvageValueDTO = AmountDTO.createZero();
    private BigDecimal inflationRate = BigDecimal.ZERO;

    public ProposedMeasureDTO() {
    }

    public ProposedMeasureDTO(String name, String description) {
        super(name, description);
    }

    public ProposedMeasureDTO(Long parreAnalysisId) {
        this.parreAnalysisId = parreAnalysisId;
    }

    public Long getParreAnalysisId() {
        return parreAnalysisId;
    }

    public void setParreAnalysisId(Long parreAnalysisId) {
        this.parreAnalysisId = parreAnalysisId;
    }

    public CounterMeasureType getType() {
        return type;
    }

    public void setType(CounterMeasureType type) {
        this.type = type;
    }

    public AmountDTO getCapitalCostDTO() {
        return capitalCostDTO;
    }

    public BigDecimal getCapitalCostQuantity() {
        return capitalCostDTO.getQuantity();
    }

    public void setCapitalCostDTO(AmountDTO capitalCostDTO) {
        this.capitalCostDTO = capitalCostDTO;
    }

    public AmountDTO getoAndMCostPerYearDTO() {
        return oAndMCostPerYearDTO;
    }

    public void setoAndMCostPerYearDTO(AmountDTO oAndMCostPerYearDTO) {
        this.oAndMCostPerYearDTO = oAndMCostPerYearDTO;
    }

    public Integer getEffectiveLife() {
        return effectiveLife;
    }

    public void setEffectiveLife(Integer effectiveLife) {
        this.effectiveLife = effectiveLife;
    }

    public AmountDTO getSalvageValueDTO() {
        return salvageValueDTO;
    }

    public void setSalvageValueDTO(AmountDTO salvageValueDTO) {
        this.salvageValueDTO = salvageValueDTO;
    }

    public BigDecimal getSalvageValueQuantity() {
        return salvageValueDTO.getQuantity();
    }

    public BigDecimal getInflationRate() {
        return inflationRate;
    }

    public void setInflationRate(BigDecimal inflationRate) {
        this.inflationRate = inflationRate;
    }
}
