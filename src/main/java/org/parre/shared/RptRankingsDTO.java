/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.shared;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Comparator;

/**
 * @author Raju Kanumuri
 * @date 10/30/2013
 */
public class RptRankingsDTO extends BaseDTO implements Serializable, Comparable<RptRankingsDTO>{
  
	private static final long serialVersionUID = -1545253553446532700L;
	private String asset;
    private String threat;
    private Long assetThreatId;
    private BigDecimal risk;
    private BigDecimal resilience;
    private BigDecimal fatality;
    private BigDecimal injury;
    private BigDecimal impact;
    private Integer riskRank;
    private Integer resilienceRank;
    private Integer fatalityRank;
    private Integer injuryRank;
    private Integer impactRank;
    
    
    
    public RptRankingsDTO(String asset, String threat, Long assetThreatId, BigDecimal risk, Integer riskRank, BigDecimal resilience, Integer resilienceRank, BigDecimal fatality, Integer fatalityRank, BigDecimal injury, Integer injuryRank,  BigDecimal impact, Integer impactRank){
		this.asset=asset;
		this.threat=threat;
		this.assetThreatId=assetThreatId;
		this.risk=risk;
		this.riskRank =riskRank;		
		this.resilience=resilience;
		this.resilienceRank=resilienceRank;
		this.fatality=fatality;
		this.fatalityRank=fatalityRank;
		this.injury=injury;
		this.injuryRank=injuryRank;
		this.impact=impact;
		this.impactRank=impactRank;
			
	}



	public RptRankingsDTO(String asset, String threat, Long assetThreatId,BigDecimal risk, BigDecimal resilience, BigDecimal fatality, BigDecimal injury, BigDecimal impact) {
		this.asset=asset;
		this.threat=threat;
		this.assetThreatId=assetThreatId;
		this.risk=risk;
		this.resilience=resilience;
		this.fatality=fatality;
		this.injury=injury;
		this.impact=impact;
	}
	
	public RptRankingsDTO(){
	}

	public String getAsset() {
		return asset;
	}



	public void setAsset(String asset) {
		this.asset = asset;
	}



	public String getThreat() {
		return threat;
	}



	public void setThreat(String threat) {
		this.threat = threat;
	}



	public Long getAssetThreatId() {
		return assetThreatId;
	}



	public void setAssetThreatId(Long assetThreatId) {
		this.assetThreatId = assetThreatId;
	}



	public BigDecimal getRisk() {
		return risk;
	}



	public void setRisk(BigDecimal risk) {
		this.risk = risk;
	}



	public BigDecimal getResilience() {
		return resilience;
	}



	public void setResilience(BigDecimal resilience) {
		this.resilience = resilience;
	}



	public BigDecimal getFatality() {
		return fatality;
	}



	public void setFatality(BigDecimal fatality) {
		this.fatality = fatality;
	}



	public BigDecimal getInjury() {
		return injury;
	}



	public void setInjury(BigDecimal injury) {
		this.injury = injury;
	}



	public BigDecimal getImpact() {
		return impact;
	}



	public void setImpact(BigDecimal impact) {
		this.impact = impact;
	}



	public Integer getRiskRank() {
		return riskRank;
	}



	public void setRiskRank(Integer riskRank) {
		this.riskRank = riskRank;
	}



	public Integer getResilienceRank() {
		return resilienceRank;
	}



	public void setResilienceRank(Integer resilienceRank) {
		this.resilienceRank = resilienceRank;
	}



	public Integer getFatalityRank() {
		return fatalityRank;
	}



	public void setFatalityRank(Integer fatalityRank) {
		this.fatalityRank = fatalityRank;
	}



	public Integer getInjuryRank() {
		return injuryRank;
	}



	public void setInjuryRank(Integer injuryRank) {
		this.injuryRank = injuryRank;
	}



	public Integer getImpactRank() {
		return impactRank;
	}



	public void setImpactRank(Integer impactRank) {
		this.impactRank = impactRank;
	}

	@Override
	public int compareTo(RptRankingsDTO o) {
		return Comparators.RISK.compare(this, o);
	}
	
	public static class Comparators {
		public static Comparator<RptRankingsDTO> RISK = new Comparator<RptRankingsDTO>() {
            @Override
            public int compare(RptRankingsDTO o1, RptRankingsDTO o2) {
                return o2.risk.compareTo(o1.risk);
            }
        };
        
        public static Comparator<RptRankingsDTO> RESILIENCE = new Comparator<RptRankingsDTO>() {
            @Override
            public int compare(RptRankingsDTO o1, RptRankingsDTO o2) {
                return o2.resilience.compareTo(o1.resilience);
            }
        };
        
        public static Comparator<RptRankingsDTO> FATALITY = new Comparator<RptRankingsDTO>() {
            @Override
            public int compare(RptRankingsDTO o1, RptRankingsDTO o2) {
                return o2.fatality.compareTo(o1.fatality);
            }
        };
        
        public static Comparator<RptRankingsDTO> INJURY = new Comparator<RptRankingsDTO>() {
            @Override
            public int compare(RptRankingsDTO o1, RptRankingsDTO o2) {
                return o2.injury.compareTo(o1.injury);
            }
        };
        
        public static Comparator<RptRankingsDTO> IMPACT = new Comparator<RptRankingsDTO>() {
            @Override
            public int compare(RptRankingsDTO o1, RptRankingsDTO o2) {
                return o2.impact.compareTo(o1.impact);
            }
        };
	}
	
	

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((asset == null) ? 0 : asset.hashCode());
		result = prime * result
				+ ((assetThreatId == null) ? 0 : assetThreatId.hashCode());
		result = prime * result
				+ ((fatality == null) ? 0 : fatality.hashCode());
		result = prime * result
				+ ((fatalityRank == null) ? 0 : fatalityRank.hashCode());
		result = prime * result + ((impact == null) ? 0 : impact.hashCode());
		result = prime * result
				+ ((impactRank == null) ? 0 : impactRank.hashCode());
		result = prime * result + ((injury == null) ? 0 : injury.hashCode());
		result = prime * result
				+ ((injuryRank == null) ? 0 : injuryRank.hashCode());
		result = prime * result
				+ ((resilience == null) ? 0 : resilience.hashCode());
		result = prime * result
				+ ((resilienceRank == null) ? 0 : resilienceRank.hashCode());
		result = prime * result + ((risk == null) ? 0 : risk.hashCode());
		result = prime * result
				+ ((riskRank == null) ? 0 : riskRank.hashCode());
		result = prime * result + ((threat == null) ? 0 : threat.hashCode());
		return result;
	}



	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		RptRankingsDTO other = (RptRankingsDTO) obj;
		if (asset == null) {
			if (other.asset != null)
				return false;
		} else if (!asset.equals(other.asset))
			return false;
		if (assetThreatId == null) {
			if (other.assetThreatId != null)
				return false;
		} else if (!assetThreatId.equals(other.assetThreatId))
			return false;
		if (fatality == null) {
			if (other.fatality != null)
				return false;
		} else if (!fatality.equals(other.fatality))
			return false;
		if (fatalityRank == null) {
			if (other.fatalityRank != null)
				return false;
		} else if (!fatalityRank.equals(other.fatalityRank))
			return false;
		if (impact == null) {
			if (other.impact != null)
				return false;
		} else if (!impact.equals(other.impact))
			return false;
		if (impactRank == null) {
			if (other.impactRank != null)
				return false;
		} else if (!impactRank.equals(other.impactRank))
			return false;
		if (injury == null) {
			if (other.injury != null)
				return false;
		} else if (!injury.equals(other.injury))
			return false;
		if (injuryRank == null) {
			if (other.injuryRank != null)
				return false;
		} else if (!injuryRank.equals(other.injuryRank))
			return false;
		if (resilience == null) {
			if (other.resilience != null)
				return false;
		} else if (!resilience.equals(other.resilience))
			return false;
		if (resilienceRank == null) {
			if (other.resilienceRank != null)
				return false;
		} else if (!resilienceRank.equals(other.resilienceRank))
			return false;
		if (risk == null) {
			if (other.risk != null)
				return false;
		} else if (!risk.equals(other.risk))
			return false;
		if (riskRank == null) {
			if (other.riskRank != null)
				return false;
		} else if (!riskRank.equals(other.riskRank))
			return false;
		if (threat == null) {
			if (other.threat != null)
				return false;
		} else if (!threat.equals(other.threat))
			return false;
		return true;
	}



	@Override
	public String toString() {
		return "RptRankingsDTO [asset=" + asset + ", threat=" + threat + ", assetThreatId =" + assetThreatId
				+ ", riskRank=" + riskRank + ", resilienceRank="
				+ resilienceRank + ", fatalityRank=" + fatalityRank
				+ ", injuryRank=" + injuryRank + ", impactRank=" + impactRank
				+ "]";
	}
}
