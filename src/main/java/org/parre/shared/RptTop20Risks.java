/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.shared;

import java.util.Comparator;

/**
 * @author Raju Kanumuri
 * @date 11/04/2013
 */
public class RptTop20Risks extends BaseDTO implements Comparable<RptTop20Risks> {
    private String asset;
    private String threat;
    private String threatClass;
    private String fatalities;
    private String injuries;
    private String finImpact;
    private String finTotal;
    private String vulnerability;
    private String likelihood;
    private String risk;
    private String finResilience;
   
    
    public RptTop20Risks(String asset, String threatClass, String threat, String fatalities, String injuries,String finImpact,String finTotal,String vulnerability,String likelihood,String risk,String finResilience){
		this.asset=asset;
		this.threatClass= threatClass;
		this.threat=threat;
		this.fatalities=fatalities;
		this.injuries=injuries;
		this.finImpact=finImpact;
		this.finTotal=finTotal;
		this.vulnerability=vulnerability;
		this.likelihood=likelihood;
		this.risk=risk;
		this.finResilience=finResilience;
	}

	public String getAsset() {
		return asset;
	}

	public void setAsset(String asset) {
		this.asset = asset;
	}

	public String getThreat() {
		return threat;
	}

	public void setThreat(String threat) {
		this.threat = threat;
	}

	public String getFatalities() {
		return fatalities;
	}

	public void setFatalities(String fatalities) {
		this.fatalities = fatalities;
	}

	public String getInjuries() {
		return injuries;
	}

	public void setInjuries(String injuries) {
		this.injuries = injuries;
	}

	public String getFinImpact() {
		return finImpact;
	}

	public void setFinImpact(String finImpact) {
		this.finImpact = finImpact;
	}

	public String getFinTotal() {
		return finTotal;
	}

	public void setFinTotal(String finTotal) {
		this.finTotal = finTotal;
	}

	public String getVulnerability() {
		return vulnerability;
	}

	public void setVulnerability(String vulnerability) {
		this.vulnerability = vulnerability;
	}

	public String getLikelihood() {
		return likelihood;
	}

	public void setLikelihood(String likelihood) {
		this.likelihood = likelihood;
	}

	public String getRisk() {
		return risk;
	}

	public void setRisk(String risk) {
		this.risk = risk;
	}

	public String getFinResilience() {
		return finResilience;
	}

	public void setFinResilience(String finResilience) {
		this.finResilience = finResilience;
	}

	public String getThreatClass() {
		return threatClass;
	}

	public void setThreatClass(String threatClass) {
		this.threatClass = threatClass;
	}

	@Override
	public int compareTo(RptTop20Risks o) {
		return Comparators.RISK.compare(this, o);
	}
	
	public static class Comparators {
		public static Comparator<RptTop20Risks> RISK = new Comparator<RptTop20Risks>() {
            @Override
            public int compare(RptTop20Risks o1, RptTop20Risks o2) {
                return o2.risk.compareTo(o1.risk);
            }
        };
	}

	
}
