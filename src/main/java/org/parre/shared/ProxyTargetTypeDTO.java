/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.shared;

import java.math.BigDecimal;

/**
 * User: Daniel
 * Date: 1/10/12
 * Time: 4:04 PM
*/
public class ProxyTargetTypeDTO extends BaseDTO {
    private Integer code;
    private String name;
    private BigDecimal probability;


    public ProxyTargetTypeDTO() {
    }


    public ProxyTargetTypeDTO(Long id) {
        setId(id);
    }

    public ProxyTargetTypeDTO(Integer code, String name, BigDecimal probability) {
        this.code = code;
        this.name = name;
        this.probability = probability;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public BigDecimal getProbability() {
        return probability;
    }

    public void setProbability(BigDecimal probability) {
        this.probability = probability;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("ProxyTargetTypeDTO");
        sb.append("{code=").append(code);
        sb.append(", name='").append(name).append('\'');
        sb.append(", probability=").append(probability);
        sb.append('}');
        return sb.toString();
    }
}
