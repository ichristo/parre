/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.shared;

/**
 * User: keithjones
 * Date: 6/28/12
 * Time: 2:40 PM
*/
public class UserPreferencesDTO extends BaseDTO {
    private boolean useScientificNotation;

    public boolean isUseScientificNotation() {
        return useScientificNotation;
    }

    public void setUseScientificNotation(boolean useScientificNotation) {
        this.useScientificNotation = useScientificNotation;
    }

    public void setUseScientificNotation(String bool) {
        if(bool.equals("true")) {
            setUseScientificNotation(true);
        }
        if(bool.equals("false")) {
            setUseScientificNotation(false);
        }
    }

    public boolean getUseScientificNotation() {
        return useScientificNotation;
    }

    @Override
    public String toString() {
        return "UserPreferencesDTO{" +
                "useScientificNotation=" + useScientificNotation +
                '}';
    }
}
