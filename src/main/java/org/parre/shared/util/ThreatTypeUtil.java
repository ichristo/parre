/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.shared.util;

import java.util.Arrays;
import java.util.List;

/**
 * The Class ThreatTypeUtil.
 */
public class ThreatTypeUtil {
	public static List<String> NATURAL_THREAT_NAMES = Arrays.asList("N(E)", "N(F)", "N(H)", "N(I)", "N(T)", "N(W)");
	
	public static boolean isNaturalThreat(String threatName) {
		return threatName != null &&
				NATURAL_THREAT_NAMES.contains(threatName);
	}
	
	 public static String removeCommas(String str) {
	        if(str != null && str.length() > 0 && str.charAt(0) == '-') {
	            return "-" + str.replaceAll("[^0-9\\.]+", "");
	        }
	    	return str == null ? null : str.replaceAll("[^0-9\\.]+", "");
	    }
}
