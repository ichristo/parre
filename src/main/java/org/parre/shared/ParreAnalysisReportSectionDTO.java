/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.shared;

import java.io.Serializable;

public class ParreAnalysisReportSectionDTO implements Serializable {
	private static final long serialVersionUID = -4591452651128485282L;
	
	private Long reportId;
	private Long reportSectionLookupId;
	private String userInput;
	private Boolean omitted = false;
	
	public ParreAnalysisReportSectionDTO() {
	}

	public ParreAnalysisReportSectionDTO(Long reportId,
			Long reportSectionLookupId, String userInput, Boolean omitted) {
		super();
		this.reportId = reportId;
		this.reportSectionLookupId = reportSectionLookupId;
		this.userInput = userInput;
		this.omitted = omitted;
	}

	public Long getReportId() {
		return reportId;
	}

	public void setReportId(Long reportId) {
		this.reportId = reportId;
	}

	public Long getReportSectionLookupId() {
		return reportSectionLookupId;
	}

	public void setReportSectionLookupId(Long reportSectionLookupId) {
		this.reportSectionLookupId = reportSectionLookupId;
	}

	public String getUserInput() {
		return userInput;
	}

	public void setUserInput(String userInput) {
		this.userInput = userInput;
	}

	public Boolean getOmitted() {
		return omitted;
	}

	public void setOmitted(Boolean omitted) {
		this.omitted = omitted;
	}

	@Override
	public String toString() {
		return "ParreAnalysisReportSectionDTO [reportId=" + reportId
				+ ", reportSectionLookupId=" + reportSectionLookupId
				+ ", userInput=" + userInput + ", omitted=" + omitted + "]";
	}
	
}
