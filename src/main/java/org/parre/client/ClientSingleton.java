/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client;

import com.google.gwt.event.shared.EventBus;
import com.google.gwt.event.shared.SimpleEventBus;

import java.util.*;

import org.parre.client.event.CurrentAnalysisChangedEvent;
import org.parre.client.nav.ParreNavAdapter;
import org.parre.client.util.ClientLoggingUtil;
import org.parre.client.util.nav.NavAdapter;
import org.parre.client.util.nav.NavEvent;
import org.parre.client.util.nav.NavEventHandler;
import org.parre.shared.*;


/**
 * The Class ClientSingleton.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 11/28/11
 */
public class ClientSingleton {
    private static final EventBus eventBus = new SimpleEventBus();
    private static AppInitData appInitData;
    private static LoginInfo loginInfo;
    private static String startView;
    private static String entryPointHash;
    private static NavEvent entryPointNavEvent;
    private static NavEventHandler navEventHandler;
    private static NavAdapter navAdapter = new ParreNavAdapter();

    public static NavAdapter getNavAdapter() {
        return navAdapter;
    }

    public static void setNavEventHandler(NavEventHandler navEventHandler) {
        ClientSingleton.navEventHandler = navEventHandler;
    }

    public static void setStartView(String startView) {
        ClientSingleton.startView = startView;
    }

    public static EventBus getEventBus() {
        return eventBus;
    }

    public static void setAppInitData(AppInitData appInitData) {
        ClientSingleton.appInitData = appInitData;
        getEventBus().fireEvent(CurrentAnalysisChangedEvent.create());
    }

    public static void setParreAnalysisDTO(ParreAnalysisDTO updatedAnalysis) {
        getAppInitData().setParreAnalysisDTO(updatedAnalysis);
        getEventBus().fireEvent(CurrentAnalysisChangedEvent.create());
    }

    public static AppInitData getAppInitData() {
        return appInitData;
    }

    public static LoginInfo getLoginInfo() {
        return loginInfo;
    }

    public static void setLoginInfo(LoginInfo loginInfo) {
        ClientSingleton.loginInfo = loginInfo;
    }

    public static Long getParreAnalysisId() {
        return getCurrentParreAnalysis().getId();
    }

    public static StatValuesDTO getStatValues() {
        return getCurrentParreAnalysis().getStatValuesDTO();
    }

    public static boolean hasCurrentParreAnalysis() {
        return getCurrentParreAnalysis() != null;
    }

    public static Long getBaselineId() {
        return getCurrentParreAnalysis().getBaselineId();
    }

    public static ParreAnalysisDTO getCurrentParreAnalysis() {
        return getAppInitData().getParreAnalysisDTO();
    }

    public static boolean isCurrentAnalysisLocked() {
        ParreAnalysisDTO currentParreAnalysis = getCurrentParreAnalysis();
        return currentParreAnalysis != null && currentParreAnalysis.isLocked();
    }

    public static boolean isCurrentAnalysisBaselined() {
        ParreAnalysisDTO currentParreAnalysis = getCurrentParreAnalysis();
        return currentParreAnalysis != null && currentParreAnalysis.isBaseline();
    }

    public static boolean hasBaseline() {
        return getAppInitData().hasBaseline();
    }

    public static boolean isLocked() {
        return getAppInitData().isLocked();
    }
    
    public static boolean isOption() {
        return getAppInitData().isOption();
    }
    
    public static Integer getDisplayOrder(){
    	return getAppInitData().getDisplayOrder();
    }
    
    public static boolean hasOptionAnalyses() {
        return getAppInitData().hasOptionAnalyses();
    }
    
    

    public static List<? extends NameDescriptionDTO> getRiskBenefitOptions() {
        return getCurrentParreAnalysis().getOptionParreAnalysisDTOs();
    }

    public static String getEntryPointHash() {
        return entryPointHash;
    }

    public static boolean hasEntryPointHash() {
        return entryPointHash != null && entryPointHash.length() > 0;
    }

    public static String getEntryPointHashValue() {
        return !hasEntryPointHash() ? "" : entryPointHash.substring(1);
    }

    public static void setEntryPointHash(String entryPointHash) {
        ClientSingleton.entryPointHash = entryPointHash;
    }

    public static void onNavEvent(NavEvent navEvent) {
        if (navEventHandler != null) {
            navEventHandler.onNavEvent(navEvent);
        }
        eventBus.fireEvent(navEvent);
    }

    public static void handleEntryPointNavEvent(NavEvent navEvent) {
        ClientLoggingUtil.info("EntryPoint Nav Event : " + navEvent.getClass());
        ClientSingleton.entryPointNavEvent = navEvent;
        onNavEvent(navEvent);
    }

    public static NavEvent getEntryPointNavEvent() {
        return entryPointNavEvent;
    }

    public static boolean enteredVia(Class<? extends NavEvent> navEventClass) {
        return entryPointNavEvent != null && entryPointNavEvent.getClass().equals(navEventClass);
    }

    public static void lockCurrentAnalysis() {
        getAppInitData().lockCurrentAnalysis();
        getEventBus().fireEvent(CurrentAnalysisChangedEvent.create());
    }

    public static void unlockCurrentAnalysis() {
        getAppInitData().unlockCurrentAnalysis();
        getEventBus().fireEvent(CurrentAnalysisChangedEvent.create());
    }
    
    public static List<LabelValueDTO> getStateList() {
    	return getAppInitData().getStateList();
    }

    public ReportSectionLookupDTO getReportSectionLookupById(Long reportSectionId) {
    	for (ReportSectionLookupDTO dto : getAppInitData().getReportSectionDTOs()) {
    		if (reportSectionId.equals(dto.getId())) {
    			return dto;
    		}
    	}
    	return null;
    }

}
