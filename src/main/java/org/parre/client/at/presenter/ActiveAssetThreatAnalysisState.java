/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.at.presenter;

import com.google.gwt.user.client.rpc.AsyncCallback;

import java.util.Set;

import org.parre.client.ClientSingleton;
import org.parre.client.messaging.ServiceFactory;
import org.parre.client.util.BaseAsyncCommand;
import org.parre.client.util.BusyHandler;
import org.parre.client.util.ExecutableAsyncCallback;
import org.parre.shared.AssetThreatLevelDTO;

/**
 * The Class ActiveAssetThreatAnalysisState.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 2/28/12
 */
public class ActiveAssetThreatAnalysisState implements AssetThreatAnalysisState {
    private AssetThreatMatrixPresenter.Display display;
    private BusyHandler busyHandler;

    public ActiveAssetThreatAnalysisState(AssetThreatMatrixPresenter.Display display) {
        this.display = display;
        busyHandler = new BusyHandler(this.display);
    }

    public void updateAssetThreatData(final Integer thresholdValue, final Set<AssetThreatLevelDTO> levelDTOs) {

        new ExecutableAsyncCallback<Integer>(busyHandler, new BaseAsyncCommand<Integer>() {
            public void execute(AsyncCallback<Integer> callback) {
                ServiceFactory.getInstance().getParreService().updateAssetThreatData
                        (thresholdValue, levelDTOs, ClientSingleton.getParreAnalysisId(), callback);
            }

            @Override
            public void handleResult(Integer result) {
                levelDTOs.clear();
                display.setAssetThreatDataUpdateButtonEnabled(false);
            }
        }).makeCall();
    }

}
