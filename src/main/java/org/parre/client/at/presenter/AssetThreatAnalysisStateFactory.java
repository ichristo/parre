/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.at.presenter;

import org.parre.client.common.presenter.AnalysisStateFactory;
import org.parre.client.util.View;


/**
 * A factory for creating AssetThreatAnalysisState objects.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 2/28/12
 */
public class AssetThreatAnalysisStateFactory implements AnalysisStateFactory<AssetThreatAnalysisState> {
    private static AnalysisStateFactory<AssetThreatAnalysisState> instance = new AssetThreatAnalysisStateFactory();

    public static AnalysisStateFactory<AssetThreatAnalysisState> getInstance() {
        return instance;
    }

    public AssetThreatAnalysisState createActiveAnalysisState(View view) {
        return new ActiveAssetThreatAnalysisState((AssetThreatMatrixPresenter.Display) view);
    }

    public AssetThreatAnalysisState createLockedAnalysisState(View view) {
        return new LockedAssetThreatAnalysisState((AssetThreatMatrixPresenter.Display) view);
    }
}
