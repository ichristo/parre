/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.at.view;

import com.google.gwt.cell.client.Cell;
import com.google.gwt.cell.client.EditTextCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.NativeEvent;
import com.google.gwt.dom.client.Style;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;
import com.google.gwt.user.cellview.client.Column;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import org.parre.client.ClientSingleton;
import org.parre.client.util.ActionSource;
import org.parre.client.util.ClientLoggingUtil;
import org.parre.client.util.DivStyleTemplate;
import org.parre.client.util.DoubleClickableTextCell;
import org.parre.client.util.dataview.DataColumn;
import org.parre.client.util.dataview.DataGridView;
import org.parre.client.util.dataview.DataProvider;
import org.parre.shared.AssetThreatLevelDTO;
import org.parre.shared.AssetThreatLevelsDTO;
import org.parre.shared.ThreatDTO;

/**
 * User: Swarn S. Dhaliwal
 * Date: 7/10/11
 * Time: 5:38 PM
*/
public class AssetThreatMatrixGrid extends DataGridView<AssetThreatLevelsDTO> {
    private static final int COLUMN_WIDTH = 60;
    private Integer assetThreatThreshold = 1;
    private ActionSource<AssetThreatLevelDTO> assetThreatUpdateSource = new ActionSource<AssetThreatLevelDTO>();
    private int widthPx;

    public AssetThreatMatrixGrid(DataProvider<AssetThreatLevelsDTO> dataProvider, List<ThreatDTO> threats) {
        super(dataProvider);
        widthPx = initColumns(threats);
    }

    public int getWidthPx() {
        return widthPx;
    }

    public void refreshColumns(List<ThreatDTO> threats) {
        clearGrid();
        ClientLoggingUtil.info("Number Of Columns : " + getDataGrid().getColumnCount());
        ClientLoggingUtil.info("Number of New Threats : " + threats.size());
        widthPx = initColumns(threats);
        ClientLoggingUtil.info("New Number Of Columns : " + getDataGrid().getColumnCount());
    }

    private int initColumns(List<ThreatDTO> threats) {
        ClientLoggingUtil.info("Creating Columns");
        List<DataColumn<AssetThreatLevelsDTO>> columnList = new ArrayList<DataColumn<AssetThreatLevelsDTO>>();
        columnList.add(createAssetIdColumn());
        for (ThreatDTO threat : threats) {
            columnList.add(createThreatLevelColumn(threat));
        }

        addColumns(columnList);
        int widthPx = Integer.valueOf(COLUMN_WIDTH + (COLUMN_WIDTH * threats.size()));
        ClientLoggingUtil.info("Adding Columns, setting width to : " + widthPx);
        String width = "100%";
        setWidth(width);
        return widthPx;
    }

    private DoubleClickableTextCell<AssetThreatLevelsDTO> createDoubleClickableTextCell() {
        return new DoubleClickableTextCell<AssetThreatLevelsDTO>(getSelectionModel(), getSelectionSource());
    }

    private DataColumn<AssetThreatLevelsDTO> createAssetIdColumn() {
        Column<AssetThreatLevelsDTO, String> column = new Column<AssetThreatLevelsDTO, String>(
                createDoubleClickableTextCell()) {
            @Override
            public String getValue(AssetThreatLevelsDTO object) {
                return object.getAssetId();
            }
        };
        column.setSortable(true);

        Comparator<AssetThreatLevelsDTO> comparator = new Comparator<AssetThreatLevelsDTO>() {
            public int compare(AssetThreatLevelsDTO o1, AssetThreatLevelsDTO o2) {
                return o1.getAssetId().compareTo(o2.getAssetId());
            }
        };
        return new DataColumn<AssetThreatLevelsDTO>("Asset", column, comparator, COLUMN_WIDTH, Style.Unit.PX);

    }

    private DataColumn<AssetThreatLevelsDTO> createThreatLevelColumn(final ThreatDTO threatDTO) {

        final EditTextCell cell = new EditTextCell();
        Column<AssetThreatLevelsDTO, String> column = new Column<AssetThreatLevelsDTO, String>(cell) {
            @Override
            public String getValue(AssetThreatLevelsDTO object) {
                Integer levelValue = getLevelValue(object, threatDTO);
                return String.valueOf(levelValue == null || levelValue == 0 ? "-" : levelValue);
            }

            @Override
            public void onBrowserEvent(Cell.Context context, Element elem, AssetThreatLevelsDTO object, NativeEvent event) {
                if (!ClientSingleton.isCurrentAnalysisLocked()) {
                    super.onBrowserEvent(context, elem, object, event);
                }
            }

            @Override
            public void render(Cell.Context context, AssetThreatLevelsDTO object, SafeHtmlBuilder sb) {
                if (!ClientSingleton.isCurrentAnalysisLocked() && cell.isEditing(context, null, getValue(object))) {
                    super.render(context, object, sb);
                } else {
                    Integer levelValue = getLevelValue(object, threatDTO);
                    try {
                        if (levelValue < assetThreatThreshold) {
                            sb.append(DivStyleTemplate.INSTANCE.render(getValue(object), "normalValueCell"));
                        } else {
                            sb.append(DivStyleTemplate.INSTANCE.render(getValue(object), "criticalValueCell"));
                        }
                    } catch (NullPointerException e) {
                        ClientLoggingUtil.info("levelValue = null in AssetThreatMatrixGrid.render");
                    }

                }
            }
        };
        column.setSortable(true);

        Comparator<AssetThreatLevelsDTO> comparator = new Comparator<AssetThreatLevelsDTO>() {
            public int compare(AssetThreatLevelsDTO o1, AssetThreatLevelsDTO o2) {
                Integer value1 = getLevelValue(o1, threatDTO);
                Integer value2 = getLevelValue(o2, threatDTO);
                return value1.compareTo(value2);
            }
        };
        column.setFieldUpdater(new FieldUpdater<AssetThreatLevelsDTO, String>() {
            public void update(int index, AssetThreatLevelsDTO object, String value) {
                try {
                    Integer levelValue = Integer.valueOf(value);
                    final AssetThreatLevelDTO levelDTO = new AssetThreatLevelDTO(object.getId(), threatDTO.getId(), levelValue);
                    assetThreatUpdateSource.dispatch(levelDTO);
                } catch (NumberFormatException e) {
                }
            }
        });

        return new DataColumn<AssetThreatLevelsDTO>(threatDTO.getName(), column, comparator, COLUMN_WIDTH, Style.Unit.PX);

    }

    private Integer getLevelValue(AssetThreatLevelsDTO object, ThreatDTO threatDTO) {
        Integer threatLevel = object.getThreatLevel(threatDTO.getId());
        return threatLevel == null ? 0 : threatLevel;
    }

    public ActionSource<AssetThreatLevelDTO> getAssetThreatUpdateSource() {
        return assetThreatUpdateSource;
    }

    public void setAssetThreatThreshold(Integer assetThreatThreshold) {
        this.assetThreatThreshold = assetThreatThreshold;
    }

}
