/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.at.view;

import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.event.logical.shared.HasCloseHandlers;
import com.google.gwt.user.client.ui.*;
import com.google.gwt.view.client.SingleSelectionModel;

import java.util.ArrayList;
import java.util.List;

import org.parre.client.ClientSingleton;
import org.parre.client.ParreClientUtil;
import org.parre.client.at.presenter.AssetThreatMatrixPresenter;
import org.parre.client.util.*;
import org.parre.client.util.dataview.DataProvider;
import org.parre.client.util.dataview.DataProviderFactory;
import org.parre.shared.AssetThreatLevelDTO;
import org.parre.shared.AssetThreatLevelsDTO;
import org.parre.shared.ThreatDTO;

/**
 * The Class ThreatAssetMatrixView.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 10/6/11
 */
public class ThreatAssetMatrixView extends BaseView implements AssetThreatMatrixPresenter.Display {
    private Widget viewWidget;
    private AssetThreatMatrixGrid matrixGrid;
    private List<HasText> assetLabels = new ArrayList<HasText>(3);
    private Button changeThresholdButton;
    private Label assetCount;
    private Label threatCount;
    private Label assetThreatCount;
    private Button changeActiveThreatsButton;
    private ListBox assetThreatThreshold;
    private ClosablePopup invalidValuePopup;
    private HTML invalidValueMessage;
    private VerticalPanel viewPanel;
    private Button assetThreatDataUpdateButton;
    private Button nextPage;

    public ThreatAssetMatrixView(List<ThreatDTO> threats) {
        viewWidget = buildView(threats);
    }

    private Widget buildView(List<ThreatDTO> threats) {
        Label header = new Label("Malevolent Threat-Asset Matrix");
        header.setStyleName("inputPanelHeader");
        DataProvider<AssetThreatLevelsDTO> dataProvider = DataProviderFactory.createDataProvider();
        matrixGrid = new AssetThreatMatrixGrid(dataProvider, threats);

        viewPanel = new VerticalPanel();
        viewPanel.setStyleName("inputPanel");
        viewPanel.setSpacing(1);
        Label label;
        label = addAssetNameLabel();
        label.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_LEFT);

        label = addAssetNameLabel();
        label.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_CENTER);

        label = addAssetNameLabel();
        label.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_RIGHT);

        HorizontalPanel labelPanel = new HorizontalPanel();

        for (HasText assetLabel : assetLabels) {
            labelPanel.add((Widget) assetLabel);
        }
        labelPanel.setWidth("100%");
        changeThresholdButton = ViewUtil.createButton("Manage Assets");
        changeActiveThreatsButton = ViewUtil.createButton("Manage Threats");
        Label assetCountLabel = new Label("Asset Count :");
        assetCount = new Label("");
        Label threatCountLabel = new Label("Threat Count :");
        Label assetThreatThresholdLabel = new Label("TA-Threshold :");
        assetThreatThreshold = new ListBox();
        for (int i = 1; i <=5; i++) {
            assetThreatThreshold.addItem(String.valueOf(i));
        }
        assetThreatThreshold.setWidth("50px");
        Label assetThreatCountLabel = new Label("TA-Count :");
        assetThreatCount = new Label();
        threatCount = new Label(String.valueOf(threats.size()));
        viewPanel.add(header);
        assetThreatDataUpdateButton = ViewUtil.createButton("Update", "Make counts and threshold persistent");
        nextPage = ViewUtil.createButton("Consequence Analysis");
        HorizontalPanel horizontalPanel = ViewUtil.horizontalPanel(5, assetCountLabel, assetCount, threatCountLabel,
                threatCount, assetThreatThresholdLabel, assetThreatThreshold, assetThreatDataUpdateButton,
                assetThreatCountLabel, assetThreatCount, nextPage);
        viewPanel.add(ViewUtil.horizontalPanel(5, changeThresholdButton, changeActiveThreatsButton, horizontalPanel));
        viewPanel.add(labelPanel);
        viewPanel.add(matrixGrid.asWidget());
        matrixGrid.setHeight("675px");
        viewPanel.setHeight("100%");
        assetThreatThreshold.addChangeHandler(new ChangeHandler() {
            public void onChange(ChangeEvent event) {
                matrixGrid.setAssetThreatThreshold(Integer.valueOf(ViewUtil.getSelectedValue(assetThreatThreshold)));
            }
        });
        initInvalidValuePopup();
        updateGridWidth();
        return new ScrollPanel(viewPanel);
    }

    private void updateGridWidth() {
        String calculatedWidth = ParreClientUtil.calculateGridWidth(matrixGrid.getWidthPx());
        viewPanel.setWidth(calculatedWidth);
        matrixGrid.setWidth(calculatedWidth);
    }

    private Label addAssetNameLabel() {
        Label label;
        label = new Label();
        label.setStyleName("normalLabel");
        assetLabels.add(label);
        return label;
    }

    private void initInvalidValuePopup() {
        invalidValuePopup = new ClosablePopup("Invalid Value", true);
        invalidValuePopup.setModal(true);
        invalidValueMessage = new HTML();
        invalidValueMessage.setWidth("150px");
        invalidValuePopup.setWidget(invalidValueMessage);
    }

    public Widget asWidget() {
        return viewWidget;
    }

    public void setAssetThreatMatrixData(List<AssetThreatLevelsDTO> assetThreatLevelsDTOs) {
        setAssetThreatDataUpdateButtonEnabled(false);
        assetCount.setText(String.valueOf(assetThreatLevelsDTOs.size()));
        matrixGrid.setData(assetThreatLevelsDTOs);
        matrixGrid.refresh();
    }

    public List<HasText> getAssetLabels() {
        return assetLabels;
    }

    public ActionSource<AssetThreatLevelsDTO> getSelectionSource() {
        return matrixGrid.getSelectionSource();
    }

    public SingleSelectionModel<AssetThreatLevelsDTO> getSelectionModel() {
        return matrixGrid.getSelectionModel();
    }

    public HasClickHandlers getChangeThresholdButton() {
        return changeThresholdButton;
    }

    public HasClickHandlers getChangeActiveThreatsButton() {
        return changeActiveThreatsButton;
    }

    public ListBox getAssetThreatThreshold() {
        return assetThreatThreshold;
    }

    public void setAssetThreatDataUpdateButtonEnabled(boolean value) {
        if(!ClientSingleton.isCurrentAnalysisLocked()) {
            assetThreatDataUpdateButton.setEnabled(value);
            if(value) {
                assetThreatDataUpdateButton.setText("Save");
            }
            else {
                assetThreatDataUpdateButton.setText("Saved");
            }
        }

    }

    @Override
    public HasClickHandlers getNextPageButton() {
        return nextPage;
    }

    public HasClickHandlers getAssetThreatDataUpdateButton() {
        return assetThreatDataUpdateButton;
    }

    public HasText getAssetThreatCount() {
        return assetThreatCount;
    }

    public ActionSource<AssetThreatLevelDTO> getAssetThreatUpdateSource() {
        return matrixGrid.getAssetThreatUpdateSource();
    }

    public void refreshAssetThreatMatrix() {
        matrixGrid.refresh();
    }

    public void setAssetThreatThreshold(Integer value) {
        ViewUtil.setSelectedValue(assetThreatThreshold, String.valueOf(value));
        matrixGrid.setAssetThreatThreshold(value);
    }

    public void showInvalidValuePopup(String message) {
        invalidValueMessage.setText(message);
        invalidValuePopup.center();
        invalidValuePopup.show();
    }
    public HasCloseHandlers<PopupPanel> getInvalidMessageDialog() {
        return invalidValuePopup;
    }

    public void updateAssetThreatMatrixData(AssetThreatLevelsDTO assetThreatLevelsDTO) {
        matrixGrid.updateData(assetThreatLevelsDTO);
    }

    public void refreshThreatList(List<ThreatDTO> result) {
        threatCount.setText(String.valueOf(result.size()));
        matrixGrid.refreshColumns(result);
        updateGridWidth();
    }

}
