/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */

/* TODO: Merge this back into the LGPL version */
package org.parre.client;

import org.parre.client.asset.presenter.*;
import org.parre.client.asset.view.*;
import org.parre.client.at.presenter.AssetThreatMatrixPresenter;
import org.parre.client.at.view.ThreatAssetMatrixView;
import org.parre.client.common.event.AssetThreatSearchType;
import org.parre.client.common.presenter.*;
import org.parre.client.common.view.AssetCriteriaView;
import org.parre.client.common.view.DisplayToggleView;
import org.parre.client.common.view.NoteRowView;
import org.parre.client.common.view.ThreatCriteriaView;
import org.parre.client.consequence.presenter.*;
import org.parre.client.consequence.view.ConsequenceView;
import org.parre.client.consequence.view.EditConsequenceView;
import org.parre.client.consequence.view.ManageConsequenceView;
import org.parre.client.dp.presenter.DpPresenter;
import org.parre.client.dp.presenter.DpSearchCommand;
import org.parre.client.dp.presenter.EditDpPresenter;
import org.parre.client.dp.presenter.ManageDpPresenter;
import org.parre.client.dp.view.DpView;
import org.parre.client.dp.view.EditDpView;
import org.parre.client.dp.view.ManageDpView;
import org.parre.client.login.presenter.LoginInfoPresenter;
import org.parre.client.login.presenter.LoginPresenter;
import org.parre.client.login.view.LoginInfoView;
import org.parre.client.login.view.LoginView;
import org.parre.client.lot.presenter.*;
import org.parre.client.lot.view.EditLotView;
import org.parre.client.lot.view.LotView;
import org.parre.client.lot.view.ManageLotView;
import org.parre.client.lot.view.ProxyIndicationView;
import org.parre.client.main.presenter.*;
import org.parre.client.main.view.*;
import org.parre.client.nt.presenter.*;
import org.parre.client.nt.view.*;
import org.parre.client.rra.presenter.*;
import org.parre.client.rra.view.*;
import org.parre.client.rrm.presenter.ManageRrmPresenter;
import org.parre.client.rrm.presenter.RrmPresenter;
import org.parre.client.rrm.view.ManageRrmView;
import org.parre.client.rrm.view.RrmView;
import org.parre.client.threat.presenter.EditThreatPresenter;
import org.parre.client.threat.presenter.ManageThreatPresenter;
import org.parre.client.threat.presenter.ManageThreatSearchCommand;
import org.parre.client.threat.presenter.ThreatPresenter;
import org.parre.client.threat.view.EditThreatView;
import org.parre.client.threat.view.ManageThreatView;
import org.parre.client.threat.view.ThreatView;
import org.parre.client.vulnerability.presenter.*;
import org.parre.client.vulnerability.view.*;
import org.parre.shared.AppInitData;
import org.parre.shared.types.VulnerabilityAnalysisType;

import org.parre.client.rpt.presenter.EditOptionPresenter;
import org.parre.client.rpt.presenter.EditTaskPresenter;
import org.parre.client.rpt.presenter.ManageRptPresenter;
import org.parre.client.rpt.presenter.RptPresenter;
import org.parre.client.rpt.view.EditOptionView;
import org.parre.client.rpt.view.EditTaskView;
import org.parre.client.rpt.view.ManageRptView;
import org.parre.client.rpt.view.RptView;

/**
 * A factory for creating View objects.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 10/20/11
 */
public class ViewFactory {

    public static LoginInfoView createLoginInfoView() {
        LoginInfoView view = new LoginInfoView();
        LoginInfoPresenter presenter = new LoginInfoPresenter(view);
        view.setPresenter(presenter);
        return view;
    }

    public static LoginView createLoginView() {
        LoginInfoView loginInfoView = createLoginInfoView();
        loginInfoView.getPresenter().go();
        LoginView view = new LoginView(loginInfoView);
        LoginPresenter presenter = new LoginPresenter(ClientSingleton.getEventBus(), view);
        view.setPresenter(presenter);
        return view;
    }

    public static MainView createMainView() {
        MainView view = new MainView();
        MainPresenter presenter = new MainPresenter(view);
        view.setPresenter(presenter);
        return view;
    }

    public static mainNavView createMainNavView() {
        mainNavView navigationViewMain = new mainNavView();
        MainNavPresenter mainNavPresenter = new MainNavPresenter(navigationViewMain);
        navigationViewMain.setPresenter(mainNavPresenter);
        return navigationViewMain;
    }

    public static AssetView createAssetView() {
        AssetView view = new AssetView();
        AssetPresenter presenter = new AssetPresenter(view);
        view.setPresenter(presenter);
        return view;
    }

    public static EditAssetView createAddAssetView(boolean popupView) {
        EditAssetView view = new EditAssetView(popupView);
        EditAssetPresenter presenter = new EditAssetPresenter(view);
        view.setPresenter(presenter);
        return view;
    }

    private static EditLocationView createEditLocationView() {
        EditLocationView view = new EditLocationView();
        EditLocationPresenter presenter = new EditLocationPresenter(view);
        view.setPresenter(presenter);
        return view;
    }
    private static EditContactView createEditContactView() {
        EditContactView view = new EditContactView();
        EditContactPresenter presenter = new EditContactPresenter(view);
        view.setPresenter(presenter);
        return view;
    }
    public static EditPhysicalResourceView createPhysicalResourceView() {
        EditPhysicalResourceView view = new EditPhysicalResourceView();
        EditAssetPresenter presenter = new EditPhysicalResourcePresenter(view);
        view.setPresenter(presenter);
        return view;
    }

    public static ThreatAssetMatrixView createThreatAssetMatrixView(AppInitData appInitData) {
        ThreatAssetMatrixView view = new ThreatAssetMatrixView(appInitData.getConsequenceAnalysisThreats());
        AssetThreatMatrixPresenter presenter = new AssetThreatMatrixPresenter(view);
        presenter.setAssetThreatThreshold(appInitData.getParreAnalysisDTO().getAssetThreatThreshold());
        view.setPresenter(presenter);
        return view;
    }

    public static ManageAssetView createManageAssetView(AppInitData appInitData) {
        AssetSearchCommand searchCommand = new ManageAssetSearchCommand();
        AssetCriteriaView criteriaView = createAssetCriteriaView(AssetThreatSearchType.ASSET, searchCommand);
        criteriaView.getPresenter().go();

        QuickEditAssetView editAssetView = createEditAssetView();
        editAssetView.getPresenter().go();
        DisplayToggleView toggleView = createDisplayToggleView();
        toggleView.getPresenter().go();
        ManageAssetView view = new ManageAssetView(criteriaView, editAssetView, toggleView);
        ManageAssetPresenter presenter = new ManageAssetPresenter(view);
        presenter.setAssetThreshold(appInitData.getParreAnalysisDTO().getAssetThreshold());
        view.setPresenter(presenter);
        return view;
    }

    private static DisplayToggleView createDisplayToggleView() {
        DisplayToggleView view = new DisplayToggleView();
        DisplayTogglePresenter presenter = new DisplayTogglePresenter(view);
        view.setPresenter(presenter);
        return view;
    }

    public static QuickEditAssetView createEditAssetView() {
        QuickEditAssetView view = new QuickEditAssetView();
        QuickEditAssetPresenter presenter = new QuickEditAssetPresenter(view);
        view.setPresenter(presenter);
        return view;
    }

    public static NoteRowView createNoteTableView() {
        NoteRowView view = new NoteRowView();
        //NoteTablePresenter presenter = new NoteTablePresenter(view);
        //view.setPresenter(presenter);
        return view;
    }

    public static AssetCriteriaView createAssetCriteriaView(AssetThreatSearchType searchType, AssetSearchCommand searchCommand) {
        AssetCriteriaView view = new AssetCriteriaView();
        AssetCriteriaPresenter presenter = new AssetCriteriaPresenter(searchType, searchCommand, view);
        view.setPresenter(presenter);
        return view;
    }

    public static ThreatView createThreatView() {
        ThreatView view = new ThreatView();
        ThreatPresenter presenter = new ThreatPresenter(view);
        view.setPresenter(presenter);
        return view;
    }

    public static EditThreatView createEditThreatView() {
        EditThreatView view = new EditThreatView();
        EditThreatPresenter presenter = new EditThreatPresenter(view);
        view.setPresenter(presenter);
        return view;
    }

    public static EditNaturalThreatView createEditNaturalThreatView() {
    	EditNaturalThreatView view = new EditNaturalThreatView();
    	EditNaturalThreatPresenter presenter = new EditNaturalThreatPresenter(view);
        view.setPresenter(presenter);
        return view;
    }
    
    public static EditTornadoThreatView createEditTornadoThreatView() {
    	EditTornadoThreatView view = new EditTornadoThreatView();
    	EditTornadoThreatPresenter presenter = new EditTornadoThreatPresenter(view);
    	view.setPresenter(presenter);
    	return view;
    }
    
    public static EditFloodThreatView createEditFloodThreatView() {
    	EditFloodThreatView view = new EditFloodThreatView();
    	EditFloodThreatPresenter presenter = new EditFloodThreatPresenter(view);
    	view.setPresenter(presenter);
    	return view;
    }
    
    public static EditEarthquakeThreatView createEditEarthquakeThreatView() {
    	EditEarthquakeThreatView view = new EditEarthquakeThreatView();
    	EditEarthquakeThreatPresenter presenter = new EditEarthquakeThreatPresenter(view);
    	view.setPresenter(presenter);
    	return view;
    }
    
    public static EditEarthquakeProfileView createEditEarthquakeProfileView() {
    	EditEarthquakeProfileView view = new EditEarthquakeProfileView();
    	EditEarthquakeProfilePresenter presenter = new EditEarthquakeProfilePresenter(view);
    	view.setPresenter(presenter);
    	return view;
    }
    
    public static EditHurricaneThreatView createEditHurricaneThreatView() {
    	EditHurricaneThreatView view = new EditHurricaneThreatView();
    	EditHurricaneThreatPresenter presenter = new EditHurricaneThreatPresenter(view);
    	view.setPresenter(presenter);
    	return view;
    }
    
    public static EditHurricaneProfileView createEditHurricaneProfileView() {
    	EditHurricaneProfileView view = new EditHurricaneProfileView();
    	EditHurricaneProfilePresenter presenter = new EditHurricaneProfilePresenter(view);
    	view.setPresenter(presenter);
    	return view;
	}
    
    public static EditIceStormThreatView createEditIceStormThreatView() {
    	EditIceStormThreatView view = new EditIceStormThreatView();
    	EditIceStormThreatPresenter presenter = new EditIceStormThreatPresenter(view);
    	view.setPresenter(presenter);
    	return view;
    }

    public static ManageThreatView createManageThreatView() {
        ThreatCriteriaView criteriaView = createThreatCriteriaView(AssetThreatSearchType.THREAT, new ManageThreatSearchCommand());
        criteriaView.getPresenter().go();
        DisplayToggleView displayToggleView = createDisplayToggleView();
        displayToggleView.getPresenter().go();
        ManageThreatView view = new ManageThreatView(criteriaView, displayToggleView);
        ManageThreatPresenter presenter = new ManageThreatPresenter(view);
        view.setPresenter(presenter);
        return view;
    }

    public static ThreatCriteriaView createThreatCriteriaView(AssetThreatSearchType searchType, ThreatSearchCommand searchCommand) {
        ThreatCriteriaView view = new ThreatCriteriaView();
        ThreatCriteriaPresenter presenter = new ThreatCriteriaPresenter(searchType, searchCommand, view);
        view.setPresenter(presenter);
        return view;
    }

    public static ConsequenceView createConsequenceView() {
        ConsequenceView view = new ConsequenceView();
        ConsequencePresenter presenter = new ConsequencePresenter(view);
        view.setPresenter(presenter);
        return view;
    }

    public static VulnerabilityView createVulnerabilityView() {
        VulnerabilityView view = new VulnerabilityView();
        VulnerabilityPresenter presenter = new VulnerabilityPresenter(view);
        view.setPresenter(presenter);
        return view;
    }

    public static LotView createLotView() {
        LotView view = new LotView();
        LotPresenter presenter = new LotPresenter(view);
        view.setPresenter(presenter);
        return view;
    }

    public static NaturalThreatView createNaturalThreatView() {
        NaturalThreatView view = new NaturalThreatView();
        NaturalThreatPresenter presenter = new NaturalThreatPresenter(view);
        view.setPresenter(presenter);
        return view;
    }

    public static DpView createDpView() {
        DpView view = new DpView();
        DpPresenter presenter = new DpPresenter(view);
        view.setPresenter(presenter);
        return view;

    }

    public static ManageConsequenceView createManageConsequenceView() {
        AssetCriteriaView assetCriteriaView = createAssetCriteriaView(AssetThreatSearchType.CONSEQUENCE, new ConsequenceAssetSearchCommand());
        assetCriteriaView.getPresenter().go();
        ThreatCriteriaView threatCriteriaView = createThreatCriteriaView(AssetThreatSearchType.CONSEQUENCE, new ConsequenceThreatSearchCommand());
        threatCriteriaView.getPresenter().go();

        EditConsequenceView editConsequenceView = createEditConsequenceView();

        editConsequenceView.getPresenter().go();
        DisplayToggleView toggleView = createDisplayToggleView();
        toggleView.getPresenter().go();
        ManageConsequenceView view = new ManageConsequenceView(assetCriteriaView, threatCriteriaView,
                editConsequenceView, toggleView);
        ManageConsequencePresenter presenter = new ManageConsequencePresenter(view);
        view.setPresenter(presenter);
        return view;
    }

    public static ManageVulnerabilityView createManageVulnerabilityView() {
        AssetCriteriaView assetCriteriaView = createAssetCriteriaView(AssetThreatSearchType.VULNERABILITY, new VulnerabilityAssetSearchCommand());
        assetCriteriaView.getPresenter().go();
        ThreatCriteriaView threatCriteriaView = createThreatCriteriaView(AssetThreatSearchType.VULNERABILITY, new VulnerabilityThreatSearchCommand());
        threatCriteriaView.getPresenter().go();

        EditVulnerabilityView editVulnerabilityView = createEditVulnerabilityView();

        editVulnerabilityView.getPresenter().go();
        DisplayToggleView toggleView = createDisplayToggleView();
        toggleView.getPresenter().go();
        ManageVulnerabilityView view = new ManageVulnerabilityView(assetCriteriaView, threatCriteriaView,
                editVulnerabilityView, toggleView);
        ManageVulnerabilityPresenter presenter = new ManageVulnerabilityPresenter(view);
        view.setPresenter(presenter);
        return view;
    }

    public static ManageLotView createManageLotView() {
        AssetCriteriaView assetCriteriaView = createAssetCriteriaView(AssetThreatSearchType.LOT, new LotAssetSearchCommand());
        assetCriteriaView.getPresenter().go();
        ThreatCriteriaView threatCriteriaView = createThreatCriteriaView(AssetThreatSearchType.LOT, new LotThreatSearchCommand());
        threatCriteriaView.getPresenter().go();

        EditLotView editLotView = createEditLotView();

        editLotView.getPresenter().go();
        DisplayToggleView toggleView = createDisplayToggleView();
        toggleView.getPresenter().go();
        ManageLotView view = new ManageLotView(assetCriteriaView, threatCriteriaView,
                editLotView, toggleView);
        ManageLotPresenter presenter = new ManageLotPresenter(view);
        view.setPresenter(presenter);
        return view;
    }

    public static ManageNaturalThreatView createManagerNaturalThreatView() {
        ThreatCriteriaView threatCriteriaView = createThreatCriteriaView(AssetThreatSearchType.NATURAL_THREAT, new NaturalThreatSearchCommand());
        threatCriteriaView.getPresenter().go();

        DisplayToggleView toggleView = createDisplayToggleView();
        toggleView.getPresenter().go();
        ManageNaturalThreatView view = new ManageNaturalThreatView(threatCriteriaView,toggleView);
        ManageNaturalThreatPresenter presenter = new ManageNaturalThreatPresenter(view);
        view.setPresenter(presenter);
        return view;
    }

    public static ManageDpView createManagerDpView() {
        ThreatCriteriaView threatCriteriaView = createThreatCriteriaView(AssetThreatSearchType.DEPENDENCY_PROXIMITY, new DpSearchCommand());
        threatCriteriaView.getPresenter().go();

        EditDpView editView = createEditDpView();

        editView.getPresenter().go();
        DisplayToggleView toggleView = createDisplayToggleView();
        toggleView.getPresenter().go();
        ManageDpView view = new ManageDpView(threatCriteriaView, editView, toggleView);
        ManageDpPresenter presenter = new ManageDpPresenter(view);
        view.setPresenter(presenter);
        return view;
    }

    private static EditConsequenceView createEditConsequenceView() {
        EditConsequenceView view = new EditConsequenceView();
        EditConsequencePresenter presenter = new EditConsequencePresenter(view);
        view.setPresenter(presenter);
        return view;
    }

    private static EditVulnerabilityView createEditVulnerabilityView() {
        EditVulnerabilityView view = new EditVulnerabilityView();
        EditVulnerabilityPresenter presenter = new EditVulnerabilityPresenter(view);
        view.setPresenter(presenter);
        return view;
    }

    private static EditLotView createEditLotView() {
        EditLotView view = new EditLotView();
        EditLotPresenter presenter = new EditLotPresenter(view);
        view.setPresenter(presenter);
        return view;
    }

    private static EditDpView createEditDpView() {
        EditDpView view = new EditDpView();
        EditDpPresenter presenter = new EditDpPresenter(view);
        view.setPresenter(presenter);
        return view;
    }

    private static EditProposedMeasureView createEditProposedMeasureView() {
        EditProposedMeasureView view = new EditProposedMeasureView();
        EditProposedMeasurePresenter presenter = new EditProposedMeasurePresenter(view);
        view.setPresenter(presenter);
        return view;
    }

    public static EditParreAnalysisView createParreAnalysisView() {
        EditLocationView editLocationView = createEditLocationView();
        editLocationView.getPresenter().go();

        EditContactView editContactView = createEditContactView();
        editContactView.getPresenter().go();

        EditProposedMeasureView measureView = createEditProposedMeasureView();
        measureView.getPresenter().go();
        EditParreAnalysisView view = new EditParreAnalysisView(editLocationView, editContactView, measureView);
        EditParreAnalysisPresenter presenter = new EditParreAnalysisPresenter(view);
        view.setPresenter(presenter);
        return view;
    }

    public static ExpertElicitationView createExpertElicitationView() {
        CounterMeasuresView counterMeasuresView = createEditCounterMeasuresView(VulnerabilityAnalysisType.DIRECT_EXPERT_ELICITATION);
        counterMeasuresView.getPresenter().go();
        ExpertElicitationView view = new ExpertElicitationView(counterMeasuresView);
        ExpertElicitationPresenter presenter = new ExpertElicitationPresenter(view);
        view.setPresenter(presenter);
        return view;
    }

    public static CounterMeasuresView createEditCounterMeasuresView(VulnerabilityAnalysisType analysisType) {
        return createEditCounterMeasuresView(analysisType, CounterMeasureValidator.NULL);
    }
    public static CounterMeasuresView createEditCounterMeasuresView(VulnerabilityAnalysisType analysisType, CounterMeasureValidator validator) {
        CounterMeasuresView view = new CounterMeasuresView();
        CounterMeasuresPresenter presenter = new CounterMeasuresPresenter(view, analysisType, validator);
        view.setPresenter(presenter);
        return view;
    }
    public static EventTreeNodeView createEventTreeNodeView() {
        EventTreeNodeView view = new EventTreeNodeView();
        EventTreeNodePresenter presenter = new EventTreeNodePresenter(view);
        view.setPresenter(presenter);
        return view;
    }

    public static LogicDiagramNodeView createLogicDiagramNodeView() {
        LogicDiagramNodeView view = new LogicDiagramNodeView();
        LogicDiagramNodePresenter presenter = new LogicDiagramNodePresenter(view);
        view.setPresenter(presenter);
        return view;
    }

    public static EditAttackStepView createEditAttackStepView() {
        EditAttackStepView view = new EditAttackStepView();
        EditAttackStepPresenter presenter = new EditAttackStepPresenter(view);
        view.setPresenter(presenter);
        return view;
    }

    public static EditResponseStepView createEditResponseStepView() {
        EditResponseStepView view = new EditResponseStepView();
        EditResponseStepPresenter presenter = new EditResponseStepPresenter(view);
        view.setPresenter(presenter);
        return view;
    }

    public static AttackPathView createAttackStepsView() {
        EditAttackStepView editAttackStepView = createEditAttackStepView();
        editAttackStepView.getPresenter().go();
        AttackPathView view = new AttackPathView(editAttackStepView);
        AttackPathPresenter presenter = new AttackPathPresenter(view);
        view.setPresenter(presenter);
        return view;
    }

    public static ResponsePathView createResponseStepsView() {
        EditResponseStepView editResponseStepView = createEditResponseStepView();
        editResponseStepView.getPresenter().go();
        ResponsePathView view = new ResponsePathView(editResponseStepView);
        ResponsePathPresenter presenter = new ResponsePathPresenter(view);
        view.setPresenter(presenter);
        return view;
    }

    public static EditPathAnalysisView createEditPathAnalysisView() {
        EditPathAnalysisView view = new EditPathAnalysisView();
        EditPathAnalysisPresenter presenter = new EditPathAnalysisPresenter(view);
        view.setPresenter(presenter);
        return view;
    }

    public static TreeAnalysisView createTreeAnalysisView() {
        CounterMeasuresView editCounterMeasuresView = createEditCounterMeasuresView(VulnerabilityAnalysisType.EVENT_TREES);
        editCounterMeasuresView.getPresenter().go();
        EventTreeNodeView eventTreeNodeView = createEventTreeNodeView();
        eventTreeNodeView.getPresenter().go();
        LogicDiagramNodeView logicDiagramNodeView = createLogicDiagramNodeView();
        logicDiagramNodeView.getPresenter().go();
        TreeAnalysisView view = new TreeAnalysisView(editCounterMeasuresView, eventTreeNodeView, logicDiagramNodeView);
        TreeAnalysisPresenter presenter = new TreeAnalysisPresenter(view);
        view.setPresenter(presenter);
        return view;
    }

    public static PathAnalysisView createPathAnalysisView() {
        EditPathAnalysisView editPathAnalysisView = createEditPathAnalysisView();
        editPathAnalysisView.getPresenter().go();
        AttackPathView attackStepsView = createAttackStepsView();
        attackStepsView.getPresenter().go();
        ResponsePathView responseStepsView = createResponseStepsView();
        responseStepsView.getPresenter().go();
        AttackPathPresenter attackStepsPresenter = (AttackPathPresenter) attackStepsView.getPresenter();
        ResponsePathPresenter responseStepsPresenter = (ResponsePathPresenter) responseStepsView.getPresenter();
        PathAnalysisState pathAnalysisState = new PathAnalysisState(attackStepsPresenter, responseStepsPresenter);
        attackStepsPresenter.setPathAnalysisState(pathAnalysisState);
        responseStepsPresenter.setPathAnalysisState(pathAnalysisState);
        ((EditPathAnalysisPresenter)editPathAnalysisView.getPresenter()).setPathAnalysisState(pathAnalysisState);
        CounterMeasuresView editCounterMeasuresView = createEditCounterMeasuresView(VulnerabilityAnalysisType.PATH_ANALYSIS, pathAnalysisState);
        editCounterMeasuresView.getPresenter().go();
        PathAnalysisView view = new PathAnalysisView(editPathAnalysisView, editCounterMeasuresView, attackStepsView, responseStepsView);
        PathAnalysisPresenter presenter = new PathAnalysisPresenter(view);
        view.setPresenter(presenter);
        return view;
    }

    public static DisplayTreeView createDisplayTreeView() {
        DisplayTreeView view = new DisplayTreeView();
        DisplayTreePresenter presenter = new DisplayTreePresenter(view);
        view.setPresenter(presenter);
        return view;
    }

    public static ProxyIndicationView createProxyIndicationView() {
        ProxyIndicationView view = new ProxyIndicationView();
        ProxyIndicationPresenter presenter = new ProxyIndicationPresenter(view);
        view.setPresenter(presenter);
        return view;

    }

    public static RraView createRraView() {
        RraView view = new RraView();
        RraPresenter presenter = new RraPresenter(view);
        view.setPresenter(presenter);
        return view;
    }

    public static RrmView createRrmView() {
        RrmView view = new RrmView();
        RrmPresenter presenter = new RrmPresenter(view);
        view.setPresenter(presenter);
        return view;
    }
    
    public static RptView createRptView() {
        RptView view = new RptView();
        RptPresenter presenter = new RptPresenter(view);
        view.setPresenter(presenter);
        return view;
    }
    
    
    
    public static ManageRraView createManageRraView() {
        EditRiskResilienceView editRiskResilienceView = createEditRiskResilienceView();
        editRiskResilienceView.getPresenter().go();
        ManageRraView view = new ManageRraView(editRiskResilienceView);
        ManageRraPresenter presenter = new ManageRraPresenter(view);
        view.setPresenter(presenter);
        return view;
    }

    public static EditRiskResilienceView createEditRiskResilienceView() {
        EditRiskResilienceView view = new EditRiskResilienceView();
        EditRiskResiliencePresenter presenter = new EditRiskResiliencePresenter(view);
        view.setPresenter(presenter);
        return view;
    }

    public static EditUriResponseView createEditUriResponseView() {
        EditUriResponseView view = new EditUriResponseView();
        EditUriResponsePresenter presenter = new EditUriResponsePresenter(view);
        view.setPresenter(presenter);
        return view;
    }

    public static ManageRrmView createManageRrmView() {
        ManageRrmView view = new ManageRrmView();
        ManageRrmPresenter presenter = new ManageRrmPresenter(view);
        view.setPresenter(presenter);
        return view;
    }

    private static EditTaskView createEditTaskView() {
    	EditTaskView view = new EditTaskView();
        EditTaskPresenter presenter = new EditTaskPresenter(view);
        view.setPresenter(presenter);
        return view;
    }
    
    private static EditOptionView createEditOptionView() {
    	EditOptionView view = new EditOptionView();
        EditOptionPresenter presenter = new EditOptionPresenter(view);
        view.setPresenter(presenter);
        return view;
    }
    
    public static ManageRptView createManageRptView() {
    	EditTaskView editTaskView = createEditTaskView();
    	editTaskView.getPresenter().go();
    	
    	EditOptionView editOptionView = createEditOptionView();
    	editOptionView.getPresenter().go();
    	
    	DisplayToggleView toggleView = createDisplayToggleView();
    	toggleView.getPresenter().go();
    	
        ManageRptView view = new ManageRptView(editTaskView, editOptionView, toggleView);
        ManageRptPresenter presenter = new ManageRptPresenter(view);
        view.setPresenter(presenter);
        return view;
    }
    

    public static ManageUriView createManageUriView() {
        EditUriResponseView editUriResponseView = createEditUriResponseView();
        editUriResponseView.getPresenter().go();
        ManageUriView view = new ManageUriView(editUriResponseView);
        ManageUriPresenter presenter = new ManageUriPresenter(view);
        view.setPresenter(presenter);
        return view;
    }

}
