/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.rpt.view;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.FlexTable.FlexCellFormatter;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HasVerticalAlignment;
import com.google.gwt.user.client.ui.Hidden;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.TabPanel;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import org.parre.client.ClientSingleton;
import org.parre.client.common.view.DisplayToggleView;
import org.parre.client.common.view.NameDescriptionView;
import org.parre.client.rpt.presenter.ManageRptPresenter;
import org.parre.client.util.ActionSource;
import org.parre.client.util.BaseModelView;
import org.parre.client.util.ClosablePopup;
import org.parre.client.util.ValueButton;
import org.parre.client.util.ViewUtil;
import org.parre.client.util.dataview.DataProvider;
import org.parre.client.util.dataview.DataProviderFactory;
import org.parre.shared.ParreAnalysisReportSectionDTO;
import org.parre.shared.ReportAssessmentTaskDTO;
import org.parre.shared.ReportInvestmentOptionDTO;
import org.parre.shared.ReportSectionLookupDTO;
import org.parre.shared.RptAssessmentDTO;
import org.parre.shared.RptAssetDTO;
import org.parre.shared.RptConsequenceDTO;
import org.parre.shared.RptMainDTO;
import org.parre.shared.RptRankingsDTO;
import org.parre.shared.RptThreatLikelihoodDTO;
import org.parre.shared.RptVulnerabilityDTO;

/**
 * This class ..
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 7/16/11
 */
public class ManageRptView extends BaseModelView<RptMainDTO> implements ManageRptPresenter.Display {
    
	private CriticalAssetsGrid criticalAssetsGrid;
	private CalConsequencesGrid consequencesGrid;
	private VulnerabilityGrid vulnerabilityGrid;
	private ThreatLikelihoodGrid threatLikelihoodGrid;
	private RiskManagementGrid riskManagementGrid;
	private RiskReductionGrid riskReductionGrid;
	private ReportAssessmentTaskGrid reportAssessmentTaskGrid;
	private ReportInvestmentOptionGrid reportInvestmentOptionGrid;
	private Widget viewWidget;
    private Button pdfReportButton;
    private NameDescriptionView nameDescriptionView;
    private HorizontalPanel pdfRptPanel;
    private HorizontalPanel rptInputPanel;
    private Label rptInputLabel;
    
    private Map<Long, Button> reportSectionButtons = new HashMap<Long, Button>();
    private Set<Label> reportSectionGroupLabels = new HashSet<Label>();
    
    private Label vulnerability;
    private Label threatLikelihood;
    
   // private CheckBox notapplicable;
    private Label instructions;
    private Label staticContent;
    private TextArea userInput;
    private Label dataTable;
    
    private TabPanel tabDisPanel;
    ScrollPanel rptDataTableScrollPanel;
    VerticalPanel rptDataTablePanel;
    private Label tableName;
    VerticalPanel rptDataTableLabelPanel;
   
    //private Button viewButton;
    private Button saveButton;
    private Button cancelButton;
    private Button addTaskButton;
    FlexTable controlInputTable;
    private DialogBox editTaskPopup;
    private EditTaskView editTaskView;
    private DialogBox editOptionPopup;
    private EditOptionView editOptionView;
    private ClickHandler sidebarClickHandler;
    private SelectionHandler<Integer> displayTableSelectionHandler;
    
    private Hidden savedReportSectionId = new Hidden();
    private Hidden clickedGridButton = new Hidden();
    
    public ManageRptView(EditTaskView editTaskView, EditOptionView editOptionView, DisplayToggleView displayToggleView) {
        this.nameDescriptionView = new NameDescriptionView();
        this.editTaskView = editTaskView;
        this.editOptionView = editOptionView;
        viewWidget = createView();
    }

    private Widget createView() {
        Label header = new Label("J-100 Compliant Risk Assessment");
        header.setStyleName("inputPanelHeader");
        VerticalPanel viewPanel = new VerticalPanel();
        viewPanel.setWidth("100%");
        viewPanel.setStyleName("inputPanel");
        viewPanel.add(header);
        pdfReportButton = ViewUtil.createButton("Generate PDF");
        pdfRptPanel = ViewUtil.horizontalPanel(2, ViewUtil.createInputLabel("PARRE ANALYSIS REPORT "), pdfReportButton );
            
        rptInputLabel = new Label("Report Information");
        rptInputPanel = ViewUtil.horizontalPanel(1, rptInputLabel);  
        
        FlexTable controlTable = new FlexTable();
        controlTable.setWidth("100%");
        //controlTable.setBorderWidth(2);
        
        ((FlexCellFormatter) controlTable.getCellFormatter()).setColSpan(0,1,3);
        controlTable.getCellFormatter().setWidth(0, 1, "320px");
        controlTable.getCellFormatter().setWidth(0, 2, "350px");
        
        controlTable.setWidget(0, 2, rptInputPanel);
        controlTable.setWidget(0, 4, pdfRptPanel);
        controlTable.getFlexCellFormatter().setHorizontalAlignment(0, 2, HasHorizontalAlignment.ALIGN_CENTER);
        controlTable.getFlexCellFormatter().setHorizontalAlignment(0, 4, HasHorizontalAlignment.ALIGN_RIGHT);
        viewPanel.add(controlTable);
        
       //First Tabular Section(Left): Report Labels
        ScrollPanel rptFieldsScrollPanel = new ScrollPanel();
        rptFieldsScrollPanel.setSize("250px", "500px");
        
        
        VerticalPanel rptFieldsPanel = new VerticalPanel();
        rptFieldsPanel.setSize("250px", "500px");
        rptFieldsPanel.setStyleName("rptFieldPanel");  
        
		String lastReportSectionLabel = "";
		for (ReportSectionLookupDTO section : ClientSingleton.getAppInitData().getReportSectionDTOs()) {
			String additionalButtonStyleName = null;
			if (section.getReportSectionGroup() != null) {
				if (!section.getReportSectionGroup().equals(lastReportSectionLabel)) {
					lastReportSectionLabel = section.getReportSectionGroup();
					Label label = new Label(section.getReportSectionGroup());
					reportSectionGroupLabels.add(label);
					rptFieldsPanel.add(label);
				}
				additionalButtonStyleName = "parre-Report-Button-indented";
			}
			ValueButton button = ViewUtil.createReportButton(section.getName());
			button.setValue(section);
			rptFieldsPanel.add(button);

			if (additionalButtonStyleName != null) {
				button.addStyleName(additionalButtonStyleName);
			}
			reportSectionButtons.put(section.getId(), button);
		}
		rptFieldsPanel.add(savedReportSectionId);
		rptFieldsPanel.add(clickedGridButton);
        
        rptFieldsScrollPanel.add(rptFieldsPanel);
        rptFieldsScrollPanel.setVerticalScrollPosition(0);
         
        TabPanel tabPanel = new TabPanel();
        tabPanel.setSize("250px", "500px");
        tabPanel.add(rptFieldsScrollPanel, "Report Parameters");
        tabPanel.selectTab(0);
        
        //Second Tabular Section(Right): Report Content Panel
        ScrollPanel rptContentScrollPanel = new ScrollPanel();
        rptContentScrollPanel.setSize("1050px", "250px");        
        
        VerticalPanel rptContentPanel = new VerticalPanel();
        rptContentPanel.setSize("1050px", "250px");
        rptContentPanel.setStyleName("rptInputPanel"); 
        
       // notapplicable = new CheckBox("Not Applicable");
               
        instructions = new Label("Input Instructions: ");
        instructions.setSize("950px", "50px");
        instructions.setTitle("Follow these Input Instructions");
        instructions.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_LEFT);
        
        staticContent = new Label("Report Static Content:");
        staticContent.setTitle("Your Input follows this content in the Report");
        staticContent.setSize("950px", "200px");
        staticContent.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_LEFT);
        
       // rptContentPanel.add(notapplicable);
        rptContentPanel.add(instructions);
        rptContentPanel.add(staticContent);
        
        rptContentScrollPanel.add(rptContentPanel);
        rptContentScrollPanel.setVerticalScrollPosition(0);
        
        //User Input Text Area Panel
        VerticalPanel rptInputPanel = new VerticalPanel();
        rptInputPanel.setSize("1050px", "250px");
        rptInputPanel.setStyleName("rptInputPanel"); 
        rptInputPanel.setVerticalAlignment(HasVerticalAlignment.ALIGN_BOTTOM);
        
        userInput = new TextArea();
        userInput.setCharacterWidth(160);
        userInput.setVisibleLines(10);                 
        saveButton = ViewUtil.createButton("Save");
        cancelButton = ViewUtil.createButton("Cancel");        
        rptInputPanel.add(userInput);
        rptInputPanel.add(ViewUtil.layoutCenter(saveButton, cancelButton));
        
        //Add Static Content ScrollPanel and User Input Text Area Vertical Panel into new Vertical to add to TabPanel
        VerticalPanel rptSavePanel = new VerticalPanel();
        rptSavePanel.add(rptContentScrollPanel);
        rptSavePanel.add(rptInputPanel);
        
       //Display Data Table Tab
        DataProvider<RptAssetDTO> dataProviderAssets = DataProviderFactory.createDataProvider();
        criticalAssetsGrid = new CriticalAssetsGrid(dataProviderAssets);
        DataProvider<RptConsequenceDTO> dataProviderConsequences = DataProviderFactory.createDataProvider();
        consequencesGrid = new CalConsequencesGrid(dataProviderConsequences);        
        DataProvider<RptVulnerabilityDTO> dataProviderVulnerabilty = DataProviderFactory.createDataProvider();
        vulnerabilityGrid = new VulnerabilityGrid(dataProviderVulnerabilty);
        DataProvider<RptThreatLikelihoodDTO> dataProviderThreatLikelihood = DataProviderFactory.createDataProvider();
        threatLikelihoodGrid = new ThreatLikelihoodGrid(dataProviderThreatLikelihood);
        DataProvider<RptRankingsDTO> dataProviderRiskManagement = DataProviderFactory.createDataProvider();
        riskManagementGrid = new RiskManagementGrid(dataProviderRiskManagement);
        DataProvider<RptAssessmentDTO> dataProviderRiskReduction = DataProviderFactory.createDataProvider();
        riskReductionGrid = new RiskReductionGrid(dataProviderRiskReduction);
        DataProvider<ReportAssessmentTaskDTO> dataProviderAssessmentTask = DataProviderFactory.createDataProvider();
        reportAssessmentTaskGrid = new ReportAssessmentTaskGrid(dataProviderAssessmentTask);
        DataProvider<ReportInvestmentOptionDTO> dataProviderInvestmentOption = DataProviderFactory.createDataProvider();
        reportInvestmentOptionGrid = new ReportInvestmentOptionGrid(dataProviderInvestmentOption);
        
        //For the Data Input Table
        addTaskButton = ViewUtil.createButton("Add Task");
        controlInputTable = new FlexTable();
        controlInputTable.setWidth("100%");
        controlInputTable.setWidget(0, 0, ViewUtil.horizontalPanel(5, addTaskButton));
        controlInputTable.getFlexCellFormatter().setHorizontalAlignment(0, 0, HasHorizontalAlignment.ALIGN_RIGHT);
      
        
        rptDataTableLabelPanel = new VerticalPanel();
        tableName = new Label("User Input for Options: ");
        rptDataTableLabelPanel.add(tableName);
        
        rptDataTableScrollPanel = new ScrollPanel();
        rptDataTableScrollPanel.setSize("1050px", "500px");   
        rptDataTablePanel = new VerticalPanel();
        rptDataTablePanel.setSize("1050px", "500px");
        dataTable = new Label("Display Data Table:");
        rptDataTablePanel.add(dataTable);
        
        //Load the Display Table only when the User Clicks on the Button.
        rptDataTableScrollPanel.add(rptDataTablePanel);
        
        
        tabDisPanel = new TabPanel();
        tabDisPanel.setSize("1050px", "500px");       
        tabDisPanel.add(rptSavePanel, "User Input"); 
        tabDisPanel.add(rptDataTableScrollPanel, "Data Table"); 
        tabDisPanel.selectTab(0);
        
        FlexTable dataTable = new FlexTable();
        dataTable.setWidth("100%");
        dataTable.setWidget(0, 0, tabPanel);
        dataTable.setWidget(0, 1, tabDisPanel);
        dataTable.getFlexCellFormatter().setHorizontalAlignment(0, 1, HasHorizontalAlignment.ALIGN_RIGHT);
        dataTable.getFlexCellFormatter().setVerticalAlignment(0, 1, HasVerticalAlignment.ALIGN_TOP);
        
        viewPanel.add(dataTable);      
        
        initEditTaskPopup();
        initEditOptionPopup();
        return viewPanel;
    }

    public HasClickHandlers getPdfReportButton() {
        return pdfReportButton;
    }

     public Widget asWidget() {
        return viewWidget;
    }

	@Override
	public void setRptAnalysis(RptMainDTO rptInfoDTO) {
	}

	@Override
	public HasClickHandlers getSaveButton() {
        return saveButton;
    }
	
	public void setSidebarClickHandler(ClickHandler clickHandler) {
		this.sidebarClickHandler = clickHandler;
		for (Long id : reportSectionButtons.keySet()) {
			((Button)reportSectionButtons.get(id)).addClickHandler(sidebarClickHandler);
		}
	}
	
	
	public void setDisplayTableHandler(SelectionHandler<Integer> dataTable) {
		this.displayTableSelectionHandler = dataTable;				
		tabDisPanel.addSelectionHandler(displayTableSelectionHandler);
	}
	
	public HasClickHandlers getAddTaskButton() {
        return addTaskButton;
    }
	
	@Override
	public boolean validate() {
		return false;
	}

	@Override
	public void updateModel() {
	}

	@Override
	public void updateView() {
	}	
	
	@Override
	public void setDefaultTab(){
		//When the user click on Sidebar button, the tab should be updated to UserInput
		tabDisPanel.selectTab(0);
	}

	@Override
	public void updateReportSection(ReportSectionLookupDTO dto) {		
		this.instructions.setText(dto.getInstructions());
		this.staticContent.setText(dto.getDescription());
		this.rptInputLabel.setText(dto.getName());
		this.rptDataTablePanel.clear();		
		clickedGridButton.setValue(dto.getName());
	}

	@Override
	public void updateReportSectionValues(ParreAnalysisReportSectionDTO dto) {
		//this.notapplicable.setValue(dto.getOmitted() != null && dto.getOmitted() == true);
		this.userInput.setText(dto.getUserInput());
		savedReportSectionId.setValue(String.valueOf(dto.getReportSectionLookupId()));
	}

	@Override
	public ParreAnalysisReportSectionDTO getReportSectionValues() {
		ParreAnalysisReportSectionDTO reportSectionValues = new ParreAnalysisReportSectionDTO();
		reportSectionValues.setReportSectionLookupId(Long.parseLong(savedReportSectionId.getValue()));
		//reportSectionValues.setOmitted(this.notapplicable.getValue());
		reportSectionValues.setUserInput(this.userInput.getText());
		return reportSectionValues;
	}
	
	@Override
	public void setCriticalAssetsGrid(List<RptAssetDTO> results) {
		this.criticalAssetsGrid.setData(results);        
		rptDataTablePanel.add(criticalAssetsGrid.asWidget());
    }
	
	@Override
	public void setConsequencesGrid(List<RptConsequenceDTO> results) {
		this.consequencesGrid.setData(results);        
		rptDataTablePanel.add(consequencesGrid.asWidget());
    }

	@Override
	public void setVulnerabilityGrid(List<RptVulnerabilityDTO> results) {
		this.vulnerabilityGrid.setData(results);        
		rptDataTablePanel.add(vulnerabilityGrid.asWidget());
	}

	@Override
	public void setThreatLikelihoodGrid(List<RptThreatLikelihoodDTO> results) {
		this.threatLikelihoodGrid.setData(results);        
		rptDataTablePanel.add(threatLikelihoodGrid.asWidget());
	}

	@Override
	public void setRiskManagementGrid(List<RptRankingsDTO> results) {
		this.riskManagementGrid.setData(results);        
		rptDataTablePanel.add(riskManagementGrid.asWidget());
	}
	
	
	@Override
	public void setRiskReductionGrid(List<RptAssessmentDTO> results) {
		this.riskReductionGrid.setData(results);        
		rptDataTablePanel.add(riskReductionGrid.asWidget());
	}
	
	@Override
	public void setReportAssessmentTaskGrid(List<ReportAssessmentTaskDTO> results) {
		this.reportAssessmentTaskGrid.setData(results); 
		rptDataTablePanel.add(controlInputTable);
		rptDataTablePanel.add(reportAssessmentTaskGrid.asWidget());
	}
	
	
	@Override
	public void setReportInvestmentOptionGrid(List<ReportInvestmentOptionDTO> results) {
		this.reportInvestmentOptionGrid.setData(results); 
		rptDataTablePanel.add(rptDataTableLabelPanel);
		rptDataTablePanel.add(reportInvestmentOptionGrid.asWidget());
	}
	

	private void initEditTaskPopup() {
        editTaskPopup = new ClosablePopup("Add/Edit Task", true);
        editTaskPopup.setModal(true);
        Widget widget = editTaskView.asWidget();
        widget.setWidth("300px");
        editTaskPopup.setWidget(widget);
    }
	
	public void showAddTaskPopup() {
		editTaskView.clearTaskValues();   
		editTaskPopup.center();
		editTaskPopup.show();
    }
	
	
	public void showEditTaskPopup() {
		editTaskPopup.center();
		editTaskPopup.show();
    }
	
	public void hideEditTaskPopup() {
		editTaskPopup.hide();
    }
	
	
	private void initEditOptionPopup() {
        editOptionPopup = new ClosablePopup("Update Investment Options", true);
        editOptionPopup.setModal(true);
        Widget widget = editOptionView.asWidget();
        widget.setWidth("300px");
        editOptionPopup.setWidget(widget);
    }
	
	public void showEditOptionPopup() {
		editOptionPopup.center();
		editOptionPopup.show();
    }
	
	public void hideEditOptionPopup() {
		editOptionPopup.hide();
    }

	@Override
	public ActionSource<ReportAssessmentTaskDTO> getTaskSelectionSource() {
		return reportAssessmentTaskGrid.getSelectionSource();
	}
	
	@Override
	public ActionSource<ReportInvestmentOptionDTO> getOptionSelectionSource() {
		return reportInvestmentOptionGrid.getSelectionSource();
	}
	
}
