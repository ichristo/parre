/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.rpt.view;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import com.google.gwt.user.cellview.client.Column;
import org.parre.client.util.DoubleClickableTextCell;
import org.parre.client.util.dataview.DataColumn;
import org.parre.client.util.dataview.DataGridView;
import org.parre.client.util.dataview.DataProvider;
import org.parre.shared.RiskResilienceDTO;
import org.parre.shared.RptAssetDTO;

public class CriticalAssetsGrid extends DataGridView<RptAssetDTO> {

    public CriticalAssetsGrid(DataProvider<RptAssetDTO> dataProvider) {
        super(dataProvider);
        List<DataColumn<RptAssetDTO>> columnList = new ArrayList<DataColumn<RptAssetDTO>>();
        columnList.add(createAssetSystemColumn());
        columnList.add(createCriticalAssetColumn());
        addColumns(columnList);
    }

    private DoubleClickableTextCell<RptAssetDTO> createDoubleClickableTextCell() {
        return new DoubleClickableTextCell<RptAssetDTO>(getSelectionModel(), getSelectionSource());
    }

    private DataColumn<RptAssetDTO> createAssetSystemColumn() {
        Column<RptAssetDTO, String> column = new Column<RptAssetDTO, String>(createDoubleClickableTextCell()) {
            @Override
            public String getValue(RptAssetDTO object) {
                return String.valueOf(object.getAssetSystem());
            }
        };	
        Comparator<RptAssetDTO> comparator = new Comparator<RptAssetDTO>(){

			@Override
			public int compare(RptAssetDTO o1, RptAssetDTO o2) {
				return 0;
			}        	
        };
            return new DataColumn<RptAssetDTO>("Asset System", column, comparator, 120);
    }

    private DataColumn<RptAssetDTO> createCriticalAssetColumn() {
        Column<RptAssetDTO, String> column = new Column<RptAssetDTO, String>(createDoubleClickableTextCell()) {
            @Override
            public String getValue(RptAssetDTO object) {
                return String.valueOf(object.getCriticalAsset());
            }
        };	
        Comparator<RptAssetDTO> comparator = new Comparator<RptAssetDTO>(){

			@Override
			public int compare(RptAssetDTO o1, RptAssetDTO o2) {
				return 0;
			}        	
        };
            return new DataColumn<RptAssetDTO>("Critical Asset", column, comparator, 120);
    }


}
