/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.rpt.view;

import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.Panel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import org.parre.client.rpt.presenter.EditTaskPresenter;
import org.parre.client.util.BaseModelView;
import org.parre.client.util.ViewUtil;
import org.parre.shared.ReportAssessmentTaskDTO;

public class EditTaskView extends BaseModelView<ReportAssessmentTaskDTO>  implements EditTaskPresenter.Display {
	private Widget viewWidget;
    private TextBox task;
    private TextBox title;
	private TextBox comments;
    private Button saveButton;
    private Long taskId;
  //  private Long reportId;

	public EditTaskView() {
        viewWidget = buildView();
	}

	private Panel buildView() {
        VerticalPanel panel = new VerticalPanel();
        FlexTable inputTable = new FlexTable();
        inputTable.setCellSpacing(1);
        int rowNum = -1;
        task = ViewUtil.addTextBoxFormEntry(inputTable, ++rowNum, "Task #");
        task.setWidth("50px");
        title = ViewUtil.addTextBoxFormEntry(inputTable, ++rowNum, "Title");
        title.setWidth("100px");
        comments = ViewUtil.addTextBoxFormEntry(inputTable, ++rowNum, "Comments");
        comments.setWidth("200px");
        saveButton = ViewUtil.createButton("Save");
        panel.add(inputTable);
        panel.add(ViewUtil.layoutRight(saveButton));
        panel.setStyleName("inputPanel");
        return panel;
    }
	

	public ReportAssessmentTaskDTO getTaskValues() {
		ReportAssessmentTaskDTO dto = new ReportAssessmentTaskDTO();
		dto.setTaskNumber(ViewUtil.getQuantity(task));
		dto.setTitle(ViewUtil.getText(title));
		dto.setComments(ViewUtil.getText(comments));
		dto.setTaskId(taskId);
	//	dto.setReportId(reportId);
		return dto;
    }
	
	public void displayTaskValues(ReportAssessmentTaskDTO dto){
		 ViewUtil.setQuantity(task, dto.getTaskNumber());
	     ViewUtil.setText(title, dto.getTitle());
	     ViewUtil.setText(comments, dto.getComments());
	     taskId = dto.getId();	     
	     //reportId = dto.getReportId();
	}
	
	public void clearTaskValues(){
		ViewUtil.setText(task, "");
	     ViewUtil.setText(title, "");
	     ViewUtil.setText(comments, "");
	     taskId = null;
	    // reportId = null;
	}
	
	public Widget asWidget() {
		return viewWidget;
	}

    public HasClickHandlers getSaveButton() {
        return saveButton;
    }
    

	@Override
	public boolean validate() {
		return false;
	}

	@Override
	public void updateModel() {
	}
	
	@Override
	public void updateView() {
	}

	}
