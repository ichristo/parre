/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.rpt.view;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import com.google.gwt.user.cellview.client.Column;
import org.parre.client.util.DoubleClickableTextCell;
import org.parre.client.util.dataview.DataColumn;
import org.parre.client.util.dataview.DataGridView;
import org.parre.client.util.dataview.DataProvider;
import org.parre.client.util.dataview.ReportDataGridView;
import org.parre.shared.RptAssessmentDTO;

public class RiskReductionGrid extends ReportDataGridView<RptAssessmentDTO> {

    public RiskReductionGrid(DataProvider<RptAssessmentDTO> dataProvider) {
        super(dataProvider);
        List<DataColumn<RptAssessmentDTO>> columnList = new ArrayList<DataColumn<RptAssessmentDTO>>();
        columnList.add(createOptionColumn());
        columnList.add(createGrossColumn());
        columnList.add(createPresentColumn());
        columnList.add(createNetColumn());
        columnList.add(createRatioColumn());
        addColumns(columnList);
    }

    private DoubleClickableTextCell<RptAssessmentDTO> createDoubleClickableTextCell() {
        return new DoubleClickableTextCell<RptAssessmentDTO>(getSelectionModel(), getSelectionSource());
    }

    private DataColumn<RptAssessmentDTO> createOptionColumn() {
        Column<RptAssessmentDTO, String> column = new Column<RptAssessmentDTO, String>(createDoubleClickableTextCell()) {
            @Override
            public String getValue(RptAssessmentDTO object) {
                return String.valueOf(object.getOptionName());
            }
        };	
        Comparator<RptAssessmentDTO> comparator = new Comparator<RptAssessmentDTO>(){

			@Override
			public int compare(RptAssessmentDTO o1, RptAssessmentDTO o2) {
				return 0;
			}        	
        };
            return new DataColumn<RptAssessmentDTO>("Option", column, comparator, 60);
    }

    private DataColumn<RptAssessmentDTO> createGrossColumn() {
        Column<RptAssessmentDTO, String> column = new Column<RptAssessmentDTO, String>(createDoubleClickableTextCell()) {
            @Override
            public String getValue(RptAssessmentDTO object) {
                return String.valueOf(object.getGross());
            }
        };	
        Comparator<RptAssessmentDTO> comparator = new Comparator<RptAssessmentDTO>(){

			@Override
			public int compare(RptAssessmentDTO o1, RptAssessmentDTO o2) {
				return 0;
			}        	
        };
            return new DataColumn<RptAssessmentDTO>("Total Gross Annual Benefits", column, comparator, 60);
    }
    
    private DataColumn<RptAssessmentDTO> createPresentColumn() {
        Column<RptAssessmentDTO, String> column = new Column<RptAssessmentDTO, String>(createDoubleClickableTextCell()) {
            @Override
            public String getValue(RptAssessmentDTO object) {
                return String.valueOf(object.getValue());
            }
        };	
        Comparator<RptAssessmentDTO> comparator = new Comparator<RptAssessmentDTO>(){

			@Override
			public int compare(RptAssessmentDTO o1, RptAssessmentDTO o2) {
				return 0;
			}        	
        };
            return new DataColumn<RptAssessmentDTO>("Present Value Annual Cost", column, comparator, 60);
    }

    private DataColumn<RptAssessmentDTO> createNetColumn() {
        Column<RptAssessmentDTO, String> column = new Column<RptAssessmentDTO, String>(createDoubleClickableTextCell()) {
            @Override
            public String getValue(RptAssessmentDTO object) {
                return String.valueOf(object.getNet());
            }
        };	
        Comparator<RptAssessmentDTO> comparator = new Comparator<RptAssessmentDTO>(){

			@Override
			public int compare(RptAssessmentDTO o1, RptAssessmentDTO o2) {
				return 0;
			}        	
        };
            return new DataColumn<RptAssessmentDTO>("Net Annual Benefits", column, comparator, 60);
    }
    
    private DataColumn<RptAssessmentDTO> createRatioColumn() {
        Column<RptAssessmentDTO, String> column = new Column<RptAssessmentDTO, String>(createDoubleClickableTextCell()) {
            @Override
            public String getValue(RptAssessmentDTO object) {
                return String.valueOf(object.getRatio());
            }
        };	
        Comparator<RptAssessmentDTO> comparator = new Comparator<RptAssessmentDTO>(){

			@Override
			public int compare(RptAssessmentDTO o1, RptAssessmentDTO o2) {
				return 0;
			}        	
        };
            return new DataColumn<RptAssessmentDTO>("B/C Ratio", column, comparator, 60);
    }


}
