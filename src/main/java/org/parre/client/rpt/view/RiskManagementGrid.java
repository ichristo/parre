/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.rpt.view;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import com.google.gwt.user.cellview.client.Column;
import org.parre.client.util.DoubleClickableTextCell;
import org.parre.client.util.dataview.DataColumn;
import org.parre.client.util.dataview.DataGridView;
import org.parre.client.util.dataview.DataProvider;
import org.parre.shared.RptRankingsDTO;

public class RiskManagementGrid extends DataGridView<RptRankingsDTO> {

    public RiskManagementGrid(DataProvider<RptRankingsDTO> dataProvider) {
        super(dataProvider);
        List<DataColumn<RptRankingsDTO>> columnList = new ArrayList<DataColumn<RptRankingsDTO>>();
        columnList.add(createAssetColumn());
        columnList.add(createThreatColumn());
        columnList.add(createRiskColumn());
        columnList.add(createResilienceColumn());
        columnList.add(createDeathColumn());
        columnList.add(createInjuryColumn());
        columnList.add(createFinancialColumn());
        addColumns(columnList);
    }

    private DoubleClickableTextCell<RptRankingsDTO> createDoubleClickableTextCell() {
        return new DoubleClickableTextCell<RptRankingsDTO>(getSelectionModel(), getSelectionSource());
    }

    private DataColumn<RptRankingsDTO> createAssetColumn() {
        Column<RptRankingsDTO, String> column = new Column<RptRankingsDTO, String>(createDoubleClickableTextCell()) {
            @Override
            public String getValue(RptRankingsDTO object) {
                return String.valueOf(object.getAsset());
            }
        };	
        Comparator<RptRankingsDTO> comparator = new Comparator<RptRankingsDTO>(){

			@Override
			public int compare(RptRankingsDTO o1, RptRankingsDTO o2) {
				return 0;
			}        	
        };
            return new DataColumn<RptRankingsDTO>("Asset", column, comparator, 60);
    }

    private DataColumn<RptRankingsDTO> createThreatColumn() {
        Column<RptRankingsDTO, String> column = new Column<RptRankingsDTO, String>(createDoubleClickableTextCell()) {
            @Override
            public String getValue(RptRankingsDTO object) {
                return String.valueOf(object.getThreat());
            }
        };	
        Comparator<RptRankingsDTO> comparator = new Comparator<RptRankingsDTO>(){

			@Override
			public int compare(RptRankingsDTO o1, RptRankingsDTO o2) {
				return 0;
			}        	
        };
            return new DataColumn<RptRankingsDTO>("Threat", column, comparator, 60);
    }
    
    private DataColumn<RptRankingsDTO> createRiskColumn() {
        Column<RptRankingsDTO, String> column = new Column<RptRankingsDTO, String>(createDoubleClickableTextCell()) {
            @Override
            public String getValue(RptRankingsDTO object) {
                return String.valueOf(object.getRiskRank());
            }
        };	
        Comparator<RptRankingsDTO> comparator = new Comparator<RptRankingsDTO>(){

			@Override
			public int compare(RptRankingsDTO o1, RptRankingsDTO o2) {
				return 0;
			}        	
        };
            return new DataColumn<RptRankingsDTO>("Risk", column, comparator, 60);
    }

    private DataColumn<RptRankingsDTO> createResilienceColumn() {
        Column<RptRankingsDTO, String> column = new Column<RptRankingsDTO, String>(createDoubleClickableTextCell()) {
            @Override
            public String getValue(RptRankingsDTO object) {
                return String.valueOf(object.getResilienceRank());
            }
        };	
        Comparator<RptRankingsDTO> comparator = new Comparator<RptRankingsDTO>(){

			@Override
			public int compare(RptRankingsDTO o1, RptRankingsDTO o2) {
				return 0;
			}        	
        };
            return new DataColumn<RptRankingsDTO>("Pair Resilience", column, comparator, 60);
    }

    private DataColumn<RptRankingsDTO> createDeathColumn() {
        Column<RptRankingsDTO, String> column = new Column<RptRankingsDTO, String>(createDoubleClickableTextCell()) {
            @Override
            public String getValue(RptRankingsDTO object) {
                return String.valueOf(object.getFatalityRank());
            }
        };	
        Comparator<RptRankingsDTO> comparator = new Comparator<RptRankingsDTO>(){

			@Override
			public int compare(RptRankingsDTO o1, RptRankingsDTO o2) {
				return 0;
			}        	
        };
            return new DataColumn<RptRankingsDTO>("Deaths", column, comparator, 60);
    }
    
    private DataColumn<RptRankingsDTO> createInjuryColumn() {
        Column<RptRankingsDTO, String> column = new Column<RptRankingsDTO, String>(createDoubleClickableTextCell()) {
            @Override
            public String getValue(RptRankingsDTO object) {
                return String.valueOf(object.getInjuryRank());
            }
        };	
        Comparator<RptRankingsDTO> comparator = new Comparator<RptRankingsDTO>(){

			@Override
			public int compare(RptRankingsDTO o1, RptRankingsDTO o2) {
				return 0;
			}        	
        };
            return new DataColumn<RptRankingsDTO>("Serious Injuries", column, comparator, 60);
    }
    
    private DataColumn<RptRankingsDTO> createFinancialColumn() {
        Column<RptRankingsDTO, String> column = new Column<RptRankingsDTO, String>(createDoubleClickableTextCell()) {
            @Override
            public String getValue(RptRankingsDTO object) {
                return String.valueOf(object.getImpactRank());
            }
        };	
        Comparator<RptRankingsDTO> comparator = new Comparator<RptRankingsDTO>(){

			@Override
			public int compare(RptRankingsDTO o1, RptRankingsDTO o2) {
				return 0;
			}        	
        };
            return new DataColumn<RptRankingsDTO>("Owner Financial", column, comparator, 60);
    }


}
