/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.rpt.view;

import org.parre.client.rpt.presenter.RptPresenter;
import org.parre.client.util.DeckResizeComposite;
import org.parre.client.util.Presenter;

/**
 * User: Raju Kanumuri Date: 10/21/2013 Time: 4:39 PM To change this template
 * use File | Settings | File Templates.
 */
public class RptView extends DeckResizeComposite implements
		RptPresenter.Display {

	private Presenter presenter;

	public RptView() {
	}

	public void setPresenter(Presenter presenter) {
		this.presenter = presenter;
	}

	public Presenter getPresenter() {
		return presenter;
	}
}