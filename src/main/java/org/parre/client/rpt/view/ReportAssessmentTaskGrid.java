/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.rpt.view;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import com.google.gwt.user.cellview.client.Column;
import org.parre.client.util.DoubleClickableTextCell;
import org.parre.client.util.dataview.DataColumn;
import org.parre.client.util.dataview.DataGridView;
import org.parre.client.util.dataview.DataProvider;
import org.parre.shared.ReportAssessmentTaskDTO;

public class ReportAssessmentTaskGrid extends DataGridView<ReportAssessmentTaskDTO> {

    public ReportAssessmentTaskGrid(DataProvider<ReportAssessmentTaskDTO> dataProvider) {
        super(dataProvider);
        List<DataColumn<ReportAssessmentTaskDTO>> columnList = new ArrayList<DataColumn<ReportAssessmentTaskDTO>>();
        columnList.add(createTaskColumn());
        columnList.add(createTitleColumn());
        columnList.add(createCommentsColumn());
        addColumns(columnList);
    }

    private DoubleClickableTextCell<ReportAssessmentTaskDTO> createDoubleClickableTextCell() {
        return new DoubleClickableTextCell<ReportAssessmentTaskDTO>(getSelectionModel(), getSelectionSource());
    }

    private DataColumn<ReportAssessmentTaskDTO> createTaskColumn() {
        Column<ReportAssessmentTaskDTO, String> column = new Column<ReportAssessmentTaskDTO, String>(createDoubleClickableTextCell()) {
            @Override
            public String getValue(ReportAssessmentTaskDTO object) {
                return String.valueOf(object.getTaskNumber());
            }
        };	
        Comparator<ReportAssessmentTaskDTO> comparator = new Comparator<ReportAssessmentTaskDTO>(){

			@Override
			public int compare(ReportAssessmentTaskDTO o1, ReportAssessmentTaskDTO o2) {
				return 0;
			}        	
        };
            return new DataColumn<ReportAssessmentTaskDTO>("Task", column, comparator, 80);
    }

    private DataColumn<ReportAssessmentTaskDTO> createTitleColumn() {
        Column<ReportAssessmentTaskDTO, String> column = new Column<ReportAssessmentTaskDTO, String>(createDoubleClickableTextCell()) {
            @Override
            public String getValue(ReportAssessmentTaskDTO object) {
                return String.valueOf(object.getTitle());
            }
        };	
        Comparator<ReportAssessmentTaskDTO> comparator = new Comparator<ReportAssessmentTaskDTO>(){

			@Override
			public int compare(ReportAssessmentTaskDTO o1, ReportAssessmentTaskDTO o2) {
				return 0;
			}        	
        };
            return new DataColumn<ReportAssessmentTaskDTO>("Title", column, comparator, 80);
    }
    
    private DataColumn<ReportAssessmentTaskDTO> createCommentsColumn() {
        Column<ReportAssessmentTaskDTO, String> column = new Column<ReportAssessmentTaskDTO, String>(createDoubleClickableTextCell()) {
            @Override
            public String getValue(ReportAssessmentTaskDTO object) {
                return String.valueOf(object.getComments());
            }
        };	
        Comparator<ReportAssessmentTaskDTO> comparator = new Comparator<ReportAssessmentTaskDTO>(){

			@Override
			public int compare(ReportAssessmentTaskDTO o1, ReportAssessmentTaskDTO o2) {
				return 0;
			}        	
        };
            return new DataColumn<ReportAssessmentTaskDTO>("Comments", column, comparator, 80);
    }


}
