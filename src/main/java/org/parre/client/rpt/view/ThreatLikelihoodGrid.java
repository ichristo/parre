/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.rpt.view;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import com.google.gwt.user.cellview.client.Column;
import org.parre.client.util.DoubleClickableTextCell;
import org.parre.client.util.dataview.DataColumn;
import org.parre.client.util.dataview.DataGridView;
import org.parre.client.util.dataview.DataProvider;
import org.parre.shared.RptThreatLikelihoodDTO;

public class ThreatLikelihoodGrid extends DataGridView<RptThreatLikelihoodDTO> {

    public ThreatLikelihoodGrid(DataProvider<RptThreatLikelihoodDTO> dataProvider) {
        super(dataProvider);
        List<DataColumn<RptThreatLikelihoodDTO>> columnList = new ArrayList<DataColumn<RptThreatLikelihoodDTO>>();
        columnList.add(createAssetColumn());
        columnList.add(createThreatColumn());
        columnList.add(createTypeAnalysisColumn());
        columnList.add(createLikelihoodColumn());
        addColumns(columnList);
    }

    private DoubleClickableTextCell<RptThreatLikelihoodDTO> createDoubleClickableTextCell() {
        return new DoubleClickableTextCell<RptThreatLikelihoodDTO>(getSelectionModel(), getSelectionSource());
    }

    private DataColumn<RptThreatLikelihoodDTO> createAssetColumn() {
        Column<RptThreatLikelihoodDTO, String> column = new Column<RptThreatLikelihoodDTO, String>(createDoubleClickableTextCell()) {
            @Override
            public String getValue(RptThreatLikelihoodDTO object) {
                return String.valueOf(object.getAsset());
            }
        };	
        Comparator<RptThreatLikelihoodDTO> comparator = new Comparator<RptThreatLikelihoodDTO>(){

			@Override
			public int compare(RptThreatLikelihoodDTO o1, RptThreatLikelihoodDTO o2) {
				return 0;
			}        	
        };
            return new DataColumn<RptThreatLikelihoodDTO>("Asset", column, comparator, 60);
    }

    private DataColumn<RptThreatLikelihoodDTO> createThreatColumn() {
        Column<RptThreatLikelihoodDTO, String> column = new Column<RptThreatLikelihoodDTO, String>(createDoubleClickableTextCell()) {
            @Override
            public String getValue(RptThreatLikelihoodDTO object) {
                return String.valueOf(object.getThreat());
            }
        };	
        Comparator<RptThreatLikelihoodDTO> comparator = new Comparator<RptThreatLikelihoodDTO>(){

			@Override
			public int compare(RptThreatLikelihoodDTO o1, RptThreatLikelihoodDTO o2) {
				return 0;
			}        	
        };
            return new DataColumn<RptThreatLikelihoodDTO>("Threat", column, comparator, 60);
    }
    
    private DataColumn<RptThreatLikelihoodDTO> createTypeAnalysisColumn() {
        Column<RptThreatLikelihoodDTO, String> column = new Column<RptThreatLikelihoodDTO, String>(createDoubleClickableTextCell()) {
            @Override
            public String getValue(RptThreatLikelihoodDTO object) {
                return String.valueOf(object.getTypeAnalysis());
            }
        };	
        Comparator<RptThreatLikelihoodDTO> comparator = new Comparator<RptThreatLikelihoodDTO>(){

			@Override
			public int compare(RptThreatLikelihoodDTO o1, RptThreatLikelihoodDTO o2) {
				return 0;
			}        	
        };
            return new DataColumn<RptThreatLikelihoodDTO>("Type Analysis", column, comparator, 60);
    }

    private DataColumn<RptThreatLikelihoodDTO> createLikelihoodColumn() {
        Column<RptThreatLikelihoodDTO, String> column = new Column<RptThreatLikelihoodDTO, String>(createDoubleClickableTextCell()) {
            @Override
            public String getValue(RptThreatLikelihoodDTO object) {
                return String.valueOf(object.getLikelihood());
            }
        };	
        Comparator<RptThreatLikelihoodDTO> comparator = new Comparator<RptThreatLikelihoodDTO>(){

			@Override
			public int compare(RptThreatLikelihoodDTO o1, RptThreatLikelihoodDTO o2) {
				return 0;
			}        	
        };
            return new DataColumn<RptThreatLikelihoodDTO>("Threat Likelihood", column, comparator, 60);
    }


}
