/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.rpt.presenter;

import com.google.gwt.event.shared.EventBus;
import org.parre.client.ClientSingleton;
import org.parre.client.ViewFactory;
import org.parre.client.nav.event.ReportsNavEvent;
import org.parre.client.rpt.event.ManageRptNavEvent;
import org.parre.client.util.Presenter;
import org.parre.client.util.View;
import org.parre.client.util.ViewContainer;

/**
 * Raju Kanumuri 10/21/2013
 */
public class RptPresenter implements Presenter {
	private Integer manageRptViewIndex;
	private EventBus eventBus;
	private Display display;
	

	public interface Display extends View, ViewContainer {
		
	}

	public RptPresenter(Display display) {
		
		this.eventBus = ClientSingleton.getEventBus();
		this.display = display;
	}

	public void go() {
		initViewLoaders();
		bind();
		display.showView(manageRptViewIndex);
	}

	private void initViewLoaders() {
		View view = ViewFactory.createManageRptView();
		view.getPresenter().go();
		manageRptViewIndex = display.addView(view.asWidget());
	}

	private void bind() {
			eventBus.addHandler(ReportsNavEvent.TYPE, new ReportsNavEvent.Handler() {
					public void onEvent(ReportsNavEvent event) {
						if (!ClientSingleton.hasBaseline()) {
							return;
						}
						display.showView(manageRptViewIndex);
						eventBus.fireEvent(ManageRptNavEvent.create());
					}
			});		
			
	}
	
	
}
