/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.rpt.presenter;

import java.util.ArrayList;
import java.util.List;

import org.parre.client.ClientSingleton;
import org.parre.client.messaging.ServiceFactory;
import org.parre.client.rpt.event.CalConsequencesGridEvent;
import org.parre.client.rpt.event.CriticalAssetsGridEvent;
import org.parre.client.rpt.event.EditReportAssessmentTaskGridEvent;
import org.parre.client.rpt.event.EditReportInvestmentOptionGridEvent;
import org.parre.client.rpt.event.ManageRptNavEvent;
import org.parre.client.rpt.event.ReportAssessmentTaskGridEvent;
import org.parre.client.rpt.event.ReportInvestmentOptionGridEvent;
import org.parre.client.rpt.event.ReportSidebarNavEvent;
import org.parre.client.rpt.event.RiskManagementGridEvent;
import org.parre.client.rpt.event.RiskReductionGridEvent;
import org.parre.client.rpt.event.ThreatLikelihoodGridEvent;
import org.parre.client.rpt.event.VulnerabilityGridEvent;
import org.parre.client.util.ActionSource;
import org.parre.client.util.BaseAsyncCommand;
import org.parre.client.util.BusyHandler;
import org.parre.client.util.ExecutableAsyncCallback;
import org.parre.client.util.MessageDialog;
import org.parre.client.util.ModelView;
import org.parre.client.util.Presenter;
import org.parre.client.util.ValueButton;
import org.parre.shared.ParreAnalysisReportSectionDTO;
import org.parre.shared.ReportAssessmentTaskDTO;
import org.parre.shared.ReportInvestmentOptionDTO;
import org.parre.shared.ReportSectionLookupDTO;
import org.parre.shared.RptAssessmentDTO;
import org.parre.shared.RptAssetDTO;
import org.parre.shared.RptConsequenceDTO;
import org.parre.shared.RptMainDTO;
import org.parre.shared.RptRankingsDTO;
import org.parre.shared.RptThreatLikelihoodDTO;
import org.parre.shared.RptVulnerabilityDTO;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Widget;

/**
 * User: Raju Kanumuri 
 * Date: 10/21/2013 
 */
public class ManageRptPresenter implements Presenter {
	
	private EventBus eventBus;
    protected Display display;
    private BusyHandler busyHandler;
    ReportSectionLookupDTO dto = null;
	
	public interface Display extends ModelView<RptMainDTO> {	
		HasClickHandlers getSaveButton();
		HasClickHandlers getPdfReportButton();
		void setRptAnalysis(RptMainDTO rptInfoDTO);
		void setSidebarClickHandler(ClickHandler clickHandler);
		void updateReportSection(ReportSectionLookupDTO dto);
		void updateReportSectionValues(ParreAnalysisReportSectionDTO dto);
		ParreAnalysisReportSectionDTO getReportSectionValues();
		void setCriticalAssetsGrid(List<RptAssetDTO> dtos);
		void setConsequencesGrid(List<RptConsequenceDTO> dtos);
		void setVulnerabilityGrid(List<RptVulnerabilityDTO> dtos);
		void setThreatLikelihoodGrid(List<RptThreatLikelihoodDTO> dtos);
		void setRiskManagementGrid(List<RptRankingsDTO> dtos);
		void setRiskReductionGrid(List<RptAssessmentDTO> dtos);		
		void setDefaultTab();
		void setDisplayTableHandler(SelectionHandler<Integer> dataTable);
		void setReportAssessmentTaskGrid(List<ReportAssessmentTaskDTO> dtos);
		void setReportInvestmentOptionGrid(List<ReportInvestmentOptionDTO> dtos);
		
		HasClickHandlers getAddTaskButton();
		void showAddTaskPopup();
		void showEditTaskPopup();
		void hideEditTaskPopup();
		void showEditOptionPopup();
		void hideEditOptionPopup();
		ActionSource<ReportAssessmentTaskDTO> getTaskSelectionSource();
		ActionSource<ReportInvestmentOptionDTO> getOptionSelectionSource();
		
	}

	
	public ManageRptPresenter(Display display) {
		this.eventBus = ClientSingleton.getEventBus();
		this.display = display;
		this.busyHandler = new BusyHandler(display);
	}

	public void go() {
		bind();
	}

	private void bind() {
		display.setSidebarClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				Widget sender = (Widget) event.getSource();
				
				if (sender instanceof ValueButton) {
					dto = (ReportSectionLookupDTO)((ValueButton)sender).getValue();
				}
				eventBus.fireEvent(ReportSidebarNavEvent.create(dto));
			}
		});
		
		display.setDisplayTableHandler(new SelectionHandler<Integer>() {			
			@Override
			public void onSelection(SelectionEvent<Integer> event) {					
			String dataGridName = null;
				if((dto != null) && (!dto.equals(null)))
				{
					dataGridName = dto.getName();											
					//GWT.log("Button Name == " + dataGridName);						
					if(dataGridName.equalsIgnoreCase("Critical Assets")){							
						List<RptAssetDTO> criticalAssets = new ArrayList<RptAssetDTO>();
						eventBus.fireEvent(CriticalAssetsGridEvent.create(criticalAssets));
					}else if(dataGridName.equalsIgnoreCase("Calculation of Consequences")){							
						List<RptConsequenceDTO> consequences = new ArrayList<RptConsequenceDTO>();
						eventBus.fireEvent(CalConsequencesGridEvent.create(consequences));
					}else if((dataGridName.equalsIgnoreCase("Expert Elicitation")  || dataGridName.equalsIgnoreCase("Path Analysis Discussion") || dataGridName.equalsIgnoreCase("Event Tree Discussion"))){						
						List<RptVulnerabilityDTO> vulnerabilities = new ArrayList<RptVulnerabilityDTO>();
						eventBus.fireEvent(VulnerabilityGridEvent.create(vulnerabilities));
					}else if((dataGridName.equalsIgnoreCase("Best Estimate")) || (dataGridName.equalsIgnoreCase("Proxy Analysis")) || (dataGridName.equalsIgnoreCase("Conditional Likelihood")) ){						
						List<RptThreatLikelihoodDTO> threatLikelihoods = new ArrayList<RptThreatLikelihoodDTO>();
						eventBus.fireEvent(ThreatLikelihoodGridEvent.create(threatLikelihoods));
					}else if(dataGridName.equalsIgnoreCase("Risk Management")){						
						List<RptRankingsDTO> riskManagement = new ArrayList<RptRankingsDTO>();
						eventBus.fireEvent(RiskManagementGridEvent.create(riskManagement));
					}else if(dataGridName.equalsIgnoreCase("Risk Reduction Options")){						
						//For Data Display Table
						List<RptAssessmentDTO> riskReduction = new ArrayList<RptAssessmentDTO>();
						eventBus.fireEvent(RiskReductionGridEvent.create(riskReduction));		
					}else if(dataGridName.equalsIgnoreCase("Assessment Tasks")){						
						List<ReportAssessmentTaskDTO> assessmentTask = new ArrayList<ReportAssessmentTaskDTO>();
						eventBus.fireEvent(ReportAssessmentTaskGridEvent.create(assessmentTask));
					}
				}
			}
		});		
		
		eventBus.addHandler(ManageRptNavEvent.TYPE,
				new ManageRptNavEvent.Handler() {
					public void onEvent(ManageRptNavEvent event) {
						
					}
				});
		
		eventBus.addHandler(ReportSidebarNavEvent.TYPE, new ReportSidebarNavEvent.Handler() {
				Long reportSectionLookupId = 0L;
				public void onEvent(ReportSidebarNavEvent event) {	
					display.setDefaultTab();
					display.updateReportSection(event.getReportSectionLookupDTO());
					reportSectionLookupId = event.getReportSectionLookupDTO().getId();
				
					ExecutableAsyncCallback<ParreAnalysisReportSectionDTO> exec = new ExecutableAsyncCallback<ParreAnalysisReportSectionDTO>(busyHandler, new BaseAsyncCommand<ParreAnalysisReportSectionDTO>() {
	                    public void execute(AsyncCallback<ParreAnalysisReportSectionDTO> callback) {
	                    	ServiceFactory.getInstance().getReportsService().getReportSection(ClientSingleton.getParreAnalysisId(), 
	                    				reportSectionLookupId, callback);	
	                    }
	
	                    @Override
	                    public void handleResult(ParreAnalysisReportSectionDTO result) {
	                    	display.updateReportSectionValues(result);
	                    }
	
	                    @Override
	                    public boolean handleError(Throwable t) {
	                        MessageDialog.popup(t.getMessage(), true);
	                        return true;
	                    }
	                });
	               exec.makeCall();
				}
			});
		
		eventBus.addHandler(CriticalAssetsGridEvent.TYPE, new CriticalAssetsGridEvent.Handler() {			
			public void onEvent(CriticalAssetsGridEvent event) {		
				ExecutableAsyncCallback<List<RptAssetDTO>> exec = new ExecutableAsyncCallback<List<RptAssetDTO>>(busyHandler, new BaseAsyncCommand<List<RptAssetDTO>>() {
                    public void execute(AsyncCallback<List<RptAssetDTO>> callback) {
                    	ServiceFactory.getInstance().getReportsService().getReportDisplayTableAssets(ClientSingleton.getParreAnalysisId(), callback);	
                    }

                    @Override
                    public void handleResult(List<RptAssetDTO> result) {
                    	display.setCriticalAssetsGrid(result);
                    }

                    @Override
                    public boolean handleError(Throwable t) {
                        MessageDialog.popup(t.getMessage(), true);
                        return true;
                    }
                });
               exec.makeCall();
			}
		});
		
		eventBus.addHandler(CalConsequencesGridEvent.TYPE, new CalConsequencesGridEvent.Handler() {			
			public void onEvent(CalConsequencesGridEvent event) {		
				ExecutableAsyncCallback<List<RptConsequenceDTO>> exec = new ExecutableAsyncCallback<List<RptConsequenceDTO>>(busyHandler, new BaseAsyncCommand<List<RptConsequenceDTO>>() {
                    public void execute(AsyncCallback<List<RptConsequenceDTO>> callback) {
                    	ServiceFactory.getInstance().getReportsService().getReportDisplayTableConsequences(ClientSingleton.getParreAnalysisId(), callback);	
                    }

                    @Override
                    public void handleResult(List<RptConsequenceDTO> result) {
                    	display.setConsequencesGrid(result);
                    }

                    @Override
                    public boolean handleError(Throwable t) {
                        MessageDialog.popup(t.getMessage(), true);
                        return true;
                    }
                });
               exec.makeCall();
			}
		});
		
		eventBus.addHandler(VulnerabilityGridEvent.TYPE, new VulnerabilityGridEvent.Handler() {			
			public void onEvent(VulnerabilityGridEvent event) {		
				ExecutableAsyncCallback<List<RptVulnerabilityDTO>> exec = new ExecutableAsyncCallback<List<RptVulnerabilityDTO>>(busyHandler, new BaseAsyncCommand<List<RptVulnerabilityDTO>>() {
                    public void execute(AsyncCallback<List<RptVulnerabilityDTO>> callback) {
                    	ServiceFactory.getInstance().getReportsService().getReportDisplayTableVulnerability(ClientSingleton.getParreAnalysisId(), callback);	
                    }

                    @Override
                    public void handleResult(List<RptVulnerabilityDTO> result) {
                    	display.setVulnerabilityGrid(result);
                    }

                    @Override
                    public boolean handleError(Throwable t) {
                        MessageDialog.popup(t.getMessage(), true);
                        return true;
                    }
                });
               exec.makeCall();
			}
		});
		
		eventBus.addHandler(ThreatLikelihoodGridEvent.TYPE, new ThreatLikelihoodGridEvent.Handler() {			
			public void onEvent(ThreatLikelihoodGridEvent event) {		
				ExecutableAsyncCallback<List<RptThreatLikelihoodDTO>> exec = new ExecutableAsyncCallback<List<RptThreatLikelihoodDTO>>(busyHandler, new BaseAsyncCommand<List<RptThreatLikelihoodDTO>>() {
                    public void execute(AsyncCallback<List<RptThreatLikelihoodDTO>> callback) {
                    	ServiceFactory.getInstance().getReportsService().getReportDisplayTableThreatLikelihood(ClientSingleton.getParreAnalysisId(), callback);	
                    }

                    @Override
                    public void handleResult(List<RptThreatLikelihoodDTO> result) {
                    	display.setThreatLikelihoodGrid(result);
                    }

                    @Override
                    public boolean handleError(Throwable t) {
                        MessageDialog.popup(t.getMessage(), true);
                        return true;
                    }
                });
               exec.makeCall();
			}
		});
		
		eventBus.addHandler(RiskManagementGridEvent.TYPE, new RiskManagementGridEvent.Handler() {			
			public void onEvent(RiskManagementGridEvent event) {		
				ExecutableAsyncCallback<List<RptRankingsDTO>> exec = new ExecutableAsyncCallback<List<RptRankingsDTO>>(busyHandler, new BaseAsyncCommand<List<RptRankingsDTO>>() {
                    public void execute(AsyncCallback<List<RptRankingsDTO>> callback) {
                    	ServiceFactory.getInstance().getReportsService().getReportDisplayTableRiskManagement(ClientSingleton.getParreAnalysisId(), callback);	
                    }

                    @Override
                    public void handleResult(List<RptRankingsDTO> result) {
                    	display.setRiskManagementGrid(result);
                    }

                    @Override
                    public boolean handleError(Throwable t) {
                        MessageDialog.popup(t.getMessage(), true);
                        return true;
                    }
                });
               exec.makeCall();
			}
		});
		
		eventBus.addHandler(RiskReductionGridEvent.TYPE, new RiskReductionGridEvent.Handler() {			
			public void onEvent(RiskReductionGridEvent event) {		
				ExecutableAsyncCallback<List<RptAssessmentDTO>> exec = new ExecutableAsyncCallback<List<RptAssessmentDTO>>(busyHandler, new BaseAsyncCommand<List<RptAssessmentDTO>>() {
                    public void execute(AsyncCallback<List<RptAssessmentDTO>> callback) {
                    	ServiceFactory.getInstance().getReportsService().getReportDisplayTableRiskReduction(ClientSingleton.getParreAnalysisId(), callback);	
                    }

                    @Override
                    public void handleResult(List<RptAssessmentDTO> result) {
                    	display.setRiskReductionGrid(result);
                    	//For Data Input Table
                    	List<ReportInvestmentOptionDTO> investmentOption = new ArrayList<ReportInvestmentOptionDTO>();
						eventBus.fireEvent(ReportInvestmentOptionGridEvent.create(investmentOption));
                    }

                    @Override
                    public boolean handleError(Throwable t) {
                        MessageDialog.popup(t.getMessage(), true);
                        return true;
                    }
                });
               exec.makeCall();
			}
		});
		
		eventBus.addHandler(ReportAssessmentTaskGridEvent.TYPE, new ReportAssessmentTaskGridEvent.Handler() {			
			public void onEvent(ReportAssessmentTaskGridEvent event) {		
				ExecutableAsyncCallback<List<ReportAssessmentTaskDTO>> exec = new ExecutableAsyncCallback<List<ReportAssessmentTaskDTO>>(busyHandler, new BaseAsyncCommand<List<ReportAssessmentTaskDTO>>() {
                    public void execute(AsyncCallback<List<ReportAssessmentTaskDTO>> callback) {
                    	ServiceFactory.getInstance().getReportsService().getReportAssessmentTasks(ClientSingleton.getParreAnalysisId(), callback);	
                    }

                    @Override
                    public void handleResult(List<ReportAssessmentTaskDTO> result) {
                    	display.hideEditTaskPopup();
                    	display.setReportAssessmentTaskGrid(result);
                    }

                    @Override
                    public boolean handleError(Throwable t) {
                        MessageDialog.popup(t.getMessage(), true);
                        return true;
                    }
                });
               exec.makeCall();
			}
		});
		
		
		eventBus.addHandler(ReportInvestmentOptionGridEvent.TYPE, new ReportInvestmentOptionGridEvent.Handler() {			
			public void onEvent(ReportInvestmentOptionGridEvent event) {		
				ExecutableAsyncCallback<List<ReportInvestmentOptionDTO>> exec = new ExecutableAsyncCallback<List<ReportInvestmentOptionDTO>>(busyHandler, new BaseAsyncCommand<List<ReportInvestmentOptionDTO>>() {
                    public void execute(AsyncCallback<List<ReportInvestmentOptionDTO>> callback) {
                    	ServiceFactory.getInstance().getReportsService().getReportInvestmentOptions(ClientSingleton.getParreAnalysisId(), callback);	
                    }

                    @Override
                    public void handleResult(List<ReportInvestmentOptionDTO> result) {
                    	display.hideEditOptionPopup();
                    	display.setReportInvestmentOptionGrid(result);
                    }

                    @Override
                    public boolean handleError(Throwable t) {
                        MessageDialog.popup(t.getMessage(), true);
                        return true;
                    }
                });
               exec.makeCall();
			}
		});
		

		display.getPdfReportButton().addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				// On Click of the PDF Report button.. call the Jasper Report from src/main/resources/..
				String url = GWT.getModuleBaseURL() + "reports?paId=" + ClientSingleton.getParreAnalysisId();
				String name = "_blank";
				String features = "enabled";
				Window.open(url, name, features);
			}
		});
		
		display.getSaveButton().addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                ExecutableAsyncCallback<ParreAnalysisReportSectionDTO> exec = new ExecutableAsyncCallback<ParreAnalysisReportSectionDTO>(busyHandler, new BaseAsyncCommand<ParreAnalysisReportSectionDTO>() {
                    public void execute(AsyncCallback<ParreAnalysisReportSectionDTO> callback) {
                    	ServiceFactory.getInstance().getReportsService().saveReportSection(ClientSingleton.getParreAnalysisId(), display.getReportSectionValues(), callback);
                    }

                    @Override
                    public void handleResult(ParreAnalysisReportSectionDTO result) {
                        display.updateReportSectionValues(result);
                    }

                    @Override
                    public boolean handleError(Throwable t) {
                        MessageDialog.popup(t.getMessage(), true);
                        return true;
                    }
                });
                exec.makeCall();
            }
        });
		
		display.getAddTaskButton().addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
            	display.showAddTaskPopup();
            }
        });
		
		display.getTaskSelectionSource().setCommand(new ActionSource.ActionCommand<ReportAssessmentTaskDTO>() {
            public void execute(ReportAssessmentTaskDTO dto) {
                editTask(dto);
            }
        });
		
		display.getOptionSelectionSource().setCommand(new ActionSource.ActionCommand<ReportInvestmentOptionDTO>() {
            public void execute(ReportInvestmentOptionDTO dto) {
                editOption(dto);
            }
        });
		
	}
	
	private void editTask(ReportAssessmentTaskDTO dto) {
        eventBus.fireEvent(EditReportAssessmentTaskGridEvent.create(dto));
        display.showEditTaskPopup();
    }
	
	private void editOption(ReportInvestmentOptionDTO dto) {
        eventBus.fireEvent(EditReportInvestmentOptionGridEvent.create(dto));
        display.showEditOptionPopup();
    }
		
}
