/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.main.presenter;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.HasText;
import com.google.gwt.user.client.ui.HasValue;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.view.client.SelectionChangeEvent;
import com.google.gwt.view.client.SingleSelectionModel;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.parre.client.ClientSingleton;
import org.parre.client.event.EditParreAnalysisEvent;
import org.parre.client.event.ParreAnalysisCreatedEvent;
import org.parre.client.event.ParreAnalysisUpdatedEvent;
import org.parre.client.main.event.*;
import org.parre.client.messaging.ServiceFactory;
import org.parre.client.util.*;
import org.parre.shared.*;
import org.parre.shared.exception.DuplicateItemException;

/**
 * The Class EditParreAnalysisPresenter.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 11/28/11
 */
public class EditParreAnalysisPresenter implements Presenter {
    private Display display;
    private BusyHandler busyHandler;
    private EventBus eventBus;
    private ParreAnalysisDTO currentAnalysis;
    private List<ProposedMeasureDTO> baselineProposedMeasures;

    /**
     * The Interface Display.
     */
    public interface Display extends ModelView<ParreAnalysisDTO> {

        ListBox getContacts();

        ListBox getLocations();

        void showCreateLocationDialog();

        void hideCreateLocationDialog();

        HasClickHandlers getSaveButton();

        HasText getFatality();

        HasText getSeriousInjury();

        HasValue<Boolean> getOverrideFatality();

        void updateFatalityDisplay();

        HasValue<Boolean> getOverrideSeriousInjury();

        void updateSeriousInjuryDisplay();

        HasValue<Boolean> getUseStatValues();

        SingleSelectionModel<ProposedMeasureDTO> getPmSelectionModel();

        ActionSource<ProposedMeasureDTO> getPmSelectionSource();

        List<ProposedMeasureDTO> getPmData();

        void setPmData(List<ProposedMeasureDTO> data);

        HasClickHandlers getRemovePmButton();

        HasClickHandlers getAddPmButton();

        void showSelectPmPopup(List<ProposedMeasureDTO> baselineProposedMeasures);

        void hideSelectPmPopup();

        HasClickHandlers getSelectPmButton();

        HasClickHandlers getCreatePmButton();

        ListBox getProposedMeasures();

        void refreshProposeMeasureView();

        void showEditPmPopup();

        void hideEditPmPopup();

        void showConfirmRemovePmPopup();

        HasClickHandlers getConfirmRemovePmButton();

        void hideConfirmRemovePmPopup();

        HasClickHandlers getCreateLocationButton();

        HasClickHandlers getEditLocationButton();

        void showCreateContactDialog();

        void hideCreateContactDialog();

        HasClickHandlers getCreateContactButton();

        HasClickHandlers getEditContactButton();
        
        HasClickHandlers getGISCoordinatesButton();
        
        HasText getZipCode();

        void setGISCoordinates(String latitude, String longitude);
    }

    public EditParreAnalysisPresenter(Display display) {
        this.display = display;
        this.eventBus = ClientSingleton.getEventBus();
    }

    public void go() {
        bind();
    }

    private void bind() {
        busyHandler = new BusyHandler(display);
        currentAnalysis = new ParreAnalysisDTO();
        display.setModelAndUpdateView(currentAnalysis);
        populateContacts("Select");
        populateLocations("Select");
        bindEvents();
        bindActions();
    }

    private void bindActions() {
        display.getUseStatValues().addValueChangeHandler(new ValueChangeHandler<Boolean>() {
            public void onValueChange(ValueChangeEvent<Boolean> event) {
                Boolean value = event.getValue();
                if (value) {
                    updateStatValuesDisplay();
                }
            }
        });
        display.getSaveButton().addClickHandler(new ValidatingClickHandler(display) {
            @Override
            protected void handleClick(ClickEvent event) {
                saveParreAnalysis();
            }
        });

        display.getOverrideFatality().addValueChangeHandler(new ValueChangeHandler<Boolean>() {
            public void onValueChange(ValueChangeEvent<Boolean> event) {
                updateFatality(event.getValue());
            }
        });

        display.getOverrideSeriousInjury().addValueChangeHandler(new ValueChangeHandler<Boolean>() {
            public void onValueChange(ValueChangeEvent<Boolean> event) {
                updateSeriousInjury(event.getValue());
            }
        });
        display.getPmSelectionModel().addSelectionChangeHandler(new SelectionChangeEvent.Handler() {
            public void onSelectionChange(SelectionChangeEvent event) {

            }
        });

        display.getPmSelectionSource().setCommand(new ActionSource.ActionCommand<ProposedMeasureDTO>() {
            public void execute(ProposedMeasureDTO proposedMeasureDTO) {
                eventBus.fireEvent(EditProposedMeasureEvent.create(proposedMeasureDTO));
                display.showEditPmPopup();
            }
        });

        display.getAddPmButton().addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                initAndShowSelectPmDialog();
            }
        });
        display.getRemovePmButton().addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                display.showConfirmRemovePmPopup();
            }
        });
        display.getConfirmRemovePmButton().addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                display.hideConfirmRemovePmPopup();
                disassociatePm(display.getPmSelectionModel().getSelectedObject());
            }
        });
        display.getSelectPmButton().addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                Long pmId = ViewUtil.getSelectedId(display.getProposedMeasures());
                associateProposedMeasure(pmId);
            }
        });
        display.getCreatePmButton().addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                display.hideSelectPmPopup();
                eventBus.fireEvent(EditProposedMeasureEvent.create(new ProposedMeasureDTO(currentAnalysis.getId())));
                display.showEditPmPopup();
            }
        });
        display.getCreateLocationButton().addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent clickEvent) {
                display.showCreateLocationDialog();
                eventBus.fireEvent(EditLocationEvent.create(new SiteLocationDTO()));
            }
        });
        display.getCreateContactButton().addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent clickEvent) {
                display.showCreateContactDialog();
                eventBus.fireEvent(EditContactEvent.create(new ContactDTO()));
            }
        });
        display.getEditContactButton().addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                if(display.getContacts().getValue(display.getContacts().getSelectedIndex()).equals("")) {
                    MessageDialog.popup("Please select a contact.");
                    return;
                }
                ExecutableAsyncCallback<ContactDTO> exec = new ExecutableAsyncCallback<ContactDTO>(busyHandler,
                        new AsyncCommand<ContactDTO>() {
                            @Override
                            public void execute(AsyncCallback<ContactDTO> contactDTOAsyncCallback) {
                                String value = display.getContacts().getValue(display.getContacts().getSelectedIndex());
                                ServiceFactory.getInstance().getParreService().getContact(
                                        Long.valueOf(value), contactDTOAsyncCallback);
                            }

                            @Override
                            public void handleResult(ContactDTO result) {
                                display.showCreateContactDialog();
                                eventBus.fireEvent(EditContactEvent.create(result));
                            }

                            @Override
                            public boolean handleError(Throwable t) {
                                return false;
                            }
                        });
                exec.makeCall();

            }
        });
        display.getEditLocationButton().addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                if(display.getLocations().getValue(display.getLocations().getSelectedIndex()).equals("")) {
                    MessageDialog.popup("Please select a location.");
                    return;
                }
                ExecutableAsyncCallback<SiteLocationDTO> exec = new ExecutableAsyncCallback<SiteLocationDTO>(busyHandler,
                        new AsyncCommand<SiteLocationDTO>() {
                            @Override
                            public void execute(AsyncCallback<SiteLocationDTO> siteLocationDTOAsyncCallback) {
                                String value = display.getLocations().getValue(display.getLocations().getSelectedIndex());
                                ServiceFactory.getInstance().getParreService().getSiteLocation(Long.valueOf(value), siteLocationDTOAsyncCallback);
                            }

                            @Override
                            public void handleResult(SiteLocationDTO result) {
                                display.showCreateLocationDialog();
                                eventBus.fireEvent(EditLocationEvent.create(result));
                            }

                            @Override
                            public boolean handleError(Throwable t) {
                                return false;
                            }
                        });
                exec.makeCall();
            }
        });
        
        display.getGISCoordinatesButton().addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent clickEvent) {
                ExecutableAsyncCallback<ZipLookupDataDTO> exec  = new ExecutableAsyncCallback<ZipLookupDataDTO>(busyHandler, new BaseAsyncCommand<ZipLookupDataDTO>() {
                    public void execute(AsyncCallback<ZipLookupDataDTO> callback) {
                        ServiceFactory.getInstance().getAssetService().getZipLookupData(display.getZipCode().getText(), callback);
                    }
                    @Override
                    public void handleResult(ZipLookupDataDTO dto) {
                        try {
                            display.setGISCoordinates(dto.getLatitude().stripTrailingZeros().toString(), dto.getLongitude().stripTrailingZeros().toString());
                        } catch(Exception e) {
                            MessageDialog.popup("ZIP Code not found.");
                            display.setGISCoordinates("", "");
                        }

                    }
                });
                exec.makeCall();
            }
        });
    }

    private void bindEvents() {
        eventBus.addHandler(EditParreAnalysisEvent.TYPE, new EditParreAnalysisEvent.Handler() {
            public void onEvent(EditParreAnalysisEvent event) {
                currentAnalysis = event.getParreAnalysisDTO();
                populateProposedMeasures();
                display.setModelAndUpdateView(currentAnalysis);
                if (currentAnalysis.getUseStatValues()) {
                    updateStatValuesDisplay();
                }
            }
        });

        eventBus.addHandler(LocationUpdatedEvent.TYPE, new LocationUpdatedEvent.Handler() {
            public void onEvent(LocationUpdatedEvent event) {
                display.hideCreateLocationDialog();
                display.getLocations().clear();
                populateLocations(event.getDto().getName());
            }
        });

        eventBus.addHandler(ContactUpdatedEvent.TYPE, new ContactUpdatedEvent.Handler() {
            public void onEvent(ContactUpdatedEvent event) {
                display.hideCreateContactDialog();
                display.getContacts().clear();
                populateContacts(event.getDto().getContactName());

            }
        });

        eventBus.addHandler(ProposedMeasureCreatedEvent.TYPE, new ProposedMeasureCreatedEvent.Handler() {
            public void onEvent(ProposedMeasureCreatedEvent event) {
                display.hideEditPmPopup();
                baselineProposedMeasures.add(event.getProposedMeasureDTO());
                display.getPmData().add(event.getProposedMeasureDTO());
                display.refreshProposeMeasureView();
            }
        });
        eventBus.addHandler(ProposedMeasureUpdatedEvent.TYPE, new ProposedMeasureUpdatedEvent.Handler() {
            public void onEvent(ProposedMeasureUpdatedEvent event) {
                display.hideEditPmPopup();
                ProposedMeasureDTO dto = event.getProposedMeasureDTO();
                List<ProposedMeasureDTO> list = baselineProposedMeasures;
                if (list != null) {
                    ViewUtil.updateEntry(dto, list);
                }
                ViewUtil.updateEntry(dto, display.getPmData());
                display.refreshProposeMeasureView();
            }
        });
    }


    private void disassociatePm(final ProposedMeasureDTO selectedObject) {

        new ExecutableAsyncCallback<Void>(busyHandler, new BaseAsyncCommand<Void>() {
            public void execute(AsyncCallback<Void> callback) {
                ServiceFactory.getInstance().getParreService().disassociateProposedMeasure(selectedObject.getId(), currentAnalysis.getId(), callback);
            }

            @Override
            public void handleResult(Void results) {
                display.getPmData().remove(selectedObject);
                display.refreshProposeMeasureView();
            }
        }).makeCall();

    }

    private void associateProposedMeasure(final Long pmId) {

        new ExecutableAsyncCallback<Void>(busyHandler, new BaseAsyncCommand<Void>() {
            public void execute(AsyncCallback<Void> callback) {
                ServiceFactory.getInstance().getParreService().associateProposedMeasure(pmId, currentAnalysis.getId(), callback);
            }

            @Override
            public void handleResult(Void results) {
                ProposedMeasureDTO baselinePm = ViewUtil.findDTO(pmId, baselineProposedMeasures);
                display.getPmData().add(baselinePm);
                display.hideSelectPmPopup();
                display.refreshProposeMeasureView();
            }
        }).makeCall();

    }

    private void initAndShowSelectPmDialog() {
        if (baselineProposedMeasures != null) {
            showSelectPmDialog();
        } else {
            initBaselinePms();
        }

    }

    private void showSelectPmDialog() {
        List<ProposedMeasureDTO> allMeasures = new ArrayList<ProposedMeasureDTO>(baselineProposedMeasures);
        allMeasures.removeAll(display.getPmData());
        display.showSelectPmPopup(allMeasures);
    }

    private void initBaselinePms() {
        new ExecutableAsyncCallback<List<ProposedMeasureDTO>>(busyHandler, new BaseAsyncCommand<List<ProposedMeasureDTO>>() {
            public void execute(AsyncCallback<List<ProposedMeasureDTO>> callback) {
                ServiceFactory.getInstance().getParreService().getBaselineProposedMeasures(currentAnalysis.getBaselineId(), callback);
            }

            @Override
            public void handleResult(List<ProposedMeasureDTO> results) {
                baselineProposedMeasures = results;
                showSelectPmDialog();
            }
        }).makeCall();
    }

    private void populateProposedMeasures() {

        new ExecutableAsyncCallback<List<ProposedMeasureDTO>>(busyHandler, new BaseAsyncCommand<List<ProposedMeasureDTO>>() {
            public void execute(AsyncCallback<List<ProposedMeasureDTO>> callback) {
                ServiceFactory.getInstance().getParreService().getProposedMeasures(currentAnalysis.getId(), callback);
            }

            @Override
            public void handleResult(List<ProposedMeasureDTO> results) {
                display.setPmData(results);
            }
        }).makeCall();

    }

    private void saveParreAnalysis() {
        final ParreAnalysisDTO dto = display.updateAndGetModel();
        final boolean isNew = dto.isNew();
        GWT.log("Is it a New Analysis: "+isNew);
        ExecutableAsyncCallback<ParreAnalysisDTO> exec = new ExecutableAsyncCallback<ParreAnalysisDTO>(busyHandler,
                new BaseAsyncCommand<ParreAnalysisDTO>() {
                    public void execute(AsyncCallback<ParreAnalysisDTO> callback) {
                        ServiceFactory.getInstance().getParreService().saveParreAnalysis(dto, callback);
                    }

                    @Override
                    public boolean handleError(Throwable t) {
                        boolean handleError = t instanceof DuplicateItemException;
                        if (handleError) {
                            MessageDialog.popup(t.getMessage());
                        }
                        return handleError;
                    }

                    @Override
                    public void handleResult(ParreAnalysisDTO parreAnalysisDTO) {
                        currentAnalysis = parreAnalysisDTO;
                        if (isNew) {
                        	GWT.log("Create New Analysis");
                            eventBus.fireEvent(ParreAnalysisCreatedEvent.create(parreAnalysisDTO));
                        } else {
                        	GWT.log("Update Analysis");
                            eventBus.fireEvent(ParreAnalysisUpdatedEvent.create(parreAnalysisDTO));
                            //MessageDialog.popup("Parre Analysis Updated", true);
                        }
                        display.setModelAndUpdateView(parreAnalysisDTO);
                    }
                });
        exec.makeCall();
    }

    private void updateFatality(Boolean override) {
        BigDecimal selectedValue = ClientSingleton.getAppInitData().getStatisticalValuesDTO().getFatalityDTO().getQuantity();

        if (override) {
            if (!currentAnalysis.isNew()) {
                BigDecimal overriddenValue = currentAnalysis.getStatValuesDTO().getFatalityQuantity();
                if (overriddenValue != null) {
                    selectedValue = overriddenValue;
                }
            }
        }
        ViewUtil.setBigDecimal(display.getFatality(), selectedValue);
        display.updateFatalityDisplay();
    }

    private void updateSeriousInjury(Boolean override) {
        BigDecimal selectedValue = ClientSingleton.getAppInitData().getStatisticalValuesDTO().getSeriousInjuryDTO().getQuantity();

        if (override) {
            if (!currentAnalysis.isNew()) {
                BigDecimal overriddenValue = currentAnalysis.getStatValuesDTO().getSeriousInjuryDTO().getQuantity();
                if (overriddenValue != null) {
                    selectedValue = overriddenValue;
                }
            }
        }
        ViewUtil.setBigDecimal(display.getSeriousInjury(), selectedValue);
        display.updateSeriousInjuryDisplay();
    }

    private void updateStatValuesDisplay() {
        updateFatality(display.getOverrideFatality().getValue());
        updateSeriousInjury(display.getOverrideSeriousInjury().getValue());
    }

    private void populateContacts(final String newValue) {
        ExecutableAsyncCallback<List<LabelValueDTO>> exec = new ExecutableAsyncCallback<List<LabelValueDTO>>(busyHandler,
                new AsyncListBoxCommand(display.getContacts()) {
                    @Override
                    protected void getListBoxContents(AsyncCallback<List<LabelValueDTO>> asyncCallback) {
                        ServiceFactory.getInstance().getParreService().getContacts(asyncCallback);

                    }

                    @Override
                    public void handleResult(List<LabelValueDTO> results) {
                        super.handleResult(results);
                        for(int i = 0; i<display.getContacts().getItemCount(); i++) {
                            if(newValue.equals(display.getContacts().getItemText(i))) {
                                display.getContacts().setItemSelected(i, true);
                            }
                        }
                    }

                });
        exec.makeCall();

    }

    private void populateLocations(final String newValue) {
        ExecutableAsyncCallback<List<LabelValueDTO>> exec = new ExecutableAsyncCallback<List<LabelValueDTO>>(busyHandler,
                new AsyncListBoxCommand(display.getLocations()) {
                    @Override
                    protected void getListBoxContents(AsyncCallback<List<LabelValueDTO>> asyncCallback) {
                        ServiceFactory.getInstance().getParreService().getSiteLocationList(asyncCallback);
                    }

                    @Override
                    public void handleResult(List<LabelValueDTO> results) {
                        super.handleResult(results);
                        for(int i = 0; i<display.getLocations().getItemCount(); i++) {
                            if(newValue.equals(display.getLocations().getItemText(i))) {
                                display.getLocations().setItemSelected(i, true);
                            }
                        }
                    }
                });
        exec.makeCall();
    }

}
