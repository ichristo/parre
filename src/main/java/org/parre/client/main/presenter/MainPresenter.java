/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.main.presenter;

import java.util.List;

import org.parre.client.ClientSingleton;
import org.parre.client.ViewFactory;
import org.parre.client.event.ClearNavEvent;
import org.parre.client.event.CurrentParreAnalysisChangedEvent;
import org.parre.client.event.EditParreAnalysisEvent;
import org.parre.client.event.GoHomeEvent;
import org.parre.client.event.LoginEvent;
import org.parre.client.event.LogoutEvent;
import org.parre.client.event.ParreAnalysisCreatedEvent;
import org.parre.client.event.ParreAnalysisUpdatedEvent;
import org.parre.client.main.view.EditParreAnalysisView;
import org.parre.client.main.view.mainNavView;
import org.parre.client.messaging.ServiceFactory;
import org.parre.client.nav.NavViewMapperFactory;
import org.parre.client.nav.ParreNavAdapter;
import org.parre.client.nav.event.RiskBenefitNavEvent;
import org.parre.client.util.ActionSource;
import org.parre.client.util.BaseAsyncCommand;
import org.parre.client.util.BusyHandler;
import org.parre.client.util.ClientLoggingUtil;
import org.parre.client.util.ConfirmDialog;
import org.parre.client.util.ExecutableAsyncCallback;
import org.parre.client.util.MessageDialog;
import org.parre.client.util.Presenter;
import org.parre.client.util.View;
import org.parre.client.util.ViewContainer;
import org.parre.client.util.ViewUtil;
import org.parre.client.util.nav.NavEvent;
import org.parre.client.util.nav.NavEventHandler;
import org.parre.client.util.nav.NavManager;
import org.parre.client.util.nav.NavViewMapper;
import org.parre.shared.LoginInfo;
import org.parre.shared.ParreAnalysisDTO;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.HasText;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.Widget;
import com.google.web.bindery.event.shared.EventBus;

/**
 * The Class MainPresenter.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 7/20/11
 */
public class MainPresenter implements Presenter {
    private NavViewMapper navViewMapper;
    private Integer parreAnalysisViewIndex;

    private EventBus eventBus;
    private Display display;
    private LoginInfo loginInfo;
    private BusyHandler busyHandler;
    private List<ParreAnalysisDTO> selectableParreAnalyses;
    
    private boolean firstDisplay = true;

    /**
     * The Interface Display.
     */
    public interface Display extends ViewContainer, View {
        HasClickHandlers getSignOutLink();

        HasClickHandlers getHomeLink();

        Widget asWidget();

        HasText getUserNameLabel();

        HasClickHandlers getManageParreAnalysisButton();

        HasText getCenterText2();

        void setParreAnalysisName(String name);

        ListBox getActiveParreAnalyses();

        HasClickHandlers getCopyAnalysisButton();

        HasClickHandlers getCreateAnalysisButton();

        HasClickHandlers getSelectAnalysisButton();

        HasClickHandlers getEditAnalysisButton();

        void showSelectAnalysisPopup(boolean autoDisplay);

        void hideSelectAnalysisPopup();

        void setCurrentViewTitle(String viewTitle);

        HasClickHandlers getHelpButton();

        HasClickHandlers getDeleteAnalysisButton();

        void showDeleteDialog();

        HasClickHandlers getConfirmDeleteButton();

        void hideDeleteDialog();
        
        String getHelpAnchor();
    }

    public MainPresenter(Display display) {
        this.eventBus = ClientSingleton.getEventBus();
        this.display = display;
        this.loginInfo = ClientSingleton.getLoginInfo();
    }


    public void go() {
        busyHandler = new BusyHandler(display);
        this.navViewMapper = NavViewMapperFactory.createNavViewMapper(display);
        initParreAnalysisView();
        bind();
        initActiveParreAnalyses();
        ParreAnalysisDTO parreAnalysisDTO = ClientSingleton.getAppInitData().getParreAnalysisDTO();
        if (parreAnalysisDTO != null) {
            showHomePage(parreAnalysisDTO);
        } else {
            display.showSelectAnalysisPopup(true);
        }
    }

    private void initActiveParreAnalyses() {
        selectableParreAnalyses = ClientSingleton.getAppInitData().getActiveParreAnalyses();
        refreshActiveAnalysesListBox();
    }

    private void refreshActiveAnalysesListBox() {
        ListBox parreAnalyses = display.getActiveParreAnalyses();
        parreAnalyses.clear();
        parreAnalyses.addItem("Select", "");
        for (ParreAnalysisDTO activeParreAnalyse : selectableParreAnalyses) {
            parreAnalyses.addItem(activeParreAnalyse.getName(), String.valueOf(activeParreAnalyse.getId()));
            if (activeParreAnalyse.hasOptionAnalyses()) {
                List<ParreAnalysisDTO> optionDTOs = activeParreAnalyse.getOptionParreAnalysisDTOs();
                for (ParreAnalysisDTO optionDTO : optionDTOs) {
                    parreAnalyses.addItem("--- " + optionDTO.getLabel(), optionDTO.getValue());
                }
            }
        }
    }

    private void showHomePage(ParreAnalysisDTO parreAnalysisDTO) {
        String name = parreAnalysisDTO.getName();
        if (parreAnalysisDTO.isOption()) {
            ParreAnalysisDTO baseline = findParreAnalysis(parreAnalysisDTO.getBaselineId());
            name = baseline.getName() + " - " + parreAnalysisDTO.getName();
        }
        display.setParreAnalysisName(name);
        displayHome();
        //navViewMapper.showView(mainNavView.class);
        //eventBus.fireEvent(GoHomeEvent.create());
    }


    private void initParreAnalysisView() {
        if (parreAnalysisViewIndex != null) {
            return;
        }
        EditParreAnalysisView view = ViewFactory.createParreAnalysisView();
        view.getPresenter().go();
        parreAnalysisViewIndex = display.addView(view.asWidget());

    }

    private void bind() {
        display.getUserNameLabel().setText(loginInfo.getNickname());
        bindEvents();
        bindActions();
    }

    private void bindEvents() {
        ParreNavAdapter.getInstance().setNavEventHandler(new NavEventHandler() {
            public void onNavEvent(NavEvent navEvent) {
                if (RiskBenefitNavEvent.isInstance(navEvent)) {
                    ParreAnalysisDTO currentParreAnalysis = ClientSingleton.getCurrentParreAnalysis();
                    if (currentParreAnalysis == null) {
                        return;
                    }
                    if (currentParreAnalysis.isOption()) {
                        currentParreAnalysis = findParreAnalysis(currentParreAnalysis.getBaselineId());
                    }
                    if (!currentParreAnalysis.isBaseline() && !currentParreAnalysis.hasOptionAnalyses()) {
                        MessageDialog.popup("A baseline must exist before this analysis can be performed!");
                        return;
                    }

                }
                String navViewId = navEvent.getNavViewId();
                navViewMapper.showView(navViewId);
            }
        });
        eventBus.addHandler(ParreAnalysisCreatedEvent.TYPE, new ParreAnalysisCreatedEvent.Handler() {
            public void onEvent(ParreAnalysisCreatedEvent event) {
                ParreAnalysisDTO newParreAnalysisDTO = event.getParreAnalysisDTO();
                updateActiveParreAnalysesList(newParreAnalysisDTO);
                selectParreAnalysis(newParreAnalysisDTO.getId());
                NavManager.performNav(GoHomeEvent.create());
            }
        });
        eventBus.addHandler(ParreAnalysisUpdatedEvent.TYPE, new ParreAnalysisUpdatedEvent.Handler() {
            public void onEvent(ParreAnalysisUpdatedEvent event) {
                ParreAnalysisDTO currentAnalysis = ClientSingleton.getAppInitData().getParreAnalysisDTO();
                ParreAnalysisDTO updatedAnalysis = event.getParreAnalysisDTO();
                if (currentAnalysis.equals(updatedAnalysis)) {
                    updateCurrentParreAnalysis(updatedAnalysis);
                }
                updateActiveParreAnalysesList(updatedAnalysis);
            }
        });
        eventBus.addHandler(CurrentParreAnalysisChangedEvent.TYPE, new CurrentParreAnalysisChangedEvent.Handler() {
            public void onEvent(CurrentParreAnalysisChangedEvent event) {
                updateCurrentParreAnalysis(event.getParreAnalysisDTO());
            }
        });
        eventBus.addHandler(EditParreAnalysisEvent.TYPE, new EditParreAnalysisEvent.Handler() {
            public void onEvent(EditParreAnalysisEvent event) {
                display.showView(parreAnalysisViewIndex);
            }
        });

        eventBus.addHandler(LoginEvent.TYPE, new LoginEvent.Handler() {
            public void onEvent(LoginEvent event) {
                ClientLoggingUtil.info("Setting user name : " + event.getLoginInfo().getNickname());
                loginInfo = event.getLoginInfo();
                display.getUserNameLabel().setText(loginInfo.getNickname());
                display.setCurrentViewTitle("Home");
                if (ClientSingleton.hasCurrentParreAnalysis()) {
                    navViewMapper.showView(mainNavView.class);
                } else {
                    display.showSelectAnalysisPopup(true);
                }
            }
        });


    }

    private void bindActions() {
        display.getSignOutLink().addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                eventBus.fireEvent(LogoutEvent.create(loginInfo));
                eventBus.fireEvent(ClearNavEvent.create());
                logOutCurrentUser();
            }
        });
        display.getHomeLink().addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                displayHome();
                /*if (!ClientSingleton.hasCurrentParreAnalysis()) {
                    display.showSelectAnalysisPopup(true);
                    return;
                }
                eventBus.fireEvent(GoHomeEvent.create());
                navViewMapper.showView(mainNavView.class);*/
            }
        });
        display.getManageParreAnalysisButton().addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                display.showSelectAnalysisPopup(false);
            }
        });
        display.getEditAnalysisButton().addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                display.hideSelectAnalysisPopup();
                editParreAnalysis(ViewUtil.getSelectedId(display.getActiveParreAnalyses()));

            }
        });
        display.getCopyAnalysisButton().addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                display.hideSelectAnalysisPopup();
                ConfirmDialog.show("A full copy of the analysis will be made, Proceed?", new ActionSource.ActionCommand() {
                    public void execute(Object o) {
                        copyAnalysis(ViewUtil.getSelectedId(display.getActiveParreAnalyses()));
                    }
                });
            }
        });
        display.getSelectAnalysisButton().addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                display.hideSelectAnalysisPopup();
                selectParreAnalysis(ViewUtil.getSelectedId(display.getActiveParreAnalyses()));
            }
        });
        display.getCreateAnalysisButton().addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                display.hideSelectAnalysisPopup();
                ParreAnalysisDTO newDTO = new ParreAnalysisDTO();
                GWT.log("DTO Value when creating new ParreAnalysis=="+newDTO.getId());	
                eventBus.fireEvent(EditParreAnalysisEvent.create(newDTO));
            }
        });
        display.getDeleteAnalysisButton().addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                display.showDeleteDialog();
            }
        });
        display.getConfirmDeleteButton().addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                display.hideDeleteDialog();
                display.hideSelectAnalysisPopup();
                deleteParreAnalysis(ViewUtil.getSelectedId(display.getActiveParreAnalyses()));
            }
        });
        display.getHelpButton().addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent clickEvent) {
                Window.open("/parre/ParreHelp.html" + display.getHelpAnchor(), "_blank", "resizable,scrollbars,status");
            }
        });
    }

    private void logOutCurrentUser() {
        ExecutableAsyncCallback<LoginInfo> exec =
                new ExecutableAsyncCallback<LoginInfo>(busyHandler, new BaseAsyncCommand<LoginInfo>() {
                    public void execute(AsyncCallback<LoginInfo> callback) {
                        ServiceFactory.getInstance().getLoginService().signOut(callback);
                    }
                });
        exec.makeCall();

    }

    private void editParreAnalysis(final Long parreAnalysisId) {
        ExecutableAsyncCallback<ParreAnalysisDTO> exec =
                new ExecutableAsyncCallback<ParreAnalysisDTO>(busyHandler, new BaseAsyncCommand<ParreAnalysisDTO>() {
                    public void execute(AsyncCallback<ParreAnalysisDTO> callback) {
                        ServiceFactory.getInstance().getParreService().getParreAnalysis(parreAnalysisId, callback);
                    }

                    @Override
                    public void handleResult(ParreAnalysisDTO results) {
                        eventBus.fireEvent(EditParreAnalysisEvent.create(results));
                    }
                });
        exec.makeCall();

    }

    private void deleteParreAnalysis(final Long parreAnalysisId) {
        ExecutableAsyncCallback<List<Long>> exec = new ExecutableAsyncCallback<List<Long>>(busyHandler, new BaseAsyncCommand<List<Long>>() {
            @Override
            public void execute(AsyncCallback<List<Long>> callback) {
                ServiceFactory.getInstance().getParreService().deleteParreAnalysis(parreAnalysisId, callback);
            }

            @Override
            public void handleResult(List<Long> results) {
                for(Long id : results) {
                    ClientLoggingUtil.info("ID to delete = " + id);
                    for(int i = 0; i<selectableParreAnalyses.size(); i++) {
                        ClientLoggingUtil.info("Checking ID " + selectableParreAnalyses.get(i).getId());
                        if(selectableParreAnalyses.get(i).getId().equals(id)) {
                            selectableParreAnalyses.remove(i);
                            break;
                        }
                        for(int j = 0; j<selectableParreAnalyses.get(i).getOptionParreAnalysisDTOs().size(); j++) {
                            if(selectableParreAnalyses.get(i).getOptionParreAnalysisDTOs().get(j).getId().equals(id)) {
                                selectableParreAnalyses.get(i).getOptionParreAnalysisDTOs().remove(j);
                                break;
                            }
                        }
                    }
                }
                refreshActiveAnalysesListBox();
                display.showSelectAnalysisPopup(true);
                
                if (selectableParreAnalyses.size() < 1) {
                	ClientLoggingUtil.debug("Deletion removed all Parre Analyses...");
                	ClientSingleton.getAppInitData().setParreAnalysisDTO(null);
                	display.setParreAnalysisName("Select Current Analysis");
                	displayHome();
                }
            }
        });
        exec.makeCall();
    }

    private void updateActiveParreAnalysesList(ParreAnalysisDTO dto) {
        if (dto.isOption()) {
            updateOptionAnalysis(dto);
        } else {
            updateBaselineAnalysis(dto);
        }
        refreshActiveAnalysesListBox();
    }

    private void updateBaselineAnalysis(ParreAnalysisDTO dto) {
        int index = selectableParreAnalyses.indexOf(dto);

        if (index < 0) {
            selectableParreAnalyses.add(0, dto);
        } else {
            selectableParreAnalyses.set(index, dto);
        }
    }

    private void updateOptionAnalysis(ParreAnalysisDTO dto) {
        Long parreAnalysisId = dto.getBaselineId();
        ParreAnalysisDTO baselineDTO = findParreAnalysis(parreAnalysisId);
        baselineDTO.updateOptionDTO(dto);
    }

    private ParreAnalysisDTO findParreAnalysis(Long parreAnalysisId) {
        return ParreAnalysisDTO.findParreAnalysis(parreAnalysisId, selectableParreAnalyses);
    }

    private void copyAnalysis(final Long parreAnalysisId) {

        new ExecutableAsyncCallback<ParreAnalysisDTO>(busyHandler, new BaseAsyncCommand<ParreAnalysisDTO>() {
            public void execute(AsyncCallback<ParreAnalysisDTO> callback) {
                ServiceFactory.getInstance().getParreService().copyParreAnalysis(parreAnalysisId, callback);
            }
            @Override
            public void handleResult(ParreAnalysisDTO results) {
                updateBaselineAnalysis(results);
                selectParreAnalysis(results.getId());
            }
        }).makeCall();

    }

    private void selectParreAnalysis(final Long parreAnalysisId) {
        ExecutableAsyncCallback<Void> exec =
                new ExecutableAsyncCallback<Void>(busyHandler, new BaseAsyncCommand<Void>() {
                    public void execute(AsyncCallback<Void> callback) {
                        ServiceFactory.getInstance().getParreService().setCurrentParreAnalysis(parreAnalysisId, callback);
                    }

                    @Override
                    public void handleResult(Void results) {
                        updateCurrentParreAnalysis(findParreAnalysis(parreAnalysisId));
                    }
                });
        exec.makeCall();

    }

    private void updateCurrentParreAnalysis(ParreAnalysisDTO updatedAnalysis) {
        ClientSingleton.setParreAnalysisDTO(updatedAnalysis);
        showHomePage(updatedAnalysis);
    }

    private void displayHome() {
        if (!ClientSingleton.hasCurrentParreAnalysis() && firstDisplay) {
            display.showSelectAnalysisPopup(true);
            return;
        }
        firstDisplay = false;
        eventBus.fireEvent(GoHomeEvent.create());
        navViewMapper.showView(mainNavView.class);
    }

}
