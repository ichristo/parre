/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.main.presenter;

import org.parre.client.ClientSingleton;
import org.parre.client.event.GoHomeEvent;
import org.parre.client.messaging.ServiceFactory;
import org.parre.client.nav.event.*;
import org.parre.client.util.*;
import org.parre.client.util.nav.NavManager;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Image;
import com.google.web.bindery.event.shared.EventBus;

/**
 * .
 * User: Swarn S. Dhaliwal
 * Date: 9/3/11
 * Time: 1:53 PM
 */
public class MainNavPresenter implements Presenter {
    private EventBus eventBus;
    private Display display;
    private BusyHandler busyHandler;
    private boolean hasAssets;
    private boolean hasAssetThreats;
    private boolean hasOption;
    private boolean hasReports;

    /**
     * The Interface Display.
     */
    public interface Display extends View {
        Image getAssetButton();

        Image getThreatButton();

        Image getAssetThreatButton();

        Image getNaturalButton();

        Image getDepButton();

        Image getConsequenceButton();

        Image getVulnerabilityButton();

        Image getThreatAnalysisButton();

        Image getAnalysisButton();

        Image getManagementButton();
        
        Image getReportsButton();

    }

    public MainNavPresenter(Display display) {
        this.eventBus = ClientSingleton.getEventBus();
        this.display = display;
    }

    public void go() {
        busyHandler = new BusyHandler(display);
        bind();
    }

    private void bind() {
        updateHomePage();

        eventBus.addHandler(GoHomeEvent.TYPE, new GoHomeEvent.Handler() {
            @Override
            public void onEvent(GoHomeEvent event) {
                updateHomePage();
            }
        });
        display.getAssetButton().addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent clickEvent) {
            	if (ClientSingleton.hasCurrentParreAnalysis()) {
            		NavManager.performNav(AssetNavEvent.create());
            	}
            }
        });
        display.getThreatButton().addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent clickEvent) {
            	if (ClientSingleton.hasCurrentParreAnalysis()) {
            		NavManager.performNav(ThreatNavEvent.create());
            	}
            }
        });
        display.getAssetThreatButton().addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent clickEvent) {
                if(hasAssets) {
                    NavManager.performNav(ThreatAssetMatrixNavEvent.create());
                }

            }
        });
        display.getNaturalButton().addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent clickEvent) {
                if(hasAssetThreats) {
                    NavManager.performNav(NaturalThreatNavEvent.create());
                }

            }
        });
        display.getDepButton().addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent clickEvent) {
                if(hasAssetThreats) {
                    NavManager.performNav(DpNavEvent.create());
                }

            }
        });
        display.getConsequenceButton().addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent clickEvent) {
                if(hasAssetThreats) {
                    NavManager.performNav(ConsequenceNavEvent.create());
                }

            }
        });
        display.getVulnerabilityButton().addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent clickEvent) {
                if(hasAssetThreats) {
                    NavManager.performNav(VulnerabilityNavEvent.create());
                }

            }
        });
        display.getThreatAnalysisButton().addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent clickEvent) {
                if(hasAssetThreats) {
                    NavManager.performNav(LotNavEvent.create());
                }

            }
        });
        display.getAnalysisButton().addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent clickEvent) {
                if (hasAssetThreats) {
                    NavManager.performNav(RiskResilienceNavEvent.create());
                }

            }
        });
        display.getManagementButton().addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent clickEvent) {
                if(hasOption) {
                    NavManager.performNav(RiskBenefitNavEvent.create());
                }

            }
        });
        display.getReportsButton().addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent clickEvent) {
                if(hasReports) {
                    NavManager.performNav(ReportsNavEvent.create());
                }

            }
        });

    }

    private void updateHomePage() {

        if(ClientSingleton.getCurrentParreAnalysis() == null) {
        	setAsset(false);
            setAssetThreat(false);
            setThreatAnalysis(false);
            setRRM(false);
            setRPT(false);
        }
        else if(ClientSingleton.getCurrentParreAnalysis().hasOptionAnalyses()) {
        	setAsset(true);
            setAssetThreat(true);
            setThreatAnalysis(true);
            setRRM(true);
            setRPT(true);
        }
        else {
        	setAsset(true);
            checkAssets();
            checkAssetThreatPairs();
            setRRM(ClientSingleton.getCurrentParreAnalysis().isLocked());
            setRPT(ClientSingleton.getCurrentParreAnalysis().isLocked());
        }

    }

    private void checkAssets() {
        ExecutableAsyncCallback<Boolean> exec = new ExecutableAsyncCallback<Boolean>(busyHandler, new BaseAsyncCommand<Boolean>() {
            public void execute(AsyncCallback<Boolean> asyncCallback) {
                ServiceFactory.getInstance().getAssetService().hasAssets(ClientSingleton.getBaselineId(), asyncCallback);
            }

            @Override
            public void handleResult(Boolean value) {
                setAssetThreat(value);
            }

            @Override
            public boolean handleError(Throwable t) {
                MessageDialog.popup(t.getMessage(), true);
                return true;
            }
        });
        exec.makeCall();
    }

    private void checkAssetThreatPairs() {
        ExecutableAsyncCallback<Boolean> exec = new ExecutableAsyncCallback<Boolean>(busyHandler, new BaseAsyncCommand<Boolean>() {
            public void execute(AsyncCallback<Boolean> asyncCallback) {
                ServiceFactory.getInstance().getAssetService().hasActiveThreats(ClientSingleton.getBaselineId(), asyncCallback);
            }

            @Override
            public void handleResult(Boolean value) {
                setThreatAnalysis(value);
            }

            @Override
            public boolean handleError(Throwable t) {
                MessageDialog.popup(t.getMessage(), true);
                return true;
            }
        });
        exec.makeCall();
    }
    
    private void setAsset(boolean value) {
    	if (value) {
    		display.getAssetButton().setUrl("images/mainNav/asset_button.png");
    		display.getAssetButton().setStyleName("mainButtons");
    	} else {
    		display.getAssetButton().setUrl("images/mainNav/asset_button_disabled.png");
    		display.getAssetButton().setStyleName("");
    	}
    }

    private void setAssetThreat(boolean value) {
        if(value) {
            display.getThreatButton().setUrl("images/mainNav/threat_button.png");
            display.getThreatButton().setStyleName("mainButtons");
            display.getAssetThreatButton().setUrl("images/mainNav/assetThreat_button.png");
            display.getAssetThreatButton().setStyleName("mainButtons");
        }
        else {
            display.getThreatButton().setUrl("images/mainNav/threat_button_disabled.png");
            display.getThreatButton().setStyleName("");
            display.getAssetThreatButton().setUrl("images/mainNav/assetThreat_button_disabled.png");
            display.getAssetThreatButton().setStyleName("");
        }
        hasAssets = value;
    }

    private void setThreatAnalysis(boolean value) {
        if(value) {
            display.getNaturalButton().setStyleName("mainButtons");
            display.getNaturalButton().setUrl("images/mainNav/natural_button.png");

            display.getDepButton().setStyleName("mainButtons");
            display.getDepButton().setUrl("images/mainNav/dependencyThreat_button.png");

            display.getConsequenceButton().setStyleName("mainButtons");
            display.getConsequenceButton().setUrl("images/mainNav/consequence_button.png");

            display.getVulnerabilityButton().setStyleName("mainButtons");
            display.getVulnerabilityButton().setUrl("images/mainNav/vulnerability_button.png");

            display.getThreatAnalysisButton().setStyleName("mainButtons");
            display.getThreatAnalysisButton().setUrl("images/mainNav/threatAnalysis_button.png");

            display.getAnalysisButton().setStyleName("mainButtons");
            display.getAnalysisButton().setUrl("images/mainNav/analysis_button.png");
        }
        else {
            display.getNaturalButton().setUrl("images/mainNav/natural_button_disabled.png");
            display.getNaturalButton().setStyleName("");

            display.getDepButton().setUrl("images/mainNav/dependencyThreat_button_disabled.png");
            display.getDepButton().setStyleName("");

            display.getConsequenceButton().setUrl("images/mainNav/consequence_button_disabled.png");
            display.getConsequenceButton().setStyleName("");

            display.getVulnerabilityButton().setUrl("images/mainNav/vulnerability_button_disabled.png");
            display.getVulnerabilityButton().setStyleName("");

            display.getThreatAnalysisButton().setUrl("images/mainNav/threatAnalysis_button_disabled.png");
            display.getThreatAnalysisButton().setStyleName("");

            display.getAnalysisButton().setUrl("images/mainNav/analysis_button_disabled.png");
            display.getAnalysisButton().setStyleName("");
        }
        hasAssetThreats = value;
    }

    private void setRRM(boolean value) {
        if(value) {
            display.getManagementButton().setStyleName("mainButtons");
            display.getManagementButton().setUrl("images/mainNav/management_button.png");
        }
        else {
            display.getManagementButton().setUrl("images/mainNav/management_button_disabled.png");
            display.getManagementButton().setStyleName("");
        }
        hasOption = value;
    }
    
    private void setRPT(boolean value) {
        if(value) {
            display.getReportsButton().setStyleName("mainButtons");
            display.getReportsButton().setUrl("images/mainNav/reports_button.png");
        }
        else {
            display.getReportsButton().setUrl("images/mainNav/reports_button_disabled.png");
            display.getReportsButton().setStyleName("");
        }
        hasReports = value;
    }
    

}
