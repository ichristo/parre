/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.main.presenter;

import org.parre.client.ClientSingleton;
import org.parre.client.main.event.EditProposedMeasureEvent;
import org.parre.client.main.event.ProposedMeasureCreatedEvent;
import org.parre.client.main.event.ProposedMeasureUpdatedEvent;
import org.parre.client.messaging.ServiceFactory;
import org.parre.client.util.*;
import org.parre.shared.ProposedMeasureDTO;
import org.parre.shared.types.AssetType;
import org.parre.shared.types.CounterMeasureType;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.ListBox;
import com.google.web.bindery.event.shared.EventBus;

/**
 * .
 * User: Swarn S. Dhaliwal
 * Date: 9/5/11
 * Time: 9:06 AM
 */
public class EditProposedMeasurePresenter implements Presenter {
    private EventBus eventBus;
    private Display display;
    private BusyHandler busyHandler;

    /**
     * The Interface Display.
     */
    public interface Display extends ModelView<ProposedMeasureDTO> {
        HasClickHandlers getSaveButton();

        ListBox getType();

    }

    public EditProposedMeasurePresenter(Display display) {
        this.eventBus = ClientSingleton.getEventBus();
        this.display = display;
    }

    public void go() {
        bind();
    }

    private void bind() {
        busyHandler = new BusyHandler(display);
        populateTypePickList(display.getType());
        eventBus.addHandler(EditProposedMeasureEvent.TYPE, new EditProposedMeasureEvent.Handler() {
            public void onEvent(EditProposedMeasureEvent event) {
                display.setModelAndUpdateView(event.getProposedMeasureDTO());
            }
        });

        display.getSaveButton().addClickHandler(new ValidatingClickHandler(display) {
            public void handleClick(ClickEvent event) {
                savePm();
            }
        });
    }

    private void savePm() {
        final ProposedMeasureDTO dto = display.updateAndGetModel();
        final boolean isNew = dto.isNew();
        new ExecutableAsyncCallback<ProposedMeasureDTO>(busyHandler, new BaseAsyncCommand<ProposedMeasureDTO>() {
            public void execute(AsyncCallback<ProposedMeasureDTO> asyncCallback) {
                if (isNew) {
                    ServiceFactory.getInstance().getParreService().createAndAssociateProposedMeasure(dto, asyncCallback);
                }
                else {
                    ServiceFactory.getInstance().getParreService().saveProposedMeasure(dto, asyncCallback);
                }
            }

            @Override
            public void handleResult(ProposedMeasureDTO result) {
                if (isNew) {
                    eventBus.fireEvent(ProposedMeasureCreatedEvent.create(result));
                }
                else {
                    eventBus.fireEvent(ProposedMeasureUpdatedEvent.create(result));
                }
            }

            @Override
            public boolean handleError(Throwable t) {
                MessageDialog.popup(t.getMessage(), true);
                return true;
            }
        }).makeCall();
    }

    private Long getParreAnalysisId() {
        return ClientSingleton.getCurrentParreAnalysis() != null ? ClientSingleton.getParreAnalysisId() : null;
    }

    protected boolean handlesAssetType(AssetType assetType) {
        return true;
    }


    private void populateTypePickList(ListBox listBox) {
        CounterMeasureType[] values = CounterMeasureType.values();
        listBox.addItem("Select", "");
        for (CounterMeasureType value : values) {
            listBox.addItem(value.getValue(), value.name());
        }
    }
}
