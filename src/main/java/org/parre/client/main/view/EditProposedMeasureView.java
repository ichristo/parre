/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.main.view;

import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.user.client.ui.*;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.parre.client.main.presenter.EditProposedMeasurePresenter;
import org.parre.client.util.BaseModelView;
import org.parre.client.util.FieldValidationHandler;
import org.parre.client.util.PositiveNumberValidator;
import org.parre.client.util.ViewUtil;
import org.parre.shared.AmountDTO;
import org.parre.shared.ProposedMeasureDTO;
import org.parre.shared.types.CounterMeasureType;
import org.parre.shared.types.MoneyUnitType;


/**
 * .
 * User: Swarn S. Dhaliwal
 * Date: 9/5/11
 * Time: 8:35 AM
 */
public class EditProposedMeasureView extends BaseModelView<ProposedMeasureDTO> implements EditProposedMeasurePresenter.Display {
    private List<FieldValidationHandler> requiredFieldHandlers = new ArrayList<FieldValidationHandler>();
    private TextBox name;
    private TextBox description;
    private ListBox type;
    private TextBox salvageValueQ;
    private TextBox capitalCostQ;
    private TextBox oAndMCostPerYearQ;
    private TextBox effectiveLife;
    private TextBox inflationRate;
    private Button saveButton;
    private Widget viewWidget;
    private Label header;
    private boolean popupView;
    private ListBox capitalCostU;
    private ListBox oAndMCostPerYearU;
    private ListBox salvageValueU;

    public EditProposedMeasureView() {
        this(false);
    }
    public EditProposedMeasureView(boolean popupView) {
        this.popupView = popupView;
        viewWidget = buildView();
    }

    private VerticalPanel buildView() {
        header = new Label("Create Proposed Measure");
        header.setStyleName("inputPanelHeader");
        FlexTable inputTable = new FlexTable();
        //inputTable.setWidth("100%");
        int rowNum = -1;
        name = ViewUtil.addTextBoxFormEntry(inputTable, ++rowNum, "Name", requiredFieldHandlers);
        description = ViewUtil.addTextBoxFormEntry(inputTable, ++rowNum, "Description", requiredFieldHandlers);
        type = ViewUtil.addListBoxFormEntry(inputTable, ++rowNum, "Measure Type", requiredFieldHandlers);
        capitalCostQ = createMoneyBox(requiredFieldHandlers);
        capitalCostU = createUnitPickList(requiredFieldHandlers);
        ViewUtil.addFormEntry(inputTable, ++rowNum, "Capital Cost", ViewUtil.horizontalPanel(capitalCostQ, capitalCostU));
        oAndMCostPerYearQ = createMoneyBox(requiredFieldHandlers);
        oAndMCostPerYearU = createUnitPickList(requiredFieldHandlers);
        ViewUtil.addFormEntry(inputTable, ++rowNum, "O&M Costs/Year", ViewUtil.horizontalPanel(oAndMCostPerYearQ, oAndMCostPerYearU));
        salvageValueQ = createMoneyBox(requiredFieldHandlers);
        salvageValueU = createUnitPickList(requiredFieldHandlers);
        ViewUtil.addFormEntry(inputTable, ++rowNum, "Salvage Value", ViewUtil.horizontalPanel(salvageValueQ, salvageValueU));
        effectiveLife = ViewUtil.addTextBoxFormEntry(inputTable, ++rowNum, "Effective Life (Yrs)", requiredFieldHandlers);
        inflationRate = ViewUtil.addTextBoxFormEntry(inputTable, ++rowNum, "Inflation Rate (%)", requiredFieldHandlers);
        saveButton = ViewUtil.createButton("Save");
        VerticalPanel viewPanel = new VerticalPanel();
        viewPanel.setWidth("100%");
        viewPanel.setSpacing(5);
        viewPanel.setStyleName("inputPanel");

        viewPanel.add(header);
        viewPanel.add(ViewUtil.layoutCenter(inputTable));
        viewPanel.add(ViewUtil.layoutCenter(saveButton));
        return viewPanel;
    }

    private TextBox createMoneyBox(List<FieldValidationHandler> requiredFieldHandlers) {
        TextBox textBox = new TextBox();
        ViewUtil.addRequiredField(textBox, requiredFieldHandlers, PositiveNumberValidator.INSTANCE);
        return textBox;
    }

    private ListBox createUnitPickList(List<FieldValidationHandler> requiredFieldHandlers) {
        ListBox listBox = new ListBox();
        listBox.addItem("Select", "");
        MoneyUnitType[] values = MoneyUnitType.values();
        for (MoneyUnitType value : values) {
            listBox.addItem(value.getValue(), value.name());
        }
        ViewUtil.addRequiredField(listBox, requiredFieldHandlers);
        return listBox;
    }

    public HasClickHandlers getSaveButton() {
        return saveButton;
    }

    public Widget asWidget() {
        return viewWidget;
    }

    public boolean validate() {
        return ViewUtil.validate(requiredFieldHandlers);
    }

    public void updateView() {
        ProposedMeasureDTO model = getModel();
        if (model.getType() != null) {
            ViewUtil.setSelectedValue(type, model.getType().name());
        }
        else {
            type.setSelectedIndex(0);
        }
        setText(name, model.getName());
        setText(description, model.getDescription());
        setBigDecimal(capitalCostQ, model.getCapitalCostQuantity());
        setMoneyUnit(capitalCostU, model.getCapitalCostDTO().getUnit());
        setBigDecimal(oAndMCostPerYearQ, model.getoAndMCostPerYearDTO().getQuantity());
        setMoneyUnit(oAndMCostPerYearU, model.getoAndMCostPerYearDTO().getUnit());
        setBigDecimal(salvageValueQ, model.getSalvageValueQuantity());
        setMoneyUnit(salvageValueU, model.getSalvageValueDTO().getUnit());
        setQuantity(effectiveLife, model.getEffectiveLife());
        setBigDecimal(inflationRate, model.getInflationRate());
    }

    public void updateModel() {
        ProposedMeasureDTO model = getModel();
        model.setType(CounterMeasureType.valueOf(getSelectedValue(type)));
        model.setName(ViewUtil.getText(name));
        model.setDescription(ViewUtil.getText(description));
        model.setCapitalCostDTO(new AmountDTO(getBigDecimal(capitalCostQ), getMoneyUnit(capitalCostU)));
        model.setoAndMCostPerYearDTO(new AmountDTO(getBigDecimal(oAndMCostPerYearQ), getMoneyUnit(oAndMCostPerYearU)));
        model.setSalvageValueDTO(new AmountDTO(getBigDecimal(salvageValueQ), getMoneyUnit(salvageValueU)));
        model.setEffectiveLife(getQuantity(effectiveLife));
        model.setInflationRate(getBigDecimal(inflationRate));
    }

    private MoneyUnitType getMoneyUnit(ListBox unitBox) {
        String selectedValue = getSelectedValue(unitBox);
        return MoneyUnitType.valueOf(selectedValue);
    }

    private void setMoneyUnit(ListBox unit, MoneyUnitType unitType) {
        if (unitType == null) {
            unitType = MoneyUnitType.NONE;
        }
        setSelectedValue(unit, unitType.name());
    }
    public ListBox getType() {
        return type;
    }

    public boolean isPopupView() {
        return popupView;
    }
}
