/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.main.view;

import org.parre.client.ClientSingleton;
import org.parre.client.main.presenter.MainNavPresenter;
import org.parre.client.util.BaseView;
import org.parre.client.util.ViewUtil;

import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.FlexTable.FlexCellFormatter;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HasHorizontalAlignment.HorizontalAlignmentConstant;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

/**
 * .
 * User: Swarn S. Dhaliwal
 * Date: 9/3/11
 * Time: 10:37 AM
 */
public class mainNavView extends BaseView implements MainNavPresenter.Display {
    private Image assetButton = new Image("images/mainNav/asset_button.png");
    private Image threatButton = new Image("images/mainNav/threat_button_disabled.png");
    private Image assetThreatButton = new Image("images/mainNav/assetThreat_button_disabled.png");
    private Image naturalButton = new Image("images/mainNav/natural_button_disabled.png");
    private Image depButton = new Image("images/mainNav/dependencyThreat_button_disabled.png");
    private Image consequenceButton = new Image("images/mainNav/consequence_button_disabled.png");
    private Image vulnerabilityButton = new Image("images/mainNav/vulnerability_button_disabled.png");
    private Image threatAnalysisButton = new Image("images/mainNav/threatAnalysis_button_disabled.png");
    private Image analysisButton = new Image("images/mainNav/analysis_button_disabled.png");
    private Image managementButton = new Image("images/mainNav/management_button_disabled.png");
    private Image reportsButton = new Image("images/mainNav/reports_button_disabled.png");


    private Widget viewWidget;
    public mainNavView() {
        viewWidget = createView();
    }

    private Widget createView() {
        assetButton.setStyleName("mainButtons");
        VerticalPanel viewPanel = new VerticalPanel();
        FlexTable inputPanel = new FlexTable();
        inputPanel.setWidth("800px");
        
        viewPanel.setWidth("100%");
        viewPanel.setHeight("100%");
        viewPanel.setStyleName("mainNavigationArea");
        
        int colNum = -1;
        ViewUtil.addWidget(inputPanel, 0, new HTML("<br />"), ++colNum);
        ViewUtil.addWidget(inputPanel, 0, new HTML("<br />"), ++colNum);
        ViewUtil.addTextUnderWidget(inputPanel, 0, "Asset Characterization", assetButton, ++colNum);
        ViewUtil.addTextUnderWidget(inputPanel, 0, "Threat Characterization", threatButton, ++colNum);
        ViewUtil.addTextUnderWidget(inputPanel, 0, "Asset/Threat Characterization", assetThreatButton, ++colNum);
        ViewUtil.addWidget(inputPanel, 0, new HTML("<br />"), ++colNum);
        
        FlexCellFormatter cellFormat = inputPanel.getFlexCellFormatter();

        cellFormat.setHorizontalAlignment(0, 0, HasHorizontalAlignment.ALIGN_CENTER);
        cellFormat.setStyleName(0, 0, "navRowHeader");
        inputPanel.setWidget(0, 0, new HTML("Threat\\Asset Identification"));
        
        cellFormat.setColSpan(2, 0, 6);
        inputPanel.setWidget(2, 0, new HTML("<hr style=\"width:100%\" />"));
        
        cellFormat.setColSpan(3, 1, 3);
        cellFormat.setStyleName(3, 1, "navSubHeader");
        inputPanel.setWidget(3, 1, new HTML("Direct Threats"));
        
        cellFormat.setColSpan(3, 2, 2);
        cellFormat.setStyleName(3, 2, "navSubHeader");
        inputPanel.setWidget(3, 2, new HTML("Indirect Threats"));
        
        colNum = -1;
        ViewUtil.addWidget(inputPanel, 0, new HTML("<br />"), ++colNum);
        ViewUtil.addTextUnderWidget(inputPanel, 4, "Consequence Analysis", consequenceButton, ++colNum);
        ViewUtil.addTextUnderWidget(inputPanel, 4, "Vulnerability Analysis", vulnerabilityButton, ++colNum);
        ViewUtil.addTextUnderWidget(inputPanel, 4, "Threat Analysis", threatAnalysisButton, ++colNum);
        ViewUtil.addTextUnderWidget(inputPanel, 4, "Natural Threat Analysis", naturalButton, ++colNum);
        ViewUtil.addTextUnderWidget(inputPanel, 4, "Dependency Threat Analysis", depButton, ++colNum);
        
        cellFormat.setHorizontalAlignment(4, 0, HasHorizontalAlignment.ALIGN_CENTER);
        cellFormat.setStyleName(4, 0, "navRowHeader");
        inputPanel.setWidget(4, 0, new HTML("Data Documentation"));
        
        cellFormat.setColSpan(6, 0, 6);
        inputPanel.setWidget(6, 0, new HTML("<hr style=\"width:100%\" />"));
        
        colNum = -1;
        ViewUtil.addWidget(inputPanel, 0, new HTML("<br />"), ++colNum);
        ViewUtil.addWidget(inputPanel, 7, new HTML("<br />"), ++colNum);
        ViewUtil.addTextUnderWidget(inputPanel, 7, "Risk\\Resilience Analysis", analysisButton, ++colNum);
        ViewUtil.addTextUnderWidget(inputPanel, 7, "Risk\\Resilience Management", managementButton, ++colNum);
        
        if (ClientSingleton.getAppInitData().isReportSubSystemAvailable()) {
        	ViewUtil.addTextUnderWidget(inputPanel, 7, "Reports", reportsButton, ++colNum);
        } else {
        	ViewUtil.addWidget(inputPanel, 7, new HTML("<br />"), ++colNum);
        }
        ViewUtil.addWidget(inputPanel, 7, new HTML("<br />"), ++colNum);
        
        cellFormat.setHorizontalAlignment(7, 0, HasHorizontalAlignment.ALIGN_CENTER);
        cellFormat.setStyleName(7, 0, "navRowHeader");
        inputPanel.setWidget(7, 0, new HTML("Data Analysis"));

        viewPanel.add(ViewUtil.layoutCenter(inputPanel));
        viewPanel.addStyleName("homeImagePanel");

        return viewPanel;
    }

    public Widget asWidget() {
        return viewWidget;
    }

    @Override
    public Image getAssetButton() {
        return assetButton;
    }

    @Override
    public Image getThreatButton() {
        return threatButton;
    }

    @Override
    public Image getAssetThreatButton() {
        return assetThreatButton;
    }

    @Override
    public Image getNaturalButton() {
        return naturalButton;
    }

    @Override
    public Image getDepButton() {
        return depButton;
    }

    @Override
    public Image getConsequenceButton() {
        return consequenceButton;
    }

    @Override
    public Image getVulnerabilityButton() {
        return vulnerabilityButton;
    }

    @Override
    public Image getThreatAnalysisButton() {
        return threatAnalysisButton;
    }

    @Override
    public Image getAnalysisButton() {
        return analysisButton;
    }

    @Override
    public Image getManagementButton() {
        return managementButton;
    }
    
    @Override
    public Image getReportsButton() {
    	return reportsButton;
    }
}
