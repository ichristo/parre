/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.main.view;

import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.user.client.ui.*;

import java.util.ArrayList;
import java.util.List;

import org.parre.client.main.presenter.EditLocationPresenter;
import org.parre.client.util.BaseModelView;
import org.parre.client.util.FieldValidationHandler;
import org.parre.client.util.ViewUtil;
import org.parre.shared.SiteLocationDTO;

/**
 * .
 * User: Swarn S. Dhaliwal
 * Date: 9/4/11
 * Time: 2:56 PM
 */
public class EditLocationView extends BaseModelView<SiteLocationDTO> implements EditLocationPresenter.Display {

    private List<FieldValidationHandler> requiredFieldHandlers;
    private TextBox locationName;
    private TextBox address;
    private TextBox city;
    private Button saveButton;
    private Widget viewWidget;
    private TextBox state;
    private TextBox zipCode;

    public EditLocationView() {
        requiredFieldHandlers = new ArrayList<FieldValidationHandler>();
        viewWidget = createView();
    }

    private VerticalPanel createView() {
        VerticalPanel viewPanel = new VerticalPanel();
        viewPanel.setStyleName("inputPanel");
        Label header = new Label("Site Information");
        header.setStyleName("inputPanelHeader");
        viewPanel.add(header);

        FlexTable inputTable = new FlexTable();
        inputTable.setCellSpacing(6);
        inputTable.setWidth("100%");
        viewPanel.add(inputTable);

        int rowNum = -1;

        locationName = ViewUtil.addTextBoxFormEntry(inputTable, ++rowNum, "Name Of Location", requiredFieldHandlers);
        address = ViewUtil.addTextBoxFormEntry(inputTable, ++rowNum, "Address");
        city = ViewUtil.addTextBoxFormEntry(inputTable, ++rowNum, "City");
        state = ViewUtil.addTextBoxFormEntry(inputTable, ++rowNum, "State");
        zipCode = ViewUtil.addTextBoxFormEntry(inputTable, ++rowNum, "Zip Code", requiredFieldHandlers);

        saveButton = ViewUtil.createButton("Save");
        viewPanel.add(ViewUtil.layoutCenter(saveButton));
        return viewPanel;
    }

    public HasClickHandlers getSaveButton() {
        return saveButton;
    }

    public void updateModel() {
        SiteLocationDTO model = getModel();
        model.setName(getText(locationName));
        model.setAddress(getText(address));
        model.setCity(getText(city));
        model.setState(getText(state));
        model.setZipCode(getText(zipCode));
    }

    public void updateView() {
        SiteLocationDTO model = getModel();
        ViewUtil.setText(locationName, model.getName());
        ViewUtil.setText(address, model.getAddress());
        ViewUtil.setText(city, model.getCity());
        ViewUtil.setText(state, model.getState());
        ViewUtil.setText(zipCode, model.getZipCode());
    }

    public Widget asWidget() {
        return viewWidget;
    }

    public boolean validate() {
        return ViewUtil.hasRequiredValues(requiredFieldHandlers);
    }

}
