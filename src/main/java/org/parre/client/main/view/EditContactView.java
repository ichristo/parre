/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.main.view;

import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.user.client.ui.*;

import java.util.ArrayList;
import java.util.List;

import org.parre.client.main.presenter.EditContactPresenter;
import org.parre.client.util.BaseModelView;
import org.parre.client.util.FieldValidationHandler;
import org.parre.client.util.ViewUtil;
import org.parre.shared.ContactDTO;

/**
 * .
 * User: Swarn S. Dhaliwal
 * Date: 9/4/11
 * Time: 2:56 PM
 */
public class EditContactView extends BaseModelView<ContactDTO> implements EditContactPresenter.Display {

    private List<FieldValidationHandler> requiredFieldHandlers;
    private TextBox contactName;
    private TextBox phone;
    private TextBox workPhone;
    private TextBox mobilePhone;
    private TextBox fax;
    private TextBox email;
    private Button saveButton;
    private Widget viewWidget;

    public EditContactView() {
        requiredFieldHandlers = new ArrayList<FieldValidationHandler>();
        viewWidget = createView();
    }

    private VerticalPanel createView() {
        VerticalPanel viewPanel = new VerticalPanel();
        viewPanel.setStyleName("inputPanel");
        Label header = new Label("Contact Information");
        header.setStyleName("inputPanelHeader");
        viewPanel.add(header);

        FlexTable inputTable = new FlexTable();
        inputTable.setCellSpacing(6);
        inputTable.setWidth("100%");
        viewPanel.add(inputTable);

        int rowNum = -1;

        contactName = ViewUtil.addTextBoxFormEntry(inputTable, ++rowNum, "Contact Name", requiredFieldHandlers);
        phone = ViewUtil.addTextBoxFormEntry(inputTable, ++rowNum, "Phone");
        workPhone = ViewUtil.addTextBoxFormEntry(inputTable, ++rowNum, "Work Phone");
        mobilePhone = ViewUtil.addTextBoxFormEntry(inputTable, ++rowNum, "Mobile Phone");
        fax = ViewUtil.addTextBoxFormEntry(inputTable, ++rowNum, "Fax");
        email = ViewUtil.addTextBoxFormEntry(inputTable, ++rowNum, "Email");

        saveButton = ViewUtil.createButton("Save");
        viewPanel.add(ViewUtil.layoutCenter(saveButton));
        return viewPanel;
    }

    public HasClickHandlers getSaveButton() {
        return saveButton;
    }

    public void updateModel() {
        ContactDTO model = getModel();
        model.setContactName(getText(contactName));
        model.setPhone(getText(phone));
        model.setWorkPhone(getText(workPhone));
        model.setMobilePhone(getText(mobilePhone));
        model.setFax(getText(fax));
        model.setEmail(getText(email));
    }

    public void updateView() {
        ContactDTO model = getModel();
        ViewUtil.setText(contactName, model.getContactName());
        ViewUtil.setText(phone, model.getPhone());
        ViewUtil.setText(workPhone, model.getWorkPhone());
        ViewUtil.setText(mobilePhone, model.getMobilePhone());
        ViewUtil.setText(fax, model.getFax());
        ViewUtil.setText(email, model.getEmail());
    }

    public Widget asWidget() {
        return viewWidget;
    }

    public boolean validate() {
        return ViewUtil.hasRequiredValues(requiredFieldHandlers);
    }

}
