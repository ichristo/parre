/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.main.view;

import org.parre.client.main.presenter.MainPresenter;
import org.parre.client.util.*;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.*;

/**
 * User: Swarn S. Dhaliwal
 * Date: 7/9/11
 * Time: 10:05 AM
*/
public class MainView extends DeckComposite implements MainPresenter.Display {
    private Presenter presenter;
    private DialogBox confirmDeleteDialog;
    private String helpAnchor;

    public HasClickHandlers getSignOutLink() {
        return signOutButton;
    }

    public HasClickHandlers getHomeLink() {
        return homeButton;
    }

    public HasText getUserNameLabel() {
        return userNameLabel;
    }

    public void setPresenter(Presenter presenter) {
        this.presenter = presenter;
    }

    public Presenter getPresenter() {
        return presenter;
    }

    public void refreshView() {

    }

    public void setEnabled(boolean value) {

    }

    /**
     * The Interface MainViewUiBinder.
     */
    interface MainViewUiBinder extends UiBinder<Widget, MainView> {

    }
    /**
     * An instance of the constants.
     */
    private static MainViewUiBinder uiBinder = GWT.create(MainViewUiBinder.class);
    @UiField
    DockLayoutPanel mainPanel;
    @UiField
    LayoutPanel mainContentPanel;
    @UiField
    HTMLPanel headerPanel;
    @UiField /*(provided = true)*/
    Image homeButton;
    @UiField
    Image signOutButton;
    @UiField
    Label userNameLabel;
    @UiField
    Image helpButton;
    @UiField (provided = true)
    Button manageParreAnalysisButton;
    @UiField
    Label centerLabel2;

    private Button editAnalysisButton;
    private Button selectAnalysisButton;
    private Button createAnalysisButton;
    private Button copyAnalysisButton;
    private Button deleteAnalysisButton;
    private DialogBox selectAnalysisPopup;
    private ListBox activeParreAnalyses;
    private Button confirmDeleteButton;


    public MainView() {
        ClientLoggingUtil.info("Creating MainView");
        manageParreAnalysisButton = ViewUtil.createDownArrowButton("Select Current Analysis");
        Widget andBindUi = uiBinder.createAndBindUi(this);
        ToolTipManager.addToolTip(homeButton, "Go Home");
        ToolTipManager.addToolTip(signOutButton, "Sign Out");
        ToolTipManager.addToolTip(helpButton, "Help Document");
        initWidget(andBindUi);
        ImageCursorHandler.add(homeButton);
        ImageCursorHandler.add(signOutButton);
        ImageCursorHandler.add(helpButton);
        getDeckLayoutPanel().setStyleName("mainContentPanel");
        mainContentPanel.add(getDeckLayoutPanel());
        initSelectProxyPopup();
        confirmDeleteDialog = createConfirmDeleteDialog();
        ClientLoggingUtil.info("MainView created");
    }

    private DialogBox createConfirmDeleteDialog() {
        VerticalPanel panel = new VerticalPanel();
        panel.setStyleName("inputPanel");
        panel.setSpacing(5);
        confirmDeleteButton = ViewUtil.createButton("OK");
        HTML message = new HTML("Confirm deleting this entire analysis?");
        panel.add(message);
        panel.add(confirmDeleteButton);
        panel.setWidth("200px");
        return ViewUtil.createDialog("Confirm", panel);
    }

    public void showView(int viewIndex, String viewTitle, String helpAnchor) {
        this.helpAnchor = helpAnchor;
        showView(viewIndex, viewTitle);
    }
    
    @Override
    public void showView(int viewIndex, String viewTitle) {
    	super.showView(viewIndex, viewTitle);
    	setCurrentViewTitle(viewTitle);
    }

    public void setCurrentViewTitle(String viewName) {
        getCenterText2().setText(viewName);
    }

    public HasClickHandlers getHelpButton() {
        return helpButton;
    }

    @Override
    public HasClickHandlers getDeleteAnalysisButton() {
        return deleteAnalysisButton;
    }

    public HasClickHandlers getManageParreAnalysisButton() {
        return manageParreAnalysisButton;
    }

    public HasText getCenterText2() {
        return centerLabel2;
    }
    
    @Override
	public String getHelpAnchor() {
		return helpAnchor == null ? "" : helpAnchor;
	}

    public void setParreAnalysisName(String name) {
        ViewUtil.setDownArrowButtonTitle(manageParreAnalysisButton, name);
    }

    public ListBox getActiveParreAnalyses() {
        return activeParreAnalyses;
    }

    public HasClickHandlers getCopyAnalysisButton() {
        return copyAnalysisButton;
    }

    public HasClickHandlers getCreateAnalysisButton() {
        return createAnalysisButton;
    }

    public HasClickHandlers getSelectAnalysisButton() {
        return selectAnalysisButton;
    }

    public HasClickHandlers getEditAnalysisButton() {
        return editAnalysisButton;
    }

    public void showDeleteDialog() {
        confirmDeleteDialog.center();
        confirmDeleteDialog.show();
    }

    public void hideDeleteDialog() {
        confirmDeleteDialog.hide();
    }

    public HasClickHandlers getConfirmDeleteButton() {
        return confirmDeleteButton;
    }

    public void showSelectAnalysisPopup(boolean autoDisplay) {
        activeParreAnalyses.setSelectedIndex(0);
        editAnalysisButton.setEnabled(false);
        copyAnalysisButton.setEnabled(false);
        selectAnalysisButton.setEnabled(false);
        if (autoDisplay) {
            selectAnalysisPopup.center();
        }
        else {
            selectAnalysisPopup.showRelativeTo(manageParreAnalysisButton);
        }
    }

    public void hideSelectAnalysisPopup() {
        selectAnalysisPopup.hide();
    }

    private void initSelectProxyPopup() {
        selectAnalysisPopup = new ClosablePopup("Edit/Select/Copy/Create Parre Analysis", true);
        VerticalPanel panel = new VerticalPanel();
        panel.setStyleName("inputPanel");
        activeParreAnalyses = new ListBox();
        activeParreAnalyses.setWidth("95%");
        activeParreAnalyses.getElement().getStyle().setMargin(5, Style.Unit.PX);
        panel.add(activeParreAnalyses);
        selectAnalysisButton = ViewUtil.createButton("Make Current", "Set selected Parre Analysis as current");
        selectAnalysisButton.setWidth("125px");
        editAnalysisButton = ViewUtil.createButton("Edit", "Edit selected Parre Analysis");
        copyAnalysisButton = ViewUtil.createButton("Copy", "Make a copy of the selected Parre Analysis");
        createAnalysisButton = ViewUtil.createButton("Create", "Create a new Parre Analysis");
        deleteAnalysisButton = ViewUtil.createButton("Delete", "Delete this analysis.");
        activeParreAnalyses.addChangeHandler(new ChangeHandler() {
            public void onChange(ChangeEvent event) {
                boolean hasSelection = activeParreAnalyses.getSelectedIndex() > 0;
                editAnalysisButton.setEnabled(hasSelection);
                selectAnalysisButton.setEnabled(hasSelection);
                copyAnalysisButton.setEnabled(hasSelection);
                deleteAnalysisButton.setEnabled(hasSelection);
            }
        });
        panel.add(ViewUtil.layoutCenter(selectAnalysisButton, editAnalysisButton, copyAnalysisButton, createAnalysisButton, deleteAnalysisButton));
        selectAnalysisPopup.setWidget(panel);
    }

}