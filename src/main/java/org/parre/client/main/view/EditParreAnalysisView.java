/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.main.view;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.event.dom.client.*;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.user.client.ui.*;
import com.google.gwt.view.client.SingleSelectionModel;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.parre.client.ClientSingleton;
import org.parre.client.main.presenter.EditParreAnalysisPresenter;
import org.parre.client.util.*;
import org.parre.client.util.dataview.DataProvider;
import org.parre.client.util.dataview.DataProviderFactory;
import org.parre.shared.*;

/**
 * The Class EditParreAnalysisView.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 11/28/11
 */
public class EditParreAnalysisView extends BaseModelView<ParreAnalysisDTO>
        implements EditParreAnalysisPresenter.Display {
    private CheckBox useStatValues;
    private CheckBox overrideFatality;
    private CheckBox overrideSeriousInjury;
    private TextBox seriousInjury;
    private TextBox fatality;
    private Widget statView;
    private TextBox name;
    private TextBox description;
    private ListBox contacts;
    private ListBox locations;
    private Button saveButton;
    private List<FieldValidationHandler> requiredFieldHandlers = new ArrayList<FieldValidationHandler>();
    private List<FieldValidationHandler> statValueValidators = new ArrayList<FieldValidationHandler>(2);
    private Button createContactButton;
    private Button editContactButton;
    private Button createLocationButton;
    private Button editLocationButton;
    private DialogBox editProposedMeasurePopup;
    private DialogBox createLocationPopup;
    private Widget viewWidget;
    private List<HasEnabled> hasEnabledWidgets = new ArrayList<HasEnabled>();
    private ProposedMeasureGrid proposedMeasureGrid;
    private EditProposedMeasureView editProposedMeasureView;
    private Button addPmButton;
    private Button removePmButton;
    private DialogBox selectPmPopup;
    private DialogBox confirmRemovePmDialog;
    private ListBox proposedMeasures;
    private Button selectPmButton;
    private Button createPmButton;
    private VerticalPanel proposedMeasuresView;
    private Button confirmRemovePmButton;
    private EditLocationView editLocationView;
    private EditContactView editContactView;
    private ClosablePopup createContactPopup;
    
    private Widget locationView;
    private CheckBox createDefaultLocation;
    private TextBox defaultPostalCode;
    private TextBox defaultLatitude;
    private TextBox defaultLongitude;
    private Button getGISCoordinatesButton;
    private List<FieldValidationHandler> locationValidators = new ArrayList<FieldValidationHandler>();

    public EditParreAnalysisView(EditLocationView editLocationView, EditContactView editContactView,
                                 EditProposedMeasureView editProposedMeasureView) {
        this.editLocationView = editLocationView;
        this.editContactView = editContactView;
        this.editProposedMeasureView = editProposedMeasureView;
        viewWidget = createView();
    }

    private Widget createView() {
        initEditPmDialog();
        initSelectPmPopup();
        initConfirmRemovePmDialog();
        initCreateLocationDialog();
        Label header = new Label("Parre Analysis Information");
        header.setStyleName("inputPanelHeader");
        FlexTable inputTable = new FlexTable();
        inputTable.setWidth("100%");
        //inputTable.setBorderWidth(1);
        int rowNum = -1;
        name = ViewUtil.addTextBoxFormEntry(inputTable, ++rowNum, "Name", requiredFieldHandlers);
        description = ViewUtil.addTextBoxFormEntry(inputTable, ++rowNum, "Description");

        createContactButton = ViewUtil.createButton("Create Contact");
        editContactButton = ViewUtil.createButton("Edit Contact");
        contacts = new ListBox();
        ViewUtil.addFormEntry(inputTable, ++rowNum, "Contact", ViewUtil.horizontalPanel(5, contacts, createContactButton, editContactButton));

        createLocationButton = ViewUtil.createButton("Create Location");
        editLocationButton = ViewUtil.createButton("Edit Location");
        locations = new ListBox();
        ViewUtil.addFormEntry(inputTable, ++rowNum, "Site Location", ViewUtil.horizontalPanel(5, locations, createLocationButton, editLocationButton));

        saveButton = ViewUtil.createButton("Save");
        VerticalPanel viewPanel = new VerticalPanel();
        viewPanel.setSpacing(10);
        viewPanel.setWidth("100%");
        viewPanel.add(header);
        viewPanel.add(inputTable);
        viewPanel.add(createStatValuesControlView());
        viewPanel.add(createLocationControlView());
        viewPanel.add(ViewUtil.layoutCenter(saveButton));
        
        ScrollPanel parreAnalysisScrollPanel = new ScrollPanel();
        parreAnalysisScrollPanel.setSize("100%", "100%");
        VerticalPanel wrapper = new VerticalPanel();
        wrapper.setStyleName("inputPanel");
        wrapper.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_CENTER);
        wrapper.add(viewPanel);
        wrapper.add(initProposedMeasuresView());
	    parreAnalysisScrollPanel.add(wrapper);
	    parreAnalysisScrollPanel.setVerticalScrollPosition(0);
	        
        initHasEnabledWidgets();
        return parreAnalysisScrollPanel;
    }

    private Widget initProposedMeasuresView() {
        proposedMeasuresView = ViewUtil.createInputPanel();
        proposedMeasuresView.add(ViewUtil.createHeader("Proposed Measures"));
        addPmButton = ViewUtil.createButton("Add");
        removePmButton = ViewUtil.createButton("Remove");
        proposedMeasuresView.add(ViewUtil.horizontalPanel(5, addPmButton, removePmButton));
        DataProvider<ProposedMeasureDTO> dataProvider = DataProviderFactory.createDataProvider();
        proposedMeasureGrid = new ProposedMeasureGrid(dataProvider);
        proposedMeasureGrid.setHeight("300px");
        proposedMeasuresView.add(proposedMeasureGrid.asWidget());
        return proposedMeasuresView;
    }

    private void initHasEnabledWidgets() {
        hasEnabledWidgets.add(contacts);
        hasEnabledWidgets.add(createContactButton);
        hasEnabledWidgets.add(editContactButton);
        hasEnabledWidgets.add(locations);
        hasEnabledWidgets.add(createLocationButton);
        hasEnabledWidgets.add(editLocationButton);
        hasEnabledWidgets.add(useStatValues);
        hasEnabledWidgets.add(fatality);
        hasEnabledWidgets.add(overrideFatality);
        hasEnabledWidgets.add(seriousInjury);
        hasEnabledWidgets.add(overrideSeriousInjury);
        hasEnabledWidgets.add(createDefaultLocation);
        hasEnabledWidgets.add(defaultPostalCode);
        hasEnabledWidgets.add(getGISCoordinatesButton);
        hasEnabledWidgets.add(defaultLatitude);
        hasEnabledWidgets.add(defaultLongitude);
    }

    private Widget createStatValuesControlView() {
        useStatValues = new CheckBox("Use Life S-Value");
        overrideFatality = new CheckBox("Override");
        overrideFatality.setValue(false);
        overrideSeriousInjury = new CheckBox("Override");
        overrideSeriousInjury.setValue(false);
        final Label fatalityLabel = new Label("Fatality Value (M$): ");
        fatalityLabel.setStyleName("normalLabel");

        fatality = createStatValueBox();

        final Label seriousInjuryLabel = new Label("Serious Injury Value (M$) : ");
        seriousInjuryLabel.setStyleName("normalLabel");

        seriousInjury = createStatValueBox();

        overrideFatality.addValueChangeHandler(new ValueChangeHandler<Boolean>() {
            public void onValueChange(ValueChangeEvent<Boolean> booleanValueChangeEvent) {
                updateFatalityDisplay();
            }
        });
        overrideSeriousInjury.addValueChangeHandler(new ValueChangeHandler<Boolean>() {
            public void onValueChange(ValueChangeEvent<Boolean> booleanValueChangeEvent) {
                updateSeriousInjuryDisplay();
            }
        });
        useStatValues.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                statView.setVisible(useStatValues.getValue());
            }
        });

        VerticalPanel view = new VerticalPanel();
        view.setStyleName("inputPanel");
        view.add(ViewUtil.layoutCenter(useStatValues));
        FlexTable inputTable = new FlexTable();
        inputTable.setWidth("100%");
        int currentRow = -1;
        ViewUtil.addFormEntry(inputTable, ++currentRow, "Fatality Value (M$)", ViewUtil.horizontalPanel(5, fatality, overrideFatality));
        ViewUtil.addFormEntry(inputTable, ++currentRow, "Serious Injury Value (M$)", ViewUtil.horizontalPanel(5, seriousInjury, overrideSeriousInjury));
        view.add(inputTable);
        statView = inputTable;
        statView.setVisible(false);
        return view;
    }
    
    private Widget createLocationControlView() {
    	createDefaultLocation = new CheckBox("Use a Default Location for Physical Assets");
    	
    	FlexTable inputTable = new FlexTable();
    	inputTable.setWidth("100%");
    	int currentRow = -1;
    	getGISCoordinatesButton = ViewUtil.createButton("Get GIS Coordinates");
        getGISCoordinatesButton.setSize("150px", "30px");
        defaultPostalCode = createPostalCodeBox();
    	ViewUtil.addFormEntry(inputTable, ++currentRow, "Zip Code", ViewUtil.horizontalPanel(5, defaultPostalCode, getGISCoordinatesButton));
    	defaultLatitude = createCoordinateBox();
        ViewUtil.addFormEntry(inputTable, ++currentRow, "Latitude", defaultLatitude);
        defaultLongitude = createCoordinateBox();
        ViewUtil.addFormEntry(inputTable, ++currentRow, "Longitude", defaultLongitude);
        
        createDefaultLocation.addClickHandler(new ClickHandler() {
        	public void onClick(ClickEvent event) {
        		locationView.setVisible(createDefaultLocation.getValue());
        	}
        });
        
        VerticalPanel view = new VerticalPanel();
        view.setStyleName("inputPanel");
        view.add(ViewUtil.layoutCenter(createDefaultLocation));
        view.add(inputTable);
        locationView = inputTable;
        locationView.setVisible(false);
        
        return view;
    }

    public void updateSeriousInjuryDisplay() {
        seriousInjury.setEnabled(overrideSeriousInjury.getValue());
    }

    public void updateFatalityDisplay() {
        fatality.setEnabled(overrideFatality.getValue());
    }
    
    private TextBox createPostalCodeBox() {
    	final TextBox textBox = new TextBox();
    	textBox.setWidth("50px");
    	locationValidators.add(new FieldValidationHandler(textBox, PostalCodeValidator.INSTANCE));
    	return textBox;
    }
    
    private TextBox createCoordinateBox() {
    	final TextBox textBox = new TextBox();
    	textBox.setWidth("250px");
    	locationValidators.add(new FieldValidationHandler(textBox, CoordinateValidator.INSTANCE));
    	return textBox;
    }

    private TextBox createStatValueBox() {
        final TextBox textBox = new TextBox();
        textBox.setWidth("35px");
        statValueValidators.add(new FieldValidationHandler(textBox, PositiveNumberValidator.INSTANCE));
        return textBox;
    }

    public void showCreateLocationDialog() {
        if(createLocationPopup == null) {
            initCreateLocationDialog();
        }
        createLocationPopup.center();
    }

    public void hideCreateLocationDialog() {
        createLocationPopup.hide();
    }

    public void showCreateContactDialog() {
        if(createContactPopup == null) {
            initCreateContactDialog();
        }
        createContactPopup.center();
    }

    public void hideCreateContactDialog() {
        createContactPopup.hide();
    }

    public void showEditPmPopup() {
        editProposedMeasurePopup.center();
    }

    public void hideEditPmPopup() {
        editProposedMeasurePopup.hide();
    }

    private void initConfirmRemovePmDialog() {
        confirmRemovePmDialog = new ClosablePopup("Confirm Remove", true);
        confirmRemovePmDialog.setModal(true);
        VerticalPanel inputPanel = ViewUtil.createInputPanel();
        inputPanel.add(ViewUtil.createInputLabel("Remove Proposed Measure from this option?"));
        confirmRemovePmButton = ViewUtil.createButton("OK");
        inputPanel.add(ViewUtil.layoutCenter(confirmRemovePmButton));
        confirmRemovePmDialog.setWidget(inputPanel);
    }

    private void initCreateLocationDialog() {
        createLocationPopup = new ClosablePopup("Add Location", true);
        createLocationPopup.setModal(true);
        createLocationPopup.setWidget(editLocationView.asWidget());
    }

    private void initCreateContactDialog() {
        createContactPopup = new ClosablePopup("Add Contact", true);
        createContactPopup.setModal(true);
        createContactPopup.setWidget(editContactView.asWidget());
    }

    private void initEditPmDialog() {
        editProposedMeasurePopup = new ClosablePopup("Edit Proposed Measure", true);
        editProposedMeasurePopup.setModal(true);
        editProposedMeasurePopup.setWidget(editProposedMeasureView.asWidget());
    }

    public boolean validate() {
        boolean isValid = ViewUtil.validate(requiredFieldHandlers);
        if (!isValid) {
            return false;
        }
        if (useStatValues.getValue()) {
            isValid = ViewUtil.validate(statValueValidators);
        }
        if (createDefaultLocation.getValue()) {
        	isValid &= ViewUtil.validate(locationValidators);
        }
        return isValid;
    }

    public void updateModel() {
        ParreAnalysisDTO model = getModel();
        model.setName(getText(name));
        model.setDescription(getText(description));
        GWT.log("Has Baseline : " + ClientSingleton.hasBaseline());
        if (ClientSingleton.hasBaseline()) {
            return;
        }
        
        try {
            model.setContactDTO(new NameDescriptionDTO(getSelectedId(contacts)));
        } catch (NumberFormatException e) {
            ClientLoggingUtil.error("No contacts selected");
            model.setContactDTO(null);
        }

        try {
            model.setSiteLocationDTO(new NameDescriptionDTO(getSelectedId(locations)));
        } catch (NumberFormatException e) {
            ClientLoggingUtil.error("No location selected");
            model.setSiteLocationDTO(null);
        }

        model.setUseStatValues(useStatValues.getValue());
        if (model.getUseStatValues()) {
            StatValuesDTO dto = new StatValuesDTO();
            dto.setFatalityDTO(new AmountDTO(getBigDecimal(fatality)));
            dto.setSeriousInjuryDTO(new AmountDTO(getBigDecimal(seriousInjury)));
            model.setStatValuesDTO(dto);
            model.setOverrideFatality(overrideFatality.getValue());
            model.setOverrideSeriousInjury(overrideSeriousInjury.getValue());
        }
        
        model.setUseDefaultLocation(createDefaultLocation.getValue());
        if (model.getUseDefaultLocation()) {
        	model.setDefaultPostalCode(defaultPostalCode.getValue());
        	model.setDefaultLongitude(new BigDecimal(defaultLongitude.getValue()));
        	model.setDefaultLatitude(new BigDecimal(defaultLatitude.getValue()));
        } else {
        	model.setDefaultPostalCode("");
        	model.setDefaultLongitude(null);
        	model.setDefaultLatitude(null);
        }
    }

    public void updateView() {
        ParreAnalysisDTO model = getModel();
        proposedMeasuresView.setVisible(model.isOption());
        setText(name, model.getName());
        setText(description, model.getDescription());
        if (model.getContactDTO() != null) {
            setSelectedId(contacts, model.getContactDTO().getId());
        }
        if(model.getSiteLocationDTO() != null) {
            setSelectedId(locations, model.getSiteLocationDTO().getId());
        }
        useStatValues.setValue(model.getUseStatValues());
        statView.setVisible(model.getUseStatValues());
        overrideFatality.setValue(model.getOverrideFatality());
        overrideSeriousInjury.setValue(model.getOverrideSeriousInjury());

        StatValuesDTO statValuesDTO = model.getStatValuesDTO();
        if (statValuesDTO != null) {
            setBigDecimal(fatality, statValuesDTO.getFatalityDTO().getQuantity());
            setBigDecimal(seriousInjury, statValuesDTO.getSeriousInjuryDTO().getQuantity());
        }
        updateFatalityDisplay();
        updateSeriousInjuryDisplay();
        setHasEnabledWidgetValue(!model.isLocked());
        
        createDefaultLocation.setValue(model.getUseDefaultLocation());
        locationView.setVisible(model.getUseDefaultLocation());
        if (model.getUseDefaultLocation() != null && model.getUseDefaultLocation() == true) {
	        ViewUtil.setText(defaultPostalCode, model.getDefaultPostalCode());
	        ViewUtil.setBigDecimal(defaultLatitude, model.getDefaultLatitude());
	        ViewUtil.setBigDecimal(defaultLongitude, model.getDefaultLongitude());
        } else {
        	ViewUtil.setText(defaultPostalCode, "");
        	ViewUtil.setText(defaultLatitude, "");
        	ViewUtil.setText(defaultLongitude, "");
        }
    }

    private void setHasEnabledWidgetValue(boolean value) {
        for (HasEnabled hasEnabled : hasEnabledWidgets) {
            hasEnabled.setEnabled(value);
        }
    }

    public Widget asWidget() {
        return viewWidget;
    }

    public ListBox getContacts() {
        return contacts;
    }

    public ListBox getLocations() {
        return locations;
    }

    public HasClickHandlers getSaveButton() {
        return saveButton;
    }

    public SingleSelectionModel<ProposedMeasureDTO> getPmSelectionModel() {
        return proposedMeasureGrid.getSelectionModel();
    }

    public ActionSource<ProposedMeasureDTO> getPmSelectionSource() {
        return proposedMeasureGrid.getSelectionSource();
    }

    public List<ProposedMeasureDTO> getPmData() {
        return proposedMeasureGrid.getData();
    }

    public void setPmData(List<ProposedMeasureDTO> data) {
        proposedMeasureGrid.setData(data);
    }

    public void showConfirmRemovePmPopup() {
        confirmRemovePmDialog.center();
    }

    public void hideConfirmRemovePmPopup() {
        confirmRemovePmDialog.hide();
    }

    public HasClickHandlers getConfirmRemovePmButton() {
        return confirmRemovePmButton;
    }

    public HasClickHandlers getRemovePmButton() {
        return removePmButton;
    }

    public HasClickHandlers getAddPmButton() {
        return addPmButton;
    }

    public HasClickHandlers getSelectPmButton() {
        return selectPmButton;
    }

    public HasClickHandlers getCreatePmButton() {
        return createPmButton;
    }

    public ListBox getProposedMeasures() {
        return proposedMeasures;
    }

    public void refreshProposeMeasureView() {
        proposedMeasureGrid.refresh();
    }

    public HasClickHandlers getCreateLocationButton() {
        return createLocationButton;
    }

    @Override
    public HasClickHandlers getEditLocationButton() {
        return editLocationButton;
    }

    public HasClickHandlers getCreateContactButton() {
        return createContactButton;
    }

    @Override
    public HasClickHandlers getEditContactButton() {
        return editContactButton;
    }

    public void showSelectPmPopup(List<ProposedMeasureDTO> baselineProposedMeasures) {
        selectPmButton.setEnabled(false);
        proposedMeasures.clear();
        proposedMeasures.addItem("Select", "");
        for (ProposedMeasureDTO dto : baselineProposedMeasures) {
            proposedMeasures.addItem(dto.getName(), String.valueOf(dto.getId()));
        }
        selectPmPopup.center();
    }

    public void hideSelectPmPopup() {
        selectPmPopup.hide();
    }

    private void initSelectPmPopup() {
        selectPmPopup = new ClosablePopup("Select Proposed Measure", true);
        selectPmPopup.setModal(true);
        proposedMeasures = new ListBox();
        proposedMeasures.setWidth("100%");
        VerticalPanel inputPanel = ViewUtil.createInputPanel();
        inputPanel.add(proposedMeasures);
        selectPmButton = ViewUtil.createButton("Select");
        createPmButton = ViewUtil.createButton("Create");
        inputPanel.add(ViewUtil.layoutCenter(selectPmButton, createPmButton));
        selectPmPopup.setWidget(inputPanel);
        proposedMeasures.addChangeHandler(new ChangeHandler() {
            public void onChange(ChangeEvent event) {
                selectPmButton.setEnabled(proposedMeasures.getSelectedIndex() > 0);
            }
        });
    }

    public HasText getFatality() {
        return fatality;
    }

    public HasText getSeriousInjury() {
        return seriousInjury;
    }

    public HasValue<Boolean> getOverrideFatality() {
        return overrideFatality;
    }

    public HasValue<Boolean> getOverrideSeriousInjury() {
        return overrideSeriousInjury;
    }

    public HasValue<Boolean> getUseStatValues() {
        return useStatValues;
    }

	@Override
	public HasClickHandlers getGISCoordinatesButton() {
		return getGISCoordinatesButton;
	}

	@Override
	public HasText getZipCode() {
		return defaultPostalCode;
	}

	@Override
	public void setGISCoordinates(String latitude, String longitude) {
		defaultLatitude.setText(latitude);
		defaultLongitude.setText(longitude);
	}
    
    

}
