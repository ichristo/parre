/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.dp.view;

import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.event.logical.shared.HasCloseHandlers;
import com.google.gwt.user.client.ui.*;
import com.google.gwt.view.client.SelectionChangeEvent;
import com.google.gwt.view.client.SingleSelectionModel;

import java.util.List;

import org.parre.client.ClientSingleton;
import org.parre.client.common.view.*;
import org.parre.client.dp.presenter.ManageDpPresenter;
import org.parre.client.util.ActionSource;
import org.parre.client.util.ClosablePopup;
import org.parre.client.util.ViewUtil;
import org.parre.client.util.dataview.DataProvider;
import org.parre.client.util.dataview.DataProviderFactory;
import org.parre.shared.DpDTO;
import org.parre.shared.NameDescriptionDTO;

/**
 * The Class ManageDpView.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 7/16/11
 */
public class ManageDpView extends BaseAnalysisView implements ManageDpPresenter.Display {
    private DpGrid dpGrid;
    private ThreatCriteriaView threatCriteriaView;
    private Image searchButton;
    private Image clearButton;
    private VerticalPanel searchPanel;
    private Widget viewWidget;
    private DialogBox editPopup;
    private EditDpView editView;
    private DisplayToggleView displayToggleView;
    private Button addCatButton;
    private Button removeCatButton;
    private Button confirmRemoveCatButton;
    private NameDescriptionView createSubCatView;
    private DialogBox confirmRemoveCatPopup;
    private Label assetName;

    public ManageDpView(ThreatCriteriaView threatCriteriaView,
                        EditDpView editVulnerabilityView, DisplayToggleView displayToggleView) {
        super(new AnalysisCommandPanel(), new NameDescriptionView());
        this.threatCriteriaView = threatCriteriaView;
        this.editView = editVulnerabilityView;
        this.displayToggleView = displayToggleView;
        viewWidget = createView();
    }

    private Widget createView() {
        createSubCatView = new NameDescriptionView();
        createSubCatView.setHeader("Create Sub Category");
        searchPanel = new VerticalPanel();
        searchPanel.setWidth("100%");

        searchButton = ViewUtil.createImageButton("search-button.png", "Perform Search");
        clearButton = ViewUtil.createImageButton("clear-button.jpg", "Clear Search Results");
        searchPanel.add(threatCriteriaView.asWidget());

        searchPanel.add(ViewUtil.layoutCenter(searchButton, clearButton));

        DataProvider<DpDTO> dataProvider = DataProviderFactory.createDataProvider();
        dpGrid = new DpGrid(dataProvider);

        displayToggleView.setText("Search Criteria");
        displayToggleView.setDisplayToggleCommand(new DisplayToggleView.DisplayToggleCommand() {
            public void toggleDisplay(boolean show) {
                setSearchVisible(show);
            }
        });
        displayToggleView.setDisplayVisible(false);
        addCatButton = ViewUtil.createButton("Add Category");
        addCatButton.setEnabled(false);
        removeCatButton = ViewUtil.createButton("Remove Category");
        removeCatButton.setEnabled(false);
        //assetButtonPanel.setWidth("75%");
        VerticalPanel viewPanel = new VerticalPanel();

        viewPanel.setWidth("100%");
        viewPanel.setHeight("100%");
        viewPanel.add(displayToggleView.asWidget());
        viewPanel.add(searchPanel);
        viewPanel.add(getCommandPanel().asWidget());
        assetName = new Label();

        getCommandPanel().setToolTips("Back to Natural Threat", "Forward to Risk and Resilience Analysis");

        viewPanel.add(ViewUtil.layoutLeft("100%", addCatButton, removeCatButton, new Label("Asset Name : "), assetName));
        viewPanel.add(dpGrid.asWidget());
        viewPanel.setSpacing(5);
        dpGrid.asWidget().setVisible(false);
        initEditDpPopup();
        initRemoveCatConfirmPopup();
        dpGrid.getSelectionModel().addSelectionChangeHandler(new SelectionChangeEvent.Handler() {
            public void onSelectionChange(SelectionChangeEvent event) {
                DpDTO selectedObject = dpGrid.getSelectionModel().getSelectedObject();
                assetName.setText(selectedObject == null ? "" : selectedObject.getAssetName());
            }
        });
        return viewPanel;
    }

    private void initRemoveCatConfirmPopup() {
        confirmRemoveCatPopup = new ClosablePopup("Confirm Remove Category", true);
        confirmRemoveCatPopup.setModal(true);
        VerticalPanel view = new VerticalPanel();
        view.setStyleName("inputPanel");
        view.add(new HTML("Permanently remove category?"));
        confirmRemoveCatButton = ViewUtil.createButton("OK");
        view.add(ViewUtil.layoutCenter(confirmRemoveCatButton));
        confirmRemoveCatPopup.setWidget(view);
    }

    public void setSearchVisible(boolean value) {
        searchPanel.setVisible(value);
        dpGrid.setHeight(value ? "500px" : "650px");
    }

    private void initEditDpPopup() {
        editPopup = new ClosablePopup("Edit Dependency/Proximity Threat", true);
        editPopup.setModal(true);
        Widget widget = editView.asWidget();
        widget.setWidth("300px");
        editPopup.setWidget(widget);
    }

    @Override
    public void setCurrentAnalysis(NameDescriptionDTO currentAnalysis) {
        super.setCurrentAnalysis(currentAnalysis);
        dpGrid.setUseStatValue(ClientSingleton.getCurrentParreAnalysis().getUseStatValues());
        dpGrid.asWidget().setVisible(currentAnalysis != null);
    }

    public SingleSelectionModel<DpDTO> getSelectionModel() {
        return dpGrid.getSelectionModel();
    }

    public ActionSource<DpDTO> getSelectionSource() {
        return dpGrid.getSelectionSource();
    }

    public HasClickHandlers getSearchButton() {
        return searchButton;
    }

    public HasClickHandlers getClearButton() {
        return clearButton;
    }

    public void setData(List<DpDTO> results) {
        dpGrid.asWidget().setVisible(true);
        getCommandPanel().setAnalysisCount(results.size());
        dpGrid.setData(results);
        dpGrid.getSelectionModel().setSelected(null, true);
    }

    public void refreshSearchResultsView() {
        getCommandPanel().setAnalysisCount(getData().size());
        dpGrid.refresh();
    }

    public Widget asWidget() {
        return viewWidget;
    }

    public HasCloseHandlers<PopupPanel> getEditPopup() {
        return editPopup;
    }

    public void showEditPopup() {
        //editPopup.showRelativeTo(dpGrid.asWidget());
        editPopup.center();
        editPopup.show();
        setSearchResultsViewDisabled(true);
    }

    public void setSearchResultsViewDisabled(boolean value) {
        if (value) {
            dpGrid.asWidget().getElement().setAttribute("disabled", "true");
        }
        else {
            dpGrid.asWidget().getElement().removeAttribute("disabled");
        }
    }

    public void hideEditAssetPopup() {
        editPopup.hide();
    }

    public List<DpDTO> getData() {
        return dpGrid.getData();
    }

    public void setAddCategoryButtonEnabled(boolean value) {
        addCatButton.setEnabled(value);
    }

    public HasClickHandlers getAddCatButton() {
        return addCatButton;
    }

    public void setRemoveCatButtonEnabled(boolean value) {
        removeCatButton.setEnabled(value);
    }

    @Override
    public HasClickHandlers getPreviousButton() {
        return getCommandPanel().getPreviousButton();
    }

    @Override
    public HasClickHandlers getNextButton() {
        return getCommandPanel().getNextButton();
    }

    public HasClickHandlers getRemoveCatButton() {
        return removeCatButton;
    }

    public HasClickHandlers getConfirmRemoveCatButton() {
        return confirmRemoveCatButton;
    }

    public void showConfirmRemoveCatPopup() {
        confirmRemoveCatPopup.center();
    }

    public void hideConfirmRemoveCatPopup() {
        confirmRemoveCatPopup.hide();
    }

    public void showCreateSubCatPopup(DpDTO dpDTO) {
        createSubCatView.setModelAndUpdateView(dpDTO);
        createSubCatView.getPopup().center();
    }

    public void hideCreateSubCatPopup() {
        createSubCatView.getPopup().hide();
    }
    public HasClickHandlers getSaveSubCatButton() {
        return createSubCatView.getSaveButton();
    }

    public DpDTO getValidatedSubCat() {
        if (!createSubCatView.validate()) {
            return null;
        }
        hideCreateSubCatPopup();
        return (DpDTO) createSubCatView.updateAndGetModel();
    }

    @Override
    public void refreshView() {
        dpGrid.forceLayout();
    }
}
