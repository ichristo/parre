/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.dp.view;
  
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.user.client.ui.*;
  
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.parre.client.dp.presenter.EditDpPresenter;
import org.parre.client.util.*;
import org.parre.shared.AmountDTO;
import org.parre.shared.DpDTO;
  
/**
 * The Class EditDpView.
 */
public class EditDpView extends BaseModelView<DpDTO>  implements EditDpPresenter.Display {
    private List<FieldValidationHandler> requiredFieldHandlers;
    private Widget viewWidget;
    private TextBox assetName;
    private TextBox threatName;
    private TextBox fatalities;
    private TextBox seriousInjuries;
    private TextBox duration;
    private TextBox severity;
    private TextBox financialImpact;
    private TextBox economicImpact;
    private TextBox vulnerability;
    private TextBox lot;
    private Button saveButton;

    public EditDpView() {
        requiredFieldHandlers = new ArrayList<FieldValidationHandler>();
        viewWidget = buildView();
    }
  
    private Panel buildView() {
        VerticalPanel panel = new VerticalPanel();
        FlexTable inputTable = new FlexTable();
        inputTable.setCellSpacing(1);
  
        int rowNum = -1;
  
        final String width = "140px";
  
        assetName = ViewUtil.addTextBoxFormEntry(inputTable, ++rowNum, "Asset");
        assetName.setWidth("50px");
        assetName.setEnabled(false);
  
        threatName = ViewUtil.addTextBoxFormEntry(inputTable, ++rowNum, "Threat");
        threatName.setWidth("50px");
        threatName.setEnabled(false);
  
        fatalities = ViewUtil.addTextBoxFormEntry(inputTable, ++rowNum, "Fatalities", requiredFieldHandlers, PositiveIntegerValidator.INSTANCE);
        fatalities.setWidth("50px");
  
        seriousInjuries = ViewUtil.addTextBoxFormEntry(inputTable, ++rowNum, "Serious Injuries", requiredFieldHandlers, PositiveIntegerValidator.INSTANCE);
        seriousInjuries.setWidth("50px");
  
        duration = ViewUtil.addTextBoxFormEntry(inputTable, ++rowNum, "Duration (Days)", requiredFieldHandlers, PositiveNumberValidator.INSTANCE);
        duration.setWidth("50px");
  
        severity = ViewUtil.addTextBoxFormEntry(inputTable, ++rowNum, "Severity (MGD)", requiredFieldHandlers, PositiveNumberValidator.INSTANCE);
        severity.setWidth("95px");
  
        financialImpact = ViewUtil.addTextBoxFormEntry(inputTable, ++rowNum, "OFI ($)", requiredFieldHandlers, PositiveNumberValidator.INSTANCE);
        financialImpact.setWidth(width);
  
        economicImpact = ViewUtil.addTextBoxFormEntry(inputTable, ++rowNum, "CEI ($)", requiredFieldHandlers, PositiveNumberValidator.INSTANCE);
        economicImpact.setWidth(width);
  
        vulnerability = ViewUtil.addTextBoxFormEntry(inputTable, ++rowNum, "Vulnerability", requiredFieldHandlers, ProbabilityValueValidator.INSTANCE);
        vulnerability.setWidth(width);
  
        lot = ViewUtil.addTextBoxFormEntry(inputTable, ++rowNum, "Threat Likelihood", requiredFieldHandlers, ProbabilityValueValidator.INSTANCE);
        lot.setWidth(width);
  
        saveButton = ViewUtil.createButton("Save");
        panel.add(inputTable);
        panel.add(ViewUtil.layoutRight(saveButton));
        panel.setStyleName("inputPanel");
        return panel;
    }
     
  
    public boolean validate() {
        return ViewUtil.validate(requiredFieldHandlers);
    }
  
    public void updateModel() {
        DpDTO model = getModel();
        model.setFatalities(getQuantity(fatalities));
        model.setSeriousInjuries(getQuantity(seriousInjuries));
        model.setDurationDays(new BigDecimal(duration.getText()));
        model.setSeverityMgd(new BigDecimal(severity.getText()));
        model.setFinancialImpactDTO(new AmountDTO(getBigDecimal(financialImpact)));
        model.setEconomicImpactDTO(new AmountDTO(getBigDecimal(economicImpact)));
        model.setVulnerability(getProbability(vulnerability));
        model.setLot(getProbability(lot));
    }
  
    public void updateView() {
        DpDTO model = getModel();
        setText(assetName, model.getAssetId());
        setText(threatName, model.getName());
        setQuantity(fatalities, model.getFatalities());
        setQuantity(seriousInjuries, model.getSeriousInjuries());
        setBigDecimal(duration, model.getDurationDays());
        setBigDecimal(severity, model.getSeverityMgd());
        setBigDecimal(financialImpact, model.getFinancialImpactDTO().getQuantity());
        setBigDecimal(economicImpact, model.getEconomicImpactDTO().getQuantity());
        setProbability(vulnerability, model.getVulnerability());
        setProbability(lot, model.getLot());
    }
  
    public Widget asWidget() {
        return viewWidget;
    }
  
    public HasClickHandlers getSaveButton() {
        return saveButton;
    }
  
    @Override
    public TextBox getFinancialImpact() {
        return financialImpact;
    }
  
    @Override
    public TextBox getEconomicImpact() {
        return economicImpact;
    }
}