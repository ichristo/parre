/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.dp.view;

import com.google.gwt.cell.client.Cell;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;
import com.google.gwt.user.cellview.client.Column;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import org.parre.client.util.DoubleClickableTextCell;
import org.parre.client.util.ViewUtil;
import org.parre.client.util.dataview.DataColumn;
import org.parre.client.util.dataview.DataGridHeaders;
import org.parre.client.util.dataview.DataGridView;
import org.parre.client.util.dataview.DataProvider;
import org.parre.shared.DpDTO;

/**
 * The Class DpGrid.
 */
public class DpGrid extends DataGridView<DpDTO> {
    private boolean useStatValue;

    public DpGrid(DataProvider<DpDTO> dataProvider) {
		super(dataProvider);
		List<DataColumn<DpDTO>> columnList = new ArrayList<DataColumn<DpDTO>>();
        columnList.add(createAssetNameColumn());
        columnList.add(createThreatNameColumn());
        columnList.add(createFatalitiesColumn());
        columnList.add(createSeriousInjuriesColumn());
        columnList.add(createDurationColumn());
        columnList.add(createSeverityColumn());
        columnList.add(createFinancialImpactColumn());
        columnList.add(createFinancialTotalColumn());
        columnList.add(createEconomicImpactColumn());
        columnList.add(createVulnerabilityColumn());
        columnList.add(createLotColumn());
        columnList.add(createTotalRiskColumn());
        addColumns(columnList);
	}


    private DataColumn<DpDTO> createAssetNameColumn() {
    	Column<DpDTO, String> column = new Column<DpDTO, String>(
				createDoubleClickableTextCell()) {
			@Override
			public String getValue(DpDTO object) {
                return	object.getType().isSub() ? "" :
                        object.getAssetId();
			}

        };
		column.setSortable(false);

		Comparator<DpDTO> comparator = new Comparator<DpDTO>() {
			public int compare(DpDTO o1, DpDTO o2) {
				return o1.getName().compareTo(o2.getName());
			}
		};
		return new DataColumn<DpDTO>("Asset", column, comparator, 10);
	}
    private DataColumn<DpDTO> createThreatNameColumn() {
    	Column<DpDTO, String> column = new Column<DpDTO, String>(
				createDoubleClickableTextCell()) {
			@Override
			public String getValue(DpDTO object) {
                return	object.getType().isSub() ? object.getName() :
                        object.getName() + " - " + object.getDescription();
			}

            @Override
            public void render(Cell.Context context, DpDTO object, SafeHtmlBuilder sb) {
                if (object.getType().isSub()) {
                    sb.appendHtmlConstant("&nbsp;&nbsp;&nbsp;");
                }
                sb.appendEscaped(getValue(object));
            }
        };
		column.setSortable(false);

		Comparator<DpDTO> comparator = new Comparator<DpDTO>() {
			public int compare(DpDTO o1, DpDTO o2) {
				return o1.getName().compareTo(o2.getName());
			}
		};
		return new DataColumn<DpDTO>("Threat", column, comparator, 20);
	}



	private DataColumn<DpDTO> createFatalitiesColumn() {
		Column<DpDTO, String> column = new Column<DpDTO, String>(
				createDoubleClickableTextCell()) {
			@Override
			public String getValue(DpDTO object) {
				return	object.getType().isMainWithSub() ? "-" :
                        object.getFatalities().toString();
			}
		};
		column.setSortable(false);

		Comparator<DpDTO> comparator = new Comparator<DpDTO>() {
			public int compare(DpDTO o1, DpDTO o2) {
				return o1.getFatalities().compareTo(o2.getFatalities());
			}
		};
		return new DataColumn<DpDTO>("Fatalities", column, comparator, 10);
	}


    private DataColumn<DpDTO> createSeriousInjuriesColumn() {
    	Column<DpDTO, String> column = new Column<DpDTO, String>(
    			createDoubleClickableTextCell()) {
    		@Override
    		public String getValue(DpDTO object) {
    			return object.getType().isMainWithSub() ? "-" :
                        object.getSeriousInjuries().toString();
    		}
    	};
    	column.setSortable(false);

    	Comparator<DpDTO> comparator = new Comparator<DpDTO>() {
    		public int compare(DpDTO o1, DpDTO o2) {
    			return o1.getSeriousInjuries().compareTo(o2.getSeriousInjuries());
    		}
    	};
    	return new DataColumn<DpDTO>(DataGridHeaders.seriousInjuries(), column, comparator, 10);
    }

    private DataColumn<DpDTO> createDurationColumn() {
		Column<DpDTO, String> column = new Column<DpDTO, String>(
				createDoubleClickableTextCell()) {
			@Override
			public String getValue(DpDTO object) {
                return object.getType().isMainWithSub() ? "-" :
                        ViewUtil.toDisplayValue(object.getDurationDays());
			}
		};
		column.setSortable(false);

		Comparator<DpDTO> comparator = new Comparator<DpDTO>() {
			public int compare(DpDTO o1, DpDTO o2) {
				return o1.getDurationDays().compareTo(o2.getDurationDays());
			}
		};
		return new DataColumn<DpDTO>(DataGridHeaders.createHeader("Duration", "(Days)"), column, comparator, 10);
	}

	private DataColumn<DpDTO> createSeverityColumn() {
		Column<DpDTO, String> column = new Column<DpDTO, String>(
				createDoubleClickableTextCell()) {
			@Override
			public String getValue(DpDTO object) {
                return object.getType().isMainWithSub() ? "-" :
                        ViewUtil.toDisplayValue(object.getSeverityMgd());
			}
		};
		column.setSortable(false);

		Comparator<DpDTO> comparator = new Comparator<DpDTO>() {
			public int compare(DpDTO o1, DpDTO o2) {
				return o1.getSeverityMgd().compareTo(o2.getSeverityMgd());
			}
		};
		return new DataColumn<DpDTO>(DataGridHeaders.createHeader("Severity", "(MGD)"), column, comparator, 10);
	}

    private DataColumn<DpDTO> createFinancialImpactColumn() {
    	Column<DpDTO, String> column = new Column<DpDTO, String>(
    			createDoubleClickableTextCell()) {
    		@Override
    		public String getValue(DpDTO object) {
    			return object.getType().isMainWithSub() ? "-" :
                        ViewUtil.toDisplayValue(object.getFinancialImpactDTO().getQuantity());
    		}
    	};
    	column.setSortable(false);

    	Comparator<DpDTO> comparator = new Comparator<DpDTO>() {
    		public int compare(DpDTO o1, DpDTO o2) {
    			return o1.getFinancialImpactDTO().getQuantity().compareTo(o2.getFinancialImpactDTO().getQuantity());
    		}
    	};
    	return new DataColumn<DpDTO>(DataGridHeaders.financialImpact(), column, comparator, 10);
    }

    private DataColumn<DpDTO> createFinancialTotalColumn() {
        Column<DpDTO, String> column = new Column<DpDTO, String>(
                createDoubleClickableTextCell()) {
            @Override
            public String getValue(DpDTO object) {
                return !useStatValue || object.getType().isMainWithSub() ? "-" :
                        ViewUtil.toDisplayValue(object.getFinancialTotalDTO().getQuantity());
            }
        };
        column.setSortable(false);

        Comparator<DpDTO> comparator = new Comparator<DpDTO>() {
            public int compare(DpDTO o1, DpDTO o2) {
                return useStatValue ? o1.getFinancialTotalDTO().getQuantity().compareTo(o2.getFinancialTotalDTO().getQuantity()) :
                        0;
            }
        };
        return new DataColumn<DpDTO>(DataGridHeaders.financialTotal(), column, comparator, 10);

    }
    
    private DataColumn<DpDTO> createEconomicImpactColumn() {
        Column<DpDTO, String> column = new Column<DpDTO, String>(
				createDoubleClickableTextCell()) {
			@Override
			public String getValue(DpDTO object) {
                return object.getType().isMainWithSub() ? "-" :
                    ViewUtil.toDisplayValue(object.getEconomicImpactDTO().getQuantity());
			}

        };
		column.setSortable(false);

		Comparator<DpDTO> comparator = new Comparator<DpDTO>() {
			public int compare(DpDTO o1, DpDTO o2) {
				return o1.getName().compareTo(o2.getName());
			}
		};
		return new DataColumn<DpDTO>(DataGridHeaders.economicImpact(), column, comparator, 10);
    }
    
    private DataColumn<DpDTO> createLotColumn() {
        Column<DpDTO, String> column = new Column<DpDTO, String>(
                createDoubleClickableTextCell()) {
            @Override
            public String getValue(DpDTO object) {
                return object.getType().isMainWithSub() ? "-" :
                        ViewUtil.toDisplayValue(object.getLot());
            }
        };
        column.setSortable(false);

        Comparator<DpDTO> comparator = new Comparator<DpDTO>() {
            public int compare(DpDTO o1, DpDTO o2) {
                return o1.getLot().compareTo(o2.getLot());
            }
        };
        return new DataColumn<DpDTO>(DataGridHeaders.threatLikelihood(), column, comparator, 10);

    }

    private DataColumn<DpDTO> createVulnerabilityColumn() {
    	Column<DpDTO, String> column = new Column<DpDTO, String>(
    			createDoubleClickableTextCell()) {
    		@Override
    		public String getValue(DpDTO object) {
                return object.getType().isMainWithSub() ? "-" :
                        ViewUtil.toDisplayValue(object.getVulnerability());
    		}
    	};
    	column.setSortable(false);
    	
    	Comparator<DpDTO> comparator = new Comparator<DpDTO>() {
    		public int compare(DpDTO o1, DpDTO o2) {
    			return o1.getVulnerability().compareTo(o2.getVulnerability());
    		}
    	};
    	return new DataColumn<DpDTO>("Vulnerability", column, comparator, 10);
    }

    private DataColumn<DpDTO> createTotalRiskColumn() {
    	Column<DpDTO, String> column = new Column<DpDTO, String>(
    			createDoubleClickableTextCell()) {
    		@Override
    		public String getValue(DpDTO object) {
    			return ViewUtil.toDisplayValue(object.getTotalRiskDTO().getQuantity());
    		}
    	};
    	column.setSortable(false);

    	Comparator<DpDTO> comparator = new Comparator<DpDTO>() {
    		public int compare(DpDTO o1, DpDTO o2) {
    			return o1.getFinancialImpactDTO().getQuantity().compareTo(o2.getFinancialImpactDTO().getQuantity());
    		}
    	};
    	return new DataColumn<DpDTO>(DataGridHeaders.totalRisk(), column, comparator, 10);
    }


    private DoubleClickableTextCell<DpDTO> createDoubleClickableTextCell() {
        return new DoubleClickableTextCell<DpDTO>(getSelectionModel(), getSelectionSource());
    }

    public void setUseStatValue(boolean useStatValue) {
        this.useStatValue = useStatValue;
    }
}
