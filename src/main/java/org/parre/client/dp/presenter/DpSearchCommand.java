/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.dp.presenter;


import com.google.gwt.user.client.rpc.AsyncCallback;

import java.util.List;

import org.parre.client.ClientSingleton;
import org.parre.client.common.presenter.ThreatSearchCommand;
import org.parre.client.dp.event.DpSearchResultsEvent;
import org.parre.client.messaging.ServiceFactory;
import org.parre.client.util.BaseAsyncCommand;
import org.parre.client.util.BusyHandler;
import org.parre.client.util.ExecutableAsyncCallback;
import org.parre.client.util.View;
import org.parre.shared.DpDTO;
import org.parre.shared.NaturalThreatDTO;
import org.parre.shared.ThreatDTO;

/**
 * The Class DpSearchCommand.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 10/27/11
 */
public class DpSearchCommand implements ThreatSearchCommand {

    public void execute(final ThreatDTO dto, final View view) {
        ExecutableAsyncCallback<List<DpDTO>> exec =
                new ExecutableAsyncCallback<List<DpDTO>>(new BusyHandler(view), new BaseAsyncCommand<List<DpDTO>>() {
                    public void execute(AsyncCallback<List<DpDTO>> callback) {
                        ServiceFactory.getInstance().getDpService().
                                getSearchResults(dto, ClientSingleton.getParreAnalysisId(), callback);
                    }

                    @Override
                    public void handleResult(List<DpDTO> results) {
                        ClientSingleton.getEventBus().fireEvent(DpSearchResultsEvent.create(results));

                    }
                });
        exec.makeCall();
    }
}
