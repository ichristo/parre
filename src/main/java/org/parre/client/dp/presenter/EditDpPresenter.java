/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.dp.presenter;

import com.google.gwt.event.dom.client.*;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.TextBox;
import com.google.web.bindery.event.shared.EventBus;

import java.math.BigDecimal;

import org.parre.client.ClientSingleton;
import org.parre.client.dp.event.DpUpdatedEvent;
import org.parre.client.dp.event.EditDpEvent;
import org.parre.client.messaging.ServiceFactory;
import org.parre.client.util.*;
import org.parre.shared.DpDTO;
import org.parre.shared.NaturalThreatDTO;

/**
 * The Class EditDpPresenter.
 */
public class EditDpPresenter implements Presenter {
    private EventBus eventBus;
    private Display display;
    private BusyHandler busyHandler;

    /**
     * The Interface Display.
     */
    public interface Display extends ModelView<DpDTO> {
        HasClickHandlers getSaveButton();

        TextBox getFinancialImpact();

        TextBox getEconomicImpact();
    }

    public EditDpPresenter(Display display) {
        this.eventBus = ClientSingleton.getEventBus();
        this.display = display;
        busyHandler = new BusyHandler(display);
    }

    public void go() {
        bind();

    }

    private void bind() {
        display.getSaveButton().addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                if (!display.validate()) {
                    return;
                }
                ExecutableAsyncCallback<DpDTO> exec =
                        new ExecutableAsyncCallback<DpDTO>(busyHandler, new BaseAsyncCommand<DpDTO>() {
                            public void execute(AsyncCallback<DpDTO> callback) {
                                ServiceFactory.getInstance().getDpService().
                                        save(display.updateAndGetModel(), callback);
                            }

                            @Override
                            public void handleResult(DpDTO result) {
                                eventBus.fireEvent(DpUpdatedEvent.create(result));
                            }
                        });
                exec.makeCall();
                display.updateView();
            }
        });
        display.getFinancialImpact().addBlurHandler(new BlurHandler() {
            @Override
            public void onBlur(BlurEvent blurEvent) {
                try {
                    display.getFinancialImpact().setText(ViewUtil.toDisplayValue(new BigDecimal(display.getFinancialImpact().getText().replace(",", ""))));
                } catch(NumberFormatException n) {
                    MessageDialog.popup("Owner's Financial Impact must be a number!");
                    display.getFinancialImpact().setFocus(true);
                }
            }
        });
        display.getFinancialImpact().addFocusHandler(new FocusHandler() {
            @Override
            public void onFocus(FocusEvent event) {
                display.getFinancialImpact().setText(display.getFinancialImpact().getText());
            }
        });
        display.getEconomicImpact().addBlurHandler(new BlurHandler() {
            @Override
            public void onBlur(BlurEvent blurEvent) {
                try {
                    display.getEconomicImpact().setText(ViewUtil.toDisplayValue(new BigDecimal(display.getEconomicImpact().getText().replace(",", ""))));
                } catch(NumberFormatException n) {
                    MessageDialog.popup("Community's Economic Impact must be a number!");
                    display.getEconomicImpact().setFocus(true);
                }
            }
        });
        display.getEconomicImpact().addFocusHandler(new FocusHandler() {
            @Override
            public void onFocus(FocusEvent event) {
                display.getEconomicImpact().setText(display.getEconomicImpact().getText());
            }
        });
        eventBus.addHandler(EditDpEvent.TYPE, new EditDpEvent.Handler() {
            public void onEvent(EditDpEvent event) {
                DpDTO editDTO = event.getDpDTO();
                display.setModelAndUpdateView(editDTO);
            }
        });
    }

}
