/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.rrm.view;
  
import java.util.List;
  
import org.parre.client.ClientSingleton;
import org.parre.client.common.view.NameDescriptionView;
import org.parre.client.rrm.presenter.ManageRrmPresenter;
import org.parre.client.util.ActionSource;
import org.parre.client.util.BaseView;
import org.parre.client.util.ClosablePopup;
import org.parre.client.util.FieldValidationHandler;
import org.parre.client.util.PositiveNumberValidator;
import org.parre.client.util.ViewUtil;
import org.parre.client.util.dataview.DataProvider;
import org.parre.client.util.dataview.DataProviderFactory;
import org.parre.shared.AmountDTO;
import org.parre.shared.BenefitCostDTO;
import org.parre.shared.NameDescriptionDTO;
import org.parre.shared.RiskBenefitAnalysisDTO;
import org.parre.shared.RiskBenefitDTO;
import org.parre.shared.types.MoneyUnitType;
import org.parre.shared.types.RiskBenefitMetricType;
  
import com.google.gwt.core.shared.GWT;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.view.client.SelectionChangeEvent;
import com.google.gwt.view.client.SingleSelectionModel;
  
/**
 * The Class ManageRrmView.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 7/16/11
 */
public class ManageRrmView extends BaseView implements ManageRrmPresenter.Display {
    private Widget viewWidget;
    private RiskBenefitGrid riskBenefitGrid;
    private TextBox budget;
    private ListBox metrics;
    private FieldValidationHandler budgetValidationHandler;
    private Button updateBudgetButton;
    private HTML assetThreatLabel;
    private Button newOptionButton;
    private NameDescriptionView nameDescriptionView;
    private HorizontalPanel benefitCostPanel;
    private SimplePanel benefitCostPanelContainer;
    private Button confirmNewOptionButton = ViewUtil.createButton("Yes, create a new option.");
    private DialogBox confirmNewOptionDialog;
    private Integer dispOrder = 0;
    private VerticalPanel optionViewPanel = new VerticalPanel();
  
    public ManageRrmView() {
        nameDescriptionView = new NameDescriptionView();
        viewWidget = createView();
    }
  
    private Widget createView() {
        Label header = new Label("Risk/Resilience Management");
        header.setStyleName("inputPanelHeader");
        VerticalPanel viewPanel = new VerticalPanel();
        viewPanel.setStyleName("inputPanel");
        viewPanel.add(header);
        budget = new TextBox();
        budgetValidationHandler = new FieldValidationHandler(budget, PositiveNumberValidator.INSTANCE);
        updateBudgetButton = ViewUtil.createButton("Update");
        assetThreatLabel = new HTML();
        newOptionButton = ViewUtil.createButton("Add New Option");
  
        metrics = new ListBox(false);
        ViewUtil.initListBox(metrics, RiskBenefitMetricType.values());
        viewPanel.add(ViewUtil.horizontalPanel(2, ViewUtil.createInputLabel("Budget: $"), budget,
                updateBudgetButton, newOptionButton, /*viewAnalysisButton,*/
                assetThreatLabel, metrics));
        viewPanel.add(initRiskBenefitGrid());
        riskBenefitGrid.getSelectionModel().addSelectionChangeHandler(new SelectionChangeEvent.Handler() {
            public void onSelectionChange(SelectionChangeEvent event) {
                RiskBenefitDTO selectedObject = riskBenefitGrid.getSelectionModel().getSelectedObject();
                if (selectedObject == null) {
                    assetThreatLabel.setText("- - -");
                } else {
                    assetThreatLabel.setText(selectedObject.getAssetName() + " - " + selectedObject.getThreatDescription());
                }
            }
        });
        benefitCostPanel = ViewUtil.horizontalPanel(1);
        benefitCostPanelContainer = new SimplePanel(new ScrollPanel(benefitCostPanel));
        benefitCostPanelContainer.setWidth("100%");
        viewPanel.add(benefitCostPanelContainer);
        createConfirmDialog();
        return viewPanel;
    }
  
    private Widget initRiskBenefitGrid() {
        DataProvider<RiskBenefitDTO> dataProvider = DataProviderFactory.createDataProvider();
        riskBenefitGrid = new RiskBenefitGrid(dataProvider, ClientSingleton.getRiskBenefitOptions());
        riskBenefitGrid.setHeight("550px");
        riskBenefitGrid.setWidth("100%");
        return riskBenefitGrid.asWidget();
    }
  
    private void createConfirmDialog() {
        confirmNewOptionDialog = new ClosablePopup("Confirm New Option", true);
        confirmNewOptionDialog.setModal(true);
        optionViewPanel.setStyleName("inputPanel");
        confirmNewOptionDialog.setWidget(optionViewPanel);
    }
     
    private Integer getDispOrder(){
        return ClientSingleton.getDisplayOrder();
    }
     
    @Override
    public void showConfirmOption() {
        confirmNewOptionDialog.show();
        confirmNewOptionDialog.center();
    }
     
    @Override
    public void setDispOrder(Integer orderNo) {
        //GWT.log("orderNo = " + orderNo);
        optionViewPanel.clear();
        if(orderNo == 0 && ClientSingleton.hasOptionAnalyses()){
            optionViewPanel.add(new HTML("Do you want to create a new option?"));
        }else if(orderNo > 0){
            optionViewPanel.add(new HTML("Do you want to create a new option?"));
        } else{
            optionViewPanel.add(new HTML("Do you want to create a new option?  No changes can be made to the baseline once an option is created."));
        }
          optionViewPanel.add(ViewUtil.layoutCenter(confirmNewOptionButton));
    }
  
  
    public SingleSelectionModel<RiskBenefitDTO> getSelectionModel() {
        return riskBenefitGrid.getSelectionModel();
    }
  
    public ActionSource<RiskBenefitDTO> getSelectionSource() {
        return riskBenefitGrid.getSelectionSource();
    }
  
    public void setAnalysis(RiskBenefitAnalysisDTO analysisDTO) {
        ViewUtil.setBigDecimal(budget, analysisDTO.getBudgetDTO().getQuantity());
        if (metrics.getSelectedIndex() == 0) {
            metrics.setSelectedIndex(1);
        }
        riskBenefitGrid.refreshColumns(analysisDTO.getOptionAnalysisDTOs(),
                RiskBenefitMetricType.valueOf(ViewUtil.getSelectedValue(metrics)));
        riskBenefitGrid.setData(analysisDTO.getRiskBenefitDTOs());
        riskBenefitGrid.refresh();
        benefitCostPanel.clear();
        List<BenefitCostDTO> benefitCostDTOs = analysisDTO.getBenefitCostDTOs();
        for (BenefitCostDTO benefitCostDTO : benefitCostDTOs) {
            benefitCostPanel.add(new BenefitCostView(benefitCostDTO));
        }
    }
  
    public HasClickHandlers getNewOptionButton() {
        return newOptionButton;
    }
  
    public HasClickHandlers getUpdateBudgetButton() {
        return updateBudgetButton;
    }
  
    public void showCreateAnalysisPopup(NameDescriptionDTO dto) {
        nameDescriptionView.setModel(dto);
        nameDescriptionView.getPopup().center();
    }
  
    public NameDescriptionDTO getAnalysisNameDescription() {
        if (!nameDescriptionView.validate()) {
            return null;
        }
        nameDescriptionView.getPopup().hide();
        return nameDescriptionView.updateAndGetModel();
    }
  
    public HasClickHandlers getSaveAnalysisButton() {
        return nameDescriptionView.getSaveButton();
    }
  
    public ListBox getMetrics() {
        return metrics;
    }
  
    @Override
    public HasClickHandlers getConfirmOptionButton() {
        return confirmNewOptionButton;
    }
  
     
    @Override
    public void hideConfirmOption() {
        confirmNewOptionDialog.hide();
    }
  
    public AmountDTO getBudgetAmount() {
        if (!budgetValidationHandler.checkValue()) {
            return null;
        }
        return new AmountDTO(ViewUtil.getBigDecimal(budget), MoneyUnitType.DOLLAR);
    }
  
    public Widget asWidget() {
        return viewWidget;
    }
  
  
}
