/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.rrm.view;

import com.google.gwt.user.client.ui.*;

import java.math.BigDecimal;

import org.parre.client.util.ViewUtil;
import org.parre.shared.BenefitCostDTO;

/**
 * The Class BenefitCostView.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 5/7/12
 */
public class BenefitCostView implements IsWidget {

    private BenefitCostDTO data;
    private Widget viewWidget;
    public BenefitCostView(BenefitCostDTO data) {
        this.data = data;
        viewWidget = createView();
    }

    private Widget createView() {
        FlexTable dataTable = new FlexTable();
        int currentRow = -1;
        ViewUtil.addFormEntry(dataTable, ++currentRow, "Total Gross Benefits : ", createLabel(data.getTotalGrossBenefits()));
        ViewUtil.addFormEntry(dataTable, ++currentRow, "Present Value Cost : ", createLabel(data.getPresentValueCost()));
        ViewUtil.addFormEntry(dataTable, ++currentRow, "Net Benefits : ", createLabel(data.getNetBenefits()));
        ViewUtil.addFormEntry(dataTable, ++currentRow, "B/C Ratio : ",
                ViewUtil.createInputLabel(ViewUtil.displayFormatForRatio(data.getBenefitCostRatio())));
        VerticalPanel inputPanel = ViewUtil.createInputPanel();
        inputPanel.add(ViewUtil.createSubHeader(data.getOptionName()));
        inputPanel.add(dataTable);
        return inputPanel;
    }

    private HTML createLabel(BigDecimal value) {
        return ViewUtil.createInputLabel("$" + ViewUtil.toDisplayValue(value));
    }

    public Widget asWidget() {
        return viewWidget;
    }
}
