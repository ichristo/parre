/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.rrm.view;

import com.google.gwt.cell.client.Cell;
import com.google.gwt.core.shared.GWT;
import com.google.gwt.dom.client.Style;
import com.google.gwt.safehtml.shared.SafeHtml;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;
import com.google.gwt.user.cellview.client.Column;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import org.parre.client.util.*;
import org.parre.client.util.dataview.DataColumn;
import org.parre.client.util.dataview.DataGridHeaders;
import org.parre.client.util.dataview.DataGridView;
import org.parre.client.util.dataview.DataProvider;
import org.parre.shared.*;
import org.parre.shared.types.RiskBenefitMetricType;

/**
 * User: Swarn S. Dhaliwal
 * Date: 7/10/11
 * Time: 5:38 PM
*/
public class RiskBenefitGrid extends DataGridView<RiskBenefitDTO> {
    private static final int COLUMN_WIDTH = 200; //Original Value: 120
    private int widthPx;
    private MetricRenderer metricRenderer = AmountMetricRenderer.INSTANCE;

    public RiskBenefitGrid(DataProvider<RiskBenefitDTO> dataProvider, List<? extends NameDescriptionDTO> options) {
        super(dataProvider);
        widthPx = initColumns(options);

    }

    public int getWidthPx() {
        return widthPx;
    }

    public void refreshColumns(List<? extends NameDescriptionDTO> options, RiskBenefitMetricType metricType) {
        clearGrid();
        updateMetricType(metricType);
        //ClientLoggingUtil.info("Number Of Columns : " + getDataGrid().getColumnCount());
        //ClientLoggingUtil.info("Number of New Options : " + options.size());
        widthPx = initColumns(options);
        //ClientLoggingUtil.info("New Number Of Columns : " + getDataGrid().getColumnCount());
    }

    private int initColumns(List<? extends NameDescriptionDTO> options) {
        ClientLoggingUtil.info("Creating Columns");
        List<DataColumn<RiskBenefitDTO>> columnList = new ArrayList<DataColumn<RiskBenefitDTO>>();
        columnList.add(createAssetColumn());
        columnList.add(createThreatColumn());
        columnList.add(createBaselineColumn());
        int optionNumber = 0;
        for (NameDescriptionDTO option : options) {
            columnList.add(createOptionColumn(option, optionNumber));
            columnList.add(createMetricDeltaColumn(option, optionNumber));
            optionNumber++;
        }
        addColumns(columnList);
        int widthPx = (2 * COLUMN_WIDTH) + (2 * COLUMN_WIDTH * options.size());
        //GWT.log("widthPx == "+ widthPx);
        //ClientLoggingUtil.info("Adding Columns, setting width to : " + widthPx);
        return widthPx;
    }

    private DoubleClickableTextCell<RiskBenefitDTO> createDoubleClickableTextCell() {
        return new DoubleClickableTextCell<RiskBenefitDTO>(getSelectionModel(), getSelectionSource());
    }

    private DataColumn<RiskBenefitDTO> createAssetColumn() {
        Column<RiskBenefitDTO, String> column = new Column<RiskBenefitDTO, String>(
                createDoubleClickableTextCell()) {
            @Override
            public String getValue(RiskBenefitDTO object) {
                return object.getAssetId() + " - " + object.getAssetName();
            }
        };
        column.setSortable(true);

        Comparator<RiskBenefitDTO> comparator = new Comparator<RiskBenefitDTO>() {
            public int compare(RiskBenefitDTO o1, RiskBenefitDTO o2) {
                return o1.getAssetId().compareTo(o2.getAssetId());
            }
        };
        return new DataColumn<RiskBenefitDTO>("Asset", column, comparator, COLUMN_WIDTH, Style.Unit.PX);

    }

    private DataColumn<RiskBenefitDTO> createThreatColumn() {
        Column<RiskBenefitDTO, String> column = new Column<RiskBenefitDTO, String>(
                createDoubleClickableTextCell()) {
            @Override
            public String getValue(RiskBenefitDTO object) {
                return object.getThreatName() + " - " + object.getThreatDescription();
            }
        };
        column.setSortable(true);

        Comparator<RiskBenefitDTO> comparator = new Comparator<RiskBenefitDTO>() {
            public int compare(RiskBenefitDTO o1, RiskBenefitDTO o2) {
                return o1.getThreatName().compareTo(o2.getThreatName());
            }
        };
        return new DataColumn<RiskBenefitDTO>("Threat", column, comparator, COLUMN_WIDTH, Style.Unit.PX);
    }

    private DataColumn<RiskBenefitDTO> createBaselineColumn() {
        final DoubleClickableTextCell<RiskBenefitDTO> cell = new DoubleClickableTextCell<RiskBenefitDTO>(getSelectionModel(), getSelectionSource());
        Column<RiskBenefitDTO, String> column = new Column<RiskBenefitDTO, String>(cell) {
            @Override
            public String getValue(RiskBenefitDTO object) {
                return ViewUtil.toDisplayValue(object.getBaselineQuantity());
            }

            @Override
            public void render(Cell.Context context, RiskBenefitDTO object, SafeHtmlBuilder sb) {
                sb.appendEscaped(getValue(object));
            }
        };
        column.setSortable(true);

        Comparator<RiskBenefitDTO> comparator = new Comparator<RiskBenefitDTO>() {
            public int compare(RiskBenefitDTO o1, RiskBenefitDTO o2) {
                return SharedCalculationUtil.compare(o1.getBaselineQuantity(), o2.getBaselineQuantity());
            }
        };

        return new DataColumn<RiskBenefitDTO>("Baseline", column, comparator, COLUMN_WIDTH, Style.Unit.PX);

    }

    private DataColumn<RiskBenefitDTO> createOptionColumn(final NameDescriptionDTO optionDTO, final int optionNumber) {
        final DoubleClickableTextCell<RiskBenefitDTO> cell = new DoubleClickableTextCell<RiskBenefitDTO>(getSelectionModel(), getSelectionSource());
        Column<RiskBenefitDTO, String> column = new Column<RiskBenefitDTO, String>(cell) {
            @Override
            public String getValue(RiskBenefitDTO object) {
                return ViewUtil.toDisplayValue(object.getOptionQuantity(optionNumber));
            }

            @Override
            public void render(Cell.Context context, RiskBenefitDTO object, SafeHtmlBuilder sb) {
                SafeHtml riskLabel = DivStyleTemplate.INSTANCE.render(getValue(object), "");
                sb.append(riskLabel);
            }
        };
        column.setSortable(true);
       

        Comparator<RiskBenefitDTO> comparator = new Comparator<RiskBenefitDTO>() {
            public int compare(RiskBenefitDTO o1, RiskBenefitDTO o2) {
                return SharedCalculationUtil.compare(o1.getOptionQuantity(optionNumber), o2.getOptionQuantity(optionNumber));
            }
        };

        return new DataColumn<RiskBenefitDTO>(DataGridHeaders.createBigHeader(optionDTO.getName(), "Total"), column, comparator, COLUMN_WIDTH, Style.Unit.PX);

    }

    private DataColumn<RiskBenefitDTO> createMetricDeltaColumn(NameDescriptionDTO optionDTO, final int optionNumber) {
        final DoubleClickableTextCell<RiskBenefitDTO> cell = new DoubleClickableTextCell<RiskBenefitDTO>(getSelectionModel(), getSelectionSource());
        Column<RiskBenefitDTO, String> column = new Column<RiskBenefitDTO, String>(cell) {
            @Override
            public String getValue(RiskBenefitDTO riskBenefitDTO) {
                return ViewUtil.toDisplayValue(riskBenefitDTO.getOptionDelta(optionNumber));
            }

            @Override
        public void render(Cell.Context context, RiskBenefitDTO object, SafeHtmlBuilder sb) {
                SafeHtml benefitLabel;
                if(object.getOptionDelta(optionNumber).doubleValue() > 0) {
                    benefitLabel = DivStyleTemplate.INSTANCE.render(getValue(object), "positiveLabel");
                }
                else if(object.getOptionDelta(optionNumber).doubleValue() == 0) {
                    benefitLabel = DivStyleTemplate.INSTANCE.render(getValue(object), "zeroLabel");
                }
                else {
                    benefitLabel = DivStyleTemplate.INSTANCE.render(getValue(object), "negativeLabel");
                }
                sb.append(benefitLabel);
            }
        };
        column.setSortable(true);

        Comparator<RiskBenefitDTO> comparator = new Comparator<RiskBenefitDTO>() {
            public int compare(RiskBenefitDTO o1, RiskBenefitDTO o2) {
                return SharedCalculationUtil.compare(o1.getOptionDelta(optionNumber), o2.getOptionDelta(optionNumber));
            }
        };

        return new DataColumn<RiskBenefitDTO>(DataGridHeaders.createBigHeader(optionDTO.getName(), "Change"), column, comparator, COLUMN_WIDTH, Style.Unit.PX);
    }

    public void updateMetricType(RiskBenefitMetricType metricType) {
        this.metricRenderer = getMetricRenderer(metricType);
    }

    private MetricRenderer getMetricRenderer(RiskBenefitMetricType metricType) {
        switch (metricType) {
            case SERIOUS_INJURIES:
            case FATALITIES:
                return QuantityMetricRenderer.INSTANCE;
            default:
                return AmountMetricRenderer.INSTANCE;
        }
    }
}
