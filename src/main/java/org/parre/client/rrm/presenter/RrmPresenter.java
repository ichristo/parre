/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.rrm.presenter;

import org.parre.client.ClientSingleton;
import org.parre.client.ViewFactory;
import org.parre.client.messaging.RiskBenefitServiceAsync;
import org.parre.client.messaging.ServiceFactory;
import org.parre.client.nav.event.RiskBenefitNavEvent;
import org.parre.client.rrm.event.ManageRrmNavEvent;
import org.parre.client.util.Presenter;
import org.parre.client.util.View;
import org.parre.client.util.ViewContainer;

import com.google.gwt.event.shared.EventBus;

/**
 * The Class RrmPresenter.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 7/19/11
 */
public class RrmPresenter implements Presenter {
    private Integer manageRrmViewIndex;
    private RiskBenefitServiceAsync service;
    private EventBus eventBus;
    private Display display;

    /**
     * The Interface Display.
     */
    public interface Display extends View, ViewContainer {

    }

    public RrmPresenter(Display display) {
        this.service = ServiceFactory.getInstance().getRrmService();
        this.eventBus = ClientSingleton.getEventBus();
        this.display = display;
    }

    public void go() {
        initViewLoaders();
        bind();
        display.showView(manageRrmViewIndex);
    }

    private void initViewLoaders() {
        View view = ViewFactory.createManageRrmView();
        view.getPresenter().go();
        manageRrmViewIndex = display.addView(view.asWidget());
    }

    private void bind() {
        eventBus.addHandler(RiskBenefitNavEvent.TYPE, new RiskBenefitNavEvent.Handler() {
            public void onEvent(RiskBenefitNavEvent event) {
                if (!ClientSingleton.hasBaseline()) {
                    return;
                }
                display.showView(manageRrmViewIndex);
                eventBus.fireEvent(ManageRrmNavEvent.create());
            }
        });
    }
}
