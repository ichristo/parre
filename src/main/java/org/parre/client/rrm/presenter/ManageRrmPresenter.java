/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.rrm.presenter;

import java.util.List;

import org.parre.client.ClientSingleton;
import org.parre.client.event.EditParreAnalysisEvent;
import org.parre.client.event.ParreAnalysisCreatedEvent;
import org.parre.client.messaging.ServiceFactory;
import org.parre.client.rrm.event.ManageRrmNavEvent;
import org.parre.client.util.ActionSource;
import org.parre.client.util.BaseAsyncCommand;
import org.parre.client.util.BusyHandler;
import org.parre.client.util.ExecutableAsyncCallback;
import org.parre.client.util.MessageDialog;
import org.parre.client.util.Presenter;
import org.parre.client.util.View;
import org.parre.client.util.ViewUtil;
import org.parre.shared.AmountDTO;
import org.parre.shared.NameDescriptionDTO;
import org.parre.shared.ParreAnalysisDTO;
import org.parre.shared.RiskBenefitAnalysisDTO;
import org.parre.shared.RiskBenefitDTO;
import org.parre.shared.types.RiskBenefitMetricType;

import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.view.client.SingleSelectionModel;


/**
 * User: Swarn S. Dhaliwal
 * Date: 8/21/11
 * Time: 5:03 PM
*/
public class ManageRrmPresenter implements Presenter {

    private RiskBenefitAnalysisDTO analysisDTO;
    private BusyHandler busyHandler;

    /**
     * The Interface Display.
     */
    public static interface Display extends View {

        SingleSelectionModel<RiskBenefitDTO> getSelectionModel();

        ActionSource<RiskBenefitDTO> getSelectionSource();

        void setAnalysis(RiskBenefitAnalysisDTO analysisDTO);

        HasClickHandlers getUpdateBudgetButton();

        AmountDTO getBudgetAmount();

        HasClickHandlers getNewOptionButton();

        void showCreateAnalysisPopup(NameDescriptionDTO dto);

        NameDescriptionDTO getAnalysisNameDescription();

        HasClickHandlers getSaveAnalysisButton();

        ListBox getMetrics();

        HasClickHandlers getConfirmOptionButton();

        void showConfirmOption();

        void hideConfirmOption();
        
        void setDispOrder(Integer orderNo);
    }

    private EventBus eventBus;
    private Display display;

    public ManageRrmPresenter(Display display) {
        this.eventBus = ClientSingleton.getEventBus();
        this.display = display;
        this.busyHandler = new BusyHandler(display);
    }

    public void go() {
        bind();
    }


    private void bind() {
    	//Disabling this, it is kicking the process to get the first data set twice.  This would only
    	//be an issue if the "deep link" worked for RRM page, which it doesn't
        //initAnalysis();
        eventBus.addHandler(ManageRrmNavEvent.TYPE, new ManageRrmNavEvent.Handler() {
            public void onEvent(ManageRrmNavEvent event) {
                initAnalysis();
            }
        });
        display.getUpdateBudgetButton().addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                AmountDTO budgetAmount = display.getBudgetAmount();
                if (budgetAmount != null) {
                    updateBudget(budgetAmount);
                }
            }
        });
        display.getConfirmOptionButton().addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                display.hideConfirmOption();
                display.showCreateAnalysisPopup(new NameDescriptionDTO());
            }
        });
        display.getNewOptionButton().addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
            	Integer orderNo = ClientSingleton.getDisplayOrder();
            	display.setDispOrder(orderNo);
                display.showConfirmOption();
            }
        });
        display.getSaveAnalysisButton().addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                NameDescriptionDTO dto = display.getAnalysisNameDescription();
                if (dto != null) {
                    startOptionAnalysis(dto);
                }
            }
        });
        display.getMetrics().addChangeHandler(new ChangeHandler() {
            public void onChange(ChangeEvent event) {
                RiskBenefitMetricType currentMetricType = getCurrentMetricType();
                if (currentMetricType != null) {
                    getMetricAnalysis(currentMetricType);
                }
            }
        });

    }

    private RiskBenefitMetricType getCurrentMetricType() {
        ListBox metrics = display.getMetrics();
        if (metrics.getSelectedIndex() == 0) {
            return null;
        }
        String selectedMetric = ViewUtil.getSelectedValue(metrics);
        return RiskBenefitMetricType.valueOf(selectedMetric);
    }

    private void startOptionAnalysis(final NameDescriptionDTO dto) {
        new ExecutableAsyncCallback<ParreAnalysisDTO>(busyHandler, new BaseAsyncCommand<ParreAnalysisDTO>() {
            public void execute(AsyncCallback<ParreAnalysisDTO> callback) {
                ServiceFactory.getInstance().getRrmService().startOptionAnalysis(analysisDTO.getId(), dto, callback);
            }

            @Override
            public void handleResult(ParreAnalysisDTO results) {
                eventBus.fireEvent(ParreAnalysisCreatedEvent.create(results));
                eventBus.fireEvent(EditParreAnalysisEvent.create(results));
            }
        }).makeCall();

    }

    private void updateBudget(final AmountDTO budgetAmount) {

        new ExecutableAsyncCallback<Void>(busyHandler, new BaseAsyncCommand<Void>() {
            public void execute(AsyncCallback<Void> callback) {
                ServiceFactory.getInstance().getRrmService().updateBudget(analysisDTO.getId(), budgetAmount, callback);
            }

            @Override
            public void handleResult(Void results) {
                analysisDTO.setBudgetDTO(budgetAmount);
            }
        }).makeCall();

    }

    private void initAnalysis() {
        RiskBenefitMetricType currentMetricType = getCurrentMetricType();
        final RiskBenefitMetricType metricType = currentMetricType != null ? currentMetricType :
                RiskBenefitMetricType.RISK;
        new ExecutableAsyncCallback<RiskBenefitAnalysisDTO>(busyHandler, new BaseAsyncCommand<RiskBenefitAnalysisDTO>() {
            public void execute(AsyncCallback<RiskBenefitAnalysisDTO> callback) {
                ServiceFactory.getInstance().getRrmService().getAnalysis(metricType, callback);
            }

            @Override
            public void handleResult(RiskBenefitAnalysisDTO results) {
                if (results == null) {
                    MessageDialog.popup("An analysis was not found", true);
                    return;
                }
                analysisDTO = results;
                display.setAnalysis(analysisDTO);
            }
        }).makeCall();

    }

    private void getMetricAnalysis(final RiskBenefitMetricType metricType) {

        new ExecutableAsyncCallback<List<RiskBenefitDTO>>(busyHandler, new BaseAsyncCommand<List<RiskBenefitDTO>>() {
            public void execute(AsyncCallback<List<RiskBenefitDTO>> callback) {
                ServiceFactory.getInstance().getRrmService().getMetricAnalysis(metricType, callback);
            }

            @Override
            public void handleResult(List<RiskBenefitDTO> results) {
                analysisDTO.setRiskBenefitDTOs(results);
                display.setAnalysis(analysisDTO);
            }
        }).makeCall();

    }

}
