/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client;

import org.parre.client.event.DisplayLoginEvent;
import org.parre.client.event.LoginEvent;
import org.parre.client.event.LogoutEvent;
import org.parre.client.messaging.ServiceFactory;
import org.parre.client.nav.ParreNavAdapter;
import org.parre.client.nav.ParreNavEventMapper;
import org.parre.client.util.*;
import org.parre.client.util.nav.NavAdapter;
import org.parre.client.util.nav.NavManager;
import org.parre.shared.AppInitData;
import org.parre.shared.LoginInfo;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.Scheduler;
import com.google.gwt.event.shared.UmbrellaException;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.RootLayoutPanel;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.Widget;

/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class ParreEntryPoint implements EntryPoint, RootViewContainer {
    private AsyncRootViewLoader loginViewLoader;
    private AsyncRootViewLoader mainViewLoader;

    private void initViewLoaders() {
        loginViewLoader = new AsyncRootViewLoader(this) {
            @Override
            protected View createView() {
                return ViewFactory.createLoginView();
            }
        };

        mainViewLoader = new AsyncRootViewLoader(this) {
            @Override
            protected View createView() {
                return ViewFactory.createMainView();
            }

            @Override
            protected void viewLoaded(View view) {
                ClientLoggingUtil.info("MainView loaded");
                Scheduler.get().scheduleDeferred(new Scheduler.ScheduledCommand() {
                    public void execute() {
                        NavManager.performEntryPointNav();
                    }
                });
            }
        };
    }

    public void setRootView(Widget widget) {
        clearMainView();
        RootLayoutPanel.get().add(widget);
    }

    private void clearMainView() {
        if (RootLayoutPanel.get().getWidgetCount() > 0) {
            RootLayoutPanel.get().remove(0);
        }
    }

    private NavAdapter createNavAdapter() {
        ParreNavAdapter parreNavAdapter = ParreNavAdapter.getInstance();
        parreNavAdapter.setEntryPointHash(Window.Location.getHash());
        return parreNavAdapter;
    }

    /**
     * This is the entry point method.
     */
    public void onModuleLoad() {
        ClientLoggingUtil.info("Loading Parre!");
        GWT.setUncaughtExceptionHandler(new GWT.UncaughtExceptionHandler() {
            public void onUncaughtException(Throwable e) {
            	Throwable unwrapped = unwrap(e);  
            	String stackTraceString = "";
            	for (StackTraceElement ste : unwrapped.getStackTrace()) {
            		stackTraceString += "> " + ste.getClass().getName() + ":" + ste.getMethodName() + ":" + ste.getLineNumber();
            	}
                //MessageDialog.popupError("Unexpected Error, please consult server logs :" + unwrapped.getClass().getName() + "; Message: " + unwrapped.getMessage() + "; Stacktrace: " + stackTraceString, 500, true);
            	MessageDialog.popupError("Oops! An unexpected error has occured. Please refresh your browser and report the issue.", 350, true);
                ClientLoggingUtil.error("Unexpected Error : ", unwrapped);
            }
        });
        NavAdapter navAdapter = createNavAdapter();
        NavManager.start(navAdapter, ParreNavEventMapper.getInstance());
        initViewLoaders();

        ClientSingleton.getEventBus().addHandler(DisplayLoginEvent.TYPE, new DisplayLoginEvent.Handler() {
            public void onEvent(DisplayLoginEvent event) {
                loadLogin();
            }
        });
        ClientSingleton.getEventBus().addHandler(LoginEvent.TYPE, new LoginEvent.Handler() {
            public void onEvent(LoginEvent event) {
                loginPerformed(event.getLoginInfo());
            }
        });
        ClientSingleton.getEventBus().addHandler(LogoutEvent.TYPE, new LogoutEvent.Handler() {
            public void onEvent(LogoutEvent event) {
                doLogout(event.getLoginInfo());
            }
        });
        checkLogin();
    }

    public Throwable unwrap(Throwable e) {   
        if(e instanceof UmbrellaException) {   
          UmbrellaException ue = (UmbrellaException) e;  
          if(ue.getCauses().size() == 1) {   
            return unwrap(ue.getCauses().iterator().next());  
          }  
        }  
        return e;  
      }  
        
    private void checkLogin() {
        AsyncCallback<LoginInfo> callback = new AsyncCallback<LoginInfo>() {
            public void onFailure(Throwable throwable) {
            }

            public void onSuccess(LoginInfo loginInfo) {
                ClientSingleton.setLoginInfo(loginInfo);
                if (loginInfo.isLoggedIn()) {
                    loginPerformed(loginInfo);
                } else {
                    loadLogin();
                }
            }
        };
        //check here if we have an existing login session
        ServiceFactory.getInstance().getLoginService().login(callback);
    }

    private void loadLogin() {
        ClientLoggingUtil.info("Loading Login ...");
        loginViewLoader.showView();
    }

    private void loadParre() {

        AsyncCallback<AppInitData> callback = new ErrorHandlingAsyncCallback<AppInitData>() {
            public void onSuccess(AppInitData appInitData) {
                ClientSingleton.setAppInitData(appInitData);
                mainViewLoader.showView();
            }
        };
        ServiceFactory.getInstance().getParreService().getAppInitData(callback);
    }


    public void loginPerformed(LoginInfo loginInfo) {
        ClientSingleton.setLoginInfo(loginInfo);
        if (loginInfo.isLoggedIn()) {
            loadParre();
        }
    }

    private void doLogout(LoginInfo loginInfo) {
        AsyncCallback<LoginInfo> asyncCallback = new ErrorHandlingAsyncCallback<LoginInfo>() {
            public void onSuccess(LoginInfo result) {
                loadLogin();
            }
        };
        ServiceFactory.getInstance().getLoginService().signOut(asyncCallback);
    }
}
