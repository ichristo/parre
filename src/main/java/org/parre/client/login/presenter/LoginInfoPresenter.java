/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.login.presenter;

import org.parre.client.ClientSingleton;
import org.parre.client.event.LoginEvent;
import org.parre.client.messaging.ServiceFactory;
import org.parre.client.util.*;
import org.parre.shared.LoginInfo;
import org.parre.shared.UserNamePasswordDTO;

import com.google.gwt.event.dom.client.*;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.web.bindery.event.shared.EventBus;

/**
 * The Class LoginInfoPresenter.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 7/20/11
 */
public class LoginInfoPresenter implements Presenter {
    private EventBus eventBus;
    private Display display;
    private BusyHandler busyHandler;

    /**
     * The Interface Display.
     */
    public interface Display extends ModelView<UserNamePasswordDTO> {
        HasClickHandlers getGsButton();

        HasKeyPressHandlers[] getKeyPressSources();

        void setLoginFailedMessage(boolean display);

        void setLoginEnabled(boolean value);

        HasClickHandlers getAdminLoginButton();
    }

    public LoginInfoPresenter(Display display) {
        this.eventBus = ClientSingleton.getEventBus();
        this.display = display;
        this.busyHandler = new BusyHandler(display);
    }


    public void go() {
        bind();
    }


    private void bind() {
        display.setModel(new UserNamePasswordDTO());
        display.getAdminLoginButton().addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                performLogin("admin", "admin");
            }
        });
        display.getGsButton().addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                ClientLoggingUtil.info("Login Button Clicked");
                handleLoginRequest();
            }
        });
        KeyPressHandler handler = createLoginHandler();
        HasKeyPressHandlers[] keyPressSources = display.getKeyPressSources();
        for (HasKeyPressHandlers keyPressSource : keyPressSources) {
            keyPressSource.addKeyPressHandler(handler);
        }
    }

    private KeyPressHandler createLoginHandler() {
        return new KeyPressHandler() {
            public void onKeyPress(KeyPressEvent event) {
                if (event.getCharCode() == KeyCodes.KEY_ENTER) {
                    handleLoginRequest();
                }
            }
        };
    }

    private void handleLoginRequest() {
        if (display.validate()) {
            performLogin();
        }
    }

    private void performLogin() {
        ClientLoggingUtil.info("Performing Login :");
        UserNamePasswordDTO passwordDTO = display.updateAndGetModel();
        performLogin(passwordDTO.getUserName(), passwordDTO.getPassword());
    }

    private void performLogin(final String userName, final String password) {
        display.setLoginEnabled(false);
        new ExecutableAsyncCallback<LoginInfo>(busyHandler, new BaseAsyncCommand<LoginInfo>() {
            public void execute(AsyncCallback<LoginInfo> callback) {
                ServiceFactory.getInstance().getLoginService().login(userName, password, callback);
            }
            @Override
            public void handleResult(LoginInfo loginInfo) {
                display.setModelAndUpdateView(new UserNamePasswordDTO());
                display.setLoginEnabled(true);
                ClientLoggingUtil.info("Login Success : " + loginInfo.isLoggedIn());
                display.setLoginFailedMessage(!loginInfo.isLoggedIn());
                if (loginInfo.isLoggedIn()) {
                    eventBus.fireEvent(LoginEvent.create(loginInfo));
                };
            }
        }).makeCall();

    }
}
