/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.login.view;

import java.util.HashMap;
import java.util.Map;

import org.parre.client.ClientSingleton;
import org.parre.client.login.presenter.LoginInfoPresenter;
import org.parre.client.resources.GlobalDisplayMessagesFactory;
import org.parre.client.util.BaseModelView;
import org.parre.client.util.ToolTipManager;
import org.parre.client.util.ViewUtil;
import org.parre.shared.UserNamePasswordDTO;

import com.google.gwt.event.dom.client.FocusEvent;
import com.google.gwt.event.dom.client.FocusHandler;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.event.dom.client.HasKeyPressHandlers;
import com.google.gwt.event.dom.client.MouseOutEvent;
import com.google.gwt.event.dom.client.MouseOutHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.PasswordTextBox;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;

/**
 * .
 * User: Swarn S. Dhaliwal
 * Date: 9/10/11
 * Time: 4:38 AM
 */
public class LoginInfoView extends BaseModelView<UserNamePasswordDTO> implements LoginInfoPresenter.Display {
    private HTML loginFailedMessage = new HTML(GlobalDisplayMessagesFactory.getGlobalDisplayMessages().loginFailedMessage());
    private HTML userNameMessage = new HTML(GlobalDisplayMessagesFactory.getGlobalDisplayMessages().userNameRequired());
    private HTML passwordMessage = new HTML(GlobalDisplayMessagesFactory.getGlobalDisplayMessages().passwordRequired());
    private FlexTable loginTable;
    private Button gsButton;
    private TextBox userName;
    private TextBox userNameDummy;
    private TextBox password;
    private TextBox passwordDummy;
    private Map<TextBox, TextBox> fieldDummyMap = new HashMap<TextBox, TextBox>();
    private Button adminSigninButton;
    private Widget viewWidget;

    public LoginInfoView() {
         viewWidget = buildView();
    }

    private Widget buildView() {
        int tabIndex = -1;
        userNameMessage.setStyleName("normalText");
        passwordMessage.setStyleName("normalText");
        int currentRow = -1;
        loginTable = new FlexTable();
        loginTable.setWidth("100%");
        userName = new TextBox();
        userNameDummy = new TextBox();
        userNameDummy.getElement().setTabIndex(++tabIndex);
        userName.getElement().setTabIndex(++tabIndex);
        userNameDummy.getElement().setAttribute("dummy", "true");
        userNameDummy.getElement().getStyle().setOpacity(0.5);
        userNameDummy.setText("User Name");
        userNameDummy.setTitle("User Name");
        if (!ClientSingleton.getLoginInfo().isDevMode()) {
        	loginTable.setWidget(++currentRow, 0, userNameDummy);
        }
        setAttribute("layoutX", "0", userName);
        setAttribute("layoutY", String.valueOf(currentRow), userName);
        setAttribute("layoutX", "0", userNameDummy);
        setAttribute("layoutY", String.valueOf(currentRow), userNameDummy);
        fieldDummyMap.put(userName, userNameDummy);
        fieldDummyMap.put(userNameDummy, userName);
        password = new PasswordTextBox();
        passwordDummy = new TextBox();
        password.getElement().setTabIndex(++tabIndex);
        passwordDummy.getElement().setTabIndex(++tabIndex);
        passwordDummy.getElement().setAttribute("dummy", "true");
        passwordDummy.getElement().getStyle().setOpacity(0.5);
        passwordDummy.setText("Password");
        passwordDummy.setTitle("Password");
        if (!ClientSingleton.getLoginInfo().isDevMode()) {
        	loginTable.setWidget(currentRow, 1, passwordDummy);
        }
        setAttribute("layoutX", "1", password);
        setAttribute("layoutY", String.valueOf(currentRow), password);
        setAttribute("layoutX", "1", passwordDummy);
        setAttribute("layoutY", String.valueOf(currentRow), passwordDummy);
        fieldDummyMap.put(password, passwordDummy);
        fieldDummyMap.put(passwordDummy, password);
        if (!ClientSingleton.getLoginInfo().isDevMode()) {
        	loginTable.setWidget(++currentRow, 0, ViewUtil.layoutCenter(userNameMessage, passwordMessage));
        	loginTable.getFlexCellFormatter().setColSpan(currentRow, 0, 2);
        }
        loginFailedMessage.setStyleName("normalText");
        loginTable.setWidget(++currentRow, 0, loginFailedMessage);
        loginTable.getFlexCellFormatter().setColSpan(currentRow, 0, 2);
        adminSigninButton = ViewUtil.createButton("Get Started");
        adminSigninButton.setStyleName("loginPageButton");
        ToolTipManager.addToolTip(adminSigninButton, GlobalDisplayMessagesFactory.getGlobalDisplayMessages().loginButtoToolTip());
        adminSigninButton.getElement().setTabIndex(++tabIndex);
        gsButton = ViewUtil.createButton("Login", "Login into PARRE system");
        gsButton.setStyleName("loginPageButton");
        gsButton.getElement().setTabIndex(++tabIndex);
        if (ClientSingleton.getLoginInfo().isDevMode()) {
            loginTable.setWidget(++currentRow, 0, ViewUtil.layoutCenter(adminSigninButton));
        }
        else{
	        loginTable.setWidget(++currentRow, 0, ViewUtil.layoutCenter(gsButton));
	        loginTable.getFlexCellFormatter().setColSpan(currentRow, 0, 2);
        }
        FocusHandler focusHandler = createFocusHandler();
        MouseOutHandler mouseOutHandler = createMouseOutHandler();
        userName.addFocusHandler(focusHandler);
        userName.addMouseOutHandler(mouseOutHandler);
        userNameDummy.addFocusHandler(focusHandler);
        userNameDummy.addMouseOutHandler(mouseOutHandler);
        password.addFocusHandler(focusHandler);
        password.addMouseOutHandler(mouseOutHandler);
        passwordDummy.addFocusHandler(focusHandler);
        passwordDummy.addMouseOutHandler(mouseOutHandler);
        return ViewUtil.layoutCenter(loginTable);
    }

    private void setField(TextBox textBox) {
        loginTable.setWidget(getAttributeAsInt("layoutY", textBox), getAttributeAsInt("layoutX", textBox), textBox);
    }

    private int getAttributeAsInt(String attName, TextBox textBox) {
        return Integer.parseInt(textBox.getElement().getAttribute(attName));
    }

    private void setAttribute(String attName, String attValue, TextBox textBox) {
        textBox.getElement().setAttribute(attName, attValue);
    }

    private FocusHandler createFocusHandler() {
        return new FocusHandler() {
            public void onFocus(FocusEvent event) {
                TextBox field = (TextBox) event.getSource();
                String dummy = field.getElement().getAttribute("dummy");
                if ("true".equals(dummy)) {
                    TextBox textBox = fieldDummyMap.get(field);
                    setField(textBox);
                    textBox.setFocus(true);
                }
            }
        };
    }

    private MouseOutHandler createMouseOutHandler() {
        return new MouseOutHandler() {
            public void onMouseOut(MouseOutEvent event) {
                TextBox field = (TextBox) event.getSource();
                String dummy = field.getElement().getAttribute("dummy");
                if (!"true".equals(dummy)) {
                    if (field.getText().length() == 0) {
                        setField(fieldDummyMap.get(field));
                    }
                }
            }
        };
    }

    public HasClickHandlers getGsButton() {
        return gsButton;
    }

    public HasClickHandlers getAdminLoginButton() {
        return adminSigninButton;
    }

    public HasKeyPressHandlers[] getKeyPressSources() {
        return new HasKeyPressHandlers[] {userName, password};
    }

    public void setLoginFailedMessage(boolean display) {
        loginFailedMessage.setStyleName(display ? "errorText" : "normalText");
    }

    public void setLoginEnabled(boolean value) {
        userName.setEnabled(value);
        password.setEnabled(value);
        gsButton.getElement().getStyle().setOpacity(value ? 1.0 : 0.1);
    }

    public boolean validate() {
        boolean userNameValid = userName.getText().length() > 0;
        boolean passwordValid = password.getText().length() > 0;
        userNameMessage.setStyleName(userNameValid ? "normalText" : "errorText");
        passwordMessage.setStyleName(passwordValid ? "normalText" : "errorText");
        return userNameValid && passwordValid;
    }

    public void updateModel() {
        getModel().setUserName(userName.getText().trim());
        getModel().setPassword(password.getText().trim());
    }

    public void updateView() {
        userName.setText(getModel().getUserName());
        password.setText(getModel().getPassword());
    }

    public Widget asWidget() {
        return viewWidget;
    }
}
