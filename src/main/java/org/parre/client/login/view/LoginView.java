/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.login.view;

import org.parre.client.login.presenter.LoginPresenter;
import org.parre.client.util.Presenter;
import org.parre.client.util.ViewUtil;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.*;


/**
 * User: Swarn S. Dhaliwal
 * Date: 7/1/11
 * Time: 10:53 AM
*/
public class LoginView extends Composite implements LoginPresenter.Display {

    private Presenter presenter;

    public void setPresenter(Presenter presenter) {
        this.presenter = presenter;
    }

    public Presenter getPresenter() {
        return presenter;
    }

    public void refreshView() {

    }

    public void setEnabled(boolean value) {

    }


    /**
     * The Interface LoginViewUiBinder.
     */
    interface LoginViewUiBinder extends UiBinder<Widget, LoginView> {

    }
    private static LoginViewUiBinder ourUiBinder = GWT.create(LoginViewUiBinder.class);


    @UiField
    DockLayoutPanel mainPanel;
    @UiField
    HTMLPanel headerPanel;

    Widget loginInfoView;
    @UiField
    VerticalPanel centerPanel;


    public LoginView(LoginInfoView loginInfoView) {
        this.loginInfoView = loginInfoView.asWidget();
        initWidget(ourUiBinder.createAndBindUi(this));
        
        centerPanel.add(new HTML("<br /><br />"));
        
        HorizontalPanel logoContainer = ViewUtil.layoutCenter(ViewUtil.createImage("parre-logo.png"));
        logoContainer.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_CENTER);
        centerPanel.add(logoContainer);

        Widget headerText = ViewUtil.createHeader("Program to Assist Risk & Resilience Examination");
        headerText.setStyleName("loginPageHeader");
        centerPanel.add(ViewUtil.layoutCenter(headerText));

        Widget headerText2 = ViewUtil.createHeader("A RAMCAP Assistance Tool");
        headerText2.setStyleName("loginPageHeader");
        centerPanel.add(ViewUtil.layoutCenter(headerText2));

        centerPanel.add(new HTML("<hr style='width:100%' />"));
        
        centerPanel.add(loginInfoView.asWidget());
        
        //centerPanel.add(new HTML("<hr style='width:100%' />"));
        
    }

}