/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.messaging;

import com.google.gwt.core.client.GWT;

/**
 * A factory for creating Service objects.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 7/16/11
 */
public class ServiceFactory {
    private static ServiceFactory instance = new ServiceFactory();
    private LoginServiceAsync loginService;
    private AssetServiceAsync assetService;
    private ThreatServiceAsync threatService;
    private ConsequenceServiceAsync consequenceService;
    private ReportsServiceAsync reportsService;

    private ServiceFactory() {

    }

    public static ServiceFactory getInstance() {
        return instance;
    }

    public ParreServiceAsync getParreService() {
        return ParreService.App.getInstance();
    }

    public LoginServiceAsync getLoginService() {
        if (loginService == null) {
            loginService = GWT.create(LoginService.class);
        }
        return loginService;
    }

    public AssetServiceAsync getAssetService() {
        if (assetService == null) {
            assetService = GWT.create(AssetService.class);
        }
        return assetService;
    }

    public ThreatServiceAsync getThreatService() {
        if (threatService == null) {
            threatService = GWT.create(ThreatService.class);
        }
        return threatService;
    }
    
    public ConsequenceServiceAsync getConsequenceService() {
    	if (consequenceService == null) {
    		consequenceService = GWT.create(ConsequenceService.class);
    	}
    	return consequenceService;
    }
    
    public ReportsServiceAsync getReportsService() {
        if (reportsService == null) {
        	reportsService = GWT.create(ReportsService.class);
        }
        return reportsService;
    }

    
    public VulnerabilityServiceAsync getVulnerabilityService() {
        return VulnerabilityService.App.getInstance();
    }

    public LotServiceAsync getLotService() {
        return LotService.App.getInstance();
    }

    public NaturalThreatServiceAsync getNaturalThreatService() {
        return NaturalThreatService.App.getInstance();
    }

    public DpServiceAsync getDpService() {
        return DpService.App.getInstance();
    }

    public RiskResilienceServiceAsync getRiskResilienceService() {
        return RiskResilienceService.App.getInstance();
    }

    public RiskBenefitServiceAsync getRrmService() {
        return RiskBenefitService.App.getInstance();
    }
    
     
}
