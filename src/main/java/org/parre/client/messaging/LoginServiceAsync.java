/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.messaging;

import org.parre.shared.AppInitData;
import org.parre.shared.LoginInfo;

import com.google.gwt.user.client.rpc.AsyncCallback;

/**
 * User: Swarn S. Dhaliwal
 * Date: 7/1/11
 * Time: 10:23 AM
*/
public interface LoginServiceAsync {

    void login(String userName, String password, AsyncCallback<LoginInfo> callback);

    void login(AsyncCallback<LoginInfo> callback);

    void signOut(AsyncCallback<LoginInfo> asyncCallback);


}
