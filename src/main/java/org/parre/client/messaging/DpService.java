/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.messaging;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.google.gwt.core.client.GWT;

import java.util.List;

import org.parre.shared.DpAnalysisDTO;
import org.parre.shared.DpDTO;
import org.parre.shared.ThreatDTO;

/*
	Date			Author				Changes
    12/7/11	Swarn S. Dhaliwal	Created
*/
/**
 * The Interface DpService.
 */
@RemoteServiceRelativePath("dp")
public interface DpService extends RemoteService {
    List<DpDTO> getSearchResults(ThreatDTO dto, Long parreAnalysisId);

    DpDTO save(DpDTO dpDTO);

    DpAnalysisDTO startAnalysis(DpAnalysisDTO dto);

    DpAnalysisDTO getCurrentAnalysis(Long parreAnalysisId);

    DpDTO addSubCategory(DpDTO subCat);

    void removeCategory(Long dpId);

    boolean hasAnalysis(Long parreAnalysisId);

    /**
     * Utility/Convenience class.
     * Use DpService.App.getInstance() to access static instance of DpServiceAsync
     */
    public static class App {
        private static final DpServiceAsync ourInstance = (DpServiceAsync) GWT.create(DpService.class);

        public static DpServiceAsync getInstance() {
            return ourInstance;
        }
    }
}
