/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.messaging;

import java.util.List;

import com.google.gwt.user.client.rpc.AsyncCallback;
import org.parre.shared.ParreAnalysisReportSectionDTO;
import org.parre.shared.ReportAssessmentTaskDTO;
import org.parre.shared.ReportInvestmentOptionDTO;
import org.parre.shared.RiskResilienceDTO;
import org.parre.shared.RptAssessmentDTO;
import org.parre.shared.RptAssetDTO;
import org.parre.shared.RptConsequenceDTO;
import org.parre.shared.RptMainDTO;
import org.parre.shared.RptRankingsDTO;
import org.parre.shared.RptThreatLikelihoodDTO;
import org.parre.shared.RptVulnerabilityDTO;


/**
 * The async counterpart of <code>WarrantyServService</code>.
 */
public interface ReportsServiceAsync {

    void getReportData(Long parreAnalysisID, AsyncCallback<List<RptMainDTO>> callback);	
	void getRptRiskResilienceDTOs(Long parreAnalysisId, AsyncCallback<List<RiskResilienceDTO>> callback);
	void getRptTop20RisksDTOs(Long parreAnalysisId, AsyncCallback<List<RiskResilienceDTO>> callback);
	void getRptTop10NatThreatDTOs(Long parreAnalysisId, AsyncCallback<List<RiskResilienceDTO>> callback);
	void getReportSection(Long parreAnalysisId, Long reportSectionLookupId, AsyncCallback<ParreAnalysisReportSectionDTO> callback);
	void saveReportSection(Long parreAnalysisId, ParreAnalysisReportSectionDTO dto, AsyncCallback<ParreAnalysisReportSectionDTO> callback);
	void getReportInputData(Long parreAnalysisId, AsyncCallback<RptMainDTO> callback);
	void getReportDisplayTableAssets(Long parreAnalysisId, AsyncCallback<List<RptAssetDTO>> callback);
	void getReportDisplayTableConsequences(Long parreAnalysisId, AsyncCallback<List<RptConsequenceDTO>> callback);
	void getReportDisplayTableVulnerability(Long parreAnalysisId, AsyncCallback<List<RptVulnerabilityDTO>> callback);
	void getReportDisplayTableThreatLikelihood(Long parreAnalysisId, AsyncCallback<List<RptThreatLikelihoodDTO>> callback);
	void getReportDisplayTableRiskManagement(Long parreAnalysisId, AsyncCallback<List<RptRankingsDTO>> callback);
	void getReportDisplayTableRiskReduction(Long parreAnalysisId, AsyncCallback<List<RptAssessmentDTO>> callback);
	void getReportAssessmentTasks(Long parreAnalysisId, AsyncCallback<List<ReportAssessmentTaskDTO>> callback);
	void getReportInvestmentOptions(Long parreAnalysisId, AsyncCallback<List<ReportInvestmentOptionDTO>> callback);
	
	void saveAddTaskPopup(Long parreAnalysisId, ReportAssessmentTaskDTO taskDTO, AsyncCallback<List<ReportAssessmentTaskDTO>> callback);
	void saveEditOptionPopup(Long parreAnalysisId, ReportInvestmentOptionDTO optionDTO, AsyncCallback<List<ReportInvestmentOptionDTO>> callback);
}
