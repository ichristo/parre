/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.messaging;


import com.google.gwt.user.client.rpc.AsyncCallback;

import java.util.List;

import org.parre.shared.*;

/*
    Date			Author				Changes
    12/1/11	Swarn S. Dhaliwal	Created
*/
/**
 * The Interface LotServiceAsync.
 */
public interface LotServiceAsync {
    void save(LotDTO vulnerabilityDTO, AsyncCallback<LotDTO> callback);

    void getAssetSearchResults(AssetSearchDTO dto, Long parreAnalysisId, AsyncCallback<List<LotDTO>> callback);

    void getThreatSearchResults(ThreatDTO dto, Long parreAnalysisId, AsyncCallback<List<LotDTO>> callback);

    void getAnalysisTypes(AsyncCallback<List<LabelValueDTO>> asyncCallback);

    void getCurrentAnalysis(Long parreAnalysisId, AsyncCallback<LotAnalysisDTO> callback);

    void startAnalysis(LotAnalysisDTO dto, AsyncCallback<LotAnalysisDTO> callback);

    void saveWithProxyIndication(LotDTO lotDTO, AsyncCallback<LotDTO> callback);

    void getProxyCities(AsyncCallback<List<LabelValueDTO>> asyncCallback);

    void getProxyTargetTypes(AsyncCallback<List<LabelValueDTO>> asyncCallback);

    void getProxyIndication(Long lotId, AsyncCallback<ProxyIndicationDTO> callback);

    void saveProxyIndication(ProxyIndicationDTO proxyIndicationDTO, AsyncCallback<ProxyIndicationDTO> callback);

    void getExistingProxyIndications(Long parreAnalysisId, AsyncCallback<List<ProxyIndicationDTO>> callback);

    void associateProxyIndication(Long lotId, Long proxyIndicationId, boolean makeCopy, AsyncCallback<ProxyIndicationDTO> callback);
}
