/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.messaging;

import com.google.gwt.user.client.rpc.AsyncCallback;

import java.util.List;

import org.parre.shared.*;

/*
	Date			Author				Changes
    12/6/11	Swarn S. Dhaliwal	Created
*/
/**
 * The Interface NaturalThreatServiceAsync.
 */
public interface NaturalThreatServiceAsync {
    void save(NaturalThreatDTO naturalThreatDTO, AsyncCallback<NaturalThreatDTO> callback);

    void getSearchResults(ThreatDTO dto, Long parreAnalysisId, AsyncCallback<List<NaturalThreatDTO>> callback);

    void getCurrentAnalysis(Long parreAnalysisId, AsyncCallback<NaturalThreatAnalysisDTO> callback);

    void startAnalysis(NaturalThreatAnalysisDTO dto, AsyncCallback<NaturalThreatAnalysisDTO> callback);

    void getEarthquakeLookupData(String earthquakeText, AsyncCallback<EarthquakeLookupDataDTO> callback);
    
    void calculateHurricaneData(NaturalThreatCalculationDTO dto, AsyncCallback<NaturalThreatCalculationDTO> callback);
    
    void getHurricaneProfile(Long id, AsyncCallback<HurricaneProfileDTO> callback);
    
    void saveHurricaneProfile(HurricaneProfileDTO dto, AsyncCallback<HurricaneProfileDTO> callback);
    
    void getAnalysisHurricaneProfiles(Long id, AsyncCallback<ParreAnalysisDTO> asyncCallback);
    
    void calculateTornadoData(NaturalThreatCalculationDTO dto, AsyncCallback<NaturalThreatCalculationDTO> callback);
    
    void calculateFloodData(NaturalThreatCalculationDTO dto, AsyncCallback<NaturalThreatCalculationDTO> callback);
    
    void calculateEarthquakeData(NaturalThreatCalculationDTO dto, AsyncCallback<NaturalThreatCalculationDTO> callback);
    
    void calculateIceStormData(NaturalThreatCalculationDTO dto, AsyncCallback<NaturalThreatCalculationDTO> callback);
    
    void getPhysicalResourceForNT(Long naturalThreatId, AsyncCallback<PhysicalResourceDTO> callback);
    
    void saveEarthquakeProfile(EarthquakeProfileDTO dto, AsyncCallback<EarthquakeProfileDTO> callback);
    
    void getEarthquakeProfile(Long id, AsyncCallback<EarthquakeProfileDTO> callback);

	void getAnalysisEarthquakeProfiles(Long id, AsyncCallback<ParreAnalysisDTO> asyncCallback);

    void getHurricaneLookupData(AsyncCallback<List<HurricaneLookupDataDTO>> async);

    void getTornadoCountyData(String stateCodeOverride, AsyncCallback<List<String>> aSyncCallback);
    
    void calculateNaturalThreat(NaturalThreatDTO dto, AsyncCallback<NaturalThreatDTO> callback);
    
    void recalculateAllNaturalThreats(Long parreAnalysisId, AsyncCallback<Boolean> callback);
}
