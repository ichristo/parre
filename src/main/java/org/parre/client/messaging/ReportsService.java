/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.messaging;

import java.util.List;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import org.parre.shared.ParreAnalysisReportSectionDTO;
import org.parre.shared.ReportAssessmentTaskDTO;
import org.parre.shared.ReportInvestmentOptionDTO;
import org.parre.shared.RiskResilienceDTO;
import org.parre.shared.RptAssessmentDTO;
import org.parre.shared.RptAssetDTO;
import org.parre.shared.RptConsequenceDTO;
import org.parre.shared.RptMainDTO;
import org.parre.shared.RptRankingsDTO;
import org.parre.shared.RptThreatLikelihoodDTO;
import org.parre.shared.RptVulnerabilityDTO;
import org.parre.shared.exception.InvalidParreAnalysisIdException;

/**
 * The client side stub for the RPC service.
 */
@RemoteServiceRelativePath("reports")
public interface ReportsService extends RemoteService {
	
    List<RptMainDTO> getReportData(Long parreAnalysisID) throws InvalidParreAnalysisIdException;
	List<RiskResilienceDTO> getRptRiskResilienceDTOs(Long parreAnalysisId);
	List<RiskResilienceDTO> getRptTop20RisksDTOs(Long parreAnalysisId);
	List<RiskResilienceDTO> getRptTop10NatThreatDTOs(Long parreAnalysisId);
	
	ParreAnalysisReportSectionDTO getReportSection(Long parreAnalysisId, Long reportSectionLookupId);
	ParreAnalysisReportSectionDTO saveReportSection(Long parreAnalysisId, ParreAnalysisReportSectionDTO dto);
	
	RptMainDTO getReportInputData(Long parreAnalysisId);
	List<RptAssetDTO> getReportDisplayTableAssets(Long parreAnalysisId);
	List<RptConsequenceDTO> getReportDisplayTableConsequences(Long parreAnalysisId);
	List<RptVulnerabilityDTO> getReportDisplayTableVulnerability(Long parreAnalysisId);
	List<RptThreatLikelihoodDTO> getReportDisplayTableThreatLikelihood(Long parreAnalysisId);
	List<RptRankingsDTO> getReportDisplayTableRiskManagement(Long parreAnalysisId);
	List<RptAssessmentDTO> getReportDisplayTableRiskReduction(Long parreAnalysisId);
	List<ReportAssessmentTaskDTO> getReportAssessmentTasks(Long parreAnalysisId);
	List<ReportInvestmentOptionDTO> getReportInvestmentOptions(Long parreAnalysisId);
	
	List<ReportAssessmentTaskDTO> saveAddTaskPopup(Long parreAnalysisId, ReportAssessmentTaskDTO taskDTO);
	List<ReportInvestmentOptionDTO> saveEditOptionPopup(Long parreAnalysisId, ReportInvestmentOptionDTO optionDTO);
	
	
}
