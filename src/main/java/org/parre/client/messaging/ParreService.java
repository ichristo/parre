/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.messaging;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.google.gwt.core.client.GWT;

import java.util.List;
import java.util.Set;

import org.parre.shared.*;
import org.parre.shared.exception.DuplicateItemException;
import org.parre.shared.types.AssetThreatAnalysisType;

/*
	Date			Author				Changes
    11/29/11	Swarn S. Dhaliwal	Created
*/
/**
 * The Interface ParreService.
 */
@RemoteServiceRelativePath("parreService")
public interface ParreService extends RemoteService {
    List<LabelValueDTO> getContacts();

    List<LabelValueDTO> getProductServices();

    ParreAnalysisDTO saveParreAnalysis(ParreAnalysisDTO dto) throws DuplicateItemException;

    AppInitData getAppInitData();

    ParreAnalysisDTO getParreAnalysis(Long parreAnalysisId);

    void setCurrentParreAnalysis(Long parreAnalysisId);

    Integer updateAssetThreatData(Integer thresholdValue, Set<AssetThreatLevelDTO> levelDTOs, Long parreAnalysisId);

    void updateAssetThreatLevel(AssetThreatLevelDTO levelDTO, Long parreAnalysisId);

    Integer updateAssetThreshold(Integer threshold, Long parreAnalysisId);

    Integer getAssetThreatAnalysisCount(Long parreAnalysisId, AssetThreatAnalysisType analysisType);

    void lockCurrentAnalysis(Long parreAnalysisId);

    List<ProposedMeasureDTO> getProposedMeasures(Long parreAnalysisId);

    List<ProposedMeasureDTO> getBaselineProposedMeasures(Long baselineParreAnalysisId);

    void associateProposedMeasure(Long proposedMeasureId, Long parreAnalysisId);

    ProposedMeasureDTO createAndAssociateProposedMeasure(ProposedMeasureDTO dto);

    void disassociateProposedMeasure(Long pmId, Long parreAnalysisId);

    SiteLocationDTO saveSiteLocation(SiteLocationDTO dto);

    List<LabelValueDTO> getSiteLocationList();

    void unlockCurrentAnalysis(Long parreAnalysisId);

    ContactDTO saveContact(ContactDTO contactDTO);

    ProposedMeasureDTO saveProposedMeasure(ProposedMeasureDTO dto);

    ParreAnalysisDTO copyParreAnalysis(Long parreAnalysisId);

    ContactDTO getContact(Long contactId);

    SiteLocationDTO getSiteLocation(Long locationId);

    List<Long> deleteParreAnalysis(Long parreAnalysisId);


    /**
     * Utility/Convenience class.
     * Use ParreService.App.getInstance() to access static instance of ParreServiceAsync
     */
    public static class App {
        private static final ParreServiceAsync ourInstance = (ParreServiceAsync) GWT.create(ParreService.class);

        public static ParreServiceAsync getInstance() {
            return ourInstance;
        }
    }
}
