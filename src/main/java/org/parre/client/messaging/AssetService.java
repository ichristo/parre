/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.messaging;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

import java.util.List;

import org.parre.shared.*;
import org.parre.shared.exception.DuplicateAssetIdException;

/**
 * The client side stub for the RPC service.
 */
@RemoteServiceRelativePath("asset")
public interface AssetService extends RemoteService {

    List<AssetDTO> getAssetSearchResults(AssetSearchDTO searchDTO);

    List<LabelValueDTO> getStateList();

    AssetDTO saveAsset(AssetDTO assetDTO, Long parreAnalysisId) throws DuplicateAssetIdException;

    List<AssetThreatLevelsDTO> getAssetThreatLevels(Long parreAnalysisId);

    List<ThreatDTO> getActiveThreats(Long parreAnalysisId);

    AssetDTO deleteAsset(AssetDTO assetDTO, Long parreAnalysisId);

    ZipLookupDataDTO getZipLookupData(String zipCode);

    boolean hasAssets(Long parreAnalysisId);

    boolean hasActiveThreats(Long parreAnalysis);
    
    List<String> getSystemTypes(Long parreAnalysisId);
}
