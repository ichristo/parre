/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.messaging;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

import java.util.List;

import org.parre.shared.*;

/*
	Date			Author				Changes
    12/6/11	Swarn S. Dhaliwal	Created
*/
/**
 * The Interface NaturalThreatService.
 */
@RemoteServiceRelativePath("naturalThreat")
public interface NaturalThreatService extends RemoteService {
    NaturalThreatDTO save(NaturalThreatDTO naturalThreatDTO);

    List<NaturalThreatDTO> getSearchResults(ThreatDTO dto, Long parreAnalysisId);

    NaturalThreatAnalysisDTO getCurrentAnalysis(Long parreAnalysisId);

    NaturalThreatAnalysisDTO startAnalysis(NaturalThreatAnalysisDTO dto);

    EarthquakeLookupDataDTO getEarthquakeLookupData(String earthquakeText);

    NaturalThreatCalculationDTO calculateTornadoData(NaturalThreatCalculationDTO dto);
    
    NaturalThreatCalculationDTO calculateFloodData(NaturalThreatCalculationDTO dto);
    
    NaturalThreatCalculationDTO calculateEarthquakeData(NaturalThreatCalculationDTO dto);
    
    NaturalThreatCalculationDTO calculateHurricaneData(NaturalThreatCalculationDTO dto);
    
    NaturalThreatCalculationDTO calculateIceStormData(NaturalThreatCalculationDTO dto);
    
    HurricaneProfileDTO getHurricaneProfile(Long hurricaneProfileId);
    
    HurricaneProfileDTO saveHurricaneProfile(HurricaneProfileDTO dto);
    
    ParreAnalysisDTO getAnalysisHurricaneProfiles(Long id);
    
    List<HurricaneLookupDataDTO> getHurricaneLookupData();
    
    PhysicalResourceDTO getPhysicalResourceForNT(Long naturalThreatId);
    
    EarthquakeProfileDTO getEarthquakeProfile(Long earthquakeProfileId);
    
    EarthquakeProfileDTO saveEarthquakeProfile(EarthquakeProfileDTO dto);
    
    ParreAnalysisDTO getAnalysisEarthquakeProfiles(Long id);

    List<String> getTornadoCountyData(String stateCodeOverride);
    
    NaturalThreatDTO calculateNaturalThreat(NaturalThreatDTO dto);
    
    Boolean recalculateAllNaturalThreats(Long parreAnalysisId);

    /**
     * Utility/Convenience class.
     * Use NaturalThreatService.App.getInstance() to access static instance of NaturalThreatServiceAsync
     */
    public static class App {
        private static final NaturalThreatServiceAsync ourInstance = (NaturalThreatServiceAsync) GWT.create(NaturalThreatService.class);

        public static NaturalThreatServiceAsync getInstance() {
            return ourInstance;
        }
    }
}
