/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.messaging;

import com.google.gwt.user.client.rpc.AsyncCallback;

import java.util.List;

import org.parre.shared.*;
import org.parre.shared.nav.Nav;

/**
 * The Interface AssetThreatServiceAsync.
 */
public interface AssetThreatServiceAsync {
	void getUserNav(LoginInfo loginInfo, AsyncCallback<Nav> callback);
	
	void getAssetSearchResults(AssetSearchDTO searchDTO, AsyncCallback<List<AssetDTO>> callback) throws IllegalArgumentException;

	void getThreatSearchResults(ThreatSearchDTO searchDTO, AsyncCallback<List<ThreatDTO>> callback) throws IllegalArgumentException;
	
	void getStateList(AsyncCallback<List<LabelValueDTO>> callback);
	
	void saveThreat(ThreatDTO model, AsyncCallback<ThreatDTO> callback);

    void saveAssetThreat(ConsequenceDTO equipmentDTO, AsyncCallback<ConsequenceDTO> callback);

    void saveThreatAsset(ConsequenceDTO customerEquipmentDTO, AsyncCallback<ConsequenceDTO> callback);
    
    
    void updateAssetThreatFatalities(Integer fatalities, AsyncCallback<Integer> callback);
    
    void updateAssetThreatSeriousInjuries(Integer seriousInjuries, AsyncCallback<Integer> callback);
}
