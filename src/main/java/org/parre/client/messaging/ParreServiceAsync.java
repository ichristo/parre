/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.messaging;

import com.google.gwt.user.client.rpc.AsyncCallback;

import java.util.List;
import java.util.Set;

import org.parre.shared.*;
import org.parre.shared.exception.DuplicateItemException;
import org.parre.shared.types.AssetThreatAnalysisType;

/*
	Date			Author				Changes
    11/29/11	Swarn S. Dhaliwal	Created
*/
/**
 * The Interface ParreServiceAsync.
 */
public interface ParreServiceAsync {
    void getContacts(AsyncCallback<List<LabelValueDTO>> callback);

    void getProductServices(AsyncCallback<List<LabelValueDTO>> asyncCallback);

    void saveParreAnalysis(ParreAnalysisDTO dto, AsyncCallback<ParreAnalysisDTO> callback);

    void getAppInitData(AsyncCallback<AppInitData> callback);

    void getParreAnalysis(Long parreAnalysisId, AsyncCallback<ParreAnalysisDTO> callback);

    void setCurrentParreAnalysis(Long parreAnalysisId, AsyncCallback<Void> callback);

    void updateAssetThreatData(Integer thresholdValue, Set<AssetThreatLevelDTO> levelDTOs, Long parreAnalysisId, AsyncCallback<Integer> callback);

    void updateAssetThreatLevel(AssetThreatLevelDTO levelDTO, Long parreAnalysisId, AsyncCallback<Void> callback);

    void updateAssetThreshold(Integer threshold, Long parreAnalysisId, AsyncCallback<Integer> async);

    void getAssetThreatAnalysisCount(Long parreAnalysisId, AssetThreatAnalysisType analysisType, AsyncCallback<Integer> callback);

    void lockCurrentAnalysis(Long parreAnalysisId, AsyncCallback<Void> callback);

    void getProposedMeasures(Long parreAnalysisId, AsyncCallback<List<ProposedMeasureDTO>> callback);

    void getBaselineProposedMeasures(Long baselineParreAnalysisId, AsyncCallback<List<ProposedMeasureDTO>> callback);

    void associateProposedMeasure(Long proposedMeasureId, Long parreAnalysisId, AsyncCallback<Void> callback);

    void createAndAssociateProposedMeasure(ProposedMeasureDTO dto, AsyncCallback<ProposedMeasureDTO> asyncCallback);

    void disassociateProposedMeasure(Long pmId, Long parreAnalysisId, AsyncCallback<Void> callback);

    void saveSiteLocation(SiteLocationDTO dto, AsyncCallback<SiteLocationDTO> async);

    void getSiteLocationList(AsyncCallback<List<LabelValueDTO>> asyncCallback);

    void unlockCurrentAnalysis(Long parreAnalysisId, AsyncCallback<Void> callback);

    void saveContact(ContactDTO contactDTO, AsyncCallback<ContactDTO> callback);

    void saveProposedMeasure(ProposedMeasureDTO dto, AsyncCallback<ProposedMeasureDTO> asyncCallback);

    void copyParreAnalysis(Long parreAnalysisId, AsyncCallback<ParreAnalysisDTO> callback);

    void getContact(Long contactId, AsyncCallback<ContactDTO> async);

    void getSiteLocation(Long locationId, AsyncCallback<SiteLocationDTO> async);

    void deleteParreAnalysis(Long parreAnalysisId, AsyncCallback<List<Long>> callback);
}
