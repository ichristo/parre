/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.nt.view;

import java.util.List;

import org.parre.client.util.ViewUtil;
import org.parre.shared.IceStormIndexWorkDTO;
import org.parre.shared.LabelValueDTO;
import org.parre.shared.NaturalThreatCalculationDTO;
import org.parre.shared.NaturalThreatDTO;
import org.parre.shared.PhysicalResourceDTO;

import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextBox;

/**
 * The Class EditIceStormThreatView.
 */
public class EditIceStormThreatView extends EditNaturalThreatView {
	private TextBox latitude;
	private TextBox longitude;
	private ListBox selectState;
	private TextBox daysOfBackupPower;
	private TextBox dailyOperationalLoss;
	
	private TextBox index3RecoveryTime;
	private TextBox index3Fatalities;
	private TextBox index3SeriousInjuries;
	
	private TextBox index4RecoveryTime;
	private TextBox index4Fatalities;
	private TextBox index4SeriousInjuries;
	
	private TextBox index5RecoveryTime;
	private TextBox index5Fatalities;
	private TextBox index5SeriousInjuries;
	
	private FlexTable indexDataTable;

    private CheckBox overrideLocation;
	
	@Override
	protected void addCalculationWidgets(FlexTable inputTable) {
		//super.addCalculationWidgets(inputTable);
		String inputBoxWidth = "120px";
		
		int currentRow = inputTable.getRowCount() - 1;
		
		latitude = ViewUtil.addTextBoxFormEntry(inputTable, ++currentRow, "Latitude", requiredCalculationFieldHandlers);
        latitude.setEnabled(false);
		longitude = ViewUtil.addTextBoxFormEntry(inputTable, ++currentRow, "Longitude", requiredCalculationFieldHandlers);
        longitude.setEnabled(false);

        selectState = ViewUtil.addListBoxFormEntry(inputTable, ++currentRow, "State", requiredCalculationFieldHandlers);
        selectState.setEnabled(false);

        overrideLocation = new CheckBox("Override location");
        ViewUtil.addFormEntry(inputTable, ++currentRow, "", overrideLocation);

        overrideLocation.addValueChangeHandler(new ValueChangeHandler<Boolean>() {
			@Override
			public void onValueChange(ValueChangeEvent<Boolean> event) {
				toggleOverrideStateValue();
			}
		});
		
		daysOfBackupPower = ViewUtil.addTextBoxFormEntry(inputTable, ++currentRow, "Days of Backup Power", requiredCalculationFieldHandlers);
		daysOfBackupPower.setWidth(inputBoxWidth);
		dailyOperationalLoss = ViewUtil.addTextBoxFormEntry(inputTable, ++currentRow, "Daily Operational Loss ($)", requiredCalculationFieldHandlers);
		dailyOperationalLoss.setWidth("175px");
		
		indexDataTable = new FlexTable();
		
		ViewUtil.addHeader(indexDataTable, 0, "Recovery Time", 1);
		ViewUtil.addHeader(indexDataTable, 0, "Loss of Life", 2);
		ViewUtil.addHeader(indexDataTable, 0, "Serious Injury", 3);
		
		index3RecoveryTime = ViewUtil.addTextBoxFormEntry(indexDataTable, 1, "Category 3", requiredCalculationFieldHandlers);
		index3RecoveryTime.setWidth(inputBoxWidth);
		index4RecoveryTime = ViewUtil.addTextBoxFormEntry(indexDataTable, 2, "Category 4", requiredCalculationFieldHandlers);
		index4RecoveryTime.setWidth(inputBoxWidth);
		index5RecoveryTime = ViewUtil.addTextBoxFormEntry(indexDataTable, 3, "Category 5", requiredCalculationFieldHandlers);
		index5RecoveryTime.setWidth(inputBoxWidth);
		
		index3Fatalities = new TextBox();
		index3Fatalities.setWidth(inputBoxWidth);
		ViewUtil.addWidget(indexDataTable, 0,index3Fatalities, 2);
		ViewUtil.addRequiredField(index3Fatalities, requiredCalculationFieldHandlers);
		index4Fatalities = new TextBox();
		index4Fatalities.setWidth(inputBoxWidth);
		ViewUtil.addWidget(indexDataTable, 1,index4Fatalities, 2);
		ViewUtil.addRequiredField(index4Fatalities, requiredCalculationFieldHandlers);
		index5Fatalities = new TextBox();
		index5Fatalities.setWidth(inputBoxWidth);
		ViewUtil.addWidget(indexDataTable, 2,index5Fatalities, 2);
		ViewUtil.addRequiredField(index5Fatalities, requiredCalculationFieldHandlers);
		
		index3SeriousInjuries = new TextBox();
		index3SeriousInjuries.setWidth(inputBoxWidth);
		ViewUtil.addWidget(indexDataTable, 0,index3SeriousInjuries, 3);
		ViewUtil.addRequiredField(index3SeriousInjuries, requiredCalculationFieldHandlers);
		index4SeriousInjuries = new TextBox();
		index4SeriousInjuries.setWidth(inputBoxWidth);
		ViewUtil.addWidget(indexDataTable, 1,index4SeriousInjuries, 3);
		ViewUtil.addRequiredField(index4SeriousInjuries, requiredCalculationFieldHandlers);
		index5SeriousInjuries = new TextBox();
		index5SeriousInjuries.setWidth(inputBoxWidth);
		ViewUtil.addWidget(indexDataTable, 2,index5SeriousInjuries, 3);
		ViewUtil.addRequiredField(index5SeriousInjuries, requiredCalculationFieldHandlers);
		
		int dataTableRow = ++currentRow;
		inputTable.getFlexCellFormatter().setColSpan(dataTableRow, 0, 2);
		inputTable.setWidget(dataTableRow, 0, indexDataTable);
	}
	
	@Override
	public void updateModel() {
		super.updateModel();
		NaturalThreatDTO model = getModel();
		model.getIceStormWorkDTO().setLatitude(getBigDecimal(latitude));
		model.getIceStormWorkDTO().setLongitude(getBigDecimal(longitude));
		model.getIceStormWorkDTO().setStateCode(selectState.getValue(selectState.getSelectedIndex()));
        model.setUseDefaultValues(overrideLocation.getValue());
		model.getIceStormWorkDTO().setDaysOfBackupPower(getBigDecimal(daysOfBackupPower));
		model.getIceStormWorkDTO().setDailyOperationalLoss(getBigDecimal(dailyOperationalLoss));
		model.getIceStormWorkDTO().addIceStormIndex(3, new IceStormIndexWorkDTO(getBigDecimal(index3RecoveryTime), getQuantity(index3Fatalities), getQuantity(index3SeriousInjuries)));
		model.getIceStormWorkDTO().addIceStormIndex(4, new IceStormIndexWorkDTO(getBigDecimal(index4RecoveryTime), getQuantity(index4Fatalities), getQuantity(index4SeriousInjuries)));
		model.getIceStormWorkDTO().addIceStormIndex(5, new IceStormIndexWorkDTO(getBigDecimal(index5RecoveryTime), getQuantity(index5Fatalities), getQuantity(index5SeriousInjuries)));
	}

	@Override
	public void updateView() {
		super.updateView();
		NaturalThreatDTO model = getModel();
        if(model.getIceStormWorkDTO().getLatitude() == null) {
            ViewUtil.setBigDecimal(latitude, physicalResource.getLatitude());
        }
        else {
            ViewUtil.setBigDecimal(latitude, model.getIceStormWorkDTO().getLatitude());
        }
        if(model.getIceStormWorkDTO().getLongitude() == null) {
            ViewUtil.setBigDecimal(longitude, physicalResource.getLongitude());
        }
        else {
            ViewUtil.setBigDecimal(longitude, model.getIceStormWorkDTO().getLongitude());
        }
        if(model.getUseDefaultValues()) {
            ViewUtil.setSelectedValue(selectState, physicalResource.getStateCode());
        }
        else {
            ViewUtil.setSelectedValue(selectState, model.getIceStormWorkDTO().getStateCode());
        }

		ViewUtil.setBigDecimal(daysOfBackupPower, model.getIceStormWorkDTO().getDaysOfBackupPower());
        ViewUtil.setBigDecimal(dailyOperationalLoss, model.getIceStormWorkDTO().getDailyOperationalLoss());

        overrideLocation.setValue(!model.getUseDefaultValues());
        toggleManualEntry();
		
		IceStormIndexWorkDTO indexDTO = null;
		
		if (model.getIceStormWorkDTO().getIndexMap().containsKey(3)) {
			indexDTO = model.getIceStormWorkDTO().getIndexMap().get(3);
			ViewUtil.setBigDecimal(index3RecoveryTime, indexDTO.getRecoveryTime());
			ViewUtil.setQuantity(index3Fatalities, indexDTO.getFatalities());
			ViewUtil.setQuantity(index3SeriousInjuries, indexDTO.getSeriousInjuries());
		}
		
		if (model.getIceStormWorkDTO().getIndexMap().containsKey(4)) {
			indexDTO = model.getIceStormWorkDTO().getIndexMap().get(4);
			ViewUtil.setBigDecimal(index4RecoveryTime, indexDTO.getRecoveryTime());
			ViewUtil.setQuantity(index4Fatalities, indexDTO.getFatalities());
			ViewUtil.setQuantity(index4SeriousInjuries, indexDTO.getSeriousInjuries());
		}
		
		if (model.getIceStormWorkDTO().getIndexMap().containsKey(5)) {
			indexDTO = model.getIceStormWorkDTO().getIndexMap().get(5);
			ViewUtil.setBigDecimal(index5RecoveryTime, indexDTO.getRecoveryTime());
			ViewUtil.setQuantity(index5Fatalities, indexDTO.getFatalities());
			ViewUtil.setQuantity(index5SeriousInjuries, indexDTO.getSeriousInjuries());
		}
	}

	@Override
	public NaturalThreatCalculationDTO prepareCalculationDTO() {
		calculation = super.prepareCalculationDTO();
		
		calculation.getIceStormWorkDTO().setLatitude(getBigDecimal(latitude));
		calculation.getIceStormWorkDTO().setLongitude(getBigDecimal(longitude));
		calculation.getIceStormWorkDTO().setStateCode(selectState.getValue(selectState.getSelectedIndex()));
		calculation.getIceStormWorkDTO().setDaysOfBackupPower(getBigDecimal(daysOfBackupPower));
		calculation.getIceStormWorkDTO().setDailyOperationalLoss(getBigDecimal(dailyOperationalLoss));
		calculation.getIceStormWorkDTO().addIceStormIndex(3, new IceStormIndexWorkDTO(getBigDecimal(index3RecoveryTime), getQuantity(index3Fatalities), getQuantity(index3SeriousInjuries)));
		calculation.getIceStormWorkDTO().addIceStormIndex(4, new IceStormIndexWorkDTO(getBigDecimal(index4RecoveryTime), getQuantity(index4Fatalities), getQuantity(index4SeriousInjuries)));
		calculation.getIceStormWorkDTO().addIceStormIndex(5, new IceStormIndexWorkDTO(getBigDecimal(index5RecoveryTime), getQuantity(index5Fatalities), getQuantity(index5SeriousInjuries)));
		return calculation;
	}

	@Override
	protected boolean usesDamageFactor() {
		return false;
	}

	@Override
	protected boolean usesLossAndReplacement() {
		return false;
	}

    @Override
    public void setStateList(List<LabelValueDTO> stateList) {
        selectState.clear();
        selectState.addItem("Please Select", "-1");
        for (LabelValueDTO value : stateList) {
            selectState.addItem(value.getLabel(), value.getValue());
        }
        selectState.getElement().getFirstChildElement().setAttribute("disabled", "disabled");
    }

    @Override
    public TextBox getOperationalLoss() {
        return dailyOperationalLoss;
    }


    @Override
	protected void toggleManualEntry() {
 		super.toggleManualEntry();
 		
 		boolean isManualEntry = useManualEntry.getValue();
 		seriousInjuries.setEnabled(isManualEntry);
 		fatalities.setEnabled(isManualEntry);
 		daysOfBackupPower.setEnabled(!isManualEntry);
 		dailyOperationalLoss.setEnabled(!isManualEntry);
 		index3RecoveryTime.setEnabled(!isManualEntry);
 		index3Fatalities.setEnabled(!isManualEntry);
 		index3SeriousInjuries.setEnabled(!isManualEntry);
 		index4RecoveryTime.setEnabled(!isManualEntry);
 		index4Fatalities.setEnabled(!isManualEntry);
 		index4SeriousInjuries.setEnabled(!isManualEntry);
 		index5RecoveryTime.setEnabled(!isManualEntry);
 		index5Fatalities.setEnabled(!isManualEntry);
 		index5SeriousInjuries.setEnabled(!isManualEntry);

        overrideLocation.setEnabled(!isManualEntry);

        toggleOverrideStateValue();
	}

    private void toggleOverrideStateValue() {
        latitude.setEnabled(overrideLocation.getValue());
        longitude.setEnabled(overrideLocation.getValue());
        selectState.setEnabled(overrideLocation.getValue());
    }

    @Override
	public void updateCalculationResults(NaturalThreatCalculationDTO calculation) {
		super.updateCalculationResults(calculation);
		
		// Update the fatalities and serious injuries to be the sum of the index 3 through 5 values
		Integer fatalityTotal = calculation.getIceStormWorkDTO().getIndexFatalityCount(3) + 
								calculation.getIceStormWorkDTO().getIndexFatalityCount(4) +
								calculation.getIceStormWorkDTO().getIndexFatalityCount(5);
		ViewUtil.setQuantity(fatalities, fatalityTotal);
		
		Integer seriousInjuryTotal = calculation.getIceStormWorkDTO().getIndexSeriousInjuryCount(3) + 
				calculation.getIceStormWorkDTO().getIndexSeriousInjuryCount(4) +
				calculation.getIceStormWorkDTO().getIndexSeriousInjuryCount(5);
		ViewUtil.setQuantity(seriousInjuries, seriousInjuryTotal);
	}

    @Override
    public void setPhysicalResource(PhysicalResourceDTO dto) {
        super.setPhysicalResource(dto);

        if(longitude.getText().equals("") && physicalResource.getLongitude() != null){
            longitude.setText(physicalResource.getLongitude().toPlainString());
        }
        if(latitude.getText().equals("") && physicalResource.getLatitude() != null) {
            latitude.setText(physicalResource.getLatitude().toPlainString());
        }
        if(selectState.getSelectedIndex() == 0 && physicalResource.getStateCode() != null) {
            ViewUtil.setSelectedValue(selectState, physicalResource.getStateCode());
        }
    }

}
