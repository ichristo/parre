/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.nt.view;

import java.math.BigDecimal;
import java.util.List;

import org.parre.client.ClientSingleton;
import org.parre.client.util.ClientLoggingUtil;
import org.parre.client.util.ViewUtil;
import org.parre.shared.HurricaneProfileDTO;
import org.parre.shared.NaturalThreatCalculationDTO;
import org.parre.shared.NaturalThreatDTO;
import org.parre.shared.ProfileDTO;

import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextBox;

/**
 * The Class EditHurricaneThreatView.
 */
public class EditHurricaneThreatView extends EditNaturalThreatView {
	
	private Button addNewProfileButton;
	private Button editCurrentProfileButton;
	private ListBox selectProfile;
	
	private HurricaneProfileDTO currentProfile;
	private List<HurricaneProfileDTO> profileChoices;
	
	private TextBox designSpeed;
	
	@Override
	protected void addCalculationWidgets(FlexTable inputTable) {
		super.addCalculationWidgets(inputTable);
		
		designSpeed = ViewUtil.addTextBoxFormEntry(inputTable, inputTable.getRowCount(), "Design Speed");
		designSpeed.setWidth(calculatorWidgetWidth);
		
		selectProfile = new ListBox();
		selectProfile.addChangeHandler(new ChangeHandler() {
			@Override
			public void onChange(ChangeEvent event) {
				updateCurrentProfile();
			}
		});
		ViewUtil.addFormEntry(inputTable, inputTable.getRowCount(), "Hurricane Profile", selectProfile);
		
		addNewProfileButton = ViewUtil.createButton("Add New Profile");
		editCurrentProfileButton = ViewUtil.createButton("Edit Selected Profile");
		
		ViewUtil.addFormEntry(inputTable, inputTable.getRowCount(), "", ViewUtil.layoutCenter(addNewProfileButton, editCurrentProfileButton));
	}
	
	@Override
	public void updateProfileList() {
		super.updateProfileList();
		calculateButton.setEnabled(false);
		editCurrentProfileButton.setEnabled(false);
		
		profileChoices = ClientSingleton.getCurrentParreAnalysis().getHurricaneProfileDTOs();
		
		selectProfile.clear();
		
		int selectIndex = -1;
		if (profileChoices != null) {
			for (ProfileDTO dto : profileChoices) {
				selectProfile.addItem(dto.getName(), String.valueOf(dto.getId()));
				if ((currentProfile != null && dto.getId() == currentProfile.getId())
						|| ClientSingleton.getCurrentParreAnalysis().getDefaultHurricaneProfileDTO() != null &&
						dto.getId() == ClientSingleton.getCurrentParreAnalysis().getDefaultHurricaneProfileDTO().getId()) {
					selectIndex = selectProfile.getItemCount() - 1;
				}
				if (selectIndex >= 0) {
					selectProfile.setSelectedIndex(selectIndex);
				}
			}
			calculateButton.setEnabled(true);
			editCurrentProfileButton.setEnabled(true);
			updateCurrentProfile();
		}
	}
	
	private void updateCurrentProfile() {
		if (selectProfile.getItemCount() > 0) {
			String selectedText = selectProfile.getValue(selectProfile.getSelectedIndex());
			ClientLoggingUtil.debug("Select hurricane profile selected value: " + selectedText);
			for (HurricaneProfileDTO dto : profileChoices) {
				if (String.valueOf(dto.getId()).equals(selectedText)) {
					currentProfile = dto;
					break;
				}
			}
		} else {
			currentProfile = null;
		}
		if (!hasDesignSpeed() && currentProfile != null) {
			safeSetDesignSpeed(currentProfile.getDefaultDesignSpeed());
		}
	}
	
	@Override
	public void updateModel() {
		super.updateModel();
		NaturalThreatDTO model = getModel();
		model.setHurricaneProfileId(currentProfile != null ? currentProfile.getId() : null);
		model.setDesignSpeed(safeGetDesignSpeed());
	}
	
	@Override
	public void updateView() {
		super.updateView();
		NaturalThreatDTO model = getModel();
		if (model.getDesignSpeed() != null && model.getDesignSpeed().intValue() > 0) {
			ViewUtil.setBigDecimal(designSpeed, model.getDesignSpeed());
		}
		
		if (getModel().getHurricaneProfileId() != null && getModel().getHurricaneProfileId() != 0) {
    		selectHurricaneProfile(getModel().getHurricaneProfileId());
    	}
			
		editCurrentProfileButton.setEnabled(currentProfile != null);
		calculateButton.setEnabled(currentProfile != null);
	}
	
	@Override
	public NaturalThreatCalculationDTO prepareCalculationDTO() {
		calculation = super.prepareCalculationDTO();
		calculation.getAnalysis().setDesignSpeed(safeGetDesignSpeed());
		calculation.setHurricaneProfile(currentProfile);
		ClientLoggingUtil.debug("Sending this hurricane profile off for calculation: " + String.valueOf(currentProfile));
		return calculation;
	}
	
	@Override
	public HasClickHandlers getAddProfileButton() {
		return addNewProfileButton;
	}

	@Override
	public HasClickHandlers getEditProfileButton() {
		return editCurrentProfileButton;
	}

	@Override
	public Long getCurrentProfileId() {
		return currentProfile != null ? currentProfile.getId() : null;
	}

    @Override
    public TextBox getOperationalLoss() {
        return operationLoss;
    }

    @Override
    public void selectHurricaneProfile(Long hurricaneProfileId) {
        profileChoices = ClientSingleton.getCurrentParreAnalysis().getHurricaneProfileDTOs();
        if(profileChoices != null) {
            for(int i = 0; i<profileChoices.size(); i++) {
                if(selectProfile.getValue(i).equals(String.valueOf(hurricaneProfileId))) {
                    selectProfile.setSelectedIndex(i);
                    updateCurrentProfile();
                    break;
                }
            }
        }
    }

    @Override
	protected void toggleManualEntry() {
 		super.toggleManualEntry();
 		
 		boolean isManualEntry = useManualEntry.getValue();
 		operationLoss.setEnabled(!isManualEntry);
 		selectProfile.setEnabled(!isManualEntry);
 		addNewProfileButton.setEnabled(!isManualEntry);
 		editCurrentProfileButton.setEnabled(!isManualEntry);
 		designSpeed.setEnabled(!isManualEntry);
	}
	
	private void safeSetDesignSpeed(BigDecimal value) {
		if (value != null && value.compareTo(BigDecimal.ZERO) == 1) {
			ViewUtil.setBigDecimal(designSpeed, value);
		} else {
			ViewUtil.setBigDecimal(designSpeed, BigDecimal.ZERO);
		}
	}
	
	private BigDecimal safeGetDesignSpeed() {
		try {
			BigDecimal value = ViewUtil.getBigDecimal(designSpeed);
			if (value != null && value.compareTo(BigDecimal.ZERO) == 1) {
				return value;
			}
		} catch (Exception ex) {
			// gulp
		}
		return BigDecimal.ZERO;
	}
	
	private boolean hasDesignSpeed() {
		return safeGetDesignSpeed().compareTo(BigDecimal.ZERO) == 1; 
	}

}
