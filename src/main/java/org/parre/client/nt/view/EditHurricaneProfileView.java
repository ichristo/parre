/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.nt.view;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.parre.client.ClientSingleton;
import org.parre.client.nt.presenter.EditHurricaneProfilePresenter;
import org.parre.client.util.BaseModelView;
import org.parre.client.util.ClientLoggingUtil;
import org.parre.client.util.FieldValidationHandler;
import org.parre.client.util.ViewUtil;
import org.parre.shared.HurricaneLookupDataDTO;
import org.parre.shared.HurricaneProfileDTO;

import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

/**
 * The Class EditHurricaneProfileView.
 */
public class EditHurricaneProfileView extends BaseModelView<HurricaneProfileDTO> implements EditHurricaneProfilePresenter.Display {
	
	private Widget viewWidget;
	private Label header;
	
    private TextBox defaultDesignSpeed;
    private Button saveHurricaneData;
    private Button cancelButton;
    private TextBox profileName;
    private CheckBox makeDefault;
    private List<FieldValidationHandler> requiredFieldHandlers;
    
    private TextBox cat0ReturnPeriod;
    private TextBox cat1ReturnPeriod;
    private TextBox cat2ReturnPeriod;
    private TextBox cat3ReturnPeriod;
    private TextBox cat4ReturnPeriod;
    private TextBox cat5ReturnPeriod;
    
    public EditHurricaneProfileView() {
    	requiredFieldHandlers = new ArrayList<FieldValidationHandler>();
    	viewWidget = buildView();
    }
    
    private Widget buildView() {
    	header = new Label("Edit Hurricane Profile");
        header.setStyleName("inputPanelHeader");
        
        FlexTable inputTable = new FlexTable();
        inputTable.setCellSpacing(1);
    	
    	VerticalPanel viewPanel = new VerticalPanel();
        viewPanel.setWidth("100%");
        viewPanel.setSpacing(5);
        viewPanel.setStyleName("inputPanel");

        viewPanel.add(header);
        viewPanel.add(createHurricaneInputArea());
        
        profileName = ViewUtil.addTextBoxFormEntry(inputTable, inputTable.getRowCount(), "Profile Name", requiredFieldHandlers);
        makeDefault = new CheckBox("Make Default");
        ViewUtil.addFormEntry(inputTable, inputTable.getRowCount(), "", makeDefault);
        viewPanel.add(ViewUtil.layoutCenter(inputTable));
        
        cancelButton = ViewUtil.createButton("Cancel");
        saveHurricaneData = ViewUtil.createButton("Save Profile");
        viewPanel.add(ViewUtil.layoutCenter(saveHurricaneData, cancelButton));
        return viewPanel;
    }
    
    private Widget createHurricaneInputArea() {
        VerticalPanel panel = new VerticalPanel();
        panel.setStyleName("inputPanel");
        panel.setSpacing(5);

        FlexTable inputTable = new FlexTable();
        inputTable.setCellSpacing(1);
        
        defaultDesignSpeed = ViewUtil.addTextBoxFormEntry(inputTable, inputTable.getRowCount(), "Default Design Speed");
        ViewUtil.addLabelFormEntry(inputTable, inputTable.getRowCount(), "Return Period (Years):");
        cat0ReturnPeriod = ViewUtil.addTextBoxFormEntry(inputTable, inputTable.getRowCount(), "Tropical Storm");
        cat1ReturnPeriod = ViewUtil.addTextBoxFormEntry(inputTable, inputTable.getRowCount(), "Category 1");
        cat2ReturnPeriod = ViewUtil.addTextBoxFormEntry(inputTable, inputTable.getRowCount(), "Category 2");
        cat3ReturnPeriod = ViewUtil.addTextBoxFormEntry(inputTable, inputTable.getRowCount(), "Category 3");
        cat4ReturnPeriod = ViewUtil.addTextBoxFormEntry(inputTable, inputTable.getRowCount(), "Category 4");
        cat5ReturnPeriod = ViewUtil.addTextBoxFormEntry(inputTable, inputTable.getRowCount(), "Category 5");
        
        panel.add(inputTable);
        
        return panel;
    }
    
	@Override
    public void setHurricaneLookupResult(List<HurricaneLookupDataDTO> result) {
        // one day I hope to grow up and be a real method
		// TODO: will use the lookup values combined with the default design speed to indicate to the user what the minimum category will be necessary when the default is used
    }
	
	@Override
	public HasClickHandlers getSaveButton() {
		return saveHurricaneData;
	}

	@Override
	public HasClickHandlers getCancelButton() {
		return cancelButton;
	}

	@Override
	public void setModel(HurricaneProfileDTO model) {
		super.setModel(model);
		updateView();
	}

	@Override
	public void setModelAndUpdateView(HurricaneProfileDTO model) {
		super.setModel(model);
		updateView();
	}

	@Override
	public boolean validate() {
		boolean valid = true;
		
		valid = ViewUtil.validate(requiredFieldHandlers);
		ClientLoggingUtil.debug("Validation result after required fields: " + valid);
		// TODO: decide if any of these fields should be required, or non-zero etc.
		ClientLoggingUtil.debug("Return value of validation: " + valid);
		return valid;
	}

	@Override
	public void updateModel() {
		HurricaneProfileDTO profile = getModel();
		profile.setParreAnalysisId(ClientSingleton.getCurrentParreAnalysis().getId());
		profile.setName(profileName.getText());
		profile.setAnalysisDefault(makeDefault.getValue());
		profile.setDefaultDesignSpeed(ViewUtil.getBigDecimal(defaultDesignSpeed, BigDecimal.ZERO));
		profile.setCat0ReturnPeriod(ViewUtil.getBigDecimal(cat0ReturnPeriod, BigDecimal.ZERO));
		profile.setCat1ReturnPeriod(ViewUtil.getBigDecimal(cat1ReturnPeriod, BigDecimal.ZERO));
		profile.setCat2ReturnPeriod(ViewUtil.getBigDecimal(cat2ReturnPeriod, BigDecimal.ZERO));
		profile.setCat3ReturnPeriod(ViewUtil.getBigDecimal(cat3ReturnPeriod, BigDecimal.ZERO));
		profile.setCat4ReturnPeriod(ViewUtil.getBigDecimal(cat4ReturnPeriod, BigDecimal.ZERO));
		profile.setCat5ReturnPeriod(ViewUtil.getBigDecimal(cat5ReturnPeriod, BigDecimal.ZERO));
	}

	@Override
	public void updateView() {
		HurricaneProfileDTO profile = getModel();
		
		ViewUtil.setBigDecimal(defaultDesignSpeed, profile.getDefaultDesignSpeed());
		ViewUtil.setText(profileName, profile.getName());
		makeDefault.setValue(ClientSingleton.getCurrentParreAnalysis().isDefaultHurricaneProfile(profile));
	
		ViewUtil.setBigDecimal(cat0ReturnPeriod, profile.getCat0ReturnPeriod());
		ViewUtil.setBigDecimal(cat1ReturnPeriod, profile.getCat1ReturnPeriod());
		ViewUtil.setBigDecimal(cat2ReturnPeriod, profile.getCat2ReturnPeriod());
		ViewUtil.setBigDecimal(cat3ReturnPeriod, profile.getCat3ReturnPeriod());
		ViewUtil.setBigDecimal(cat4ReturnPeriod, profile.getCat4ReturnPeriod());
		ViewUtil.setBigDecimal(cat5ReturnPeriod, profile.getCat5ReturnPeriod());
	}

	@Override
	public Widget asWidget() {
		return viewWidget;
	}

}
