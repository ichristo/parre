/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.nt.view;

import java.util.List;

import org.parre.client.ClientSingleton;
import org.parre.client.util.ClientLoggingUtil;
import org.parre.client.util.ViewUtil;
import org.parre.shared.EarthquakeProfileDTO;
import org.parre.shared.NaturalThreatCalculationDTO;
import org.parre.shared.NaturalThreatDTO;
import org.parre.shared.PhysicalResourceDTO;
import org.parre.shared.ProfileDTO;

import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextBox;

/**
 * The Class EditEarthquakeThreatView.
 */
public class EditEarthquakeThreatView extends EditNaturalThreatView {
	
	private TextBox yearBuilt;
	private Button addNewProfileButton;
	private Button editCurrentProfileButton;
	private ListBox selectProfile;
	
	private EarthquakeProfileDTO currentProfile;
	private List<EarthquakeProfileDTO> profileChoices;
	
	@Override
	protected void addCalculationWidgets(FlexTable inputTable) {
		super.addCalculationWidgets(inputTable);
		yearBuilt = ViewUtil.addTextBoxFormEntry(inputTable, inputTable.getRowCount(), "Year Built");
		yearBuilt.setWidth(calculatorWidgetWidth);
		yearBuilt.setEnabled(false);
		
		selectProfile = new ListBox();
		selectProfile.addChangeHandler(new ChangeHandler() {
			@Override
			public void onChange(ChangeEvent event) {
				updateCurrentProfile();
			}
		});
		ViewUtil.addFormEntry(inputTable, inputTable.getRowCount(), "Earthquake Profile", selectProfile);
		
		addNewProfileButton = ViewUtil.createButton("Add New Profile");
		editCurrentProfileButton = ViewUtil.createButton("Edit Selected Profile");
		
		ViewUtil.addFormEntry(inputTable, inputTable.getRowCount(), "", ViewUtil.layoutCenter(addNewProfileButton, editCurrentProfileButton));
	}
	
	@Override
	public void updateProfileList() {
		super.updateProfileList();
		calculateButton.setEnabled(false);
		editCurrentProfileButton.setEnabled(false);
		
		profileChoices = ClientSingleton.getCurrentParreAnalysis().getEarthquakeProfileDTOs();
		
		selectProfile.clear();
		
		int selectIndex = -1;
		if (profileChoices != null) {
			for (ProfileDTO dto : profileChoices) {
				selectProfile.addItem(dto.getName(), String.valueOf(dto.getId()));
				if ((currentProfile != null && dto.getId() == currentProfile.getId())
						|| ClientSingleton.getCurrentParreAnalysis().getDefaultEarthquakeProfileDTO() != null &&
						dto.getId() == ClientSingleton.getCurrentParreAnalysis().getDefaultEarthquakeProfileDTO().getId()) {
					selectIndex = selectProfile.getItemCount() - 1;
				}
			}
			if (selectIndex >= 0) {
				selectProfile.setSelectedIndex(selectIndex);
			}
			calculateButton.setEnabled(true);
			editCurrentProfileButton.setEnabled(true);
			updateCurrentProfile();
		}
	}

    @Override
    public TextBox getOperationalLoss() {
        return operationLoss;
    }

    private void updateCurrentProfile() {
		if (selectProfile.getItemCount() > 0) {
			String selectedText = selectProfile.getValue(selectProfile.getSelectedIndex());
			ClientLoggingUtil.debug("Select profile selected value: " + selectedText);
			for (EarthquakeProfileDTO dto : profileChoices) {
				if (String.valueOf(dto.getId()).equals(selectedText)) {
					currentProfile = dto;
					break;
				}
			}
		} else {
			currentProfile = null;
		}

	}
	
	@Override
	public void updateModel() {
		super.updateModel();
		NaturalThreatDTO model = getModel();
		
		model.setEarthquakeProfileId(currentProfile != null ? currentProfile.getId() : null);
	}
	
	@Override
	public void updateView() {
		super.updateView();
		if (getModel().getEarthquakeProfileId() != null && getModel().getEarthquakeProfileId() != 0) {
			selectEarthquakeProfile(getModel().getEarthquakeProfileId());
		}
		editCurrentProfileButton.setEnabled(currentProfile != null);
		calculateButton.setEnabled(currentProfile != null);
	}
	
	@Override
	public NaturalThreatCalculationDTO prepareCalculationDTO() {
		calculation = super.prepareCalculationDTO();
		calculation.setEarthquakeProfile(currentProfile);
		ClientLoggingUtil.debug("Sending this profile off for calculation: " + String.valueOf(currentProfile));
		return calculation;
	}
	
	@Override
	protected void toggleManualEntry() {
 		super.toggleManualEntry();
 		
 		boolean isManualEntry = useManualEntry.getValue();
 		operationLoss.setEnabled(!isManualEntry);
 		selectProfile.setEnabled(!isManualEntry);
 		addNewProfileButton.setEnabled(!isManualEntry);
 		editCurrentProfileButton.setEnabled(!isManualEntry);
 		
	}
	
	@Override
	public void setPhysicalResource(PhysicalResourceDTO dto) {
		super.setPhysicalResource(dto);
		yearBuilt.setText(dto.getYearBuilt() != null ? String.valueOf(dto.getYearBuilt()) : "");
	}

	@Override
	public HasClickHandlers getAddProfileButton() {
		return addNewProfileButton;
	}

	@Override
	public HasClickHandlers getEditProfileButton() {
		return editCurrentProfileButton;
	}

	@Override
	public Long getCurrentProfileId() {
		return currentProfile != null ? currentProfile.getId() : null;
	}

    @Override
    public void selectEarthquakeProfile(Long earthquakeProfileId) {
        profileChoices = ClientSingleton.getCurrentParreAnalysis().getEarthquakeProfileDTOs();
        if(profileChoices != null) {
            for(int i = 0; i<profileChoices.size(); i++) {
                if(selectProfile.getValue(i).equals(String.valueOf(earthquakeProfileId))) {
                    selectProfile.setSelectedIndex(i);
                    updateCurrentProfile();
                    break;
                }
            }
        }
    }

	
}
