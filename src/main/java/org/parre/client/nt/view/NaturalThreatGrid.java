/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.nt.view;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import org.parre.client.util.DoubleClickableTextCell;
import org.parre.client.util.ViewUtil;
import org.parre.client.util.dataview.DataColumn;
import org.parre.client.util.dataview.DataGridHeaders;
import org.parre.client.util.dataview.DataGridView;
import org.parre.client.util.dataview.DataProvider;
import org.parre.shared.NaturalThreatDTO;

import com.google.gwt.user.cellview.client.Column;

/**
 * The Class NaturalThreatGrid.
 */
public class NaturalThreatGrid extends DataGridView<NaturalThreatDTO> {
    private boolean useStatValue;
	public NaturalThreatGrid(DataProvider<NaturalThreatDTO> dataProvider) {
		super(dataProvider);
		List<DataColumn<NaturalThreatDTO>> columnList = new ArrayList<DataColumn<NaturalThreatDTO>>();
		columnList.add(createAssetColumn());
		columnList.add(createThreatNameColumn());
        columnList.add(createFatalitiesColumn());
        columnList.add(createSeriousInjuriesColumn());
        columnList.add(createDurationColumn());
        columnList.add(createSeverityColumn());
        columnList.add(createOperationLossColumn());
        columnList.add(createFinancialImpactColumn());
        columnList.add(createFinancialTotalColumn());
        columnList.add(createEconomicImpactColumn());
        columnList.add(createVulnerabilityColumn());
        columnList.add(createLotColumn());
        columnList.add(createTotalRiskColumn());
        addColumns(columnList);
	}

    private DataColumn<NaturalThreatDTO> createEconomicImpactColumn() {
        Column<NaturalThreatDTO, String> column = new Column<NaturalThreatDTO, String>(
                createDoubleClickableTextCell()) {
            @Override
            public String getValue(NaturalThreatDTO object) {
                return ViewUtil.toDisplayValue(object.getEconomicImpactDTO().getQuantity());
            }
        };
        column.setSortable(true);

        Comparator<NaturalThreatDTO> comparator = new Comparator<NaturalThreatDTO>() {
            public int compare(NaturalThreatDTO o1, NaturalThreatDTO o2) {
                return o1.getAssetId().compareTo(o2.getAssetId());
            }
        };
        return new DataColumn<NaturalThreatDTO>(DataGridHeaders.economicImpact(), column, comparator, 10);
    }

    private DataColumn<NaturalThreatDTO> createAssetColumn() {
        Column<NaturalThreatDTO, String> column = new Column<NaturalThreatDTO, String>(
                createDoubleClickableTextCell()) {
            @Override
            public String getValue(NaturalThreatDTO object) {
                return object.getAssetId() + " - " + object.getAssetName();
            }
        };
        column.setSortable(true);

        Comparator<NaturalThreatDTO> comparator = new Comparator<NaturalThreatDTO>() {
            public int compare(NaturalThreatDTO o1, NaturalThreatDTO o2) {
                return o1.getAssetId().compareTo(o2.getAssetId());
            }
        };
        return new DataColumn<NaturalThreatDTO>("Asset", column, comparator, 20);
    }


    private DataColumn<NaturalThreatDTO> createThreatNameColumn() {
    	Column<NaturalThreatDTO, String> column = new Column<NaturalThreatDTO, String>(
				createDoubleClickableTextCell()) {
			@Override
			public String getValue(NaturalThreatDTO object) {
				return	object.getThreatName() + " - " + object.getThreatDescription();
			}
		};
		column.setSortable(true);

		Comparator<NaturalThreatDTO> comparator = new Comparator<NaturalThreatDTO>() {
			public int compare(NaturalThreatDTO o1, NaturalThreatDTO o2) {
				return o1.getThreatName().compareTo(o2.getThreatName());
			}
		};
		return new DataColumn<NaturalThreatDTO>("Threat", column, comparator, 20);
	}



	private DataColumn<NaturalThreatDTO> createFatalitiesColumn() {
		Column<NaturalThreatDTO, String> column = new Column<NaturalThreatDTO, String>(
				createDoubleClickableTextCell()) {
			@Override
			public String getValue(NaturalThreatDTO object) {
				return	object.getFatalities().toString();
			}
		};
		column.setSortable(true);

		Comparator<NaturalThreatDTO> comparator = new Comparator<NaturalThreatDTO>() {
			public int compare(NaturalThreatDTO o1, NaturalThreatDTO o2) {
				return o1.getFatalities().compareTo(o2.getFatalities());
			}
		};
		return new DataColumn<NaturalThreatDTO>("Fatalities", column, comparator, 10);
	}


    private DataColumn<NaturalThreatDTO> createSeriousInjuriesColumn() {
    	Column<NaturalThreatDTO, String> column = new Column<NaturalThreatDTO, String>(
    			createDoubleClickableTextCell()) {
    		@Override
    		public String getValue(NaturalThreatDTO object) {
    			return object.getSeriousInjuries().toString();
    		}
    	};
    	column.setSortable(true);

    	Comparator<NaturalThreatDTO> comparator = new Comparator<NaturalThreatDTO>() {
    		public int compare(NaturalThreatDTO o1, NaturalThreatDTO o2) {
    			return o1.getSeriousInjuries().compareTo(o2.getSeriousInjuries());
    		}
    	};
    	return new DataColumn<NaturalThreatDTO>(DataGridHeaders.seriousInjuries(), column, comparator, 10);
    }
    
    private DataColumn<NaturalThreatDTO> createDurationColumn() {
		Column<NaturalThreatDTO, String> column = new Column<NaturalThreatDTO, String>(
				createDoubleClickableTextCell()) {
			@Override
			public String getValue(NaturalThreatDTO object) {
                return ViewUtil.toDisplayValue(object.getDurationDays());
			}
		};
		column.setSortable(true);

		Comparator<NaturalThreatDTO> comparator = new Comparator<NaturalThreatDTO>() {
			public int compare(NaturalThreatDTO o1, NaturalThreatDTO o2) {
				return o1.getDurationDays().compareTo(o2.getDurationDays());
			}
		};
		return new DataColumn<NaturalThreatDTO>(DataGridHeaders.createHeader("Duration", "(Days)"), column, comparator, 10);
	}

	private DataColumn<NaturalThreatDTO> createSeverityColumn() {
		Column<NaturalThreatDTO, String> column = new Column<NaturalThreatDTO, String>(
				createDoubleClickableTextCell()) {
			@Override
			public String getValue(NaturalThreatDTO object) {
                return ViewUtil.toDisplayValue(object.getSeverityMgd());
			}
		};
		column.setSortable(true);

		Comparator<NaturalThreatDTO> comparator = new Comparator<NaturalThreatDTO>() {
			public int compare(NaturalThreatDTO o1, NaturalThreatDTO o2) {
				return o1.getSeverityMgd().compareTo(o2.getSeverityMgd());
			}
		};
		return new DataColumn<NaturalThreatDTO>(DataGridHeaders.createHeader("Severity", "(MGD)"), column, comparator, 10);
	}
	
	private DataColumn<NaturalThreatDTO> createOperationLossColumn() {
		Column<NaturalThreatDTO, String> column = new Column<NaturalThreatDTO, String>(
				createDoubleClickableTextCell()) {
			@Override
			public String getValue(NaturalThreatDTO object) {
                return ViewUtil.toDisplayValue(object.getOperationalLossDTO().getQuantity());
			}
		};
		column.setSortable(true);

		Comparator<NaturalThreatDTO> comparator = new Comparator<NaturalThreatDTO>() {
			public int compare(NaturalThreatDTO o1, NaturalThreatDTO o2) {
				return o1.getOperationalLossDTO().getQuantity().compareTo(o2.getOperationalLossDTO().getQuantity());
			}
		};
		return new DataColumn<NaturalThreatDTO>(DataGridHeaders.operationalLoss(), column, comparator, 10);
	}
    
    private DataColumn<NaturalThreatDTO> createFinancialImpactColumn() {
    	Column<NaturalThreatDTO, String> column = new Column<NaturalThreatDTO, String>(
    			createDoubleClickableTextCell()) {
    		@Override
    		public String getValue(NaturalThreatDTO object) {
    			return ViewUtil.toDisplayValue(object.getFinancialImpactDTO().getQuantity());
    		}
    	};
    	column.setSortable(true);

    	Comparator<NaturalThreatDTO> comparator = new Comparator<NaturalThreatDTO>() {
    		public int compare(NaturalThreatDTO o1, NaturalThreatDTO o2) {
    			return o1.getFinancialImpactDTO().getQuantity().compareTo(o2.getFinancialImpactDTO().getQuantity());
    		}
    	};
    	return new DataColumn<NaturalThreatDTO>(DataGridHeaders.financialImpact(), column, comparator, 10);
    }

    private DataColumn<NaturalThreatDTO> createLotColumn() {
        Column<NaturalThreatDTO, String> column = new Column<NaturalThreatDTO, String>(
                createDoubleClickableTextCell()) {
            @Override
            public String getValue(NaturalThreatDTO object) {
                return ViewUtil.formatProbability(object.getLot());
            }
        };
        column.setSortable(true);

        Comparator<NaturalThreatDTO> comparator = new Comparator<NaturalThreatDTO>() {
            public int compare(NaturalThreatDTO o1, NaturalThreatDTO o2) {
                return o1.getLot().compareTo(o2.getLot());
            }
        };
        return new DataColumn<NaturalThreatDTO>(DataGridHeaders.threatLikelihood(), column, comparator, 10);

    }

    private DataColumn<NaturalThreatDTO> createVulnerabilityColumn() {
    	Column<NaturalThreatDTO, String> column = new Column<NaturalThreatDTO, String>(
    			createDoubleClickableTextCell()) {
    		@Override
    		public String getValue(NaturalThreatDTO object) {
                return ViewUtil.formatProbability(object.getVulnerability());
    		}
    	};
    	column.setSortable(true);
    	
    	Comparator<NaturalThreatDTO> comparator = new Comparator<NaturalThreatDTO>() {
    		public int compare(NaturalThreatDTO o1, NaturalThreatDTO o2) {
    			return o1.getVulnerability().compareTo(o2.getVulnerability());
    		}
    	};
    	return new DataColumn<NaturalThreatDTO>("Vulnerability", column, comparator, 10);
    }

    private DataColumn<NaturalThreatDTO> createFinancialTotalColumn() {
        Column<NaturalThreatDTO, String> column = new Column<NaturalThreatDTO, String>(
                createDoubleClickableTextCell()) {
            @Override
            public String getValue(NaturalThreatDTO object) {
                return useStatValue ? ViewUtil.toDisplayValue(object.getFinancialTotalDTO().getQuantity()) :
                        "";
            }
        };
        column.setSortable(true);

        Comparator<NaturalThreatDTO> comparator = new Comparator<NaturalThreatDTO>() {
            public int compare(NaturalThreatDTO o1, NaturalThreatDTO o2) {
                return useStatValue ? o1.getFinancialTotalDTO().getQuantity().compareTo(o2.getFinancialTotalDTO().getQuantity()) :
                        0;
            }
        };
        return new DataColumn<NaturalThreatDTO>(DataGridHeaders.financialTotal(), column, comparator, 10);

    }
    
    private DataColumn<NaturalThreatDTO> createTotalRiskColumn() {
    	Column<NaturalThreatDTO, String> column = new Column<NaturalThreatDTO, String>(
    			createDoubleClickableTextCell()) {
    		@Override
    		public String getValue(NaturalThreatDTO object) {
    			return ViewUtil.toDisplayValue((object.getTotalRiskDTO().getQuantity()));
    		}
    	};
    	column.setSortable(true);

    	Comparator<NaturalThreatDTO> comparator = new Comparator<NaturalThreatDTO>() {
    		public int compare(NaturalThreatDTO o1, NaturalThreatDTO o2) {
    			return o1.getTotalRiskDTO().getQuantity().compareTo(o2.getTotalRiskDTO().getQuantity());
    		}
    	};
    	return new DataColumn<NaturalThreatDTO>(DataGridHeaders.totalRisk(), column, comparator, 10);
    }

    private DoubleClickableTextCell<NaturalThreatDTO> createDoubleClickableTextCell() {
        return new DoubleClickableTextCell<NaturalThreatDTO>(getSelectionModel(), getSelectionSource());
    }

    public void setUseStatValue(boolean useStatValue) {
        this.useStatValue = useStatValue;
    }
}
