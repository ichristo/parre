/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.nt.view;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.parre.client.ClientSingleton;
import org.parre.client.nt.presenter.EditEarthquakeProfilePresenter;
import org.parre.client.util.BaseModelView;
import org.parre.client.util.ClientLoggingUtil;
import org.parre.client.util.FieldValidationHandler;
import org.parre.client.util.ViewUtil;
import org.parre.client.util.dataview.DataProvider;
import org.parre.client.util.dataview.DataProviderFactory;
import org.parre.shared.EarthquakeLookupDataDTO;
import org.parre.shared.EarthquakeMagnitudeProbabilityDTO;
import org.parre.shared.EarthquakeProfileDTO;

import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

/**
 * The Class EditEarthquakeProfileView.
 */
public class EditEarthquakeProfileView extends BaseModelView<EarthquakeProfileDTO> implements EditEarthquakeProfilePresenter.Display {
	
	private Widget viewWidget;
	private Label header;
    private TextBox percentG;
    private Label seismicZoneLabel;
    private Label seismicZone;
    private Label magnitudeLevelsLabel;
    private Label magnitudeLevels;
    private Button updateEarthquake;
    private EarthquakeMagnitudeGrid earthquakeMagnitudeGrid;
    private HorizontalPanel htmlPanel2;
    private TextBox returnPeriodUsed;
    private Button saveEarthquakeData;
    private Button cancelButton;
    private List<EarthquakeMagnitudeProbabilityDTO> earthquakeMagnitudeProbabilityDTOs;
    private TextBox profileName;
    private CheckBox makeDefault;
    private List<FieldValidationHandler> requiredFieldHandlers;
    private FlexTable returnPeriodTable;
    
    public EditEarthquakeProfileView() {
    	requiredFieldHandlers = new ArrayList<FieldValidationHandler>();
    	viewWidget = buildView();
    }
    
    private Widget buildView() {
    	returnPeriodTable = new FlexTable();
    	 
    	header = new Label("Edit Earthquake Profile");
        header.setStyleName("inputPanelHeader");
        
        FlexTable inputTable = new FlexTable();
        inputTable.setCellSpacing(1);
    	
    	VerticalPanel viewPanel = new VerticalPanel();
        viewPanel.setWidth("100%");
        viewPanel.setSpacing(5);
        viewPanel.setStyleName("inputPanel");

        viewPanel.add(header);
        viewPanel.add(createEarthquakeDialogBox());
        
        profileName = ViewUtil.addTextBoxFormEntry(inputTable, inputTable.getRowCount(), "Profile Name", requiredFieldHandlers);
        makeDefault = new CheckBox("Make Default");
        ViewUtil.addFormEntry(inputTable, inputTable.getRowCount(), "", makeDefault);
        viewPanel.add(ViewUtil.layoutCenter(inputTable));
        
        cancelButton = ViewUtil.createButton("Cancel");
        viewPanel.add(ViewUtil.layoutCenter(saveEarthquakeData, cancelButton));
        return viewPanel;
    }
    
    private Widget createEarthquakeDialogBox() {
        DataProvider<EarthquakeMagnitudeProbabilityDTO> dataProvider = DataProviderFactory.createDataProvider();
        earthquakeMagnitudeGrid = new EarthquakeMagnitudeGrid(dataProvider);
        earthquakeMagnitudeGrid.setHeight("250px");
        earthquakeMagnitudeGrid.setWidth("500px");
        earthquakeMagnitudeGrid.setData(Collections.<EarthquakeMagnitudeProbabilityDTO>emptyList());

        HTML html = new HTML("<p><a href = 'https://geohazards.usgs.gov/hazards/apps/cmaps/' target='_blank'>" +
                "Go to this website to find the percent g for your location.</a></p>");

        HorizontalPanel htmlPanel1 = ViewUtil.layoutCenter(html);

        FlexTable percentGTable = new FlexTable();
        percentG = ViewUtil.addTextBoxFormEntry(percentGTable, percentGTable.getRowCount(), "Percent g", requiredFieldHandlers);
        updateEarthquake = ViewUtil.createButton("Update Percent G");
        ViewUtil.addFormEntry(percentGTable, percentGTable.getRowCount(), "", updateEarthquake);
        
        VerticalPanel panel = new VerticalPanel();
        panel.setStyleName("inputPanel");
        panel.setSpacing(5);

        seismicZoneLabel = new Label("Seismic Zone:");
        seismicZone = new Label(" - ");
        magnitudeLevelsLabel = new Label("Magnitude Levels:");
        magnitudeLevels = new Label(" - ");
        HorizontalPanel labelPanel =
                ViewUtil.layoutCenter(seismicZoneLabel, seismicZone, magnitudeLevelsLabel, magnitudeLevels);

        html = new HTML("<p><a href = 'https://geohazards.usgs.gov/eqprob/2009/' target='_blank'>" +
                "Go to this website to find the probabilities for the given magnitudes.</a></p>");
        htmlPanel2 = ViewUtil.layoutCenter(html);
        
        returnPeriodUsed = ViewUtil.addTextBoxFormEntry(returnPeriodTable, returnPeriodTable.getRowCount(), "Return Period Used", requiredFieldHandlers);
        saveEarthquakeData = ViewUtil.createButton("Save Profile");

        panel.add(htmlPanel1);
        panel.add(ViewUtil.layoutCenter(percentGTable));
        panel.add(labelPanel);
        panel.add(htmlPanel2);
        panel.add(ViewUtil.layoutCenter(earthquakeMagnitudeGrid.asWidget()));
        panel.add(ViewUtil.layoutCenter(returnPeriodTable));
        return panel;

    }
    
    @Override
	public HasClickHandlers getPercentGButton() {
		return updateEarthquake;
	}
    
	@Override
	public String getPercentGValue() {
		return percentG.getText();
	}

	@Override
    public void setEarthquakeLookupResult(EarthquakeLookupDataDTO result) {
        earthquakeMagnitudeProbabilityDTOs = new ArrayList<EarthquakeMagnitudeProbabilityDTO>();
        seismicZone.setText(result.getSeismicZone().toString());
        Long dummyId = 0l;
        if(result.getUpperRichter().toString().equals("-1.00")) {
            magnitudeLevels.setText(result.getLowerRichter().toString() + " and above");
        }
        else {
            magnitudeLevels.setText(result.getLowerRichter().toString() + " - " + result.getUpperRichter().toString());
        }
        BigDecimal richter = result.getOther();
        while(8.0 >= richter.doubleValue()) {
        	EarthquakeMagnitudeProbabilityDTO dummyDto = new EarthquakeMagnitudeProbabilityDTO(richter);
        	dummyDto.setId(++dummyId);
            earthquakeMagnitudeProbabilityDTOs.add(dummyDto);
            richter = richter.add(new BigDecimal("0.5"));
        }
        earthquakeMagnitudeGrid.setData(earthquakeMagnitudeProbabilityDTOs);
    }
	
	@Override
	public HasClickHandlers getSaveButton() {
		return saveEarthquakeData;
	}

	@Override
	public HasClickHandlers getCancelButton() {
		return cancelButton;
	}

	@Override
	public void setModel(EarthquakeProfileDTO model) {
		super.setModel(model);
		updateView();
	}

	@Override
	public void setModelAndUpdateView(EarthquakeProfileDTO model) {
		super.setModel(model);
		updateView();
	}

	@Override
	public boolean validate() {
		boolean valid = true;
		
		valid = ViewUtil.validate(requiredFieldHandlers);
		ClientLoggingUtil.debug("Validation result after required fields: " + valid);
		for (EarthquakeMagnitudeProbabilityDTO dto : earthquakeMagnitudeGrid.getData()) {
			ClientLoggingUtil.debug("Checking probability: " + dto.getProbability());
			if (dto.getProbability() == null || dto.getProbability().doubleValue() < 0d) {
				valid = false;
			}
		}
		
		ClientLoggingUtil.debug("Return value of validation: " + valid);
		return valid;
	}

	@Override
	public void updateModel() {
		EarthquakeProfileDTO profile = getModel();
		profile.setParreAnalysisId(ClientSingleton.getCurrentParreAnalysis().getId());
		profile.setPercentG(ViewUtil.getBigDecimal(percentG));
		profile.setReturnValue(ViewUtil.getBigDecimal(returnPeriodUsed));
		profile.setName(profileName.getText());
		profile.setAnalysisDefault(makeDefault.getValue());
		
		Set<EarthquakeMagnitudeProbabilityDTO> copies = new HashSet<EarthquakeMagnitudeProbabilityDTO>();
		for (EarthquakeMagnitudeProbabilityDTO dto : earthquakeMagnitudeGrid.getData()) {
			copies.add(new EarthquakeMagnitudeProbabilityDTO(dto.getMagnitude(), dto.getProbability()));
		}
		profile.setMagnitudeProbabilityDTOs(copies);
	}

	@Override
	public void updateView() {
		EarthquakeProfileDTO profile = getModel();
		
		if (profile.getPercentG() != null && profile.getPercentG().compareTo(BigDecimal.ZERO) == 1) {
			ViewUtil.setBigDecimal(percentG, profile.getPercentG());
			earthquakeMagnitudeGrid.setData(EarthquakeProfileDTO.getMagnitudeProbabilitiesInOrder(profile));
		} else {
			percentG.setText("");
			earthquakeMagnitudeGrid.setData(new ArrayList<EarthquakeMagnitudeProbabilityDTO>());
		}
		
		ViewUtil.setBigDecimal(returnPeriodUsed, profile.getReturnValue());
		ViewUtil.setText(profileName, profile.getName());
		makeDefault.setValue(ClientSingleton.getCurrentParreAnalysis().isDefaultEarthquakeProfile(profile));
	
	}

	@Override
	public Widget asWidget() {
		return viewWidget;
	}


}
