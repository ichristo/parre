/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.nt.view;

import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextBox;

import java.util.List;

import org.parre.client.util.ClientLoggingUtil;
import org.parre.client.util.ViewUtil;
import org.parre.shared.LabelValueDTO;
import org.parre.shared.NaturalThreatCalculationDTO;
import org.parre.shared.NaturalThreatDTO;
import org.parre.shared.PhysicalResourceDTO;

/**
 * The Class EditTornadoThreatView.
 */
public class EditTornadoThreatView extends EditNaturalThreatView {
	private CheckBox overrideLocation;
	private ListBox countyName;
	private ListBox stateCode;
	
	@Override
	protected void addCalculationWidgets(FlexTable inputTable) {
		super.addCalculationWidgets(inputTable);
		
		int currentRow = inputTable.getRowCount() - 1;
		
		overrideLocation = new CheckBox("Override county/state value");
		ViewUtil.addFormEntry(inputTable, ++currentRow, "", overrideLocation);

        stateCode = ViewUtil.addListBoxFormEntry(inputTable, ++currentRow, "State Code");
    	stateCode.setWidth(calculatorWidgetWidth);
    	stateCode.setEnabled(false);
		
		countyName = ViewUtil.addListBoxFormEntry(inputTable, ++currentRow, "County");
    	countyName.setWidth(calculatorWidgetWidth);
    	countyName.setEnabled(false);
	}
	
	public void toggleOverride() {
		countyName.setEnabled(overrideLocation.getValue());
		stateCode.setEnabled(overrideLocation.getValue());
        if(!overrideLocation.getValue()) {
        	ClientLoggingUtil.info("Setting location from physical resource: " + String.valueOf(physicalResource));
            setSelectedValue(countyName, physicalResource == null ? "" : physicalResource.getCountyName());
            setSelectedValue(stateCode, physicalResource == null ? "" : physicalResource.getStateCode());
        }
	}

	@Override
	public void updateModel() {
		super.updateModel();
		NaturalThreatDTO model = getModel();
		model.setUseDefaultValues(!overrideLocation.getValue());
		
		if (model.getUseDefaultValues()) {
			model.setCountyOverride(null);
			model.setStateCodeOverride(null);
		} else {
			model.setCountyOverride(getSelectedValue(countyName));
			model.setStateCodeOverride(getSelectedValue(stateCode));
		}
	}

	@Override
	public void updateView() {
		super.updateView();
		NaturalThreatDTO model = getModel();
		overrideLocation.setValue(!model.getUseDefaultValues());
		if (overrideLocation.getValue()) {
			setSelectedValue(countyName, model.getCountyOverride());
			setSelectedValue(stateCode, model.getStateCodeOverride());
		}

		toggleOverride();
	}

    @Override
    public TextBox getOperationalLoss() {
        return operationLoss;
    }

    @Override
    public Boolean getTornadoLocationOverride() {
        return overrideLocation.getValue();
    }

    @Override
    public ListBox getTornadoCountyName() {
        return countyName;
    }

    @Override
	public NaturalThreatCalculationDTO prepareCalculationDTO() {
		calculation = super.prepareCalculationDTO();
		
		calculation.getAnalysis().setUseDefaultValues(!overrideLocation.getValue());
		calculation.getAnalysis().setCountyOverride(getSelectedValue(countyName));
		calculation.getAnalysis().setStateCodeOverride(getSelectedValue(stateCode));
		
		return calculation;
	}

	@Override
	public void setPhysicalResource(PhysicalResourceDTO dto) {
		if (dto != null) {
			ClientLoggingUtil.info("Received physical resource in edit natural threat view.");
			super.setPhysicalResource(dto);
			if (!overrideLocation.getValue()) {
                setSelectedValue(countyName, dto.getCountyName());
			    setSelectedValue(stateCode, dto.getStateCode());
	            replacementCost.setText(ViewUtil.toDisplayValue(dto.getReplacementCostDTO().getQuantity()));
	            damageFactor.setText(ViewUtil.toDisplayValue(dto.getDamageFactor()));
			}
		} else {
			ClientLoggingUtil.info("Null physical resource passed into edit natural threat view.");
		}
	}
	
	@Override
	protected void toggleManualEntry() {
 		super.toggleManualEntry();
 		
 		boolean isManualEntry = useManualEntry.getValue();
 		operationLoss.setEnabled(!isManualEntry);
 		overrideLocation.setEnabled(!isManualEntry);
	}

    @Override
    public void setTornadoStateList(List<LabelValueDTO> stateList) {
        stateCode.clear();
        stateCode.addItem("Please Select", "-1");
        for (LabelValueDTO value : stateList) {
            stateCode.addItem(value.getLabel(), value.getValue());
        }
        stateCode.getElement().getFirstChildElement().setAttribute("disabled", "disabled");
    }

    public ListBox getStateCode() {
        return stateCode;
    }

    public CheckBox getOverrideLocation() {
        return overrideLocation;
    }
	
}
