/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.nt.view;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import org.parre.client.ClientSingleton;
import org.parre.client.util.ClientLoggingUtil;
import org.parre.client.util.DivStyleTemplate;
import org.parre.client.util.DoubleClickableTextCell;
import org.parre.client.util.ViewUtil;
import org.parre.client.util.dataview.DataColumn;
import org.parre.client.util.dataview.DataGridView;
import org.parre.client.util.dataview.DataProvider;
import org.parre.shared.EarthquakeMagnitudeProbabilityDTO;

import com.google.gwt.cell.client.Cell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.cell.client.TextInputCell;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.NativeEvent;
import com.google.gwt.dom.client.Style;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;
import com.google.gwt.user.cellview.client.Column;

/**
 * User: keithjones
 * Date: 5/7/12
 * Time: 11:48 AM
*/
public class EarthquakeMagnitudeGrid extends DataGridView<EarthquakeMagnitudeProbabilityDTO> {
	
    public EarthquakeMagnitudeGrid(DataProvider<EarthquakeMagnitudeProbabilityDTO> earthquakeDataProvider) {
        super(earthquakeDataProvider);
        List<DataColumn<EarthquakeMagnitudeProbabilityDTO>> columnList =
                new ArrayList<DataColumn<EarthquakeMagnitudeProbabilityDTO>>();
        columnList.add(createMagnitudeColumn());
        columnList.add(createProbabilityColumn());
        addColumns(columnList);
    }

    private DataColumn<EarthquakeMagnitudeProbabilityDTO> createMagnitudeColumn() {
        Column<EarthquakeMagnitudeProbabilityDTO, String> column = new Column<EarthquakeMagnitudeProbabilityDTO,String>(
                createDoubleClickableTextCell()) {
            @Override
            public String getValue(EarthquakeMagnitudeProbabilityDTO object) {
                return object.getMagnitude().toString();
            }
        };
        column.setSortable(true);
        Comparator<EarthquakeMagnitudeProbabilityDTO> comparator = new Comparator<EarthquakeMagnitudeProbabilityDTO>() {
            public int compare(EarthquakeMagnitudeProbabilityDTO o1, EarthquakeMagnitudeProbabilityDTO o2) {
                return o1.getMagnitude().compareTo(o2.getMagnitude());
            }
        };
        return new DataColumn<EarthquakeMagnitudeProbabilityDTO>("Magnitude", column, comparator, 30, Style.Unit.PX);
    }

    private DoubleClickableTextCell<EarthquakeMagnitudeProbabilityDTO> createDoubleClickableTextCell() {
        return new DoubleClickableTextCell<EarthquakeMagnitudeProbabilityDTO>(getSelectionModel(), getSelectionSource());
    }

    private DataColumn<EarthquakeMagnitudeProbabilityDTO>createProbabilityColumn() {
        final TextInputCell cell = new TextInputCell();
        Column<EarthquakeMagnitudeProbabilityDTO, String> column = new Column<EarthquakeMagnitudeProbabilityDTO,String>(cell) {
            @Override
            public String getValue(EarthquakeMagnitudeProbabilityDTO object) {
                return object.getProbability() == null ? "0" : ViewUtil.formatProbability(object.getProbability());
            }

            @Override
            public void onBrowserEvent(Cell.Context context, Element elem, EarthquakeMagnitudeProbabilityDTO object, NativeEvent event) {
                if (!ClientSingleton.isCurrentAnalysisLocked()) {
                    super.onBrowserEvent(context, elem, object, event);
                }
            }

            @Override
            public void render(Cell.Context context, EarthquakeMagnitudeProbabilityDTO object, SafeHtmlBuilder sb) {
                if (!ClientSingleton.isCurrentAnalysisLocked()) { // && cell.isEditing(context, null, getValue(object))) {
                    super.render(context, object, sb);
                } else {
	                try {
	                	sb.append(DivStyleTemplate.INSTANCE.render(getValue(object), "normalValueCell"));
	                } catch (NullPointerException e) {
	                    ClientLoggingUtil.info("error caught in magnitude grid: " + e.getLocalizedMessage());
	                }
                }
            }
            
        };
        column.setSortable(true);
        
        Comparator<EarthquakeMagnitudeProbabilityDTO> comparator = new Comparator<EarthquakeMagnitudeProbabilityDTO>() {
            public int compare(EarthquakeMagnitudeProbabilityDTO o1, EarthquakeMagnitudeProbabilityDTO o2) {
            	if (o1.getProbability() == null || o2.getProbability() == null) {
            		return 0;
            	}
                return o1.getProbability().compareTo(o2.getProbability());
            }
        };

        column.setFieldUpdater(new FieldUpdater<EarthquakeMagnitudeProbabilityDTO, String>() {
            public void update(int index, EarthquakeMagnitudeProbabilityDTO object, String value) {
                try {
                    ClientLoggingUtil.debug("Setting row value to : "+  value);
                    object.setProbability(ViewUtil.parseProbability(value));
                } catch (NumberFormatException e) {
                	ClientLoggingUtil.debug("Error with field updater...");
                }
            }
        });
        return new DataColumn<EarthquakeMagnitudeProbabilityDTO>("Probability", column, comparator, 30, Style.Unit.PX);
    }

}
