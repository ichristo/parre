/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.nt.view;

import java.util.List;

import org.parre.client.ClientSingleton;
import org.parre.client.common.view.AnalysisCommandPanel;
import org.parre.client.common.view.BaseAnalysisView;
import org.parre.client.common.view.DisplayToggleView;
import org.parre.client.common.view.NameDescriptionView;
import org.parre.client.common.view.ThreatCriteriaView;
import org.parre.client.nt.presenter.ManageNaturalThreatPresenter;
import org.parre.client.util.ActionSource;
import org.parre.client.util.ViewUtil;
import org.parre.client.util.dataview.DataProvider;
import org.parre.client.util.dataview.DataProviderFactory;
import org.parre.shared.NameDescriptionDTO;
import org.parre.shared.NaturalThreatDTO;

import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.event.logical.shared.HasCloseHandlers;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.view.client.SingleSelectionModel;

/**
 * The Class ManageNaturalThreatView.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 7/16/11
 */
public class ManageNaturalThreatView extends BaseAnalysisView implements ManageNaturalThreatPresenter.Display {
    private NaturalThreatGrid naturalThreatGrid;
    private ThreatCriteriaView threatCriteriaView;
    private Image searchButton;
    private Image clearButton;
    private VerticalPanel searchPanel;
    private Widget viewWidget;
    private DialogBox editPopup;
    private DisplayToggleView displayToggleView;
    
    public ManageNaturalThreatView(ThreatCriteriaView threatCriteriaView, DisplayToggleView displayToggleView) {
        super(new AnalysisCommandPanel(), new NameDescriptionView());
        this.threatCriteriaView = threatCriteriaView;
        this.displayToggleView = displayToggleView;
        viewWidget = createView();
    }

    private Widget createView() {
        searchPanel = new VerticalPanel();
        searchPanel.setWidth("100%");
        getCommandPanel().setToolTips("Back to Threat Analysis", "Forward to Dependency Threat Analysis");

        searchButton = ViewUtil.createImageButton("search-button.png", "Perform Search");
        clearButton = ViewUtil.createImageButton("clear-button.jpg", "Clear Search Results");
        searchPanel.add(threatCriteriaView.asWidget());

        searchPanel.add(ViewUtil.layoutCenter(searchButton, clearButton));

        DataProvider<NaturalThreatDTO> dataProvider = DataProviderFactory.createDataProvider();
        naturalThreatGrid = new NaturalThreatGrid(dataProvider);
        naturalThreatGrid.asWidget().setVisible(false);

        displayToggleView.setText("Search Criteria");
        displayToggleView.setDisplayToggleCommand(new DisplayToggleView.DisplayToggleCommand() {
            public void toggleDisplay(boolean show) {
                setSearchVisible(show);
            }
        });
        displayToggleView.setDisplayVisible(false);
        VerticalPanel viewPanel = new VerticalPanel();

        viewPanel.setWidth("100%");
        viewPanel.setHeight("100%");
        viewPanel.add(displayToggleView.asWidget());
        viewPanel.add(searchPanel);
        viewPanel.add(getCommandPanel().asWidget());
        
        viewPanel.add(naturalThreatGrid.asWidget());
        viewPanel.setSpacing(6);
        return viewPanel;
    }

    public void setSearchVisible(boolean value) {
        searchPanel.setVisible(value);
        naturalThreatGrid.setHeight(value ? "500px" : "650px");
    }

    @Override
    public void setCurrentAnalysis(NameDescriptionDTO currentAnalysis) {
        super.setCurrentAnalysis(currentAnalysis);
        naturalThreatGrid.setUseStatValue(ClientSingleton.getCurrentParreAnalysis().getUseStatValues());
        naturalThreatGrid.asWidget().setVisible(currentAnalysis != null);
    }

    @Override
    public HasClickHandlers getPreviousButton() {
        return getCommandPanel().getPreviousButton();
    }

    @Override
    public HasClickHandlers getNextButton() {
        return getCommandPanel().getNextButton();
    }

    public SingleSelectionModel<NaturalThreatDTO> getSelectionModel() {
        return naturalThreatGrid.getSelectionModel();
    }

    public ActionSource<NaturalThreatDTO> getNaturalThreatSelectionSource() {
        return naturalThreatGrid.getSelectionSource();
    }

    public HasClickHandlers getSearchButton() {
        return searchButton;
    }

    public HasClickHandlers getClearButton() {
        return clearButton;
    }

    public void setSearchResults(List<NaturalThreatDTO> results) {
        naturalThreatGrid.asWidget().setVisible(true);
        getCommandPanel().setAnalysisCount(results.size());
        naturalThreatGrid.setData(results);
        naturalThreatGrid.getSelectionModel().setSelected(null, true);
    }

    public void refreshSearchResultsView() {
        naturalThreatGrid.refresh();
    }

    public Widget asWidget() {
        return viewWidget;
    }

    public HasCloseHandlers<PopupPanel> getEditPopup() {
        return editPopup;
    }

    public void showEditPopup() {
        //editPopup.showRelativeTo(naturalThreatGrid.asWidget());
        editPopup.center();
        editPopup.show();
        setSearchResultsViewDisabled(true);
    }

    public void setSearchResultsViewDisabled(boolean value) {
        if (value) {
            naturalThreatGrid.asWidget().getElement().setAttribute("disabled", "true");
        }
        else {
            naturalThreatGrid.asWidget().getElement().removeAttribute("disabled");
        }
    }

    public void hideEditAssetPopup() {
        editPopup.hide();
    }

    public List<NaturalThreatDTO> getSearchResults() {
        return naturalThreatGrid.getData();
    }

    @Override
    public void refreshView() {
        naturalThreatGrid.forceLayout();
    }
}
