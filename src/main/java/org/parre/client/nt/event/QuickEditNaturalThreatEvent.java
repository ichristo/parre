/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.nt.event;

import org.parre.shared.NaturalThreatDTO;

import com.google.gwt.event.shared.EventHandler;
import com.google.gwt.event.shared.GwtEvent;

/**
 * .
 * User: Swarn S. Dhaliwal
 * Date: 9/5/11
 * Time: 9:14 AM
 */
public class QuickEditNaturalThreatEvent extends GwtEvent<QuickEditNaturalThreatEvent.Handler> {

    private NaturalThreatDTO naturalThreatDTO;

    public static QuickEditNaturalThreatEvent create(NaturalThreatDTO assetDTO) {
        return new QuickEditNaturalThreatEvent(assetDTO);
    }

    private QuickEditNaturalThreatEvent(NaturalThreatDTO assetThreatDTO) {
        this.naturalThreatDTO = assetThreatDTO;
    }

    public NaturalThreatDTO getNaturalThreatDTO() {
        return naturalThreatDTO;
    }

    /* WARNING : The following section contains GwtEvent framework stuff
       and should not be changed
    */
    @Override
    public Type<Handler> getAssociatedType() {
        return TYPE;
    }

    protected void dispatch(Handler handler) {
        handler.onEvent(this);
    }

    public static final Type<Handler> TYPE = new Type<Handler>();

    /**
     * The Interface Handler.
     */
    public interface Handler extends EventHandler {
        void onEvent(QuickEditNaturalThreatEvent event);
    }

}
