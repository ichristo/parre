/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.nt.event;

import com.google.gwt.event.shared.EventHandler;
import com.google.gwt.event.shared.GwtEvent;

/**
 * User: keithjones
 * Date: 9/11/13
 * Time: 12:20 PM
*/
public class GetTornadoCountyDataEvent extends GwtEvent<GetTornadoCountyDataEvent.Handler> {

	public static GetTornadoCountyDataEvent create() {
		return new GetTornadoCountyDataEvent();
	}

	/* WARNING : The following section contains GwtEvent framework stuff
	    and should not be changed
	 */
	 @Override
	 public Type<Handler> getAssociatedType() {
	     return TYPE;
	 }

	 protected void dispatch(Handler handler) {
	     handler.onEvent(this);
	 }

	 public static final Type<Handler> TYPE = new Type<Handler>();

	 /**
 	 * The Interface Handler.
 	 */
 	public interface Handler extends EventHandler {
	     void onEvent(GetTornadoCountyDataEvent event);
	 }
}

