/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.nt.presenter;

import java.util.Collections;
import java.util.List;

import org.parre.client.ClientSingleton;
import org.parre.client.ParreClientUtil;
import org.parre.client.asset.event.ClearAssetSearchEvent;
import org.parre.client.common.event.AssetThreatSearchType;
import org.parre.client.common.presenter.ManageAnalysisPresenter;
import org.parre.client.common.view.AnalysisView;
import org.parre.client.event.GoHomeEvent;
import org.parre.client.messaging.ServiceFactory;
import org.parre.client.nav.event.DpNavEvent;
import org.parre.client.nav.event.LotNavEvent;
import org.parre.client.nav.event.NaturalThreatNavEvent;
import org.parre.client.nt.event.EditNaturalThreatEvent;
import org.parre.client.nt.event.ManageNaturalThreatNavEvent;
import org.parre.client.nt.event.NaturalThreatSearchResultsEvent;
import org.parre.client.nt.event.NaturalThreatUpdatedEvent;
import org.parre.client.threat.event.ThreatSearchEvent;
import org.parre.client.util.ActionSource;
import org.parre.client.util.BaseAsyncCommand;
import org.parre.client.util.BusyHandler;
import org.parre.client.util.ExecutableAsyncCallback;
import org.parre.client.util.Presenter;
import org.parre.client.util.nav.NavManager;
import org.parre.shared.NaturalThreatAnalysisDTO;
import org.parre.shared.NaturalThreatDTO;
import org.parre.shared.ThreatDTO;
import org.parre.shared.types.AssetThreatAnalysisType;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.view.client.SingleSelectionModel;


/**
 * User: Swarn S. Dhaliwal
 * Date: 8/21/11
 * Time: 5:03 PM
*/
public class ManageNaturalThreatPresenter extends ManageAnalysisPresenter implements Presenter {
    private ThreatSearchEvent searchEvent = ThreatSearchEvent.create(AssetThreatSearchType.NATURAL_THREAT);
    private List<NaturalThreatDTO> naturalThreatList;
    private BusyHandler busyHandler;

    /**
     * The Interface Display.
     */
    public static interface Display extends AnalysisView {
        SingleSelectionModel<NaturalThreatDTO> getSelectionModel();

        ActionSource<NaturalThreatDTO> getNaturalThreatSelectionSource();

        HasClickHandlers getSearchButton();

        HasClickHandlers getClearButton();

        void setSearchResults(List<NaturalThreatDTO> dtos);

        void refreshSearchResultsView();

        List<NaturalThreatDTO> getSearchResults();

        void setSearchResultsViewDisabled(boolean value);
    }

    private EventBus eventBus;
    private Display display;

    public ManageNaturalThreatPresenter(Display display) {
        super(display);
        this.eventBus = ClientSingleton.getEventBus();
        this.display = display;
        this.busyHandler = new BusyHandler(display);
    }

    public void go() {
        bind();
        init();
    }

    private void bind() {
        eventBus.addHandler(ManageNaturalThreatNavEvent.TYPE, new ManageNaturalThreatNavEvent.Handler() {
            public void onEvent(ManageNaturalThreatNavEvent event) {
                getCurrentAnalysis();
            }
        });
        eventBus.addHandler(NaturalThreatUpdatedEvent.TYPE, new NaturalThreatUpdatedEvent.Handler() {
            public void onEvent(NaturalThreatUpdatedEvent event) {
                NaturalThreatDTO updated = event.getNaturalThreatDTO();
                List<NaturalThreatDTO> currentResults = display.getSearchResults();
                int index = currentResults.indexOf(updated);
                currentResults.set(index, updated);
                display.refreshSearchResultsView();
                index = naturalThreatList.indexOf(updated);
                naturalThreatList.set(index, updated);
            }
        });
        eventBus.addHandler(GoHomeEvent.TYPE, new GoHomeEvent.Handler() {
            public void onEvent(GoHomeEvent event) {
                display.setSearchResults(Collections.<NaturalThreatDTO>emptyList());
            }
        });
        display.getClearButton().addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                eventBus.fireEvent(ClearAssetSearchEvent.create());
                display.setSearchResults(Collections.<NaturalThreatDTO>emptyList());
            }
        });
        display.getSearchButton().addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                eventBus.fireEvent(searchEvent);
            }
        });
        display.getNaturalThreatSelectionSource().setCommand(new ActionSource.ActionCommand<NaturalThreatDTO>() {
            public void execute(NaturalThreatDTO dto) {
                if (ParreClientUtil.handleAnalysisBaselined()) {
                    return;
                }
                eventBus.fireEvent(EditNaturalThreatEvent.create(dto));
            }
        });

        display.getPreviousButton().addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent clickEvent) {
                NavManager.performNav(LotNavEvent.create());
            }
        });
        display.getNextButton().addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent clickEvent) {
                NavManager.performNav(DpNavEvent.create());
            }
        });

        eventBus.addHandler(NaturalThreatSearchResultsEvent.TYPE, new NaturalThreatSearchResultsEvent.Handler() {
            public void onEvent(NaturalThreatSearchResultsEvent event) {
                naturalThreatList = event.getResults();
                display.setSearchResults(naturalThreatList);
                display.getSelectionModel().setSelected(null, true);
                display.refreshView();
            }
        });
        
        eventBus.addHandler(NaturalThreatUpdatedEvent.TYPE, new NaturalThreatUpdatedEvent.Handler() {
            public void onEvent(NaturalThreatUpdatedEvent event) {
                NaturalThreatDTO threatDTO = event.getNaturalThreatDTO();
                int index = display.getSearchResults().indexOf(threatDTO);
                if (index < 0) {
                    display.getSearchResults().add(threatDTO);
                } else {
                    display.getSearchResults().set(index, threatDTO);
                }
                eventBus.fireEvent(NaturalThreatNavEvent.create());
            }
        });
    }
    
    @Override
    protected AssetThreatAnalysisType getAnalysisType() {
        return AssetThreatAnalysisType.NATURAL_THREAT;
    }

    @Override
    protected void startAnalysis() {
        final NaturalThreatAnalysisDTO analysisDTO = new NaturalThreatAnalysisDTO();
        analysisDTO.setParreAnalysisId(ClientSingleton.getParreAnalysisId());
        ExecutableAsyncCallback<NaturalThreatAnalysisDTO> exec =
                new ExecutableAsyncCallback<NaturalThreatAnalysisDTO>(busyHandler, new BaseAsyncCommand<NaturalThreatAnalysisDTO>() {
                    public void execute(AsyncCallback<NaturalThreatAnalysisDTO> callback) {
                        ServiceFactory.getInstance().getNaturalThreatService().
                                startAnalysis(analysisDTO, callback);
                    }

                    @Override
                    public void handleResult(NaturalThreatAnalysisDTO results) {
                        initCurrentAnalysis(results);
                    }
                });
        exec.makeCall();

    }

    @Override
    protected void getCurrentAnalysis() {
        ExecutableAsyncCallback<NaturalThreatAnalysisDTO> exec =
                new ExecutableAsyncCallback<NaturalThreatAnalysisDTO>(busyHandler, new BaseAsyncCommand<NaturalThreatAnalysisDTO>() {
                    public void execute(AsyncCallback<NaturalThreatAnalysisDTO> callback) {
                        ServiceFactory.getInstance().getNaturalThreatService().
                                getCurrentAnalysis(ClientSingleton.getParreAnalysisId(), callback);
                    }

                    @Override
                    public void handleResult(NaturalThreatAnalysisDTO results) {
                        initCurrentAnalysis(results);
                    }
                });
        exec.makeCall();

    }
    
    private void initCurrentAnalysis(NaturalThreatAnalysisDTO results) {
        super.initCurrentAnalysis(results);
        display.setCurrentAnalysis(results);
        if (results != null) {
            new NaturalThreatSearchCommand().execute(new ThreatDTO(), display);
        }
    }

}
