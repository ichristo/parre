/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.nt.presenter;

import java.util.List;

import org.parre.client.ClientSingleton;
import org.parre.client.messaging.NaturalThreatServiceAsync;
import org.parre.client.messaging.ServiceFactory;
import org.parre.client.nt.event.EditNaturalThreatCancelledEvent;
import org.parre.client.nt.event.EditNaturalThreatEvent;
import org.parre.client.nt.event.GetTornadoCountyDataEvent;
import org.parre.client.nt.event.NaturalThreatUpdatedEvent;
import org.parre.client.util.BaseAsyncCommand;
import org.parre.client.util.BusyHandler;
import org.parre.client.util.ExecutableAsyncCallback;
import org.parre.client.util.MessageDialog;
import org.parre.client.util.ModelView;
import org.parre.client.util.Presenter;
import org.parre.client.util.ViewUtil;
import org.parre.shared.LabelValueDTO;
import org.parre.shared.NaturalThreatCalculationDTO;
import org.parre.shared.NaturalThreatDTO;
import org.parre.shared.PhysicalResourceDTO;
import org.parre.shared.types.NaturalThreatType;

import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.HasText;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextBox;
import com.google.web.bindery.event.shared.EventBus;


/**
 * The Class EditNaturalThreatPresenter.
 */
public class EditNaturalThreatPresenter implements Presenter {

	protected EventBus eventBus;
    protected Display display;
    protected NaturalThreatServiceAsync service;
    protected BusyHandler busyHandler;
    
    /**
     * The Interface Display.
     */
    public interface Display extends ModelView<NaturalThreatDTO> {
        HasClickHandlers getSaveButton();
        HasClickHandlers getCancelButton();
        HasClickHandlers getCalculateButton();
        HasText getHeader();
        void setPhysicalResource(PhysicalResourceDTO dto);
        PhysicalResourceDTO getPhysicalResourceDTO();
        NaturalThreatCalculationDTO prepareCalculationDTO();
        void updateCalculationResults(NaturalThreatCalculationDTO calculation);
        HasClickHandlers getAddProfileButton();
        HasClickHandlers getEditProfileButton();
        Long getCurrentProfileId();
        void updateProfileList();
        Long getLastParreAnalysisId();
        void setStateList(List<LabelValueDTO> stateList);
        boolean validateCalculator();

        TextBox getEconomicImpact();

        TextBox getOperationalLoss();

        TextBox getFinancialImpact();
        
        void setHasCalculator(boolean value);

        void selectHurricaneProfile(Long hurricaneProfileId);

        void selectEarthquakeProfile(Long earthquakeProfileId);

        HasClickHandlers getManualCalculateButton();

        void manuallyCalculate();

        Boolean getTornadoLocationOverride();

        ListBox getTornadoCountyName();

        void setTornadoStateList(List<LabelValueDTO> stateList);

        ListBox getStateCode();

        CheckBox getOverrideLocation();

        void toggleOverride();
        
        CheckBox getUseManualEntry();
    }
    
    public EditNaturalThreatPresenter (Display display) {
    	this.service = ServiceFactory.getInstance().getNaturalThreatService();
    	this.eventBus = ClientSingleton.getEventBus();
    	this.display = display;
    }
    
	@Override
	public void go() {
		bind();
	}
	
	private void bind() {
		busyHandler = new BusyHandler(display);
		
		eventBus.addHandler(EditNaturalThreatEvent.TYPE, new EditNaturalThreatEvent.Handler() {
            public void onEvent(EditNaturalThreatEvent event) {
                NaturalThreatDTO naturalThreatDTO = event.getNaturalThreatDTO();
                
                final NaturalThreatType naturalThreatType = NaturalThreatType.getNaturalThreatType(naturalThreatDTO.getThreatDescription());
                if (!handlesNaturalThreatType(naturalThreatType)) {
                    return;
                }
                
                display.getHeader().setText("Edit/Calculate " + naturalThreatType.value() + " Threat");
                display.setModel(naturalThreatDTO);
                
                // Get the physical resource
                ExecutableAsyncCallback<PhysicalResourceDTO> exec = new ExecutableAsyncCallback<PhysicalResourceDTO>(busyHandler, new BaseAsyncCommand<PhysicalResourceDTO>() {
                    public void execute(AsyncCallback<PhysicalResourceDTO> asyncCallback) {
                        service.getPhysicalResourceForNT(display.getModel().getId(), asyncCallback);
                    }

                    @Override
                    public void handleResult(PhysicalResourceDTO result) {
                        display.setPhysicalResource(result);
                        display.updateView();
                        if(naturalThreatType.equals(NaturalThreatType.TORNADO)) {
                            eventBus.fireEvent(GetTornadoCountyDataEvent.create());
                        }
                    }

                    @Override
                    public boolean handleError(Throwable t) {
                    	display.updateView();
                        MessageDialog.popup(t.getMessage(), true);
                        return true;
                    }
                });
                exec.makeCall();
            }
        });
		display.getSaveButton().addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                if (!(display.validate())) {
                    return;
                }
                ExecutableAsyncCallback<NaturalThreatDTO> exec = new ExecutableAsyncCallback<NaturalThreatDTO>(busyHandler, new BaseAsyncCommand<NaturalThreatDTO>() {
                    public void execute(AsyncCallback<NaturalThreatDTO> asyncCallback) {
                        service.save(display.updateAndGetModel(), asyncCallback);
                    }

                    @Override
                    public void handleResult(NaturalThreatDTO result) {
                        eventBus.fireEvent(NaturalThreatUpdatedEvent.create(result));
                    }

                    @Override
                    public boolean handleError(Throwable t) {
                        MessageDialog.popup(t.getMessage(), true);
                        return true;
                    }
                });
                exec.makeCall();
            }
        });
        display.getEconomicImpact().addChangeHandler(new ChangeHandler() {
            @Override
            public void onChange(ChangeEvent changeEvent) {
                display.getEconomicImpact().setText(ViewUtil.toDisplayValue(
                        ViewUtil.getBigDecimal(display.getEconomicImpact())));
            }
        });
        display.getFinancialImpact().addChangeHandler(new ChangeHandler() {
            @Override
            public void onChange(ChangeEvent changeEvent) {
                display.getFinancialImpact().setText(ViewUtil.toDisplayValue(
                        ViewUtil.getBigDecimal(display.getFinancialImpact())));
            }
        });
        display.getCancelButton().addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                eventBus.fireEvent(EditNaturalThreatCancelledEvent.create());
            }
        });
        display.getCalculateButton().addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				calculate();
			}
        });
        display.getManualCalculateButton().addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                display.manuallyCalculate();
            }
        });
	}
	
	protected void calculate() {
		MessageDialog.popup("No calculator exists for this Natural Threat type.", true);
	}
	
	protected boolean handlesNaturalThreatType(NaturalThreatType type) {
		//TODO: Update this test when Wildfire has it's own presenter/view
		boolean hasCalculator = !(type == NaturalThreatType.UNKNOWN || type == NaturalThreatType.WILDFIRE);
		display.setHasCalculator(hasCalculator);
		return !hasCalculator; 
	}
	
}
