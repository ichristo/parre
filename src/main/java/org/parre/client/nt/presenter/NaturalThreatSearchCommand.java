/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.nt.presenter;


import com.google.gwt.user.client.rpc.AsyncCallback;

import java.util.List;

import org.parre.client.ClientSingleton;
import org.parre.client.common.presenter.ThreatSearchCommand;
import org.parre.client.messaging.ServiceFactory;
import org.parre.client.nt.event.NaturalThreatSearchResultsEvent;
import org.parre.client.util.*;
import org.parre.shared.NaturalThreatDTO;
import org.parre.shared.ThreatDTO;

/**
 * The Class NaturalThreatSearchCommand.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 10/27/11
 */
public class NaturalThreatSearchCommand implements ThreatSearchCommand {

    public void execute(final ThreatDTO dto, final View view) {
        ExecutableAsyncCallback<List<NaturalThreatDTO>> exec =
                new ExecutableAsyncCallback<List<NaturalThreatDTO>>(new BusyHandler(view), new BaseAsyncCommand<List<NaturalThreatDTO>>() {
                    public void execute(AsyncCallback<List<NaturalThreatDTO>> callback) {
                        ServiceFactory.getInstance().getNaturalThreatService().
                                getSearchResults(dto, ClientSingleton.getParreAnalysisId(), callback);
                    }

                    @Override
                    public void handleResult(List<NaturalThreatDTO> results) {
                        ClientSingleton.getEventBus().fireEvent(NaturalThreatSearchResultsEvent.create(results));

                    }
                });
        exec.makeCall();
    }
}
