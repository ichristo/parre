/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.nt.presenter;

import org.parre.client.ClientSingleton;
import org.parre.client.ViewFactory;
import org.parre.client.nav.event.NaturalThreatNavEvent;
import org.parre.client.nt.event.EarthquakeProfileUpdatedEvent;
import org.parre.client.nt.event.EditEarthquakeProfileCancelledEvent;
import org.parre.client.nt.event.EditEarthquakeProfileEvent;
import org.parre.client.nt.event.EditHurricaneProfileCancelledEvent;
import org.parre.client.nt.event.EditHurricaneProfileEvent;
import org.parre.client.nt.event.EditNaturalThreatCancelledEvent;
import org.parre.client.nt.event.EditNaturalThreatEvent;
import org.parre.client.nt.event.HurricaneProfileUpdatedEvent;
import org.parre.client.nt.event.ManageNaturalThreatNavEvent;
import org.parre.client.nt.view.EditEarthquakeProfileView;
import org.parre.client.nt.view.EditEarthquakeThreatView;
import org.parre.client.nt.view.EditFloodThreatView;
import org.parre.client.nt.view.EditHurricaneProfileView;
import org.parre.client.nt.view.EditHurricaneThreatView;
import org.parre.client.nt.view.EditIceStormThreatView;
import org.parre.client.nt.view.EditNaturalThreatView;
import org.parre.client.nt.view.EditTornadoThreatView;
import org.parre.client.nt.view.ManageNaturalThreatView;
import org.parre.client.util.Presenter;
import org.parre.client.util.View;
import org.parre.client.util.ViewContainer;
import org.parre.shared.types.NaturalThreatType;

import com.google.gwt.event.shared.EventBus;

/**
 * The Class NaturalThreatPresenter.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 7/19/11
 */
public class NaturalThreatPresenter implements Presenter {
    private Integer manageLotViewIndex;
    private Integer editNaturalThreatViewIndex;
    private Integer editTornadoThreatViewIndex;
    private Integer editFloodThreatViewIndex;
    private Integer editEarthquakeThreatViewIndex;
    private Integer editEarthquakeProfileViewIndex;
    private Integer editHurricaneThreatViewIndex;
    private Integer editHurricaneProfileViewIndex;
    private Integer editIceStormThreatViewIndex;

    private EventBus eventBus;
    private Display display;

    /**
     * The Interface Display.
     */
    public interface Display extends View, ViewContainer {

        void showView(int viewIndex);
    }

    public NaturalThreatPresenter(Display display) {
        this.eventBus = ClientSingleton.getEventBus();
        this.display = display;
    }

    public void go() {
        initViewLoaders();
        bind();
    }

    private void initViewLoaders() {
    	EditNaturalThreatView editView = ViewFactory.createEditNaturalThreatView();
    	editView.getPresenter().go();
    	editNaturalThreatViewIndex = display.addView(editView.asWidget());
    	
    	EditTornadoThreatView editTornadoView = ViewFactory.createEditTornadoThreatView();
    	editTornadoView.getPresenter().go();
    	editTornadoThreatViewIndex = display.addView(editTornadoView.asWidget());
    	
    	EditFloodThreatView editFloodView = ViewFactory.createEditFloodThreatView();
    	editFloodView.getPresenter().go();
    	editFloodThreatViewIndex = display.addView(editFloodView.asWidget());
    	
    	EditEarthquakeThreatView editEarthquakeView = ViewFactory.createEditEarthquakeThreatView();
    	editEarthquakeView.getPresenter().go();
    	editEarthquakeThreatViewIndex = display.addView(editEarthquakeView.asWidget());
    	
    	EditEarthquakeProfileView editEarthquakeProfileView = ViewFactory.createEditEarthquakeProfileView();
    	editEarthquakeProfileView.getPresenter().go();
    	editEarthquakeProfileViewIndex = display.addView(editEarthquakeProfileView.asWidget());
    	
    	EditHurricaneThreatView editHurricaneView = ViewFactory.createEditHurricaneThreatView();
    	editHurricaneView.getPresenter().go();
    	editHurricaneThreatViewIndex = display.addView(editHurricaneView.asWidget());
    	
    	EditHurricaneProfileView editHurricaneProfileView = ViewFactory.createEditHurricaneProfileView();
    	editHurricaneProfileView.getPresenter().go();
    	editHurricaneProfileViewIndex = display.addView(editHurricaneProfileView.asWidget());
    	
    	EditIceStormThreatView editIceStormThreatView = ViewFactory.createEditIceStormThreatView();
    	editIceStormThreatView.getPresenter().go();
    	editIceStormThreatViewIndex = display.addView(editIceStormThreatView.asWidget());
    	
        ManageNaturalThreatView naturalThreatView = ViewFactory.createManagerNaturalThreatView();
        naturalThreatView.getPresenter().go();
        manageLotViewIndex = display.addAndShowView(naturalThreatView.asWidget());
    }

    private void bind() {
        eventBus.addHandler(NaturalThreatNavEvent.TYPE, new NaturalThreatNavEvent.Handler() {
            public void onEvent(NaturalThreatNavEvent event) {
            	display.showView(manageLotViewIndex);
                eventBus.fireEvent(ManageNaturalThreatNavEvent.create());
            }
        });
        
        eventBus.addHandler(EditNaturalThreatEvent.TYPE, new EditNaturalThreatEvent.Handler() {
            public void onEvent(EditNaturalThreatEvent event) {
            	showEditNaturalThreatView(NaturalThreatType.getNaturalThreatType(event.getNaturalThreatDTO().getThreatDescription()));
            }
        });
        
        eventBus.addHandler(EditNaturalThreatCancelledEvent.TYPE, new EditNaturalThreatCancelledEvent.Handler() {
            public void onEvent(EditNaturalThreatCancelledEvent event) {
                display.showView(manageLotViewIndex);
            }
        });
        
        eventBus.addHandler(EditEarthquakeProfileEvent.TYPE, new EditEarthquakeProfileEvent.Handler() {
			@Override
			public void onEvent(EditEarthquakeProfileEvent event) {
				display.showView(editEarthquakeProfileViewIndex);
			}
        });
        
        eventBus.addHandler(EditEarthquakeProfileCancelledEvent.TYPE, new EditEarthquakeProfileCancelledEvent.Handler() {
			@Override
			public void onEvent(EditEarthquakeProfileCancelledEvent event) {
				display.showView(editEarthquakeThreatViewIndex);
			}
        });
        
        eventBus.addHandler(EarthquakeProfileUpdatedEvent.TYPE, new EarthquakeProfileUpdatedEvent.Handler() {
			@Override
			public void onEvent(EarthquakeProfileUpdatedEvent event) {
				display.showView(editEarthquakeThreatViewIndex);
			}
        });
        
        eventBus.addHandler(EditHurricaneProfileEvent.TYPE, new EditHurricaneProfileEvent.Handler() {
        	@Override
        	public void onEvent(EditHurricaneProfileEvent event) {
        		display.showView(editHurricaneProfileViewIndex);
        	}
        });
        
        eventBus.addHandler(EditHurricaneProfileCancelledEvent.TYPE, new EditHurricaneProfileCancelledEvent.Handler() {
        	@Override
        	public void onEvent(EditHurricaneProfileCancelledEvent event) {
        		display.showView(editHurricaneThreatViewIndex);
        	}
        });
        
        eventBus.addHandler(HurricaneProfileUpdatedEvent.TYPE, new HurricaneProfileUpdatedEvent.Handler() {
        	@Override
        	public void onEvent(HurricaneProfileUpdatedEvent event) {
        		display.showView(editHurricaneThreatViewIndex);
        	}
        });
    }
    
    private void showEditNaturalThreatView(NaturalThreatType type) {
    	switch(type) {
	    	case TORNADO: 
	    		display.showView(editTornadoThreatViewIndex);
	    		break;
	    	case FLOOD:
	    		display.showView(editFloodThreatViewIndex);
	    		break;
	    	case EARTHQUAKE:
	    		display.showView(editEarthquakeThreatViewIndex);
	    		break;
	    	case HURRICANE:
	    		display.showView(editHurricaneThreatViewIndex);
	    		break;
	    	case ICE_STORM:
	    		display.showView(editIceStormThreatViewIndex);
	    		break;
	    	default:
	    		display.showView(editNaturalThreatViewIndex);
	    		break;
    	}
    }

}
