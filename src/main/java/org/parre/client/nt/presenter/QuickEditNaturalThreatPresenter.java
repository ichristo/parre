/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.nt.presenter;

import org.parre.client.ClientSingleton;
import org.parre.client.messaging.ServiceFactory;
import org.parre.client.nt.event.NaturalThreatUpdatedEvent;
import org.parre.client.nt.event.QuickEditNaturalThreatEvent;
import org.parre.client.util.*;
import org.parre.shared.NaturalThreatDTO;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.web.bindery.event.shared.EventBus;

/**
 * The Class QuickEditNaturalThreatPresenter.
 */
public class QuickEditNaturalThreatPresenter implements Presenter {
    private EventBus eventBus;
    private Display display;
    private BusyHandler busyHandler;

    /**
     * The Interface Display.
     */
    public interface Display extends ModelView<NaturalThreatDTO> {
        HasClickHandlers getSaveButton();

    }

    public QuickEditNaturalThreatPresenter(Display display) {
        this.eventBus = ClientSingleton.getEventBus();
        this.display = display;
        busyHandler = new BusyHandler(display);
    }

    public void go() {
        bind();

    }

    private void bind() {
        display.getSaveButton().addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                if (!display.validate()) {
                    return;
                }
                ExecutableAsyncCallback<NaturalThreatDTO> exec =
                        new ExecutableAsyncCallback<NaturalThreatDTO>(busyHandler, new BaseAsyncCommand<NaturalThreatDTO>() {
                            public void execute(AsyncCallback<NaturalThreatDTO> callback) {
                                ServiceFactory.getInstance().getNaturalThreatService().
                                        save(display.updateAndGetModel(), callback);
                            }

                            @Override
                            public void handleResult(NaturalThreatDTO result) {
                                eventBus.fireEvent(NaturalThreatUpdatedEvent.create(result));
                            }
                        });
                exec.makeCall();
                display.updateView();
            }
        });
        eventBus.addHandler(QuickEditNaturalThreatEvent.TYPE, new QuickEditNaturalThreatEvent.Handler() {
            public void onEvent(QuickEditNaturalThreatEvent event) {
                NaturalThreatDTO editDTO = event.getNaturalThreatDTO();
                display.setModelAndUpdateView(editDTO);
            }
        });
    }

}
