/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.nt.presenter;

import java.util.List;

import org.parre.client.ClientSingleton;
import org.parre.client.messaging.NaturalThreatServiceAsync;
import org.parre.client.messaging.ServiceFactory;
import org.parre.client.nt.event.EditHurricaneProfileCancelledEvent;
import org.parre.client.nt.event.EditHurricaneProfileEvent;
import org.parre.client.nt.event.HurricaneProfileUpdatedEvent;
import org.parre.client.util.BaseAsyncCommand;
import org.parre.client.util.BusyHandler;
import org.parre.client.util.ExecutableAsyncCallback;
import org.parre.client.util.MessageDialog;
import org.parre.client.util.ModelView;
import org.parre.client.util.Presenter;
import org.parre.shared.HurricaneLookupDataDTO;
import org.parre.shared.HurricaneProfileDTO;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.web.bindery.event.shared.EventBus;

/**
 * The Class EditHurricaneProfilePresenter.
 */
public class EditHurricaneProfilePresenter implements Presenter {
	
	private EventBus eventBus;
	private Display display;
	private NaturalThreatServiceAsync service;
	private BusyHandler busyHandler;
	
	/**
	 * The Interface Display.
	 */
	public interface Display extends ModelView<HurricaneProfileDTO> {
		HasClickHandlers getSaveButton();
        HasClickHandlers getCancelButton();
        void setHurricaneLookupResult(List<HurricaneLookupDataDTO> results);
	}
	
	public EditHurricaneProfilePresenter(Display display) {
    	this.service = ServiceFactory.getInstance().getNaturalThreatService();
    	this.eventBus = ClientSingleton.getEventBus();
    	this.display = display;
    }

	@Override
	public void go() {
		bind();
	}
	
	private void bind() {
		busyHandler = new BusyHandler(display);
		
		eventBus.addHandler(EditHurricaneProfileEvent.TYPE, new EditHurricaneProfileEvent.Handler() {
            public void onEvent(EditHurricaneProfileEvent event) {
                final Long id = event.getProfileId();
                
                if (id == null) {
                	display.setModelAndUpdateView(new HurricaneProfileDTO());
                } else {
	                // Get the Hurricane profile
	                ExecutableAsyncCallback<HurricaneProfileDTO> exec = new ExecutableAsyncCallback<HurricaneProfileDTO>(busyHandler, new BaseAsyncCommand<HurricaneProfileDTO>() {
	                    public void execute(AsyncCallback<HurricaneProfileDTO> asyncCallback) {
	                        service.getHurricaneProfile(id, asyncCallback);
	                    }
	
	                    @Override
	                    public void handleResult(HurricaneProfileDTO result) {
	                        display.setModelAndUpdateView(result);
	                    }
	
	                    @Override
	                    public boolean handleError(Throwable t) {
	                        MessageDialog.popup(t.getMessage(), true);
	                        return true;
	                    }
	                });
	                exec.makeCall();
                }
            }
        });
		display.getSaveButton().addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                if (!(display.validate())) {
                    return;
                }
                if (ClientSingleton.getCurrentParreAnalysis().isLocked()) {
                	MessageDialog.popup("Cannot modify data, this analysis is locked.");
                	return;
                }
                ExecutableAsyncCallback<HurricaneProfileDTO> exec = new ExecutableAsyncCallback<HurricaneProfileDTO>(busyHandler, new BaseAsyncCommand<HurricaneProfileDTO>() {
                    public void execute(AsyncCallback<HurricaneProfileDTO> asyncCallback) {
                        service.saveHurricaneProfile(display.updateAndGetModel(), asyncCallback);
                    }

                    @Override
                    public void handleResult(HurricaneProfileDTO result) {
                    	eventBus.fireEvent(HurricaneProfileUpdatedEvent.create(result));
                    }

                    @Override
                    public boolean handleError(Throwable t) {
                        MessageDialog.popup(t.getMessage(), true);
                        return true;
                    }
                });
                exec.makeCall();
            }
        });
        display.getCancelButton().addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                eventBus.fireEvent(EditHurricaneProfileCancelledEvent.create());
            }
        });
	}
}
