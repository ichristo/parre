/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.nt.presenter;

import org.parre.client.ClientSingleton;
import org.parre.client.nt.event.EditHurricaneProfileEvent;
import org.parre.client.nt.event.EditNaturalThreatEvent;
import org.parre.client.nt.event.HurricaneProfileUpdatedEvent;
import org.parre.client.util.BaseAsyncCommand;
import org.parre.client.util.ClientLoggingUtil;
import org.parre.client.util.ExecutableAsyncCallback;
import org.parre.client.util.MessageDialog;
import org.parre.client.util.ViewUtil;
import org.parre.shared.NaturalThreatCalculationDTO;
import org.parre.shared.ParreAnalysisDTO;
import org.parre.shared.types.NaturalThreatType;

import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;

/**
 * The Class EditHurricaneThreatPresenter.
 */
public class EditHurricaneThreatPresenter extends EditNaturalThreatPresenter {

	public EditHurricaneThreatPresenter(Display display) {
		super(display);
	}

	@Override
	public void go() {
		super.go();
		bind();
	}
	
	private void bind() {
		eventBus.addHandler(EditNaturalThreatEvent.TYPE, new EditNaturalThreatEvent.Handler() {
            public void onEvent(EditNaturalThreatEvent event) {
            	NaturalThreatType threatType = NaturalThreatType.getNaturalThreatType(event.getNaturalThreatDTO().getThreatDescription());
            	ClientLoggingUtil.debug("Received edit request for type: " + threatType);
            	if (NaturalThreatType.HURRICANE == threatType) {
            		ClientLoggingUtil.debug("Inner clause, lastParreAnalysis: " + display.getLastParreAnalysisId() + ", currentParreAnalysisID: " + ClientSingleton.getParreAnalysisId());
            		if (display.getLastParreAnalysisId() == null || display.getLastParreAnalysisId() != ClientSingleton.getParreAnalysisId()) {
            			updateHurricaneProfiles();
            		}
            	}
            }
        });
        
        eventBus.addHandler(HurricaneProfileUpdatedEvent.TYPE, new HurricaneProfileUpdatedEvent.Handler() {
			@Override
			public void onEvent(HurricaneProfileUpdatedEvent event) {
				updateHurricaneProfiles();
			}
        });
		display.getAddProfileButton().addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				eventBus.fireEvent(EditHurricaneProfileEvent.create(null));
			}
		});
		
		display.getEditProfileButton().addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				if (display.getCurrentProfileId() != null) {
					eventBus.fireEvent(EditHurricaneProfileEvent.create(display.getCurrentProfileId()));
				}
			}
		});
        display.getOperationalLoss().addChangeHandler(new ChangeHandler() {
            @Override
            public void onChange(ChangeEvent changeEvent) {
                display.getOperationalLoss().setText(ViewUtil.toDisplayValue(
                        ViewUtil.getBigDecimal(display.getOperationalLoss())));
            }
        });
		
	}

	@Override
	protected void calculate() {
		ExecutableAsyncCallback<NaturalThreatCalculationDTO> exec = new ExecutableAsyncCallback<NaturalThreatCalculationDTO>(busyHandler, new BaseAsyncCommand<NaturalThreatCalculationDTO>() {
            public void execute(AsyncCallback<NaturalThreatCalculationDTO> asyncCallback) {
                service.calculateHurricaneData(display.prepareCalculationDTO(), asyncCallback);
            }

            @Override
            public void handleResult(NaturalThreatCalculationDTO result) {
                display.updateCalculationResults(result);
            }

            @Override
            public boolean handleError(Throwable t) {
                MessageDialog.popup(t.getMessage(), true);
                return true;
            }
        });
        exec.makeCall();
	}
	
	@Override
	protected boolean handlesNaturalThreatType(NaturalThreatType type) {
		return NaturalThreatType.HURRICANE == type;
	}
	
	private void updateHurricaneProfiles() {
		ExecutableAsyncCallback<ParreAnalysisDTO> exec = new ExecutableAsyncCallback<ParreAnalysisDTO>(busyHandler, new BaseAsyncCommand<ParreAnalysisDTO>() {
            public void execute(AsyncCallback<ParreAnalysisDTO> asyncCallback) {
                service.getAnalysisHurricaneProfiles(ClientSingleton.getCurrentParreAnalysis().getId(), asyncCallback);
            }

            @Override
            public void handleResult(ParreAnalysisDTO result) {
            	ClientSingleton.getCurrentParreAnalysis().setHurricaneProfileDTOs(result.getHurricaneProfileDTOs());
            	ClientSingleton.getCurrentParreAnalysis().setDefaultHurricaneProfileDTO(result.getDefaultHurricaneProfileDTO());
            	display.updateProfileList();
                display.updateView();  // to update design speed and profile if overridden
            }

            @Override
            public boolean handleError(Throwable t) {
                MessageDialog.popup(t.getMessage(), true);
                return true;
            }
        });
        exec.makeCall();
	}
}
