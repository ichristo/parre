/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.rra.presenter;

import java.util.List;

import org.parre.client.ClientSingleton;
import org.parre.client.common.presenter.ManageAnalysisPresenter;
import org.parre.client.common.view.AnalysisView;
import org.parre.client.consequence.event.RiskResilienceUpdatedEvent;
import org.parre.client.messaging.RiskResilienceServiceAsync;
import org.parre.client.messaging.ServiceFactory;
import org.parre.client.nav.event.DpNavEvent;
import org.parre.client.nav.event.RiskBenefitNavEvent;
import org.parre.client.rra.event.ManageRraNavEvent;
import org.parre.client.rra.event.ManageUriNavEvent;
import org.parre.client.util.ActionSource;
import org.parre.client.util.BaseAsyncCommand;
import org.parre.client.util.BusyHandler;
import org.parre.client.util.ClientLoggingUtil;
import org.parre.client.util.ExecutableAsyncCallback;
import org.parre.client.util.Presenter;
import org.parre.client.util.nav.NavManager;
import org.parre.shared.RiskResilienceAnalysisDTO;
import org.parre.shared.RiskResilienceDTO;
import org.parre.shared.UnitPriceDTO;
import org.parre.shared.types.AssetThreatAnalysisType;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.view.client.SingleSelectionModel;


/**
 * User: Swarn S. Dhaliwal
 * Date: 8/21/11
 * Time: 5:03 PM
*/
public class ManageRraPresenter extends ManageAnalysisPresenter implements Presenter {

    private RiskResilienceServiceAsync service;
    private BusyHandler busyHandler;
    private RiskResilienceAnalysisDTO currentAnalysis;

    /**
     * The Interface Display.
     */
    public static interface Display extends AnalysisView {

        void setCurrentAnalysis(RiskResilienceAnalysisDTO dto);

        void setData(List<RiskResilienceDTO> resilienceDTOs);

        void refreshAnalysisGrid();

        SingleSelectionModel<RiskResilienceDTO> getSelectionModel();

        ActionSource<RiskResilienceDTO> getSelectionSource();

        List<RiskResilienceDTO> getData();

        HasClickHandlers getManageUriQuestionsButton();

        HasClickHandlers getUpdateUnitPriceButton();

        UnitPriceDTO getUnitPrice();

        void setUnitPrice(UnitPriceDTO unitPrice);

        HasClickHandlers getLockButton();

        void showConfirmLockPopup();

        void hideConfirmLockPopup();

        HasClickHandlers getConfirmLockButton();

        void updateControls();

        void showConfirmUnlockPopup();

        HasClickHandlers getConfirmUnlockButton();

        void hideConfirmUnlockPopup();
    }

    private EventBus eventBus;
    private Display display;

    public ManageRraPresenter(Display display) {
        super(display);
        this.service = ServiceFactory.getInstance().getRiskResilienceService();
        this.eventBus = ClientSingleton.getEventBus();
        this.display = display;
        this.busyHandler = new BusyHandler(display);
    }

    public void go() {
        bind();
        init();
        initRiskResilienceAnalysis();
    }


    private void bind() {
        eventBus.addHandler(ManageRraNavEvent.TYPE, new ManageRraNavEvent.Handler() {
            public void onEvent(ManageRraNavEvent event) {
                initRiskResilienceAnalysis();
            }
        });
        eventBus.addHandler(RiskResilienceUpdatedEvent.TYPE, new RiskResilienceUpdatedEvent.Handler() {
            public void onEvent(RiskResilienceUpdatedEvent event) {
                RiskResilienceDTO dto = event.getRiskResilienceDTO();
                dto.calculateOwnerResilience();
                List<RiskResilienceDTO> data = display.getData();
                int index = data.indexOf(dto);
                data.set(index, dto);
                display.refreshAnalysisGrid();
            }
        });
        display.getLockButton().addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                if (ClientSingleton.getCurrentParreAnalysis().isLocked()) {
                    display.showConfirmUnlockPopup();
                } else {
                    display.showConfirmLockPopup();
                }

            }
        });
        display.getConfirmLockButton().addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                display.hideConfirmLockPopup();
                lockAnalysis();
            }
        });
        display.getConfirmUnlockButton().addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent clickEvent) {
                display.hideConfirmUnlockPopup();
                unlockAnalysis();
            }
        });
        display.getManageUriQuestionsButton().addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                eventBus.fireEvent(ManageUriNavEvent.create(currentAnalysis));
            }
        });
        display.getUpdateUnitPriceButton().addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                updateUnitPrice(display.getUnitPrice());
            }
        });
        display.getPreviousButton().addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent clickEvent) {
                NavManager.performNav(DpNavEvent.create());
            }
        });
        display.getNextButton().addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent clickEvent) {
                NavManager.performNav(RiskBenefitNavEvent.create());
            }
        });
    }

    private void lockAnalysis() {
        new ExecutableAsyncCallback<Void>(busyHandler, new BaseAsyncCommand<Void>() {
            public void execute(AsyncCallback<Void> callback) {
                ServiceFactory.getInstance().getParreService().lockCurrentAnalysis(ClientSingleton.getParreAnalysisId(), callback);

            }

            @Override
            public void handleResult(Void results) {
                ClientSingleton.lockCurrentAnalysis();
                display.updateControls();
            }
        }).makeCall();

    }

    private void unlockAnalysis() {

        new ExecutableAsyncCallback<Void>(busyHandler, new BaseAsyncCommand<Void>() {
            public void execute(AsyncCallback<Void> callback) {
                ServiceFactory.getInstance().getParreService().unlockCurrentAnalysis(ClientSingleton.getParreAnalysisId(), callback);

            }

            @Override
            public void handleResult(Void results) {
                ClientSingleton.unlockCurrentAnalysis();
                display.updateControls();
            }
        }).makeCall();

    }

    private void updateUnitPrice(final UnitPriceDTO unitPrice) {

        new ExecutableAsyncCallback<Void>(busyHandler, new BaseAsyncCommand<Void>() {
            public void execute(AsyncCallback<Void> callback) {
                ServiceFactory.getInstance().getRiskResilienceService().updateUnitPrice(currentAnalysis.getId(), unitPrice, callback);
            }

            @Override
            public void handleResult(Void results) {
                updateCalculatedValues(unitPrice);
            }
        }).makeCall();

    }

    private void updateCalculatedValues(UnitPriceDTO unitPrice) {
        currentAnalysis.setUnitPriceDTO(unitPrice);
        List<RiskResilienceDTO> data = display.getData();
        for (RiskResilienceDTO riskResilienceDTO : data) {
            riskResilienceDTO.setUnitPriceDTO(unitPrice);
            riskResilienceDTO.calculateOwnerResilience();
        }
        display.refreshAnalysisGrid();
    }

    @Override
    protected AssetThreatAnalysisType getAnalysisType() {
        return AssetThreatAnalysisType.RISK_RESILIENCE;
    }

    @Override
    protected void startAnalysis() {

    }

    @Override
    protected void getCurrentAnalysis() {

    }

    private void initRiskResilienceAnalysis() {
        ClientLoggingUtil.info("ParreAnalysisId = " + ClientSingleton.getParreAnalysisId());
        ExecutableAsyncCallback<RiskResilienceAnalysisDTO> exec =
                new ExecutableAsyncCallback<RiskResilienceAnalysisDTO>(busyHandler, new BaseAsyncCommand<RiskResilienceAnalysisDTO>() {
                    public void execute(AsyncCallback<RiskResilienceAnalysisDTO> callback) {
                        service.getAnalysis(ClientSingleton.getParreAnalysisId(), callback);
                    }

                    @Override
                    public void handleResult(RiskResilienceAnalysisDTO results) {
                        initAnalysis(results);
                    }
                });
        exec.makeCall();

    }

    private void initAnalysis(RiskResilienceAnalysisDTO dto) {
        this.currentAnalysis = dto;
        display.setCurrentAnalysis(dto);
        if (dto != null) {
            getRiskResilienceDTOs();
        }

    }

    private void getRiskResilienceDTOs() {
        ExecutableAsyncCallback<List<RiskResilienceDTO>> exec =
                new ExecutableAsyncCallback<List<RiskResilienceDTO>>(busyHandler, new BaseAsyncCommand<List<RiskResilienceDTO>>() {
                    public void execute(AsyncCallback<List<RiskResilienceDTO>> callback) {
                        ServiceFactory.getInstance().getRiskResilienceService().getRiskResilienceDTOs(currentAnalysis.getId(), ClientSingleton.getParreAnalysisId(), callback);
                    }

                    @Override
                    public void handleResult(List<RiskResilienceDTO> results) {
                        for(RiskResilienceDTO result : results) {
                            if(result.getThreatName().equals("D(S)")) {
                                ClientLoggingUtil.info("Risk = " + result.getRisk());
                            }
                        }
                        display.setData(results);
                    }
                });
        exec.makeCall();

    }
}
