/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.rra.presenter;

import org.parre.client.ClientSingleton;
import org.parre.client.messaging.ServiceFactory;
import org.parre.client.rra.event.EditUriResponseEvent;
import org.parre.client.rra.event.UriResponseUpdatedEvent;
import org.parre.client.util.*;
import org.parre.shared.UriResponseDTO;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.web.bindery.event.shared.EventBus;

/**
 * The Class EditUriResponsePresenter.
 */
public class EditUriResponsePresenter implements Presenter {
    private EventBus eventBus;
    private Display display;
    private BusyHandler busyHandler;

    /**
     * The Interface Display.
     */
    public interface Display extends ModelView<UriResponseDTO> {

        HasClickHandlers getSaveButton();

    }

    public EditUriResponsePresenter(Display display) {
        this.eventBus = ClientSingleton.getEventBus();
        this.display = display;
        this.busyHandler = new BusyHandler(display);
    }

    public void go() {
        bind();

    }

    private void bind() {
        display.getSaveButton().addClickHandler(new ValidatingClickHandler(display) {
            @Override
            protected void handleClick(ClickEvent event) {
                eventBus.fireEvent(UriResponseUpdatedEvent.create(display.updateAndGetModel()));
            }
        });
        eventBus.addHandler(EditUriResponseEvent.TYPE, new EditUriResponseEvent.Handler() {
            public void onEvent(EditUriResponseEvent event) {
                UriResponseDTO editDTO = event.getUriResponseDTO();
                display.setModelAndUpdateView(editDTO);
            }
        });
    }

}
