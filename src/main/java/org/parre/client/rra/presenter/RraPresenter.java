/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.rra.presenter;

import org.parre.client.ClientSingleton;
import org.parre.client.ViewFactory;
import org.parre.client.messaging.RiskResilienceServiceAsync;
import org.parre.client.messaging.ServiceFactory;
import org.parre.client.nav.event.RiskResilienceNavEvent;
import org.parre.client.rra.event.ManageRraDisplayEvent;
import org.parre.client.rra.event.ManageRraNavEvent;
import org.parre.client.rra.event.ManageUriNavEvent;
import org.parre.client.util.Presenter;
import org.parre.client.util.View;
import org.parre.client.util.ViewContainer;

import com.google.gwt.event.shared.EventBus;

/**
 * The Class RraPresenter.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 7/19/11
 */
public class RraPresenter implements Presenter {
    private Integer manageRraViewIndex;
    private Integer manageUriViewIndex;
    private RiskResilienceServiceAsync service;
    private EventBus eventBus;
    private Display display;

    /**
     * The Interface Display.
     */
    public interface Display extends View, ViewContainer {

    }

    public RraPresenter(Display display) {
        this.service = ServiceFactory.getInstance().getRiskResilienceService();
        this.eventBus = ClientSingleton.getEventBus();
        this.display = display;
    }

    public void go() {
        initViewLoaders();
        bind();
        display.showView(manageRraViewIndex);
    }

    private void initViewLoaders() {
        View view;
        view = ViewFactory.createManageRraView();
        view.getPresenter().go();
        manageRraViewIndex = display.addView(view.asWidget());

        view = ViewFactory.createManageUriView();
        view.getPresenter().go();
        manageUriViewIndex = display.addView(view.asWidget());
    }

    private void bind() {
        eventBus.addHandler(RiskResilienceNavEvent.TYPE, new RiskResilienceNavEvent.Handler() {
            public void onEvent(RiskResilienceNavEvent event) {
                display.showView(manageRraViewIndex);
                eventBus.fireEvent(ManageRraNavEvent.create());
            }
        });
        eventBus.addHandler(ManageRraNavEvent.TYPE, new ManageRraNavEvent.Handler() {
            public void onEvent(ManageRraNavEvent event) {
                display.showView(manageRraViewIndex);
            }
        });

        eventBus.addHandler(ManageRraDisplayEvent.TYPE, new ManageRraDisplayEvent.Handler() {
            public void onEvent(ManageRraDisplayEvent event) {
                display.showView(manageRraViewIndex);
            }
        });

        eventBus.addHandler(ManageUriNavEvent.TYPE, new ManageUriNavEvent.Handler() {
            public void onEvent(ManageUriNavEvent event) {
                display.showView(manageUriViewIndex);
            }
        });
    }

}
