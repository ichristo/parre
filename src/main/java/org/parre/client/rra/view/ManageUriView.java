/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.rra.view;

import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.user.client.ui.*;
import com.google.gwt.view.client.SingleSelectionModel;

import java.math.BigDecimal;
import java.util.List;

import org.parre.client.rra.presenter.ManageUriPresenter;
import org.parre.client.util.*;
import org.parre.client.util.dataview.DataProvider;
import org.parre.client.util.dataview.DataProviderFactory;
import org.parre.shared.RiskResilienceAnalysisDTO;
import org.parre.shared.UriResponseDTO;

/**
 * The Class ManageUriView.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 7/16/11
 */
public class ManageUriView extends BaseView implements ManageUriPresenter.Display {
    private Widget viewWidget;
    private UriResponseGrid friResponseGrid;
    private UriResponseGrid oriResponseGrid;
    private Widget uriQuestionsView;
    private Button updateFriResponsesButton;
    private Button updateOriResponsesButton;
    private ListBox uriQuestions = new ListBox();
    private DialogBox editPopup;
    private EditUriResponseView editUriResponseView;
    private Button selectQuestionButton;
    private DialogBox uriQuestionsPopup;
    private Button backButton;
    private HTML calculatedUri;
    private HTML friNeedsSavingLabel;
    private HTML oriNeedsSavingLabel;

    public ManageUriView(EditUriResponseView editUriResponseView) {
        this.editUriResponseView = editUriResponseView;
        viewWidget = createView();
    }

    private Widget createView() {
        Label header = new Label("Risk/Resilience");
        header.setStyleName("inputPanelHeader");
        uriQuestionsView = createUriQuestionsView();
        VerticalPanel viewPanel = new VerticalPanel();
        viewPanel.setStyleName("inputPanel");
        viewPanel.add(header);
        viewPanel.add(uriQuestionsView);
        initEditPopup();
        initUriQuestionsPopup();
        return viewPanel;
    }

    private void initUriQuestionsPopup() {
        uriQuestionsPopup = new ClosablePopup("Select Question", true);
        uriQuestionsPopup.setModal(true);
        VerticalPanel viewPanel = new VerticalPanel();
        viewPanel.setStyleName("inputPanel");
        viewPanel.add(uriQuestions);
        selectQuestionButton = ViewUtil.createButton("Go", "Select this question");
        viewPanel.add(ViewUtil.layoutCenter(selectQuestionButton));
        uriQuestionsPopup.setWidget(viewPanel);
    }

    private void initEditPopup() {
        editPopup = new ClosablePopup("Edit", true);
        editPopup.setModal(true);
        editPopup.setWidth("800px");
        editPopup.setWidget(editUriResponseView.asWidget());
    }

    private Widget createUriQuestionsView() {
        backButton = ViewUtil.createButton("Back");
        VerticalPanel responseView = new VerticalPanel();
        //responseView.setSpacing(2);
        responseView.setStyleName("inputPanel");
        FlexTable headerTable = new FlexTable();
        headerTable.setWidth("100%");
        headerTable.setWidget(0, 0, backButton);
        HTML label= new HTML("Calculated URI : ");
        label.setStyleName("normalLabel");
        calculatedUri = new HTML("");
        calculatedUri.setStyleName("normalLabel");
        headerTable.setWidget(0, 1, ViewUtil.horizontalPanel(5, label, calculatedUri));
        responseView.add(headerTable);
        DataProvider<UriResponseDTO> dataProvider;
        UriResponseGrid responseGrid;
        dataProvider = DataProviderFactory.createDataProvider();
        responseGrid = new UriResponseGrid(dataProvider);
        responseGrid.setHeight("200px");
        updateFriResponsesButton = ViewUtil.createButton("Update");
        friNeedsSavingLabel = createHaveChangedIndicator();
        responseView.add(ViewUtil.layoutCenter(ViewUtil.createInputLabel("Financial Resilience Index"),
                updateFriResponsesButton, friNeedsSavingLabel));
        responseView.add(responseGrid.asWidget());
        friResponseGrid = responseGrid;

        dataProvider = DataProviderFactory.createDataProvider();
        responseGrid = new UriResponseGrid(dataProvider);
        responseGrid.setHeight("400px");
        updateOriResponsesButton = ViewUtil.createButton("Update");
        oriNeedsSavingLabel = createHaveChangedIndicator();
        responseView.add(ViewUtil.layoutCenter(ViewUtil.createInputLabel("Operational Resilience Index"),
                updateOriResponsesButton, oriNeedsSavingLabel));
        responseView.add(responseGrid.asWidget());
        oriResponseGrid = responseGrid;
        return responseView;
    }

    public static HTML createHaveChangedIndicator() {
        HTML html = new HTML("*");
        html.setStyleName("haveChanges");
        ToolTipManager.addToolTip(html, "Have unsaved changes");
        html.setVisible(false);
        return html;
    }

    public void setCurrentAnalysis(RiskResilienceAnalysisDTO dto) {
        boolean haveAnalysis = dto != null;
        friResponseGrid.asWidget().setVisible(haveAnalysis);
    }

    public void setFriResponses(List<UriResponseDTO> responseDTOs) {
        updateFriResponsesButton.setEnabled(false);
        friResponseGrid.setData(responseDTOs);
        refreshFriResponseGrid();
    }

    public void refreshFriResponseGrid() {
        friResponseGrid.refresh();
    }

    public void setOriResponses(List<UriResponseDTO> responseDTOs) {
        updateOriResponsesButton.setEnabled(false);
        oriResponseGrid.setData(responseDTOs);
        refreshOriResponseGrid();
    }

    public void refreshOriResponseGrid() {
        oriResponseGrid.refresh();
    }

    public void setFriNeedsSaving(boolean value) {
        friNeedsSavingLabel.setVisible(value);
        updateFriResponsesButton.click();
    }

    public void setOriNeedsSaving(boolean value) {
        oriNeedsSavingLabel.setVisible(value);
        updateOriResponsesButton.click();
    }

    public void updateFriResponse(UriResponseDTO uriResponseDTO) {
        friResponseGrid.updateData(uriResponseDTO);
        setFriNeedsSaving(true);
    }

    public void updateOriResponse(UriResponseDTO uriResponseDTO) {
        oriResponseGrid.updateData(uriResponseDTO);
        setOriNeedsSaving(true);
    }

    public void setUpdateFriEnabled(boolean enabled) {
        updateFriResponsesButton.setEnabled(enabled);
    }

    public void setUpdateOriEnabled(boolean enabled) {
        updateOriResponsesButton.setEnabled(enabled);
    }

    public HasClickHandlers getBackButton() {
        return backButton;
    }

    public HasClickHandlers getUpdateFriResponsesButton() {
        return updateFriResponsesButton;
    }

    public HasClickHandlers getUpdateOriResponsesButton() {
        return updateOriResponsesButton;
    }

    public void setCalculatedUri(BigDecimal uriValue) {
        calculatedUri.setText(ViewUtil.toDisplayUriValue(uriValue));
    }

    public HasClickHandlers getSelectQuestionButton() {
        return selectQuestionButton;
    }

    public void showUriQuestionPopup() {
        uriQuestionsPopup.center();
    }

    public void hideUriQuestionsPopup() {
        uriQuestionsPopup.hide();
    }

    public ListBox getUriQuestions() {
        return uriQuestions;
    }

    public SingleSelectionModel<UriResponseDTO> getFriSelectionModel() {
        return friResponseGrid.getSelectionModel();
    }

    public ActionSource<UriResponseDTO> getFriSelectionSource() {
        return friResponseGrid.getSelectionSource();
    }

    public SingleSelectionModel<UriResponseDTO> getOriSelectionModel() {
        return oriResponseGrid.getSelectionModel();
    }

    public ActionSource<UriResponseDTO> getOriSelectionSource() {
        return oriResponseGrid.getSelectionSource();
    }

    public void showEditPopup() {
        editPopup.center();
    }

    public void hideEditPopup() {
        editPopup.hide();
    }

    public List<UriResponseDTO> getFriData() {
        return friResponseGrid.getData();
    }

    public List<UriResponseDTO> getOriData() {
        return oriResponseGrid.getData();
    }

    public Widget asWidget() {
        return viewWidget;
    }
}
