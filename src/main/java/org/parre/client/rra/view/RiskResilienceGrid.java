/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.rra.view;

import com.google.gwt.user.cellview.client.Column;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import org.parre.client.util.DoubleClickableTextCell;
import org.parre.client.util.ViewUtil;
import org.parre.client.util.dataview.DataColumn;
import org.parre.client.util.dataview.DataGridHeaders;
import org.parre.client.util.dataview.DataGridView;
import org.parre.client.util.dataview.DataProvider;
import org.parre.shared.RiskResilienceDTO;
import org.parre.shared.SharedCalculationUtil;

/**
 * The Class RiskResilienceGrid.
 */
public class RiskResilienceGrid extends DataGridView<RiskResilienceDTO> {
    public RiskResilienceGrid(DataProvider<RiskResilienceDTO> dataProvider) {
		super(dataProvider);
		List<DataColumn<RiskResilienceDTO>> columnList = new ArrayList<DataColumn<RiskResilienceDTO>>();
		columnList.add(createAssetColumn());
        columnList.add(createThreatColumn());
        columnList.add(createFatalitiesColumn());
        columnList.add(createSeriousInjuriesColumn());
        columnList.add(createOwnersFinancialImpactColumn());
        columnList.add(createFinancialTotalColumn());
        columnList.add(createVulnerabilityColumn());
        columnList.add(createLotColumn());
        columnList.add(createRiskColumn());
        columnList.add(createEconomicResilienceColumn());
        columnList.add(createFinancialResilienceColumn());

        addColumns(columnList);
	}

    private DataColumn<RiskResilienceDTO> createAssetColumn() {
        Column<RiskResilienceDTO, String> column = new Column<RiskResilienceDTO, String>(
                createDoubleClickableTextCell()) {
            @Override
            public String getValue(RiskResilienceDTO object) {
                return object.getAssetId() + " - " + object.getAssetName();
            }
        };
        column.setSortable(true);

        Comparator<RiskResilienceDTO> comparator = new Comparator<RiskResilienceDTO>() {
            public int compare(RiskResilienceDTO o1, RiskResilienceDTO o2) {
                return o1.getAssetId().compareTo(o2.getAssetId());
            }
        };
        return new DataColumn<RiskResilienceDTO>("Asset", column, comparator, 10);
    }

    private DataColumn<RiskResilienceDTO> createThreatColumn() {
        Column<RiskResilienceDTO, String> column = new Column<RiskResilienceDTO, String>(
                createDoubleClickableTextCell()) {
            @Override
            public String getValue(RiskResilienceDTO object) {
                return object.getThreatName() + " - " + object.getThreatDescription();
            }
        };
        column.setSortable(true);

        Comparator<RiskResilienceDTO> comparator = new Comparator<RiskResilienceDTO>() {
            public int compare(RiskResilienceDTO o1, RiskResilienceDTO o2) {
                return o1.getThreatName().compareTo(o2.getThreatName());
            }
        };
        return new DataColumn<RiskResilienceDTO>("Threat", column, comparator, 10);
    }


    private DataColumn<RiskResilienceDTO> createFatalitiesColumn() {
        Column<RiskResilienceDTO, String> column = new Column<RiskResilienceDTO, String>(
                createDoubleClickableTextCell()) {
            @Override
            public String getValue(RiskResilienceDTO riskResilienceDTO) {
                return ViewUtil.toDisplayValue(riskResilienceDTO.getFatalities());
            }
        };
        column.setSortable(true);

        Comparator<RiskResilienceDTO> comparator = new Comparator<RiskResilienceDTO>() {
            @Override
            public int compare(RiskResilienceDTO o1, RiskResilienceDTO o2) {
                return o1.getFatalities().compareTo(o2.getFatalities());
            }
        };
        return new DataColumn<RiskResilienceDTO>("Fatalities", column, comparator, 10);
    }

    private DataColumn<RiskResilienceDTO> createSeriousInjuriesColumn() {
        Column<RiskResilienceDTO, String> column = new Column<RiskResilienceDTO, String>(
                createDoubleClickableTextCell()) {
            @Override
            public String getValue(RiskResilienceDTO riskResilienceDTO) {
                return ViewUtil.toDisplayValue(riskResilienceDTO.getSeriousInjuries());
            }
        };
        column.setSortable(true);

        Comparator<RiskResilienceDTO> comparator = new Comparator<RiskResilienceDTO>() {
            @Override
            public int compare(RiskResilienceDTO o1, RiskResilienceDTO o2) {
                return o1.getSeriousInjuries().compareTo(o2.getSeriousInjuries());
            }
        };
        return new DataColumn<RiskResilienceDTO>(DataGridHeaders.seriousInjuries(), column, comparator, 10);
    }


    private DataColumn<RiskResilienceDTO> createVulnerabilityColumn() {
    	Column<RiskResilienceDTO, String> column = new Column<RiskResilienceDTO, String>(
    			createDoubleClickableTextCell()) {
    		@Override
    		public String getValue(RiskResilienceDTO object) {
    			return ViewUtil.toDisplayValue(object.getVulnerability());
    		}
    	};
    	column.setSortable(true);

    	Comparator<RiskResilienceDTO> comparator = new Comparator<RiskResilienceDTO>() {
    		public int compare(RiskResilienceDTO o1, RiskResilienceDTO o2) {
    			return SharedCalculationUtil.compare(o1.getVulnerability(), o2.getVulnerability());
                //return o1.getVulnerability().compareTo(o2.getVulnerability());
    		}
    	};
    	return new DataColumn<RiskResilienceDTO>("Vulnerability", column, comparator, 10);
    }

    private DataColumn<RiskResilienceDTO> createLotColumn() {
    	Column<RiskResilienceDTO, String> column = new Column<RiskResilienceDTO, String>(
    			createDoubleClickableTextCell()) {
    		@Override
    		public String getValue(RiskResilienceDTO object) {
    			return ViewUtil.toDisplayValue(object.getLot());
    		}
    	};
    	column.setSortable(true);

    	Comparator<RiskResilienceDTO> comparator = new Comparator<RiskResilienceDTO>() {
    		public int compare(RiskResilienceDTO o1, RiskResilienceDTO o2) {
    			return SharedCalculationUtil.compare(o1.getLot(), o2.getLot());
    		}
    	};
    	return new DataColumn<RiskResilienceDTO>(DataGridHeaders.threatLikelihood(), column, comparator, 10);
    }

    private DataColumn<RiskResilienceDTO> createFinancialTotalColumn() {
    	Column<RiskResilienceDTO, String> column = new Column<RiskResilienceDTO, String>(
    			createDoubleClickableTextCell()) {
    		@Override
    		public String getValue(RiskResilienceDTO object) {
    			return ViewUtil.toDisplayValue(object.getFinancialTotalQuantity());
    		}
    	};
    	column.setSortable(true);

    	Comparator<RiskResilienceDTO> comparator = new Comparator<RiskResilienceDTO>() {
    		public int compare(RiskResilienceDTO o1, RiskResilienceDTO o2) {
    			return SharedCalculationUtil.compare(o1.getFinancialTotalQuantity(), o2.getFinancialTotalQuantity());
    		}
    	};
        return new DataColumn<RiskResilienceDTO>(DataGridHeaders.financialTotal(), column, comparator, 10);
    }

    private DataColumn<RiskResilienceDTO> createRiskColumn() {
    	Column<RiskResilienceDTO, String> column = new Column<RiskResilienceDTO, String>(
    			createDoubleClickableTextCell()) {
    		@Override
    		public String getValue(RiskResilienceDTO object) {
    			return ViewUtil.displayFormatForRisk(object.getRisk());
    		}
    	};
    	column.setSortable(true);

    	Comparator<RiskResilienceDTO> comparator = new Comparator<RiskResilienceDTO>() {
    		public int compare(RiskResilienceDTO o1, RiskResilienceDTO o2) {
    			return SharedCalculationUtil.compare(o1.getRisk(), o2.getRisk());
    		}
    	};
        return new DataColumn<RiskResilienceDTO>("Risk($)", column, comparator, 10);
    }

    private DataColumn<RiskResilienceDTO> createEconomicResilienceColumn() {
    	Column<RiskResilienceDTO, String> column = new Column<RiskResilienceDTO, String>(
    			createDoubleClickableTextCell()) {
    		@Override
    		public String getValue(RiskResilienceDTO object) {
    			return ViewUtil.toDisplayValue(object.getEconomicResilienceQuantity());
    		}
    	};
    	column.setSortable(true);

    	Comparator<RiskResilienceDTO> comparator = new Comparator<RiskResilienceDTO>() {
    		public int compare(RiskResilienceDTO o1, RiskResilienceDTO o2) {
    			return SharedCalculationUtil.compare(o1.getEconomicResilienceQuantity(), o2.getEconomicResilienceQuantity());
    		}
    	};
        return new DataColumn<RiskResilienceDTO>(DataGridHeaders.economicResilience(), column, comparator, 15);
    }

    private DataColumn<RiskResilienceDTO> createFinancialResilienceColumn() {
    	Column<RiskResilienceDTO, String> column = new Column<RiskResilienceDTO, String>(
    			createDoubleClickableTextCell()) {
    		@Override
    		public String getValue(RiskResilienceDTO object) {
    			return ViewUtil.toDisplayValue(object.getOwnerResilienceQuantity());
    		}
    	};
    	column.setSortable(true);

    	Comparator<RiskResilienceDTO> comparator = new Comparator<RiskResilienceDTO>() {
    		public int compare(RiskResilienceDTO o1, RiskResilienceDTO o2) {
    			return SharedCalculationUtil.compare(o1.getOwnerResilienceQuantity(), o2.getOwnerResilienceQuantity());
    		}
    	};

        return new DataColumn<RiskResilienceDTO>(DataGridHeaders.financialResilience(), column, comparator, 15);
    }

    private DoubleClickableTextCell<RiskResilienceDTO> createDoubleClickableTextCell() {
        return new DoubleClickableTextCell<RiskResilienceDTO>(getSelectionModel(), getSelectionSource());
    }

    private DataColumn<RiskResilienceDTO> createOwnersFinancialImpactColumn() {
        Column<RiskResilienceDTO, String> column = new Column<RiskResilienceDTO, String>(createDoubleClickableTextCell()) {

            @Override
            public String getValue(RiskResilienceDTO riskResilienceDTO) {
                return ViewUtil.toDisplayValue(riskResilienceDTO.getOwnersFinancialImpact());
            }
        };
        column.setSortable(true);
        Comparator<RiskResilienceDTO> comparator = new Comparator<RiskResilienceDTO>() {
            @Override
            public int compare(RiskResilienceDTO riskResilienceDTO, RiskResilienceDTO riskResilienceDTO1) {
                return SharedCalculationUtil.compare(riskResilienceDTO.getOwnersFinancialImpact(),
                                                     riskResilienceDTO1.getOwnersFinancialImpact());
            }
        };
        return new DataColumn<RiskResilienceDTO>(DataGridHeaders.financialImpact(), column, comparator, 10);
    }


}
