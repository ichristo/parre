/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.rra.view;

import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.user.client.ui.*;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.parre.client.rra.presenter.EditUriResponsePresenter;
import org.parre.client.util.*;
import org.parre.shared.UriOptionDTO;
import org.parre.shared.UriQuestionDTO;
import org.parre.shared.UriResponseDTO;

/**
 * The Class EditUriResponseView.
 */
public class EditUriResponseView extends BaseModelView<UriResponseDTO>  implements EditUriResponsePresenter.Display {
	private List<FieldValidationHandler> requiredFieldHandlers;
	private Widget viewWidget;
    private Button saveButton;
    private HTML questionType;
    private HTML questionName;
    private ListBox uriOptions;
    private TextBox optionValue;
    private TextBox optionWeight;
    private HTML questionDescription;


    public EditUriResponseView() {
		requiredFieldHandlers = new ArrayList<FieldValidationHandler>();
        viewWidget = buildView();
		
	}

	private Widget buildView() {
        FlexTable inputTable = new FlexTable();
        inputTable.setWidth("100%");
        int rowNum = -1;
        questionType = new HTML();
        ViewUtil.addFormEntry(inputTable, ++rowNum, "Question Type : ", questionType);
        ViewUtil.makeWhiteSpaceNormal(questionType);
        questionName = new HTML();
        ViewUtil.addFormEntry(inputTable, ++rowNum, "Question Name : ", questionName);
        ViewUtil.makeWhiteSpaceNormal(questionName);
        questionDescription = new HTML();
        ViewUtil.addFormEntry(inputTable, ++rowNum, "Question Description : ", questionDescription);
        ViewUtil.makeWhiteSpaceNormal(questionDescription);
        uriOptions = ViewUtil.addListBoxFormEntry(inputTable, ++rowNum, "Option", requiredFieldHandlers);
        optionValue = ViewUtil.addTextBoxFormEntry(inputTable, ++rowNum, "Option Value", requiredFieldHandlers, ProbabilityValueValidator.INSTANCE);
        optionWeight = ViewUtil.addTextBoxFormEntry(inputTable, ++rowNum, "Option Weight", requiredFieldHandlers);

        Label header = new Label("Edit URI Question");
        header.setStyleName("inputPanelHeader");
        VerticalPanel viewPanel = new VerticalPanel();
        viewPanel.setStyleName("inputPanel");
        viewPanel.add(header);
        viewPanel.add(inputTable);
        saveButton = ViewUtil.createButton("Save");
        viewPanel.add(ViewUtil.layoutCenter(saveButton));
        uriOptions.addChangeHandler(new ChangeHandler() {
            public void onChange(ChangeEvent event) {
                Long selectedId = getSelectedId(uriOptions);
                List<UriOptionDTO> optionDTOs = getModel().getUriQuestionDTO().getOptionDTOs();
                for (UriOptionDTO optionDTO : optionDTOs) {
                    if (optionDTO.hasId(selectedId)) {
                        setBigDecimal(optionValue, optionDTO.getValue());
                        setBigDecimal(optionWeight, optionDTO.getWeight());
                        break;
                    }
                }
            }
        });
        return viewPanel;

    }


    public boolean validate() {
		return ViewUtil.validate(requiredFieldHandlers);
	}

	public void updateModel() {
        UriResponseDTO model = getModel();
        model.setUriOptionId(getSelectedId(uriOptions));
        model.setValue(getBigDecimal(optionValue));
        model.setWeight(getBigDecimal(optionWeight));
        model.populateReferenceValues();
    }

	public void updateView() {
        UriResponseDTO model = getModel();
        UriQuestionDTO uriQuestionDTO = model.getUriQuestionDTO();
        setText(questionType, uriQuestionDTO.getType().getValue());
        setText(questionName, uriQuestionDTO.getName());
        setText(questionDescription, uriQuestionDTO.getDescription());
        setBigDecimal(optionValue, model.getValue());
        setBigDecimal(optionWeight, model.getWeight());
        uriOptions.clear();
        uriOptions.addItem("Select", "");
        List<UriOptionDTO> optionDTOs = uriQuestionDTO.getOptionDTOs();
        UriOptionDTO temp;
        while(!correctOrder(optionDTOs)) {
            for(int i = 0; i<4; i++) {
                if(optionDTOs.get(i).getValue().doubleValue() > optionDTOs.get(i+1).getValue().doubleValue()) {
                    temp = optionDTOs.get(i);
                    optionDTOs.set(i, optionDTOs.get(i+1));
                    optionDTOs.set(i+1, temp);
                }
            }
        }

        for (UriOptionDTO optionDTO : optionDTOs) {
            uriOptions.addItem(optionDTO.getName(), String.valueOf(optionDTO.getId()));
            boolean selected = optionDTO.hasId(model.getUriOptionId());
            if (selected) {
                ClientLoggingUtil.info("Goes Here");
                uriOptions.setSelectedIndex(uriOptions.getItemCount() - 1);
            }
        }
    }

	public Widget asWidget() {
		return viewWidget;
	}

    public HasClickHandlers getSaveButton() {
        return saveButton;
    }

    private boolean correctOrder(List<UriOptionDTO> list) {
        for(int i = 0; i<list.size() - 1; i++) {
            if(list.get(i).getValue().doubleValue() > list.get(i+1).getValue().doubleValue()) {
                return false;
            }
        }
        return true;
    }
}
