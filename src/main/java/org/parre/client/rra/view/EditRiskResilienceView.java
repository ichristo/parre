/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.rra.view;

import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;

import java.util.ArrayList;
import java.util.List;

import org.parre.client.rra.presenter.EditRiskResiliencePresenter;
import org.parre.client.util.*;
import org.parre.shared.RiskResilienceDTO;

/**
 * The Class EditRiskResilienceView.
 */
public class EditRiskResilienceView extends BaseModelView<RiskResilienceDTO>  implements EditRiskResiliencePresenter.Display {
	private List<FieldValidationHandler> requiredFieldHandlers;
	private Widget viewWidget;
    private TextBox assetId;
    private TextBox threatName;
	private TextBox durationDays;
    private TextBox severityMgd;
    private Button saveButton;

	
	
	public EditRiskResilienceView() {
		requiredFieldHandlers = new ArrayList<FieldValidationHandler>();
        viewWidget = buildView();
		
	}

	private Widget buildView() {
        FlexTable inputTable = new FlexTable();
        inputTable.setCellSpacing(5);

        int colNum = -1;

        assetId = ViewUtil.addTextBoxWithHeader(inputTable, 0, "Asset", ++colNum);
        assetId.setWidth("50px");
        assetId.setEnabled(false);

        threatName = ViewUtil.addTextBoxWithHeader(inputTable, 0, "Threat", ++colNum);
        threatName.setWidth("150px");
        threatName.setEnabled(false);
        /*FlexTable.FlexCellFormatter formatter = inputTable.getFlexCellFormatter();
        inputTable.setHTML(0, ++colNum, "Duration(Days)");
        formatter.getElement(0, colNum).getStyle().setProperty("whiteSpace", "nowrap");  */
        durationDays = ViewUtil.addTextBoxWithHeader(inputTable, 0, "Duration (Days)", ++colNum, requiredFieldHandlers, PositiveNumberValidator.INSTANCE);
        durationDays.setWidth("50px");

        /*inputTable.setHTML(0, ++colNum, "Severity(MGD)");
        formatter.getElement(0, colNum).getStyle().setProperty("whiteSpace", "nowrap"); */
        severityMgd = ViewUtil.addTextBoxWithHeader(inputTable, 0, "Severity (MGD)", ++colNum, requiredFieldHandlers, PositiveNumberValidator.INSTANCE);
        severityMgd.setWidth("50px");

        saveButton = ViewUtil.createButton("Save");
        inputTable.setWidget(1, ++colNum, saveButton);
        inputTable.setStyleName("inputPanel");
        return inputTable;

    }
	

	public boolean validate() {
		return ViewUtil.validate(requiredFieldHandlers);
	}

	public void updateModel() {
        RiskResilienceDTO model = getModel();
        //model.setDurationDays(ViewUtil.getBigDecimal(durationDays));
        //model.setSeverityMgd(ViewUtil.getBigDecimal(severityMgd));
    }

	public void updateView() {
        RiskResilienceDTO model = getModel();
        ViewUtil.setText(assetId, model.getAssetId());
        ViewUtil.setText(threatName, model.getThreatName());
        //setBigDecimal(durationDays, model.getDurationDays());
        //setBigDecimal(severityMgd, model.getSeverityMgd());
	}

	public Widget asWidget() {
		return viewWidget;
	}

    public HasClickHandlers getSaveButton() {
        return saveButton;
    }
}
