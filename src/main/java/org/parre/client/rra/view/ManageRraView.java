/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.rra.view;

import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.user.client.ui.*;
import com.google.gwt.view.client.SelectionChangeEvent;
import com.google.gwt.view.client.SingleSelectionModel;

import java.util.List;

import org.parre.client.ClientSingleton;
import org.parre.client.common.view.AnalysisCommandPanel;
import org.parre.client.common.view.BaseAnalysisView;
import org.parre.client.common.view.NameDescriptionView;
import org.parre.client.rra.presenter.ManageRraPresenter;
import org.parre.client.util.ActionSource;
import org.parre.client.util.ClosablePopup;
import org.parre.client.util.ToolTipManager;
import org.parre.client.util.ViewUtil;
import org.parre.client.util.dataview.DataProvider;
import org.parre.client.util.dataview.DataProviderFactory;
import org.parre.shared.ParreAnalysisDTO;
import org.parre.shared.RiskResilienceAnalysisDTO;
import org.parre.shared.RiskResilienceDTO;
import org.parre.shared.UnitPriceDTO;

/**
 * The Class ManageRraView.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 7/16/11
 */
public class ManageRraView extends BaseAnalysisView implements ManageRraPresenter.Display {
    private Widget viewWidget;
    private TextBox unitPricePkg;
    private Widget riskResilienceView;
    private RiskResilienceGrid riskResilienceGrid;
    private Label assetThreat;
    private DialogBox editPopup;
    private EditRiskResilienceView editRiskResilienceView;
    private Button manageUriQuestionsButton;
    private Button updateUnitPriceButton;
    private Widget headerView;
    private Button lockButton;
    private Button confirmLockButton;
    private DialogBox confirmLockPopup;
    private DialogBox confirmUnlockPopup;
    private Button confirmUnlockButton;


    public ManageRraView(EditRiskResilienceView editRiskResilienceView) {
        super(new AnalysisCommandPanel(), new NameDescriptionView());
        this.editRiskResilienceView = editRiskResilienceView;
        viewWidget = createView();
    }

    private Widget createView() {
        Label header = new Label("Risk/Resilience");
        header.setStyleName("inputPanelHeader");
        riskResilienceView = createRiskResilienceView();
        VerticalPanel viewPanel = new VerticalPanel();
        viewPanel.setStyleName("inputPanel");
        viewPanel.add(header);
        viewPanel.add(getCommandPanel().asWidget());
        viewPanel.add(riskResilienceView);
        getCommandPanel().setToolTips("Back to Dependency Threat Analysis", "Forward to Risk and Resilience Management");
        initEditPopup();
        initConfirmLockPopup();
        initConfirmUnlockPopup();
        return viewPanel;
    }

    private void initConfirmLockPopup() {
        confirmLockPopup = new ClosablePopup("Confirm Lock", true);
        confirmLockPopup.setModal(true);
        VerticalPanel viewPanel = new VerticalPanel();
        viewPanel.setStyleName("inputPanel");
        viewPanel.add(new HTML("Do you want to lock this analysis?  Changes can only be made after it is unlocked by an administrator."));
        confirmLockButton = ViewUtil.createButton("OK");
        viewPanel.add(ViewUtil.layoutCenter(confirmLockButton));
        confirmLockPopup.setWidget(viewPanel);
    }

    public void initConfirmUnlockPopup() {
        confirmUnlockPopup = new ClosablePopup("Confirm Unlock", true);
        confirmUnlockPopup.setModal(true);
        VerticalPanel viewPanel = new VerticalPanel();
        viewPanel.setStyleName("inputPanel");
        viewPanel.add(new HTML("Unlock this analysis?"));
        confirmUnlockButton = ViewUtil.createButton("OK");
        viewPanel.add(ViewUtil.layoutCenter(confirmUnlockButton));
        confirmUnlockPopup.setWidget(viewPanel);
    }

    private void initEditPopup() {
        editPopup = new ClosablePopup("Edit", true);
        editPopup.setModal(true);
        editPopup.setWidget(editRiskResilienceView.asWidget());
    }

    private Widget createRiskResilienceView() {
        VerticalPanel viewPanel = new VerticalPanel();
        viewPanel.setWidth("100%");
        DataProvider<RiskResilienceDTO> dataProvider = DataProviderFactory.createDataProvider();
        riskResilienceGrid = new RiskResilienceGrid(dataProvider);
        riskResilienceGrid.setHeight("650px");
        assetThreat = new Label();
        manageUriQuestionsButton = ViewUtil.createButton("Manage URI Questions");
        unitPricePkg = new TextBox();
        updateUnitPriceButton = ViewUtil.createButton("Update");
        HTML assetThreatLabel = ViewUtil.createInputLabel("Asset Threat : ");
        HTML unitPriceLabel = ViewUtil.createInputLabel("Price $/1000 gal : ");
        lockButton = ViewUtil.createButton("Lock");
        headerView = ViewUtil.horizontalPanel(5, lockButton, manageUriQuestionsButton, assetThreatLabel, assetThreat, unitPriceLabel,
                unitPricePkg, updateUnitPriceButton);
        headerView.setVisible(false);
        viewPanel.add(headerView);
        viewPanel.add(riskResilienceGrid.asWidget());
        riskResilienceGrid.getSelectionModel().addSelectionChangeHandler(new SelectionChangeEvent.Handler() {
            public void onSelectionChange(SelectionChangeEvent event) {
                RiskResilienceDTO selectedObject = riskResilienceGrid.getSelectionModel().getSelectedObject();
                assetThreat.setText(selectedObject == null ? "Not Selected" : selectedObject.getAssetName() + " - " + selectedObject.getThreatDescription());
            }
        });
        return viewPanel;
    }

    public HasClickHandlers getUpdateUnitPriceButton() {
        return updateUnitPriceButton;
    }

    public UnitPriceDTO getUnitPrice() {
        return new UnitPriceDTO(ViewUtil.getBigDecimal(unitPricePkg), 1000);
    }

    public void setUnitPrice(UnitPriceDTO unitPrice) {
        ViewUtil.setBigDecimal(unitPricePkg, unitPrice.getPrice());
    }

    public void setCurrentAnalysis(RiskResilienceAnalysisDTO dto) {
        updateControls();

        super.setCurrentAnalysis(dto);
        boolean haveAnalysis = dto != null;
        headerView.setVisible(haveAnalysis);
        riskResilienceGrid.asWidget().setVisible(haveAnalysis);
        if (haveAnalysis) {
            setUnitPrice(dto.getUnitPriceDTO());
        }
    }

    public void updateControls() {
        ParreAnalysisDTO analysisDTO = ClientSingleton.getCurrentParreAnalysis();

        boolean isBaseline = analysisDTO.isBaseline();
        String lockButtonLabel = isBaseline ? "Unlock" : "Lock";
        lockButton.setText(lockButtonLabel);
        ToolTipManager.addToolTip(lockButton, lockButtonLabel);
        unitPricePkg.setEnabled(!isBaseline);
        //manageUriQuestionsButton.setEnabled(!isBaseline);
        updateUnitPriceButton.setEnabled(!isBaseline);
        lockButton.setVisible(!analysisDTO.isOption());
        boolean isEnabled = !isBaseline || ClientSingleton.getLoginInfo().isAdmin();
        lockButton.setEnabled(isEnabled);
        if(analysisDTO.getOptionParreAnalysisDTOs().size() != 0) {
            lockButton.setVisible(false);
        }

    }

    public void setData(List<RiskResilienceDTO> resilienceDTOs) {
        riskResilienceGrid.setData(resilienceDTOs);
        refreshAnalysisGrid();
    }

    public void refreshAnalysisGrid() {
        getCommandPanel().setAnalysisCount(riskResilienceGrid.getData().size());
        riskResilienceGrid.refresh();
    }

    public SingleSelectionModel<RiskResilienceDTO> getSelectionModel() {
        return riskResilienceGrid.getSelectionModel();
    }

    public ActionSource<RiskResilienceDTO> getSelectionSource() {
        return riskResilienceGrid.getSelectionSource();
    }

    public List<RiskResilienceDTO> getData() {
        return riskResilienceGrid.getData();
    }

    public HasClickHandlers getLockButton() {
        return lockButton;
    }

    public HasClickHandlers getConfirmLockButton() {
        return confirmLockButton;
    }

    public void showConfirmLockPopup() {
        confirmLockPopup.center();
    }

    public void hideConfirmLockPopup() {
        confirmLockPopup.hide();
    }

    public HasClickHandlers getManageUriQuestionsButton() {
        return manageUriQuestionsButton;
    }

    public Widget asWidget() {
        return viewWidget;
    }

    public HasClickHandlers getConfirmUnlockButton() {
        return confirmUnlockButton;
    }

    public void hideConfirmUnlockPopup() {
        confirmUnlockPopup.hide();
    }

    public void showConfirmUnlockPopup() {
        confirmUnlockPopup.center();
    }

    @Override
    public HasClickHandlers getPreviousButton() {
        return getCommandPanel().getPreviousButton();
    }

    @Override
    public HasClickHandlers getNextButton() {
        return getCommandPanel().getNextButton();
    }
}
