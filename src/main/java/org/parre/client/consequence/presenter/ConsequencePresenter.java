/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.consequence.presenter;

import org.parre.client.ClientSingleton;
import org.parre.client.ViewFactory;
import org.parre.client.asset.event.AddAssetCancelledEvent;
import org.parre.client.consequence.event.ManageConsequenceNavEvent;
import org.parre.client.consequence.view.ManageConsequenceView;
import org.parre.client.messaging.ConsequenceServiceAsync;
import org.parre.client.messaging.ServiceFactory;
import org.parre.client.nav.event.ConsequenceNavEvent;
import org.parre.client.util.*;

import com.google.gwt.event.shared.EventBus;

/**
 * The Class ConsequencePresenter.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 7/19/11
 */
public class ConsequencePresenter implements Presenter {
    private Integer manageConsequenceViewIndex;

    private ConsequenceServiceAsync service;
    private EventBus eventBus;
    private Display display;
    private BusyHandler busyHandler;

    /**
     * The Interface Display.
     */
    public interface Display extends View, ViewContainer {

        void showView(int editEquipmentView);
    }

    public ConsequencePresenter(Display display) {
        this.service = ServiceFactory.getInstance().getConsequenceService();
        this.eventBus = ClientSingleton.getEventBus();
        this.display = display;
        this.busyHandler = new BusyHandler(display);
    }

    public void go() {
        initViewLoaders();
        bind();
    }

    private void initViewLoaders() {
        ManageConsequenceView manageConsequenceView = ViewFactory.createManageConsequenceView();
        manageConsequenceView.getPresenter().go();
        manageConsequenceViewIndex = display.addAndShowView(manageConsequenceView.asWidget());
    }

    private void bind() {
        eventBus.addHandler(ConsequenceNavEvent.TYPE, new ConsequenceNavEvent.Handler() {
            public void onEvent(ConsequenceNavEvent event) {
                eventBus.fireEvent(ManageConsequenceNavEvent.create());
            }
        });
        eventBus.addHandler(AddAssetCancelledEvent.TYPE, new AddAssetCancelledEvent.Handler() {
            public void onEvent(AddAssetCancelledEvent event) {
                display.showView(manageConsequenceViewIndex);
            }
        });

    }

}
