/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.consequence.presenter;

import org.parre.client.ClientSingleton;
import org.parre.client.consequence.event.ConsequenceUpdatedEvent;
import org.parre.client.consequence.event.EditConsequenceEvent;
import org.parre.client.messaging.ConsequenceServiceAsync;
import org.parre.client.messaging.ServiceFactory;
import org.parre.client.util.ActionSource;
import org.parre.client.util.BaseAsyncCommand;
import org.parre.client.util.BusyHandler;
import org.parre.client.util.ConfirmDialog;
import org.parre.client.util.ExecutableAsyncCallback;
import org.parre.client.util.ModelView;
import org.parre.client.util.Presenter;
import org.parre.client.util.ValidatingClickHandler;
import org.parre.client.util.ViewUtil;
import org.parre.shared.ConsequenceDTO;

import com.google.gwt.event.dom.client.BlurEvent;
import com.google.gwt.event.dom.client.BlurHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.TextBox;
import com.google.web.bindery.event.shared.EventBus;

/**
 * The Class EditConsequencePresenter.
 */
public class EditConsequencePresenter implements Presenter {
	private EventBus eventBus;
    private Display display;
    private ConsequenceServiceAsync service;
    private BusyHandler busyHandler;
    
    /**
     * The Interface Display.
     */
    public interface Display extends ModelView<ConsequenceDTO> {
        HasClickHandlers getSaveButton();
        TextBox getFinancialImpact();
        TextBox getEconomicImpact();
		TextBox getSeverity();
    }
    
    public EditConsequencePresenter(Display display) {
        this.service = ServiceFactory.getInstance().getConsequenceService();
        this.eventBus = ClientSingleton.getEventBus();
        this.display = display;
        busyHandler = new BusyHandler(display);
    }

	public void go() {
		bind();
	}
	
	private void bind() {
        display.getSaveButton().addClickHandler(new ValidatingClickHandler(display) {
            public void handleClick(ClickEvent event) {

            	new ExecutableAsyncCallback<ConsequenceDTO>(busyHandler, new BaseAsyncCommand<ConsequenceDTO>() {
            	    public void execute(AsyncCallback<ConsequenceDTO> callback) {
            	        ServiceFactory.getInstance().getConsequenceService().saveConsequence(display.updateAndGetModel(), callback);
            	    }
            	    @Override
            	    public void handleResult(ConsequenceDTO results) {
            	        eventBus.fireEvent(ConsequenceUpdatedEvent.create(results));
            	    }
            	}).makeCall();

                display.updateView();
            }
        });
        eventBus.addHandler(EditConsequenceEvent.TYPE, new EditConsequenceEvent.Handler() {

			public void onEvent(EditConsequenceEvent event) {
				ConsequenceDTO editDTO = event.getConsequenceDTO();
                display.setModelAndUpdateView(editDTO);
			}
        });
        /*display.getFinancialImpact().addBlurHandler(new BlurHandler() {
            @Override
            public void onBlur(BlurEvent blurEvent) {
                try {
                    display.getFinancialImpact().setText(ViewUtil.toDisplayValue(new BigDecimal(display.getFinancialImpact().getText().replace(",", ""))));
                } catch(NumberFormatException n) {
                    MessageDialog.popup("Owner's Financial Impact must be a number!");
                    display.getFinancialImpact().setFocus(true);
                }
            }
        });
        display.getFinancialImpact().addFocusHandler(new FocusHandler() {
            @Override
            public void onFocus(FocusEvent event) {
                display.getFinancialImpact().setText(display.getFinancialImpact().getText());
            }
        });
        */
        display.getSeverity().addBlurHandler(new BlurHandler() {
			@Override
            public void onBlur(BlurEvent blurEvent) {
            	try{
            		String severityStr = display.getSeverity().getText().replaceAll(",", "");
                	int severityMGD = Integer.parseInt(severityStr);
                    if(severityMGD > 100){
                    	 String message = "Severity is " + ViewUtil.addCommasForString(severityStr) + " million gallons per day, Are you sure?";
    					 ConfirmDialog.show(message, new ActionSource.ActionCommand() {
    	                    public void execute(Object o) {
    	                    }
    	                });	
                    }
            	}             
            	catch(NumberFormatException n){
            		//MessageDialog.popup("Severity must be a number!");
            		//display.getSeverity().setFocus(true);
            	}
            }
            });

	}
}

    


