/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.consequence.view;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.parre.client.consequence.presenter.EditConsequencePresenter;
import org.parre.client.util.BaseModelView;
import org.parre.client.util.FieldValidationHandler;
import org.parre.client.util.PositiveIntegerValidator;
import org.parre.client.util.PositiveNumberValidator;
import org.parre.client.util.ViewUtil;
import org.parre.shared.AmountDTO;
import org.parre.shared.ConsequenceDTO;

import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.Panel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

/**
 * The Class EditConsequenceView.
 */
public class EditConsequenceView extends BaseModelView<ConsequenceDTO>  implements EditConsequencePresenter.Display {
	private List<FieldValidationHandler> requiredFieldHandlers;
	private Widget viewWidget;
    private TextBox assetId;
    private TextBox threatId;
	private TextBox fatalities;
    private TextBox seriousInjuries;
    private TextBox duration;
    private TextBox severity;
    private TextBox financialImpact;
    private TextBox economicImpact;
    private Button saveButton;

	public EditConsequenceView() {
		requiredFieldHandlers = new ArrayList<FieldValidationHandler>();
        viewWidget = buildView();
	}

	private Panel buildView() {
        VerticalPanel panel = new VerticalPanel();
        FlexTable inputTable = new FlexTable();
        inputTable.setCellSpacing(1);

        int rowNum = -1;

        assetId = ViewUtil.addTextBoxFormEntry(inputTable, ++rowNum, "Asset");
        assetId.setWidth("50px");
        assetId.setEnabled(false);

        threatId = ViewUtil.addTextBoxFormEntry(inputTable, ++rowNum, "Threat");
        threatId.setWidth("50px");
        threatId.setEnabled(false);

        fatalities = ViewUtil.addTextBoxFormEntry(inputTable, ++rowNum, "Fatalities", requiredFieldHandlers, PositiveIntegerValidator.INSTANCE);
        fatalities.setWidth("50px");
        fatalities.getElement().setAttribute("maxlength", "6");

        seriousInjuries = ViewUtil.addTextBoxFormEntry(inputTable, ++rowNum, "Serious Injuries", requiredFieldHandlers, PositiveIntegerValidator.INSTANCE);
        seriousInjuries.setWidth("50px");
        seriousInjuries.getElement().setAttribute("maxlength", "6");

        duration = ViewUtil.addTextBoxFormEntry(inputTable, ++rowNum, "Duration (Days)", requiredFieldHandlers, PositiveNumberValidator.INSTANCE);
        duration.setWidth("50px");
        duration.getElement().setAttribute("maxlength", "6");

        severity = ViewUtil.addTextBoxFormEntry(inputTable, ++rowNum, "Severity (MGD)", requiredFieldHandlers, PositiveNumberValidator.INSTANCE);
        severity.setWidth("50px");
        severity.getElement().setAttribute("maxlength", "6");

        financialImpact = ViewUtil.addTextBoxFormEntry(inputTable, ++rowNum, "OFI ($)", requiredFieldHandlers, PositiveNumberValidator.INSTANCE);
        financialImpact.setWidth("150px");
        financialImpact.getElement().setAttribute("maxlength", "18");

        economicImpact = ViewUtil.addTextBoxFormEntry(inputTable, ++rowNum, "CEI ($)", requiredFieldHandlers, PositiveNumberValidator.INSTANCE);
        economicImpact.setWidth("150px");
        economicImpact.getElement().setAttribute("maxlength", "18");

        saveButton = ViewUtil.createButton("Save");
        panel.add(inputTable);
        panel.add(ViewUtil.layoutRight(saveButton));
        panel.setStyleName("inputPanel");
        return panel;

    }
	

	public boolean validate() {
		return ViewUtil.validate(requiredFieldHandlers);
	}

	public void updateModel() {
        ConsequenceDTO model = getModel();
        model.setFatalities(ViewUtil.getQuantity(fatalities));
        model.setSeriousInjuries(ViewUtil.getQuantity(seriousInjuries));
        model.setDurationDays(new BigDecimal(duration.getText().trim().replaceAll(",", "")).stripTrailingZeros());
        model.setSeverityMgd(new BigDecimal(severity.getText().trim().replaceAll(",", "")).stripTrailingZeros());
        //model.setSeverityMgd(new BigDecimal(severity.getText()).stripTrailingZeros());
        model.setFinancialImpactDTO(new AmountDTO(getBigDecimal(financialImpact)));
        model.setEconomicImpactDTO(new AmountDTO(getBigDecimal(economicImpact)));
    }

	public void updateView() {
        ConsequenceDTO model = getModel();
        ViewUtil.setText(assetId, model.getAssetId());
        ViewUtil.setText(threatId, model.getThreatName());
        ViewUtil.setQuantity(fatalities, model.getFatalities());
        ViewUtil.setQuantity(seriousInjuries, model.getSeriousInjuries());
        ViewUtil.setBigDecimal(duration, model.getDurationDays().stripTrailingZeros());
        ViewUtil.setBigDecimal(severity, model.getSeverityMgd().stripTrailingZeros());
        ViewUtil.setBigDecimal(financialImpact, model.getFinancialImpactDTO().getQuantity());
        ViewUtil.setBigDecimal(economicImpact, model.getEconomicImpactDTO().getQuantity());
	}

	public Widget asWidget() {
		return viewWidget;
	}

    public HasClickHandlers getSaveButton() {
        return saveButton;
    }

    @Override
    public TextBox getFinancialImpact() {
        return financialImpact;
    }

    @Override
    public TextBox getEconomicImpact() {
        return economicImpact;
    }
    
    @Override
    public TextBox getSeverity() {
        return severity;
    }
}
