/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.consequence.view;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import org.parre.client.util.DoubleClickableTextCell;
import org.parre.client.util.ViewUtil;
import org.parre.client.util.dataview.DataColumn;
import org.parre.client.util.dataview.DataGridHeaders;
import org.parre.client.util.dataview.DataGridView;
import org.parre.client.util.dataview.DataProvider;
import org.parre.shared.ConsequenceDTO;

import com.google.gwt.user.cellview.client.Column;

/**
 * The Class ConsequenceGrid.
 */
public class ConsequenceGrid extends DataGridView<ConsequenceDTO> {
    private Boolean useStatValue = Boolean.FALSE;
	public ConsequenceGrid(DataProvider<ConsequenceDTO> dataProvider) {
		super(dataProvider);
		List<DataColumn<ConsequenceDTO>> columnList = new ArrayList<DataColumn<ConsequenceDTO>>();
		columnList.add(createAssetColumn());
		columnList.add(createThreatNameColumn());
        columnList.add(createFatalitiesColumn());
        columnList.add(createSeriousInjuriesColumn());
        columnList.add(createDurationColumn());
        columnList.add(createSeverityColumn());
        columnList.add(createFinancialImpactColumn());
        columnList.add(createFinancialTotalColumn());
        columnList.add(createEconomicImpactColumn());
        addColumns(columnList);
	}



    private DataColumn<ConsequenceDTO> createAssetColumn() {
        Column<ConsequenceDTO, String> column = new Column<ConsequenceDTO, String>(
                createDoubleClickableTextCell()) {
            @Override
            public String getValue(ConsequenceDTO object) {
                return object.getAssetId() + " - " + object.getAssetName();
            }
        };
        column.setSortable(true);

        Comparator<ConsequenceDTO> comparator = new Comparator<ConsequenceDTO>() {
            public int compare(ConsequenceDTO o1, ConsequenceDTO o2) {
                return o1.getAssetId().compareTo(o2.getAssetId());
            }
        };
        return new DataColumn<ConsequenceDTO>("Asset", column, comparator, 20);
    }



    private DataColumn<ConsequenceDTO> createThreatNameColumn() {
    	Column<ConsequenceDTO, String> column = new Column<ConsequenceDTO, String>(
				createDoubleClickableTextCell()) {
			@Override
			public String getValue(ConsequenceDTO object) {
				return	object.getThreatName() + " - " + object.getThreatDescription();
			}
		};
		column.setSortable(true);

		Comparator<ConsequenceDTO> comparator = new Comparator<ConsequenceDTO>() {
			public int compare(ConsequenceDTO o1, ConsequenceDTO o2) {
				return o1.getThreatName().compareTo(o2.getThreatName());
			}
		};
		return new DataColumn<ConsequenceDTO>("Threat", column, comparator, 20);
	}



	private DataColumn<ConsequenceDTO> createFatalitiesColumn() {
		Column<ConsequenceDTO, String> column = new Column<ConsequenceDTO, String>(
				createDoubleClickableTextCell()) {
			@Override
			public String getValue(ConsequenceDTO object) {
				return	object.getFatalities().toString();
			}
		};
		column.setSortable(true);

		Comparator<ConsequenceDTO> comparator = new Comparator<ConsequenceDTO>() {
			public int compare(ConsequenceDTO o1, ConsequenceDTO o2) {
				return o1.getFatalities().compareTo(o2.getFatalities());
			}
		};
		return new DataColumn<ConsequenceDTO>("Fatalities", column, comparator, 10);
	}


    private DataColumn<ConsequenceDTO> createSeriousInjuriesColumn() {
    	Column<ConsequenceDTO, String> column = new Column<ConsequenceDTO, String>(
    			createDoubleClickableTextCell()) {
    		@Override
    		public String getValue(ConsequenceDTO object) {
    			return object.getSeriousInjuries().toString();
    		}
    	};
    	column.setSortable(true);

    	Comparator<ConsequenceDTO> comparator = new Comparator<ConsequenceDTO>() {
    		public int compare(ConsequenceDTO o1, ConsequenceDTO o2) {
    			return o1.getSeriousInjuries().compareTo(o2.getSeriousInjuries());
    		}
    	};
    	return new DataColumn<ConsequenceDTO>(DataGridHeaders.seriousInjuries(), column, comparator, 12);
    }

    private DataColumn<ConsequenceDTO> createDurationColumn() {
		Column<ConsequenceDTO, String> column = new Column<ConsequenceDTO, String>(
				createDoubleClickableTextCell()) {
			@Override
			public String getValue(ConsequenceDTO object) {
				return	ViewUtil.toDisplayValue(object.getDurationDays());
			}
		};
		column.setSortable(true);

		Comparator<ConsequenceDTO> comparator = new Comparator<ConsequenceDTO>() {
			public int compare(ConsequenceDTO o1, ConsequenceDTO o2) {
				return o1.getDurationDays().compareTo(o2.getDurationDays());
			}
		};
		return new DataColumn<ConsequenceDTO>(DataGridHeaders.createHeader("Duration", "(Days)"), column, comparator, 10);
	}

	private DataColumn<ConsequenceDTO> createSeverityColumn() {
		Column<ConsequenceDTO, String> column = new Column<ConsequenceDTO, String>(
				createDoubleClickableTextCell()) {
			@Override
			public String getValue(ConsequenceDTO object) {
				return	ViewUtil.toDisplayValue(object.getSeverityMgd());
			}
		};
		column.setSortable(true);

		Comparator<ConsequenceDTO> comparator = new Comparator<ConsequenceDTO>() {
			public int compare(ConsequenceDTO o1, ConsequenceDTO o2) {
				return o1.getSeverityMgd().compareTo(o2.getSeverityMgd());
			}
		};
		return new DataColumn<ConsequenceDTO>(DataGridHeaders.createHeader("Severity", "(MGD)"), column, comparator, 10);
	}
    private DataColumn<ConsequenceDTO> createFinancialImpactColumn() {
    	Column<ConsequenceDTO, String> column = new Column<ConsequenceDTO, String>(
    			createDoubleClickableTextCell()) {
    		@Override
    		public String getValue(ConsequenceDTO object) {
                return ViewUtil.toDisplayValue(object.getFinancialImpactDTO().getQuantity());
    		}
    	};
    	column.setSortable(true);

    	Comparator<ConsequenceDTO> comparator = new Comparator<ConsequenceDTO>() {
    		public int compare(ConsequenceDTO o1, ConsequenceDTO o2) {
    			return o1.getFinancialImpactDTO().getQuantity().compareTo(o2.getFinancialImpactDTO().getQuantity());
    		}
    	};
        return new DataColumn<ConsequenceDTO>(DataGridHeaders.financialImpact(), column, comparator, 12);
    }

    private DataColumn<ConsequenceDTO> createFinancialTotalColumn() {
        Column<ConsequenceDTO, String> column = new Column<ConsequenceDTO, String>(
                createDoubleClickableTextCell()) {
            @Override
            public String getValue(ConsequenceDTO object) {
                return useStatValue ? ViewUtil.toDisplayValue(object.getFinancialTotalDTO().getQuantity()) : "";
            }
        };
        column.setSortable(true);

        Comparator<ConsequenceDTO> comparator = new Comparator<ConsequenceDTO>() {
            public int compare(ConsequenceDTO o1, ConsequenceDTO o2) {
                return useStatValue ? o1.getFinancialTotalDTO().getQuantity().compareTo(o2.getFinancialTotalDTO().getQuantity()) :
                        0;
            }
        };
        return new DataColumn<ConsequenceDTO>(DataGridHeaders.financialTotal(), column, comparator, 12);

    }

    private DataColumn<ConsequenceDTO> createEconomicImpactColumn() {
    	Column<ConsequenceDTO, String> column = new Column<ConsequenceDTO, String>(
    			createDoubleClickableTextCell()) {
    		@Override
    		public String getValue(ConsequenceDTO object) {
                return ViewUtil.toDisplayValue(object.getEconomicImpactDTO().getQuantity());
    		}
    	};
    	column.setSortable(true);
    	
    	Comparator<ConsequenceDTO> comparator = new Comparator<ConsequenceDTO>() {
    		public int compare(ConsequenceDTO o1, ConsequenceDTO o2) {
    			return o1.getEconomicImpactDTO().getQuantity().compareTo(o2.getEconomicImpactDTO().getQuantity());
    		}
    	};
    	return new DataColumn<ConsequenceDTO>(DataGridHeaders.economicImpact(), column, comparator, 12);
    }

    private DoubleClickableTextCell<ConsequenceDTO> createDoubleClickableTextCell() {
        return new DoubleClickableTextCell<ConsequenceDTO>(getSelectionModel(), getSelectionSource());
    }


    public void setUseStatValue(Boolean useStatValue) {
        this.useStatValue = useStatValue;
    }
}
