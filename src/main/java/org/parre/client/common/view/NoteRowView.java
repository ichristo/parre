/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.common.view;

import com.google.gwt.dom.client.Style;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.ui.*;

import java.util.Date;

import org.parre.client.ClientSingleton;
import org.parre.client.util.BaseModelView;
import org.parre.client.util.ClientLoggingUtil;
import org.parre.client.util.ViewUtil;
import org.parre.shared.NoteDTO;

/**
 * User: keithjones
 * Date: 11/20/12
 * Time: 11:17 AM
*/
public class NoteRowView extends BaseModelView<NoteDTO> {
    private Label userName = new Label("");
    private Label dateLabel = new Label("");
    private Label message = new Label("");
    private Date date;
    private HorizontalPanel hPanel;
    private FlexTable inputTable;
    private Widget viewWidget;
    private VerticalPanel vPanel;
    private Boolean isNew = false;


    public NoteRowView() {
        viewWidget = new FlexTable();
        setModel(new NoteDTO());
    }

    public NoteRowView(NoteDTO noteDTO) {
        setModel(noteDTO);
        userName.setText(noteDTO.getUserName());
        setDate(noteDTO.getDateCreated());
        message.setText(noteDTO.getMessage());
        isNew = noteDTO.isNew();
        viewWidget = createView();
    }

    public Widget createView() {
        inputTable = new FlexTable();
        hPanel = new HorizontalPanel();
        vPanel = new VerticalPanel();
        userName.getElement().getStyle().setFontSize(11, Style.Unit.PX);
        userName.getElement().getStyle().setFontWeight(Style.FontWeight.BOLD);
        dateLabel.getElement().getStyle().setFontSize(11, Style.Unit.PX);
        dateLabel.getElement().getStyle().setColor("#7A7A7A");
        hPanel = ViewUtil.layoutLeft(userName, dateLabel);
        message.getElement().getStyle().setPaddingLeft(10, Style.Unit.PX);
        message.getElement().getStyle().setPaddingBottom(10, Style.Unit.PX);
        vPanel.add(ViewUtil.layoutLeft(0, hPanel.asWidget()));
        vPanel.add(ViewUtil.layoutLeft(0, message));
        vPanel.setWidth("380px");
        return vPanel.asWidget();
    }

    public String getUsername() {
        return userName.getText();
    }

    public String getMessage() {
        return message.getText();
    }

    public void setUserName(String userName) {
        this.userName.setText(userName);
    }

    public void setDateLabel(Date date) {
        this.dateLabel.setText(getSpecifiedDate(date));
    }

    public void setDate() {
        date = new Date();
        setDateLabel(date);
    }

    public Date getDate() {
        return date;
    }

    public String getDateLabel() {
        return getSpecifiedDate(date);
    }

    private void setDate(Date dateCreated) {
        date = dateCreated;
        setDateLabel(date);
    }

    public void setMessage(String message) {
        this.message.setText(message);
    }

    @Override
    public boolean validate() {
        return true;
    }

    @Override
    public void updateModel() {
        NoteDTO model = getModel();
        model.setDateCreated(new Date());
        model.setUserName(ClientSingleton.getLoginInfo().getNickname());
        model.setMessage(message.getText());
        ClientLoggingUtil.debug(model.toString());
    }

    @Override
    public void updateView() {
        NoteDTO model = getModel();
        setUserName(model.getUserName());
        setDateLabel(model.getDateCreated());
        setMessage(model.getMessage());
    }

    @Override
    public Widget asWidget() {
        return viewWidget;
    }

    public FlexTable getInputTable() {
        return inputTable;
    }

    public void setInputTable(FlexTable inputTable) {
        this.inputTable = inputTable;
    }

    public String getCurrentDate() {
        return DateTimeFormat.getFormat("MM.dd.yyyy hh:mm").format(new Date());
    }

    public String getSpecifiedDate(Date date) {
        return DateTimeFormat.getFormat("MM/dd/yyyy hh:mm a").format(date);
    }

    @Override
    public String toString() {
        ClientLoggingUtil.debug("NoteTableView{" + "userName=" + userName.getText());
        ClientLoggingUtil.debug("dateCreated=" + getSpecifiedDate(date));
        ClientLoggingUtil.debug("message=" + message.getText());
        return "";
    }

    public Boolean getNew() {
        return isNew;
    }

    public void setNew(Boolean aNew) {
        isNew = aNew;
    }
}
