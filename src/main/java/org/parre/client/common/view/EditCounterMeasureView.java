/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.common.view;

import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.user.client.ui.*;

import java.util.ArrayList;
import java.util.List;

import org.parre.client.util.*;
import org.parre.shared.CounterMeasureDTO;
import org.parre.shared.types.CounterMeasureType;

/**
 * The Class EditCounterMeasureView.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 12/8/11
 */
public class EditCounterMeasureView extends BaseModelView<CounterMeasureDTO> {
    private List<FieldValidationHandler> fieldValidationHandlers = new ArrayList<FieldValidationHandler>();
    private TextBox name;
    private TextBox description;
    private Button saveButton;
    private Widget viewWidget;
    private ListBox counterMeasureTypes;

    public EditCounterMeasureView() {
        viewWidget = createView();
    }

    private FlexTable createView() {
        FlexTable inputTable = new FlexTable();
        inputTable.setWidth("100%");
        inputTable.setStyleName("inputPanel");
        Label header = new Label("Edit Counter Measure");
        header.setStyleName("inputPanelHeader");
        int currentRow = -1;
        inputTable.setWidget(++currentRow, 0, header);
        alignCenter(currentRow, inputTable);
        counterMeasureTypes = ViewUtil.addListBoxFormEntry(inputTable, ++currentRow, "Type", fieldValidationHandlers);
        initCounterMeasureTypes();
        name = ViewUtil.addTextBoxFormEntry(inputTable, ++currentRow, "Name", fieldValidationHandlers);
        description = ViewUtil.addTextBoxFormEntry(inputTable, ++currentRow, "Description");
        saveButton = ViewUtil.createButton("Save");
        inputTable.setWidget(++currentRow, 0, saveButton);
        alignCenter(currentRow, inputTable);
        return inputTable;
    }

    private void initCounterMeasureTypes() {
        CounterMeasureType[] values = CounterMeasureType.values();
        counterMeasureTypes.addItem("Select", "");
        for (CounterMeasureType value : values) {
            counterMeasureTypes.addItem(value.getValue(), value.name());
        }
    }


    private void alignCenter(int currentRow, FlexTable inputTable) {
        inputTable.getFlexCellFormatter().setColSpan(currentRow, 0, 2);
        inputTable.getFlexCellFormatter().setHorizontalAlignment(currentRow, 0, HasHorizontalAlignment.ALIGN_CENTER);
    }

    public HasClickHandlers getSaveButton() {
        return saveButton;
    }


    public void updateModel() {
        CounterMeasureDTO model = getModel();
        model.setName(ViewUtil.getText(name));
        model.setDescription(ViewUtil.getText(description));
        model.setType(CounterMeasureType.valueOf(getSelectedValue(counterMeasureTypes)));
    }

    public void updateView() {
        CounterMeasureDTO model = getModel();
        CounterMeasureType type = model.getType();
        setSelectedValue(counterMeasureTypes, type != null ? type.name() : "");
        setText(name, model.getName());
        setText(description, model.getDescription());
    }

    public boolean validate() {
        return ViewUtil.validate(fieldValidationHandlers);
    }

    public Widget asWidget() {
        return viewWidget;
    }
}
