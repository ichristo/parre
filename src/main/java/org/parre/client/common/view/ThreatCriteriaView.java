/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.common.view;

import com.google.gwt.user.client.ui.*;

import java.util.ArrayList;
import java.util.List;

import org.parre.client.common.presenter.ThreatCriteriaPresenter;
import org.parre.client.util.BaseModelView;
import org.parre.client.util.ViewUtil;
import org.parre.shared.ThreatDTO;
import org.parre.shared.types.ThreatCategoryType;

/**
 * .
 * User: Swarn S. Dhaliwal
 * Date: 8/28/11
 * Time: 5:37 PM
 */
public class ThreatCriteriaView extends BaseModelView<ThreatDTO> implements ThreatCriteriaPresenter.Display {

    private TextBox hazardCode;
    private TextBox hazardDescription;
    private List<TextBox> inputFields = new ArrayList<TextBox>(2);
    private HorizontalPanel panel;
    private TextBox hazardType;
    private ListBox threatCategory;

    public ThreatCriteriaView() {
        createUI();
    }

    private void createUI() {
        FlexTable searchTable = new FlexTable();
        searchTable.setCellSpacing(6);
        int rowNum = -1;

        hazardCode = ViewUtil.addTextBoxFormEntry(searchTable, ++rowNum, "Hazard code");
        hazardDescription = ViewUtil.addTextBoxFormEntry(searchTable, rowNum, "Hazard Description", 2, 3);

        hazardType = ViewUtil.addTextBoxFormEntry(searchTable, ++rowNum, "Hazard Type");
        threatCategory = ViewUtil.addListBoxFormEntry(searchTable, rowNum, "Threat Category", 2, 3);

        inputFields.add(hazardCode);
        inputFields.add(hazardDescription);
        inputFields.add(hazardType);
        panel = ViewUtil.layoutCenter(searchTable);
    }


    public Widget asWidget() {
        return panel;
    }

    public List<TextBox> getTextInputFields() {
        return inputFields;
    }

    public ListBox getThreatCategories() {
        return threatCategory;
    }

    public boolean validate() {
        return true;
    }

    public void updateModel() {
        ThreatDTO model = getModel();
        model.setName(ViewUtil.getText(hazardCode));
        model.setDescription(ViewUtil.getText(hazardDescription));
        model.setHazardType(ViewUtil.getText(hazardType));
        model.setThreatCategory(ThreatCategoryType.valueOf(ViewUtil.getSelectedValue(threatCategory)));
    }

    public void updateView() {
        ThreatDTO model = getModel();
        ViewUtil.setText(hazardCode, model.getName());
        ViewUtil.setText(hazardDescription, model.getDescription());
        ViewUtil.setText(hazardType, model.getHazardType());
        ViewUtil.setSelectedValue(threatCategory, model.getThreatCategory().name());
    }
}
