/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.common.view;


import org.parre.client.util.BaseView;
import org.parre.client.util.ClosablePopup;
import org.parre.client.util.ModelView;
import org.parre.client.util.ViewUtil;
import org.parre.shared.NameDescriptionDTO;

import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.user.client.ui.*;

/**
 * The Class BaseAnalysisView.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 12/8/11
 */
public abstract class BaseAnalysisView  extends BaseView implements AnalysisView {
    private AnalysisCommandPanel commandPanel;
    private NameDescriptionView startAnalysisView;
    private DialogBox confirmDialog;
    private HTML assetThreatCount;
    private Button confirmStartAnalysisButton;

    protected BaseAnalysisView(AnalysisCommandPanel commandPanel, NameDescriptionView startAnalysisView) {
        this.commandPanel = commandPanel;
        this.startAnalysisView = startAnalysisView;
        initConfirmDialog();
    }

    private void initConfirmDialog() {
        confirmDialog = new ClosablePopup("Confirm Start Analysis", true);
        confirmDialog.setModal(true);
        VerticalPanel viewPanel = new VerticalPanel();
        viewPanel.setStyleName("inputPanel");
        assetThreatCount = new HTML();
        assetThreatCount.setStyleName("normalLabel");
        viewPanel.add(ViewUtil.layoutCenter(assetThreatCount, new HTML(" : Asset Threat Pairs will be Analyzed, Proceed?")));
        confirmStartAnalysisButton = ViewUtil.createButton("OK");
        viewPanel.add(ViewUtil.layoutCenter(confirmStartAnalysisButton));
        confirmDialog.setWidget(viewPanel);
    }


    public HasClickHandlers getConfirmStartAnalysisButton() {
        return confirmStartAnalysisButton;
    }

    public void showConfirmStartAnalysisPopup(Integer count) {
        assetThreatCount.setText(String.valueOf(count));
        confirmDialog.center();
    }

    public void hideConfirmStartAnalysisPopup() {
        confirmDialog.hide();
    }

    public AnalysisCommandPanel getCommandPanel() {
        return commandPanel;
    }

    public ModelView<NameDescriptionDTO> getStartAnalysisView() {
        return startAnalysisView;
    }

    public void setCurrentAnalysis(NameDescriptionDTO currentAnalysis) {
        commandPanel.setAnalysis(currentAnalysis);
    }

    public HasClickHandlers getStartAnalysisButton() {
        return commandPanel.getStartAnalysisButton();
    }

    public HasClickHandlers getSaveAnalysisButton() {
        return startAnalysisView.getSaveButton();
    }

    public void showStartAnalysisPopup() {
        startAnalysisView.getPopup().center();
        startAnalysisView.getPopup().show();
    }

    public void hideStartAnalysisPopup() {
        startAnalysisView.getPopup().hide();
    }

}
