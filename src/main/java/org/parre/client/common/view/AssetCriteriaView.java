/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.common.view;

import com.google.gwt.event.dom.client.HasKeyPressHandlers;
import com.google.gwt.user.client.ui.*;

import java.util.ArrayList;
import java.util.List;

import org.parre.client.common.presenter.AssetCriteriaPresenter;
import org.parre.client.util.BaseModelView;
import org.parre.client.util.ViewUtil;
import org.parre.shared.AssetSearchDTO;

/**
 * .
 * User: Swarn S. Dhaliwal
 * Date: 8/28/11
 * Time: 5:37 PM
 */
public class AssetCriteriaView extends BaseModelView<AssetSearchDTO> implements AssetCriteriaPresenter.Display {

    private TextBox assetId;
    private TextBox assetName;
    private CheckBox isRemoved;
    private List<HasKeyPressHandlers> inputFields = new ArrayList<HasKeyPressHandlers>(2);
    private Widget viewWidget;

    public AssetCriteriaView() {
        viewWidget = createView();
    }

    private Widget createView() {
        FlexTable searchTable = new FlexTable();
        searchTable.setCellSpacing(6);
        int rowNum = -1;

        assetId = ViewUtil.addTextBoxFormEntry(searchTable, ++rowNum, "Asset ID");
        assetName = ViewUtil.addTextBoxFormEntry(searchTable, rowNum, "Asset Name", 2, 3);
        isRemoved = new CheckBox();
        ViewUtil.addInputEntry(searchTable, rowNum, 4, isRemoved);
        ViewUtil.addInputEntry(searchTable, rowNum, 5, new Label("Is Removed?"));
        inputFields.add(assetId);
        inputFields.add(assetName);
        return ViewUtil.layoutCenter(searchTable);
    }


    public Widget asWidget() {
        return viewWidget;
    }

    public List<HasKeyPressHandlers> getSearchInputFields() {
        return inputFields;
    }

    public boolean validate() {
        return true;
    }

    public void updateModel() {
        AssetSearchDTO model = getModel();
        model.setAssetId(ViewUtil.getText(assetId));
        model.setAssetName(ViewUtil.getText(assetName));
        model.setRemoved(isRemoved.getValue());
    }

    public void updateView() {
        AssetSearchDTO model = getModel();
        ViewUtil.setText(assetId, model.getAssetId());
        ViewUtil.setText(assetName, model.getAssetName());
        isRemoved.setValue(model.getRemoved());
    }
}
