/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.common.view;

import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.user.client.ui.*;

import java.math.BigDecimal;

import org.parre.client.ClientSingleton;
import org.parre.client.util.ClientLoggingUtil;
import org.parre.client.util.ToolTipManager;
import org.parre.client.util.ViewUtil;
import org.parre.shared.NameDescriptionDTO;
import org.parre.shared.ParreAnalysisDTO;

/**
 * The Class AnalysisCommandPanel.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 12/7/11
 */
public class AnalysisCommandPanel extends FlexTable {

    private Button startAnalysisButton;
    private Label analysisCount;
    private Label analysisName;
    private Label analysisCountLabel;
    private HorizontalPanel analysisCountPanel;
    private HTML fatalityValue;
    private HTML seriousInjuryValue;
    private HorizontalPanel statValuesView;
    private Image previousButton;
    private Image nextButton;

    public AnalysisCommandPanel() {
        analysisCountLabel = new Label("Analysis Count : ");
        analysisCountLabel.setStyleName("normalLabel");
        this.analysisCount = new Label("");

        previousButton = ViewUtil.createImageButton("button_left.png", "Previous Section", 30, 30, true);
        nextButton = ViewUtil.createImageButton("button_right.png", "Next Section", 30, 30, true);

        analysisCountPanel = ViewUtil.horizontalPanel(5, analysisCountLabel, this.analysisCount);
        analysisCountPanel.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_RIGHT);
        analysisCountPanel.setVisible(false);
        startAnalysisButton = ViewUtil.createButton("Start Analysis");
        startAnalysisButton.setVisible(false);
        analysisName = new Label("");
        analysisName.setStyleName("normalLabel");
        FlexTable commandTable = this;
        commandTable.setWidth("100%");
        fatalityValue = new HTML();
        seriousInjuryValue = new HTML();
        statValuesView = ViewUtil.horizontalPanel(5, ViewUtil.createInputLabel("Stat Values : F($M):"),
                fatalityValue, ViewUtil.createInputLabel("SI($M):"), seriousInjuryValue);
        commandTable.setWidget(0, 0, previousButton);
        commandTable.setWidget(0, 1, ViewUtil.horizontalPanel(statValuesView));
        commandTable.setWidget(0, 2, analysisName);
        commandTable.setWidget(0, 3, analysisCountPanel);
        commandTable.setWidget(0, 4, nextButton);
        FlexCellFormatter formatter = commandTable.getFlexCellFormatter();
        formatter.setWidth(0, 0, "30px");
        formatter.setWidth(0, 1, "30%");
        formatter.setWidth(0, 2, "33%");
        formatter.setWidth(0, 3, "30%");
        formatter.setWidth(0, 4, "30px");
        formatter.setHorizontalAlignment(0, 0, HasHorizontalAlignment.ALIGN_LEFT);
        formatter.setHorizontalAlignment(0, 1, HasHorizontalAlignment.ALIGN_CENTER);
        formatter.setHorizontalAlignment(0, 2, HasHorizontalAlignment.ALIGN_CENTER);
        formatter.setHorizontalAlignment(0, 3, HasHorizontalAlignment.ALIGN_CENTER);
        formatter.setHorizontalAlignment(0, 4, HasHorizontalAlignment.ALIGN_RIGHT);
    }

    public HasClickHandlers getStartAnalysisButton() {
        return startAnalysisButton;
    }

    public void setAnalysisName(String analysisName) {
    	this.analysisName.setText(analysisName);
    }

    public void setAnalysisCount(int count) {
        analysisCount.setText(String.valueOf(count));
    }

    public void setToolTips(String previous, String next) {
        ToolTipManager.addToolTip(previousButton, previous);
        ToolTipManager.addToolTip(nextButton, next);
    }

    public HasClickHandlers getPreviousButton() {
        return previousButton;
    }

    public HasClickHandlers getNextButton() {
        return nextButton;
    }

    public void setAnalysis(NameDescriptionDTO dto) {
        boolean haveAnalysis = dto != null;
        startAnalysisButton.setVisible(!haveAnalysis);
        analysisCountPanel.setVisible(haveAnalysis);
        analysisName.setText(haveAnalysis ? dto.getName() : "");
        ParreAnalysisDTO currentParreAnalysis = ClientSingleton.getCurrentParreAnalysis();
        boolean displayStatValues = haveAnalysis && currentParreAnalysis.getUseStatValues();
        statValuesView.setVisible(displayStatValues);
        if (haveAnalysis) {
            try {
                ViewUtil.setBigDecimal(fatalityValue, currentParreAnalysis.getStatValuesDTO().getFatalityQuantity());
                ViewUtil.setBigDecimal(seriousInjuryValue, currentParreAnalysis.getStatValuesDTO().getSeriousInjuryQuantity());
            } catch (NullPointerException e) {
                ClientLoggingUtil.error("NullPointerException at AnalysisCommandPanel.setAnalysis()");
                ViewUtil.setBigDecimal(fatalityValue, BigDecimal.ZERO);
                ViewUtil.setBigDecimal(seriousInjuryValue, BigDecimal.ZERO);
            }

        }
    }
}
