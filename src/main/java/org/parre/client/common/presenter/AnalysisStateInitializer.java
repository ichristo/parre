/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.common.presenter;

import org.parre.client.ClientSingleton;
import org.parre.client.event.CurrentAnalysisChangedEvent;

/**
 * The Class AnalysisStateInitializer.
 *
 * @param <T> the generic type
 * @author Swarn S. Dhaliwal
 * @version 1.0 2/28/12
 */
public class AnalysisStateInitializer<T> {
    private AnalysisStateFactory<T> handlerFactory;
    private AnalysisStateHost<T> handlerHost;

    public AnalysisStateInitializer(AnalysisStateHost<T> handlerHost,
                                    AnalysisStateFactory<T> handlerFactory) {
        this.handlerHost = handlerHost;
        this.handlerFactory = handlerFactory;
        bind();
    }

    public void go() {
        init();
        bind();
    }

    private void bind() {
        ClientSingleton.getEventBus().addHandler(CurrentAnalysisChangedEvent.TYPE, new CurrentAnalysisChangedEvent.Handler() {
            public void onEvent(CurrentAnalysisChangedEvent event) {
                init();
            }
        });
    }

    private void init() {
        if (ClientSingleton.isCurrentAnalysisLocked()) {
            handlerHost.setHandler(handlerFactory.createLockedAnalysisState(handlerHost.getView()));
        }
        else {
            handlerHost.setHandler(handlerFactory.createActiveAnalysisState(handlerHost.getView()));
        }
    }
}
