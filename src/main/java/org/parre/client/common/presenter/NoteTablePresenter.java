/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.common.presenter;

import com.google.web.bindery.event.shared.EventBus;

import java.util.Date;

import org.parre.client.ClientSingleton;
import org.parre.client.messaging.AssetServiceAsync;
import org.parre.client.messaging.ServiceFactory;
import org.parre.client.util.*;
import org.parre.shared.NoteDTO;

/**
 * User: keithjones
 * Date: 11/20/12
 * Time: 12:09 PM
*/
public class NoteTablePresenter implements Presenter {
    private EventBus eventBus;
    private Display display;
    private AssetServiceAsync service;
    private BusyHandler busyHandler;

    /**
     * The Interface Display.
     */
    public static interface Display extends ModelView<NoteDTO> {
        void setUserName(String userName);

        void setDateLabel(Date date);

        void setMessage(String message);

        void startEditing();

        void saveNote();

        String getUsername();

        Date getDateLabel();

        String getMessage();
    }

    public NoteTablePresenter(Display display) {
        this.service = ServiceFactory.getInstance().getAssetService();
        this.eventBus = ClientSingleton.getEventBus();
        this.display = display;
        this.busyHandler = new BusyHandler(display);
    }

    @Override
    public void go() {
        //bind();
        display.setModel(new NoteDTO());
    }
}
