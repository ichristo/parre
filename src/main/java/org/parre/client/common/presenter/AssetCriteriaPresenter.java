/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.common.presenter;

import com.google.gwt.event.dom.client.HasKeyPressHandlers;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.event.dom.client.KeyPressEvent;
import com.google.gwt.event.dom.client.KeyPressHandler;
import com.google.web.bindery.event.shared.EventBus;

import java.util.List;

import org.parre.client.ClientSingleton;
import org.parre.client.asset.event.AssetSearchEvent;
import org.parre.client.asset.event.ClearAssetSearchEvent;
import org.parre.client.common.event.AssetThreatSearchType;
import org.parre.client.util.ModelView;
import org.parre.client.util.Presenter;
import org.parre.shared.AssetSearchDTO;

/**
 * .
 * User: Swarn S. Dhaliwal
 * Date: 8/28/11
 * Time: 6:03 PM
 */
public class AssetCriteriaPresenter implements Presenter {
    private EventBus eventBus;
    private Display display;
    private AssetSearchCommand searchCommand;
    private AssetThreatSearchType searchType;

    /**
     * The Interface Display.
     */
    public interface Display extends ModelView<AssetSearchDTO> {

        List<HasKeyPressHandlers> getSearchInputFields();
    }

    public AssetCriteriaPresenter(AssetThreatSearchType searchType, AssetSearchCommand searchCommand, Display display) {
        this.searchType = searchType;
        this.searchCommand = searchCommand;
        this.eventBus = ClientSingleton.getEventBus();
        this.display = display;
    }

    public void go() {
        bind();
        display.setModel(new AssetSearchDTO(ClientSingleton.getParreAnalysisId()));
    }

    private void bind() {
        KeyPressHandler handler = new KeyPressHandler() {
            public void onKeyPress(KeyPressEvent event) {
                if (KeyCodes.KEY_ENTER == event.getNativeEvent().getKeyCode()) {
                    performSearch();
                }
            }
        };

        List<HasKeyPressHandlers> searchInputFields = display.getSearchInputFields();
        for (HasKeyPressHandlers searchInputField : searchInputFields) {
            searchInputField.addKeyPressHandler(handler);
        }

        eventBus.addHandler(ClearAssetSearchEvent.TYPE, new ClearAssetSearchEvent.Handler() {
            public void onEvent(ClearAssetSearchEvent event) {
            	AssetSearchDTO searchDTO = display.getModel();
            	searchDTO.setAssetName(null);
            	searchDTO.setAssetId(null);
            	searchDTO.setRemoved(false);
            	display.setModelAndUpdateView(searchDTO);
            }
        });
        eventBus.addHandler(AssetSearchEvent.TYPE, new AssetSearchEvent.Handler() {
            public void onEvent(AssetSearchEvent event) {
                if (searchType == event.getSearchType()) {
                    performSearch();
                }
            }
        });
    }

    private void performSearch() {
    	display.getModel().setParreAnalysisId(ClientSingleton.getCurrentParreAnalysis().getId());
        searchCommand.execute(display.updateAndGetModel(), display);
    }
}
