/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.common.presenter;

import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.event.dom.client.KeyPressEvent;
import com.google.gwt.event.dom.client.KeyPressHandler;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextBox;
import com.google.web.bindery.event.shared.EventBus;

import java.util.List;

import org.parre.client.ClientSingleton;
import org.parre.client.common.event.AssetThreatSearchType;
import org.parre.client.threat.event.ClearThreatSearchEvent;
import org.parre.client.threat.event.ThreatSearchEvent;
import org.parre.client.threat.presenter.ThreatCategoryListCommand;
import org.parre.client.util.BusyHandler;
import org.parre.client.util.ClientLoggingUtil;
import org.parre.client.util.ModelView;
import org.parre.client.util.Presenter;
import org.parre.shared.ThreatDTO;

/**
 * .
 * User: Swarn S. Dhaliwal
 * Date: 8/28/11
 * Time: 6:03 PM
 */
public class ThreatCriteriaPresenter implements Presenter {
    private EventBus eventBus;
    private Display display;
    private ThreatSearchCommand searchCommand;
    private AssetThreatSearchType searchType;
    private BusyHandler busyHandler;

    /**
     * The Interface Display.
     */
    public interface Display extends ModelView<ThreatDTO> {
        List<TextBox> getTextInputFields();

        ListBox getThreatCategories();
    }

    public ThreatCriteriaPresenter(AssetThreatSearchType searchType, ThreatSearchCommand searchCommand, Display display) {
        this.searchType = searchType;
        this.searchCommand = searchCommand;
        this.eventBus = ClientSingleton.getEventBus();
        this.display = display;
        this.busyHandler = new BusyHandler(display);
    }

    public void go() {
        bind();
    }

    private void bind() {
        List<TextBox> textInputFields = display.getTextInputFields();
        KeyPressHandler handler = new KeyPressHandler() {
            public void onKeyPress(KeyPressEvent keyPressEvent) {
                if (KeyCodes.KEY_ENTER == keyPressEvent.getNativeEvent().getKeyCode()) {
                    performSearch();
                }
            }
        };
        for (TextBox textInputField : textInputFields) {
            textInputField.addKeyPressHandler(handler);
        }

        eventBus.addHandler(ClearThreatSearchEvent.TYPE, new ClearThreatSearchEvent.Handler() {
            public void onEvent(ClearThreatSearchEvent event) {
                display.setModelAndUpdateView(new ThreatDTO());
            }
        });
        eventBus.addHandler(ThreatSearchEvent.TYPE, new ThreatSearchEvent.Handler() {
            public void onEvent(ThreatSearchEvent event) {
                if (event.getSearchType() == searchType) {
                    performSearch();
                }
            }
        });

        new ThreatCategoryListCommand(display.getThreatCategories(), busyHandler).execute();
        display.setModel(new ThreatDTO());
    }

    private void performSearch() {
        ClientLoggingUtil.info("Peforming Threat Search");
        ThreatDTO dto = display.updateAndGetModel();
        searchCommand.execute(dto, display);
    }

}
