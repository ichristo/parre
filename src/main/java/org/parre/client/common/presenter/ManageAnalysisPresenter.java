/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.common.presenter;

import org.parre.client.ClientSingleton;
import org.parre.client.ParreClientUtil;
import org.parre.client.common.view.AnalysisView;
import org.parre.client.messaging.ServiceFactory;
import org.parre.client.util.BaseAsyncCommand;
import org.parre.client.util.BusyHandler;
import org.parre.client.util.ExecutableAsyncCallback;
import org.parre.shared.AnalysisDTO;
import org.parre.shared.types.AssetThreatAnalysisType;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;

/**
 * The Class ManageAnalysisPresenter.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 12/8/11
 */
public abstract class ManageAnalysisPresenter {
    private AnalysisView display;
    private BusyHandler busyHandler;

    public ManageAnalysisPresenter(AnalysisView display) {
        this.display = display;
        this.busyHandler = new BusyHandler(display);
    }

    protected void init() {
        display.getStartAnalysisButton().addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                if (!ParreClientUtil.handleAnalysisBaselined()) {
                    showConfirmStartAnalysisPopup();
                }
            }
        });
        display.getSaveAnalysisButton().addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                if (!display.getStartAnalysisView().validate()) {
                    return;
                }
                display.hideStartAnalysisPopup();
                startAnalysis();
            }
        });
        display.getConfirmStartAnalysisButton().addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                display.hideConfirmStartAnalysisPopup();
                /*display.getStartAnalysisView().setModelAndUpdateView(new NameDescriptionDTO());
                display.showStartAnalysisPopup();*/
                startAnalysis();
            }
        });
        getCurrentAnalysis();
    }

    private void showConfirmStartAnalysisPopup() {

        new ExecutableAsyncCallback<Integer>(busyHandler, new BaseAsyncCommand<Integer>() {
            public void execute(AsyncCallback<Integer> callback) {
                ServiceFactory.getInstance().getParreService().getAssetThreatAnalysisCount
                        (ClientSingleton.getParreAnalysisId(), getAnalysisType(), callback);
            }
            @Override
            public void handleResult(Integer count) {
                display.showConfirmStartAnalysisPopup(count);
            }
        }).makeCall();

    }

    protected void initCurrentAnalysis(AnalysisDTO analysisDTO) {
        if (analysisDTO == null) {
            showConfirmStartAnalysisPopup();
        }
    }

    protected abstract AssetThreatAnalysisType getAnalysisType();

    protected abstract void startAnalysis();

    protected abstract void getCurrentAnalysis();

}
