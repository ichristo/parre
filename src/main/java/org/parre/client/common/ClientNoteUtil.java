/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.common;

import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.VerticalPanel;

import java.util.ArrayList;
import java.util.List;

import org.parre.client.common.view.NoteRowView;
import org.parre.client.util.ClientLoggingUtil;
import org.parre.client.util.ViewUtil;
import org.parre.shared.NoteDTO;

/**
 * User: keithjones
 * Date: 12/19/12
 * Time: 3:32 PM
*/
public class ClientNoteUtil {

    public static List<NoteDTO> getNoteDTOs(List<NoteRowView> noteViews) {
        List<NoteDTO> dtos = new ArrayList<NoteDTO>();
        for(int i = 0; i<noteViews.size(); i++) {
            dtos.add(new NoteDTO());
            dtos.get(i).setUserName(noteViews.get(i).getUsername());
            dtos.get(i).setDateCreated(noteViews.get(i).getDate());
            dtos.get(i).setMessage(noteViews.get(i).getMessage());
            dtos.get(i).setNew(noteViews.get(i).getNew());
        }
        return dtos;
    }

    public static VerticalPanel setNotesPanel(List<NoteRowView> noteViews) {
        VerticalPanel notesPanel = new VerticalPanel();
        if(noteViews.size() > 0) {
            for(NoteRowView note : noteViews) {
                notesPanel.add(note.createView());
            }
        }

        return notesPanel;
    }

}
