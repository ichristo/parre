/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.example.tree;

import com.google.gwt.cell.client.*;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Element;
import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.ImageResource;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;
import com.google.gwt.user.client.ui.AbstractImagePrototype;
import com.google.gwt.view.client.DefaultSelectionEventManager;
import com.google.gwt.view.client.ListDataProvider;
import com.google.gwt.view.client.SelectionModel;
import com.google.gwt.view.client.TreeViewModel;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * The Class ContactTreeViewModel.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 1/12/12
 */
public class ContactTreeViewModel implements TreeViewModel {

    /**
     * The images used for this example.
     */
    static interface Images extends ClientBundle {
        ImageResource contact();

        ImageResource contactsGroup();
    }

    /**
     * The cell used to render categories.
     */
    private static class CategoryCell extends AbstractCell<ContactDatabase.Category> {

        /**
         * The html of the image used for contacts.
         */
        private final String imageHtml;

        public CategoryCell(ImageResource image) {
            this.imageHtml = AbstractImagePrototype.create(image).getHTML();
        }

        @Override
        public void render(Context context, ContactDatabase.Category value, SafeHtmlBuilder sb) {
            if (value != null) {
                sb.appendHtmlConstant(imageHtml).appendEscaped(" ");
                sb.appendEscaped(value.getDisplayName());
            }
        }
    }

    /**
     * Tracks the number of contacts in a category that begin with the same
     * letter.
     */
    private static class LetterCount implements Comparable<LetterCount> {
        private final ContactDatabase.Category category;
        private final char firstLetter;
        private int count;

        /**
         * Construct a new {@link LetterCount} for one contact.
         *
         * @param category    the category
         * @param firstLetter the first letter of the contacts name
         */
        public LetterCount(ContactDatabase.Category category, char firstLetter) {
            this.category = category;
            this.firstLetter = firstLetter;
            this.count = 1;
        }

        public int compareTo(LetterCount o) {
            return (o == null) ? -1 : (firstLetter - o.firstLetter);
        }

        @Override
        public boolean equals(Object o) {
            return compareTo((LetterCount) o) == 0;
        }

        @Override
        public int hashCode() {
            return firstLetter;
        }

        /**
         * Increment the count.
         */
        public void increment() {
            count++;
        }
    }

    /**
     * A Cell used to render the LetterCount.
     */
    private static class LetterCountCell extends AbstractCell<LetterCount> {

        @Override
        public void render(Context context, LetterCount value, SafeHtmlBuilder sb) {
            if (value != null) {
                sb.appendEscaped(value.firstLetter + " (" + value.count + ")");
            }
        }
    }

    /**
     * The static images used in this model.
     */
    private static Images images;

    private final ListDataProvider<ContactDatabase.Category> categoryDataProvider;
    private final Cell<ContactDatabase.ContactInfo> contactCell;
    private final DefaultSelectionEventManager<ContactDatabase.ContactInfo> selectionManager =
            DefaultSelectionEventManager.createCheckboxManager();
    private final SelectionModel<ContactDatabase.ContactInfo> selectionModel;

    public ContactTreeViewModel(final SelectionModel<ContactDatabase.ContactInfo> selectionModel) {
        this.selectionModel = selectionModel;
        if (images == null) {
            images = GWT.create(Images.class);
        }

        // Create a data provider that provides categories.
        categoryDataProvider = new ListDataProvider<ContactDatabase.Category>();
        List<ContactDatabase.Category> categoryList = categoryDataProvider.getList();
        for (ContactDatabase.Category category : ContactDatabase.get().queryCategories()) {
            categoryList.add(category);
        }

        // Construct a composite cell for contacts that includes a checkbox.
        List<HasCell<ContactDatabase.ContactInfo, ?>> hasCells = new ArrayList<HasCell<ContactDatabase.ContactInfo, ?>>();
        hasCells.add(new HasCell<ContactDatabase.ContactInfo, Boolean>() {

            private CheckboxCell cell = new CheckboxCell(true, false);

            public Cell<Boolean> getCell() {
                return cell;
            }

            public FieldUpdater<ContactDatabase.ContactInfo, Boolean> getFieldUpdater() {
                return null;
            }

            public Boolean getValue(ContactDatabase.ContactInfo object) {
                return selectionModel.isSelected(object);
            }
        });
        hasCells.add(new HasCell<ContactDatabase.ContactInfo, ContactDatabase.ContactInfo>() {

            //private ContactCell cell = new ContactCell(images.contact());

            public Cell<ContactDatabase.ContactInfo> getCell() {
                return null;
            }

            public FieldUpdater<ContactDatabase.ContactInfo, ContactDatabase.ContactInfo> getFieldUpdater() {
                return null;
            }

            public ContactDatabase.ContactInfo getValue(ContactDatabase.ContactInfo object) {
                return object;
            }
        });
        contactCell = new CompositeCell<ContactDatabase.ContactInfo>(hasCells) {
            @Override
            public void render(Context context, ContactDatabase.ContactInfo value, SafeHtmlBuilder sb) {
                sb.appendHtmlConstant("<table><tbody><tr>");
                super.render(context, value, sb);
                sb.appendHtmlConstant("</tr></tbody></table>");
            }

            @Override
            protected Element getContainerElement(Element parent) {
                // Return the first TR element in the table.
                return parent.getFirstChildElement().getFirstChildElement().getFirstChildElement();
            }

            @Override
            protected <X> void render(Context context, ContactDatabase.ContactInfo value,
                                      SafeHtmlBuilder sb, HasCell<ContactDatabase.ContactInfo, X> hasCell) {
                Cell<X> cell = hasCell.getCell();
                sb.appendHtmlConstant("<td>");
                cell.render(context, hasCell.getValue(value), sb);
                sb.appendHtmlConstant("</td>");
            }
        };
    }

    public <T> NodeInfo<?> getNodeInfo(T value) {
        if (value == null) {
            // Return top level categories.
            return new DefaultNodeInfo<ContactDatabase.Category>(categoryDataProvider,
                    new CategoryCell(images.contactsGroup()));
        } else if (value instanceof ContactDatabase.Category) {
            // Return the first letters of each first name.
            ContactDatabase.Category category = (ContactDatabase.Category) value;
            List<ContactDatabase.ContactInfo> contacts = ContactDatabase.get().queryContactsByCategory(
                    category);
            Map<Character, LetterCount> counts = new TreeMap<Character, LetterCount>();
            for (ContactDatabase.ContactInfo contact : contacts) {
                Character first = contact.getFirstName().charAt(0);
                LetterCount count = counts.get(first);
                if (count == null) {
                    count = new LetterCount(category, first);
                    counts.put(first, count);
                } else {
                    count.increment();
                }
            }
            List<LetterCount> orderedCounts = new ArrayList<LetterCount>(
                    counts.values());
            return new DefaultNodeInfo<LetterCount>(
                    new ListDataProvider<LetterCount>(orderedCounts),
                    new LetterCountCell());
        } else if (value instanceof LetterCount) {
            // Return the contacts with the specified character and first name.
            LetterCount count = (LetterCount) value;
            List<ContactDatabase.ContactInfo> contacts = ContactDatabase.get().queryContactsByCategoryAndFirstName(
                    count.category, count.firstLetter + "");
            ListDataProvider<ContactDatabase.ContactInfo> dataProvider = new ListDataProvider<ContactDatabase.ContactInfo>(
                    contacts, ContactDatabase.ContactInfo.KEY_PROVIDER);
            return new DefaultNodeInfo<ContactDatabase.ContactInfo>(
                    dataProvider, contactCell, selectionModel, selectionManager, null);
        }

        // Unhandled type.
        String type = value.getClass().getName();
        throw new IllegalArgumentException("Unsupported object type: " + type);
    }

    public boolean isLeaf(Object value) {
        return value instanceof ContactDatabase.ContactInfo;
    }
}