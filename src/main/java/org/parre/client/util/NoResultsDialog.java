/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.util;

import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;

/**
 * .
 * User: Swarn S. Dhaliwal
 * Date: 9/17/11
 * Time: 6:22 AM
 */
public class NoResultsDialog {
    private static DialogBox instance = createDialogBox();

    private static DialogBox createDialogBox() {
        DialogBox dialogBox = new ClosablePopup("No Results", true);
        dialogBox.setModal(true);
        dialogBox.setTitle("No Results");
        dialogBox.setWidget(new Label("Query found no results"));
        dialogBox.hide();
        return dialogBox;
    }

    public static DialogBox getInstance() {
        return instance;
    }

    public static void show(Widget widget) {
        instance.showRelativeTo(widget);
        instance.show();
    }

    public static void show() {
        instance.center();
        instance.show();
    }
    public static void hide() {
        instance.hide();
    }
}
