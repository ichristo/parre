/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.util;

/**
 * The Class PostalCodeValidator.
 */
public class PostalCodeValidator extends RegularExpressionValidator {

	public final static PostalCodeValidator INSTANCE = new PostalCodeValidator();
	
	@Override
	public String getMessage() {
		return "Please enter a 5 digit postal code.";
	}

	@Override
	protected String getPattern() {
		return "\\d{5}";
	}

}
