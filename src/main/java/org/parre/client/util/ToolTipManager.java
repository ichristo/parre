/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.util;

import com.google.gwt.event.dom.client.*;
import com.google.gwt.event.logical.shared.CloseEvent;
import com.google.gwt.event.logical.shared.CloseHandler;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.UIObject;

import java.util.HashMap;
import java.util.Map;

/**
 * User: Swarn S. Dhaliwal
 * Date: 8/4/11
 * Time: 4:01 PM
*/
public class ToolTipManager extends PopupPanel {

	private static final int VISIBLE_DELAY = 2000;

	private static Timer removeDelay;

    private static final  Map<Object, String> toolTipMessageMap = new HashMap<Object, String>();
    private static final PopupPanel toolTipPopup = new PopupPanel();
    private static final Label toolTipLabel = new Label("ToolTip");

    static {
        toolTipPopup.setWidget(toolTipLabel);
        toolTipPopup.setStyleName("toolTip");
        toolTipPopup.addCloseHandler(new CloseHandler<PopupPanel>() {
            public void onClose(CloseEvent<PopupPanel> popupPanelCloseEvent) {
                removeDelay.cancel();
            }
        });
    }
    private static final MouseOverHandler toolTipShowHandler = new MouseOverHandler() {
        public void onMouseOver(MouseOverEvent event) {
            removeDelay = new Timer() {
                public void run() {
                    toolTipPopup.hide();
                }
            };
            removeDelay.schedule(VISIBLE_DELAY);
            UIObject source = (UIObject) event.getSource();
            toolTipLabel.setText(toolTipMessageMap.get(source));
            toolTipPopup.showRelativeTo(source);
        }
    };

    private static final MouseOutHandler toolTipHideHandler = new MouseOutHandler() {
        public void onMouseOut(MouseOutEvent event) {
            hideToolTip();
        }
    };

    private static final MouseDownHandler toolTipHideHandler1 = new MouseDownHandler() {

        public void onMouseDown(MouseDownEvent event) {
            hideToolTip();
        }
    };

    private static void hideToolTip() {
        toolTipPopup.hide();
        removeDelay.cancel();
    }

    public static void addToolTip(UIObject uiObject, String message) {
        toolTipMessageMap.put(uiObject, message);
        ((HasMouseOverHandlers) uiObject).addMouseOverHandler(toolTipShowHandler);
        ((HasMouseOutHandlers) uiObject).addMouseOutHandler(toolTipHideHandler);
        ((HasMouseDownHandlers) uiObject).addMouseDownHandler(toolTipHideHandler1);
    }
}