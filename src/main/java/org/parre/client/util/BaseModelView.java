/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.util;

import java.math.BigDecimal;

import com.google.gwt.user.client.ui.HasText;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextBox;

/**
 * .
 * User: Swarn S. Dhaliwal
 * Date: 9/7/11
 * Time: 11:41 PM
 *
 * @param <T> the generic type
 */
public abstract class BaseModelView<T> extends BaseView implements ModelView<T> {
    private T model;
    public BaseModelView() {
    }

    public static TextBox createProbabilityBox() {
        return makeProbabilityBox(new TextBox());
    }

    public static TextBox makeProbabilityBox(TextBox probability) {
        probability.setWidth("70px");
        probability.setMaxLength(9);
        return probability;
    }

    public void setModel(T model) {
        this.model = model;
    }

    public void setModelAndUpdateView(T model) {
        setModel(model);
        updateView();
    }

    public T getModel() {
        return model;
    }

    public T updateAndGetModel() {
        updateModel();
        return model;
    }

    protected BigDecimal getBigDecimal(HasText hasText) {
        return ViewUtil.getBigDecimal(hasText);
    }

    protected BigDecimal getProbability(HasText hasText) {
        return ViewUtil.parseProbability(ViewUtil.getText(hasText));
    }

    protected void setProbability(HasText hasText, BigDecimal bigDecimal) {
        hasText.setText(ViewUtil.formatProbability(bigDecimal != null ? bigDecimal : BigDecimal.ZERO));
    }

    protected void setBigDecimal(HasText hasText, BigDecimal bigDecimal) {
        ViewUtil.setBigDecimal(hasText, bigDecimal);
    }

    protected void setText(HasText hasText, String value) {
        ViewUtil.setText(hasText, value);
    }

    protected String getText(HasText hasText) {
        return ViewUtil.getText(hasText);
    }

    protected Integer getQuantity(HasText hasText) {
        return ViewUtil.getQuantity(hasText);
    }

    protected void setQuantity(HasText hasText, Integer quantity) {
        ViewUtil.setQuantity(hasText, quantity);
    }

    protected String getSelectedValue(ListBox listBox) {
        return ViewUtil.getSelectedValue(listBox);
    }

    protected Long getSelectedId(ListBox listBox) {
        return Long.valueOf(ViewUtil.getSelectedValue(listBox));
    }

    protected void setSelectedValue(ListBox listBox, String value) {
        ViewUtil.setSelectedValue(listBox, value);
    }

    protected void setSelectedId(ListBox listBox, Long value) {
        ViewUtil.setSelectedValue(listBox, String.valueOf(value));
    }
}
