/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.util;

/**
 * .
 * User: Swarn S. Dhaliwal
 * Date: 9/17/11
 * Time: 3:19 PM
 */

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.*;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.ui.*;

/**
 * The Class Spinner.
 */
public class Spinner extends HorizontalPanel {

    private TextBox textBox = new TextBox();
    private int step = 20;
    private int max = 0;
    private int min = 0;
    private int curVal = 0;
    // delay to wait before increasing/decreasing further, in milliseconds
    private int delay = 120;

    Timer t_raise = new Timer() {
        public void run() {
            raise();
        }
    };
    Timer t_lower = new Timer() {
        public void run() {
            lower();
        }
    };


    public Spinner(int minVal, int maxVal, int stepVal, int startVal) {
        Image upImage = new Image(GWT.getHostPageBaseURL() + "images/up.gif");
        upImage.setPixelSize(10, 10);
        Image downImage = new Image(GWT.getHostPageBaseURL() + "images/down.gif");
        downImage.setPixelSize(10, 10);

        // Take over parameters
        step = stepVal;
        max = maxVal;
        min = minVal;

        // set curVal inside min or max bounds
        if (startVal < min) {
            curVal = min;
        } else if (startVal > max) {
            curVal = max;
        } else {
            curVal = startVal;
        }

        // Build our UI
        VerticalPanel images = new VerticalPanel();
        images.add(upImage);
        upImage.addMouseDownHandler(new MouseDownHandler() {
            public void onMouseDown(MouseDownEvent event) {
                t_raise.run();
                t_raise.scheduleRepeating(delay);
            }
        });
        upImage.addMouseUpHandler(new MouseUpHandler() {
            public void onMouseUp(MouseUpEvent event) {
                t_raise.cancel();
            }
        });

        images.add(downImage);
        downImage.addMouseDownHandler(new MouseDownHandler() {
            public void onMouseDown(MouseDownEvent event) {
                t_lower.run();
                t_lower.scheduleRepeating(delay);
            }
        });
        downImage.addMouseUpHandler(new MouseUpHandler() {
            public void onMouseUp(MouseUpEvent event) {
                t_lower.cancel();
            }
        });

        updateTextBox();
        textBox.addKeyPressHandler(new KeyPressHandler() {
            public void onKeyPress(KeyPressEvent event) {
                TextBox tb = (TextBox) event.getSource();
                int index = tb.getCursorPos();
                String str = tb.getText();

                // If no number has been entered, ignore it
                char keyCode = event.getCharCode();
                if (!Character.isDigit(keyCode)) {
                    tb.cancelKey();
                    return;
                }

                // If we have 0123 as input and 123 is selected, we need to
                // calculate with "09" if "9" was entered
                String newstr;
                if (tb.getSelectionLength() > 0) {
                    //str = str.replaceFirst(str.substring(tb.getCursorPos(), tb.getSelectionLength()+1), "");
                    newstr = str.substring(0, tb.getCursorPos());
                    newstr += keyCode;
                    newstr += str.substring(tb.getCursorPos() + tb.getSelectionLength(), str.length());

                } else {
                    newstr = str.substring(0, index) + keyCode + str.substring(index, str.length());
                }

                int newint = Integer.parseInt(newstr);

                // If we are not inside the bounds, leave this method
                // TODO: Choose, whether to display the maxvalue or leave
                if (newint > max || newint < min) {
                    tb.cancelKey();
                    return;
                }

                curVal = newint;

            }
        });

        // set width analog to max length of the maximum value
        textBox.setWidth((((max + "").length() + 1) / 2) + 1 + "em");
        textBox.setAlignment(ValueBoxBase.TextAlignment.RIGHT);
        add(textBox);
        add(images);
    }

    private void updateTextBox() {
        textBox.setText(curVal + "");
        if (curVal == max) {
            t_raise.cancel();
        } else if (curVal == min) {
            t_lower.cancel();
        }
    }

    private void raise() {
        if (curVal + step < max) {
            curVal += step;
        } else {
            curVal = max;
        }
        updateTextBox();
    }

    private void lower() {
        if (curVal - step > min) {
            curVal -= step;
        } else {
            curVal = min;
        }
        updateTextBox();
    }

    public void setDelay(int delayMilliSeconds) {
        delay = delayMilliSeconds;
    }

    public TextBox getTextBox() {
        return textBox;
    }

    public int getValue() {
        ClientLoggingUtil.info("Spinner Value=" + curVal);
        return curVal;
    }

    public void reset() {
        curVal = min;
        updateTextBox();
    }
}
