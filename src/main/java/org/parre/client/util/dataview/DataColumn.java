/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.util.dataview;

import com.google.gwt.dom.client.Style;
import com.google.gwt.safehtml.shared.SafeHtml;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;
import com.google.gwt.user.cellview.client.Column;

import java.util.Comparator;

/**
 * The Class DataColumn.
 *
 * @param <T> the generic type
 * @author Swarn S. Dhaliwal
 * @version 1.0 9/22/11
 */
public class DataColumn<T> {
    private SafeHtml columnHeader;
    private Column column;
    private Comparator<T> sortComparator;
    private int columnWidth;
    private Style.Unit columnWidthUnit;

    public DataColumn(String columnHeader, Column column, Comparator<T> sortComparator, int columnWidth) {
        this(columnHeader, column, sortComparator, columnWidth,  Style.Unit.PCT);
    }
    public DataColumn(SafeHtml columnHeader, Column column, Comparator<T> sortComparator, int columnWidth) {
        this(columnHeader, column, sortComparator, columnWidth,  Style.Unit.PCT);
    }

    public DataColumn(String columnHeader, Column column, Comparator<T> sortComparator, int columnWidth, Style.Unit columnWidthUnit) {
        this(toSafeHtml(columnHeader), column, sortComparator, columnWidth, columnWidthUnit);
    }

    private static SafeHtml toSafeHtml(String columnHeader) {
        return new SafeHtmlBuilder().appendEscaped(columnHeader).toSafeHtml();
    }

    public DataColumn(SafeHtml columnHeader, Column column, Comparator<T> sortComparator, int columnWidth, Style.Unit columnWidthUnit) {
        this.columnHeader = columnHeader;
        this.column = column;
        this.sortComparator = sortComparator;
        this.columnWidth = columnWidth;
        this.columnWidthUnit = columnWidthUnit;
    }

    public SafeHtml getColumnHeader() {
        return columnHeader;
    }

    public Column getColumn() {
        return column;
    }

    public Comparator<T> getSortComparator() {
        return sortComparator;
    }

    public int getColumnWidth() {
        return columnWidth;
    }

    public Style.Unit getColumnWidthUnit() {
        return columnWidthUnit;
    }
}
