/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.util.dataview;

import com.google.gwt.view.client.HasData;
import com.google.gwt.view.client.ListDataProvider;
import com.google.gwt.view.client.ProvidesKey;

import java.util.List;

/**
 * User: Swarn S. Dhaliwal
 * Date: 7/10/11
 * Time: 1:05 PM
|| *
 * @param <T> the generic type
 */
public class DataProvider<T> {
    /**
     * The key provider that provides the unique ID of a contact.
     */
    private ListDataProvider<T> dataProvider;
    private ProvidesKey<T> keyProvider;
    public DataProvider(ProvidesKey<T> keyProvider, ListDataProvider<T> dataProvider) {
        this.keyProvider = keyProvider;
        this.dataProvider = dataProvider;
    }

    public ProvidesKey<T> getKeyProvider() {
        return keyProvider;
    }

    public void setData(List<T> data) {
        dataProvider.getList().clear();
        dataProvider.getList().addAll(data);
    }

    public void addDataDisplay(HasData<T> dataDisplay) {
        dataProvider.addDataDisplay(dataDisplay);
    }

    public ListDataProvider<T> getDataProvider() {
        return dataProvider;
    }
    /**
     * Refresh all displays.
     */
    public void refreshDisplays() {
        dataProvider.refresh();
    }

}
