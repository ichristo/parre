/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.util.dataview;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style;
import com.google.gwt.safehtml.shared.SafeHtml;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;
import com.google.gwt.user.cellview.client.ColumnSortEvent;
import com.google.gwt.user.cellview.client.DataGrid;
import com.google.gwt.user.cellview.client.SimplePager;
import com.google.gwt.user.client.ui.*;
import com.google.gwt.view.client.SingleSelectionModel;

import java.util.List;

import org.parre.client.util.ActionSource;
import org.parre.client.util.ClientLoggingUtil;
import org.parre.client.util.ViewUtil;
import org.parre.shared.ThreatDTO;

/**
 * User: Swarn S. Dhaliwal
 * Date: 7/10/11
 * Time: 5:38 PM
|| *
 * @param <T> the generic type
 */
public class DataGridView<T> {

    private DataGrid<T> dataGrid;
    private SimplePager pager;
    private DataProvider<T> dataProvider;
    private SingleSelectionModel<T> selectionModel;
    private ActionSource<T> selectionSource = new ActionSource<T>();
    private ColumnSortEvent.ListHandler<T> sortHandler;
    private Widget viewWidget;
    private DockLayoutPanel layoutPanel;
    private ColumnSortEvent lastSortEvent;

    public DataGridView(DataProvider<T> dataProvider) {
        this.dataProvider = dataProvider;
        viewWidget = createView();
    }

    private Widget createView() {
        layoutPanel = new DockLayoutPanel(Style.Unit.EM);
        dataGrid = new DataGrid<T>(dataProvider.getKeyProvider());
        dataGrid.setWidth("100%");
        dataGrid.setHeight("100%");
        dataGrid.setEmptyTableWidget(new HTML("No Results"));
        // Attach a column sort handler to the ListDataProvider to sort the list.
        sortHandler = new ColumnSortEvent.ListHandler<T>(
                dataProvider.getDataProvider().getList()) {
            @Override
            public void onColumnSort(ColumnSortEvent event) {
                lastSortEvent = event;
                super.onColumnSort(event);
            }
        };
        dataGrid.addColumnSortHandler(sortHandler);

        // Create a Pager to control the table.
        SimplePager.Resources pagerResources = GWT.create(SimplePager.Resources.class);
        pager = new SimplePager(SimplePager.TextLocation.RIGHT, pagerResources, false, 0, true);
        pager.setDisplay(dataGrid);
        dataGrid.setPageSize(100);
        // Add a selection model so we can select cells.
        selectionModel = new SingleSelectionModel<T>(dataProvider.getKeyProvider());
        dataGrid.setSelectionModel(selectionModel);

        dataProvider.addDataDisplay(dataGrid);

        HorizontalPanel panel = ViewUtil.layoutCenter(pager);
        layoutPanel.addSouth(panel, 3);
        layoutPanel.add(dataGrid);
        layoutPanel.setHeight("500px");
        VerticalPanel verticalPanel = new VerticalPanel();
        verticalPanel.setStyleName("inputPanel");
        //verticalPanel.setBorderWidth(1);
        verticalPanel.add(layoutPanel);
        return verticalPanel;
    }

    public void setHeight(String height) {
        layoutPanel.setHeight(height);
        layoutPanel.forceLayout();
    }

    public void setWidth(String width) {
        layoutPanel.setWidth(width);
        layoutPanel.forceLayout();
    }
    public void setPageSize(int size) {
        dataGrid.setPageSize(size);
    }

    public Widget asWidget() {
        return viewWidget;
    }
    public void setData(List<T> dataList) {
        dataProvider.setData(dataList);
        resort();
    }

    private void resort() {
        if (lastSortEvent != null) {
            dataGrid.fireEvent(lastSortEvent);
        }
    }

    public ActionSource<T> getSelectionSource() {
        return selectionSource;
    }

    public SingleSelectionModel<T> getSelectionModel() {
        return selectionModel;
    }

    public void refresh() {
        dataProvider.refreshDisplays();
        resort();
    }

    protected void addColumns(List<DataColumn<T>> columnList) {
        for (DataColumn searchResultColumn : columnList) {
            sortHandler.setComparator(searchResultColumn.getColumn(), searchResultColumn.getSortComparator());
            dataGrid.addColumn(searchResultColumn.getColumn(), searchResultColumn.getColumnHeader());
            dataGrid.setColumnWidth(searchResultColumn.getColumn(), searchResultColumn.getColumnWidth(), searchResultColumn.getColumnWidthUnit());
        }
    }

    protected void addDisplayColumns(List<DataColumn<T>> columnList) {
        for (DataColumn searchResultColumn : columnList) {
           // sortHandler.setComparator(searchResultColumn.getColumn(), searchResultColumn.getSortComparator());
            dataGrid.addColumn(searchResultColumn.getColumn(), searchResultColumn.getColumnHeader());
            dataGrid.setColumnWidth(searchResultColumn.getColumn(), searchResultColumn.getColumnWidth(), searchResultColumn.getColumnWidthUnit());
        }
    }
    
    public List<T> getData() {
        return dataProvider.getDataProvider().getList();
    }

    public void setData(int index, T object) {
        dataProvider.getDataProvider().getList().set(index, object);
        resort();
    }

    public void updateData(T object) {
        int index = dataProvider.getDataProvider().getList().indexOf(object);
        if (index >= 0) {
            dataProvider.getDataProvider().getList().set(index, object);
        }
        else {
            dataProvider.getDataProvider().getList().add(object);
        }
        resort();
    }
    public void removeData(T object) {
        dataProvider.getDataProvider().getList().remove(object);
    }

    protected DataGrid<T> getDataGrid() {
        return dataGrid;
    }

    protected void clearGrid() {
        int columnCount = dataGrid.getColumnCount();
        ClientLoggingUtil.info("Removing Columns : " + columnCount);
        while (columnCount > 0) {
            dataGrid.removeColumn(columnCount - 1);
            columnCount = dataGrid.getColumnCount();
        }
        ClientLoggingUtil.info("Removed Columns");
        sortHandler.getList().clear();
        ClientLoggingUtil.info("Cleared Sort Handler List");

    }

    public void forceLayout() {
        layoutPanel.forceLayout();
    }
}
