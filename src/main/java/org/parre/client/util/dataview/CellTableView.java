/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.util.dataview;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.cellview.client.CellTable;
import com.google.gwt.user.cellview.client.ColumnSortEvent;
import com.google.gwt.user.cellview.client.SimplePager;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.view.client.SingleSelectionModel;

import java.util.List;

import org.parre.client.util.ActionSource;

/**
 * User: Swarn S. Dhaliwal
 * Date: 7/10/11
 * Time: 5:38 PM
|| *
 * @param <T> the generic type
 */
public class CellTableView<T> extends VerticalPanel {

    private CellTable resultsTable;
    private SimplePager pager;
    private DataProvider<T> dataProvider;
    private SingleSelectionModel<T> selectionModel;
    private ActionSource<T> selectionSource = new ActionSource<T>();
    private ColumnSortEvent.ListHandler<T> sortHandler;

    public CellTableView(DataProvider dataProvider) {
        this.dataProvider = dataProvider;
        resultsTable = new CellTable<T>(dataProvider.getKeyProvider());
        resultsTable.setWidth("100%", true);
        resultsTable.setPageSize(10);
        // Attach a column sort handler to the ListDataProvider to sort the list.
        sortHandler = new ColumnSortEvent.ListHandler<T>(
                dataProvider.getDataProvider().getList());
        resultsTable.addColumnSortHandler(sortHandler);

        // Create a Pager to control the table.
        SimplePager.Resources pagerResources = GWT.create(SimplePager.Resources.class);
        pager = new SimplePager(SimplePager.TextLocation.RIGHT, pagerResources, false, 0, true);
        pager.setDisplay(resultsTable);

        // Add a selection model so we can select cells.
        selectionModel = new SingleSelectionModel<T>(dataProvider.getKeyProvider());
        resultsTable.setSelectionModel(selectionModel);

        dataProvider.addDataDisplay(resultsTable);

        add(resultsTable);
        add(pager);
        setWidth("100%");
        setHeight("100%");
        setBorderWidth(1);
    }

    public void setSearchResults(List<T> dataList) {
        dataProvider.setData(dataList);
    }

    public ActionSource<T> getSelectionSource() {
        return selectionSource;
    }

    public SingleSelectionModel<T> getSelectionModel() {
        return selectionModel;
    }

    public void refresh() {
        dataProvider.refreshDisplays();
    }

    protected void addColumns(List<DataColumn<T>> columnList) {
        for (DataColumn searchResultColumn : columnList) {
            sortHandler.setComparator(searchResultColumn.getColumn(), searchResultColumn.getSortComparator());
            resultsTable.addColumn(searchResultColumn.getColumn(), searchResultColumn.getColumnHeader());
            resultsTable.setColumnWidth(searchResultColumn.getColumn(), searchResultColumn.getColumnWidth(), searchResultColumn.getColumnWidthUnit());
        }
    }

}
