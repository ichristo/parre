/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.util.dataview;


import com.google.gwt.core.shared.GWT;
import com.google.gwt.safehtml.shared.SafeHtml;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;

/**
 * The Class DataGridHeaders.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 12/12/11
 */
public class DataGridHeaders {

    public static SafeHtml createHeader(String line1, String line2) {
        SafeHtmlBuilder headerBuilder = new SafeHtmlBuilder();
        headerBuilder.appendEscaped(line1).appendHtmlConstant("<br/>").appendEscaped(line2);
        return headerBuilder.toSafeHtml();
    }
    
    public static SafeHtml createBigHeader(String line1, String line2) {
        SafeHtmlBuilder headerBuilder = new SafeHtmlBuilder();
        int optionNameLength = line1.length();
        if(optionNameLength > 24){
        	String updatedLine1 = splitOptionName(line1, 24);
        	String[] tokens = updatedLine1.split("<br/>");
        	for (String string : tokens) {
        		headerBuilder.appendEscaped(string).appendHtmlConstant("<br/>");
			}
        } else{
        	headerBuilder.appendEscaped(line1).appendHtmlConstant("<br/>");
        } 
        headerBuilder.appendEscaped(line2);
        return headerBuilder.toSafeHtml();
    }


    
    private static String splitOptionName(String input, int maxLineLength) {
		 String NEWLINE = "<br/>";
		 String SPACE_SEPARATOR = " ";
		
		String[] tokens = input.split(SPACE_SEPARATOR);
	    StringBuilder output = new StringBuilder(input.length());
	    int lineLen = 0;
	    for (int i = 0; i < tokens.length; i++) {
	        String word = tokens[i];

	        if (lineLen + (SPACE_SEPARATOR + word).length() > maxLineLength) {
	            if (i > 0) {
	                output.append(NEWLINE);
	            }
	            lineLen = 0;
	        }
	        if (i < tokens.length - 1 && (lineLen + (word + SPACE_SEPARATOR).length() + tokens[i + 1].length() <= maxLineLength)) {
	            word += SPACE_SEPARATOR;
	        }
	        output.append(word);
	        lineLen += word.length();
	    }
	    return output.toString();
	}
    
    
	public static SafeHtml createHeader(String line1, String line2, String line3) {
        SafeHtmlBuilder headerBuilder = new SafeHtmlBuilder();
        headerBuilder.appendEscaped(line1).appendHtmlConstant("<br/>").appendEscaped(line2).appendHtmlConstant("<br/>").appendEscaped(line3);
        return headerBuilder.toSafeHtml();
    }

    public static SafeHtml seriousInjuries() {
        return createHeader("Serious", "Injuries");
    }

    public static SafeHtml financialImpact() {
        return createHeader("Owners", "Financial", "Impact ($)");
    }

    public static SafeHtml financialResilience() {
        return createHeader("Financial", "Resilience ($)");
    }

    public static SafeHtml totalRisk() {
        return createHeader("Total", "Risk ($)");
    }

    public static SafeHtml financialTotal() {
        return createHeader("Financial", "Total ($)");
    }

    public static SafeHtml economicImpact() {
        return createHeader("Community's", "Economic", "Impact ($)");
    }

    public static SafeHtml economicResilience() {
        return createHeader("Economic", "Resilience ($)");
    }

    public static SafeHtml threatLikelihood() {
        return createHeader("Threat", "Likelihood");
    }
    
    public static SafeHtml operationalLoss() {
    	return createHeader("Operational", "Loss ($)");
    }

}
