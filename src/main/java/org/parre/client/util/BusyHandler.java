/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.util;

/**
 * .
 * User: Swarn S. Dhaliwal
 * Date: 11/11/11
 * Time: 4:26 PM
 */
public class BusyHandler {
    private boolean busy;
    private View parentView;

    public BusyHandler(View parentView) {
        this.parentView = parentView;
    }

    public final boolean isBusy() {
        return busy;
    }

    public final void setBusy(boolean busy) {
        this.busy = busy;
        if (busy) {
            GWTUtil.setWaitCursor(parentView.asWidget());
            ModalProgressDialog.show();
        } else {
            GWTUtil.setNormalCursor(parentView.asWidget());
            ModalProgressDialog.hide();
        }
        parentView.setEnabled(!busy);
    }
}
