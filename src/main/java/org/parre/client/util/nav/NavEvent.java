/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.util.nav;

import com.google.gwt.event.shared.EventHandler;
import com.google.gwt.event.shared.GwtEvent;


/**
 * The Class NavEvent.
 *
 * @param <H> the generic type
 * @author Swarn S. Dhaliwal
 * @version 1.0 3/27/12
 */
public abstract class NavEvent<H extends EventHandler> extends GwtEvent<H> {
    public static final String DTO_SUFFIX = "DTO";
    public static final String EVENT_SUFFIX = "Event";
    private NavState navState;
    private String navToken;
    private String navViewId;

    protected NavEvent(String navViewId) {
        this.navViewId = navViewId;
        navToken = initNavToken();
        navState = NullNavState.INSTANCE;
    }

    public NavEvent(Class viewClass) {
        this(viewClass.getName());
    }

    public String getNavToken() {
        return navToken;
    }

    private String initNavToken() {
        String token = getSimpleClassName(getClass());
        token = stripSuffix(token, EVENT_SUFFIX);
        return token;
    }

    protected String stripSuffix(String token, String suffix) {
        if (token.endsWith(suffix)) {
            token = token.substring(0, token.lastIndexOf(suffix));
        }
        return token;
    }

    public boolean hasNavState() {
        return navState != NullNavState.INSTANCE;
    }

    public NavState getNavState() {
        return navState;
    }

    public void setNavState(NavState navState) {
        this.navState = navState!= null ? navState : NullNavState.INSTANCE;
    }

    public String getNavViewId() {
        return navViewId;
    }

    public static String getSimpleClassName(Class<?> aClass) {
        String className = aClass.getName();
        return className.substring(className.lastIndexOf('.') + 1);
    }

}
