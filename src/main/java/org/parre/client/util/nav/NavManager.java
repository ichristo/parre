/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.util.nav;

import org.parre.client.util.Base64;
import org.parre.client.util.ClientLoggingUtil;

import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.History;

/**
 * The Class NavManager.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 3/27/12
 */
public class NavManager {
    private static HandlerRegistration handlerRegistration;
    private static NavAdapter navAdapter;
    private static NavEventMapper navEventMapper;
    public static void start(final NavAdapter navAdapter, final NavEventMapper navEventMapper) {
        if (handlerRegistration != null) {
            return;
        }
        NavManager.navAdapter = navAdapter;
        NavManager.navEventMapper = navEventMapper;
        handlerRegistration = History.addValueChangeHandler(new ValueChangeHandler<String>() {
            public void onValueChange(ValueChangeEvent<String> event) {
                String historyToken = event.getValue();
                handleHistoryToken(historyToken);
            }
        });
        ClientLoggingUtil.info("NavManager started");
    }

    private static void handleHistoryToken(String historyToken) {
        ClientLoggingUtil.info("History Changed : '" + historyToken + "'");
        if (historyToken.length() == 0) {
            addHistoryItem(navEventMapper.getDefaultNavEvent());
        }
        else {
            performHistoryNav(historyToken);
        }
    }

    public static void performNav(NavEvent navEvent) {
        addHistoryItem(navEvent);
        navAdapter.onNavEvent(navEvent);
    }

    private static void performHistoryNav(String navToken) {
        NavEvent navEvent = createNavEvent(navToken);
        if (navEvent == null) {
            return;
        }
        navAdapter.onNavEvent(navEvent);
    }

    public static void addHistoryItem(NavEvent navEvent) {
        String navToken = navEventMapper.getNavToken(navEvent);
        if (navEvent.hasNavState()) {
            navToken = navToken + "~" + encode(navEvent.getNavState().toStringValue());
        }
        History.newItem(navToken, false);
    }

    private static String encode(String navState) {
        return Base64.encode(navState);
    }

    private static String decode(String data) {
        return Base64.decode(data);
    }

    private static NavEvent createNavEvent(String navToken) {
        ClientLoggingUtil.info("Navigating to : " + navToken);
        int index = navToken.indexOf('~');
        String token = index >= 0 ? navToken.substring(0, index) : navToken;
        NavEvent navEvent = navEventMapper.getNavEvent(token);
        if (navEvent == null) {
            ClientLoggingUtil.info("No event found for token : " + token);
            addHistoryItem(navEventMapper.getDefaultNavEvent());
            return null;
        }
        if (index >= 0) {
            String decode = decode(navToken.substring(index + 1));
            navEvent.getNavState().fromStringValue(decode);
        }
        return navEvent;
    }

    public static void performEntryPointNav() {
        if (!navAdapter.hasEntryPointHash()) {
            return;
        }
        NavEvent navEvent = createNavEvent(navAdapter.getEntryPointHashValue());
        if (navEvent == null) {
            return;
        }
        navAdapter.handleEntryPointNavEvent(navEvent);
    }
}
