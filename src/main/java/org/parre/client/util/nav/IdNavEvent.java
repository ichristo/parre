/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.util.nav;

import com.google.gwt.event.shared.EventHandler;

/**
 * The Class IdNavEvent.
 *
 * @param <H> the generic type
 */
public abstract class IdNavEvent<H extends EventHandler> extends NavEvent<H> {

    private IdNavState navState;

    protected IdNavEvent(Long id, Class navView) {
        super(navView);
        if (id == null) {
            id = 0L;
        }
        navState = new IdNavState(id);
        setNavState(navState);
    }

    public Long getId() {
        return hasId() ? navState.getId() : null;
    }

    public boolean hasId() {
        return navState.getId() != 0;
    }

}
