/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.util.nav;

/**
 * This interface acts as a bridge between the general purpose navigation handling and an app specific handling. Typically
 * and app will provide a singleton implementation of this at the time when the NavManager is started.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 4/5/12
 */
public interface NavAdapter {
    boolean hasEntryPointHash();
    String getEntryPointHashValue();
    void onNavEvent(NavEvent navEvent);
    void handleEntryPointNavEvent(NavEvent navEvent);
}
