/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.util.nav;

/**
 * .
 * User: Swarn S. Dhaliwal
 * Date: 4/1/12
 * Time: 5:16 PM
 */
public class IdNavState implements NavState {
    private Long id;

    public IdNavState(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public String toStringValue() {
        return String.valueOf(id);
    }

    public void fromStringValue(String value) {
        id = Long.valueOf(value);
    }
}
