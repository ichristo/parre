/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.util;

import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.UIObject;

/**
 * The Class MessageDialog.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 9/28/11
 */
public class MessageDialog {
    private static ClosablePopup popupPanel = new ClosablePopup("Info", true);
    private static HTML messageContent = new HTML();
    private static int defaultWidth = 300;
  
    
    static {
        messageContent.setWidth(defaultWidth + "px");
        messageContent.setStyleName("normalLabel");
       // popupPanel.setWidget(messageContent);
    }

    public static void popup(String message) {
        popup(message, false);
    }

    public static void popup(String message, boolean isError) {
        popup(message, defaultWidth, isError);
    }

    public static void popup(String message, int pixelWidth) {
        popup(message, pixelWidth, false);
    }
    public static void popup(String message, int pixelWidth, boolean isError) {
    	messageContent.setText(message);
        String color = isError ? "red" : "black";
        messageContent.getElement().getStyle().setColor(color);
    	popupPanel.setWidget(messageContent);
        popupPanel.setWidth(pixelWidth + "px");
        popupPanel.setTitle(isError ? "Error" : "Info");
        popupPanel.center();
        popupPanel.show();
    }
    
    public static void popupError(String message, int pixelWidth, boolean isError) {
   	 	messageContent.setText(message);
        String color = isError ? "black" : "black";
        messageContent.getElement().getStyle().setColor(color);
	   	ScrollPanel popupMsgScrollPanel = new ScrollPanel();
	   	popupMsgScrollPanel.setSize("350px", "100px");
	   	popupMsgScrollPanel.setStyleName("errorMsgLabel");
	   	popupMsgScrollPanel.add(messageContent);
	   	popupPanel.add(popupMsgScrollPanel);
	    popupPanel.setWidth(pixelWidth + "px");
	    popupPanel.setTitle(isError ? "Unexpected Error" : "Info");
	    popupPanel.center();
	    popupPanel.show();
   }

    public static void popup(String message, UIObject anchor) {
        messageContent.setText(message);
        popupPanel.setWidget(messageContent);
        popupPanel.showRelativeTo(anchor);
        popupPanel.show();
    }

    /**
     * The Interface CloseHandler.
     */
    public static interface CloseHandler {
        void onClose();
    }

}
