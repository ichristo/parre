/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.util;

import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.ImageResource;

/**
 * .
 * User: Swarn S. Dhaliwal
 * Date: 1/14/12
 * Time: 2:05 PM
 */
public interface UtilImages extends ClientBundle {
    public static final UtilImages INSTANCE = GWT.create(UtilImages.class);
    @Source("images/bullet-arrow-down-icon.png")
    ImageResource downArrow();
    @Source("images/bullet-arrow-up-icon.png")
    ImageResource upArrow();
    @Source("images/bullet-toggle-plus-icon.png")
    ImageResource expand();
}
