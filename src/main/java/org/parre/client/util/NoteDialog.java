/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.util;

import com.google.gwt.event.dom.client.*;
import com.google.gwt.user.client.ui.*;


/**
 * .
 * User: Swarn S. Dhaliwal
 * Date: 10/9/11
 * Time: 9:56 AM
 */
public class NoteDialog {
    private ClosablePopup dialogBox;
    private TextArea noteTextArea;
    private Button saveNoteButton;
    private HorizontalPanel contentPanel;

    public NoteDialog() {
        dialogBox = new ClosablePopup("Add Note", true);
        dialogBox.setModal(true);
        contentPanel = new HorizontalPanel();
        contentPanel.setStyleName("inputPanel");
        contentPanel.setSpacing(5);
        contentPanel.setWidth("400px");
        noteTextArea = new TextArea();
        noteTextArea.setWidth("350px");
        noteTextArea.setVisibleLines(2);
        saveNoteButton = ViewUtil.createButton("Save");
        contentPanel.add(noteTextArea);
        contentPanel.add(saveNoteButton);
        dialogBox.setWidget(contentPanel);
        noteTextArea.addKeyUpHandler(new KeyUpHandler() {
            public void onKeyUp(KeyUpEvent keyUpEvent) {
                updateSaveButton();

            }
        });
    }

    private void updateSaveButton() {
        saveNoteButton.setEnabled(noteTextArea.getText().length() > 0);
    }

    public void setWidth(String width) {
        contentPanel.setWidth(width);
    }

    public void show() {
        updateSaveButton();
        dialogBox.center();
        dialogBox.show();
        noteTextArea.setFocus(true);
    }

    public void show(UIObject anchor) {
        updateSaveButton();
        dialogBox.showRelativeTo(anchor);
        dialogBox.show();
        noteTextArea.setFocus(true);
    }

    public void hide() {
        dialogBox.hide();
    }

    public ClosablePopup getDialogBox() {
        return dialogBox;
    }

    public Button getSaveNoteButton() {
        return saveNoteButton;
    }

    public TextArea getNoteTextArea() {
        return noteTextArea;
    }

}
