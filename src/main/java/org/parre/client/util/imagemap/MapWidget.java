/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.util.imagemap;

import com.google.gwt.core.client.Scheduler;
import com.google.gwt.user.client.Command;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.Element;
import com.google.gwt.user.client.Event;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Widget;

/**
 * .
 * User: Swarn S. Dhaliwal
 * Date: 9/3/11
 * Time: 10:21 AM
 */
public class MapWidget extends Widget {
    private AreaWidget[] items;

    public MapWidget() {
        super();
        setElement(DOM.createElement("map"));
        sinkEvents(Event.ONCLICK);
    }

    public MapWidget(AreaWidget[] areas) {
        this();
        items = areas;
        if (items != null && items.length > 0) {
            for (int i = 0; i < items.length; i++) {
                DOM.appendChild(getElement(), items[i].getElement());
            }
        }
    }

    public void bindImage(Image image) {
        usemap(image.getElement(), getName());
    }

    native void usemap(Element element, String name)/*-{
        element.useMap = "#" + name;
    }-*/;

    public String getID() {
        return DOM.getElementAttribute(getElement(), "id");
    }

    public String getName() {
        return DOM.getElementAttribute(getElement(), "name");
    }

    /* @Override */
    public void onBrowserEvent(Event event) {
        switch (DOM.eventGetType(event)) {
            case Event.ONCLICK:
                /*if (items != null && items.length > 0) {
                    Element target = DOM.eventGetTarget(event);
                    for (int i = 0; i < items.length; i++) {
                        if (target == items[i].getElement()) {
                            Command command = items[i].getCommand();
                            if (command != null) {
                                Scheduler.get().scheduleDeferred(command);
                            }
                        }
                    }
                    DOM.eventPreventDefault(event);
                    return;
                }*/
        }
        super.onBrowserEvent(event);
    }

    public void setID(String id) {
        DOM.setElementAttribute(getElement(), "id", (id == null) ? "" : id);
    }

    public void setName(String name) {
        DOM.setElementAttribute(getElement(), "name", (name == null) ? "" : name);
    }
}