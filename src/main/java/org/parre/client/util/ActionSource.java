/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.util;

/**
 * .
 * User: Swarn S. Dhaliwal
 * Date: 8/22/11
 * Time: 9:50 PM
 *
 * @param <T> the generic type
 */
public class ActionSource<T> {
    private ActionCommand<T> command;
    public ActionCommand<T> getCommand() {
        return command;
    }

    public void dispatch(T t) {
        if (command != null) {
            command.execute(t);
        }
    }
    public void fire() {
        dispatch(null);
    }

    public void setCommand(ActionCommand<T> command) {
        this.command = command;
    }

    /**
     * The Interface ActionCommand.
     *
     * @param <T> the generic type
     */
    public static interface ActionCommand<T> {
        void execute(T t);
    }
}
