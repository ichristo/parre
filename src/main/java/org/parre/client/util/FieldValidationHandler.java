/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.util;

import com.google.gwt.dom.client.Style;
import com.google.gwt.event.dom.client.*;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.Widget;

/**
 * .
 * User: Swarn S. Dhaliwal
 * Date: 9/4/11
 * Time: 1:42 PM
 */
public class FieldValidationHandler implements BlurHandler, ValueChangeHandler, ChangeHandler, FocusHandler {
    private Widget sourceWidget;
    private Validator hasValueChecker;
    private String originalBorderColor;
    private String errorBorderColor = "red";
    private Style style;
    private String message = "Required";
    private String overriddenMessage;
    private boolean isValid = true;
    private static final PopupPanel messagePopup = new PopupPanel();
    private static final Label messageText = new Label("Validation Message");

    static {
        messagePopup.setWidget(messageText);
        Style style1 = messagePopup.getElement().getStyle();
        style1.setBorderColor("#1538be");
        style1.setBorderWidth(1, Style.Unit.PX);
        style1.setColor("#45579f");
        style1.setBackgroundColor("#f7f9fb");
    }


    public FieldValidationHandler(Widget sourceWidget, Validator hasValueChecker) {
        this(sourceWidget, hasValueChecker, hasValueChecker.getMessage());
    }
    public FieldValidationHandler(Widget sourceWidget, Validator hasValueChecker, String message) {
        this.sourceWidget = sourceWidget;
        this.hasValueChecker = hasValueChecker;
        this.message = message;
        style = sourceWidget.getElement().getStyle();
        originalBorderColor = style.getBorderColor();
    }

    public Widget getSourceWidget() {
        return sourceWidget;
    }

    public void onBlur(BlurEvent event) {
        overriddenMessage = null;
        checkValue();
        messagePopup.hide();
    }

    public void onFocus(FocusEvent event) {
        if (!isValid) {
            showPopup();
        }
    }

    private void showPopup() {
        messageText.setText(overriddenMessage != null ? overriddenMessage : message);
        messagePopup.showRelativeTo(sourceWidget);
    }

    public boolean  checkValue() {
        isValid = hasValueChecker.validate(sourceWidget);
        updateDisplay();
        return isValid;
    }

    private void updateDisplay() {
        style.setBorderColor(isValid ? originalBorderColor : errorBorderColor);
        if (!isValid) {
            showPopup();
        }
        else {
            messagePopup.hide();
        }
    }

    public void onValueChange(ValueChangeEvent valueChangeEvent) {
        checkValue();
    }

    public void setValid(boolean valid, String message) {
        isValid = valid;
        overriddenMessage = message;
        updateDisplay();
    }

    public void onChange(ChangeEvent event) {
        checkValue();
    }
}
