/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.util;

import org.parre.client.ClientSingleton;
import org.parre.client.event.DisplayLoginEvent;

import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;


/**
 * The Class ErrorHandlingAsyncCallback.
 *
 * @param <T> the generic type
 * @author Swarn S. Dhaliwal
 * @version 1.0 7/16/11
 */
public abstract class ErrorHandlingAsyncCallback<T> implements AsyncCallback<T> {
    public void onFailure(Throwable caught) {
        GWTUtil.setNormalCursor();
        if (notLoggedIn(caught)) {
            ClientSingleton.getEventBus().fireEvent(DisplayLoginEvent.create());
        }
        else {
            MessageDialog.popup("Unexpected error : " + caught.getMessage());
        }
    }

    private boolean notLoggedIn(Throwable caught) {
        String message = caught.getMessage();
        if (message == null) {
            message = "";
        }
        return message.startsWith("403");
    }


}
