/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.util;

import com.google.gwt.cell.client.Cell;
import com.google.gwt.cell.client.TextCell;
import com.google.gwt.cell.client.ValueUpdater;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.NativeEvent;
import com.google.gwt.view.client.SingleSelectionModel;

import java.util.Collections;
import java.util.Set;

/**
 * .
 * User: Swarn S. Dhaliwal
 * Date: 9/17/11
 * Time: 8:33 AM
 *
 * @param <T> the generic type
 */
public class DoubleClickableTextCell<T> extends TextCell {
    private ActionSource<T> selectionSource;
    private SingleSelectionModel<T> selectionModel;

    public DoubleClickableTextCell(SingleSelectionModel<T> selectionModel) {
        this.selectionModel = selectionModel;
    }

    public DoubleClickableTextCell(SingleSelectionModel<T> selectionModel, ActionSource<T> selectionSource) {
        this(selectionModel);
        this.selectionSource = selectionSource;
    }

    public ActionSource getSelectionSource() {
        return selectionSource;
    }

    public void setSelectionSource(ActionSource<T> selectionSource) {
        this.selectionSource = selectionSource;
    }

    @Override
    public Set<String> getConsumedEvents() {
        return Collections.singleton("dblclick");
    }

    @Override
    public void onBrowserEvent(Cell.Context context, Element parent, String value, NativeEvent event, ValueUpdater<String> stringValueUpdater) {
        super.onBrowserEvent(context, parent, value, event, stringValueUpdater);
        ActionSource.ActionCommand<T> command = selectionSource.getCommand();
        if (command != null) {
            command.execute(selectionModel.getSelectedObject());
        }
    }
}
