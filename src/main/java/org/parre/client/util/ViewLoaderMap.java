/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.util;




import java.util.HashMap;
import java.util.Map;

import org.parre.client.util.nav.NavViewMapper;

/**
 * This class is a utility class for tracking view loaders.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 4/10/12
 */
public class ViewLoaderMap implements NavViewMapper {
    private Map<String, AsyncViewLoader> viewLoaderMap = new HashMap<String, AsyncViewLoader>();
    public ViewLoaderMap() {
    }

    public void mapView(Class viewClass, final AsyncViewLoader viewLoader) {
        mapView(viewClass.getName(), viewLoader);
    }

    public void mapView(String viewId, final AsyncViewLoader viewLoader) {
        viewLoaderMap.put(viewId, viewLoader);
    }

    private AsyncViewLoader getViewLoader(Class viewClass) {
        return getViewLoader(viewClass.getName());
    }

    private AsyncViewLoader getViewLoader(String viewId) {
        return viewLoaderMap.get(viewId);
    }

    public void showView(String viewId) {
        getViewLoader(viewId).showView();
    }

    public void showView(Class viewClass) {
        getViewLoader(viewClass).showView();
    }

    public void showView(Class<? extends View> viewClass, String viewTitle) {
        getViewLoader(viewClass).showView(viewTitle);
    }

}
