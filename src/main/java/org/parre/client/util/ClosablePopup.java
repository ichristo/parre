/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.util;

import com.google.gwt.dom.client.EventTarget;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.i18n.client.HasDirection;
import com.google.gwt.user.client.Element;
import com.google.gwt.user.client.ui.*;

/**
 * The Class ClosablePopup.
 *
 * @author Andrey Talnikov
 */
public class ClosablePopup extends DialogBox {

    private Anchor closeAnchor;
    private FlexTable captionLayoutTable;

    /**
     * Instantiates new closable popup.
     *
     * @param title        the title
     * @param defaultClose it {@code true}, hide popup on 'x' click
     */
    public ClosablePopup(String title, boolean defaultClose) {
        super(false);

        closeAnchor = new Anchor("x");

        captionLayoutTable = new FlexTable();
        captionLayoutTable.setWidth("100%");
        captionLayoutTable.setText(0, 0, title);
        captionLayoutTable.setWidget(0, 1, closeAnchor);
        captionLayoutTable.getCellFormatter().setHorizontalAlignment(0, 1, HasHorizontalAlignment.HorizontalAlignmentConstant.endOf(HasDirection.Direction.LTR));
        
        HTML caption = (HTML) getCaption();
        caption.getElement().appendChild(captionLayoutTable.getElement());
        caption.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                EventTarget target = event.getNativeEvent().getEventTarget();
                Element targetElement = (Element) target.cast();

                if (targetElement == closeAnchor.getElement()) {
                    closeAnchor.fireEvent(event);
                }
            }
        });

        if (defaultClose) {
            addCloseHandler(new ClickHandler() {
                public void onClick(ClickEvent event) {
                    hide(true);
                }
            });
        }
    }

    @Override
    public void setTitle(String title) {
        captionLayoutTable.setText(0, 0, title);
    }

    public void setCloseAnchorColor(String color) {
        closeAnchor.getElement().getStyle().setColor(color);
    }
    private void addCloseHandler(ClickHandler handler) {
        closeAnchor.addClickHandler(handler);
    }
}
