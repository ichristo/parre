/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.util;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;

/**
 * The Class ValidatingClickHandler.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 1/20/12
 */
public abstract class ValidatingClickHandler implements ClickHandler{
    private ModelView modelView;

    public ValidatingClickHandler(ModelView modelView) {
        this.modelView = modelView;
    }

    public void onClick(ClickEvent event) {
        if (modelView.validate()) {
            handleClick(event);
        }
    }

    protected abstract void handleClick(ClickEvent event);
}
