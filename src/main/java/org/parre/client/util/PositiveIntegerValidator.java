/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.util;

import com.google.gwt.user.client.ui.HasValue;

/**
 * .
 * User: Swarn S. Dhaliwal
 * Date: 9/4/11
 * Time: 1:51 PM
 */
public class PositiveIntegerValidator implements Validator {
    public static final PositiveIntegerValidator INSTANCE = new PositiveIntegerValidator();
    public boolean validate(Object source) {
        String value = ((HasValue<String>) source).getValue().replace(",", "");
        boolean valid = value.trim().length() > 0;
        if (valid) {
            try {
                valid = Integer.parseInt(value) >= 0;
            } catch (NumberFormatException e) {
                valid = false;
            }
        }
        return valid;
    }

    public String getMessage() {
        return "Must be positive Integer";
    }
}
