/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.util;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.parre.client.ClientSingleton;
import org.parre.shared.BaseDTO;
import org.parre.shared.LabelValue;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.resources.client.ImageResource;
import com.google.gwt.user.client.ui.AbstractImagePrototype;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DeckLayoutPanel;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HasText;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.UIObject;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.user.datepicker.client.DateBox;

/**
 * .
 * User: Swarn S. Dhaliwal
 * Date: 9/4/11
 * Time: 11:53 AM
 */
@SuppressWarnings("unchecked")
public class ViewUtil {
    private static final DateTimeFormat DATE_TIME_FORMAT = DateTimeFormat.getFormat(DateTimeFormat.PredefinedFormat.DATE_MEDIUM);
    private static final NumberFormat PROBABILITY_FORMAT = NumberFormat.getFormat("'0'.000E0");

    public static HTML addLabelFormEntry(FlexTable flexTable, int rowNum, String entryLabel) {
        HTML labelField = new HTML();
        addFormEntry(flexTable, rowNum, entryLabel, labelField);
        return labelField;
    }
    
    public static TextBox addInputLabelFormEntry(FlexTable flexTable, int rowNum, String entryLabel) {
        TextBox textBox = createTextBox();
        textBox.setEnabled(false);
        addFormEntry(flexTable, rowNum, entryLabel, textBox);
        return textBox;
    }

    public static TextBox addInputLabelFormEntry(FlexTable flexTable, int rowNum, String entryLabel, int labelCol) {
        TextBox textBox = createTextBox();
        textBox.setEnabled(false);
        addFormEntry(flexTable, rowNum, entryLabel, textBox, labelCol);
        return textBox;
    }

    public static TextBox addTextBoxFormEntry(FlexTable flexTable, int rowNum, String entryLabel) {
        TextBox textBox = createTextBox();
        addFormEntry(flexTable, rowNum, entryLabel, textBox);
        return textBox;
    }

    public static TextBox addReadOnlyFormEntry(FlexTable flexTable, int rowNum, String entryLabel) {
        TextBox textBox = createTextBox();
        textBox.setEnabled(false);
        addFormEntry(flexTable, rowNum, entryLabel, textBox);
        return textBox;
    }

    public static TextBox addTextBoxFormEntry(FlexTable flexTable, int rowNum, String entryLabel, List<FieldValidationHandler> handlers, Validator validator) {
        TextBox textBox = createTextBox();
        addFormEntry(flexTable, rowNum, entryLabel, textBox);
        addRequiredField(textBox, handlers, validator);
        return textBox;
    }

    public static TextBox addTextBoxFormEntry(FlexTable flexTable, int rowNum, String entryLabel, List<FieldValidationHandler> handlers) {
        return addTextBoxFormEntry(flexTable, rowNum, entryLabel, handlers, TextHasValueValidator.INSTANCE);
    }

    public static TextBox addTextBox(FlexTable flexTable, int rowNum, int colNum) {
        TextBox textBox = createTextBox();
        addInputEntry(flexTable, rowNum, colNum, textBox);
        return textBox;
    }

    public static TextBox addTextBox(FlexTable flexTable, int rowNum, int colNum, List<FieldValidationHandler> handlers) {
        TextBox textBox = createTextBox();
        addInputEntry(flexTable, rowNum, colNum, textBox);
        addRequiredField(textBox, handlers);
        return textBox;
    }

    public static TextBox addTextBox(FlexTable flexTable, int rowNum, int colNum, List<FieldValidationHandler> handlers, Validator validator) {
        TextBox textBox = createTextBox();
        addInputEntry(flexTable, rowNum, colNum, textBox);
        addRequiredField(textBox, handlers, validator);
        return textBox;
    }

    private static TextBox createTextBox() {
        TextBox textBox = new TextBox();
        textBox.setWidth("250px");
        return textBox;
    }

    public static ListBox addListBox(FlexTable flexTable, int rowNum, int colNum, List<FieldValidationHandler> handlers) {
        ListBox listBox = new ListBox();
        addInputEntry(flexTable, rowNum, colNum, listBox);
        addRequiredField(listBox, handlers);
        return listBox;
    }

    public static ListBox addListBoxFormEntry(FlexTable flexTable, int rowNum, String entryLabel) {
        ListBox listBox = new ListBox();
        addFormEntry(flexTable, rowNum, entryLabel, listBox);
        return listBox;
    }

    public static ListBox addListBoxFormEntry(FlexTable flexTable, int rowNum, String entryLabel, int labelCol, int fieldCol) {
        ListBox listBox = new ListBox();
        addFormEntry(flexTable, rowNum, entryLabel, listBox, labelCol, fieldCol);
        return listBox;
    }

    public static ListBox addListBoxFormEntry(FlexTable flexTable, int rowNum, String entryLabel, List<FieldValidationHandler> handlers) {
        ListBox listBox = new ListBox();
        addFormEntry(flexTable, rowNum, entryLabel, listBox);
        addRequiredField(listBox, handlers);
        return listBox;
    }

    public static DateBox addDateBoxFormEntry(FlexTable flexTable, int rowNum, String entryLabel) {
        DateBox dateBox = new DateBox();
        addFormEntry(flexTable, rowNum, entryLabel, dateBox);
        return dateBox;
    }

    public static DateBox addDateBoxFormEntry(FlexTable flexTable, int rowNum, String entryLabel, List<FieldValidationHandler> handlers) {
        DateBox dateBox = new DateBox();
        addFormEntry(flexTable, rowNum, entryLabel, dateBox);
        addRequiredField(dateBox, handlers);
        return dateBox;
    }


    public static DateBox addDateBoxFormEntry(FlexTable flexTable, int rowNum, String entryLabel, HTML errorWidget) {
        DateBox dateBox = new DateBox();
        addFormEntry(flexTable, rowNum, entryLabel, ViewUtil.horizontalPanel(dateBox, errorWidget));
        return dateBox;
    }

    public static void addFormEntry(FlexTable flexTable, int rowNum, String entryLabel, Widget entryField) {
        int labelCol = 0;
        int fieldCol = 1;
        addFormEntry(flexTable, rowNum, entryLabel, entryField, labelCol, fieldCol);
    }

    public static void addInputEntry(FlexTable flexTable, int rowNum, int col, Widget entryField) {
        flexTable.setWidget(rowNum, col, entryField);
    }

    public static HTML addLabelFormEntry(FlexTable flexTable, int rowNum, String entryLabel, int labelCol, int fieldCol) {
        HTML labelField = new HTML();
        addFormEntry(flexTable, rowNum, entryLabel, labelField, labelCol, fieldCol);
        return labelField;
    }

    public static TextBox addTextBoxFormEntry(FlexTable flexTable, int rowNum, String entryLabel, int labelCol, int fieldCol) {
        TextBox textBox = createTextBox();
        addFormEntry(flexTable, rowNum, entryLabel, textBox, labelCol, fieldCol);
        return textBox;
    }

    public static DateBox addDateBoxFormEntry(FlexTable flexTable, int rowNum, String entryLabel, int labelCol, int fieldCol) {
        DateBox dateBox = new DateBox();
        addFormEntry(flexTable, rowNum, entryLabel, dateBox, labelCol, fieldCol);
        return dateBox;
    }

    public static HTML addLabelFormEntry(FlexTable flexTable, int rowNum, String entryLabel, int labelCol) {
        HTML labelField = new HTML();
        addFormEntry(flexTable, rowNum, entryLabel, labelField, labelCol);
        return labelField;
    }

    public static TextBox addTextBoxFormEntry(FlexTable flexTable, int rowNum, String entryLabel, int labelCol) {
        TextBox textBox = createTextBox();
        addFormEntry(flexTable, rowNum, entryLabel, textBox, labelCol);
        return textBox;
    }

    public static DateBox addDateBoxFormEntry(FlexTable flexTable, int rowNum, String entryLabel, int labelCol) {
        DateBox dateBox = new DateBox();
        addFormEntry(flexTable, rowNum, entryLabel, dateBox, labelCol);
        return dateBox;
    }

    public static TextArea addTextAreaFormEntry(FlexTable flexTable, int rowNum, String entryLabel) {
        TextArea textArea = new TextArea();
        textArea.setCharacterWidth(30);
        textArea.setVisibleLines(2);
        addFormEntry(flexTable, rowNum, entryLabel, textArea, 0);
        return textArea;
    }

    public static TextArea addTextAreaFormEntry(FlexTable flexTable, int rowNum, int colNum, String entryLabel) {
        TextArea textArea = new TextArea();
        textArea.setCharacterWidth(30);
        textArea.setVisibleLines(2);
        addFormEntry(flexTable, rowNum, entryLabel, textArea, colNum);
        return textArea;
    }

    public static void addFormEntry(FlexTable flexTable, int rowNum, String entryLabel, Widget entryField, int labelCol) {
        addFormEntry(flexTable, rowNum, entryLabel, entryField, labelCol, labelCol + 1);
    }

    public static void addFormEntry(FlexTable flexTable, int rowNum, String entryLabel, Widget entryField, int labelCol, int fieldCol) {
        flexTable.setHTML(rowNum, labelCol, entryLabel);
        flexTable.setWidget(rowNum, fieldCol, entryField);
        Style leftColStyle = flexTable.getFlexCellFormatter().getElement(rowNum, labelCol).getStyle();
        leftColStyle.setWidth(30, Style.Unit.PCT);
        leftColStyle.setProperty("textAlign", "right");
        leftColStyle.setProperty("whiteSpace", "nowrap");
        leftColStyle.setProperty("fontWeight", "600");
        Style rightColStyle = flexTable.getFlexCellFormatter().getElement(rowNum, fieldCol).getStyle();
        rightColStyle.setWidth(70, Style.Unit.PCT);
        rightColStyle.setProperty("textAlign", "left");
        rightColStyle.setProperty("whiteSpace", "nowrap");
    }

    public static TextBox addTextBoxWithHeader(FlexTable flexTable, int rowNum, String label, int colNum, List<FieldValidationHandler> handlers) {
        TextBox textBox = createTextBox();
        addRequiredField(textBox, handlers);
        addWidgetWithHeader(flexTable, rowNum, label, textBox, colNum);
        return textBox;
    }

    public static TextBox addTextBoxWithHeader(FlexTable flexTable, int rowNum, String label, int colNum, List<FieldValidationHandler> handlers, Validator validator) {
        TextBox textBox = createTextBox();
        addRequiredField(textBox, handlers, validator);
        addWidgetWithHeader(flexTable, rowNum, label, textBox, colNum);
        return textBox;
    }

    public static TextBox addTextBoxWithHeader(FlexTable flexTable, int rowNum, String label, int colNum) {
        TextBox textBox = createTextBox();
        addWidgetWithHeader(flexTable, rowNum, label, textBox, colNum);
        return textBox;
    }

    public static ListBox addListBoxWithHeader(FlexTable flexTable, int rowNum, String label, int colNum, List<FieldValidationHandler> handlers) {
        ListBox listBox = new ListBox();
        addRequiredField(listBox, handlers);
        addWidgetWithHeader(flexTable, rowNum, label, listBox, colNum);
        return listBox;
    }

    public static void addTextUnderWidget(FlexTable flexTable, int rowNum, String label, Widget widget, int colNum) {
        flexTable.setWidget(rowNum, colNum, widget);
        flexTable.setHTML(rowNum + 1, colNum, label);
        Style headerStyle = flexTable.getFlexCellFormatter().getElement(rowNum+1, colNum).getStyle();
        headerStyle.setWidth(100, Style.Unit.PX);
        headerStyle.setProperty("textAlign", "center");
        headerStyle.setPaddingBottom(25, Style.Unit.PX);
        headerStyle.setProperty("fontWeight", "600");
        Style widgetStyle = flexTable.getFlexCellFormatter().getElement(rowNum, colNum).getStyle();
        widgetStyle.setWidth(100, Style.Unit.PX);
        widgetStyle.setProperty("textAlign", "left");
        widgetStyle.setProperty("whiteSpace", "nowrap");
        widgetStyle.setPaddingLeft(15, Style.Unit.PX);
        widgetStyle.setPaddingRight(15, Style.Unit.PX);
    }

    public static void addWidgetWithHeader(FlexTable flexTable, int rowNum, String label, Widget widget, int colNum) {
    	addHeader(flexTable, rowNum, label, colNum);
    	addWidget(flexTable, rowNum, widget, colNum);
    }
    
    public static void addWidget(FlexTable flexTable, int rowNum, Widget widget, int colNum) {
    	flexTable.setWidget(rowNum + 1, colNum, widget);
        Style widgetStyle = flexTable.getFlexCellFormatter().getElement(rowNum+1, colNum).getStyle();
        widgetStyle.setWidth(100, Style.Unit.PX);
        widgetStyle.setProperty("textAlign", "left");
        widgetStyle.setProperty("whiteSpace", "nowrap");
    }
    
    public static void addHeader(FlexTable flexTable, int rowNum, String label, int colNum) {
    	flexTable.setHTML(rowNum, colNum, label);
        Style headerStyle = flexTable.getFlexCellFormatter().getElement(rowNum, colNum).getStyle();
        headerStyle.setWidth(100, Style.Unit.PX);
        headerStyle.setProperty("textAlign", "left");
        headerStyle.setProperty("whiteSpace", "nowrap");
        headerStyle.setProperty("fontWeight", "600");
    }

    public static Button createPictureButton(String picLink) {
        Image image = new Image(GWT.getHostPageBaseURL() + picLink);
        Button button = new Button();
        button.getElement().appendChild(image.getElement());
        return button;
    }

    public static HorizontalPanel horizontalPanel(Widget... widgets) {
        return horizontalPanel(0, widgets);
    }

    public static HorizontalPanel horizontalPanel(int spacing, Widget... widgets) {
        HorizontalPanel panel = new HorizontalPanel();
        for (Widget widget : widgets) {
            panel.add(widget);
        }
        panel.setSpacing(spacing);
        return panel;
    }

    public static HorizontalPanel layoutLeft(Widget... widgets) {
        return layoutLeft("100%", widgets);
    }

    public static HorizontalPanel layoutLeft(String width, Widget... widgets) {
        HorizontalPanel container = new HorizontalPanel();
        container.setWidth(width);
        container.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_LEFT);
        container.add(horizontalPanel(5, widgets));
        return container;
    }

    public static HorizontalPanel layoutLeft(int spacing, Widget... widgets) {
        HorizontalPanel container = new HorizontalPanel();
        container.setWidth("100%");
        container.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_LEFT);
        container.add(horizontalPanel(spacing, widgets));
        return container;
    }

    public static HorizontalPanel layoutCenter(Widget... widgets) {
        return layoutCenter("100%", widgets);
    }

    public static HorizontalPanel layoutCenter(String width, Widget... widgets) {
        HorizontalPanel container = new HorizontalPanel();
        container.setWidth(width);
        container.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_CENTER);
        container.add(horizontalPanel(5, widgets));
        return container;
    }

    public static HorizontalPanel layoutCenter(int spacing, Widget... widgets) {
        HorizontalPanel container = new HorizontalPanel();
        container.setWidth("100%");
        container.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_CENTER);
        container.add(horizontalPanel(spacing, widgets));
        return container;
    }

    public static HorizontalPanel layoutRight(Widget... widgets) {
        return layoutRight("100%", widgets);
    }

    public static HorizontalPanel layoutRight(String width, Widget... widgets) {
        HorizontalPanel container = new HorizontalPanel();
        container.setWidth(width);
        container.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_RIGHT);
        container.add(horizontalPanel(5, widgets));
        return container;
    }

	public static void addRequiredField(DateBox sourceWidget, List<FieldValidationHandler> requiredFieldHandlers) {
        FieldValidationHandler handler = new FieldValidationHandler(sourceWidget, DateHasValueValidator.INSTANCE);
        sourceWidget.addValueChangeHandler(handler);
        requiredFieldHandlers.add(handler);
    }

    public static void addRequiredField(TextBox sourceWidget, List<FieldValidationHandler> requiredFieldHandlers) {
        FieldValidationHandler handler = new FieldValidationHandler(sourceWidget, TextHasValueValidator.INSTANCE);
        sourceWidget.addBlurHandler(handler);
        sourceWidget.addValueChangeHandler(handler);
        sourceWidget.addFocusHandler(handler);
        requiredFieldHandlers.add(handler);
    }

    public static void addRequiredField(TextBox sourceWidget, List<FieldValidationHandler> requiredFieldHandlers, Validator validator) {
        FieldValidationHandler handler = new FieldValidationHandler(sourceWidget, validator);
        sourceWidget.addBlurHandler(handler);
        sourceWidget.addValueChangeHandler(handler);
        sourceWidget.addFocusHandler(handler);
        requiredFieldHandlers.add(handler);
    }

    public static void addRequiredField(ListBox sourceWidget, List<FieldValidationHandler> requiredFieldHandlers) {
        FieldValidationHandler handler = new FieldValidationHandler(sourceWidget, ListBoxHasValueValidator.INSTANCE);
        sourceWidget.addBlurHandler(handler);
        sourceWidget.addChangeHandler(handler);
        sourceWidget.addFocusHandler(handler);
        requiredFieldHandlers.add(handler);
    }

    public static boolean hasRequiredValues(List<FieldValidationHandler> handlers) {
        boolean valid = true;
        for (FieldValidationHandler requiredFieldHandler : handlers) {
            if (!requiredFieldHandler.checkValue()) {
                valid = false;
            }
        }
        return valid;
    }

    public static void setDate(DateBox dateBox, Date date) {
        dateBox.setValue(date);
    }

    public static void setDate(HasText hasText, Date date) {
        hasText.setText(DATE_TIME_FORMAT.format(date));
    }

    public static Date getDate(DateBox dateBox) {
        return dateBox.getValue();
    }

    public static void setBigDecimal(HasText hasText, BigDecimal moneyValue) {
        hasText.setText(moneyValue != null ? toDisplayValue(moneyValue) : "");
    }

    public static void setText(HasText hasText, String textValue) {
        hasText.setText(textValue != null ? textValue : "");
    }

    public static void setText(HasText hasText, Date dateValue) {
        hasText.setText(DATE_TIME_FORMAT.format(dateValue));
    }

    public static void setQuantity(HasText hasText) {
        setQuantity(hasText, getQuantity(hasText));
    }

    public static void setQuantity(HasText hasText, Integer quantity) {
        hasText.setText(quantity != null ? toDisplayValue(new BigDecimal(quantity)) : "");
    }

    public static String getText(HasText hasText) {
        return hasText.getText().trim() != null ? hasText.getText().trim() : "";
    }

    public static BigDecimal getBigDecimal(HasText hasText) {
    	String cleanText = cleanEmptyToNull(removeCommas(hasText.getText().trim()));
        return cleanText == null ? null : new BigDecimal(cleanText);
    }
    
    public static String cleanEmptyToNull(String value) {
    	if (value == null) { return null; }
    	
    	String cleanValue = value.trim();
    	
    	return cleanValue == null || cleanValue.length() == 0 || "null".equalsIgnoreCase(cleanValue)
    				? null
    				: cleanValue;
    }
    
    public static BigDecimal getBigDecimal(HasText hasText, BigDecimal defaultValue) {
    	if (removeCommas(hasText.getText()) != null) {
    		return getBigDecimal(hasText);
    	}
    	return defaultValue;
    }

    public static Integer getQuantity(HasText value) {
        if(value.getText().equals("")) {
            return null;
        }
        try {
            return Integer.valueOf(removeCommas(value.getText().trim()));
        }
        catch (NumberFormatException nfe) {
            return null;
        }
    }
    
    public static Image createImage(String imageFileName) {
        return new Image(getImageUrl(imageFileName));
    }

    public static boolean validate(List<FieldValidationHandler> handlers) {
        boolean isValid = true;
        for (FieldValidationHandler requiredFieldHandler : handlers) {
            if (!requiredFieldHandler.checkValue()) {
                isValid = false;
            }
        }
        return isValid;
    }

    public static void setSelectedValue(ListBox listBox, Boolean value) {
        setSelectedValue(listBox,  value != null ? String.valueOf(value) : "");
    }

    public static void setSelectedValue(ListBox listBox, Number value) {
        setSelectedValue(listBox, value != null ? String.valueOf(value) : "");
    }

    public static void setSelectedValue(ListBox listBox, String value) {
        int itemCount = listBox.getItemCount();
        for (int i = 0; i < itemCount; i++) {
            String pickValue = listBox.getValue(i);
            if (pickValue.equals(value)) {
                listBox.setSelectedIndex(i);
                return;
            }
        }
        listBox.setSelectedIndex(0);
    }

    public static String getImageUrl(String imageFilename) {
        return GWT.getHostPageBaseURL() + "images/" + imageFilename;
    }

    public static void addImageCursorHandler(Image... images) {
        for (Image image : images) {
            ImageCursorHandler.add(image);
        }
    }
    public static Image createImageButton(String imageFilename, String toolTip) {
        return createImageButton(imageFilename, toolTip, 50);
    }

    public static Image createImageButton(String imageFilename, String toolTip, boolean scale) {
        return createImageButton(imageFilename, toolTip, 50, scale);
    }

    public static Image createImageButton(String imageFilename, String toolTip, int maxWidth) {
        return createImageButton(imageFilename, toolTip, maxWidth, 50, true);
    }

    public static Image createImageButton(String imageFilename, String toolTip, int maxWidth, boolean scale) {
        return createImageButton(imageFilename, toolTip, maxWidth, 50, scale);
    }

    public static Image createImageButton(String imageFilename, String toolTip, int maxWidth, int height, boolean scale) {
        Image image = new Image(getImageUrl(imageFilename));
        if (scale) {
            image.setHeight(height + "px");
        }
        ImageCursorHandler.add(image);
        Style style = image.getElement().getStyle();
        style.setProperty("borderRadius", "10px");
        style.setProperty("maxWidth", maxWidth + "px");
        ToolTipManager.addToolTip(image, toolTip);
        return image;
    }

    public static Long getSelectedId(ListBox listBox) {
        return Long.valueOf(listBox.getValue(listBox.getSelectedIndex()));
    }

    public static String getSelectedValue(ListBox listBox) {
        return listBox.getValue(listBox.getSelectedIndex());
    }

    public static Boolean getSelectedBoolean(ListBox listBox) {
        return Boolean.valueOf(listBox.getValue(listBox.getSelectedIndex()));
    }

    public static Integer getSelectedQuantity(ListBox listBox) {
        return Integer.valueOf(listBox.getValue(listBox.getSelectedIndex()));
    }

    public static int addScrollableWidget(Widget viewWidget, DeckLayoutPanel deckLayoutPanel) {
        ScrollPanel panel = new ScrollPanel(viewWidget);
        deckLayoutPanel.add(panel);
        return deckLayoutPanel.getWidgetIndex(panel);
    }

    public static DialogBox createDialog(String title, Widget widget) {
        DialogBox dialogBox = new ClosablePopup(title, true);
        dialogBox.setModal(true);
        dialogBox.setWidget(widget);
        return dialogBox;
    }

    public static String toDisplayValue(BigDecimal quantity) {
        if(quantity == null || quantity.doubleValue() == 0) {
            return "0";
        }
        if(quantity.doubleValue() < 1 && quantity.doubleValue() > -1) {
            return formatProbability(quantity);
        }
        if(quantity.doubleValue() >= 100  || quantity.doubleValue() <= -1000) {
            return toDisplayValueWithCommas(quantity);
        }
        if(quantity.longValue()%10 == 0) {
            return quantity.stripTrailingZeros().toPlainString();
        }
        return quantity.setScale(5, RoundingMode.HALF_EVEN).stripTrailingZeros().toString();
    }
    
    public static String displayFormatForRatio(BigDecimal quantity) {
        if(quantity == null || quantity.doubleValue() == 0) {
            return "0";
        }
        return quantity.setScale(3, RoundingMode.CEILING).toString();
    }
    
    public static String displayFormatForRisk(BigDecimal quantity) {
        if(quantity == null || quantity.doubleValue() == 0) {
            return "0";
        }
        
        if((quantity.compareTo(BigDecimal.ZERO) > 0) &&   (quantity.compareTo(BigDecimal.ONE) < 1) ){
        	return "0";
        }

        if(quantity.doubleValue() < 1 && quantity.doubleValue() > -1) {
            return formatProbability(quantity);
        }
        if(quantity.doubleValue() >= 1  || quantity.doubleValue() <= -1000) {
            return toDisplayValueWithCommas(quantity);
        }
        if(quantity.longValue()%10 == 0) {
            return quantity.stripTrailingZeros().toPlainString();
        }
        return quantity.setScale(5, RoundingMode.HALF_EVEN).stripTrailingZeros().toString();
    }
    

    public static String toDisplayUriValue(BigDecimal quantity) {
        if(quantity.doubleValue() == 0) {
            return "0";
        }
        return quantity.stripTrailingZeros().toPlainString();
    }

    private static String toDisplayValueWithCommas(BigDecimal quantity) {
        boolean isNegative = false;
        if(quantity.longValue() < 0) {
            isNegative = true;
        }
       
        return addCommas(Long.toString(quantity.abs().longValue()), Long.toString(quantity.abs().longValue()).length(), isNegative);
    }
    
    
    
    public static String addCommasForString(String str){
    	return addCommas(str, str.length(), false);
    }
    
    private static String addCommas(String str, int length, boolean isNegative) {
        if(length < 4) {
            if(isNegative) {
                str = "-" + str;
            }
            return str;
        }
        return addCommas(str.subSequence(0, (length - 3)) + "," + str.subSequence(length - 3, str.length()), length-3, isNegative);
    }

    public static String removeCommas(String str) {
        if(str != null && str.length() > 0 && str.charAt(0) == '-') {
            return "-" + str.replaceAll("[^0-9\\.]+", "");
        }
    	return str == null ? null : str.replaceAll("[^0-9\\.]+", "");
    }
    
    public static Button createButton(String text) {
        return createButton(text, text);
    }

    public static Button createButton(String text, String tooltip) {
        Button button = new Button(text);
        button.setStyleName("parre-Button");
        ToolTipManager.addToolTip(button, tooltip);
        return button;
    }
    
    public static ValueButton createReportButton(String text) {
        return createReportButton(text, text);
    }

    public static ValueButton createReportButton(String text, String tooltip) {
        ValueButton button = new ValueButton(text);
        button.setStyleName("parre-Report-Button");
        return button;
    }

    public static void setBorder(Style style) {
        style.setBorderWidth(1, Style.Unit.PX);
        style.setBorderStyle(Style.BorderStyle.SOLID);
        style.setBorderColor("#489dfa");
    }

    public static String formatProbability(BigDecimal bd) {
        if(ClientSingleton.getAppInitData().getUserPreferencesDTO().getUseScientificNotation()) {
            return PROBABILITY_FORMAT.format(bd);
        }
        return bd.doubleValue() == 0 ? "0" : bd.stripTrailingZeros().toPlainString();

    }
    
   
    public static BigDecimal parseProbability(String text) {
        try {
            return BigDecimal.valueOf(PROBABILITY_FORMAT.parse(text));
        } catch (NumberFormatException e) {
            return new BigDecimal(text);
        }

    }

    public static void setDownArrowButtonTitle(Button button, String title) {
        setButtonHtml(button, title, UtilImages.INSTANCE.downArrow());
    }

    public static void setUpArrowButtonTitle(Button button, String title) {
        setButtonHtml(button, title, UtilImages.INSTANCE.upArrow());
    }

    public static Button createDownArrowButton(String title) {
        String buttonHtml = createButtonHtml(title, UtilImages.INSTANCE.downArrow());
        return createButton(buttonHtml, "Select");
    }

    public static Button createUpArrowButton(String title) {
        String buttonHtml = createButtonHtml(title, UtilImages.INSTANCE.upArrow());
        return createButton(buttonHtml, "Select");
    }

    public static String createButtonHtml(String selectTitle, ImageResource resource) {
        String imageHtml = AbstractImagePrototype.create(resource).getHTML();
        return "<div style='float:left;'><div style='float:left;margin-right:5px;'>" + selectTitle + "</div><div style='float:left;vertical-align:text-top;'>" + imageHtml + "</div></div>";
    }

    public static void setButtonHtml(Button button, String title, ImageResource resource) {
        button.setHTML(createButtonHtml(title, resource));
    }

    public static Image createImageButton(ImageResource imageResource) {
        Image image = new Image(imageResource);
        ImageCursorHandler.add(image);
        return image;
    }

    public static Image createImageButton(String imageUrl) {
        Image image = new Image(imageUrl);
        ImageCursorHandler.add(image);
        return image;
    }

    public static void makeWhiteSpaceNormal(UIObject uiObject) {
        uiObject.getElement().getStyle().setProperty("whiteSpace", "normal");
    }

    public static HTML createInputLabel(String labelHtml) {
        HTML html = new HTML(labelHtml);
        html.setStyleName("normalLabel");
        return html;
    }

    public static VerticalPanel createInputPanel() {
        VerticalPanel panel = new VerticalPanel();
        panel.setStyleName("inputPanel");
        return panel;
    }

    public static Widget createHeader(String text) {
        Label header = new Label(text);
        header.setStyleName("inputPanelHeader");
        return header;
    }

    public static Widget createSubHeader(String text) {
        Label header = new Label(text);
        header.setStyleName("inputPanelSubHeader");
        return header;
    }

    public static <T> void updateEntry(T entry, List<T> list) {
        int index = list.indexOf(entry);
        if (index < 0) {
            list.add(entry);
        }
        else {
            list.set(index, entry);
        }
    }

    public static <T> T findDTO(Long pmId, Collection<T> dtos) {
        Collection<BaseDTO> baseDTOs = (Collection<BaseDTO>) dtos;
        for (BaseDTO dto : baseDTOs) {
            if (dto.hasId(pmId)) {
                return (T) dto;
            }
        }
        return null;
    }

    public static void initListBox(ListBox fieldType, LabelValue[] values) {
        fieldType.addItem("Select", "");
        for (LabelValue value : values) {
            fieldType.addItem(value.getLabel(), value.getValue());
        }
    }

    public static void initListBox(ListBox fieldType, Collection<? extends LabelValue> values) {
        fieldType.addItem("Select", "");
        for (LabelValue value : values) {
            fieldType.addItem(value.getLabel(), value.getValue());
        }
    }


}
