/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.lot.view;

import org.parre.client.lot.presenter.LotPresenter;
import org.parre.client.util.DeckResizeComposite;
import org.parre.client.util.Presenter;


/**
 * User: Swarn S. Dhaliwal
 * Date: 7/10/11
 * Time: 4:39 PM
*/
public class LotView extends DeckResizeComposite implements LotPresenter.Display {
    private Presenter presenter;
    public LotView() {
        super();
    }

    public void setPresenter(Presenter presenter) {
        this.presenter = presenter;
    }

    public Presenter getPresenter() {
        return presenter;
    }
}