/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.lot.view;

import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.user.client.ui.*;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.parre.client.lot.presenter.ProxyIndicationPresenter;
import org.parre.client.util.*;
import org.parre.shared.LotDTO;
import org.parre.shared.ProxyCityDTO;
import org.parre.shared.ProxyIndicationDTO;
import org.parre.shared.ProxyTargetTypeDTO;

/**
 * The Class ProxyIndicationView.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 1/12/12
 */
public class ProxyIndicationView extends BaseModelView<LotDTO> implements ProxyIndicationPresenter.Display {

    private VerticalPanel viewWidget;
    private Label assetThreatLabel;
    private HTML probability;
    private Button saveButton;
    private Button cancelButton;
    private DialogBox selectProxyPopup;
    private ListBox proxyPaths;
    private Button changeProxyButton;
    private Button selectProxyButton;
    private Button createProxyButton;
    private Button copyProxyButton;
    private List<FieldValidationHandler> validationHandlers = new ArrayList<FieldValidationHandler>();
    private TextBox name;
    private TextBox description;
    private TextBox attacksPerYear;
    private ListBox proxyCities;
    private TextBox population;
    private ListBox targetTypes;
    private TextBox facilitiesNum;
    private TextBox targetTypeFacilitiesNum;
    private TextBox facilityCapacity;
    private TextBox regionalCapacity;
    private Button saveProxyButton;
    private FieldValidationHandler populationValidationHandler;
    private FieldValidationHandler capacityValidationHandler;
    private FieldValidationHandler facilitiesValidationHandler;
    private String tempPop = "0";

    public ProxyIndicationView() {
        viewWidget = createView();
    }

    private VerticalPanel createView() {
        initSelectProxyPopup();
        VerticalPanel view = new VerticalPanel();
        DockPanel dockPanel = new DockPanel();

        view.setStyleName("inputPanel");
        Label header = new Label("Proxy Indication");
        header.setStyleName("inputPanelHeader");

        assetThreatLabel = new Label("Asset - Threat");
        assetThreatLabel.setStyleName("inputPanelHeader");
        view.add(header);
        view.add(assetThreatLabel);

        HTML label = new HTML("Calculated Probability : ");
        probability = new HTML("");
        view.add(ViewUtil.layoutCenter(label, probability));

        changeProxyButton = ViewUtil.createButton("Change Proxy Indication");
        changeProxyButton.setWidth("200px");

        dockPanel.add(ViewUtil.layoutLeft(changeProxyButton), DockPanel.WEST);
        dockPanel.setWidth("100%");
        view.add(dockPanel);

        Widget proxyWidget = createProxyView();
        view.add(proxyWidget);

        saveButton = ViewUtil.createButton("Save");
        cancelButton = ViewUtil.createButton("Cancel");
        view.add(ViewUtil.layoutCenter(saveButton, cancelButton));
        return view;
    }

    private Widget createProxyView() {
        FlexTable inputTable = new FlexTable();
        inputTable.setWidth("100%");
        int currentRow = -1;
        name = ViewUtil.addTextBoxFormEntry(inputTable, ++currentRow, "Name", validationHandlers);

        description = ViewUtil.addTextBoxFormEntry(inputTable, ++currentRow, "Description");

        attacksPerYear = ViewUtil.addTextBoxFormEntry(inputTable, ++currentRow, "Attacks in U.S.", validationHandlers);
        ToolTipManager.addToolTip(attacksPerYear, "Number Of Attacks in U.S");
        attacksPerYear.setText("4");

        proxyCities = ViewUtil.addListBoxFormEntry(inputTable, ++currentRow, "Metro Region", validationHandlers);

        population = ViewUtil.addTextBoxFormEntry(inputTable, ++currentRow, "Population");
        population.setEnabled(false);
        populationValidationHandler = new FieldValidationHandler(population, PositiveIntegerValidator.INSTANCE);

        targetTypes = ViewUtil.addListBoxFormEntry(inputTable, ++currentRow, "Target Type", validationHandlers);

        targetTypeFacilitiesNum = ViewUtil.addTextBoxFormEntry(inputTable, ++currentRow, "Facilities Of Target Type", validationHandlers);
        facilitiesNum = ViewUtil.addTextBoxFormEntry(inputTable, ++currentRow, "Facilities Of Sub Type", validationHandlers);
        ToolTipManager.addToolTip(targetTypeFacilitiesNum, "Number Of Facilities of Subtype in your Metro Region");

        facilitiesValidationHandler = validationHandlers.get(validationHandlers.size() - 1);
        facilityCapacity = ViewUtil.addTextBoxFormEntry(inputTable, ++currentRow, "Facility Capacity", validationHandlers);
        regionalCapacity = ViewUtil.addTextBoxFormEntry(inputTable, ++currentRow, "Regional Capacity", validationHandlers);
        capacityValidationHandler = validationHandlers.get(validationHandlers.size() - 1);

        VerticalPanel view = new VerticalPanel();
        view.setStyleName("inputPanel");
        view.add(inputTable);
        //saveProxyButton = ViewUtil.createButton("Save Proxy");
        //view.add(ViewUtil.layoutCenter(saveProxyButton));
        return view;
    }

    private void initSelectProxyPopup() {
        selectProxyPopup = new ClosablePopup("Select/Copy/Create Proxy Indication", true);
        VerticalPanel panel = new VerticalPanel();
        panel.setStyleName("inputPanel");
        proxyPaths = new ListBox();
        panel.add(proxyPaths);
        selectProxyButton = ViewUtil.createButton("Select", "Use this Proxy Indication");
        copyProxyButton = ViewUtil.createButton("Copy", "Make a copy and use the copy");
        createProxyButton = ViewUtil.createButton("Create", "Create a Proxy Indication");
        proxyPaths.addChangeHandler(new ChangeHandler() {
            public void onChange(ChangeEvent event) {
                boolean hasSelection = proxyPaths.getSelectedIndex() > 0;
                selectProxyButton.setEnabled(hasSelection);
                copyProxyButton.setEnabled(hasSelection);
            }
        });
        panel.add(ViewUtil.layoutCenter(selectProxyButton, copyProxyButton, createProxyButton));
        selectProxyPopup.setWidget(panel);
    }

    public ListBox getProxyCities() {
        return proxyCities;
    }

    public ListBox getTargetTypes() {
        return targetTypes;
    }

    public HasClickHandlers getSaveButton() {
        return saveButton;
    }

    public HasClickHandlers getCancelButton() {
        return cancelButton;
    }

    public HasClickHandlers getSelectProxyButton() {
        return selectProxyButton;
    }

    public HasClickHandlers getCopyProxyButton() {
        return copyProxyButton;
    }

    public HasClickHandlers getCreateProxyButton() {
        return createProxyButton;
    }

    public HasClickHandlers getChangeProxyButton() {
        return changeProxyButton;
    }

    public void setPopulationActive(Boolean value) {
        if(value) {
            population.setText(tempPop);
        }
        else {
            tempPop = population.getText();
            population.setText("");
        }
        population.setEnabled(value);
    }

    @Override
    public TextBox getTargetTypeFacilitiesNum() {
        return targetTypeFacilitiesNum;
    }

    @Override
    public TextBox getFacilitiesNum() {
        return facilitiesNum;
    }

    @Override
    public TextBox getFacilityCapacity() {
        return facilityCapacity;
    }

    @Override
    public TextBox getRegionalCapacity() {
        return regionalCapacity;
    }

    @Override
    public TextBox getPopulation() {
        return population;
    }

    public ListBox getProxyIndications() {
        return proxyPaths;
    }

    public void showSelectProxyIndicationPopup() {
        selectProxyButton.setEnabled(false);
        copyProxyButton.setEnabled(false);
        selectProxyPopup.center();
    }

    public void hideSelectProxyIndicationPopup() {
        selectProxyPopup.hide();
    }

    public void setCalculatedProbability(BigDecimal calculatedProbability) {
        setProbability(probability, calculatedProbability);
    }


    public boolean validate() {
        if (getModel() == null) {
            return false;
        }
        if (!ViewUtil.validate(validationHandlers)) {
            return false;
        }
        Long proxyCityId = Long.valueOf(getSelectedValue(proxyCities));
        if (proxyCityId == -1) {
            return populationValidationHandler.checkValue();
        }
        if (getQuantity(facilitiesNum) > getQuantity(targetTypeFacilitiesNum)) {
            facilitiesValidationHandler.setValid(false, "Facilities should be less than Target Type Facilities");
            return false;
        }
        if (getQuantity(facilityCapacity) > getQuantity(regionalCapacity)) {
            capacityValidationHandler.setValid(false, "Facility capacity must be less than Regional Capacity");
            return false;
        }
        facilitiesValidationHandler.setValid(true, "");
        capacityValidationHandler.setValid(true, "");
        populationValidationHandler.setValid(true, "");
        return true;
    }

    public void updateModel() {
        ProxyIndicationDTO dto = getModel().getProxyIndicationDTO();
        dto.setName(getText(name));
        dto.setDescription(getText(description));
        dto.setAttacksPerYear(getQuantity(attacksPerYear));
        dto.setProxyCityDTO(new ProxyCityDTO(getSelectedId(proxyCities)));
        dto.setPopulation(getQuantity(population));
        dto.setTargetTypeDTO(new ProxyTargetTypeDTO(getSelectedId(targetTypes)));
        dto.setFacilitiesInRegionNum(getQuantity(facilitiesNum));
        dto.setTargetTypeInRegionNum(getQuantity(targetTypeFacilitiesNum));
        dto.setFacilityCapacity(getQuantity(facilityCapacity));
        dto.setRegionalCapacity(getQuantity(regionalCapacity));
    }

    public void clearDisplay() {
        setModel(null);
        assetThreatLabel.setText("");
        probability.setText("");
        saveButton.setEnabled(false);
    }

    public void updateView() {
        saveButton.setEnabled(true);
        assetThreatLabel.setText(getModel().getAssetName() + " - " + getModel().getThreatName());
        setProbability(probability, getModel().getLot());
        ProxyIndicationDTO dto = getModel().getProxyIndicationDTO();
        setText(name, dto.getName());
        setText(description, dto.getDescription());
        setQuantity(attacksPerYear, dto.getAttacksPerYear());
        String proxyCity = "";
        if (dto.getProxyCityDTO() != null) {
            proxyCity = String.valueOf(dto.getProxyCityDTO().getId());
        }
        ClientLoggingUtil.info("ProxyCity : " + proxyCity);
        setSelectedValue(proxyCities, proxyCity);
        setQuantity(population, dto.getPopulation());

        String targetType = "";
        if (dto.getTargetTypeDTO() != null) {
            targetType = String.valueOf(dto.getTargetTypeDTO().getId());
        }
        ClientLoggingUtil.info("TargetType : " + targetType);
        setSelectedValue(targetTypes, targetType);
        setQuantity(facilitiesNum, dto.getFacilitiesInRegionNum());
        setQuantity(targetTypeFacilitiesNum, dto.getTargetTypeInRegionNum());
        setQuantity(facilityCapacity, dto.getFacilityCapacity());
        setQuantity(regionalCapacity, dto.getRegionalCapacity());
    }

    public HasClickHandlers getSaveProxyIndicationButton() {
        return saveProxyButton;
    }

    public Widget asWidget() {
        return viewWidget;
    }
}
