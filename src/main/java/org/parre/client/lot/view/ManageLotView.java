/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.lot.view;

import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.event.logical.shared.HasCloseHandlers;
import com.google.gwt.user.client.ui.*;
import com.google.gwt.view.client.SingleSelectionModel;

import java.util.List;

import org.parre.client.common.view.*;
import org.parre.client.lot.presenter.ManageLotPresenter;
import org.parre.client.util.ActionSource;
import org.parre.client.util.ClosablePopup;
import org.parre.client.util.ViewUtil;
import org.parre.client.util.dataview.DataProvider;
import org.parre.client.util.dataview.DataProviderFactory;
import org.parre.shared.LotAnalysisDTO;
import org.parre.shared.LotDTO;
import org.parre.shared.types.LotAnalysisType;

/**
 * The Class ManageLotView.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 7/16/11
 */
public class ManageLotView extends BaseAnalysisView implements ManageLotPresenter.Display {
    private LotGrid lotGrid;
    private DeckLayoutPanel searchCriteriaDeck;
    private AssetCriteriaView assetCriteriaView;
    private ThreatCriteriaView threatCriteriaView;
    private Image searchButton;
    private Image clearButton;
    private Image threatCriteriaButton;
    private Image assetCriteriaButton;
    private VerticalPanel searchPanel;
    private Widget viewWidget;
    private DialogBox editPopup;
    private EditLotView editVulnerabilityView;
    private DisplayToggleView displayToggleView;
    private Button changeMethodButton;
    private DialogBox setAnalysisPopUp;
    private ListBox analysisType;

    public ManageLotView(AssetCriteriaView assetCriteriaView, ThreatCriteriaView threatCriteriaView,
                         EditLotView editVulnerabilityView, DisplayToggleView displayToggleView) {
        super(new AnalysisCommandPanel(), new NameDescriptionView());
        this.assetCriteriaView = assetCriteriaView;
        this.threatCriteriaView = threatCriteriaView;
        this.editVulnerabilityView = editVulnerabilityView;
        this.displayToggleView = displayToggleView;
        viewWidget = createView();
    }

    private Widget createView() {
        searchPanel = new VerticalPanel();
        searchPanel.setWidth("100%");

        searchButton = ViewUtil.createImageButton("search-button.png", "Perform Search");
        clearButton = ViewUtil.createImageButton("clear-button.jpg", "Clear Search Results");
        threatCriteriaButton = ViewUtil.createImageButton("threat.jpg", "Search Using Threats");
        assetCriteriaButton = ViewUtil.createImageButton("asset.jpg", "Search Using Assets");

        searchCriteriaDeck = new DeckLayoutPanel();
        searchCriteriaDeck.setStyleName("inputPanel");
        searchCriteriaDeck.setHeight("100px");
        searchCriteriaDeck.add(assetCriteriaView.asWidget());
        searchCriteriaDeck.add(threatCriteriaView.asWidget());
        searchPanel.add(searchCriteriaDeck);

        searchPanel.add(ViewUtil.layoutCenter(searchButton, assetCriteriaButton, threatCriteriaButton, clearButton));

        DataProvider<LotDTO> dataProvider = DataProviderFactory.createDataProvider();
        lotGrid = new LotGrid(dataProvider);

        searchCriteriaDeck.showWidget(0);

        getCommandPanel().setToolTips("Back to Vulnerability Analysis", "Forward to Natural Threat Analysis");
        displayToggleView.setText("Search Criteria");
        displayToggleView.setDisplayToggleCommand(new DisplayToggleView.DisplayToggleCommand() {
            public void toggleDisplay(boolean show) {
                setSearchVisible(show);
            }
        });

        changeMethodButton = ViewUtil.createButton("Change Analysis Method");
        changeMethodButton.setEnabled(false);

        displayToggleView.setDisplayVisible(false);
        //assetButtonPanel.setWidth("75%");
        VerticalPanel viewPanel = new VerticalPanel();

        viewPanel.setWidth("100%");
        viewPanel.setHeight("100%");
        viewPanel.add(displayToggleView.asWidget());
        viewPanel.add(searchPanel);
        viewPanel.add(getCommandPanel().asWidget());
        viewPanel.add(ViewUtil.layoutCenter(changeMethodButton));
        viewPanel.add(lotGrid.asWidget());
        viewPanel.setSpacing(5);
        lotGrid.asWidget().setVisible(false);
        initEditVulnerabilityPopup();
        initSetAnalysisPopUp();
        return viewPanel;
    }

    public void setSearchVisible(boolean value) {
        searchPanel.setVisible(value);
        lotGrid.setHeight(value ? "500px" : "650px");
    }

    private void initEditVulnerabilityPopup() {
        editPopup = new ClosablePopup("Edit Likelihood of Threat", true);
        editPopup.setModal(true);
        Widget widget = editVulnerabilityView.asWidget();
        widget.setWidth("300px");
        editPopup.setWidget(widget);
    }

    private void initSetAnalysisPopUp() {
        setAnalysisPopUp = new ClosablePopup("Set Analysis Method", true);
        setAnalysisPopUp.setModal(true);
        VerticalPanel panel = new VerticalPanel();
        analysisType = new ListBox();
        panel.add(analysisType);
        setAnalysisPopUp.add(panel);
    }

    public SingleSelectionModel<LotDTO> getSearchResultsSelectionModel() {
        return lotGrid.getSelectionModel();
    }

    public ActionSource<LotDTO> getSelectionSource() {
        return lotGrid.getSelectionSource();
    }

    public HasClickHandlers getSearchButton() {
        return searchButton;
    }

    public HasClickHandlers getClearButton() {
        return clearButton;
    }

    public HasClickHandlers getThreatCriteriaButton() {
        return threatCriteriaButton;
    }

    public HasClickHandlers getAssetCriteriaButton() {
        return assetCriteriaButton;
    }

    public void showSearchView(int searchViewIndex) {
        searchCriteriaDeck.showWidget(searchViewIndex);
        if (ManageLotPresenter.ASSET_SEARCH == searchViewIndex) {
            assetCriteriaButton.setVisible(false);
            threatCriteriaButton.setVisible(true);
        }
        else {
            assetCriteriaButton.setVisible(true);
            threatCriteriaButton.setVisible(false);
        }
    }

    public void setSearchResults(List<LotDTO> results) {
        lotGrid.asWidget().setVisible(true);
        getCommandPanel().setAnalysisCount(results.size());
        lotGrid.setData(results);
        lotGrid.getSelectionModel().setSelected(null, true);
    }

    public void refreshSearchResultsView() {
        lotGrid.refresh();
    }

    public Widget asWidget() {
        return viewWidget;
    }

    public HasCloseHandlers<PopupPanel> getEditPopup() {
        return editPopup;
    }

    public void showEditPopup() {
        editPopup.center();
        editPopup.show();
        setSearchResultsViewDisabled(true);
    }

    public void setSearchResultsViewDisabled(boolean value) {
        if (value) {
            lotGrid.asWidget().getElement().setAttribute("disabled", "true");
        }
        else {
            lotGrid.asWidget().getElement().removeAttribute("disabled");
        }
    }

    public void setCurrentAnalysis(LotAnalysisDTO dto) {
        super.setCurrentAnalysis(dto);
        boolean haveAnalysis = dto != null;
        lotGrid.asWidget().setVisible(haveAnalysis);
    }

    @Override
    public void showSetAnalysisPopUp() {
        analysisType.setSelectedIndex(0);
        setAnalysisPopUp.show();
        setAnalysisPopUp.center();
    }

    @Override
    public void hideSetAnalysisPopUp() {
        setAnalysisPopUp.hide();
    }

    @Override
    public ListBox getAnalysisType() {
        return analysisType;
    }

    @Override
    public void setChangeAnalysisActive(boolean value) {
        changeMethodButton.setEnabled(value);
    }

    @Override
    public HasClickHandlers getChangeAnalysisButton() {
        return changeMethodButton;
    }

    public void hideEditAssetPopup() {
        editPopup.hide();
    }

    public List<LotDTO> getSearchResults() {
        return lotGrid.getData();
    }

    @Override
    public void refreshView() {
        lotGrid.forceLayout();
    }

    @Override
    public HasClickHandlers getPreviousButton() {
        return getCommandPanel().getPreviousButton();
    }

    @Override
    public HasClickHandlers getNextButton() {
        return getCommandPanel().getNextButton();
    }
}
