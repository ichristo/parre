/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.lot.presenter;

import com.google.gwt.event.dom.client.*;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.HasText;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextBox;

import java.math.BigDecimal;
import java.util.List;

import org.parre.client.ClientSingleton;
import org.parre.client.lot.event.EditProxyIndicationEvent;
import org.parre.client.lot.event.LotEditFinishedEvent;
import org.parre.client.lot.event.LotUpdatedEvent;
import org.parre.client.lot.event.ProxyIndicationEditFinishedEvent;
import org.parre.client.messaging.ServiceFactory;
import org.parre.client.util.*;
import org.parre.shared.LabelValueDTO;
import org.parre.shared.LotDTO;
import org.parre.shared.ProxyIndicationDTO;

/**
 * The Class ProxyIndicationPresenter.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 1/12/12
 */
public class ProxyIndicationPresenter implements Presenter {
    private Display display;
    private EventBus eventBus;
    private BusyHandler busyHandler;
    private LotDTO lotDTO;
    private List<ProxyIndicationDTO> existingProxyIndications;

    /**
     * The Interface Display.
     */
    public interface Display extends ModelView<LotDTO> {

        HasClickHandlers getSaveProxyIndicationButton();

        void setCalculatedProbability(BigDecimal calculatedProbability);

        HasClickHandlers getSaveButton();

        HasClickHandlers getCancelButton();

        HasClickHandlers getSelectProxyButton();

        ListBox getProxyIndications();

        void showSelectProxyIndicationPopup();

        void hideSelectProxyIndicationPopup();

        void clearDisplay();

        HasClickHandlers getCopyProxyButton();

        ListBox getProxyCities();

        ListBox getTargetTypes();

        HasClickHandlers getCreateProxyButton();

        HasClickHandlers getChangeProxyButton();

        void setPopulationActive(Boolean value);

        TextBox getTargetTypeFacilitiesNum();

        TextBox getFacilitiesNum();

        TextBox getFacilityCapacity();

        TextBox getRegionalCapacity();

        TextBox getPopulation();
    }

    public ProxyIndicationPresenter(Display display) {
        this.display = display;
        this.eventBus = ClientSingleton.getEventBus();
        this.busyHandler = new BusyHandler(display);
    }

    public void go() {
        bind();
    }

    private void bind() {
        new PopulateListBoxCommand(display.getProxyCities(), busyHandler) {
            @Override
            protected void getListBoxContents(AsyncCallback<List<LabelValueDTO>> asyncCallback) {
                ServiceFactory.getInstance().getLotService().getProxyCities(asyncCallback);
            }
        }.execute();

        new PopulateListBoxCommand(display.getTargetTypes(), busyHandler) {
            @Override
            protected void getListBoxContents(AsyncCallback<List<LabelValueDTO>> asyncCallback) {
                ServiceFactory.getInstance().getLotService().getProxyTargetTypes(asyncCallback);
            }
        }.execute();

        eventBus.addHandler(EditProxyIndicationEvent.TYPE, new EditProxyIndicationEvent.Handler() {
            public void onEvent(EditProxyIndicationEvent event) {
                lotDTO = event.getLotDTO();
                if (!lotDTO.hasProxyIndication()) {
                    initProxyIndication();
                }
                else {
                    display.setModelAndUpdateView(lotDTO);
                }
            }
        });
        /*display.getSaveProxyIndicationButton().addClickHandler(new ValidatingClickHandler(display) {
            @Override
            protected void handleClick(ClickEvent event) {
                saveProxy();
            }
        });*/

        display.getSaveButton().addClickHandler(new ValidatingClickHandler(display) {
            @Override
            protected void handleClick(ClickEvent event) {
                saveProxy();
                //saveLot();
            }
        });

        display.getCancelButton().addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                eventBus.fireEvent(LotEditFinishedEvent.create(display.getModel()));
                eventBus.fireEvent(ProxyIndicationEditFinishedEvent.create(display.getModel()));
                display.clearDisplay();
            }
        });


        display.getSelectProxyButton().addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                selectAndAssociate(false);
            }
        });
        display.getCopyProxyButton().addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                selectAndAssociate(true);
            }
        });
        display.getCreateProxyButton().addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                display.hideSelectProxyIndicationPopup();
                initDisplay(new ProxyIndicationDTO());
            }
        });
        display.getChangeProxyButton().addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                showSelectProxyIndicationPopup();
            }
        });
        display.getProxyCities().addChangeHandler(new ChangeHandler() {
            public void onChange(ChangeEvent changeEvent) {
                if (display.getProxyCities().getSelectedIndex() != 15) {
                    display.setPopulationActive(false);
                } else {
                    display.setPopulationActive(true);
                }
            }
        });
        display.getPopulation().addBlurHandler(new BlurHandler() {
            @Override
            public void onBlur(BlurEvent event) {
                ViewUtil.setQuantity(display.getPopulation());
            }
        });
        display.getTargetTypeFacilitiesNum().addBlurHandler(new BlurHandler() {
            @Override
            public void onBlur(BlurEvent event) {
                ViewUtil.setQuantity(display.getTargetTypeFacilitiesNum());
            }
        });
        display.getFacilitiesNum().addBlurHandler(new BlurHandler() {
            @Override
            public void onBlur(BlurEvent event) {
               ViewUtil.setQuantity(display.getFacilitiesNum());
            }
        });
        display.getFacilityCapacity().addBlurHandler(new BlurHandler() {
            @Override
            public void onBlur(BlurEvent event) {
                ViewUtil.setQuantity(display.getFacilityCapacity());
            }
        });
        display.getRegionalCapacity().addBlurHandler(new BlurHandler() {
            @Override
            public void onBlur(BlurEvent event) {
                ViewUtil.setQuantity(display.getRegionalCapacity());
            }
        });
    }

    private void selectAndAssociate(boolean makeCopy) {
        display.hideSelectProxyIndicationPopup();
        Long proxyIndicationId = Long.valueOf(ViewUtil.getSelectedValue(display.getProxyIndications()));
        for (ProxyIndicationDTO dto : existingProxyIndications) {
            if (proxyIndicationId.equals(dto.getId())) {
                lotDTO.setProxyIndicationDTO(dto);
                break;
            }
        }
        associateProxyIndicationWithLot(makeCopy);
    }

    private void saveLot() {
        ExecutableAsyncCallback<LotDTO> exec =
                new ExecutableAsyncCallback<LotDTO>(busyHandler, new BaseAsyncCommand<LotDTO>() {
                    public void execute(AsyncCallback<LotDTO> callback) {
                        LotDTO lotDTO = display.updateAndGetModel();
                        ServiceFactory.getInstance().getLotService().saveWithProxyIndication(lotDTO, callback);
                    }

                    @Override
                    public void handleResult(LotDTO results) {
                        eventBus.fireEvent(LotUpdatedEvent.create(results));
                        eventBus.fireEvent(LotEditFinishedEvent.create(results));
                        eventBus.fireEvent(ProxyIndicationEditFinishedEvent.create(results));
                        display.clearDisplay();
                    }
                });
        exec.makeCall();
    }

    private void associateProxyIndicationWithLot(final boolean makeCopy) {
        ExecutableAsyncCallback<ProxyIndicationDTO> exec =
                new ExecutableAsyncCallback<ProxyIndicationDTO>(busyHandler, new BaseAsyncCommand<ProxyIndicationDTO>() {
                    public void execute(AsyncCallback<ProxyIndicationDTO> callback) {
                        Long lotId = lotDTO.getId();
                        Long proxyIndicationId = lotDTO.getProxyIndicationDTO().getId();
                        ServiceFactory.getInstance().getLotService().associateProxyIndication(lotId, proxyIndicationId, makeCopy, callback);
                    }

                    @Override
                    public void handleResult(ProxyIndicationDTO result) {
                        lotDTO.setProxyIndicationDTO(result);
                        lotDTO.setLot(result.getCalculatedProbability());
                        display.setModelAndUpdateView(lotDTO);
                    }
                });
        exec.makeCall();
    }

    private void saveProxy() {
        ExecutableAsyncCallback<ProxyIndicationDTO> exec =
                new ExecutableAsyncCallback<ProxyIndicationDTO>(busyHandler, new BaseAsyncCommand<ProxyIndicationDTO>() {
                    public void execute(AsyncCallback<ProxyIndicationDTO> callback) {
                        ProxyIndicationDTO proxyIndicationDTO = display.updateAndGetModel().getProxyIndicationDTO();
                        proxyIndicationDTO.setParreAnalysisId(ClientSingleton.getParreAnalysisId());
                        ServiceFactory.getInstance().getLotService().saveProxyIndication(proxyIndicationDTO, callback);
                    }

                    @Override
                    public void handleResult(ProxyIndicationDTO indicationDTO) {
                        lotDTO.setProxyIndicationDTO(indicationDTO);
                        saveLot();
                    }
                });
        exec.makeCall();

    }


    private void initProxyIndication() {
        ExecutableAsyncCallback<ProxyIndicationDTO> exec =
                new ExecutableAsyncCallback<ProxyIndicationDTO>(busyHandler, new BaseAsyncCommand<ProxyIndicationDTO>() {
                    public void execute(AsyncCallback<ProxyIndicationDTO> callback) {
                        ServiceFactory.getInstance().getLotService().getProxyIndication(lotDTO.getId(), callback);
                    }

                    @Override
                    public void handleResult(ProxyIndicationDTO proxyIndicationDTO) {
                        if (proxyIndicationDTO == null) {
                            showSelectProxyIndicationPopup();
                        } else {
                            initDisplay(proxyIndicationDTO);
                        }
                    }
                });
        exec.makeCall();
    }

    private void initDisplay(ProxyIndicationDTO proxyIndicationDTO) {
        lotDTO.setProxyIndicationDTO(proxyIndicationDTO);
        display.setModelAndUpdateView(lotDTO);
    }

    private void showSelectProxyIndicationPopup() {
        ExecutableAsyncCallback<List<ProxyIndicationDTO>> exec =
                new ExecutableAsyncCallback<List<ProxyIndicationDTO>>(busyHandler, new BaseAsyncCommand<List<ProxyIndicationDTO>>() {
                    public void execute(AsyncCallback<List<ProxyIndicationDTO>> callback) {
                        ServiceFactory.getInstance().getLotService().getExistingProxyIndications(ClientSingleton.getParreAnalysisId(), callback);
                    }

                    @Override
                    public void handleResult(List<ProxyIndicationDTO> results) {
                        existingProxyIndications = results;
                        ListBox proxyIndications = display.getProxyIndications();
                        proxyIndications.clear();
                        proxyIndications.addItem("Select", "");
                        for (ProxyIndicationDTO dto : results) {
                            proxyIndications.addItem(dto.getName(), String.valueOf(dto.getId()));
                        }
                        display.showSelectProxyIndicationPopup();
                    }
                });
        exec.makeCall();

    }

}
