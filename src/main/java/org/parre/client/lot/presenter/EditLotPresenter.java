/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.lot.presenter;

import com.google.gwt.event.dom.client.*;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.web.bindery.event.shared.EventBus;

import java.math.BigDecimal;

import org.parre.client.ClientSingleton;
import org.parre.client.lot.event.*;
import org.parre.client.messaging.ServiceFactory;
import org.parre.client.util.*;
import org.parre.shared.LotDTO;
import org.parre.shared.types.LotAnalysisType;

/**
 * The Class EditLotPresenter.
 */
public class EditLotPresenter implements Presenter {
    private EventBus eventBus;
    private Display display;
    private BusyHandler busyHandler;
    private LotAnalysisType newType = null;

    /**
     * The Interface Display.
     */
    public interface Display extends ModelView<LotDTO> {
        HasClickHandlers getSaveButton();

        void setProbabilty(String value);

        void setProbabilityEnabled(Boolean value);
    }

    public EditLotPresenter(Display display) {
        this.eventBus = ClientSingleton.getEventBus();
        this.display = display;
        busyHandler = new BusyHandler(display);
    }

    public void go() {
        bind();

    }

    private void bind() {
        eventBus.addHandler(BestEstimateEvent.TYPE, new BestEstimateEvent.Handler() {
            public void onEvent(BestEstimateEvent event) {
                LotDTO editDTO = event.getLotDTO();
                newType = LotAnalysisType.BEST_ESTIMATE;
                display.setModelAndUpdateView(editDTO);
                display.setProbabilityEnabled(true);
            }
        });
        eventBus.addHandler(ConditionalRiskEvent.TYPE, new ConditionalRiskEvent.Handler() {
            public void onEvent(ConditionalRiskEvent event) {
                LotDTO editDTO = event.getLotDTO();
                newType = LotAnalysisType.CONDITIONAL_RISK;
                display.setModelAndUpdateView(editDTO);
                display.setProbabilty("1");
                display.setProbabilityEnabled(false);
            }
        });
        display.getSaveButton().addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                if (!display.validate()) {
                    return;
                }
                ExecutableAsyncCallback<LotDTO> exec =
                        new ExecutableAsyncCallback<LotDTO>(busyHandler, new BaseAsyncCommand<LotDTO>() {
                            public void execute(AsyncCallback<LotDTO> callback) {
                                LotDTO dto = display.updateAndGetModel();
                                if(newType != null) {
                                    dto.setLotAnalysisType(newType);
                                }
                                ServiceFactory.getInstance().getLotService().save(dto, callback);
                            }

                            @Override
                            public void handleResult(LotDTO result) {
                                eventBus.fireEvent(LotUpdatedEvent.create(result));
                                eventBus.fireEvent(LotUpdateFinishedEvent.create(result));
                            }
                        });
                exec.makeCall();
                display.updateView();
            }
        });

    }

}
