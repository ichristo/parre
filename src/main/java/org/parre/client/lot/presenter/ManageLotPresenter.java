/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.lot.presenter;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.parre.client.ClientSingleton;
import org.parre.client.ParreClientUtil;
import org.parre.client.asset.event.AssetSearchEvent;
import org.parre.client.asset.event.ClearAssetSearchEvent;
import org.parre.client.common.event.AssetThreatSearchType;
import org.parre.client.common.presenter.ManageAnalysisPresenter;
import org.parre.client.common.view.AnalysisView;
import org.parre.client.event.GoHomeEvent;
import org.parre.client.event.LogoutEvent;
import org.parre.client.lot.event.BestEstimateEvent;
import org.parre.client.lot.event.ConditionalRiskEvent;
import org.parre.client.lot.event.EditProxyIndicationEvent;
import org.parre.client.lot.event.LotSearchResultsEvent;
import org.parre.client.lot.event.LotUpdateFinishedEvent;
import org.parre.client.lot.event.LotUpdatedEvent;
import org.parre.client.lot.event.ManageLotNavEvent;
import org.parre.client.messaging.ServiceFactory;
import org.parre.client.nav.event.NaturalThreatNavEvent;
import org.parre.client.nav.event.VulnerabilityNavEvent;
import org.parre.client.threat.event.ThreatSearchEvent;
import org.parre.client.util.ActionSource;
import org.parre.client.util.BaseAsyncCommand;
import org.parre.client.util.BusyHandler;
import org.parre.client.util.ClientLoggingUtil;
import org.parre.client.util.ExecutableAsyncCallback;
import org.parre.client.util.Presenter;
import org.parre.client.util.ViewUtil;
import org.parre.client.util.nav.NavManager;
import org.parre.shared.AssetSearchDTO;
import org.parre.shared.LotAnalysisDTO;
import org.parre.shared.LotDTO;
import org.parre.shared.types.AssetThreatAnalysisType;
import org.parre.shared.types.LotAnalysisType;

import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.event.logical.shared.CloseEvent;
import com.google.gwt.event.logical.shared.CloseHandler;
import com.google.gwt.event.logical.shared.HasCloseHandlers;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.event.shared.GwtEvent;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.view.client.SelectionChangeEvent;
import com.google.gwt.view.client.SingleSelectionModel;


/**
 * User: Swarn S. Dhaliwal
 * Date: 8/21/11
 * Time: 5:03 PM
*/
public class ManageLotPresenter extends ManageAnalysisPresenter implements Presenter {
    private LotDTO selectedLotDTO;
    public static final int ASSET_SEARCH = 0;
    public static final int THREAT_SEARCH = 1;

    public static final Map<Integer, GwtEvent> viewEventMap = new HashMap<Integer, GwtEvent>(2);

    static {
        viewEventMap.put(ASSET_SEARCH, AssetSearchEvent.create(AssetThreatSearchType.LOT));
        viewEventMap.put(THREAT_SEARCH, ThreatSearchEvent.create(AssetThreatSearchType.LOT));
    }

    private List<LotDTO> lotList;
    private LotAnalysisDTO currentAnalysis;

    /**
     * The Interface Display.
     */
    public static interface Display extends AnalysisView {
        SingleSelectionModel<LotDTO> getSearchResultsSelectionModel();

        ActionSource<LotDTO> getSelectionSource();

        HasClickHandlers getSearchButton();

        HasClickHandlers getClearButton();

        HasClickHandlers getThreatCriteriaButton();

        void setSearchResults(List<LotDTO> dtos);

        void refreshSearchResultsView();

        void showSearchView(int searchViewIndex);

        HasClickHandlers getAssetCriteriaButton();

        HasCloseHandlers<PopupPanel> getEditPopup();

        void showEditPopup();

        void hideEditAssetPopup();

        List<LotDTO> getSearchResults();

        void setSearchResultsViewDisabled(boolean value);

        void setCurrentAnalysis(LotAnalysisDTO dto);

        void showSetAnalysisPopUp();

        void hideSetAnalysisPopUp();

        ListBox getAnalysisType();

        void setChangeAnalysisActive(boolean value);

        HasClickHandlers getChangeAnalysisButton();
    }

    private int currentSearchView = ASSET_SEARCH;
    private EventBus eventBus;
    private Display display;
    private BusyHandler busyHandler;

    public ManageLotPresenter(Display display) {
        super(display);
        this.eventBus = ClientSingleton.getEventBus();
        this.display = display;
        this.busyHandler = new BusyHandler(display);
    }

    public void go() {
        bind();
        display.showSearchView(currentSearchView);
        init();
    }

    private GwtEvent getSearchEvent() {
        return viewEventMap.get(currentSearchView);
    }

    private void bind() {
        new LotAnalysisTypeCommand(display.getAnalysisType(), busyHandler).execute();
        eventBus.addHandler(ManageLotNavEvent.TYPE, new ManageLotNavEvent.Handler() {
            public void onEvent(ManageLotNavEvent event) {
                getCurrentAnalysis();
            }
        });
        eventBus.addHandler(LotUpdateFinishedEvent.TYPE, new LotUpdateFinishedEvent.Handler() {
            public void onEvent(LotUpdateFinishedEvent event) {
                display.hideEditAssetPopup();
            }
        });
        eventBus.addHandler(EditProxyIndicationEvent.TYPE, new EditProxyIndicationEvent.Handler() {
            public void onEvent(EditProxyIndicationEvent event) {
                display.hideEditAssetPopup();
            }
        });
        eventBus.addHandler(LotUpdatedEvent.TYPE, new LotUpdatedEvent.Handler() {
            public void onEvent(LotUpdatedEvent event) {
                LotDTO updated = event.getLotDTO();
                List<LotDTO> currentResults = display.getSearchResults();
                int index = currentResults.indexOf(updated);
                currentResults.set(index, updated);
                display.refreshSearchResultsView();
                index = lotList.indexOf(updated);
                lotList.set(index, updated);
            }
        });
        eventBus.addHandler(GoHomeEvent.TYPE, new GoHomeEvent.Handler() {
            public void onEvent(GoHomeEvent event) {
                currentSearchView = ASSET_SEARCH;
                display.showSearchView(currentSearchView);
                display.setSearchResults(Collections.<LotDTO>emptyList());
            }
        });
        display.getClearButton().addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                eventBus.fireEvent(ClearAssetSearchEvent.create());
                display.setSearchResults(Collections.<LotDTO>emptyList());
            }
        });
        display.getThreatCriteriaButton().addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                switchSearchView(THREAT_SEARCH);
            }
        });
        display.getAssetCriteriaButton().addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                switchSearchView(ASSET_SEARCH);
            }
        });
        display.getSearchButton().addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                eventBus.fireEvent(getSearchEvent());
            }
        });
        display.getSelectionSource().setCommand(new ActionSource.ActionCommand<LotDTO>() {
            public void execute(LotDTO dto) {
                edit(dto);
            }
        });
        display.getEditPopup().addCloseHandler(new CloseHandler<PopupPanel>() {
            public void onClose(CloseEvent<PopupPanel> popupPanelCloseEvent) {
                display.setSearchResultsViewDisabled(false);
            }
        });

        display.getSearchResultsSelectionModel().addSelectionChangeHandler(new SelectionChangeEvent.Handler() {
            public void onSelectionChange(SelectionChangeEvent event) {
                SingleSelectionModel<LotDTO> selectionModel = (SingleSelectionModel<LotDTO>) event.getSource();
                vulnerabilitySelected(selectionModel.getSelectedObject());
            }
        });
        display.getPreviousButton().addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent clickEvent) {
                NavManager.performNav(VulnerabilityNavEvent.create());
            }
        });
        display.getNextButton().addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent clickEvent) {
                NavManager.performNav(NaturalThreatNavEvent.create());
            }
        });
        display.getAnalysisType().addChangeHandler(new ChangeHandler() {
            public void onChange(ChangeEvent event) {
                String selectedValue = ViewUtil.getSelectedValue(display.getAnalysisType());
                LotDTO dto = display.getSearchResultsSelectionModel().getSelectedObject();
                ClientLoggingUtil.debug("Selected Value = " + selectedValue);
                if (selectedValue.length() == 0) {
                    return;
                }
                LotAnalysisType selectedType = LotAnalysisType.valueOf(selectedValue);
                if(selectedType.equals(LotAnalysisType.BEST_ESTIMATE)) {
                    eventBus.fireEvent(BestEstimateEvent.create(dto));
                    display.showEditPopup();
                }
                else if(selectedType.equals(LotAnalysisType.CONDITIONAL_RISK)) {
                    eventBus.fireEvent(ConditionalRiskEvent.create(dto));
                    display.showEditPopup();
                }
                else if(selectedType.equals(LotAnalysisType.PROXY_INDICATOR)) {
                    eventBus.fireEvent(EditProxyIndicationEvent.create(dto));
                }
                else {
                    return;
                }
                display.hideSetAnalysisPopUp();
            }
        });
        display.getChangeAnalysisButton().addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent clickEvent) {
                display.showSetAnalysisPopUp();
            }
        });
        eventBus.addHandler(LotSearchResultsEvent.TYPE, new LotSearchResultsEvent.Handler() {
            public void onEvent(LotSearchResultsEvent event) {
                lotList = event.getResults();
                display.setSearchResults(lotList);
                display.getSearchResultsSelectionModel().setSelected(null, true);
                display.refreshView();
            }
        });
        eventBus.addHandler(LogoutEvent.TYPE, new LogoutEvent.Handler() {
            public void onEvent(LogoutEvent event) {
                switchSearchView(ASSET_SEARCH);
            }
        });
    }

    private void edit(LotDTO dto) {
        if (ParreClientUtil.handleAnalysisBaselined()) {
            return;
        }
        if(dto.getLotAnalysisType().equals(LotAnalysisType.BEST_ESTIMATE)) {
            eventBus.fireEvent(BestEstimateEvent.create(dto));
            display.showEditPopup();
        }
        else if(dto.getLotAnalysisType().equals(LotAnalysisType.PROXY_INDICATOR)) {
            eventBus.fireEvent(EditProxyIndicationEvent.create(dto));
        }
        else if(dto.getLotAnalysisType().equals(LotAnalysisType.CONDITIONAL_RISK)) {
            eventBus.fireEvent(ConditionalRiskEvent.create(dto));
            display.showEditPopup();
        }
        else {
            display.showSetAnalysisPopUp();
        }
    }

    @Override
    protected AssetThreatAnalysisType getAnalysisType() {
        return AssetThreatAnalysisType.LOT;
    }

    @Override
    protected void startAnalysis() {
        final LotAnalysisDTO analysisDTO = new LotAnalysisDTO();
        analysisDTO.setParreAnalysisId(ClientSingleton.getParreAnalysisId());
        ExecutableAsyncCallback<LotAnalysisDTO> exec =
                new ExecutableAsyncCallback<LotAnalysisDTO>(busyHandler, new BaseAsyncCommand<LotAnalysisDTO>() {
                    public void execute(AsyncCallback<LotAnalysisDTO> callback) {
                        ServiceFactory.getInstance().getLotService().startAnalysis(analysisDTO, callback);
                    }

                    @Override
                    public void handleResult(LotAnalysisDTO results) {
                        initCurrentAnalysis(results);
                    }
                });
        exec.makeCall();

    }

    protected void getCurrentAnalysis() {
        ExecutableAsyncCallback<LotAnalysisDTO> exec =
                new ExecutableAsyncCallback<LotAnalysisDTO>(busyHandler, new BaseAsyncCommand<LotAnalysisDTO>() {
                    public void execute(AsyncCallback<LotAnalysisDTO> callback) {
                        ServiceFactory.getInstance().getLotService().
                                getCurrentAnalysis(ClientSingleton.getParreAnalysisId(), callback);
                    }

                    @Override
                    public void handleResult(LotAnalysisDTO results) {
                        initCurrentAnalysis(results);
                    }
                });
        exec.makeCall();

    }

    private void initCurrentAnalysis(LotAnalysisDTO dto) {
        this.currentAnalysis = dto;
        super.initCurrentAnalysis(dto);
        display.setCurrentAnalysis(dto);
        if (dto != null) {
            new LotAssetSearchCommand().execute(new AssetSearchDTO(), display);
        }
    }


    private void switchSearchView(int viewIndex) {
        currentSearchView = viewIndex;
        display.showSearchView(viewIndex);
        display.setSearchResults(Collections.<LotDTO>emptyList());
    }


    public void vulnerabilitySelected(LotDTO dto) {
        selectedLotDTO = dto;
        if(ClientSingleton.isCurrentAnalysisBaselined()) {
            display.setChangeAnalysisActive(false);
        } else {
            display.setChangeAnalysisActive(dto != null);
        }

    }

}
