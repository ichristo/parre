/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.lot.presenter;

import org.parre.client.ClientSingleton;
import org.parre.client.ViewFactory;
import org.parre.client.lot.event.EditProxyIndicationEvent;
import org.parre.client.lot.event.ManageLotNavEvent;
import org.parre.client.lot.event.ProxyIndicationEditFinishedEvent;
import org.parre.client.nav.event.LotNavEvent;
import org.parre.client.util.Presenter;
import org.parre.client.util.View;
import org.parre.client.util.ViewContainer;

import com.google.gwt.event.shared.EventBus;

/**
 * The Class LotPresenter.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 7/19/11
 */
public class LotPresenter implements Presenter {
    private Integer manageLotViewIndex;
    private Integer editProxyViewIndex;

    private EventBus eventBus;
    private Display display;

    /**
     * The Interface Display.
     */
    public interface Display extends View, ViewContainer {

        void showView(int editEquipmentView);
    }

    public LotPresenter(Display display) {
        this.eventBus = ClientSingleton.getEventBus();
        this.display = display;
    }

    public void go() {
        initViewLoaders();
        bind();
    }

    private void initViewLoaders() {
        View view;
        view = ViewFactory.createManageLotView();
        view.getPresenter().go();
        manageLotViewIndex = display.addAndShowView(view.asWidget());
        view = ViewFactory.createProxyIndicationView();
        view.getPresenter().go();
        editProxyViewIndex = display.addView(view.asWidget());
    }

    private void bind() {
        eventBus.addHandler(LotNavEvent.TYPE, new LotNavEvent.Handler() {
            public void onEvent(LotNavEvent event) {
                eventBus.fireEvent(ManageLotNavEvent.create());
            }
        });
        eventBus.addHandler(EditProxyIndicationEvent.TYPE, new EditProxyIndicationEvent.Handler() {
            public void onEvent(EditProxyIndicationEvent event) {
                display.showView(editProxyViewIndex);
            }
        });
        eventBus.addHandler(ProxyIndicationEditFinishedEvent.TYPE, new ProxyIndicationEditFinishedEvent.Handler() {
            public void onEvent(ProxyIndicationEditFinishedEvent event) {
                display.showView(manageLotViewIndex);
            }
        });
    }

}
