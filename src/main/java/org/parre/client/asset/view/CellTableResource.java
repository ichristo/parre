/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.asset.view;

import com.google.gwt.user.cellview.client.CellTable;

/**
 * .
 * User: Swarn S. Dhaliwal
 * Date: 8/27/11
 * Time: 7:05 PM
 */
public interface CellTableResource extends CellTable.Resources {
    /**
     * The Interface CellTableStyle.
     */
    public interface CellTableStyle extends CellTable.Style {
    };
    /*
     * (non-Javadoc)
     *
     * @see
        com.google.gwt.user.cellview.client.CellTable.Resources#cellTableStyle()
     */
    @Source({"SourceViewTable.css"})
    CellTableStyle cellTableStyle();
};
