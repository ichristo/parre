/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.asset.view;

import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.user.client.ui.*;

import java.util.ArrayList;
import java.util.List;

import org.parre.client.ClientSingleton;
import org.parre.client.asset.presenter.EditAssetPresenter;
import org.parre.client.common.view.NoteRowView;
import org.parre.client.util.*;
import org.parre.shared.AssetDTO;
import org.parre.shared.NoteDTO;

import static org.parre.client.common.ClientNoteUtil.*;


/**
 * .
 * User: Swarn S. Dhaliwal
 * Date: 9/5/11
 * Time: 8:35 AM
 */
public class EditAssetView extends BaseModelView<AssetDTO> implements EditAssetPresenter.Display {
    private List<FieldValidationHandler> requiredFieldHandlers = new ArrayList<FieldValidationHandler>();
    private TextBox assetId;
    private TextBox assetName;
    private TextBox assetDescription;
    private ListBox humanPl;
    private ListBox financialPl;
    private ListBox economicPl;
    private ListBox critical;
    private Button saveButton;
    private Button cancelButton;
    private Button notesButton;
    private Widget viewWidget;
    private Label header;
    private boolean popupView;
    private VerticalPanel notesPanel;
    private List<NoteRowView> noteViews = new ArrayList<NoteRowView>();
    private Button addNoteButton;
    private NoteDialog noteDialog = new NoteDialog();
    private FlexTable notesTable = new FlexTable();

    public EditAssetView() {
        this(false);
    }
    public EditAssetView(boolean popupView) {
        this.popupView = popupView;
        viewWidget = buildView();
    }

    private VerticalPanel buildView() {
        header = new Label("Create Asset");
        header.setStyleName("inputPanelHeader");
        FlexTable inputTable = new FlexTable();
        int rowNum = -1;

        assetId = ViewUtil.addTextBoxFormEntry(inputTable, ++rowNum, "Asset ID");
        assetName = ViewUtil.addTextBoxFormEntry(inputTable, ++rowNum, "Asset Name", requiredFieldHandlers);
        assetDescription = ViewUtil.addTextBoxFormEntry(inputTable, ++rowNum, "Description");
        rowNum = addInputFields(inputTable, rowNum, requiredFieldHandlers);
        critical = ViewUtil.addListBoxFormEntry(inputTable, ++rowNum, "Is Critical?", requiredFieldHandlers);
        humanPl = ViewUtil.addListBoxFormEntry(inputTable, ++rowNum, "Human PL", requiredFieldHandlers);
        financialPl = ViewUtil.addListBoxFormEntry(inputTable, ++rowNum, "Financial PL", requiredFieldHandlers);
        economicPl = ViewUtil.addListBoxFormEntry(inputTable, ++rowNum, "Economic PL", requiredFieldHandlers);
        saveButton = ViewUtil.createButton("Save");
        cancelButton = ViewUtil.createButton("Cancel");
        notesButton = ViewUtil.createButton("View/Hide All Notes");
        cancelButton.setVisible(!popupView);

        VerticalPanel viewPanel = new VerticalPanel();
        viewPanel.setWidth("100%");
        viewPanel.setSpacing(5);
        viewPanel.setStyleName("inputPanel");

        viewPanel.add(header);
        viewPanel.add(ViewUtil.layoutCenter(inputTable));
        viewPanel.add(ViewUtil.layoutCenter(saveButton, cancelButton, notesButton));

        notesPanel = new VerticalPanel();
        addNoteButton = ViewUtil.createButton("Add Note", "Create new note.");
        ViewUtil.addFormEntry(notesTable, 0, "Notes", addNoteButton);
        notesPanel.add(ViewUtil.layoutCenter(notesTable));
        notesPanel.setVisible(false);
        notesPanel.setStyleName("inputPanel");

        viewPanel.add(ViewUtil.layoutCenter(notesPanel));
        return viewPanel;
    }

    public HasClickHandlers getSaveButton() {
        return saveButton;
    }

    public HasClickHandlers getCancelButton() {
        return cancelButton;
    }

    public Widget asWidget() {
        return viewWidget;
    }

    protected int addInputFields(FlexTable inputTable, int rowNum, List<FieldValidationHandler> fieldHandlers) {
        return rowNum;
    }
    public boolean validate() {
        return ViewUtil.validate(requiredFieldHandlers);
    }

    public void updateView() {
        AssetDTO model = getModel();
        ViewUtil.setText(assetId, model.getAssetId());
        ViewUtil.setText(assetName, model.getName());
        ViewUtil.setText(assetDescription, model.getDescription());
        ViewUtil.setSelectedValue(critical, model.getCritical());
        ViewUtil.setSelectedValue(humanPl, model.getHumanPl());
        ViewUtil.setSelectedValue(financialPl, model.getFinancialPl());
        ViewUtil.setSelectedValue(economicPl, model.getEconomicPl());
        notesPanel.clear();
        noteViews = new ArrayList<NoteRowView>();
        for(NoteDTO dto : model.getNoteDTOs()) {
            noteViews.add(new NoteRowView(dto));
        }
        notesPanel.add(ViewUtil.layoutCenter(notesTable));
        notesPanel.add(setNotesPanel(noteViews));

    }

    public void updateModel() {
        AssetDTO model = getModel();
        model.setAssetId(ViewUtil.getText(assetId));
        model.setName(ViewUtil.getText(assetName));
        model.setDescription(ViewUtil.getText(assetDescription));
        model.setCritical(ViewUtil.getSelectedBoolean(critical));
        model.setHumanPl(ViewUtil.getSelectedQuantity(humanPl));
        model.setFinancialPl(ViewUtil.getSelectedQuantity(financialPl));
        model.setEconomicPl(ViewUtil.getSelectedQuantity(economicPl));
        model.setNoteDTOs(getNoteDTOs(noteViews));
    }

    public ListBox getHumanPl() {
        return humanPl;
    }

    public ListBox getFinancialPl() {
        return financialPl;
    }

    public ListBox getEconomicPl() {
        return economicPl;
    }

    public ListBox getCritical() {
        return critical;
    }

    public HasText getHeader() {
        return header;
    }

    public ListBox getDamageFactor() {
        return new ListBox();
        //this method is overridden in EditPhysicalResourceView
    }

    @Override
    public ListBox getSystemTypeList() {
        return new ListBox();
      //this method is overridden in EditPhysicalResourceView
    }
    
    public HasClickHandlers getGISCoordinatesButton() {
        return new Button("");
        //this method is overridden in EditPhysicalResourceView
    }
    
    public HasText getZipCode() {
        return new TextBox();
        //this method is overridden in EditPhysicalResourceView
    }

    public void setGISCoordinates(String latitude, String longitude) {
        //this method is overridden in EditPhysicalResourceView
    }
    
    
	public void setSystemTypeList(List<String> assets) {
    	//this method is overridden in EditPhysicalResourceView
	}
	
	public void setSystemTypeText(String systemType) {
		//this method is overridden in EditPhysicalResourceView		
	}
	

    public HasClickHandlers getNotesButton() {
        return notesButton;
    }

    public void toggleNotesPanel() {
        notesPanel.setVisible(!notesPanel.isVisible());
    }

    @Override
    public TextBox getReplacementCost() {
        return new TextBox();
        //this method is overridden in EditPhysicalResourceView
    }

    @Override
    public void setReplacementCost(String replacementCost) {
        //this method is overridden in EditPhysicalResourceView
    }

    @Override
    public HasClickHandlers getAddNoteButton() {
        return addNoteButton;
    }

    @Override
    public void displayAddNotePopup() {
        noteDialog.show();
    }

    @Override
    public HasClickHandlers getSaveNoteButton() {
        return noteDialog.getSaveNoteButton();
    }

    @Override
    public void saveNote() {
        NoteRowView noteRowView = new NoteRowView();
        noteRowView.setUserName(ClientSingleton.getLoginInfo().getNickname());
        noteRowView.setDate();
        noteRowView.setMessage(noteDialog.getNoteTextArea().getText());
        noteRowView.setNew(true);
        noteViews.add(noteRowView);
        notesPanel.add(noteRowView.createView());
        noteDialog.getNoteTextArea().setText("");
        noteDialog.hide();
        updateModel();
    }


    public boolean isPopupView() {
        return popupView;
    }
	
	
}
