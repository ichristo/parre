/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.asset.view;

import com.google.gwt.user.cellview.client.Column;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import org.parre.client.util.DoubleClickableTextCell;
import org.parre.client.util.dataview.DataColumn;
import org.parre.client.util.dataview.DataGridView;
import org.parre.client.util.dataview.DataProvider;
import org.parre.shared.AssetDTO;

/**
 * User: Swarn S. Dhaliwal
 * Date: 7/10/11
 * Time: 5:38 PM
*/
public class AssetGrid extends DataGridView<AssetDTO> {

    public AssetGrid(DataProvider<AssetDTO> searchResultsProvider) {
        super(searchResultsProvider);
        List<DataColumn<AssetDTO>> columnList = new ArrayList<DataColumn<AssetDTO>>();
        columnList.add(createAssetIdColumn());
        columnList.add(createAssetNameColumn());
        columnList.add(createCriticalColumn());
        columnList.add(createHumanPlColumn());
        columnList.add(createFinancialPlColumn());
        columnList.add(createEconomicPlColumn());
        columnList.add(createTotalPlColumn());
        addColumns(columnList);
    }



    private DataColumn<AssetDTO> createTotalPlColumn() {
        Column<AssetDTO, String> column = new Column<AssetDTO, String>(
                createDoubleClickableTextCell()) {
            @Override
            public String getValue(AssetDTO object) {
                return String.valueOf(object.getTotalPl());
            }
        };
        column.setSortable(true);

        Comparator<AssetDTO> comparator = new Comparator<AssetDTO>() {
            public int compare(AssetDTO o1, AssetDTO o2) {
                return o1.getTotalPl().compareTo(o2.getTotalPl());
            }
        };
        return new DataColumn<AssetDTO>("Total PL", column, comparator, 10);
    }

    private DataColumn<AssetDTO> createEconomicPlColumn() {
        Column<AssetDTO, String> column = new Column<AssetDTO, String>(
                createDoubleClickableTextCell()) {
            @Override
            public String getValue(AssetDTO object) {
                return String.valueOf(object.getEconomicPl());
            }
        };
        column.setSortable(true);

        Comparator<AssetDTO> comparator = new Comparator<AssetDTO>() {
            public int compare(AssetDTO o1, AssetDTO o2) {
                return o1.getEconomicPl().compareTo(o2.getEconomicPl());
            }
        };
        return new DataColumn<AssetDTO>("Economic PL", column, comparator, 10);
    }

    private DataColumn<AssetDTO> createFinancialPlColumn() {
        Column<AssetDTO, String> column = new Column<AssetDTO, String>(
                createDoubleClickableTextCell()) {
            @Override
            public String getValue(AssetDTO object) {
                return String.valueOf(object.getFinancialPl());
            }
        };
        column.setSortable(true);

        Comparator<AssetDTO> comparator = new Comparator<AssetDTO>() {
            public int compare(AssetDTO o1, AssetDTO o2) {
                return o1.getFinancialPl().compareTo(o2.getFinancialPl());
            }
        };
        return new DataColumn<AssetDTO>("Financial PL", column, comparator, 10);
    }

    private DataColumn<AssetDTO> createHumanPlColumn() {
        Column<AssetDTO, String> column = new Column<AssetDTO, String>(
                createDoubleClickableTextCell()) {
            @Override
            public String getValue(AssetDTO object) {
                return String.valueOf(object.getHumanPl());
            }
        };
        column.setSortable(true);

        Comparator<AssetDTO> comparator = new Comparator<AssetDTO>() {
            public int compare(AssetDTO o1, AssetDTO o2) {
                return o1.getHumanPl().compareTo(o2.getHumanPl());
            }
        };
        return new DataColumn<AssetDTO>("Human PL", column, comparator, 10);

    }

    private DoubleClickableTextCell<AssetDTO> createDoubleClickableTextCell() {
        return new DoubleClickableTextCell<AssetDTO>(getSelectionModel(), getSelectionSource());
    }

    private DataColumn<AssetDTO> createAssetNameColumn() {
        Column<AssetDTO, String> column = new Column<AssetDTO, String>(
                createDoubleClickableTextCell()) {
            @Override
            public String getValue(AssetDTO object) {
                return object.getName();
            }
        };
        column.setSortable(true);

        Comparator<AssetDTO> comparator = new Comparator<AssetDTO>() {
            public int compare(AssetDTO o1, AssetDTO o2) {
                return o1.getName().compareTo(o2.getName());
            }
        };
        return new DataColumn<AssetDTO>("Asset Name", column, comparator, 30);
    }

    private DataColumn<AssetDTO> createCriticalColumn() {
        Column<AssetDTO, String> column = new Column<AssetDTO, String>(
                createDoubleClickableTextCell()) {
            @Override
            public String getValue(AssetDTO object) {
                return object.getCritical() ? "Yes" : "No" ;
            }
        };
        column.setSortable(true);

        Comparator<AssetDTO> comparator = new Comparator<AssetDTO>() {
            public int compare(AssetDTO o1, AssetDTO o2) {
                return o1.getCritical().compareTo(o2.getCritical());
            }
        };
        return new DataColumn<AssetDTO>("Is Critical ?", column, comparator, 20);

    }

    private DataColumn<AssetDTO> createAssetIdColumn() {
        Column<AssetDTO, String> column = new Column<AssetDTO, String>(
                createDoubleClickableTextCell()) {
            @Override
            public String getValue(AssetDTO object) {
                return object.getAssetId();
            }
        };
        column.setSortable(true);

        Comparator<AssetDTO> comparator = new Comparator<AssetDTO>() {
            public int compare(AssetDTO o1, AssetDTO o2) {
                return o1.getAssetId().compareTo(o2.getAssetId());
            }
        };
        return new DataColumn<AssetDTO>("Asset ID", column, comparator, 10);

    }

}
