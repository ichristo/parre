/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.asset.view;

import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.event.logical.shared.HasCloseHandlers;
import com.google.gwt.user.client.ui.*;
import com.google.gwt.view.client.SingleSelectionModel;

import java.util.List;

import org.parre.client.asset.presenter.ManageAssetPresenter;
import org.parre.client.common.view.AssetCriteriaView;
import org.parre.client.common.view.DisplayToggleView;
import org.parre.client.util.ActionSource;
import org.parre.client.util.BaseView;
import org.parre.client.util.ViewUtil;
import org.parre.client.util.dataview.DataProvider;
import org.parre.client.util.dataview.DataProviderFactory;
import org.parre.shared.AssetDTO;

/**
 * The Class ManageAssetView.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 7/16/11
 */
public class ManageAssetView extends BaseView implements ManageAssetPresenter.Display {
    private AssetGrid searchResultsView;
    private Button createAssetButton;
    private Button editAssetButton;
    private Button deleteAssetButton;
    private Button assetThreatMatrixButton;
    private Button manageThreatsButton;
    private AssetCriteriaView assetCriteriaView;
    private Image searchButton;
    private Image clearButton;
    private VerticalPanel searchPanel;
    private Widget viewWidget;
    private HorizontalPanel thresholdPanel;
    private ListBox plThresholdListBox;
    private DialogBox editPopup;
    private QuickEditAssetView editAssetView;
    private Label assetCountValue;
    private DialogBox selectAssetTypeDialog;
    private ListBox assetTypeListBox;
    private Button selectAssetTypeButton;
    private Button confirmDeleteButton;
    private DialogBox confirmDeleteDialog;
    private DisplayToggleView displayToggleView;
    private Button updatePlThresholdButton;

    public ManageAssetView(AssetCriteriaView assetCriteriaView, QuickEditAssetView editAssetView, DisplayToggleView toggleView) {
        this.assetCriteriaView = assetCriteriaView;
        this.editAssetView = editAssetView;
        this.displayToggleView = toggleView;
        viewWidget = createView();
    }

    private Widget createView() {
        searchPanel = new VerticalPanel();
        searchPanel.setStyleName("inputPanel");

        searchButton = ViewUtil.createImageButton("search-button.png", "Perform Search");
        clearButton = ViewUtil.createImageButton("clear-button.jpg", "Clear Search Results");
        
        searchPanel.add(assetCriteriaView.asWidget());

        searchPanel.add(ViewUtil.layoutCenter(searchButton, clearButton));

        DataProvider<AssetDTO> dataProvider = DataProviderFactory.createDataProvider();
        searchResultsView = new AssetGrid(dataProvider);

        plThresholdListBox = new ListBox();
        for (int i = 3; i <= 15; i++) {
            plThresholdListBox.addItem(String.valueOf(i));
        }
        plThresholdListBox.setWidth("50px");
        Label thresholdLabel = new Label("Pl Threshold : ");
        Label assetCountLabel = new Label("Asset Count : ");
        assetCountValue = new Label("");
        updatePlThresholdButton = ViewUtil.createButton("Save", "Make this value persistent");
        thresholdPanel = ViewUtil.horizontalPanel(5, thresholdLabel, plThresholdListBox,
                updatePlThresholdButton, assetCountLabel, assetCountValue);
        thresholdPanel.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_RIGHT);

        createAssetButton = ViewUtil.createButton("Create Asset");
        editAssetButton = ViewUtil.createButton("Edit Asset");
        deleteAssetButton = ViewUtil.createButton("Delete Asset");
        assetThreatMatrixButton = ViewUtil.createButton("Threat Asset Matrix");
        manageThreatsButton = ViewUtil.createButton("Manage Threats");

        FlexTable controlTable = new FlexTable();
        controlTable.setWidth("100%");
        controlTable.setWidget(0, 0, ViewUtil.horizontalPanel(5, createAssetButton, editAssetButton, deleteAssetButton, assetThreatMatrixButton, manageThreatsButton));
        controlTable.setWidget(0, 1, thresholdPanel);
        controlTable.getFlexCellFormatter().setHorizontalAlignment(0, 1, HasHorizontalAlignment.ALIGN_RIGHT);

        //assetButtonPanel.setWidth("75%");

        displayToggleView.setText("Search Criteria");
        displayToggleView.setDisplayToggleCommand(new DisplayToggleView.DisplayToggleCommand() {
            public void toggleDisplay(boolean show) {
                setSearchVisible(show);
            }
        });
        displayToggleView.setDisplayVisible(false);
        VerticalPanel viewPanel = new VerticalPanel();

        viewPanel.setWidth("100%");
        viewPanel.setHeight("100%");
        viewPanel.add(displayToggleView.asWidget());
        viewPanel.add(searchPanel);
        viewPanel.add(controlTable);
        viewPanel.add(searchResultsView.asWidget());
        viewPanel.setSpacing(5);
        //searchResultsView.asWidget().setVisible(false);
        thresholdPanel.setVisible(false);
        confirmDeleteDialog = createConfirmDeleteDialog();
        selectAssetTypeDialog = createSelectAssetTypePopup();
        editPopup = createEditAssetPopup();
        searchResultsView.refresh();
        return viewPanel;
    }

    private DialogBox createConfirmDeleteDialog() {
        VerticalPanel panel = new VerticalPanel();
        panel.setStyleName("inputPanel");
        panel.setSpacing(5);
        confirmDeleteButton = ViewUtil.createButton("OK");
        HTML message = new HTML("Confirm Delete Asset?");
        panel.add(message);
        panel.add(confirmDeleteButton);
        panel.setWidth("200px");
        return ViewUtil.createDialog("Confirm", panel);
    }

    private DialogBox createSelectAssetTypePopup() {
        VerticalPanel panel = new VerticalPanel();
        panel.setStyleName("inputPanel");
        panel.setSpacing(5);
        selectAssetTypeButton = ViewUtil.createButton("Select");
        assetTypeListBox = new ListBox();
        assetTypeListBox.addChangeHandler(new ChangeHandler() {
            public void onChange(ChangeEvent event) {
                selectAssetTypeButton.setEnabled(assetTypeListBox.getSelectedIndex() > 0);
            }
        });
        panel.add(assetTypeListBox);
        panel.add(selectAssetTypeButton);
        panel.setWidth("100px");
        return ViewUtil.createDialog("Select Asset Type", panel);
    }

    private DialogBox createEditAssetPopup() {
        Widget widget = editAssetView.asWidget();
        widget.setWidth("400px");
        DialogBox dialogBox = ViewUtil.createDialog("Edit Asset", widget);
        dialogBox.setWidth("400px");
        return dialogBox;
    }

    public HasClickHandlers getUpdatePlThresholdButton() {
        return updatePlThresholdButton;
    }

    @Override
    public int getAssetCount() {
        return Integer.valueOf(assetCountValue.getText());
    }

    public ListBox getPlThresholdListBox() {
        return plThresholdListBox;
    }
    public HasClickHandlers getAssetThreatMatrixButton() {
        return assetThreatMatrixButton;
    }

    public HasClickHandlers getCreateAssetButton() {
        return createAssetButton;
    }

    public HasClickHandlers getManageThreatsButton() {
        return manageThreatsButton;
    }

    public SingleSelectionModel<AssetDTO> getSearchResultsSelectionModel() {
        return searchResultsView.getSelectionModel();
    }

    public ActionSource<AssetDTO> getAssetSelectionSource() {
        return searchResultsView.getSelectionSource();
    }

    public HasClickHandlers getSearchButton() {
        return searchButton;
    }

    public HasClickHandlers getClearButton() {
        return clearButton;
    }

    public void setSearchResults(List<AssetDTO> results) {
        assetCountValue.setText(String.valueOf(results.size()));
        searchResultsView.asWidget().setVisible(true);
        thresholdPanel.setVisible(true);
        searchResultsView.setData(results);
        searchResultsView.getSelectionModel().setSelected(null, true);
    }

    public void refreshSearchResultsView() {
        searchResultsView.refresh();
    }

    public Widget asWidget() {
        return viewWidget;
    }

    public HasCloseHandlers<PopupPanel> getEditPopup() {
        return editPopup;
    }

    public void showEditPopup() {
        //editAssetPopup.showRelativeTo(searchResultsView.asWidget());
        editPopup.center();
        editPopup.show();
        setSearchResultsViewDisabled(true);
    }

    public void setSearchResultsViewDisabled(boolean value) {
        if (value) {
            searchResultsView.asWidget().getElement().setAttribute("disabled", "true");
        }
        else {
            searchResultsView.asWidget().getElement().removeAttribute("disabled");
        }
    }

    public void setAssetThreshold(Integer value) {
        ViewUtil.setSelectedValue(plThresholdListBox, String.valueOf(value));
    }

    public void hideEditAssetPopup() {
        editPopup.hide();
    }

    public List<AssetDTO> getSearchResults() {
        return searchResultsView.getData();
    }

    public ListBox getAssetTypeListBox() {
        return assetTypeListBox;
    }

    public HasClickHandlers getSelectAssetTypeButton() {
        return selectAssetTypeButton;
    }

    public void showAssetTypePopup() {
        selectAssetTypeDialog.center();
        selectAssetTypeDialog.show();
    }

    public void hideSelectAssetTypePopup() {
        selectAssetTypeDialog.hide();
    }

    public void setSelectAssetTypeEnabled(boolean value) {
        selectAssetTypeButton.setEnabled(value);
    }


    public void setSearchVisible(boolean value) {
        searchPanel.setVisible(value);
        searchResultsView.setHeight(value ? "500px" : "650px");
    }
    public HasClickHandlers getEditAssetButton() {
        return editAssetButton;
    }

    public HasClickHandlers getDeleteAssetButton() {
        return deleteAssetButton;
    }

    public void setEditAssetEnabled(boolean value) {
        editAssetButton.setEnabled(value);
    }

    public void setDeleteAssetEnabled(boolean value) {
        deleteAssetButton.setEnabled(value);
    }

    public void showConfirmDeletePopup() {
        confirmDeleteDialog.center();
    }

    public void hideConfirmDeleteDialog() {
       confirmDeleteDialog.hide();
    }

    public Button getConfirmDeleteButton() {
        return confirmDeleteButton;
    }

    @Override
    public void refreshView() {
        searchResultsView.refresh();
        searchResultsView.forceLayout();
    }
}
