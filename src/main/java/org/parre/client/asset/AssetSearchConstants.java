/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.asset;

import com.google.gwt.core.client.GWT;
import com.google.gwt.i18n.client.Constants;

/**
 * User: Swarn S. Dhaliwal
 * Date: 7/9/11
 * Time: 7:31 PM
*/
public interface AssetSearchConstants extends Constants {
    public static final AssetSearchConstants SINGLETON = GWT.create(AssetSearchConstants.class);
    String serialNumberHeader();

    String modelNumberHeader();

    String customerNameHeader();

    String customerAddressHeader();

    String installDataHeader();

    String warrantyEndDateHeader();

    String customerName();

    String contactName();

    String streetAddress();

    String city();

    String state();

    String postalCode();

    String search();

    String clearSearchFields();

    String customerSearch();

    String equipmentSearch();
}
