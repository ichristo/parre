/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.asset.presenter;

import java.util.List;

import org.parre.client.ClientSingleton;
import org.parre.client.ViewFactory;
import org.parre.client.asset.event.AddAssetCancelledEvent;
import org.parre.client.asset.event.CreateAssetEvent;
import org.parre.client.asset.event.EditAssetEvent;
import org.parre.client.asset.event.ManageAssetNavEvent;
import org.parre.client.asset.view.EditAssetView;
import org.parre.client.asset.view.EditPhysicalResourceView;
import org.parre.client.asset.view.ManageAssetView;
import org.parre.client.messaging.AssetServiceAsync;
import org.parre.client.messaging.ServiceFactory;
import org.parre.client.nav.event.AssetNavEvent;
import org.parre.client.util.BaseAsyncCommand;
import org.parre.client.util.ExecutableAsyncCallback;
import org.parre.client.util.MessageDialog;
import org.parre.client.util.Presenter;
import org.parre.client.util.View;
import org.parre.client.util.ViewContainer;
import org.parre.shared.AssetDTO;
import org.parre.shared.types.AssetType;

import com.google.gwt.event.shared.EventBus;
import com.google.gwt.user.client.rpc.AsyncCallback;

/**
 * The Class AssetPresenter.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 7/19/11
 */
public class AssetPresenter implements Presenter {
    private Integer addAssetViewIndex = null;
    private Integer addPhysicalResourceViewIndex = null;
    private Integer manageAssetViewIndex = null;
    private AssetServiceAsync service;
    private EventBus eventBus;
    private Display display;

    /**
     * The Interface Display.
     */
    public interface Display extends View, ViewContainer {

    }

    public AssetPresenter(Display display) {
        this.service = ServiceFactory.getInstance().getAssetService();
        this.eventBus = ClientSingleton.getEventBus();
        this.display = display;
    }

    public void go() {
        initViewLoaders();
        bind();
        display.showView(manageAssetViewIndex);
    }

    private void initViewLoaders() {
        EditAssetView addAssetView = ViewFactory.createAddAssetView(false);
        addAssetView.getPresenter().go();
        addAssetViewIndex = display.addView(addAssetView.asWidget());
        EditPhysicalResourceView physicalResourceView = ViewFactory.createPhysicalResourceView();
        physicalResourceView.getPresenter().go();
        addPhysicalResourceViewIndex = display.addView(physicalResourceView.asWidget());

        ManageAssetView manageAssetView = ViewFactory.createManageAssetView(ClientSingleton.getAppInitData());
        manageAssetView.getPresenter().go();
        manageAssetViewIndex = display.addAndShowView(manageAssetView.asWidget());
    }

    private void bind() {
        eventBus.addHandler(AssetNavEvent.TYPE, new AssetNavEvent.Handler() {
            public void onEvent(AssetNavEvent event) {
                display.showView(manageAssetViewIndex);
                eventBus.fireEvent(ManageAssetNavEvent.create());
            }
        });
        eventBus.addHandler(ManageAssetNavEvent.TYPE, new ManageAssetNavEvent.Handler() {
            public void onEvent(ManageAssetNavEvent event) {
                display.showView(manageAssetViewIndex);
            }
        });

        eventBus.addHandler(CreateAssetEvent.TYPE, new CreateAssetEvent.Handler() {
            public void onEvent(CreateAssetEvent event) {
            	//TODO: Get updated System Type
                showEditAssetView(event.getAssetType());
            }
        });
        

        eventBus.addHandler(EditAssetEvent.TYPE, new EditAssetEvent.Handler() {
            public void onEvent(EditAssetEvent event) {
                showEditAssetView(event.getAssetDTO().getAssetType());
            }
        });

        eventBus.addHandler(AddAssetCancelledEvent.TYPE, new AddAssetCancelledEvent.Handler() {
            public void onEvent(AddAssetCancelledEvent event) {
                display.showView(manageAssetViewIndex);
            }
        });
    }

    private void showEditAssetView(AssetType assetType) {
        if (AssetType.PHYSICAL_ASSET == assetType) {
            display.showView(addPhysicalResourceViewIndex);
        } else {
            display.showView(addAssetViewIndex);
        }
    }

}
