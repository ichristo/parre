/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.asset.presenter;

import java.util.List;

import org.parre.client.ClientSingleton;
import org.parre.client.asset.event.AssetUpdatedEvent;
import org.parre.client.asset.event.EditAssetEvent;
import org.parre.client.asset.event.QuickEditAssetEvent;
import org.parre.client.messaging.AssetServiceAsync;
import org.parre.client.messaging.ServiceFactory;
import org.parre.client.util.*;
import org.parre.shared.AssetDTO;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.web.bindery.event.shared.EventBus;

/**
 * .
 * User: Swarn S. Dhaliwal
 * Date: 9/5/11
 * Time: 8:07 AM
 */
public class QuickEditAssetPresenter implements Presenter {
    private EventBus eventBus;
    private Display display;
    private AssetServiceAsync service;
    private BusyHandler busyHandler;

    /**
     * The Interface Display.
     */
    public interface Display extends ModelView<AssetDTO> {

        HasClickHandlers getSaveButton();
    }

    public QuickEditAssetPresenter(Display display) {
        this.service = ServiceFactory.getInstance().getAssetService();
        this.eventBus = ClientSingleton.getEventBus();
        this.display = display;
        this.busyHandler = new BusyHandler(display);
    }

    public void go() {
        bind();
    }

    private void bind() {
        display.getSaveButton().addClickHandler(new ValidatingClickHandler(display) {
            public void handleClick(ClickEvent event) {

                new ExecutableAsyncCallback<AssetDTO>(busyHandler, new BaseAsyncCommand<AssetDTO>() {
                    public void execute(AsyncCallback<AssetDTO> callback) {
                        ServiceFactory.getInstance().getAssetService().saveAsset(display.updateAndGetModel(), ClientSingleton.getParreAnalysisId(), callback);
                    }
                    @Override
                    public void handleResult(AssetDTO results) {
                        eventBus.fireEvent(AssetUpdatedEvent.create(results));
                    }
                }).makeCall();
            }
        });
        eventBus.addHandler(QuickEditAssetEvent.TYPE, new QuickEditAssetEvent.Handler() {
            public void onEvent(QuickEditAssetEvent event) {
                AssetDTO editDTO = event.getAssetDTO().cloneAsset();
                display.setModelAndUpdateView(editDTO);
            }
        });
        
    }
    
    protected Long getParreAnalysisId() {
        return ClientSingleton.getBaselineId();
    }
}
