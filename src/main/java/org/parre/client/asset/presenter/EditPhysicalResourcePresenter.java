/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.asset.presenter;

import java.util.List;

import org.parre.client.asset.event.CreateAssetEvent;
import org.parre.client.asset.event.EditAssetEvent;
import org.parre.client.util.BaseAsyncCommand;
import org.parre.client.util.ExecutableAsyncCallback;
import org.parre.client.util.MessageDialog;
import org.parre.shared.types.AssetType;
import org.parre.shared.types.PhysicalAssetType;

import com.google.gwt.user.client.rpc.AsyncCallback;

/**
 * The Class EditPhysicalResourcePresenter.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 11/1/11
 */
public class EditPhysicalResourcePresenter extends EditAssetPresenter {
	
	public void go() {
		super.go();
		bind();
	}
	
	private void bind() {
		display.getDamageFactor().addItem("Select", "0");
		
		for (PhysicalAssetType physicalAssetType : PhysicalAssetType.values()) {
			display.getDamageFactor().addItem(physicalAssetType.getDisplayName(), physicalAssetType.getDisplayName());
		}
			
		eventBus.addHandler(EditAssetEvent.TYPE, new EditAssetEvent.Handler() {
            public void onEvent(EditAssetEvent event) {
            	ExecutableAsyncCallback<List <String>> exec = new ExecutableAsyncCallback<List <String>>(busyHandler, new BaseAsyncCommand<List <String>>() {
                    public void execute(AsyncCallback<List <String>> asyncCallback) {
                        service.getSystemTypes(getParreAnalysisId(), asyncCallback);
                    }
                    @Override
                    public void handleResult(List <String> results) {
                    	display.setSystemTypeList(results);
                    }

                    @Override
                    public boolean handleError(Throwable t) {
                        MessageDialog.popup(t.getMessage(), true);
                        return true;
                    }
                });
                exec.makeCall();
            }
        });
		
		eventBus.addHandler(CreateAssetEvent.TYPE, new CreateAssetEvent.Handler() {
            public void onEvent(CreateAssetEvent event) {
            	ExecutableAsyncCallback<List <String>> exec = new ExecutableAsyncCallback<List <String>>(busyHandler, new BaseAsyncCommand<List <String>>() {
                    public void execute(AsyncCallback<List <String>> asyncCallback) {
                        service.getSystemTypes(getParreAnalysisId(), asyncCallback);
                    }
                    @Override
                    public void handleResult(List <String> results) {
                    	display.setSystemTypeList(results);
                    }

                    @Override
                    public boolean handleError(Throwable t) {
                        MessageDialog.popup(t.getMessage(), true);
                        return true;
                    }
                });
                exec.makeCall();
            }
        });
		
	}
	
    public EditPhysicalResourcePresenter(Display display) {
        super(display);
    }

    protected boolean handlesAssetType(AssetType assetType) {
        return AssetType.PHYSICAL_ASSET == assetType;
    }
}
