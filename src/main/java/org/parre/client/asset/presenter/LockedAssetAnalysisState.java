/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.asset.presenter;

import org.parre.client.ParreClientUtil;
import org.parre.shared.AssetDTO;

/**
 * The Class LockedAssetAnalysisState.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 2/28/12
 */
public class LockedAssetAnalysisState implements AssetAnalysisState {
    private ManageAssetPresenter.Display display;

    public LockedAssetAnalysisState(ManageAssetPresenter.Display display) {
        this.display = display;
    }

    public void deleteAsset() {
        ParreClientUtil.showCurrentAnalysisLockedMessage();
    }

    public void editAsset(AssetDTO dto) {
        ParreClientUtil.showCurrentAnalysisLockedMessage();
    }

    public void createAsset() {
        ParreClientUtil.showCurrentAnalysisLockedMessage();
    }

    public void quickEditAsset(AssetDTO dto) {
        ParreClientUtil.showCurrentAnalysisLockedMessage();
    }

    public void assetSelected(AssetDTO dto) {
        display.setEditAssetEnabled(false);
        display.setDeleteAssetEnabled(false);
    }

    public void persistAssetThreshold(int threshold) {
        ParreClientUtil.showCurrentAnalysisLockedMessage();
    }
}
