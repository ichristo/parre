/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.asset.presenter;

import com.google.gwt.event.dom.client.*;
import com.google.gwt.event.logical.shared.CloseEvent;
import com.google.gwt.event.logical.shared.CloseHandler;
import com.google.gwt.event.logical.shared.HasCloseHandlers;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.event.shared.GwtEvent;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.view.client.SelectionChangeEvent;
import com.google.gwt.view.client.SingleSelectionModel;

import java.util.*;

import org.parre.client.ClientSingleton;
import org.parre.client.asset.event.*;
import org.parre.client.common.event.AssetThreatSearchType;
import org.parre.client.common.presenter.AnalysisStateHost;
import org.parre.client.common.presenter.AnalysisStateInitializer;
import org.parre.client.event.*;
import org.parre.client.messaging.AssetServiceAsync;
import org.parre.client.messaging.ServiceFactory;
import org.parre.client.nav.event.AssetNavEvent;
import org.parre.client.nav.event.ThreatAssetMatrixNavEvent;
import org.parre.client.nav.event.ThreatNavEvent;
import org.parre.client.util.*;
import org.parre.client.util.nav.NavManager;
import org.parre.server.domain.AssetNote;
import org.parre.shared.AssetDTO;
import org.parre.shared.AssetSearchDTO;
import org.parre.shared.NoteDTO;
import org.parre.shared.types.AssetType;


/**
 * User: Swarn S. Dhaliwal
 * Date: 8/21/11
 * Time: 5:03 PM
*/
public class ManageAssetPresenter implements Presenter, AnalysisStateHost<AssetAnalysisState> {
    private AssetDTO selectedAssetDTO;
    public static final int ASSET_SEARCH = 0;
    public static final int THREAT_SEARCH = 1;

    public static final Map<Integer, GwtEvent> viewEventMap = new HashMap<Integer, GwtEvent>(2);

    static {
        viewEventMap.put(ASSET_SEARCH, AssetSearchEvent.create(AssetThreatSearchType.ASSET));
        viewEventMap.put(THREAT_SEARCH, ThreatAssetSearchEvent.create());
    }

    private List<AssetDTO> assetList;
    private AssetServiceAsync service;
    private Integer assetThreshold;
    private BusyHandler busyHandler;
    private AssetAnalysisState handler;
    private AnalysisStateInitializer<AssetAnalysisState> handlerInitializer;

    public void setAssetThreshold(Integer assetThreshold) {
        this.assetThreshold = assetThreshold;
    }

    public void setHandler(AssetAnalysisState handler) {
        ClientLoggingUtil.info("Handler Set to : " + handler);
        this.handler = handler;
    }

    public View getView() {
        return display;
    }

    /**
     * The Interface Display.
     */
    public static interface Display extends View {
        HasClickHandlers getAssetThreatMatrixButton();

        SingleSelectionModel<AssetDTO> getSearchResultsSelectionModel();

        ActionSource<AssetDTO> getAssetSelectionSource();

        HasClickHandlers getSearchButton();

        HasClickHandlers getClearButton();

        void setSearchResults(List<AssetDTO> dtos);

        void refreshSearchResultsView();

        HasClickHandlers getCreateAssetButton();

        ListBox getPlThresholdListBox();

        HasCloseHandlers<PopupPanel> getEditPopup();

        void showEditPopup();

        void hideEditAssetPopup();

        List<AssetDTO> getSearchResults();

        void setSearchResultsViewDisabled(boolean value);

        void setAssetThreshold(Integer value);

        HasClickHandlers getManageThreatsButton();

        ListBox getAssetTypeListBox();

        HasClickHandlers getSelectAssetTypeButton();

        void showAssetTypePopup();

        void hideSelectAssetTypePopup();

        void setSelectAssetTypeEnabled(boolean value);

        HasClickHandlers getEditAssetButton();

        HasClickHandlers getDeleteAssetButton();

        void setEditAssetEnabled(boolean value);

        void setDeleteAssetEnabled(boolean value);

        void showConfirmDeletePopup();

        void hideConfirmDeleteDialog();

        Button getConfirmDeleteButton();

        void setSearchVisible(boolean value);

        HasClickHandlers getUpdatePlThresholdButton();

        int getAssetCount();
    }

    private int currentSearchView = ASSET_SEARCH;
    private EventBus eventBus;
    private Display display;

    public ManageAssetPresenter(Display display) {
        this.service = ServiceFactory.getInstance().getAssetService();
        this.eventBus = ClientSingleton.getEventBus();
        this.display = display;
        this.busyHandler = new BusyHandler(display);
        handlerInitializer = new AnalysisStateInitializer<AssetAnalysisState>(this,
                AssetAnalysisStateFactory.getInstance());
    }

    public void go() {
        handlerInitializer.go();
        bind();
        ViewUtil.setSelectedValue(display.getPlThresholdListBox(), String.valueOf(assetThreshold));
        display.setDeleteAssetEnabled(false);
        display.setEditAssetEnabled(false);
        getAllAssets();
    }


    private GwtEvent getSearchEvent() {
        return viewEventMap.get(currentSearchView);
    }

    private void bind() {
        populateAssetTypeListBox(display.getAssetTypeListBox());
        bindEvents();
        bindActions();
    }

    private void bindActions() {
        display.getClearButton().addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                eventBus.fireEvent(ClearAssetSearchEvent.create());
                display.setSearchResults(Collections.<AssetDTO>emptyList());
            }
        });
        display.getSearchButton().addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                eventBus.fireEvent(getSearchEvent());
            }
        });
        display.getEditPopup().addCloseHandler(new CloseHandler<PopupPanel>() {
            public void onClose(CloseEvent<PopupPanel> popupPanelCloseEvent) {
                display.setSearchResultsViewDisabled(false);
            }
        });
        display.getAssetThreatMatrixButton().addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                if(display.getAssetCount() > 0) {
                    NavManager.performNav(ThreatAssetMatrixNavEvent.create());
                }
                else {
                    MessageDialog.popup("Need at least one asset to create asset-threat pairs.");
                }
            }
        });
        display.getAssetSelectionSource().setCommand(new ActionSource.ActionCommand<AssetDTO>() {
            public void execute(AssetDTO dto) {
                handler.editAsset(dto);
                //handler.quickEditAsset(dto);
            }
        });

        display.getCreateAssetButton().addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                handler.createAsset();
            }
        });
        display.getEditAssetButton().addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                handler.editAsset(selectedAssetDTO);
            }
        });

        display.getDeleteAssetButton().addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                handler.deleteAsset();
            }
        });
        display.getConfirmDeleteButton().addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                display.hideConfirmDeleteDialog();

                new ExecutableAsyncCallback<AssetDTO>(busyHandler, new BaseAsyncCommand<AssetDTO>() {
                    public void execute(AsyncCallback<AssetDTO> callback) {
                        ServiceFactory.getInstance().getAssetService().deleteAsset(selectedAssetDTO, ClientSingleton.getBaselineId(), callback);
                    }
                    @Override
                    public void handleResult(AssetDTO results) {
                        display.getSearchResults().remove(selectedAssetDTO);
                        assetList.remove(selectedAssetDTO);
                        selectedAssetDTO = null;
                        MessageDialog.popup("Asset " + results.getName() + " was deleted.");
                    }
                }).makeCall();
            }
        });
        display.getSelectAssetTypeButton().addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                display.hideSelectAssetTypePopup();
                String selectedValue = ViewUtil.getSelectedValue(display.getAssetTypeListBox());
                AssetType assetType = AssetType.valueOf(selectedValue);
                //TODO: Make Sure to get the Latest Values for System Type Dropdown.
                eventBus.fireEvent(CreateAssetEvent.create(assetType));
            }
        });

        display.getManageThreatsButton().addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                if(display.getAssetCount() > 0) {
                    NavManager.performNav(ThreatNavEvent.create());
                }
                else {
                    MessageDialog.popup("Need at least one asset to manage threats.");
                }
            }
        });

        display.getSearchResultsSelectionModel().addSelectionChangeHandler(new SelectionChangeEvent.Handler() {
            public void onSelectionChange(SelectionChangeEvent event) {
                SingleSelectionModel<AssetDTO> selectionModel = (SingleSelectionModel<AssetDTO>) event.getSource();
                selectedAssetDTO = selectionModel.getSelectedObject();
                handler.assetSelected(selectedAssetDTO);
            }
        });
        display.getPlThresholdListBox().addChangeHandler(new ChangeHandler() {
            public void onChange(ChangeEvent changeEvent) {
                setThresholdFilteredResults(updateAssetThreshold());
            }
        });
        display.getUpdatePlThresholdButton().addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                handler.persistAssetThreshold(assetThreshold);
            }
        });
    }

    private void bindEvents() {
        eventBus.addHandler(ManageAssetNavEvent.TYPE, new ManageAssetNavEvent.Handler() {
            public void onEvent(ManageAssetNavEvent event) {
                getAllAssets();
            }
        });
        eventBus.addHandler(AssetUpdatedEvent.TYPE, new AssetUpdatedEvent.Handler() {
            public void onEvent(AssetUpdatedEvent event) {
                display.hideEditAssetPopup();
                AssetDTO updated = event.getAssetDTO();
                List<AssetDTO> currentResults = display.getSearchResults();
                int index = currentResults.indexOf(updated);
                currentResults.set(index, updated);
                display.refreshSearchResultsView();
                index = assetList.indexOf(updated);
                assetList.set(index, updated);
            }
        });
        eventBus.addHandler(GoHomeEvent.TYPE, new GoHomeEvent.Handler() {
            public void onEvent(GoHomeEvent event) {
                display.setSearchResults(Collections.<AssetDTO>emptyList());
            }
        });
        eventBus.addHandler(AssetSavedEvent.TYPE, new AssetSavedEvent.Handler() {
            public void onEvent(AssetSavedEvent event) {
                AssetDTO assetDTO = event.getAssetDTO();
                int index = display.getSearchResults().indexOf(assetDTO);
                if (index < 0) {
                    display.getSearchResults().add(assetDTO);
                } else {
                    display.getSearchResults().set(index, assetDTO);
                }
                eventBus.fireEvent(AssetNavEvent.create());
            }
        });
        eventBus.addHandler(AssetSearchResultsEvent.TYPE, new AssetSearchResultsEvent.Handler() {
            public void onEvent(AssetSearchResultsEvent event) {
                assetList = event.getResults();
                setThresholdFilteredResults(assetThreshold);
                display.getSearchResultsSelectionModel().setSelected(null, true);
                display.refreshView();
            }
        });
        eventBus.addHandler(LogoutEvent.TYPE, new LogoutEvent.Handler() {
            public void onEvent(LogoutEvent event) {
            }
        });
    }

    private int updateAssetThreshold() {
        ListBox listBox = display.getPlThresholdListBox();
        int threshold = Integer.parseInt(listBox.getValue(listBox.getSelectedIndex()));
        assetThreshold = threshold;
        return threshold;
    }


    private void populateAssetTypeListBox(ListBox assetTypeListBox) {
        assetTypeListBox.addItem("Select", "");
        assetTypeListBox.addItem("Human Asset", AssetType.HUMAN_ASSET.name());
        assetTypeListBox.addItem("External Asset", AssetType.EXTERNAL_ASSET.name());
        assetTypeListBox.addItem("Physical Asset", AssetType.PHYSICAL_ASSET.name());
    }


    private int setThresholdFilteredResults(int threshold) {
        List<AssetDTO> filteredList = new ArrayList<AssetDTO>(assetList.size());
        for (AssetDTO assetDTO : assetList) {
            if (assetDTO.getTotalPl() >= threshold) {
                filteredList.add(assetDTO);
            }
        }
        display.setSearchResults(filteredList);
        return threshold;
    }


    private void getAllAssets() {
        new ManageAssetSearchCommand().execute(new AssetSearchDTO(ClientSingleton.getBaselineId()), display);
    }

}
