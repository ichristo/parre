/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.asset.presenter;

import java.math.BigDecimal;
import java.util.List;

import org.parre.client.ClientSingleton;
import org.parre.client.asset.event.AddAssetCancelledEvent;
import org.parre.client.asset.event.AssetSavedEvent;
import org.parre.client.asset.event.CreateAssetEvent;
import org.parre.client.asset.event.EditAssetEvent;
import org.parre.client.messaging.AssetServiceAsync;
import org.parre.client.messaging.ServiceFactory;
import org.parre.client.util.BaseAsyncCommand;
import org.parre.client.util.BusyHandler;
import org.parre.client.util.ExecutableAsyncCallback;
import org.parre.client.util.MessageDialog;
import org.parre.client.util.ModelView;
import org.parre.client.util.Presenter;
import org.parre.client.util.ViewUtil;
import org.parre.shared.AssetDTO;
import org.parre.shared.ZipLookupDataDTO;
import org.parre.shared.types.AssetType;

import com.google.gwt.event.dom.client.BlurEvent;
import com.google.gwt.event.dom.client.BlurHandler;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.HasText;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextBox;
import com.google.web.bindery.event.shared.EventBus;

/**
 * .
 * User: Swarn S. Dhaliwal
 * Date: 9/5/11
 * Time: 9:06 AM
 */
public class EditAssetPresenter implements Presenter {
    protected EventBus eventBus;
    protected Display display;
    protected AssetServiceAsync service;
    protected BusyHandler busyHandler;

    /**
     * The Interface Display.
     */
    public interface Display extends ModelView<AssetDTO> {
        HasClickHandlers getSaveButton();

        HasClickHandlers getCancelButton();

        ListBox getHumanPl();

        ListBox getFinancialPl();

        ListBox getEconomicPl();

        ListBox getCritical();

        HasText getHeader();

        ListBox getDamageFactor();
        
        ListBox getSystemTypeList();
        
        void setSystemTypeList(List<String> assets);
        
        void setSystemTypeText(String systemType);
        
        HasClickHandlers getGISCoordinatesButton();

        HasText getZipCode();

        void setGISCoordinates(String Latitude, String Longitude);

        HasClickHandlers getNotesButton();

        void toggleNotesPanel();

        TextBox getReplacementCost();

        void setReplacementCost(String replacementCost);

        HasClickHandlers getAddNoteButton();

        void displayAddNotePopup();

        HasClickHandlers getSaveNoteButton();

        void saveNote();
    }

    public EditAssetPresenter(Display display) {
        this.service = ServiceFactory.getInstance().getAssetService();
        this.eventBus = ClientSingleton.getEventBus();
        this.display = display;
    }

    public void go() {
        bind();
    }
    

    private void bind() {
        busyHandler = new BusyHandler(display);
        populatePlPickList(display.getHumanPl());
        populatePlPickList(display.getFinancialPl());
        populatePlPickList(display.getEconomicPl());
        display.getCritical().addItem("Select", "");
        display.getCritical().addItem("Yes", "true");
        display.getCritical().addItem("No", "false");

        eventBus.addHandler(CreateAssetEvent.TYPE, new CreateAssetEvent.Handler() {
            public void onEvent(CreateAssetEvent event) {
                AssetType assetType = event.getAssetType();
                if (!handlesAssetType(assetType)) {
                    return;
                }
                display.getHeader().setText("Create " + assetType.value());
                display.setModelAndUpdateView(assetType.createAssetDTO());
            }
        });
        eventBus.addHandler(EditAssetEvent.TYPE, new EditAssetEvent.Handler() {
            public void onEvent(EditAssetEvent event) {
                AssetDTO assetDTO = event.getAssetDTO();
                AssetType assetType = assetDTO.getAssetType();
                if (!handlesAssetType(assetType)) {
                    return;
                }
                display.getHeader().setText("Edit " + assetType.value());
                display.setModelAndUpdateView(assetDTO);
            }
        });
        
        display.getGISCoordinatesButton().addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent clickEvent) {
                ExecutableAsyncCallback<ZipLookupDataDTO> exec  = new ExecutableAsyncCallback<ZipLookupDataDTO>(busyHandler, new BaseAsyncCommand<ZipLookupDataDTO>() {
                    public void execute(AsyncCallback<ZipLookupDataDTO> callback) {
                        ServiceFactory.getInstance().getAssetService().getZipLookupData(display.getZipCode().getText(), callback);
                    }
                    @Override
                    public void handleResult(ZipLookupDataDTO dto) {
                        try {
                            display.setGISCoordinates(dto.getLatitude().stripTrailingZeros().toString(), dto.getLongitude().stripTrailingZeros().toString());
                        } catch(Exception e) {
                            MessageDialog.popup("ZIP Code not found.");
                            display.setGISCoordinates("", "");
                        }

                    }
                });
                exec.makeCall();
            }
        });
        display.getSaveButton().addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                if (!(display.validate())) {
                    return;
                }
                ExecutableAsyncCallback<AssetDTO> exec = new ExecutableAsyncCallback<AssetDTO>(busyHandler, new BaseAsyncCommand<AssetDTO>() {
                    public void execute(AsyncCallback<AssetDTO> asyncCallback) {
                        service.saveAsset(display.updateAndGetModel(), getParreAnalysisId(), asyncCallback);
                    }

                    @Override
                    public void handleResult(AssetDTO result) {
                        eventBus.fireEvent(AssetSavedEvent.create(result));
                    }

                    @Override
                    public boolean handleError(Throwable t) {
                        MessageDialog.popup(t.getMessage(), true);
                        return true;
                    }
                });
                exec.makeCall();
            }
        });
        
        display.getSystemTypeList().addChangeHandler(new ChangeHandler() {
			@Override
			public void onChange(ChangeEvent event) {
				int systemTypeSelected = display.getSystemTypeList().getSelectedIndex();
				String systemType = display.getSystemTypeList().getItemText(systemTypeSelected);
				display.setSystemTypeText(systemType);
			}
		});
        
        display.getCancelButton().addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                eventBus.fireEvent(AddAssetCancelledEvent.create());
            }
        });
        display.getNotesButton().addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent clickEvent) {
                display.toggleNotesPanel();
            }
        });
        display.getReplacementCost().addValueChangeHandler(new ValueChangeHandler<String>() {
            @Override
            public void onValueChange(ValueChangeEvent<String> stringValueChangeEvent) {
                ViewUtil.toDisplayValue(new BigDecimal(stringValueChangeEvent.getValue().replace(",", "")));
            }
        });
        display.getAddNoteButton().addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent clickEvent) {
                display.displayAddNotePopup();
            }
        });
        display.getSaveNoteButton().addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent clickEvent) {
                display.saveNote();
            }
        });
        display.getReplacementCost().addBlurHandler(new BlurHandler() {
            @Override
            public void onBlur(BlurEvent blurEvent) {
                try {
                    ViewUtil.setBigDecimal(display.getReplacementCost(), ViewUtil.getBigDecimal(display.getReplacementCost()));
                } catch(NumberFormatException n) {
                    MessageDialog.popup("Replacement cost must be a number!");
                    display.getReplacementCost().setFocus(true);
                }

            }
        });

    }

    protected Long getParreAnalysisId() {
        return ClientSingleton.getBaselineId();
    }

    protected boolean handlesAssetType(AssetType assetType) {
        return true;
    }


    private void populatePlPickList(ListBox listBox) {
        listBox.addItem("Select", "0");
        for (int i = 1; i <= 5; i++) {
            listBox.addItem(String.valueOf(i));
        }
    }
   
}
