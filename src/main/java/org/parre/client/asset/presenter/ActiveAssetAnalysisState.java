/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.asset.presenter;

import org.parre.client.ClientSingleton;
import org.parre.client.asset.event.EditAssetEvent;
import org.parre.client.asset.event.QuickEditAssetEvent;
import org.parre.client.messaging.ServiceFactory;
import org.parre.client.util.*;
import org.parre.shared.AssetDTO;

import com.google.gwt.event.shared.EventBus;
import com.google.gwt.user.client.rpc.AsyncCallback;

/**
 * The Class ActiveAssetAnalysisState.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 2/28/12
 */
public class ActiveAssetAnalysisState implements AssetAnalysisState {
    private ManageAssetPresenter.Display display;
    private EventBus eventBus;
    private BusyHandler busyHandler;

    public ActiveAssetAnalysisState(ManageAssetPresenter.Display display) {
        this.display = display;
        this.eventBus = ClientSingleton.getEventBus();
        this.busyHandler = new BusyHandler(display);
    }

    public void deleteAsset() {
        display.showConfirmDeletePopup();
    }

    public void editAsset(AssetDTO dto) {
        eventBus.fireEvent(EditAssetEvent.create(dto));
    }

    public void createAsset() {
        display.getAssetTypeListBox().setSelectedIndex(0);
        display.setSelectAssetTypeEnabled(false);
        display.showAssetTypePopup();
    }

    public void quickEditAsset(AssetDTO dto) {
        eventBus.fireEvent(QuickEditAssetEvent.create(dto));
        display.showEditPopup();
    }

    public void assetSelected(AssetDTO dto) {
        display.setEditAssetEnabled(dto != null);
        display.setDeleteAssetEnabled(dto != null);
    }

    public void persistAssetThreshold(final int threshold) {

        new ExecutableAsyncCallback<Integer>(busyHandler, new BaseAsyncCommand<Integer>() {
            public void execute(AsyncCallback<Integer> callback) {
                ServiceFactory.getInstance().getParreService().
                        updateAssetThreshold(threshold, ClientSingleton.getParreAnalysisId(), callback);
            }
            @Override
            public void handleResult(Integer oldValue) {
                ClientLoggingUtil.info("Threshold value replaced newValue=" + threshold + ", oldValue=" + oldValue);
            }

            @Override
            public boolean handleError(Throwable t) {
                MessageDialog.popup("Problem persisting Asset Threshold value = " + threshold);
                return true;
            }
        }).makeCall();
    }

}
