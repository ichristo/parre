/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.nav;



import java.util.HashMap;
import java.util.Map;

import org.parre.client.event.GoHomeEvent;
import org.parre.client.nav.event.*;
import org.parre.client.util.nav.NavEvent;
import org.parre.client.util.nav.NavEventMapper;

/**
 * The Class ParreNavEventMapper.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 3/27/12
 */
public class ParreNavEventMapper implements NavEventMapper {
    private static NavEventMapper instance = new ParreNavEventMapper();
    private static Map<String, NavEvent> navEventMap = new HashMap<String, NavEvent>();
    static {
        mapEvent(GoHomeEvent.create());
        mapEvent(AssetNavEvent.create());
        mapEvent(ThreatAssetMatrixNavEvent.create());
        mapEvent(ConsequenceNavEvent.create());
        mapEvent(LotNavEvent.create());
        mapEvent(DpNavEvent.create());
        mapEvent(NaturalThreatNavEvent.create());
        mapEvent(RiskBenefitNavEvent.create());
        mapEvent(ReportsNavEvent.create());
        mapEvent(RiskResilienceNavEvent.create());
        mapEvent(ThreatNavEvent.create());
        mapEvent(VulnerabilityNavEvent.create());
    }

    private static void mapEvent(NavEvent event) {
        navEventMap.put(generateToken(event), event);
    }

    private static String generateToken(NavEvent event) {
        return event.getNavToken();
    }

    public static NavEventMapper getInstance() {
        return instance;
    }

    private ParreNavEventMapper() {
    }

    public NavEvent getNavEvent(String token) {
        return navEventMap.get(token);
    }

    public NavEvent getDefaultNavEvent() {
        return GoHomeEvent.create();
    }

    public String getNavToken(NavEvent event) {
        return generateToken(event);
    }
}
