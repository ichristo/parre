/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */

/* TODO: Merge this back into the LGPL version */
package org.parre.client.nav;

import org.parre.client.ClientSingleton;
import org.parre.client.ViewFactory;
import org.parre.client.asset.view.AssetView;
import org.parre.client.at.view.ThreatAssetMatrixView;
import org.parre.client.consequence.view.ConsequenceView;
import org.parre.client.dp.view.DpView;
import org.parre.client.lot.view.LotView;
import org.parre.client.main.view.mainNavView;
import org.parre.client.nt.view.NaturalThreatView;
import org.parre.client.rpt.view.RptView;
import org.parre.client.rra.view.RraView;
import org.parre.client.rrm.view.RrmView;
import org.parre.client.threat.view.ThreatView;
import org.parre.client.util.AsyncViewLoader;
import org.parre.client.util.View;
import org.parre.client.util.ViewContainer;
import org.parre.client.util.ViewLoaderMap;
import org.parre.client.util.nav.NavViewMapper;
import org.parre.client.vulnerability.view.VulnerabilityView;

/**
 * A factory for creating NavViewMapper objects.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 4/16/12
 */
public class NavViewMapperFactory {
    public static NavViewMapper createNavViewMapper(ViewContainer viewContainer) {
        ViewLoaderMap viewLoaderMap = new ViewLoaderMap(); 
        initViewLoaders(viewLoaderMap, viewContainer);
        return viewLoaderMap;
    }
    
    private static void initViewLoaders(ViewLoaderMap viewLoaderMap, ViewContainer viewContainer) {
        viewLoaderMap.mapView(mainNavView.class, new AsyncViewLoader(viewContainer, "Home", "#system_navigation") {
            @Override
            protected View createView() {
                return ViewFactory.createMainNavView();
            }
        });
        viewLoaderMap.mapView(AssetView.class, new AsyncViewLoader(viewContainer, "Asset Management", "#asset_characterization") {
            @Override
            protected View createView() {
                return ViewFactory.createAssetView();
            }
        });
        viewLoaderMap.mapView(ThreatView.class, new AsyncViewLoader(viewContainer, "Threat Management", "#threat_characterization") {
            @Override
            protected View createView() {
                return ViewFactory.createThreatView();
            }
        });
        viewLoaderMap.mapView(ThreatAssetMatrixView.class, new AsyncViewLoader(viewContainer, "Threat-Asset Matrix", "#asset_threat_management") {
            @Override
            protected View createView() {
                return ViewFactory.createThreatAssetMatrixView(ClientSingleton.getAppInitData());
            }
        });
        viewLoaderMap.mapView(ConsequenceView.class, new AsyncViewLoader(viewContainer, "Consequence Analysis", "#consequence_analysis") {
            @Override
            protected View createView() {
                return ViewFactory.createConsequenceView();
            }
        });
        viewLoaderMap.mapView(VulnerabilityView.class, new AsyncViewLoader(viewContainer, "Vulnerability Analysis", "#vulnerability_analysis") {
            @Override
            protected View createView() {
                return ViewFactory.createVulnerabilityView();
            }
        });
        viewLoaderMap.mapView(LotView.class, new AsyncViewLoader(viewContainer, "Likelihood of Threat Analysis", "#threat_analysis") {
            @Override
            protected View createView() {
                return ViewFactory.createLotView();
            }
        });
        viewLoaderMap.mapView(NaturalThreatView.class, new AsyncViewLoader(viewContainer, "Natural Threat Analysis", "#natural_threat_analysis") {
            @Override
            protected View createView() {
                return ViewFactory.createNaturalThreatView();
            }
        });
        viewLoaderMap.mapView(DpView.class, new AsyncViewLoader(viewContainer, "Dependency Proximity Analysis", "#dp_threat_analysis") {
            @Override
            protected View createView() {
                return ViewFactory.createDpView();
            }
        });
        viewLoaderMap.mapView(RraView.class, new AsyncViewLoader(viewContainer, "Risk/Resilience Analysis", "#rra") {
            @Override
            protected View createView() {
                return ViewFactory.createRraView();
            }
        });
        viewLoaderMap.mapView(RrmView.class, new AsyncViewLoader(viewContainer, "Risk/Resilience Management", "#rrm") {
            @Override
            protected View createView() {
                return ViewFactory.createRrmView();
            }
        });
        viewLoaderMap.mapView(RptView.class, new AsyncViewLoader(viewContainer, "Reports", "#reports") {
            @Override
            protected View createView() {
                return ViewFactory.createRptView();
            }
        });
    }
    
}
