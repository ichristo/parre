/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.threat.presenter;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.ListBox;

import java.util.List;

import org.parre.client.messaging.ServiceFactory;
import org.parre.client.util.BusyHandler;
import org.parre.client.util.PopulateListBoxCommand;
import org.parre.shared.LabelValueDTO;
import org.parre.shared.types.ThreatCategoryType;

/**
 * .
 * User: Swarn S. Dhaliwal
 * Date: 9/4/11
 * Time: 3:30 PM
 */
public class ThreatCategoryListCommand extends PopulateListBoxCommand {

    public ThreatCategoryListCommand(ListBox listBox, BusyHandler busyHandler) {
        super(listBox, busyHandler);
    }

    @Override
    protected void getListBoxContents(AsyncCallback<List<LabelValueDTO>> asyncCallback) {
        ServiceFactory.getInstance().getThreatService().getThreatCategories(asyncCallback);
    }


    @Override
    protected String getFirstEntryValue() {
        return ThreatCategoryType.NONE.name();
    }
}
