/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.threat.presenter;

import org.parre.client.ClientSingleton;
import org.parre.client.messaging.ServiceFactory;
import org.parre.client.messaging.ThreatServiceAsync;
import org.parre.client.threat.event.CreateThreatEvent;
import org.parre.client.threat.event.EditThreatCancelEvent;
import org.parre.client.threat.event.EditThreatEvent;
import org.parre.client.threat.event.ThreatUpdatedEvent;
import org.parre.client.util.*;
import org.parre.shared.ThreatDTO;

import com.google.gwt.event.dom.client.*;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.ListBox;
import com.google.web.bindery.event.shared.EventBus;

/**
 * .
 * User: Swarn S. Dhaliwal
 * Date: 9/4/11
 * Time: 2:57 PM
 */
public class EditThreatPresenter implements Presenter {
    private EventBus eventBus;
    private Display display;
    private ThreatServiceAsync service;
    private BusyHandler busyHandler;

    /**
     * The Interface Display.
     */
    public interface Display extends ModelView<ThreatDTO> {

        HasClickHandlers getSaveButton();
        HasClickHandlers getCancelButton();

        ListBox getThreatCategories();

        HasClickHandlers getAddNoteButton();

        void showNotePopUp();

        HasClickHandlers getSaveNoteButton();

        void saveNote();

        void enableDetectionLikelihood(boolean b);
    }

    public EditThreatPresenter(Display display) {
        this.service = ServiceFactory.getInstance().getThreatService();
        this.eventBus = ClientSingleton.getEventBus();
        this.display = display;
        this.busyHandler = new BusyHandler(display);
    }

    public void go() {
        bind();
    }

    private void bind() {

        display.getSaveButton().addClickHandler(new ValidatingClickHandler(display) {
            public void handleClick(ClickEvent event) {

                new ExecutableAsyncCallback<ThreatDTO>(busyHandler, new BaseAsyncCommand<ThreatDTO>() {
                    public void execute(AsyncCallback<ThreatDTO> callback) {
                        ThreatDTO model = display.updateAndGetModel();
                        ServiceFactory.getInstance().getThreatService().saveThreat(model, ClientSingleton.getBaselineId(), callback);
                    }
                    @Override
                    public void handleResult(ThreatDTO results) {
                        eventBus.fireEvent(ThreatUpdatedEvent.create(results));
                    }
                }).makeCall();
            }
        });
        display.getCancelButton().addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                display.setModel(null);
                eventBus.fireEvent(EditThreatCancelEvent.create());
            }
        });
        display.getAddNoteButton().addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent clickEvent) {
                display.showNotePopUp();
            }
        });
        display.getSaveNoteButton().addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent clickEvent) {
                display.saveNote();
            }
        });
        display.getThreatCategories().addChangeHandler(new ChangeHandler() {
            @Override
            public void onChange(ChangeEvent event) {
                if(display.getThreatCategories().getSelectedIndex() == 2) {
                    display.enableDetectionLikelihood(true);
                }
                else {
                    display.enableDetectionLikelihood(false);
                }
            }
        });

        eventBus.addHandler(CreateThreatEvent.TYPE, new CreateThreatEvent.Handler() {
            public void onEvent(CreateThreatEvent event) {
                ThreatDTO model = new ThreatDTO();
                model.setUserDefined(Boolean.TRUE);
                display.setModelAndUpdateView(model);
            }
        });
        eventBus.addHandler(EditThreatEvent.TYPE, new EditThreatEvent.Handler() {
            public void onEvent(EditThreatEvent event) {
                display.setModelAndUpdateView(event.getThreatDTO());
            }
        });

        new ThreatCategoryListCommand(display.getThreatCategories(), busyHandler).execute();
    }
}
