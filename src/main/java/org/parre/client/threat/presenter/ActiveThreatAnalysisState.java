/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.threat.presenter;

import org.parre.client.ClientSingleton;
import org.parre.client.event.ThreatListChangedEvent;
import org.parre.client.messaging.ServiceFactory;
import org.parre.client.threat.event.CreateThreatEvent;
import org.parre.client.threat.event.EditThreatEvent;
import org.parre.client.util.*;
import org.parre.shared.ThreatDTO;
import org.parre.shared.exception.ItemInUseException;

import com.google.gwt.event.shared.EventBus;
import com.google.gwt.user.client.rpc.AsyncCallback;

/**
 * The Class ActiveThreatAnalysisState.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 2/28/12
 */
public class ActiveThreatAnalysisState implements ThreatAnalysisState {
    private ManageThreatPresenter.Display display;
    private EventBus eventBus;
    private BusyHandler busyHandler;

    public ActiveThreatAnalysisState(ManageThreatPresenter.Display display) {
        this.display = display;
        this.eventBus = ClientSingleton.getEventBus();
        this.busyHandler = new BusyHandler(display);
    }

    public void changeThreatActiveStatus(ThreatDTO threatDTO) {
        if (!threatDTO.getActive()) {
            display.showNoteDialog();
        }
        else {
            threatDTO.setInactiveReason("");
            updateThreatActive(threatDTO);
        }
    }

    public void createThreat() {
        eventBus.fireEvent(CreateThreatEvent.create());
    }

    public void deleteThreat(final ThreatDTO selectedThreatDTO) {
        ConfirmDialog.show("Delete this threat?", new ActionSource.ActionCommand() {
            public void execute(Object o) {
                doDeleteThreat(selectedThreatDTO);
            }
        });
    }

    private void doDeleteThreat(final ThreatDTO selectedThreatDTO) {

        new ExecutableAsyncCallback<Void>(busyHandler, new BaseAsyncCommand<Void>() {
            public void execute(AsyncCallback<Void> callback) {
                ServiceFactory.getInstance().getThreatService().deleteThreat(selectedThreatDTO, callback);
            }

            @Override
            public boolean handleError(Throwable t) {
                boolean needToHandle = t instanceof ItemInUseException;
                if (needToHandle) {
                    MessageDialog.popup(t.getMessage(), true);
                }
                return needToHandle;
            }

            @Override
            public void handleResult(Void results) {
                display.removeThreat(selectedThreatDTO);
                MessageDialog.popup("Threat " + selectedThreatDTO.getName() + " was deleted.");
            }
        }).makeCall();

    }

    public void editThreat(ThreatDTO threatDTO) {
        eventBus.fireEvent(EditThreatEvent.create(threatDTO));
    }

    public void threatSelected(ThreatDTO dto) {
        //boolean value = dto != null && dto.getUserDefined();
        if(dto != null) {
            display.getEditThreatEnabled().setEnabled(true);
            if(dto.getUserDefined()) {
                display.getDeleteThreatEnabled().setEnabled(true);
            }
            else {
                display.getDeleteThreatEnabled().setEnabled(false);
            }
        }
        else {
            display.getEditThreatEnabled().setEnabled(false);
            display.getDeleteThreatEnabled().setEnabled(false);
        }


    }

    public void updateThreatActive(final ThreatDTO threatDTO) {

        new ExecutableAsyncCallback<ThreatDTO>(busyHandler, new BaseAsyncCommand<ThreatDTO>() {
            public void execute(AsyncCallback<ThreatDTO> callback) {
                ServiceFactory.getInstance().getThreatService().updateThreatActive(threatDTO, ClientSingleton.getBaselineId(), callback);
            }
            @Override
            public void handleResult(ThreatDTO results) {
                eventBus.fireEvent(ThreatListChangedEvent.create());
                display.getSearchResultsSelectionModel().setSelected(results, true);
                display.refreshSearchResultsView();
            }
        }).makeCall();

    }

}
