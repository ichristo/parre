/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.threat.view;

import org.parre.client.threat.presenter.ThreatPresenter;
import org.parre.client.util.*;

import com.google.gwt.user.client.ui.*;


/**
 * User: Swarn S. Dhaliwal
 * Date: 7/10/11
 * Time: 4:39 PM
*/
public class ThreatView extends DeckResizeComposite implements ThreatPresenter.Display {

    private Presenter presenter;

    public ThreatView() {
    }

    public void setPresenter(Presenter presenter) {
        this.presenter = presenter;
    }

    public Presenter getPresenter() {
        return presenter;
    }
}