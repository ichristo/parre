/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.threat.view;

import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.event.logical.shared.HasCloseHandlers;
import com.google.gwt.user.client.ui.*;
import com.google.gwt.view.client.SingleSelectionModel;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.parre.client.common.view.DisplayToggleView;
import org.parre.client.common.view.ThreatCriteriaView;
import org.parre.client.threat.presenter.ManageThreatPresenter;
import org.parre.client.util.ActionSource;
import org.parre.client.util.BaseView;
import org.parre.client.util.NoteDialog;
import org.parre.client.util.ViewUtil;
import org.parre.client.util.dataview.DataProvider;
import org.parre.client.util.dataview.DataProviderFactory;
import org.parre.shared.ThreatDTO;

/**
 * The Class ManageThreatView.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 7/16/11
 */
public class ManageThreatView extends BaseView implements ManageThreatPresenter.Display {
    private final Map<Integer, Image> viewImageButtonMap = new HashMap<Integer, Image>(2);
    private ThreatGrid threatGrid;
    private Button editThreatButton;
    private Button deleteThreatButton;
    private Button createThreatButton;
    private Button manageAssetsButton;
    private Button assetThreatMatrixButton;
    private ThreatCriteriaView threatSearchView;
    private Image searchButton;
    private Image clearButton;
    private Image threatSearchButton;
    private VerticalPanel searchPanel;
    private NoteDialog noteDialog = new NoteDialog();
    private Widget viewWidget;
    private DisplayToggleView displayToggleView;

    public ManageThreatView(ThreatCriteriaView threatSearchView, DisplayToggleView displayToggleView) {
        this.threatSearchView = threatSearchView;
        this.displayToggleView = displayToggleView;
        viewWidget = createView();
    }

    private Widget createView() {
        searchPanel = new VerticalPanel();
        searchPanel.setWidth("100%");

        searchButton = ViewUtil.createImageButton("search-button.png", "Perform Search");
        clearButton = ViewUtil.createImageButton("clear-button.jpg", "Clear Search Results");
        threatSearchButton = ViewUtil.createImageButton("threat.jpg", "Search Using Threats");
        searchPanel.add(threatSearchView.asWidget());
        searchPanel.add(ViewUtil.layoutCenter(searchButton, clearButton));

        DataProvider<ThreatDTO> dataProvider = DataProviderFactory.createDataProvider();
        threatGrid = new ThreatGrid(dataProvider);
        editThreatButton = ViewUtil.createButton("Edit Threat");
        deleteThreatButton = ViewUtil.createButton("Delete Threat");
        createThreatButton = ViewUtil.createButton("Create Threat");
        manageAssetsButton = ViewUtil.createButton("Manage Assets");
        assetThreatMatrixButton = ViewUtil.createButton("Threat Asset Matrix");
        editThreatButton.setEnabled(false);
        deleteThreatButton.setEnabled(false);

        displayToggleView.setText("Search Criteria");
        displayToggleView.setDisplayToggleCommand(new DisplayToggleView.DisplayToggleCommand() {
            public void toggleDisplay(boolean show) {
                setSearchVisible(show);
            }
        });

        displayToggleView.setDisplayVisible(false);

        VerticalPanel viewPanel = new VerticalPanel();

        viewPanel.setWidth("100%");
        viewPanel.setHeight("100%");
        viewPanel.add(displayToggleView.asWidget());
        viewPanel.add(searchPanel);
        viewPanel.add(ViewUtil.horizontalPanel(5, editThreatButton, deleteThreatButton, createThreatButton,
                manageAssetsButton, assetThreatMatrixButton));
        viewPanel.add(threatGrid.asWidget());
        viewPanel.setSpacing(5);

        return viewPanel;
    }

    private void setSearchVisible(boolean value) {
        searchPanel.setVisible(value);
        threatGrid.setHeight(value ? "500px" : "650px");
    }

    private DialogBox createProgressDialog() {
        DialogBox.CaptionImpl captionWidget = new DialogBox.CaptionImpl();
        captionWidget.setText("Working");
        DialogBox progressDialog = new DialogBox(false, true, captionWidget);
        progressDialog.setTitle("Progress Dialog");
        progressDialog.setWidget(new Label("Performing Search, please wait..."));
        progressDialog.center();
        return progressDialog;
    }


    public HasClickHandlers getEditThreatButton() {
        return editThreatButton;
    }

    public HasClickHandlers getDeleteThreatButton() {
        return deleteThreatButton;
    }
    public HasClickHandlers getCreateThreatButton() {
        return createThreatButton;
    }

    public HasClickHandlers getManageAssetsButton() {
        return manageAssetsButton;
    }
    public HasClickHandlers getAssetThreatMatrixButton() {
        return assetThreatMatrixButton;
    }

    public HasEnabled getEditThreatEnabled() {
        return editThreatButton;
    }

    public HasEnabled getDeleteThreatEnabled() {
        return deleteThreatButton;
    }

    public void updateThreat(ThreatDTO threatDTO) {
        threatGrid.updateData(threatDTO);
    }

    public void removeThreat(ThreatDTO selectedThreatDTO) {
        threatGrid.removeData(selectedThreatDTO);
    }

    public void showNoteDialog() {
        noteDialog.getNoteTextArea().setText("");
        noteDialog.show();
    }

    public void hideNoteDialog() {
        noteDialog.hide();
    }

    public HasClickHandlers getSaveNoteButton() {
        return noteDialog.getSaveNoteButton();
    }

    public HasText getNoteText() {
        return noteDialog.getNoteTextArea();
    }

    public HasCloseHandlers<PopupPanel> getNoteDialog() {
        return noteDialog.getDialogBox();
    }

    public ActionSource<ThreatDTO> getActiveStatusChangedSource() {
        return threatGrid.getActiveStatusChangedSource();
    }

    public SingleSelectionModel<ThreatDTO> getSearchResultsSelectionModel() {
        return threatGrid.getSelectionModel();
    }

    public ActionSource<ThreatDTO> getThreatSelectionSource() {
        return threatGrid.getSelectionSource();
    }

    public HasClickHandlers getSearchButton() {
        return searchButton;
    }

    public HasClickHandlers getClearButton() {
        return clearButton;
    }

    public void setSearchResults(List<ThreatDTO> results) {
        threatGrid.setData(results);
        threatGrid.getSelectionModel().setSelected(null, true);
    }

    public void refreshSearchResultsView() {
        threatGrid.refresh();
    }

    public Widget asWidget() {
        return viewWidget;
    }
}
