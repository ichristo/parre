/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.threat.view;

import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.user.client.ui.*;

import java.util.ArrayList;
import java.util.List;

import org.parre.client.ClientSingleton;
import org.parre.client.common.view.NoteRowView;
import org.parre.client.threat.presenter.EditThreatPresenter;
import org.parre.client.util.BaseModelView;
import org.parre.client.util.FieldValidationHandler;
import org.parre.client.util.NoteDialog;
import org.parre.client.util.ViewUtil;
import org.parre.shared.NoteDTO;
import org.parre.shared.ThreatDTO;
import org.parre.shared.types.ThreatCategoryType;

import static org.parre.client.common.ClientNoteUtil.getNoteDTOs;
import static org.parre.client.common.ClientNoteUtil.setNotesPanel;

/**
 * User: Swarn S. Dhaliwal
 * Date: 9/4/11
 * Time: 2:56 PM
 */
public class EditThreatView extends BaseModelView<ThreatDTO> implements EditThreatPresenter.Display {
    private HorizontalPanel controlPanel;

    private List<FieldValidationHandler> requiredFieldHandlers;
    private TextBox hazardCode;
    private TextBox hazardDescription;
    private TextBox hazardType;
    private TextBox detectionLikelihood;
    private Button saveButton;

    private Button cancelButton;
    private Widget viewWidget;
    private ListBox threatCategory;

    private VerticalPanel notesPanel;
    private List<NoteRowView> noteViews = new ArrayList<NoteRowView>();
    private Button addNoteButton;
    private NoteDialog noteDialog = new NoteDialog();
    private FlexTable notesTable = new FlexTable();

    public EditThreatView() {
        requiredFieldHandlers = new ArrayList<FieldValidationHandler>();
        viewWidget = createView();
    }

    private VerticalPanel createView() {

        VerticalPanel viewPanel = new VerticalPanel();
        viewPanel.setStyleName("inputPanel");
        Label header = new Label("Threat Information");
        header.setStyleName("inputPanelHeader");
        viewPanel.add(header);

        FlexTable inputTable = new FlexTable();
        inputTable.setCellSpacing(6);
        inputTable.setWidth("100%");
        viewPanel.add(inputTable);

        int rowNum = -1;

        hazardCode = ViewUtil.addTextBoxFormEntry(inputTable, ++rowNum, "Hazard Code", requiredFieldHandlers);
        hazardDescription = ViewUtil.addTextBoxFormEntry(inputTable, ++rowNum, "Hazard Description", requiredFieldHandlers);
        hazardType = ViewUtil.addTextBoxFormEntry(inputTable, ++rowNum, "Hazard Type", requiredFieldHandlers);
        detectionLikelihood = ViewUtil.addTextBoxFormEntry(inputTable, ++rowNum, "Detection Likelihood");
        threatCategory = ViewUtil.addListBoxFormEntry(inputTable, ++rowNum, "Threat Category", requiredFieldHandlers);

        notesPanel = new VerticalPanel();
        addNoteButton = ViewUtil.createButton("Add Note", "Create new note.");
        ViewUtil.addFormEntry(notesTable, 0, "Notes", addNoteButton);
        notesPanel.add(ViewUtil.layoutCenter(notesTable));
        notesPanel.setStyleName("inputPanel");

        saveButton = ViewUtil.createButton("Save");
        cancelButton = ViewUtil.createButton("Cancel");

        controlPanel = ViewUtil.layoutCenter(saveButton, cancelButton);
        viewPanel.add(controlPanel);
        viewPanel.add(notesPanel);
        setControlsVisible(true);
        return viewPanel;
    }

    public HasClickHandlers getSaveButton() {
        return saveButton;
    }

    public HasClickHandlers getCancelButton() {
        return cancelButton;
    }

    public ListBox getThreatCategories() {
        return threatCategory;
    }

    @Override
    public HasClickHandlers getAddNoteButton() {
        return addNoteButton;
    }

    @Override
    public void showNotePopUp() {
        noteDialog.show();
    }

    @Override
    public HasClickHandlers getSaveNoteButton() {
        return noteDialog.getSaveNoteButton();
    }

    @Override
    public void saveNote() {
        NoteRowView noteRowView = new NoteRowView();
        noteRowView.setUserName(ClientSingleton.getLoginInfo().getNickname());
        noteRowView.setDate();
        noteRowView.setMessage(noteDialog.getNoteTextArea().getText());
        noteRowView.setNew(true);
        noteViews.add(noteRowView);
        updateModel();
        updateView();
        noteDialog.getNoteTextArea().setText("");
        noteDialog.hide();
    }

    @Override
    public void enableDetectionLikelihood(boolean b) {
        if(b) {
            detectionLikelihood.setText("");
        }
        else {
            detectionLikelihood.setText(" ");
        }
        detectionLikelihood.setEnabled(b);
    }

    public void updateModel() {
        ThreatDTO model = getModel();
        model.setName(ViewUtil.getText(hazardCode));
        model.setDescription(ViewUtil.getText(hazardDescription));
        model.setHazardType(ViewUtil.getText(hazardType));
        model.setThreatCategory(ThreatCategoryType.valueOf(ViewUtil.getSelectedValue(threatCategory)));
        model.setNoteDTOs(getNoteDTOs(noteViews));
        if(model.getThreatCategory().isManMadeHazard()) {
            model.setDetectionLikelihood(ViewUtil.getBigDecimal(detectionLikelihood));
        }
    }

    public void updateView() {
        ThreatDTO model = getModel();
        ViewUtil.setText(hazardCode, model.getName());
        hazardCode.setEnabled(model.getUserDefined());
        ViewUtil.setText(hazardDescription, model.getDescription());
        hazardDescription.setEnabled(model.getUserDefined());
        ViewUtil.setText(hazardType, model.getHazardType());
        hazardType.setEnabled(model.getUserDefined());
        ViewUtil.setSelectedValue(threatCategory, model.getThreatCategory().name());
        threatCategory.setEnabled(model.getUserDefined());
        if(model.getThreatCategory().isManMadeHazard()) {
            ViewUtil.setBigDecimal(detectionLikelihood, model.getDetectionLikelihood());
            detectionLikelihood.setEnabled(model.getUserDefined());
        }
        else {
            detectionLikelihood.setEnabled(false);
            detectionLikelihood.setText(" ");
        }
        notesPanel.clear();
        noteViews = new ArrayList<NoteRowView>();
        for(NoteDTO dto : model.getNoteDTOs()) {
            noteViews.add(new NoteRowView(dto));
        }
        notesPanel.add(ViewUtil.layoutCenter(notesTable));
        notesPanel.add(setNotesPanel(noteViews));
    }

    public Widget asWidget() {
        return viewWidget;
    }


    public void setControlsVisible(boolean value) {
        controlPanel.setVisible(value);
    }

    public boolean validate() {
        return ViewUtil.hasRequiredValues(requiredFieldHandlers);
    }

}
