/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.server;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicLong;

import javax.persistence.EntityManager;

import org.apache.log4j.Logger;
import org.parre.server.domain.Address;
import org.parre.server.domain.BaseEntity;
import org.parre.server.domain.Contact;
import org.parre.server.domain.DetectionLikelihood;
import org.parre.server.domain.ExternalResource;
import org.parre.server.domain.HumanResource;
import org.parre.server.domain.LookupValue;
import org.parre.server.domain.Org;
import org.parre.server.domain.PhysicalResource;
import org.parre.server.domain.ProxyCity;
import org.parre.server.domain.ProxyTargetType;
import org.parre.server.domain.ProxyTier;
import org.parre.server.domain.Role;
import org.parre.server.domain.Threat;
import org.parre.server.domain.UriOption;
import org.parre.server.domain.UriQuestion;
import org.parre.server.domain.User;
import org.parre.server.domain.UserPreferences;
import org.parre.server.domain.ZipLookupData;
import org.parre.server.domain.manager.AssetManager;
import org.parre.server.domain.manager.LotManager;
import org.parre.server.domain.manager.ThreatManager;
import org.parre.server.domain.util.DomainUtil;
import org.parre.server.messaging.invocation.ServiceInvocationContext;
import org.parre.server.util.DataUtil;
import org.parre.shared.AddressDTO;
import org.parre.shared.AmountDTO;
import org.parre.shared.AssetDTO;
import org.parre.shared.BaseDTO;
import org.parre.shared.ConsequenceDTO;
import org.parre.shared.ContactDTO;
import org.parre.shared.DetectionLikelihoodDTO;
import org.parre.shared.ExternalResourceDTO;
import org.parre.shared.HumanResourceDTO;
import org.parre.shared.LabelValueDTO;
import org.parre.shared.OrgDTO;
import org.parre.shared.PhysicalResourceDTO;
import org.parre.shared.ProxyCityDTO;
import org.parre.shared.ProxyTargetTypeDTO;
import org.parre.shared.ProxyTierDTO;
import org.parre.shared.StatValuesDTO;
import org.parre.shared.ThreatDTO;
import org.parre.shared.UserPreferencesDTO;
import org.parre.shared.ZipLookupDataDTO;
import org.parre.shared.types.LookupType;
import org.parre.shared.types.MoneyUnitType;
import org.parre.shared.types.ThreatCategoryType;
import org.parre.shared.types.UriQuestionType;


/**
 * User: Swarn Dhaliwal
 * Date: 7/14/11
 * Time: 11:30 AM
*/
@SuppressWarnings("unchecked")
public class ParreDatabase {
    private static transient Logger log = Logger.getLogger(ParreDatabase.class);
    private static Map<Class<?>, Map<Long, BaseDTO>> database;
    private static final String packagePrefix = "org.parre.shared.";
    private static ParreDatabase instance = new ParreDatabase();
    private AtomicLong objectId = new AtomicLong(1000);
    private Map<String, User> userMap = new HashMap<String, User>();
    private List<LabelValueDTO> stateList = new ArrayList<LabelValueDTO>(51);
    private Integer assetThreshold = 3;
    private Integer assetThreatThreshold = 1;

    private Map<Long, Map<Long, Integer>> assetThreatLevelMatrix = new HashMap<Long, Map<Long, Integer>>();
    private List<ConsequenceDTO> consequenceAnalysis = Collections.emptyList();
    private StatValuesDTO statisticalValuesDTO;
    private List<ZipLookupDataDTO> zipLookupDataDTOs = new ArrayList<ZipLookupDataDTO>();

    private ParreDatabase() {
        statisticalValuesDTO = new StatValuesDTO();
        statisticalValuesDTO.setFatalityDTO(new AmountDTO(BigDecimal.valueOf(7.4), MoneyUnitType.MILLION));
        statisticalValuesDTO.setSeriousInjuryDTO(new AmountDTO(BigDecimal.valueOf(1.5), MoneyUnitType.MILLION));
    }

    private void initDbMap() {
        database = new HashMap<Class<?>, Map<Long, BaseDTO>>();
    }

    public void initLookupValues(File file) {
        try {
            List<String[]> dataRecords;
            dataRecords = DataUtil.createRecords(file, "\t");
            for (String[] dataRecord : dataRecords) {
                LookupType lookupType = LookupType.valueOf(dataRecord[0]);
                LookupValue lookupValue = new LookupValue(dataRecord[1], dataRecord[2], lookupType);
                saveEntity(lookupValue);
            }
        } catch (Exception e) {
            String message = "Error initializing states : " + e.getMessage();
            log.error(message, e);
            throw new RuntimeException(message);
        }

    }

    public void initQuestions(File file) {
        try {
            List<String[]> dataRecords;
            dataRecords = DataUtil.createRecords(file, "\t");
            DataUtil.removeQuotes(dataRecords);
            Map<String, List<String[]>> questionMap = new HashMap<String, List<String[]>>();
            for (String[] dataRecord : dataRecords) {
                String questionNumber = dataRecord[0];
                List<String[]> questions = questionMap.get(questionNumber);
                if (questions == null) {
                    questions = new ArrayList<String[]>();
                    questionMap.put(questionNumber, questions);
                }
                questions.add(dataRecord);
            }
            List<String> questionNumbers = new LinkedList<String>(questionMap.keySet());
            Collections.sort(questionNumbers);
            for (String questionNumber : questionNumbers) {
                List<String[]> answers = questionMap.get(questionNumber);
                String questionText = answers.get(0)[1];
                String[] parts = questionText.split(":");
                if (log.isDebugEnabled()) { log.debug(questionNumber + " - " + parts[0]); }
                if (log.isDebugEnabled()) { log.debug("\t" + parts[1]); }
                UriQuestionType questionType = questionNumber.startsWith("FRI") ?
                        UriQuestionType.FRI : UriQuestionType.ORI;

                UriQuestion question = new UriQuestion(questionType, parts[0], parts[1]);
                saveEntity(question);
                for (String[] option : answers) {
                	if (log.isDebugEnabled()) { log.debug("\t\t" + option[2] + " " + option[3] + " " + option[4]); }
                    UriOption uriOption = new UriOption(option[2], new BigDecimal(option[3]), new BigDecimal(option[4]));
                    question.addOption(uriOption);
                    saveEntity(uriOption);
                }
            }

        } catch (Exception e) {
            String message = "Error initializing states : " + e.getMessage();
            log.error(message, e);
            throw new RuntimeException(message);
        }

    }

    public void initData(File dataFile) {
        initDbMap();
        try {
            List<String[]> dataRecords;
            dataRecords = DataUtil.createRecords(dataFile);
            BaseDTO dto;
            for (String[] dataRecord : dataRecords) {
                dto = createDataRecord(dataRecord);
                save(dto);
            }
        } catch (Exception e) {
            String message = "Error initializing data : " + e.getMessage();
            log.error(message, e);
            File f;
            f=new File("myfile.txt");
            if(!f.exists()){
                try {
                    f.createNewFile();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
            throw new RuntimeException(message);
        }
    }

    public void initCityTiers(File usersFile) {
        try {
            List<String[]> tiers;
            tiers = DataUtil.createRecords(usersFile);
            DataUtil.removeQuotes(tiers);
            ProxyCityDTO dto;
            for (String[] tier : tiers) {
                dto = createCityTier(tier);
                ProxyCity proxyCity = new ProxyCity();
                proxyCity.setCity(dto.getCity());
                proxyCity.setProxyTier(LotManager.findProxyTier(dto.getTierNumber(), entityManager()));
                saveEntity(proxyCity);
            }
        } catch (Exception e) {
            String message = "Error initializing city tiers : " + e.getMessage();
            log.error(message, e);
            throw new RuntimeException(message);
        }
    }

    public void initTargetTypes(File usersFile) {
        try {
            List<String[]> types;
            types = DataUtil.createRecords(usersFile);
            DataUtil.removeQuotes(types);
            ProxyTargetTypeDTO dto;
            for (String[] tier : types) {
                dto = createTargetType(tier);
                ProxyTargetType entity = new ProxyTargetType();
                entity.copyFrom(dto);
                saveEntity(entity);
            }
        } catch (Exception e) {
            String message = "Error initializing target types : " + e.getMessage();
            log.error(message, e);
            throw new RuntimeException(message);
        }
    }

    public void initDetectionLikelihood(File usersFile) {
        try {
            List<String[]> likelihoods;
            likelihoods = DataUtil.createRecords(usersFile);
            DetectionLikelihoodDTO dto;
            for (String[] likelihood : likelihoods) {
                dto = createDetectionLikelihood(likelihood);
                DetectionLikelihood entity = new DetectionLikelihood();
                Threat threat = ThreatManager.findThreat(dto.getName(), entityManager());
                entity.setThreat(threat);
                entity.setLikelihood(dto.getLikelihood());
                saveEntity(entity);
            }
        } catch (Exception e) {
            String message = "Error initializing detection likelihood : " + e.getMessage();
            log.error(message, e);
            throw new RuntimeException(message);
        }
    }

    public void initCityTierTotals(File usersFile) {
        try {
            List<String[]> tierTotals;
            tierTotals = DataUtil.createRecords(usersFile);
            ProxyTierDTO dto;
            for (String[] tierTotal : tierTotals) {
                dto = createCityTierTotals(tierTotal);
                getCityTierTotalsDTOs().add(dto);
                ProxyTier proxyTier = new ProxyTier();
                proxyTier.setTierNumber(dto.getTierNumber());
                proxyTier.setNumberOfCities(dto.getNumberOfCities());
                saveEntity(proxyTier);
            }
        } catch (Exception e) {
            String message = "Error initializing detection likelihood : " + e.getMessage();
            log.error(message, e);
            throw new RuntimeException(message);
        }
    }

    private ProxyCityDTO createCityTier(String[] tier) {
        ProxyCityDTO dto = new ProxyCityDTO();
        dto.setCity(tier[0]);
        dto.setTierNumber(Integer.parseInt(tier[1]));
        return dto;
    }

    private ProxyTargetTypeDTO createTargetType(String[] type) {
        ProxyTargetTypeDTO dto = new ProxyTargetTypeDTO();
        dto.setCode(Integer.valueOf(type[0]));
        dto.setName(type[1]);
        dto.setProbability(new java.math.BigDecimal(type[2]));
        return dto;
    }

    private DetectionLikelihoodDTO createDetectionLikelihood(String[] likelihood) {
        DetectionLikelihoodDTO dto = new DetectionLikelihoodDTO();
        dto.setName(likelihood[0]);
        dto.setLikelihood(new java.math.BigDecimal(likelihood[1]));
        return dto;
    }

    private ProxyTierDTO createCityTierTotals(String[] tiers) {
        ProxyTierDTO dto = new ProxyTierDTO();
        dto.setTierNumber(Integer.parseInt(tiers[0]));
        dto.setNumberOfCities(Integer.parseInt(tiers[1]));
        return dto;
    }

    public void initUsers(File usersFile) {
        Set<Role> roles = new HashSet<Role>();
        Role role;
        role = new Role("admin", "Administrative User");
        saveEntity(role);
        roles.add(role);
        role = new Role("analyst", "Analyst User");
        saveEntity(role);
        roles.add(role);

        List<String[]> records = DataUtil.createRecords(usersFile);
        for (String[] record : records) {
            User user = createUser(record, roles);
            userMap.put(user.getEmail(), user);
        }
    }

    private User createUser(String[] dataRecord, Set<Role> roles) {
        User user = new User();
        if (log.isDebugEnabled()) { log.debug("DataRecord=" + Arrays.toString(dataRecord)); }
        int fieldIndex = -1;
        user.setEmail(dataRecord[++fieldIndex]);
        user.setPassword(dataRecord[++fieldIndex]);
        user.setNickname(dataRecord[++fieldIndex]);
        Long orgId = Long.valueOf(dataRecord[++fieldIndex]);
        user.setOrg(entityManager().find(Org.class, orgId));
        Long prefId = Long.valueOf(dataRecord[++fieldIndex]);
        user.setUserPreferences(entityManager().find(UserPreferences.class, prefId));
        saveEntity(user);
        user.setRoles(roles);
        saveEntity(user);
        return user;
    }

    public <T> T save(T value) {
        BaseDTO dto = (BaseDTO) value;
        if (log.isDebugEnabled()) { log.debug("Adding Obj : " + dto.getId() + " " + dto); }
        if (dto.isNew()) {
            dto.setId(generateObjectId());
        }
        Map<Long, BaseDTO> objectMap = database.get(dto.getClass());
        if (objectMap == null) {
            objectMap = new HashMap<Long, BaseDTO>();
            database.put(dto.getClass(), objectMap);
        }
        objectMap.put(dto.getId(), dto);
        return value;
    }

    private BaseDTO createDataRecord(String[] dataRecord) throws Exception {
        String objType = dataRecord[0];
        Object objInstance = Class.forName(packagePrefix + objType + "DTO").newInstance();
        if (objInstance instanceof PhysicalResourceDTO) {
            return populateData((PhysicalResourceDTO) objInstance, dataRecord);
        } else if (objInstance instanceof HumanResourceDTO) {
            return populateData((HumanResourceDTO) objInstance, dataRecord);
        } else if (objInstance instanceof ExternalResourceDTO) {
            return populateData((ExternalResourceDTO) objInstance, dataRecord);
        } else if (objInstance instanceof ThreatDTO) {
            return populateData((ThreatDTO) objInstance, dataRecord);
        } else if (objInstance instanceof ContactDTO) {
            return populateData((ContactDTO) objInstance, dataRecord);
        } else if (objInstance instanceof OrgDTO) {
            return populateData((OrgDTO) objInstance, dataRecord);
        } else if (objInstance instanceof AddressDTO) {
            return populateData((AddressDTO) objInstance, dataRecord);
        } else if (objInstance instanceof UserPreferencesDTO) {
            return populateData((UserPreferencesDTO) objInstance, dataRecord);
        }
        throw new IllegalArgumentException("Unknown type : " + objInstance.getClass().getName());
    }

    private BaseDTO populateData(AddressDTO addressDTO, String[] dataRecord) {
        int pos = 0;
        addressDTO.setStreetAddress(dataRecord[++pos]);
        addressDTO.setUnit(dataRecord[++pos]);
        addressDTO.setCity(dataRecord[++pos]);
        addressDTO.setState(dataRecord[++pos]);
        addressDTO.setPostalCode(dataRecord[++pos]);
        addressDTO.setCountry("US");
        Address address = new Address();
        address.copyFrom(addressDTO);
        saveEntity(address);
        return addressDTO;
    }

    private BaseDTO populateData(OrgDTO dto, String[] dataRecord) {
        int pos = 0;
        dto.setName(dataRecord[++pos]);
        dto.setDescription(dataRecord[++pos]);
        Long addressId = Long.valueOf(dataRecord[++pos]);
        Org org = new Org();
        org.copyFrom(dto);
        org.setAddress(entityManager().find(Address.class, addressId));
        saveEntity(org);
        return dto;
    }


    private BaseDTO populateData(ContactDTO dto, String[] dataRecord) {
        int pos = 0;
        dto.setContactName(dataRecord[++pos]);
        dto.setEmail(dataRecord[++pos]);
        dto.setPhone(dataRecord[++pos]);
        dto.setMobilePhone(dataRecord[++pos]);
        dto.setWorkPhone(dataRecord[++pos]);
        dto.setFax(dataRecord[++pos]);
        Long addressId = Long.valueOf(dataRecord[++pos]);
        Long orgId = Long.valueOf(dataRecord[++pos]);
        Contact contact = new Contact();
        contact.copyFrom(dto);
        contact.setAddress(entityManager().find(Address.class, addressId));
        contact.setOrg(entityManager().find(Org.class, orgId));
        saveEntity(contact);
        return dto;
    }

    private BaseDTO populateData(ThreatDTO dto, String[] dataRecord) {
        int pos = 0;
        //dto.setId(Long.valueOf(dataRecord[++pos]));
        ++pos; // to skip over id
        dto.setHazardType(dataRecord[++pos]);
        dto.setName(dataRecord[++pos]);
        dto.setDescription(dataRecord[++pos]);
        dto.setThreatCategory(ThreatCategoryType.fromValue(dataRecord[++pos]));
        dto.setBaselineId(-1L);
        Threat threat = new Threat();
        threat.copyFrom(dto);
        saveEntity(threat);
        return dto;
    }

    private BaseDTO populateData(UserPreferencesDTO dto, String[] dataRecord) {
        int pos = 0;
        dto.setUseScientificNotation(dataRecord[++pos]);
        //dto.setId(Long.valueOf(dataRecord[++pos]));
        UserPreferences userPreferences = new UserPreferences();
        userPreferences.copyFrom(dto);
        saveEntity(userPreferences);
        return dto;
    }

    private void saveEntity(BaseEntity baseEntity) {
        EntityManager entityManager = entityManager();
        entityManager.persist(baseEntity);
        entityManager.flush();
    }
    
    private void saveAll(List<BaseEntity> entities) {
    	EntityManager entityManager = entityManager();
    	for (BaseEntity baseEntity : entities) {
    		entityManager.persist(baseEntity);
    	}
        entityManager.flush();
    }
    
    private EntityManager entityManager() {
        return ServiceInvocationContext.getCurrentInstance().getEntityManager();
    }

    private BaseDTO populateData(PhysicalResourceDTO dto, String[] dataRecord) {
        int pos = populateAssetData(dto, dataRecord);
        dto.setYearBuilt(Integer.valueOf(dataRecord[++pos]));
        dto.setPostalCode(dataRecord[++pos]);
        dto.setReplacementCostDTO(new AmountDTO(new java.math.BigDecimal(dataRecord[++pos]), MoneyUnitType.MILLION));
        PhysicalResource resource = new PhysicalResource();
        resource.copyFrom(dto);
        if (log.isDebugEnabled()) { log.debug("Replacement Cost : " + resource.getReplacementCost()); }
        saveEntity(resource);
        return dto;
    }

    private BaseDTO populateData(HumanResourceDTO dto, String[] dataRecord) {
        populateAssetData(dto, dataRecord);
        HumanResource hr = new HumanResource();
        hr.copyFrom(dto);
        saveEntity(hr);
        return dto;
    }

    private BaseDTO populateData(ExternalResourceDTO dto, String[] dataRecord) {
        populateAssetData(dto, dataRecord);
        ExternalResource er = new ExternalResource();
        er.copyFrom(dto);
        saveEntity(er);
        return dto;
    }

    private int populateAssetData(AssetDTO dto, String[] dataRecord) {
        int pos = 0;
        dto.setId(Long.valueOf(dataRecord[++pos]));
        dto.setName(dataRecord[++pos]);
        dto.setAssetId(dataRecord[++pos]);
        dto.setCritical(Boolean.valueOf(dataRecord[++pos]));
        dto.setHumanPl(Integer.valueOf(dataRecord[++pos]));
        dto.setFinancialPl(Integer.valueOf(dataRecord[++pos]));
        dto.setEconomicPl(Integer.valueOf(dataRecord[++pos]));
        return pos;
    }

    public <T> T delete(T t) {
        BaseDTO dto = (BaseDTO) t;
        database.get(dto.getClass()).remove(dto.getId());
        return t;
    }

    public <T> T getDTO(Class<T> aClass, Long id) {
    	if (log.isDebugEnabled()) { log.debug("Getting obj : " + id + " " + aClass.getSimpleName()); }
        return (T) database.get(aClass).get(id);
    }

    public <T> List<T> getDTOs(Class<T> type) {
        Map<Long, BaseDTO> dtoMap = database.get(type);
        return (List<T>) (dtoMap == null ? Collections.EMPTY_LIST : new ArrayList<BaseDTO>(dtoMap.values()));
    }

    public <T> List<T> getDTOs(Class<T> type, boolean includeInherited) {
        if (!includeInherited) {
            return getDTOs(type);
        }
        List<T> list = new ArrayList<T>();
        Set<Class<?>> classes = database.keySet();
        for (Class<?> aClass : classes) {
            if (type.isAssignableFrom(aClass)) {
                list.addAll((Collection<T>) database.get(aClass).values());
            }
        }
        return list;
    }

    public static ParreDatabase get() {
        return instance;
    }


    public Long generateObjectId() {
        return objectId.incrementAndGet();
    }

    public User getUser(String email) {
        return userMap.get(email);
    }

    public List<LabelValueDTO> getStateList() {
        return stateList;
    }

    public Integer updateAssetThreshold(Integer newValue) {
        Integer oldValue = this.assetThreshold;
        this.assetThreshold = newValue;
        return oldValue;
    }

    public Integer getAssetThreshold() {
        return assetThreshold;
    }

    public Integer updateAssetThreatThreshold(Integer thresholdValue) {
        Integer oldValue = this.assetThreatThreshold;
        this.assetThreatThreshold = thresholdValue;
        return oldValue;
    }

    public Integer getAssetThreatThreshold() {
        return assetThreatThreshold;
    }

    public List<ThreatDTO> getActiveThreats() {
        List<ThreatDTO> dtos = (List<ThreatDTO>) getDTOs(ThreatDTO.class);
        List<ThreatDTO> filteredDTOs = new ArrayList<ThreatDTO>(dtos.size());
        for (ThreatDTO dto : dtos) {
            if (dto.getActive()) {
                filteredDTOs.add(dto);
            }
        }
        return filteredDTOs;
    }

    public Integer putAssetThreatLevel(Long assetId, Long threatId, Integer level) {
        Map<Long, Integer> assetThreatLevels = assetThreatLevelMatrix.get(assetId);
        if (assetThreatLevels == null) {
            assetThreatLevels = new HashMap<Long, Integer>();
            assetThreatLevelMatrix.put(assetId, assetThreatLevels);
        }
        return assetThreatLevels.put(threatId, level);
    }

    public Integer getAssetThreatLevel(Long assetId, Long threatId) {
        Map<Long, Integer> assetThreatLevels = assetThreatLevelMatrix.get(assetId);
        if (assetThreatLevels == null) {
            putAssetThreatLevel(assetId, threatId, 0);
            return 0;
        } else {
            Integer level = assetThreatLevels.get(threatId);
            if (level == null) {
                level = 0;
                putAssetThreatLevel(assetId, threatId, level);
            }
            return level;
        }
    }

    public void setConsequenceAnalysis(List<ConsequenceDTO> consequenceDTOs) {
        this.consequenceAnalysis = consequenceDTOs;
    }

    public List<ConsequenceDTO> getConsequenceAnalysis() {
        return consequenceAnalysis;
    }

    public StatValuesDTO getStatisticalValuesDTO() {
        return statisticalValuesDTO;
    }

    public List<DetectionLikelihoodDTO> getDetectionLikelihoodDTOs() {
        return LotManager.getDetectionLikelihoodDTOs(entityManager());
    }

    public List<ProxyTargetTypeDTO> getTargetTypesDTOs() {
        List<ProxyTargetType> targetTypes = LotManager.getProxyTargetTypes(entityManager());
        return ProxyTargetType.createDTOs(targetTypes);
    }

    public List<ProxyCityDTO> getCityTierDTOs() {
        return LotManager.getProxyCityTierDTOs(entityManager());
    }

    public List<ProxyTierDTO> getCityTierTotalsDTOs() {
        List<ProxyTier> proxyTiers = LotManager.getProxyTiers(entityManager());
        return ProxyTier.createDTOs(proxyTiers);
    }

    public void initZipCodes(File userFile) {
    	zipLookupDataDTOs = new ArrayList<ZipLookupDataDTO>();
        try {
            List<String[]> zipCodes = DataUtil.createRecords(userFile, ",");
            DataUtil.removeQuotes(zipCodes);
            ZipLookupDataDTO dto;
            List<BaseEntity> entities = new ArrayList<BaseEntity>();
            for (String[] zipCode : zipCodes) {
            	if (zipCode.length >= 5) {
	                dto = createZipCodes(zipCode);
	                ZipLookupData entity = new ZipLookupData();
	                DomainUtil.copyProperties(entity, dto);
	                //saveEntity(entity);
	                entities.add(entity);
            	} else {
            		log.debug("Not loading: " + Arrays.toString(zipCode));
            	}
            }
            
            if (entities.size() > 0) {
            	saveAll(entities);
            }
        } catch (Exception e) {
            String message = "Error initializing zip codes: " + e.getMessage();
            log.error(message, e);
            throw new RuntimeException(message);
        }
    }

    public List<ZipLookupDataDTO> getZipLookupDataDTOs() {
    	if (zipLookupDataDTOs == null || zipLookupDataDTOs.size() == 0) {
    		List<ZipLookupData> zipLookupDatas = AssetManager.getZipLookupData(entityManager());
    		zipLookupDataDTOs = ZipLookupData.createDTOs(zipLookupDatas);
    	}
    	return zipLookupDataDTOs;
    }
    
    private ZipLookupDataDTO createZipCodes(String[] zipCode) {
        ZipLookupDataDTO dto = new ZipLookupDataDTO();
        dto.setZipCode(zipCode[0]);
        dto.setCounty(zipCode[4]);
        dto.setStateCode(zipCode[3]);
        
        try {
			dto.setLatitude(new BigDecimal(zipCode[1]));
			dto.setLongitude(new BigDecimal(zipCode[2]));
		} catch (Exception e) {
			dto.setLatitude(BigDecimal.ZERO);
			dto.setLongitude(BigDecimal.ZERO);
		}
		return dto;
    }

}
