/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.server;

import java.io.File;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;

import org.apache.log4j.Logger;
import org.parre.server.domain.BaseEntity;
import org.parre.server.domain.EarthquakeLookupData;
import org.parre.server.domain.HurricaneLookupData;
import org.parre.server.domain.IceworkLookupData;
import org.parre.server.domain.TornadoLookupData;
import org.parre.server.domain.WindLookupData;
import org.parre.server.domain.ZipLookupData;
import org.parre.server.domain.manager.NaturalThreatManager;
import org.parre.server.messaging.invocation.ServiceInvocationContext;
import org.parre.server.util.DataUtil;
import org.parre.server.util.GeneralUtil;
import org.parre.shared.EarthquakeLookupDataDTO;
import org.parre.shared.HurricaneLookupDataDTO;
import org.parre.shared.IceworkLookupDataDTO;
import org.parre.shared.TornadoLookupDataDTO;
import org.parre.shared.WindLookupDataDTO;


/**
 * The Class NaturalThreatLookupData.
 */
public class NaturalThreatLookupData {
	private static transient Logger log = Logger.getLogger(NaturalThreatLookupData.class);
	
	private static NaturalThreatLookupData instance = new NaturalThreatLookupData();
	
	private List<EarthquakeLookupDataDTO> earthquakeLookupDataDTOs = new ArrayList<EarthquakeLookupDataDTO>();
    private List<HurricaneLookupDataDTO> hurricaneLookupDataDTOs = new ArrayList<HurricaneLookupDataDTO>();
    private List<TornadoLookupDataDTO> tornadoLookupDataDTOs = new ArrayList<TornadoLookupDataDTO>();
    private List<IceworkLookupDataDTO> iceworkLookupDataDTOs = new ArrayList<IceworkLookupDataDTO>();
    
    private NaturalThreatLookupData() {
    }
    
    public static NaturalThreatLookupData get() { 
    	return instance;
    }
	
	public void initEarthquakeLookupData(File userFile) {
        try {
            List<String[]> earthquakeCalcs = DataUtil.createRecords(userFile);
            for (String[] earthquakeCalc : earthquakeCalcs) {
                EarthquakeLookupData earthquakeLookupData = new EarthquakeLookupData();
                earthquakeLookupData.setSeismicZone(earthquakeCalc[0]);
                earthquakeLookupData.setLowerGBoundry(new BigDecimal(earthquakeCalc[1]));
                earthquakeLookupData.setUpperGBoundry(new BigDecimal(earthquakeCalc[2]));
                earthquakeLookupData.setLowerRichter(new BigDecimal(earthquakeCalc[3]));
                earthquakeLookupData.setUpperRichter(new BigDecimal(earthquakeCalc[4]));
                earthquakeLookupData.setPre1988Vulnerability(new BigDecimal(earthquakeCalc[5]));
                earthquakeLookupData.setPost1988Vulnerability(new BigDecimal(earthquakeCalc[6]));
                earthquakeLookupData.setOther(new BigDecimal(earthquakeCalc[7]));
                saveEntity(earthquakeLookupData);
            }

        } catch (Exception e) {
            String message = "Error initializing earthquake lookup data: " + e.getMessage();
            log.error(message, e);
            throw new RuntimeException(message);
        }
    }

    public List<EarthquakeLookupDataDTO> getEarthquakeLookupDataDTOs() {
    	if (earthquakeLookupDataDTOs == null || earthquakeLookupDataDTOs.size() == 0) {
    		List<EarthquakeLookupData> earthquakeLookupDatas = NaturalThreatManager.getEarthquakeLookupData(entityManager());
    		earthquakeLookupDataDTOs = EarthquakeLookupData.createDTOs(earthquakeLookupDatas);
    	}
    	return earthquakeLookupDataDTOs;
    }
    
    public BigDecimal getEarthquakeVulnerability(BigDecimal magnitude, Integer yearBuilt) {
    	log.debug("Checking values (" + magnitude + ", " + yearBuilt + ")");
    	BigDecimal result = BigDecimal.ZERO;
    	for (EarthquakeLookupDataDTO dto : getEarthquakeLookupDataDTOs()) {
    		if (dto.getOther().doubleValue() <= magnitude.doubleValue()) {
                if(yearBuilt != null) {
                    result = yearBuilt < 1988
	    				? dto.getPre1988Vulnerability()
	    				: dto.getPost1988Vulnerability();
                }
                else {
                    result = dto.getPost1988Vulnerability();
                }

    		} else {
    			break;
    		}
    	}
    	log.debug("Returning: " + result);
    	return result;
    }

    public void initHurricaneCalc(File userFile) {
        try {
            List<String[]> hurricaneCalcs = DataUtil.createRecords(userFile);
            for (String[] hurricaneCalc : hurricaneCalcs) {
                HurricaneLookupData data = new HurricaneLookupData();
                data.setCategory(hurricaneCalc[0]);
                data.setLowerWindSpeed(new BigDecimal(hurricaneCalc[1]));
                data.setUpperWindSpeed(new BigDecimal(hurricaneCalc[2]));
                saveEntity(data);
            }
        } catch (Exception e) {
            String message = "Error initializing hurricane lookup data: " + e.getMessage();
            log.error(message, e);
            throw new RuntimeException(message);
        }
    }

    public List<HurricaneLookupDataDTO> getHurricaneLookupDataDTOs() {
    	if (hurricaneLookupDataDTOs == null || hurricaneLookupDataDTOs.isEmpty()) {
	        hurricaneLookupDataDTOs = HurricaneLookupData.createDTOs(
	        		NaturalThreatManager.getHurricaneLookupData(entityManager()));
    	}
    	return hurricaneLookupDataDTOs;
    }

    public void initTornadoCalc(File userFile) {
        try {
            List<String[]> tornadoCalcs = DataUtil.createRecords(userFile);
            for (String[] tornadoCalc : tornadoCalcs) {
                TornadoLookupData data = new TornadoLookupData();
                data.setState(tornadoCalc[0]);
                data.setCounty(tornadoCalc[1]);
                data.setNumberOfTornadoes(Integer.parseInt(tornadoCalc[2]));
                data.setAreaOfCounty(new BigDecimal(tornadoCalc[3]));
                data.setEstimatedTornadoFrequency(new BigDecimal(tornadoCalc[4]));
                saveEntity(data);
            }
        } catch (Exception e) {
            String message = "Error initializing tornadoCalcs: " + e.getMessage();
            log.error(message, e);
            throw new RuntimeException(message);
        }
    }

    public List<TornadoLookupDataDTO> getTornadoLookupDataDTOs() {
    	if (tornadoLookupDataDTOs == null || tornadoLookupDataDTOs.isEmpty()) {
	        tornadoLookupDataDTOs = TornadoLookupData.createDTOs(
	        		NaturalThreatManager.getTornadoLookupData(entityManager()));
    	}
    	return tornadoLookupDataDTOs;
    }

    public void initIceworkCalc(File userFile) {
        try {
            List<String[]> iceworkCalcs = DataUtil.createRecords(userFile);
            for (String[] iceworkCalc : iceworkCalcs) {
                IceworkLookupData data = new IceworkLookupData();
                data.setState(iceworkCalc[0]);
                data.setInitials(iceworkCalc[1]);
                data.setInterval1(new BigDecimal(iceworkCalc[2]));
                data.setInterval2(new BigDecimal(iceworkCalc[3]));
                data.setInterval3(new BigDecimal(iceworkCalc[4]));
                data.setInterval4(new BigDecimal(iceworkCalc[5]));
                data.setInterval5(new BigDecimal(iceworkCalc[6]));
                data.setInterval6(new BigDecimal(iceworkCalc[7]));
                saveEntity(data);
            }
        } catch (Exception e) {
            String message = "Error initializing iceworkStatesCalc: " + e.getMessage();
            log.error(message, e);
            throw new RuntimeException(message);
        }
    }

    public List<IceworkLookupDataDTO> getIceworkLookupDataDTOs() {
    	if (iceworkLookupDataDTOs == null || iceworkLookupDataDTOs.isEmpty()) {
	        iceworkLookupDataDTOs = IceworkLookupData.createDTOs(
	        		NaturalThreatManager.getIceworkLookupData(entityManager()));
    	}
        return iceworkLookupDataDTOs;
    }
    
    public IceworkLookupDataDTO getIceworkLookupDataDTO(String stateCode) {
    	for (IceworkLookupDataDTO dto : getIceworkLookupDataDTOs()) {
    		if (stateCode != null && stateCode.equalsIgnoreCase(dto.getInitials())) {
    			return dto;
    		}
    	}
    	return null;
    }

    public void initWindLookupData(File userFile) {
        try {
            List<String[]> windDataCalcs = DataUtil.createRecords(userFile);
            for (String[] windDataCalc : windDataCalcs) {
                WindLookupData data = new WindLookupData();
                data.setLongitudeAndLatitude(windDataCalc[0]);
                data.setLatitude(windDataCalc[1]);
                data.setLongitude(windDataCalc[2]);
                data.setInterval1(GeneralUtil.createBigDecimal(windDataCalc[3]));
                data.setInterval2(GeneralUtil.createBigDecimal(windDataCalc[4]));
                data.setInterval3(GeneralUtil.createBigDecimal(windDataCalc[5]));
                data.setInterval4(GeneralUtil.createBigDecimal(windDataCalc[6]));
                data.setInterval5(GeneralUtil.createBigDecimal(windDataCalc[7]));
                data.setInterval6(GeneralUtil.createBigDecimal(windDataCalc[8]));
                saveEntity(data);
            }
        } catch (Exception e) {
            String message = "Error initializing windDataCalc: " + e.getMessage();
            log.error(message, e);
            throw new RuntimeException(message);
        }
    }
    
    public WindLookupDataDTO getWindLookup(BigDecimal latitude, BigDecimal longitude) {
    	if (latitude != null && longitude != null) {
	    	Integer concatenatedLocation = 
	                       Integer.valueOf(String.valueOf(Math.abs(longitude.intValue())) +
	                                                       String.valueOf(Math.abs(latitude.intValue())));
	    	if (log.isDebugEnabled()) { log.debug("Converted lat/long: " + latitude + "/" + longitude + " to: " + concatenatedLocation); }
	       return (NaturalThreatManager.getWindLookupData(concatenatedLocation, entityManager())).copyInto(new WindLookupDataDTO());
    	}
    	return WindLookupDataDTO.createEmpty();
    }
    
	public BigDecimal getTornadoVulnerability(String zipCode) {
		ZipLookupData zipCodeData;
		try {
			zipCodeData = (ZipLookupData)entityManager()
					.createQuery("select zld from ZipLookupData zld where zld.zipCode = :zipCode")
						.setParameter("zipCode", zipCode)
						.setMaxResults(1)
						.getSingleResult();
		} catch (Exception ex) {
			log.debug("Could not lookup tornado value for zip code: " + zipCode, ex);
			throw new IllegalArgumentException("No tornado data found for zip code : (" + zipCode + ")");
		}
		return getTornadoVulnerability(zipCodeData.getCounty(), zipCodeData.getStateCode());
	}
	
	public BigDecimal getTornadoVulnerability(String countyName, String stateCode) {
		try {
			TornadoLookupData tld = NaturalThreatManager.findTornadoDataByCountyState(countyName, stateCode, entityManager());
			return tld.getEstimatedTornadoFrequency();
		} catch (Exception ex) {
            //Many counties in database accidentally have a space at the end of their name.
            try {
                TornadoLookupData tld = NaturalThreatManager.findTornadoDataByCountyState(countyName + " ", stateCode, entityManager());
                return tld.getEstimatedTornadoFrequency();
            } catch (Exception ex2) {
                log.debug("Could not lookup tornado value for county/state: " + countyName + ", " + stateCode, ex);
			    throw new IllegalArgumentException("No tornado data found for county/state: (" + countyName + ", " + stateCode + ")");
            }
		}
	}
    
    private void saveEntity(BaseEntity baseEntity) {
        baseEntity.setId(null);
        EntityManager entityManager = entityManager();
        entityManager.persist(baseEntity);
        entityManager.flush();
    }
    
    private EntityManager entityManager() {
        return ServiceInvocationContext.getCurrentInstance().getEntityManager();
    }
}
