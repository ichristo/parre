/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.server.nav;

import org.apache.log4j.Logger;
import org.parre.shared.nav.*;

import java.io.Serializable;
import java.util.*;

/**
 * User: Swarn S. Dhaliwal
 * Date: 7/30/11
 * Time: 2:37 PM
*/
public class NavDef implements Serializable {
    private static transient final Logger log= Logger.getLogger(NavDef.class);
    private String name;
    private StartStepDef startStep;
    private Map<String, NavStepDef> navSteps;
    private EndStepDef endStep;

    public NavDef(String defName) {
        this.name = defName;
        navSteps = new HashMap<String, NavStepDef>();
    }

    public String getName() {
        return name;
    }

    public StartStepDef getStartStep() {
        return startStep;
    }

    public void setStartStep(StartStepDef startStep) {
        this.startStep = startStep;
    }

    public EndStepDef getEndStep() {
        return endStep;
    }

    public void setEndStep(EndStepDef endStep) {
        this.endStep = endStep;
    }

    public void addNavStep(NavStepDef navStep) {
        if (navSteps.containsKey(navStep.getName())) {
            throw new IllegalArgumentException("Attempt to add duplicate step name : " + navStep.getName());
        }
        if (navStep.isStartStep()) {
            startStep = (StartStepDef) navStep;
        }
        else if (navStep.isEndStep()) {
            endStep = (EndStepDef) navStep;
        }
        navSteps.put(navStep.getName(), navStep);
    }

    @Override
    public String toString() {
        return "NavDef{" +
                "name='" + name + '\'' +
                ", startStep=" + startStep +
                ", navSteps=" + navSteps +
                ", endStep=" + endStep +
                '}';
    }

    public Nav createNav() {
        Nav nav = new Nav();
        Collection<NavStepDef> values = navSteps.values();
        for (NavStepDef value : values) {
            nav.addNavStep(value.createNavStep());
        }

        if (log.isDebugEnabled()) { log.debug("Before Populating next steps " + nav); }
        Collection<NavStep> navSteps1 = nav.getNavSteps();
        for (NavStep navStep : navSteps1) {
            NavStepDef navStepDef = navSteps.get(navStep.getName());
            List<String> nextStepNames = navStepDef.getNextSteps();
            List<NavStep> nextSteps = new ArrayList<NavStep>(nextStepNames.size());
            for (String nextStepName : nextStepNames) {
                nextSteps.add(nav.getNavStep(nextStepName));
            }
            navStep.setNextSteps(nextSteps);
        }
        if (log.isDebugEnabled()) { log.debug("After completion : " + nav); }
        return nav;
    }


}
