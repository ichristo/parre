/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.server.nav;

import org.apache.log4j.Logger;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;
import org.parre.shared.nav.EndStepDef;
import org.parre.shared.nav.NavStepDef;
import org.parre.shared.nav.StartStepDef;

import java.io.File;
import java.util.*;

/**
 * User: Swarn S. Dhaliwal
 * Date: 7/30/11
 * Time: 2:57 PM
*/
public class NavDefFactory {
    private static transient final Logger log = Logger.getLogger(NavDefFactory.class);

    private Map<String, NavDef> navDefs;

    private static NavDefFactory instance = new NavDefFactory();

    public void initialize(File[] defFiles) throws Exception {
    	if (log.isDebugEnabled()) { log.debug("Initializing NavDef using files : " + Arrays.asList(defFiles)); }
        navDefs = new HashMap<String, NavDef>(defFiles.length);
        SAXBuilder builder = new SAXBuilder();
        for (File defFile : defFiles) {
            Document defDoc = builder.build(defFile);
            NavDef def = createNavDef(defDoc);
            if (log.isDebugEnabled()) { log.debug("Created NavDef : " + def.getName()); }
            navDefs.put(def.getName(), def);
        }
    }

    private NavDef createNavDef(Document defDoc) {
        Element rootElement = defDoc.getRootElement();
        String defName = rootElement.getAttributeValue("name");
        NavDef navDef = new NavDef(defName);
        List<Element> steps = rootElement.getChildren();
        for (Element step : steps) {
            navDef.addNavStep(createStep(step));
        }
        return navDef;
    }

    private NavStepDef createStep(Element step) {
        if ("start".equals(step.getName())) {
            return new StartStepDef(step.getAttributeValue("name"), step.getAttributeValue("label"));
        } else if ("end".equals(step.getName())) {
            return new EndStepDef();
        } else {
            return createNavStep(step);
        }
    }

    private NavStepDef createNavStep(Element step) {
        String name = step.getAttributeValue("name");
        String label = step.getAttributeValue("label");
        List<Element> nextStepElements = step.getChild("next-steps").getChildren();
        List<String> nextSteps = new ArrayList<String>(nextStepElements.size());
        for (Element nextStepElement : nextStepElements) {
            nextSteps.add(nextStepElement.getText());
        }
        return new NavStepDef(name, label, nextSteps);
    }

    public static NavDefFactory getInstance() {
        return instance;
    }

    public NavDef getNavDef(String defName) {
        return navDefs.get(defName);
    }
}
