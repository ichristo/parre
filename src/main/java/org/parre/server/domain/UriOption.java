/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.server.domain;


import javax.persistence.*;

import org.parre.shared.UriOptionDTO;

import java.math.BigDecimal;

/**
 * User: Swarn S. Dhaliwal
 * Date: 7/14/11
 * Time: 11:20 AM
*/
@Entity
@Table(name = "URI_OPTION")
public class UriOption extends BaseEntity {
    @ManyToOne(targetEntity = UriQuestion.class)
    @JoinColumn(name = "QUESTION_ID")
    private UriQuestion question;
    @Column(name = "NAME")
    private String name;
    @Column(name = "VALUE", columnDefinition = "DECIMAL(5,2)")
    private BigDecimal value;
    @Column(name = "WEIGHT", columnDefinition = "DECIMAL(5,2)")
    private BigDecimal weight;

    public UriOption() {
    }

    public UriOption(String name, BigDecimal value, BigDecimal weight) {
        this.name = name;
        this.value = value;
        this.weight = weight;
    }

    public UriQuestion getQuestion() {
        return question;
    }

    public void setQuestion(UriQuestion question) {
        this.question = question;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getValue() {
        return value;
    }

    public void setValue(BigDecimal value) {
        this.value = value;
    }

    public BigDecimal getWeight() {
        return weight;
    }

    public void setWeight(BigDecimal weight) {
        this.weight = weight;
    }

    public UriOptionDTO createDTO() {
        return copyInto(new UriOptionDTO());
    }
}
