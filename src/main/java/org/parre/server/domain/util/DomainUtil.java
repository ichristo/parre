/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.server.domain.util;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.log4j.Logger;
import org.parre.server.domain.BaseEntity;
import org.parre.shared.BaseDTO;

import java.util.Collections;
import java.util.List;

/**
 * .
 * User: Swarn S. Dhaliwal
 * Date: 11/5/11
 * Time: 10:51 AM
 */
public class DomainUtil {
        private transient static final Logger log = Logger.getLogger(DomainUtil.class);
    private static final List nullIdList = Collections.singletonList(new Long(0));

    public static List nullIdList() {
        return nullIdList;
    }

    public static boolean haveSameIdentity(BaseEntity entity, BaseDTO dto) {
        if (entity == null) {
            return false;
        }
        return entity.hasSameIdentity(dto);
    }


    public static void copyProperties(Object dst, Object src) {
        try {
            PropertyUtils.copyProperties(dst, src);
        }
        catch (Exception e) {
            log.info("Error copying properties from : " + src + " to " + dst);
        }

    }

    public static String sqlNumberList(Object[] array) {
        StringBuilder builder = new StringBuilder();
        if (array.length > 0) {
            builder.append(array[0]);
            for (int i = 1; i < array.length; i++) {
                builder.append(",").append(array[i]);
            }
        }
        return builder.toString();
    }

    public static String sqlStringList(Object[] array) {
        StringBuilder builder = new StringBuilder();
        if (array.length > 0) {
            builder.append("'").append(array[0]).append("'");
            for (int i = 1; i < array.length; i++) {
                builder.append(", '").append(array[i]).append("'");
            }
        }
        return builder.toString();
    }

}
