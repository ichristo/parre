/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.server.domain.util;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.log4j.Logger;

import java.io.Serializable;

/**
 * The Class DataObject.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 11/15/11
 */
public abstract class DataObject implements Serializable {
    private static transient final Logger log = Logger.getLogger(DataObject.class);

    public static void copyProperties(Object dest, Object src) {
        try {
            PropertyUtils.copyProperties(dest, src);
        } catch (Exception e) {
            log.error("Error copying properties : dest " + dest + ", src=" + src, e);
        }
    }

    public <T> T copyInto(T t) {
        copyProperties(t, this);
        return t;
    }

    public <T> void copyFrom(T t) {
        copyProperties(this, t);
    }

}
