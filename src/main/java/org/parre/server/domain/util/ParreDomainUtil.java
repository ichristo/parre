/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.server.domain.util;

import org.parre.server.domain.AssetThreatEntity;

/**
 * The Class ParreDomainUtil.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 2/23/12
 */
public class ParreDomainUtil {
    public static boolean equal(AssetThreatEntity e1, AssetThreatEntity e2) {
        return e1.getAsset().equals(e2.getAsset()) &&
                e1.getThreat().equals(e2.getThreat());
    }

    public static String createUniqueKey(AssetThreatEntity e) {
        return new StringBuilder().append(e.getAsset().getId()).
                append("-").append(e.getThreat().getId()).toString();
    }
}
