/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.server.domain;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.parre.shared.BaseDTO;
import org.parre.shared.ProxyTierDTO;

import java.util.ArrayList;
import java.util.List;

/**
 * The Class ProxyTier.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 2/3/12
 */
@Entity
@Table(name = "PROXY_TIER", uniqueConstraints =
        {@UniqueConstraint(columnNames = {"TIER_NUMBER", "NUMBER_OF_CITIES"})})
public class ProxyTier extends BaseEntity {
    @Column(name = "TIER_NUMBER")
    private Integer tierNumber;
    @Column(name = "NUMBER_OF_CITIES")
    private Integer numberOfCities;

    public Integer getTierNumber() {
        return tierNumber;
    }

    public void setTierNumber(Integer tierNumber) {
        this.tierNumber = tierNumber;
    }

    public Integer getNumberOfCities() {
        return numberOfCities;
    }

    public void setNumberOfCities(Integer numberOfCities) {
        this.numberOfCities = numberOfCities;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("ProxyTier");
        sb.append("{tierNumber=").append(tierNumber);
        sb.append(", numberOfCities=").append(numberOfCities);
        sb.append('}');
        return sb.toString();
    }

    public static List<ProxyTierDTO> createDTOs(List<ProxyTier> entities) {
        List<ProxyTierDTO> dtos = new ArrayList<ProxyTierDTO>(entities.size());
        for (ProxyTier entity : entities) {
            dtos.add(entity.copyInto(new ProxyTierDTO()));
        }
        return dtos;
    }
}
