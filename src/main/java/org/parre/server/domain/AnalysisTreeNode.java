/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.server.domain;


import javax.persistence.*;
import javax.validation.constraints.NotNull;

import org.apache.log4j.Logger;
import org.bouncycastle.crypto.RuntimeCryptoException;
import org.parre.shared.AnalysisTreeNodeDTO;
import org.parre.shared.types.AnalysisTreeType;

import java.math.BigDecimal;
import java.util.*;

/**
 * The Class AnalysisTreeNode.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 1/17/12
 */
@Entity
@Table(name = "ANALYSIS_TREE_NODE")
public class AnalysisTreeNode extends NameDescription {
	private final static transient Logger log = Logger.getLogger(AnalysisTreeNode.class);
    @Column(name = "SUCCESS")
    private Boolean success;
    @Column(name = "LEAF")
    private Boolean leaf;
    @Column(name = "PROBABILITY", columnDefinition = "DECIMAL(21,20)")
    private BigDecimal probability;
    @ManyToOne(targetEntity = AnalysisTreeNode.class)
    @JoinColumn(name = "PARENT_ID")
    private AnalysisTreeNode parent;
    @OneToMany(mappedBy = "parent", cascade = {CascadeType.REMOVE} )
    private Set<AnalysisTreeNode> children = Collections.emptySet();

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public Boolean getLeaf() {
        return leaf;
    }

    public void setLeaf(Boolean leaf) {
        this.leaf = leaf;
    }

    public BigDecimal getProbability() {
        return probability;
    }

    public void setProbability(BigDecimal probability) {
        this.probability = probability;
    }

    public AnalysisTreeNode getParent() {
        return parent;
    }

    public void setParent(AnalysisTreeNode parent) {
        this.parent = parent;
    }

    public Set<AnalysisTreeNode> getChildren() {
        return children;
    }

    public void setChildren(Set<AnalysisTreeNode> children) {
        this.children = children;
    }

    public void addChild(AnalysisTreeNode childNode) {
        if (children.isEmpty()) {
            children = new HashSet<AnalysisTreeNode>();
        }
        childNode.setParent(this);
        children.add(childNode);
    }

    public AnalysisTreeNodeDTO createDTO() {
        return copyInto(new AnalysisTreeNodeDTO());
    }
    public AnalysisTreeNodeDTO buildTreeDTO() {
        return populateChildren(createDTO());
    }

    private AnalysisTreeNodeDTO populateChildren(AnalysisTreeNodeDTO rootDTO) {
    	// Moving children to failure node, making some assumptions about data integrity here...(If there are children, there is exactly 1 success and 1 failure)
    	if (getLeafChildNodes() != null && getLeafChildNodes().size() > 0 && getNonLeafChildNodes() != null && getNonLeafChildNodes().size() > 0) {
	    	AnalysisTreeNode leafNode = getLeafChildNodes().get(0);
	    	AnalysisTreeNode childNode = getNonLeafChildNodes().get(0);
	    	
	    	AnalysisTreeNodeDTO leafDTO = leafNode.createDTO();
	    	AnalysisTreeNodeDTO childDTO = childNode.createDTO();
	    	
	    	if (leafDTO.getSuccess() == true) {
	    		rootDTO.addChildDTO(leafDTO);  // Success first..
	    		rootDTO.addChildDTO(childDTO);
	    		childNode.populateChildren(childDTO);
	    		
	    		childDTO.setLeaf(false);
	    		leafDTO.setLeaf(true);
	    		
	    		if (childDTO.hasChildren()) {
	    			childDTO.sort(childDTO.getChildDTOs());
	    		}
	    	} else {
	    		rootDTO.addChildDTO(childDTO);  // Success first..
	    		rootDTO.addChildDTO(leafDTO);
	    		childNode.populateChildren(leafDTO);
	    		
	    		childDTO.setLeaf(true);
	    		leafDTO.setLeaf(false);
	    		
	    		if (leafDTO.hasChildren()) {
	    			leafDTO.sort(leafDTO.getChildDTOs());
	    		}
	    	}
	    	childDTO.setParentDTO(rootDTO);
	    	leafDTO.setParentDTO(rootDTO);
    	}
        return rootDTO;
    }

    private List<AnalysisTreeNode> getLeafChildNodes() {
        List<AnalysisTreeNode> leafChildNodes = new ArrayList<AnalysisTreeNode>();
        for(AnalysisTreeNode treeNode : children) {
            if(treeNode.getLeaf()) {
                leafChildNodes.add(treeNode);
            }
        }
        return leafChildNodes;
    }

    private List<AnalysisTreeNode> getNonLeafChildNodes() {
        List<AnalysisTreeNode> nonLeafChildNodes = new ArrayList<AnalysisTreeNode>();
        for(AnalysisTreeNode treeNode : children) {
            if(!treeNode.getLeaf()) {
                nonLeafChildNodes.add(treeNode);
            }
        }
        return nonLeafChildNodes;
    }

    public static List<AnalysisTreeNodeDTO> createDTOs(List<AnalysisTreeNode> entities) {
        List<AnalysisTreeNodeDTO> dtos = new ArrayList<AnalysisTreeNodeDTO>(entities.size());
        for (AnalysisTreeNode entity : entities) {
            dtos.add(entity.createDTO());
        }
        return dtos;
    }

    public AnalysisTreeNode makeCopy() {
        AnalysisTreeNode copyNode = new AnalysisTreeNode();
        copyNode.copyFrom(this);
        if (isRootNode()) {
            copyNode.copyName();
        }
        copyNode.setId(null);
        copyNode.setParent(null);
        copyNode.setChildren(Collections.<AnalysisTreeNode>emptySet());
        Set<AnalysisTreeNode> chidNodes = getChildren();
        for (AnalysisTreeNode chidNode : chidNodes) {
            AnalysisTreeNode childCopy = chidNode.makeCopy();
            copyNode.addChild(childCopy);
        }
        return copyNode;
    }

    private boolean isRootNode() {
        return parent == null;
    }

    private void copyName() {
        setName(getName() + " - Copy");
    }
}
