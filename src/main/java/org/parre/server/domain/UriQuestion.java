/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.server.domain;


import javax.persistence.*;

import org.parre.shared.UriQuestionDTO;
import org.parre.shared.types.UriQuestionType;

import java.util.*;

/**
 * User: Swarn S. Dhaliwal
 * Date: 7/14/11
 * Time: 11:20 AM
*/
@Entity
@Table(name = "URI_QUESTION")
public class UriQuestion extends NameDescription {
    @Column(name = "TYPE") @Enumerated(EnumType.STRING)
    private UriQuestionType type;
    @OneToMany(mappedBy = "question")
    @OrderBy("value")
    private Set<UriOption> options = Collections.emptySet();

    public UriQuestion() {
    }

    public UriQuestion(UriQuestionType questionType, String name, String description) {
        super(name, description);
        this.type = questionType;
    }

    public UriQuestionType getType() {
        return type;
    }

    public void setType(UriQuestionType type) {
        this.type = type;
    }

    public Set<UriOption> getOptions() {
        return options;
    }

    public void setOptions(Set<UriOption> options) {
        this.options = options;
    }

    public void addOption(UriOption option) {
        if (options.isEmpty()) {
            options = new HashSet<UriOption>();
        }
        option.setQuestion(this);
        options.add(option);
    }

    private UriQuestionDTO createDTO() {
        UriQuestionDTO uriQuestionDTO = copyInto(new UriQuestionDTO());
        for (UriOption option : options) {
            uriQuestionDTO.addOptionDTO(option.createDTO());
        }
        return uriQuestionDTO;
    }

    public static List<UriQuestionDTO> createDTOs(List<UriQuestion> entities) {
        List<UriQuestionDTO> dtos = new ArrayList<UriQuestionDTO>(entities.size());
        for (UriQuestion entity : entities) {
            dtos.add(entity.createDTO());
        }
        return dtos;

    }
}
