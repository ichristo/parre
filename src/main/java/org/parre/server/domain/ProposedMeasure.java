/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.server.domain;


import javax.persistence.*;

import org.parre.shared.AmountDTO;
import org.parre.shared.ProposedMeasureDTO;
import org.parre.shared.types.CounterMeasureType;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * The Class ProposedMeasure.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 1/19/12
 */
@Entity
@Table(name = "PROPOSED_MEASURE")
public class ProposedMeasure extends NameDescription {
    @ManyToOne(targetEntity = ParreAnalysis.class)
    @JoinColumn(name = "BASELINE_PARRE_ANALYSIS_ID")
    private ParreAnalysis baselineParreAnalysis;

    @Column(name = "TYPE") @Enumerated(EnumType.STRING)
    private CounterMeasureType type;
    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name = "quantity", column = @Column(name = "CC_QUANTITY", columnDefinition = "DECIMAL(10,2)")),
            @AttributeOverride(name = "unit", column = @Column(name = "CC_UNIT"))
    })

    private Amount capitalCost;
    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name = "quantity", column = @Column(name = "OAM_QUANTITY", columnDefinition = "DECIMAL(10,2)")),
            @AttributeOverride(name = "unit", column = @Column(name = "OAM_UNIT"))
    })

    private Amount oAndMCostPerYear;
    @Column(name = "EFFECTIVE_LIFE")
    private Integer effectiveLife;
    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name = "quantity", column = @Column(name = "SV_QUANTITY", columnDefinition = "DECIMAL(10,2)")),
            @AttributeOverride(name = "unit", column = @Column(name = "SV_UNIT"))
    })
    private Amount salvageValue;
    @Column(name = "INFLATION_RATE", columnDefinition = "DECIMAL(4,3)")
    private BigDecimal inflationRate;

    public ProposedMeasure() {
    }

    public ProposedMeasure(String name, String description) {
        super(name, description);
    }

    public ParreAnalysis getBaselineParreAnalysis() {
        return baselineParreAnalysis;
    }

    public void setBaselineParreAnalysis(ParreAnalysis baselineParreAnalysis) {
        this.baselineParreAnalysis = baselineParreAnalysis;
    }

    public CounterMeasureType getType() {
        return type;
    }

    public void setType(CounterMeasureType type) {
        this.type = type;
    }

    public Amount getCapitalCost() {
        return capitalCost;
    }

    public void setCapitalCost(Amount capitalCost) {
        this.capitalCost = capitalCost;
    }

    public AmountDTO getCapitalCostDTO() {
        return capitalCost.createDTO();
    }

    public void setCapitalCostDTO(AmountDTO amountDTO) {
        this.capitalCost = new Amount(amountDTO);
    }

    public Amount getoAndMCostPerYear() {
        return oAndMCostPerYear;
    }

    public void setoAndMCostPerYear(Amount oAndMCostPerYear) {
        this.oAndMCostPerYear = oAndMCostPerYear;
    }

    public AmountDTO getoAndMCostPerYearDTO() {
        return oAndMCostPerYear.createDTO();
    }

    public void setoAndMCostPerYearDTO(AmountDTO amountDTO) {
        this.oAndMCostPerYear = new Amount(amountDTO);
    }

    public Integer getEffectiveLife() {
        return effectiveLife;
    }

    public void setEffectiveLife(Integer effectiveLife) {
        this.effectiveLife = effectiveLife;
    }

    public Amount getSalvageValue() {
        return salvageValue;
    }

    public void setSalvageValue(Amount salvageValue) {
        this.salvageValue = salvageValue;
    }

    public AmountDTO getSalvageValueDTO() {
        return salvageValue.createDTO();
    }

    public void setSalvageValueDTO(AmountDTO amountDTO) {
        this.salvageValue = new Amount(amountDTO);
    }

    public BigDecimal getInflationRate() {
        return inflationRate;
    }

    public void setInflationRate(BigDecimal inflationRate) {
        this.inflationRate = inflationRate;
    }

    public ProposedMeasureDTO createDTO() {
        ProposedMeasureDTO dto = copyInto(new ProposedMeasureDTO());
        return dto;
    }

    public static List<ProposedMeasureDTO> createDTOs(List<ProposedMeasure> entities) {
        List<ProposedMeasureDTO> dtos = new ArrayList<ProposedMeasureDTO>(entities.size());
        for (ProposedMeasure entity : entities) {
            dtos.add(entity.copyInto(new ProposedMeasureDTO()));
        }
        return dtos;

    }
}
