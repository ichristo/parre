/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.server.domain;

import javax.persistence.*;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/**
 * The Class Org.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 11/18/11
 */
@Entity
@Table(name="ORG")
public class Org extends NameDescription {
    @OneToOne(targetEntity = Contact.class)
    @JoinColumn(name = "CONTACT_ID")
    private Contact contact;
    @OneToOne(targetEntity = Address.class)
    @JoinColumn(name = "ADDRESS_ID")
    private Address address;
    @OneToMany(targetEntity = Asset.class)
    @JoinColumn(name = "ORG_ID")
    private Set<Asset> assets = Collections.emptySet();
    public Contact getContact() {
        return contact;
    }

    public void setContact(Contact contact) {
        this.contact = contact;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public Set<Asset> getAssets() {
        return assets;
    }

    public void setAssets(Set<Asset> productServices) {
        this.assets = productServices;
    }

    public Asset findAsset(String assetId) {
        for (Asset asset : assets) {
            if (asset.getAssetId().equals(assetId)) {
                return asset;
            }
        }
        return null;
    }

    public void addAsset(Asset asset) {
        if (assets.isEmpty()) {
            assets = new HashSet<Asset>();
        }
        assets.add(asset);
    }
}
