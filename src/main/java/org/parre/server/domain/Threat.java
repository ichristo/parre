/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.server.domain;


import javax.persistence.*;

import org.parre.shared.ThreatDTO;
import org.parre.shared.types.ThreatCategoryType;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * User: Swarn S. Dhaliwal
 * Date: 7/14/11
 * Time: 11:20 AM
*/
@Entity
@Table(name = "THREAT")
public class Threat extends NameDescription {
    @Column(name = "HAZARD_TYPE")
    private String hazardType = "";
    @Column(name = "THREAT_CATEGORY") @Enumerated(value = EnumType.STRING)
    private ThreatCategoryType threatCategory = ThreatCategoryType.NONE;
    @Column(name = "IS_USER_DEFINED")
    private Boolean userDefined = Boolean.FALSE;
    @Column(name = "BASELINE_ID")
    private Long baselineId;
    @OneToMany(mappedBy = "threat")
    private Set<ThreatNote> threatNotes;

    public String getHazardType() {
        return hazardType;
    }

    public void setHazardType(String hazardType) {
        this.hazardType = hazardType;
    }

    public ThreatCategoryType getThreatCategory() {
        return threatCategory;
    }

    public void setThreatCategory(ThreatCategoryType threatCategory) {
        this.threatCategory = threatCategory;
    }


    public Boolean getUserDefined() {
        return userDefined;
    }

    public void setUserDefined(Boolean userDefined) {
        this.userDefined = userDefined;
    }


    @Override
    public String toString() {
        return "Threat{" +
                "hazardType='" + hazardType + '\'' +
                ", threatCategory=" + threatCategory +
                ", userDefined=" + userDefined +
                ", baselineId=" + baselineId +
                '}';
    }

    public static List<ThreatDTO> createDTOs(List<Threat> entities) {
        List<ThreatDTO> dtos = new ArrayList<ThreatDTO>(entities.size());
        for (Threat entity : entities) {
            dtos.add(entity.copyInto(new ThreatDTO()));
        }
        return dtos;
    }

    public static List<ThreatDTO> createDTOs(List<Threat> entities, long baselineId) {
        List<ThreatDTO> dtos = new ArrayList<ThreatDTO>();
        for(Threat entity:entities) {
            if(entity.getBaselineId().equals(-1L) || entity.getBaselineId().equals(baselineId)) {
                dtos.add(entity.copyInto(new ThreatDTO()));
            }
        }
        return dtos;
    }

    public Long getBaselineId() {
    	return baselineId != null ? baselineId : -1l;
    }

    public void setBaselineId(Long baselineId) {
        this.baselineId = baselineId;
    }

    public Set<ThreatNote> getThreatNotes() {
        return threatNotes;
    }

    public void setThreatNotes(Set<ThreatNote> threatNotes) {
        this.threatNotes = threatNotes;
    }
}
