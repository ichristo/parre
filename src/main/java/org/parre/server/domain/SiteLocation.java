/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.server.domain;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.parre.shared.LabelValueDTO;
import org.parre.shared.NameDescriptionDTO;
import org.parre.shared.SiteLocationDTO;

/**
 * User: keithjones
 * Date: 3/26/12
 * Time: 1:44 PM
*/
@Entity
@Table(name = "SITE_LOCATION")
public class SiteLocation extends BaseEntity {
    @Column(name = "NAME")
    private String name;
    @Column(name = "ADDRESS")
    private String address;
    @Column(name = "CITY")
    private String city;
    @Column(name = "STATE")
    private String state;
    @Column(name = "ZIP_CODE")
    private String zipCode;

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    @Override
    public String toString() {
        return "SiteInformationDTO{" +
                "name='" + name + '\'' +
                ", address='" + address + '\'' +
                ", city='" + city + '\'' +
                ", state='" + state + '\'' +
                ", zipCode='" + zipCode + '\'' +
                '}';
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public SiteLocationDTO createDTO() {
        return this.copyInto(new SiteLocationDTO());
    }

    public LabelValueDTO createLabelValueDTO() {
        return new LabelValueDTO(getName(), String.valueOf(getId()));
    }

    public NameDescriptionDTO createNameDescriptionDTO() {
        NameDescriptionDTO nameDescriptionDTO = new NameDescriptionDTO(getId());
        nameDescriptionDTO.setName(getName());
        nameDescriptionDTO.setDescription(getAddress());
        return nameDescriptionDTO;
    }
}
