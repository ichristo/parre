/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.server.domain;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.parre.shared.BaseDTO;
import org.parre.shared.EarthquakeLookupDataDTO;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * User: keithjones
 * Date: 1/26/12
 * Time: 11:06 AM
*/
@Entity
@Table (name = "EARTHQUAKE_LOOKUP_DATA")
public class EarthquakeLookupData extends BaseEntity {
    @Column(name = "SEISMIC_ZONE")
    private String seismicZone;
    @Column(name = "LOWER_G_BOUNDRY", columnDefinition = "DECIMAL(10,2)")
    private BigDecimal lowerGBoundry;
    @Column(name = "UPPER_G_BOUNDRY", columnDefinition = "DECIMAL(10,2)")
    private BigDecimal upperGBoundry;
    @Column(name = "LOWER_RICHTER", columnDefinition = "DECIMAL(10,2)")
    private BigDecimal lowerRichter;
    @Column(name = "UPPER_RICHTER", columnDefinition = "DECIMAL(10,2)")
    private BigDecimal upperRichter;
    @Column(name = "PRE_1988_VULNERABILITY", columnDefinition = "DECIMAL(10,2)")
    private BigDecimal pre1988Vulnerability;
    @Column(name = "POST_1988_VULNERABILITY", columnDefinition = "DECIMAL(10,2)")
    private BigDecimal post1988Vulnerability;
    @Column(name = "OTHER", columnDefinition = "DECIMAL(10,2)")
    private BigDecimal other;

    public String getSeismicZone() {
        return seismicZone;
    }

    public void setSeismicZone(String seismicZone) {
        this.seismicZone = seismicZone;
    }

    public BigDecimal getLowerGBoundry() {
        return lowerGBoundry;
    }

    public void setLowerGBoundry(BigDecimal lowerGBoundry) {
        this.lowerGBoundry = lowerGBoundry;
    }

    public BigDecimal getUpperGBoundry() {
        return upperGBoundry;
    }

    public void setUpperGBoundry(BigDecimal upperGBoundry) {
        this.upperGBoundry = upperGBoundry;
    }

    public BigDecimal getLowerRichter() {
        return lowerRichter;
    }

    public void setLowerRichter(BigDecimal lowerRichter) {
        this.lowerRichter = lowerRichter;
    }

    public BigDecimal getUpperRichter() {
        return upperRichter;
    }

    public void setUpperRichter(BigDecimal upperRichter) {
        this.upperRichter = upperRichter;
    }

    public BigDecimal getPre1988Vulnerability() {
        return pre1988Vulnerability;
    }

    public void setPre1988Vulnerability(BigDecimal pre1988Vulnerability) {
        this.pre1988Vulnerability = pre1988Vulnerability;
    }

    public BigDecimal getPost1988Vulnerability() {
        return post1988Vulnerability;
    }

    public void setPost1988Vulnerability(BigDecimal post1988Vulnerability) {
        this.post1988Vulnerability = post1988Vulnerability;
    }

    public BigDecimal getOther() {
        return other;
    }

    public void setOther(BigDecimal other) {
        this.other = other;
    }

    public static List<EarthquakeLookupDataDTO> createDTOs(List<EarthquakeLookupData> entities) {
        List<EarthquakeLookupDataDTO> dtos = new ArrayList<EarthquakeLookupDataDTO>(entities.size());
        for (EarthquakeLookupData entity : entities) {
            dtos.add(entity.copyInto(new EarthquakeLookupDataDTO()));
        }
        return dtos;
    }

    @Override
    public String toString() {
        return "EarthquakeCalcs{" +
                "seismicZone='" + seismicZone + '\'' +
                ", lowerGBoundry=" + lowerGBoundry +
                ", upperGBoundry=" + upperGBoundry +
                ", lowerRichter=" + lowerRichter +
                ", upperRichter=" + upperRichter +
                ", pre1988Vulnerability=" + pre1988Vulnerability +
                ", post1988Vulnerability=" + post1988Vulnerability +
                ", other=" + other +
                '}';
    }
}
