/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.server.domain;


import javax.persistence.*;

import org.parre.shared.AssetThreatLevelDTO;
import org.parre.shared.AssetThreatLevelsDTO;

import java.util.ArrayList;
import java.util.List;

/**
 * The Class AssetThreatLevel.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 10/12/11
 */
@Entity
@Table(name = "ASSET_THREAT_LEVEL",
        uniqueConstraints = @UniqueConstraint(columnNames = {"PARRE_ANALYSIS_ID", "ASSET_ID", "THREAT_ID"}))
public class AssetThreatLevel extends BaseEntity {
    @Column(name = "PARRE_ANALYSIS_ID")
    private Long parreAnalysisId;
    @Column(name = "ASSET_ID")
    private Long assetId;
    @Column(name = "THREAT_ID")
    private Long threatId;
    @Column(name = "LEVEL")
    private Integer level;

    public AssetThreatLevel() {
    }

    public AssetThreatLevel(Long parreAnalysisId, AssetThreatLevelDTO levelDTO) {
        this.parreAnalysisId = parreAnalysisId;
        this.assetId = levelDTO.getAssetId();
        this.threatId = levelDTO.getThreatId();
    }

    public AssetThreatLevel(Long threatId, Integer level) {
        this.threatId = threatId;
        this.level = level;
    }

    public Long getParreAnalysisId() {
        return parreAnalysisId;
    }

    public void setParreAnalysisId(Long parreAnalysisId) {
        this.parreAnalysisId = parreAnalysisId;
    }

    public Long getAssetId() {
        return assetId;
    }

    public void setAssetId(Long assetId) {
        this.assetId = assetId;
    }

    public Long getThreatId() {
        return threatId;
    }

    public void setThreatId(Long threatId) {
        this.threatId = threatId;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public static List<AssetThreatLevelsDTO> createDTOs(List<AssetThreatLevel> entities) {
        List<AssetThreatLevelsDTO> dtos = new ArrayList<AssetThreatLevelsDTO>(entities.size());
        for (AssetThreatLevel entity : entities) {
            dtos.add(entity.copyInto(new AssetThreatLevelsDTO()));
        }
        return dtos;
    }

    public boolean isForAssetThreat(AssetThreatEntity assetThreatEntity) {
        return isForAssetThreat(assetThreatEntity.getAsset().getId(), assetThreatEntity.getThreat().getId());
    }

    public boolean isForAssetThreat(Long assetId, Long threatId) {
        return this.assetId.equals(assetId) && this.threatId.equals(threatId);
    }

    public AssetThreatLevel createCopy() {
        return new AssetThreatLevel(threatId, level);
    }
}
