/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.server.domain;

import java.math.BigDecimal;

import javax.persistence.Embeddable;

import org.parre.server.domain.util.DataObject;
import org.parre.shared.UnitPriceDTO;


/**
 * The Class UnitPrice.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 9/22/11
 */
@Embeddable
public class UnitPrice extends DataObject {
    private BigDecimal price;
    private Integer quantity;

    public UnitPrice() {
    }

    public UnitPrice(UnitPriceDTO dto) {
        this(dto.getPrice(), dto.getQuantity());
    }

    public UnitPrice(BigDecimal price, Integer quantity) {
        this.price = price;
        this.quantity = quantity;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public Integer getQuantity() {
        return quantity;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UnitPrice costDTO = (UnitPrice) o;

        if (!price.equals(costDTO.price)) return false;
        if (quantity != costDTO.quantity) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = price.hashCode();
        result = 31 * result + quantity.hashCode();
        return result;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("Amount");
        sb.append("{quantity=").append(price);
        sb.append(", unit=").append(quantity);
        sb.append('}');
        return sb.toString();
    }

    public UnitPriceDTO createDTO() {
        return new UnitPriceDTO(getPrice(), getQuantity());
    }
}
