/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.server.domain;

import java.math.BigDecimal;

import javax.persistence.Embeddable;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

import org.parre.server.domain.util.DataObject;
import org.parre.shared.AmountDTO;
import org.parre.shared.types.MoneyUnitType;


/**
 * The Class Amount.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 9/22/11
 */
@Embeddable
public class Amount extends DataObject {
    public static final Amount ZERO = new Amount(BigDecimal.ZERO);
    private BigDecimal quantity;
    @Enumerated(EnumType.STRING)
    private MoneyUnitType unit;

    public Amount() {
    }

    public Amount(AmountDTO dto) {
        this(dto.getQuantity(), dto.getUnit());
    }

    public Amount(BigDecimal quantity) {
        this(quantity, MoneyUnitType.DOLLAR);
    }

    public Amount(BigDecimal quantity, MoneyUnitType unit) {
        this.quantity = quantity;
        this.unit = unit;
    }

    public void setQuantity(BigDecimal quantity) {
        this.quantity = quantity;
    }

    public void setUnit(MoneyUnitType unit) {
        this.unit = unit;
    }

    public BigDecimal getQuantity() {
        return quantity;
    }

    public MoneyUnitType getUnit() {
        return unit;
    }

    public BigDecimal createDollarValue() {
        return quantity.multiply(unit.getDollarEquivalent());
    }
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Amount costDTO = (Amount) o;

        if (!quantity.equals(costDTO.quantity)) return false;
        if (unit != costDTO.unit) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = quantity.hashCode();
        result = 31 * result + unit.hashCode();
        return result;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("Amount");
        sb.append("{quantity=").append(quantity);
        sb.append(", unit=").append(unit);
        sb.append('}');
        return sb.toString();
    }

    public AmountDTO createDTO() {
        return new AmountDTO(getQuantity(), getUnit());
    }
}
