/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.server.domain;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.parre.shared.AmountDTO;
import org.parre.shared.RiskBenefitAnalysisDTO;
import org.parre.shared.types.MoneyUnitType;


/**
 * The Class RiskBenefitAnalysis.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 2/15/12
 */
@Entity
@Table(name = "RISK_BENEFIT_ANALYSIS")
public class RiskBenefitAnalysis extends NameDescription {
    @OneToOne(targetEntity = ParreAnalysis.class)
    @JoinColumn(name = "PARRE_ANALYSIS_ID")
    private ParreAnalysis parreAnalysis;
    @Column(name = "STARTED_DATE") @Temporal(value = TemporalType.DATE)
    private Date startedDate;

    @Embedded
    @AttributeOverrides( {
            @AttributeOverride(name="quantity", column = @Column(name="BUDGET_Q", columnDefinition = "DECIMAL(10,2)") ),
            @AttributeOverride(name="unit", column = @Column(name="BUDGET_U") )
    } )
    private Amount budget;

    public RiskBenefitAnalysis() {
    }

    public RiskBenefitAnalysis(ParreAnalysis parreAnalysis) {
        super("Analysis for baseline : " + parreAnalysis.getName(), "");
        setParreAnalysis(parreAnalysis);
        budget = new Amount(BigDecimal.ZERO, MoneyUnitType.MILLION);
    }

    public ParreAnalysis getParreAnalysis() {
        return parreAnalysis;
    }

    public void setParreAnalysis(ParreAnalysis parreAnalysis) {
        this.parreAnalysis = parreAnalysis;
    }

    public Date getStartedDate() {
        return startedDate;
    }

    public void setStartedDate(Date startedDate) {
        this.startedDate = startedDate;
    }

    public Amount getBudget() {
        return budget;
    }

    public void setBudget(Amount budget) {
        this.budget = budget;
    }

    public AmountDTO getBudgetDTO() {
        return budget.createDTO();
    }

    public void setBudgetDTO(AmountDTO dto) {
        if (dto != null) {
            budget = new Amount(dto);
        }
    }

    public RiskBenefitAnalysisDTO createDTO() {
        return copyInto(new RiskBenefitAnalysisDTO());
    }

}
