/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.server.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "PARRE_ANALYSIS_REPORT_SECTION")
public class ParreAnalysisReportSection implements Serializable {
	private static final long serialVersionUID = 908959729782875454L;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "REPORT_ID")
	@Id
	private ParreAnalysisReport parreAnalysisReport;
	
	@ManyToOne
	@JoinColumn(name = "REPORT_SECTION_ID")
	@Id
	private ReportSectionLookup reportSectionLookup;
	
	@Column(name = "USER_INPUT", columnDefinition = "VARCHAR(4000)", length = 4000)
	private String userInput;
	
	@Column(name = "OMITTED")
	private Boolean omitted = false;

	public ParreAnalysisReport getParreAnalysisReport() {
		return parreAnalysisReport;
	}

	public void setParreAnalysisReport(ParreAnalysisReport parreAnalysisReport) {
		this.parreAnalysisReport = parreAnalysisReport;
	}

	public ReportSectionLookup getReportSectionLookup() {
		return reportSectionLookup;
	}

	public void setReportSectionLookup(ReportSectionLookup reportSectionLookup) {
		this.reportSectionLookup = reportSectionLookup;
	}

	public String getUserInput() {
		return userInput;
	}

	public void setUserInput(String userInput) {
		this.userInput = userInput;
	}

	public Boolean getOmitted() {
		return omitted;
	}

	public void setOmitted(Boolean omitted) {
		this.omitted = omitted;
	}

	@Override
	public String toString() {
		return "ParreAnalysisReportSection [parreAnalysisReport="
				+ parreAnalysisReport + ", reportSectionLookup="
				+ reportSectionLookup + ", userInput=" + userInput
				+ ", omitted=" + omitted + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((omitted == null) ? 0 : omitted.hashCode());
		result = prime
				* result
				+ ((parreAnalysisReport == null) ? 0 : parreAnalysisReport
						.hashCode());
		result = prime
				* result
				+ ((reportSectionLookup == null) ? 0 : reportSectionLookup
						.hashCode());
		result = prime * result
				+ ((userInput == null) ? 0 : userInput.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ParreAnalysisReportSection other = (ParreAnalysisReportSection) obj;
		if (omitted == null) {
			if (other.omitted != null)
				return false;
		} else if (!omitted.equals(other.omitted))
			return false;
		if (parreAnalysisReport == null) {
			if (other.parreAnalysisReport != null)
				return false;
		} else if (!parreAnalysisReport.equals(other.parreAnalysisReport))
			return false;
		if (reportSectionLookup == null) {
			if (other.reportSectionLookup != null)
				return false;
		} else if (!reportSectionLookup.equals(other.reportSectionLookup))
			return false;
		if (userInput == null) {
			if (other.userInput != null)
				return false;
		} else if (!userInput.equals(other.userInput))
			return false;
		return true;
	}
	
	

	
}
