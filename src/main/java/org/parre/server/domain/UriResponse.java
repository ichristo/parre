/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.server.domain;

import javax.persistence.*;
import java.math.BigDecimal;

/**
 * User: Swarn S. Dhaliwal
 * Date: 7/14/11
 * Time: 11:20 AM
*/
@Entity
@Table(name = "URI_RESPONSE")
public class UriResponse extends BaseEntity {
    @ManyToOne(targetEntity = RiskResilienceAnalysis.class)
    @JoinColumn(name = "ANALYSIS_ID")
    private RiskResilienceAnalysis analysis;
    @OneToOne(targetEntity = UriOption.class)
    @JoinColumn(name = "OPTION_ID")
    private UriOption uriOption;
    @Column(name = "VALUE", columnDefinition = "DECIMAL(5,2)")
    private BigDecimal value;
    @Column(name = "WEIGHT", columnDefinition = "DECIMAL(5,2)")
    private BigDecimal weight;

    public UriResponse() {
    }

    public UriResponse(RiskResilienceAnalysis analysis, UriOption uriOption) {
        this.analysis = analysis;
        this.uriOption = uriOption;
    }

    public RiskResilienceAnalysis getAnalysis() {
        return analysis;
    }

    public void setAnalysis(RiskResilienceAnalysis analysis) {
        this.analysis = analysis;
    }

    public UriOption getUriOption() {
        return uriOption;
    }

    public void setUriOption(UriOption uriOption) {
        this.uriOption = uriOption;
    }

    public BigDecimal getValue() {
        return value;
    }

    public void setValue(BigDecimal value) {
        this.value = value;
    }

    public BigDecimal getWeight() {
        return weight;
    }

    public void setWeight(BigDecimal weight) {
        this.weight = weight;
    }
}
