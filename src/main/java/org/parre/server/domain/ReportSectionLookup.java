/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.server.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name = "REPORT_SECTION_LOOKUP", uniqueConstraints = @UniqueConstraint(columnNames = {"NAME"}))
public class ReportSectionLookup extends NameDescription {
	private static final long serialVersionUID = 5337957438588858867L;

	@Column(name = "ADDITIONAL_DESCRIPTION", columnDefinition = "VARCHAR(4000)", length = 4000)
    private String additionalDescription;
	
	@Column(name = "INSTRUCTIONS", columnDefinition = "VARCHAR(4000)", length = 4000)
	private String instructions;
	
	@Column(name = "DISPLAY_ORDER")
	private Integer displayOrder;
	
	@Column(name = "REPORT_SECTION_GROUP", columnDefinition = "VARCHAR(200)", length = 200)
	private String reportSectionGroup;
	
	@Column(name = "DISPLAY_TABLE")
	private Boolean displayTable = false;

	@Column(name = "INPUT_TABLE")
	private Boolean inputTable = false;
	
	@Column(name = "REPORT_DISPLAY")
	private Boolean reportDisplay = false;

	
	public ReportSectionLookup() { 
		super();
	}
	
	public ReportSectionLookup(String name, String description, String additionalDescription, String instructions, Integer displayOrder, String reportSectionGroup, Boolean displayTable, Boolean inputTable, Boolean reportDisplay) {
		super(name, description);
		this.additionalDescription = additionalDescription;
		this.instructions = instructions;
		this.displayOrder = displayOrder;
		this.reportSectionGroup = reportSectionGroup;
		this.displayTable = displayTable;
		this.inputTable = inputTable;
		this.reportDisplay =reportDisplay;
	}

	public String getAdditionalDescription() {
		return additionalDescription;
	}

	public void setAdditionalDescription(String additionalDescription) {
		this.additionalDescription = additionalDescription;
	}

	public String getInstructions() {
		return instructions;
	}

	public void setInstructions(String instructions) {
		this.instructions = instructions;
	}

	public Integer getDisplayOrder() {
		return displayOrder;
	}

	public void setDisplayOrder(Integer displayOrder) {
		this.displayOrder = displayOrder;
	}
	
	public String getReportSectionGroup() {
		return reportSectionGroup;
	}

	public void setReportSectionGroup(String reportSectionGroup) {
		this.reportSectionGroup = reportSectionGroup;
	}

	public Boolean getReportDisplay() {
		return reportDisplay;
	}

	public void setReportDisplay(Boolean reportDisplay) {
		this.reportDisplay = reportDisplay;
	}

	@Override
	public String toString() {
		return "ReportSectionLookup [additionalDescription="
				+ additionalDescription + ", instructions=" + instructions
				+ ", displayOrder=" + displayOrder + ", reportSectionGroup=" + reportSectionGroup 
				+ ", displayTable=" + displayTable + ", inputTable=" + inputTable +  ", reportDisplay=" + reportDisplay + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime
				* result
				+ ((additionalDescription == null) ? 0 : additionalDescription
						.hashCode());
		result = prime * result
				+ ((displayOrder == null) ? 0 : displayOrder.hashCode());
		result = prime * result
				+ ((displayTable == null) ? 0 : displayTable.hashCode());
		result = prime * result
				+ ((inputTable == null) ? 0 : inputTable.hashCode());
		result = prime * result
				+ ((instructions == null) ? 0 : instructions.hashCode());
		result = prime * result
				+ ((reportDisplay == null) ? 0 : reportDisplay.hashCode());
		result = prime
				* result
				+ ((reportSectionGroup == null) ? 0 : reportSectionGroup
						.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		ReportSectionLookup other = (ReportSectionLookup) obj;
		if (additionalDescription == null) {
			if (other.additionalDescription != null)
				return false;
		} else if (!additionalDescription.equals(other.additionalDescription))
			return false;
		if (displayOrder == null) {
			if (other.displayOrder != null)
				return false;
		} else if (!displayOrder.equals(other.displayOrder))
			return false;
		if (displayTable == null) {
			if (other.displayTable != null)
				return false;
		} else if (!displayTable.equals(other.displayTable))
			return false;
		if (inputTable == null) {
			if (other.inputTable != null)
				return false;
		} else if (!inputTable.equals(other.inputTable))
			return false;
		if (instructions == null) {
			if (other.instructions != null)
				return false;
		} else if (!instructions.equals(other.instructions))
			return false;
		if (reportDisplay == null) {
			if (other.reportDisplay != null)
				return false;
		} else if (!reportDisplay.equals(other.reportDisplay))
			return false;
		if (reportSectionGroup == null) {
			if (other.reportSectionGroup != null)
				return false;
		} else if (!reportSectionGroup.equals(other.reportSectionGroup))
			return false;
		return true;
	}
	
	
	
}
