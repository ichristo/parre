/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.server.domain;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.parre.shared.DiagramAnalysisDTO;
import org.parre.shared.types.AnalysisTreeType;


/**
 * User: keithjones
 * Date: 6/18/13
 * Time: 9:53 AM
*/
@Entity
@Table(name = "DIAGRAM_ANALYSIS")
public class DiagramAnalysis extends BaseEntity {

    @ManyToMany(targetEntity = CounterMeasure.class)
    @JoinTable(name = "DIAGRAM_ANALYSIS_COUNTER_MEASURE",
            joinColumns = {@JoinColumn(name = "DIAGRAM_ANALYSIS_ID")},
            inverseJoinColumns = {@JoinColumn(name = "COUNTER_MEASURE_ID")})
    private Set<CounterMeasure> diagramAnalysisCounterMeasures = Collections.<CounterMeasure>emptySet();

    @OneToOne(targetEntity = AnalysisTreeNode.class)
    @JoinColumn(name = "DT_ANALYSIS_TREE_ROOT")
    private AnalysisTreeNode analysisTreeRoot;

    @Column(name = "ANALYSIS_TREE_TYPE") @Enumerated(EnumType.STRING) @NotNull
    private AnalysisTreeType analysisTreeType;

    @OneToOne(targetEntity = ParreAnalysis.class)
    @JoinColumn(name = "PARRE_ANALYSIS_ID")
    private ParreAnalysis parreAnalysis;

    public Set<CounterMeasure> getDiagramAnalysisCounterMeasures() {
        return diagramAnalysisCounterMeasures;
    }

    public void setDiagramAnalysisCounterMeasures(Set<CounterMeasure> diagramAnalysisCounterMeasures) {
        this.diagramAnalysisCounterMeasures = diagramAnalysisCounterMeasures;
    }

    public AnalysisTreeNode getAnalysisTreeRoot() {
        return analysisTreeRoot;
    }

    public void setAnalysisTreeRoot(AnalysisTreeNode analysisTreeRoot) {
        this.analysisTreeRoot = analysisTreeRoot;
    }

    public AnalysisTreeType getAnalysisTreeType() {
        return analysisTreeType;
    }

    public void setAnalysisTreeType(AnalysisTreeType analysisTreeType) {
        this.analysisTreeType = analysisTreeType;
    }

    public DiagramAnalysisDTO createDTO() {
        DiagramAnalysisDTO dto = new DiagramAnalysisDTO();
        dto.setId(this.getId());
        dto.setCounterMeasureDTOs(CounterMeasure.createSetOfDTOs(diagramAnalysisCounterMeasures));
        try {
            dto.setAnalysisTreeRootDTO(this.getAnalysisTreeRoot().buildTreeDTO());
            dto.setAnalysisTreeType(this.getAnalysisTreeType());
        } catch (NullPointerException e) {
        }

        return dto;
    }

    public DiagramAnalysis makeCopy() {
        DiagramAnalysis newDiagram = this.copyInto(new DiagramAnalysis());
        newDiagram.setId(null);
        newDiagram.setParreAnalysis(null);
        newDiagram.setAnalysisTreeRoot(this.getAnalysisTreeRoot().makeCopy());
        return newDiagram;
    }

    public static List<DiagramAnalysisDTO> createDTOs(List<DiagramAnalysis> diagramAnalysisList) {
        List<DiagramAnalysisDTO> diagramAnalysisDTOs = new ArrayList<DiagramAnalysisDTO>();
        for(DiagramAnalysis diagramAnalysis : diagramAnalysisList) {
            diagramAnalysisDTOs.add(diagramAnalysis.createDTO());
        }
        return diagramAnalysisDTOs;
    }

    public ParreAnalysis getParreAnalysis() {
        return parreAnalysis;
    }

    public void setParreAnalysis(ParreAnalysis parreAnalysis) {
        this.parreAnalysis = parreAnalysis;
    }

    public void addCounterMeasure(CounterMeasure counterMeasure) {
        if(diagramAnalysisCounterMeasures.isEmpty()) {
            diagramAnalysisCounterMeasures = new HashSet<CounterMeasure>();
        }
        diagramAnalysisCounterMeasures.add(counterMeasure);
        counterMeasure.addDiagramAnalysis(this);
    }
    
    public void addCounterMeasureSimple(CounterMeasure counterMeasure) {
    	if(diagramAnalysisCounterMeasures.isEmpty()) {
    		diagramAnalysisCounterMeasures = new HashSet<CounterMeasure>();
    	}
    	diagramAnalysisCounterMeasures.add(counterMeasure);
    }
}
