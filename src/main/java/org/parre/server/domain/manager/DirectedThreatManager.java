/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.server.domain.manager;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.apache.log4j.Logger;
import org.parre.server.domain.Amount;
import org.parre.server.domain.DirectedThreat;
import org.parre.server.domain.DirectedThreatAnalysis;
import org.parre.server.domain.ParreAnalysis;
import org.parre.shared.AssetSearchDTO;
import org.parre.shared.ConsequenceDTO;
import org.parre.shared.LotDTO;
import org.parre.shared.ThreatDTO;
import org.parre.shared.VulnerabilityDTO;
import org.parre.shared.types.LotAnalysisType;
import org.parre.shared.types.VulnerabilityAnalysisType;

/**
 * The Class DirectedThreatManager.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 11/30/11
 */
public class DirectedThreatManager extends ParreManager {
    private static transient final Logger log = Logger.getLogger(DirectedThreatManager.class);

    public static DirectedThreatAnalysis getCurrentAnalysis(Long parreAnalysisId, EntityManager entityManager) {
        String queryString = "select analysis from DirectedThreatAnalysis analysis" +
                " where analysis.parreAnalysis.id = :parreAnalysisId";
        Query query = entityManager.createQuery(queryString);
        query.setParameter("parreAnalysisId", parreAnalysisId);
        List<DirectedThreatAnalysis> resultList = query.getResultList();
        return getSingleResultWithNull(resultList);
    }

    public static List<ConsequenceDTO> searchConsequences(AssetSearchDTO searchDTO, Long analysisId, EntityManager entityManager) {
        StringBuilder builder = createConsequenceSelect();
        Query query = createAssetAnalysisSearchQuery(searchDTO, analysisId, builder, entityManager);
        return createConsequenceDTOs(query);
    }

    public static List<VulnerabilityDTO> searchVulnerabilities(AssetSearchDTO searchDTO, Long analysisId, EntityManager entityManager) {
        StringBuilder builder = createVulnerabilitySelect();
        Query query = createAssetAnalysisSearchQuery(searchDTO, analysisId, builder, entityManager);
        return createVulnerabilityDTOs(query);
    }

    public static List<LotDTO> searchLots(AssetSearchDTO searchDTO, Long analysisId, EntityManager entityManager) {
        StringBuilder builder = createLotSelect();
        Query query = createAssetAnalysisSearchQuery(searchDTO, analysisId, builder, entityManager);
        return createLotDTOs(query);
    }

    public static List<ConsequenceDTO> searchConsequences(ThreatDTO searchDTO, Long analysisId, EntityManager entityManager) {
        StringBuilder builder = createConsequenceSelect();
        Query query = createThreatAnalysisSearchQuery(searchDTO, analysisId, builder, entityManager);
        return createConsequenceDTOs(query);
    }

    public static List<VulnerabilityDTO> searchVulnerabilities(ThreatDTO searchDTO, Long analysisId, EntityManager entityManager) {
        StringBuilder builder = createVulnerabilitySelect();
        Query query = createThreatAnalysisSearchQuery(searchDTO, analysisId, builder, entityManager);
        return createVulnerabilityDTOs(query);
    }

    public static List<LotDTO> searchLots(ThreatDTO searchDTO, Long analysisId, EntityManager entityManager) {
        StringBuilder builder = createLotSelect();
        Query query = createThreatAnalysisSearchQuery(searchDTO, analysisId, builder, entityManager);
        return createLotDTOs(query);
    }

    private static StringBuilder createConsequenceSelect() {
        StringBuilder queryBuilder = createAssetThreatAnalysisSelect();
        queryBuilder.append(", e.fatalities, e.seriousInjuries, e.financialImpact, e.economicImpact, e.financialTotal, e.durationDays, e.severityMgd");
        appendFromAndWhereClause(queryBuilder);
        return queryBuilder;
    }

    private static List<ConsequenceDTO> createConsequenceDTOs(Query query) {
        List<Object[]> resultList = query.getResultList();
        if (log.isDebugEnabled()) { log.debug("Number of results : " + resultList.size()); }
        List<ConsequenceDTO> dtos = new ArrayList<ConsequenceDTO>(resultList.size());
        for (Object[] objects : resultList) {
            ConsequenceDTO dto = new ConsequenceDTO();
            int index = populateAssetThreatDTO(objects, dto);
            dto.setFatalities((Integer) objects[index++]);
            dto.setSeriousInjuries((Integer) objects[index++]);
            Amount amount;
            amount = (Amount) objects[index++];
            dto.setFinancialImpactDTO(amount.createDTO());
            amount = (Amount) objects[index++];
            dto.setEconomicImpactDTO(amount.createDTO());
            amount = (Amount) objects[index++];
            dto.setFinancialTotalDTO(amount.createDTO());
            dto.setDurationDays(objects[index] != null ? new BigDecimal(objects[index++].toString()) : BigDecimal.ZERO);
            dto.setSeverityMgd(objects[index] != null ? new BigDecimal(objects[index++].toString()) : BigDecimal.ZERO);
            dtos.add(dto);
        }
        return dtos;
    }

    private static StringBuilder createVulnerabilitySelect() {
        StringBuilder queryBuilder = createAssetThreatAnalysisSelect();
        queryBuilder.append(", e.vulnerabilityAnalysisType, e.vulnerability");
        appendFromAndWhereClause(queryBuilder);
        return queryBuilder;
    }

    public static List<VulnerabilityDTO> createVulnerabilityDTOs(Query query) {
        List<Object[]> resultList = query.getResultList();
        if (log.isDebugEnabled()) { log.debug("Number of results : " + resultList.size()); }
        List<VulnerabilityDTO> dtos = new ArrayList<VulnerabilityDTO>(resultList.size());
        for (Object[] objects : resultList) {
            VulnerabilityDTO dto = new VulnerabilityDTO();
            int index = populateAssetThreatDTO(objects, dto);
            dto.setVulnerabilityAnalysisType((VulnerabilityAnalysisType) objects[index++]);
            dto.setVulnerability((BigDecimal) objects[index++]);
            dtos.add(dto);
        }
        return dtos;
    }

    private static StringBuilder createLotSelect() {
        StringBuilder queryBuilder = createAssetThreatAnalysisSelect();
        queryBuilder.append(", e.lotAnalysisType, e.lot");
        appendFromAndWhereClause(queryBuilder);
        return queryBuilder;
    }

    public static List<LotDTO> createLotDTOs(Query query) {
        List<Object[]> resultList = query.getResultList();
        if (log.isDebugEnabled()) { log.debug("Number of results : " + resultList.size()); }
        List<LotDTO> dtos = new ArrayList<LotDTO>(resultList.size());
        for (Object[] objects : resultList) {
            LotDTO dto = new LotDTO();
            int index = populateAssetThreatDTO(objects, dto);
            dto.setLotAnalysisType((LotAnalysisType) objects[index++]);
            dto.setLot((BigDecimal) objects[index++]);
            dtos.add(dto);
        }
        return dtos;
    }

    private static void appendFromAndWhereClause(StringBuilder queryBuilder) {
        queryBuilder.append(" from DirectedThreat e");
        appendWhereClause(queryBuilder);
    }

    public static List<DirectedThreat> getDirectedThreats(ParreAnalysis parreAnalysis, EntityManager entityManager) {
        String queryString = "select e from DirectedThreat e" +
                " where e.analysis.parreAnalysis.id = :parreAnalysis" +
                " and e.assetThreat.removed = :removed";
        Query query = entityManager.createQuery(queryString);
        query.setParameter("parreAnalysis", parreAnalysis.getId());
        query.setParameter("removed", Boolean.FALSE);
        return query.getResultList();
    }

    public static void deleteDirectedThreatAnalysisWithParreAnalysis(Long parreAnalysisId, EntityManager entityManager) {
        entityManager.createQuery("delete from DirectedThreatAnalysis dta where dta.parreAnalysis.id = :parreAnalysisId")
                                .setParameter("parreAnalysisId", parreAnalysisId).executeUpdate();
    }
}
