/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.server.domain.manager;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.apache.log4j.Logger;
import org.parre.server.domain.*;
import org.parre.server.domain.util.DomainUtil;
import org.parre.server.util.NaturalThreatCalculator;
import org.parre.server.util.NaturalThreatCalculatorImpl;
import org.parre.shared.AssetSearchDTO;
import org.parre.shared.EarthquakeMagnitudeProbabilityDTO;
import org.parre.shared.EarthquakeProfileDTO;
import org.parre.shared.HurricaneProfileDTO;
import org.parre.shared.IceStormIndexWorkDTO;
import org.parre.shared.IceStormWorkDTO;
import org.parre.shared.NaturalThreatCalculationDTO;
import org.parre.shared.NaturalThreatDTO;
import org.parre.shared.PhysicalResourceDTO;
import org.parre.shared.SharedCalculationUtil;
import org.parre.shared.StatValuesDTO;
import org.parre.shared.ThreatDTO;
import org.parre.shared.types.NaturalThreatType;


/**
 * The Class NaturalThreatManager.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 11/30/11
 */
public class NaturalThreatManager extends ParreManager {
    private static transient final Logger log = Logger.getLogger(NaturalThreatManager.class);
    private final static NaturalThreatCalculator naturalThreatCalculator = new NaturalThreatCalculatorImpl();

    public static NaturalThreatAnalysis getCurrentAnalysis(Long parreAnalysisId, EntityManager entityManager) {
        String queryString = "select analysis from NaturalThreatAnalysis analysis" +
                " where analysis.parreAnalysis.id = :parreAnalysisId";
        Query query = entityManager.createQuery(queryString);
        query.setParameter("parreAnalysisId", parreAnalysisId);
        List<NaturalThreatAnalysis> resultList = query.getResultList();
        return getSingleResultWithNull(resultList);
    }
    
    public static List<NaturalThreatDTO> search(AssetSearchDTO searchDTO, Long analysisId, Long parreAnalysisId, EntityManager entityManager) {
        StringBuilder builder = createSearchSelect();
        Query query = createAssetAnalysisSearchQuery(searchDTO, analysisId, builder, entityManager);
        return createDTOs(analysisId, parreAnalysisId, query, entityManager);
    }

    public static List<NaturalThreatDTO> search(ThreatDTO searchDTO, Long analysisId, Long parreAnalysisId, EntityManager entityManager) {
        StringBuilder builder = createSearchSelect();
        Query query = createThreatAnalysisSearchQuery(searchDTO, analysisId, builder, entityManager);
        return createDTOs(analysisId, parreAnalysisId, query, entityManager);
    }
    
    public static NaturalThreatDTO buildByAssetThreatId(Long assetThreatId, Long parreAnalysisId, EntityManager entityManager) {
    	if (log.isDebugEnabled()) { log.debug("Looking up NaturalThreatEntity by assetThreatId: " + assetThreatId); }
    	StringBuilder builder = createSearchSelect();
    	NaturalThreat nt = (NaturalThreat)entityManager.createQuery("select nt from NaturalThreat nt where nt.assetThreat.id = :assetThreatId and nt.parreAnalysis.id = :parreAnalysisId")
    				.setParameter("assetThreatId", assetThreatId)
    				.setParameter("parreAnalysisId", parreAnalysisId)
    				.getSingleResult();
    	builder.append(" and e.assetThreat.id = :assetThreatId");
        Query query = entityManager.createQuery(builder.toString());
        setAnalysisQueryParameters(nt.getAnalysis().getId(), query);
        query.setParameter("assetThreatId", assetThreatId);
    	NaturalThreatDTO dto = createDTOs(nt.getAnalysis().getId(), nt.getParreAnalysis().getId(), query, entityManager).get(0);
    	if (log.isDebugEnabled()) { log.debug("Returning dto: " + dto.toString()); }
    	return dto;
    }
    
    private static List<NaturalThreatDTO> createDTOs(Long analysisId, Long parreAnalysisId, Query query, EntityManager entityManager) {
    	// Moved financialTotal and totalRisk calculations from client to server b/c of Ice Storms
    	// ... and then moved them again to calculate on save, not on load due to performance issues
    	StatValuesDTO statValuesDTO = null;
    	ParreAnalysis analysis = entityManager.find(ParreAnalysis.class, parreAnalysisId);
    	if (log.isDebugEnabled()) { log.debug("Natural Threat Analysis Id = " + analysisId); }
    	if (analysis.getUseStatValues()) {
    		statValuesDTO = analysis.getStatValuesDTO();
    	}
    	
        List<Object[]> resultList = query.getResultList();
        if (log.isDebugEnabled()) { log.debug("Number of results : " + resultList.size()); }
        List<NaturalThreatDTO> dtos = new ArrayList<NaturalThreatDTO>(resultList.size());
        for (Object[] objects : resultList) {
            NaturalThreatDTO dto = new NaturalThreatDTO();
            int index = populateAssetThreatDTO(objects, dto);
            dto.setFatalities((Integer) objects[index++]);
            dto.setSeriousInjuries((Integer) objects[index++]);
            Amount amount = (Amount) objects[index++];
            dto.setFinancialImpactDTO(amount.createDTO());
            amount = (Amount) objects[index++];
            dto.setEconomicImpactDTO(amount.createDTO());
            dto.setVulnerability((BigDecimal) objects[index++]);
            dto.setLot((BigDecimal) objects[index++]);
            dto.setManuallyEntered((Boolean) objects[index++]);
            dto.setUseDefaultValues((Boolean) objects[index++]);
            dto.setLatitudeOverride((BigDecimal) objects[index++]);
            dto.setLongitudeOverride((BigDecimal) objects[index++]);
            dto.setCountyOverride((String) objects[index++]);
            dto.setStateCodeOverride((String) objects[index++]);
            dto.setFloodRisk((Boolean) objects[index++]);
            dto.setEarthquakeProfileId((Long)objects[index++]);
            dto.setHurricaneProfileId((Long)objects[index++]);
            dto.setDurationDays((BigDecimal)objects[index++]);
            dto.setSeverityMgd((BigDecimal)objects[index++]);
            amount = (Amount) objects[index++];
            
            dto.setOperationalLossDTO(amount == null ? (Amount.ZERO).createDTO() : amount.createDTO());
            
            //SharedCalculationUtil.calculateFinancialTotal(dto, statValuesDTO);
            amount = (Amount) objects[index++];
            dto.setFinancialTotalDTO(amount == null ? (Amount.ZERO).createDTO() : amount.createDTO());
            
            amount = (Amount) objects[index++];
            dto.setTotalRiskDTO(amount == null ? (Amount.ZERO).createDTO() : amount.createDTO());
            
            dto.setDesignSpeed((BigDecimal)objects[index++]);
            // Get and set Ice Threat details if necessary
            if (NaturalThreatType.getNaturalThreatType(dto.getThreatDescription()).equals(NaturalThreatType.ICE_STORM)) {
            	IceStormThreatDetails details = null;
            	try {
            		details = (IceStormThreatDetails)entityManager
            				.createQuery("select istd from IceStormThreatDetails istd where istd.naturalThreat.id = :naturalThreatId")
            				.setParameter("naturalThreatId", dto.getId())
            				.getSingleResult();
            	} catch (NoResultException nre) {
            		details = new IceStormThreatDetails();
            	}
            	
            	IceStormWorkDTO iceWork = new IceStormWorkDTO();
            	iceWork.setDailyOperationalLoss(details.getDailyOperationalLoss().getQuantity());
            	iceWork.setDaysOfBackupPower(details.getDaysOfBackupPower());
            	iceWork.setStateCode(details.getStateCode());
            	iceWork.setLatitude(details.getLatitude());
            	iceWork.setLongitude(details.getLongitude());
            	
            	IceStormIndexWorkDTO indexWork = new IceStormIndexWorkDTO();
            	indexWork.setRecoveryTime(details.getIndex3RecoveryTime());
            	indexWork.setFatalities(details.getIndex3Fatalities());
            	indexWork.setSeriousInjuries(details.getIndex3SeriousInjuries());
            	iceWork.addIceStormIndex(3, indexWork);
            	
            	indexWork = new IceStormIndexWorkDTO();
            	indexWork.setRecoveryTime(details.getIndex4RecoveryTime());
            	indexWork.setFatalities(details.getIndex4Fatalities());
            	indexWork.setSeriousInjuries(details.getIndex4SeriousInjuries());
            	iceWork.addIceStormIndex(4, indexWork);
            	
            	indexWork = new IceStormIndexWorkDTO();
            	indexWork.setRecoveryTime(details.getIndex5RecoveryTime());
            	indexWork.setFatalities(details.getIndex5Fatalities());
            	indexWork.setSeriousInjuries(details.getIndex5SeriousInjuries());
            	iceWork.addIceStormIndex(5, indexWork);
            	
            	dto.setIceStormWorkDTO(iceWork);
            }
            
            if (log.isDebugEnabled()) { log.debug("Converted natural threat to dto: " + dto.toString()); }
            dtos.add(dto);
        }

        return dtos;
    }

    private static StringBuilder createSearchSelect() {
        StringBuilder queryBuilder = createAssetThreatAnalysisSelect();
        queryBuilder.append(", e.fatalities, e.seriousInjuries, e.financialImpact, e.economicImpact, e.vulnerability, e.lot,")
        		.append(" e.manuallyEntered, e.useDefaultValues, e.latitudeOverride, e.longitudeOverride, e.countyOverride,")
        		.append(" e.stateCodeOverride, e.floodRisk, e.earthquakeProfile.id, e.hurricaneProfile.id,")
        		.append(" e.durationDays, e.severityMgd, e.operationalLoss, e.financialTotal, e.totalRisk, e.designSpeed")
                .append(" from NaturalThreat e");
        appendWhereClause(queryBuilder);
        return queryBuilder;
    }
    
    public static IceStormThreatDetails getIceStormThreatDetails(NaturalThreat naturalThreat, EntityManager entityManager) {
    	try {
    		return (IceStormThreatDetails) entityManager.createQuery("select istd from IceStormThreatDetails istd where istd.naturalThreat = :naturalThreat")
    								.setParameter("naturalThreat", naturalThreat)
    								.getSingleResult();
    	} catch (NoResultException nre) {
    		return new IceStormThreatDetails(naturalThreat);
    	}
    }

    public static List<EarthquakeLookupData> getEarthquakeLookupData(EntityManager entityManager){
        String queryString = "select eld from EarthquakeLookupData eld";
        Query query = entityManager.createQuery(queryString);
        List<EarthquakeLookupData> resultList = query.getResultList();
        return resultList;
    }

    public static List<HurricaneLookupData> getHurricaneLookupData(EntityManager entityManager){
        String queryString = "select hld from HurricaneLookupData hld order by hld.lowerWindSpeed ";
        Query query = entityManager.createQuery(queryString);
        List<HurricaneLookupData> resultList = query.getResultList();
        return resultList;
    }
    
    public static List<IceworkLookupData> getIceworkLookupData(EntityManager entityManager){
        String queryString = "select ild from IceworkLookupData ild";
        Query query = entityManager.createQuery(queryString);
        List<IceworkLookupData> resultList = query.getResultList();
        return resultList;
    }

    public static List<TornadoLookupData> getTornadoLookupData(EntityManager entityManager){
        String queryString = "select tld from TornadoLookupData tld";
        Query query = entityManager.createQuery(queryString);
        List<TornadoLookupData> resultList = query.getResultList();
        return resultList;
    }

    public static WindLookupData getWindLookupData(Integer concatenatedLocation, EntityManager entityManager) {
    	try {
	        String queryString = "select wld from WindLookupData wld where wld.longitudeAndLatitude = :longitudeAndLatitude";
	        Query query = entityManager.createQuery(queryString).setParameter("longitudeAndLatitude", String.valueOf(concatenatedLocation));
	        return (WindLookupData) query.getSingleResult();
    	} catch (NoResultException nre) {
    		log.warn("Could not lookup wind data with input: " + concatenatedLocation);
    	}
    	return WindLookupData.createEmpty();
    }
    
    public static TornadoLookupData findTornadoDataByCountyState(String county, String stateCode, EntityManager entityManager) {
    	String queryString = "select tld from TornadoLookupData tld where upper(tld.county) = :county and upper(tld.state) = :stateCode";
    	return (TornadoLookupData)entityManager.createQuery(queryString)
    				.setParameter("county", county.toUpperCase())
    				.setParameter("stateCode", stateCode.toUpperCase())
    				.setMaxResults(1)
    				.getSingleResult();
    }
    
    public static EarthquakeProfileDTO createEarthquakeProfileDTO(EarthquakeProfile earthquakeProfile) {
    	EarthquakeProfileDTO dto = new EarthquakeProfileDTO();
    	if (earthquakeProfile != null) {
        	DomainUtil.copyProperties(dto, earthquakeProfile);
        	for (EarthquakeMagnitudeProbability probability : earthquakeProfile.getMagnitudeProbabilities()) {
        		EarthquakeMagnitudeProbabilityDTO probDTO = new EarthquakeMagnitudeProbabilityDTO();
        		DomainUtil.copyProperties(probDTO, probability);
        		dto.addMagnitudeProbabilityDTO(probDTO); 
        	}
        }
    	return dto;
    }

	public static PhysicalResourceDTO getPhysicalResourceForNT(Long naturalThreatId, EntityManager entityManager) {
		String queryString = "select nt.assetThreat.asset from NaturalThreat nt where nt.id = :id";

        Object object = entityManager.createQuery(queryString)
						.setParameter("id", naturalThreatId)
						.getSingleResult();
        PhysicalResourceDTO dto = null;
        if(object instanceof PhysicalResource) {
            PhysicalResource physicalResource =
				(PhysicalResource) entityManager.createQuery(queryString)
						.setParameter("id", naturalThreatId)
						.getSingleResult();
            dto = physicalResource.copyInto(new PhysicalResourceDTO());
            dto.setParreAnalysisId(physicalResource.getParreAnalysis().getId());
        }
        if(object instanceof ExternalResource) {
            ExternalResource externalResource = (ExternalResource) entityManager.createQuery(queryString)
						.setParameter("id", naturalThreatId)
						.getSingleResult();
            dto = externalResource.copyInto(new PhysicalResourceDTO());
            dto.setParreAnalysisId(externalResource.getParreAnalysis().getId());
        }
		
		try {
			ZipLookupData zipCodeData = (ZipLookupData)entityManager
					.createQuery("select zld from ZipLookupData zld where zld.zipCode = :zipCode")
						.setParameter("zipCode", dto.getPostalCode())
						.setMaxResults(1)
						.getSingleResult();
			dto.setCountyName(zipCodeData.getCounty());
			dto.setStateCode(zipCodeData.getStateCode());
		} catch (Exception ignore) {
			log.debug("Could not look up county and state.", ignore);
		}
		
		return dto;
	}

	public static EarthquakeProfileDTO findEarthquakeProfileById(Long earthquakeProfileId, EntityManager entityManager) {
		return createEarthquakeProfileDTO(entityManager.find(EarthquakeProfile.class, earthquakeProfileId));
	}

	public static EarthquakeProfileDTO saveEarthquakeProfile(EarthquakeProfileDTO dto, EntityManager entityManager) {
		EarthquakeProfile profile =
				dto.getId() == null 
					? new EarthquakeProfile()
					: entityManager.find(EarthquakeProfile.class, dto.getId());
		DomainUtil.copyProperties(profile, dto);
		profile.setMagnitudeProbabilities(createMagnitudeProbabilities(dto));
		
		ParreAnalysis parreAnalysis = entityManager.find(ParreAnalysis.class, dto.getParreAnalysisId());
		if (profile.isNew()) {
			profile.setParreAnalysis(parreAnalysis);
		}
		
		if (dto.getAnalysisDefault()) {
			parreAnalysis.setDefaultEarthquakeProfile(profile);
		}
					
		entityManager.persist(profile);
		/*for (EarthquakeMagnitudeProbability probability : profile.getMagnitudeProbabilities()) {
			entityManager.persist(probability);
		}*/
		
		entityManager.flush();
		
		return createEarthquakeProfileDTO(profile);
	}
	
	private static Set<EarthquakeMagnitudeProbability> createMagnitudeProbabilities(EarthquakeProfileDTO dto) {
		Set<EarthquakeMagnitudeProbability> probabilities = new HashSet<EarthquakeMagnitudeProbability>();
		
		if (dto.getMagnitudeProbabilityDTOs() != null) {
			for (EarthquakeMagnitudeProbabilityDTO probDTO : dto.getMagnitudeProbabilityDTOs()) {
				EarthquakeMagnitudeProbability probability = new EarthquakeMagnitudeProbability();
				DomainUtil.copyProperties(probability, probDTO);
				probabilities.add(probability);
			}
		}
		return probabilities;
	}

	public static HurricaneProfileDTO findHurricaneProfileById(Long hurricaneProfileId, EntityManager entityManager) {
		return createHurricaneProfileDTO(entityManager.find(HurricaneProfile.class, hurricaneProfileId));
	}

	public static HurricaneProfileDTO createHurricaneProfileDTO(HurricaneProfile profile) {
		HurricaneProfileDTO dto = new HurricaneProfileDTO();
		DomainUtil.copyProperties(dto, profile);
		return dto;
	}

	public static HurricaneProfileDTO saveHurricaneProfile(HurricaneProfileDTO dto, EntityManager entityManager) {
		HurricaneProfile profile =
				dto.getId() == null 
					? new HurricaneProfile()
					: entityManager.find(HurricaneProfile.class, dto.getId());
		DomainUtil.copyProperties(profile, dto);
		
		ParreAnalysis parreAnalysis = entityManager.find(ParreAnalysis.class, dto.getParreAnalysisId());
		if (profile.isNew()) {
			profile.setParreAnalysis(parreAnalysis);
		}
		
		if (dto.getAnalysisDefault()) {
			parreAnalysis.setDefaultHurricaneProfile(profile);
		}
					
		entityManager.persist(profile);
		entityManager.flush();
		
		return createHurricaneProfileDTO(profile);
	}

    public static void deleteNaturalThreatAnalysisWithParreAnalysis(Long parreAnalysisId, EntityManager entityManager) {
        entityManager.createQuery("delete from NaturalThreatAnalysis nta where nta.parreAnalysis.id = :parreAnalysisId")
                                .setParameter("parreAnalysisId", parreAnalysisId).executeUpdate();
    }

    public static List<String> findCountyDataByState(String stateCodeOverride, EntityManager entityManager) {
        String queryString = "select tld.county from TornadoLookupData tld where tld.state = :stateCodeOverride";
        Query query = entityManager.createQuery(queryString);
        query.setParameter("stateCodeOverride", stateCodeOverride);
        return query.getResultList();

    }
}
