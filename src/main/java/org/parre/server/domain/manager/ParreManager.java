/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */

/* TODO: Merge this back into the LGPL version */
package org.parre.server.domain.manager;

import org.apache.log4j.Logger;
import org.parre.client.messaging.ServiceFactory;
import org.parre.server.domain.*;
import org.parre.server.domain.util.DomainUtil;
import org.parre.server.util.GeneralUtil;
import org.parre.shared.AssetDTO;
import org.parre.shared.AssetSearchDTO;
import org.parre.shared.AssetThreatDTO;
import org.parre.shared.AssetThreatLevelDTO;
import org.parre.shared.ReportSectionLookupDTO;
import org.parre.shared.ThreatDTO;
import org.parre.shared.types.AssetType;
import org.parre.shared.types.LookupType;
import org.parre.shared.types.ParreAnalysisStatusType;
import org.parre.shared.types.ThreatCategoryType;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * The Class ParreManager.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 11/18/11
 */
public class ParreManager {
    private static transient final Logger log = Logger.getLogger(ParreManager.class);

    public static User findUser(String email, EntityManager entityManager) {
        String queryString = "select user from User user where lower(user.email) = :email";
        Query query = entityManager.createQuery(queryString);
        query.setParameter("email", email.toLowerCase());
        List<User> resultList = query.getResultList();
        return getSingleResultWithNull(resultList);
    }

    public static ParreAnalysis findParreAnalysis(String name, Org org, EntityManager entityManager) {
        String queryString = "select pa from ParreAnalysis pa" +
                " where pa.name = :name and pa.org = :org";
        Query query = entityManager.createQuery(queryString);
        query.setParameter("name", name);
        query.setParameter("org", org);
        List<ParreAnalysis> resultList = query.getResultList();
        return getSingleResultWithNull(resultList);
    }

    protected static <T> T getSingleResultWithNull(List<T> resultList) {
        if (resultList.isEmpty()) {
            return null;
        }
        if (resultList.size() > 1) {
            log.warn("Wrong number of results, expected 1, actual : " + resultList.size());
        }
        return resultList.get(0);
    }
    public static List<LookupValue> getLookupValues(LookupType lookupType, EntityManager manager) {
        String queryString = "select lv from LookupValue lv where lv.lookupType = :lookupType order by lv.description";
        Query query = manager.createQuery(queryString);
        query.setParameter("lookupType", lookupType);
        return query.getResultList();
    }

    protected static String stripWildChar(String value) {
        String fixedValue = value.endsWith("*") ? value.substring(0, value.length() - 1) + "%" : value;
        fixedValue = fixedValue.toLowerCase();
        return fixedValue;
    }

    protected static boolean addCriteria(String fieldName, String propertyName, String propertyValue, boolean hasWhere, StringBuilder queryBuilder) {
        if (!hasWhere) {
            queryBuilder.append(" where");
            hasWhere = true;
        } else {
            queryBuilder.append(" and ");
        }
        queryBuilder.append(" ").append("lower(").append(fieldName).append(".").append(propertyName).append(")");
        if (propertyValue.endsWith("*")) {
            queryBuilder.append(" like ");
        } else {
            queryBuilder.append(" = ");
        }
        queryBuilder.append("'").append(stripWildChar(propertyValue)).append("'");
        return hasWhere;
    }

    public static List<Contact> getOrgContacts(Long orgId, EntityManager entityManager) {
        String queryString = "select contact from Contact contact where contact.org.id = :orgId";
        Query query = entityManager.createQuery(queryString);
        query.setParameter("orgId", orgId);
        List<Contact> resultList = query.getResultList();
        return resultList;
    }

    public static Contact getContact(Long contactId, EntityManager entityManager) {
        String queryString = "select contact from Contact contact where contact.id = :contactId";
        Query query = entityManager.createQuery(queryString);
        query.setParameter("contactId", contactId);
        Contact result = (Contact)query.getSingleResult();
        return result;
    }

    protected static void addInClause(List<Long> ids, StringBuilder queryBuilder) {
        Iterator<Long> iterator = ids.iterator();
        queryBuilder.append(" in (").append(iterator.next());
        while (iterator.hasNext()) {
            queryBuilder.append(", ").append(iterator.next());
        }
        queryBuilder.append(")");
    }

    public static AssetThreatLevel findAssetThreatLevel(Long parreAnalysisId, AssetThreatLevelDTO levelDTO, EntityManager entityManager) {
        String queryString = "select atl from AssetThreatLevel atl" +
                " where atl.parreAnalysisId = :parreAnalysisId" +
                " and atl.assetId = :assetId and atl.threatId = :threatId";
        Query query = entityManager.createQuery(queryString);
        query.setParameter("parreAnalysisId", parreAnalysisId);
        query.setParameter("assetId", levelDTO.getAssetId());
        query.setParameter("threatId", levelDTO.getThreatId());
        List<AssetThreatLevel> resultList = query.getResultList();
        return getSingleResultWithNull(resultList);
    }

    public static List<AssetThreatLevel> getAssetThreatLevels(Long parreAnalysisId, EntityManager entityManager) {
        String queryString = "select atl from AssetThreatLevel atl where atl.parreAnalysisId = :parreAnalysisId";
        Query query = entityManager.createQuery(queryString);
        query.setParameter("parreAnalysisId", parreAnalysisId);
        return query.getResultList();
    }

    public static Long getAssetThreatCount(Long parreAnalysisId, EntityManager entityManager) {
        String queryString = "select count(at) from AssetThreat at" +
                " where at.parreAnalysis.id = :parreAnalysisId and at.removed = :removed";
        Query query = entityManager.createQuery(queryString);
        query.setParameter("parreAnalysisId", parreAnalysisId);
        query.setParameter("removed", Boolean.FALSE);
        return (Long)query.getSingleResult();
    }

    protected static void setAnalysisQueryParameters(Long analysisId, Query query) {
        query.setParameter("analysisId", analysisId);
        query.setParameter("removed", Boolean.FALSE);
    }

    public static List<AssetThreat> getActiveAssetThreats(Long parreAnalysisId, EntityManager entityManager) {
        return getAssetThreats(parreAnalysisId, Boolean.FALSE, entityManager);
    }

    public static List<AssetThreat> getAssetThreats(Long parreAnalysisId, Boolean removed, EntityManager entityManager) {
        String queryString = "select at from AssetThreat at" +
                " where at.removed = :removed and at.parreAnalysis.id = :parreAnalysisId";
        Query query = entityManager.createQuery(queryString);
        query.setParameter("removed", removed);
        query.setParameter("parreAnalysisId", parreAnalysisId);
        return query.getResultList();
    }

    public static List<AssetThreat> getActiveAssetThreats(Long parreAnalysisId, ThreatCategoryType threatCategoryType, EntityManager entityManager) {
        String queryString = "select at from AssetThreat at" +
                " where at.threat.threatCategory = :threatCategory and at.parreAnalysis.id = :parreAnalysisId";
        Query query = entityManager.createQuery(queryString);
        query.setParameter("threatCategory", threatCategoryType);
        query.setParameter("parreAnalysisId", parreAnalysisId);
        return query.getResultList();
    }

    public static Long getAllAssetThreatCount(Long parreAnalysisId, EntityManager entityManager) {
        String queryString = "select count(at) from AssetThreat at" +
                " where at.parreAnalysis.id = :parreAnalysisId";
        Query query = entityManager.createQuery(queryString);
        query.setParameter("parreAnalysisId", parreAnalysisId);
        return (Long) query.getSingleResult();
    }

    public static List<AssetThreat> getAllAssetThreats(Long parreAnalysisId, EntityManager entityManager) {
        String queryString = "select at from AssetThreat at" +
                " where at.parreAnalysis.id = :parreAnalysisId";
        Query query = entityManager.createQuery(queryString);
        query.setParameter("parreAnalysisId", parreAnalysisId);
        return query.getResultList();
    }

    public static List<AssetThreatAnalysis> getAssetThreats(Long parreAnalysisId, ThreatCategoryType threatCategoryType, EntityManager entityManager) {
        String queryString = "select at from AssetThreat at" +
                " where at.removed = :removed" +
                " and at.parreAnalysis.id = :parreAnalysisId and at.threat.threatCategory = :threatCategoryType";
        Query query = entityManager.createQuery(queryString);
        query.setParameter("removed", Boolean.FALSE);
        query.setParameter("parreAnalysisId", parreAnalysisId);
        query.setParameter("threatCategoryType", threatCategoryType);
        return query.getResultList();
    }

    public static boolean isAssetInUse(Asset asset, EntityManager entityManager) {
        String queryString = "select count(at) from AssetThreat at" +
                " where at.asset = :asset";
        Query query = entityManager.createQuery(queryString);
        query.setParameter("asset", asset);
        
        return ((Long)query.getSingleResult()) > 0;
    }

    protected static void appendWhereClause(StringBuilder queryBuilder) {
        queryBuilder.append(" where e.assetThreat.removed = :removed and e.analysis.id = :analysisId");
    }

    protected static StringBuilder createAssetThreatAnalysisSelect() {
        StringBuilder queryBuilder = new StringBuilder();
        queryBuilder.append("select e.id, e.assetThreat.id, e.assetThreat.asset.assetId, e.assetThreat.asset.name").
                append(", e.assetThreat.threat.name, e.assetThreat.threat.description");
        return queryBuilder;
    }

    protected static Query createAssetAnalysisSearchQuery(AssetSearchDTO searchDTO, Long analysisId, StringBuilder builder, EntityManager entityManager) {
        boolean hasAssetId = !GeneralUtil.isNullOrEmpty(searchDTO.getAssetId());
        if (hasAssetId) {
            addCriteria("e.assetThreat.asset", "assetId", searchDTO.getAssetId(), true, builder);
        }
        boolean hasAssetName = !GeneralUtil.isNullOrEmpty(searchDTO.getAssetName());
        if (hasAssetName) {
            addCriteria("e.assetThreat.asset", "name", searchDTO.getAssetName(), true, builder);
        }
        String queryString = builder.toString();
        Query query = entityManager.createQuery(queryString);
        setAnalysisQueryParameters(analysisId, query);
        return query;
    }

    protected static Query createThreatAnalysisSearchQuery(ThreatDTO searchDTO, Long analysisId, StringBuilder builder, EntityManager entityManager) {
        boolean hasWhere = true;
        boolean hasThreatName = !GeneralUtil.isNullOrEmpty(searchDTO.getName());
        if (hasThreatName) {
            hasWhere = addCriteria("e.assetThreat.threat", "name", searchDTO.getName(), hasWhere, builder);
        }

        boolean hasDescription = !GeneralUtil.isNullOrEmpty(searchDTO.getDescription());
        if (hasDescription) {
            hasWhere = addCriteria("e.assetThreat.threat", "description", searchDTO.getDescription(), hasWhere, builder);
        }

        boolean hasHazardType = !GeneralUtil.isNullOrEmpty(searchDTO.getHazardType());
        if (hasHazardType) {
            hasWhere = addCriteria("e.assetThreat.threat", "hazardType", searchDTO.getHazardType(), hasWhere, builder);
        }

        boolean hasThreatCategory = searchDTO.getThreatCategory() != null && searchDTO.getThreatCategory() != ThreatCategoryType.NONE;
        if (hasThreatCategory) {
            if (hasWhere) {
                builder.append(" and ");
            }
            else {
                builder.append(" where ");
            }
            builder.append(" e.assetThreat.threat.threatCategory = :threatCategory");
            hasWhere = true;
        }
        builder.append(" order by e.assetThreat.asset.assetId, e.assetThreat.threat.name");
        String queryString = builder.toString();
        Query query = entityManager.createQuery(queryString);
        setAnalysisQueryParameters(analysisId, query);
        if (hasThreatCategory) {
            query.setParameter("threatCategory", searchDTO.getThreatCategory());
        }
        return query;
    }

    protected static int populateAssetThreatDTO(Object[] objects, AssetThreatDTO dto) {
        int index = 0;
        dto.setId((Long) objects[index++]);
        dto.setAssetThreatId((Long) objects[index++]);
        dto.setAssetId((String) objects[index++]);
        dto.setAssetName((String) objects[index++]);
        dto.setThreatName((String) objects[index++]);
        dto.setThreatDescription((String) objects[index++]);
        return index;
    }

    public static List<ProposedMeasure> getProposedMeasures(Long parreAnalysisId, EntityManager entityManager) {
        String queryString = "select pm from ParreAnalysis pa join pa.proposedMeasures pm" +
                " where pa.id = :parreAnalysisId";
        Query query = entityManager.createQuery(queryString);
        query.setParameter("parreAnalysisId", parreAnalysisId);
        List<ProposedMeasure> resultList = query.getResultList();
        return resultList;
    }

    public static List<ProposedMeasure> getBaselineProposedMeasures(Long parreAnalysisId, EntityManager entityManager) {
        String queryString = "select pm from ProposedMeasure pm" +
                " where pm.baselineParreAnalysis.id = :parreAnalysisId";
        Query query = entityManager.createQuery(queryString);
        query.setParameter("parreAnalysisId", parreAnalysisId);
        List<ProposedMeasure> resultList = query.getResultList();
        return resultList;
    }

    public static List<SiteLocation> getSiteLocationList(EntityManager entityManager) {
        String queryString = "select sl from SiteLocation sl";
        Query query = entityManager.createQuery(queryString);
        List<SiteLocation> resultList = query.getResultList();
        return resultList;
    }

    public static SiteLocation getSiteLocation(Long locationId, EntityManager entityManager) {
        String queryString = "select sl from SiteLocation sl where sl.id = :locationId";
        Query query = entityManager.createQuery(queryString);
        query.setParameter("locationId", locationId);
        SiteLocation result = (SiteLocation)query.getSingleResult();
        return result;
    }

    public static boolean haveUsers(EntityManager entityManager) {
        String queryString = "select count(user) from User user";
        Query query = entityManager.createQuery(queryString);
        return ((Number)query.getSingleResult()).intValue() > 0;
    }

    public static List<AssetThreatAnalysis> getAssetThreatAnalyses(Long parreAnalysisId, EntityManager entityManager) {
        String queryString = "select ata from AssetThreatAnalysis ata" +
                " where ata.parreAnalysis.id = :parreAnalysisId and ata.assetThreat.removed = :removed";
        Query query = entityManager.createQuery(queryString);
        query.setParameter("parreAnalysisId", parreAnalysisId);
        query.setParameter("removed", Boolean.FALSE);
        return query.getResultList();
    }

    public static List<CounterMeasure> getCounterMeasures(Long parreAnalysisId, EntityManager entityManager) {
        String queryString = "select cm from CounterMeasure cm" +
                " where cm.parreAnalysis.id = :parreAnalysisId";
        Query query = entityManager.createQuery(queryString);
        query.setParameter("parreAnalysisId", parreAnalysisId);
        List<CounterMeasure> resultList = query.getResultList();
        return resultList;
    }

    public static List<Long> getCounterMeasureIds(Long parreAnalysisId, EntityManager entityManager) {
        String queryString = "select cm.id from CounterMeasure cm" +
                " where cm.parreAnalysis.id = :parreAnalysisId";
        Query query = entityManager.createQuery(queryString);
        query.setParameter("parreAnalysisId", parreAnalysisId);
        List<Long> resultList = query.getResultList();
        return resultList;
    }

    public static List<DiagramAnalysis> getAllDiagramAnalysis(Long parreAnalysisId, EntityManager entityManager) {
        String queryString = "select da from DiagramAnalysis da" +
                " where da.parreAnalysis.id = :parreAnalysisId";
        Query query = entityManager.createQuery(queryString);
        query.setParameter("parreAnalysisId", parreAnalysisId);
        return query.getResultList();
    }

    public static List<PathAnalysis> getAllPathAnalysisAnalysis(Long parreAnalysisId, EntityManager entityManager) {
        String queryString = "select pa from PathAnalysis pa" +
                " where pa.parreAnalysis.id = :parreAnalysisId";
        Query query = entityManager.createQuery(queryString);
        query.setParameter("parreAnalysisId", parreAnalysisId);
        return query.getResultList();
    }

    public static List<ProxyIndication> getProxyIndications(Long parreAnalysisId, EntityManager entityManager) {
        String queryString = "select pi from ProxyIndication pi" +
                " where pi.parreAnalysis.id = :parreAnalysisId";
        Query query = entityManager.createQuery(queryString);
        query.setParameter("parreAnalysisId", parreAnalysisId);
        return query.getResultList();
    }

    public static void removeCurrentParreAnalysisFromUsers(Long parreAnalysisId, EntityManager entityManager) {
        entityManager.createQuery("update User u set u.currentAnalysis = null where u.currentAnalysis.id = :parreAnalysisId")
                                .setParameter("parreAnalysisId", parreAnalysisId).executeUpdate();
    }

	@SuppressWarnings("unchecked")
	public static List<ReportSectionLookupDTO> getReportSectionValues(EntityManager entityManager) {
		//List<ReportSectionLookup> entities = entityManager.createQuery("select rsl from ReportSectionLookup rsl order by rsl.displayOrder").getResultList();
		String queryString = "select rsl from ReportSectionLookup rsl"
				+ " where rsl.reportDisplay = :reportDisplay order by rsl.displayOrder";
		Query query = entityManager.createQuery(queryString);
		query.setParameter("reportDisplay", Boolean.FALSE);
		List<ReportSectionLookup> entities = query.getResultList();
		List<ReportSectionLookupDTO> dtos = new ArrayList<ReportSectionLookupDTO>();
		for (ReportSectionLookup rsl : entities) {
			ReportSectionLookupDTO dto = new ReportSectionLookupDTO();
			DomainUtil.copyProperties(dto, rsl);
			dtos.add(dto);
		}
		return dtos;
	}
	
}
