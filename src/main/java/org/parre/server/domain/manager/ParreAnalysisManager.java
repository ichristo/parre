/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.server.domain.manager;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.parre.server.domain.ParreAnalysis;
import org.parre.shared.types.ParreAnalysisStatusType;


/**
 * The Class ParreAnalysisManager.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 11/23/11
 */
public class ParreAnalysisManager extends ParreManager {

    public static List<ParreAnalysis> getNotArchivedAnalyses(Long orgId, EntityManager entityManager) {
        String queryString = "select pa from ParreAnalysis pa" +
                " where pa.status <> :status and pa.org.id = :orgId and pa.baseline is null";
        Query query = entityManager.createQuery(queryString);
        query.setParameter("status", ParreAnalysisStatusType.ARCHIVED);
        query.setParameter("orgId", orgId);
        List<ParreAnalysis> resultList = query.getResultList();
        return resultList;
    }

    public static List<ParreAnalysis> getActiveParreAnalyses(Long orgId, EntityManager entityManager) {
        String queryString = "select pa from ParreAnalysis pa" +
                " where (pa.status <> :status) and pa.org.id = :orgId order by pa.createdTime desc ";
        Query query = entityManager.createQuery(queryString);
        query.setParameter("status", ParreAnalysisStatusType.ACTIVE);
        query.setParameter("orgId", orgId);
        List<ParreAnalysis> resultList = query.getResultList();
        return resultList;
    }

    public static void deleteProposedMeasures(Long parreAnalysisId, EntityManager entityManager) {
        entityManager.createQuery("delete from ProposedMeasure pm where pm.baselineParreAnalysis.id = :parreAnalysisId")
                                .setParameter("parreAnalysisId", parreAnalysisId).executeUpdate();
    }
}
