/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.server.domain.manager;

import org.apache.log4j.Logger;
import org.parre.server.domain.ParreAnalysis;
import org.parre.server.domain.RiskBenefitAnalysis;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;

/**
 * The Class RiskBenefitManager.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 3/2/12
 */
public class RiskBenefitManager extends ParreManager {
    private static transient final Logger log = Logger.getLogger(RiskBenefitManager.class);

    public static RiskBenefitAnalysis getRiskBenefitAnalysis(ParreAnalysis parreAnalysis, EntityManager entityManager) {
        String queryString = "select rba from RiskBenefitAnalysis rba" +
                " where rba.parreAnalysis = :parreAnalysis";
        Query query = entityManager.createQuery(queryString);
        query.setParameter("parreAnalysis", parreAnalysis);
        List<RiskBenefitAnalysis> resultList = query.getResultList();
        return getSingleResultWithNull(resultList);
    }

    public static void deleteRiskBenefitAnalysisFromParreAnalysis(Long parreAnalysisId, EntityManager entityManager) {
        String queryString = "delete from RiskBenefitAnalysis rba where rba.parreAnalysis.id = :parreAnalysisId";
        Query query = entityManager.createQuery(queryString);
        query.setParameter("parreAnalysisId", parreAnalysisId);
        int deleted = query.executeUpdate();
        log.debug("Removed " + deleted  + " RiskBenefitAnalysis");
    }

}
