/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */

/* TODO: Merge this back into the LGPL version */
package org.parre.server.domain.manager;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.apache.log4j.Logger;
import org.parre.server.ParreDatabase;
import org.parre.server.domain.*;
import org.parre.shared.*;
import org.parre.shared.util.ThreatTypeUtil;
import org.hibernate.annotations.ResultCheckStyle;

/**
 * The Class RiskResilienceManager.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 2/16/12
 */
public class RiskResilienceManager extends ParreManager {
    private static transient final Logger log = Logger.getLogger(RiskResilienceManager.class);

    
    public static RiskResilienceAnalysis getCurrentAnalysis(Long parreAnalysisId, EntityManager entityManager) {
        String queryString = "select rra from RiskResilienceAnalysis rra where rra.parreAnalysis.id = :parreAnalysisId";
        Query query = entityManager.createQuery(queryString);
        query.setParameter("parreAnalysisId", parreAnalysisId);
        List<RiskResilienceAnalysis> resultList = query.getResultList();
        return getSingleResultWithNull(resultList);
    }

    public static List<RiskResilienceDTO> getRiskResilienceDTOs(Long parreAnalysisId, EntityManager entityManager) {
    	String queryString = "select rr.id, rr.assetThreat.id, rr.assetThreat.asset.assetId, rr.assetThreat.asset.name" +
    			", rr.assetThreat.threat.name,  rr.assetThreat.threat.description" +
    			", ata.vulnerability, ata.lot, ata.durationDays, ata.severityMgd" +
    			", ata.economicImpact, ata.financialTotal " +
    			", ata.fatalities, ata.seriousInjuries, ata.financialImpact, ata.totalRisk" +
    			" from RiskResilience rr, AssetThreatAnalysis ata" +
    			" where ata.assetThreat = rr.assetThreat and rr.assetThreat.removed = :removed" +
    			" and rr.parreAnalysis.id = :parreAnalysisId and ata.parreAnalysis.id = :parreAnalysisId " +
    			" order by rr.assetThreat.id";
    	
    	Query query = entityManager.createQuery(queryString);
    	return createDTOs(parreAnalysisId, query, entityManager);
    }

    
    public static List<RiskResilienceDTO> createDTOs(Long parreAnalysisId, Query query, EntityManager entityManager) {
        query.setParameter("removed", Boolean.FALSE);
        query.setParameter("parreAnalysisId", parreAnalysisId);
        List<Object[]> records = query.getResultList();
               
        List<RiskResilienceDTO> atRras = new ArrayList<RiskResilienceDTO>(records.size());
        for (Object[] record : records) {
            RiskResilienceDTO dto = createRiskResilienceDTO(-1, record, parreAnalysisId, entityManager);
            atRras.add(dto);
        }
        return atRras;
    }


    private static Map<Long, List<RiskResilienceDTO>> createParreAnalysisDTOs(Query query, EntityManager entityManager) {
        query.setParameter("removed", Boolean.FALSE);
        List<Object[]> records = query.getResultList();
        Map<Long, List<RiskResilienceDTO>> rrMap = new HashMap<Long, List<RiskResilienceDTO>>();
        for (Object[] record : records) {
            Long parreAnalysisId = (Long) record[0];
            List<RiskResilienceDTO> atRras = rrMap.get(parreAnalysisId);
            if (atRras == null) {
                atRras = new ArrayList<RiskResilienceDTO>();
                rrMap.put(parreAnalysisId, atRras);
            }
            RiskResilienceDTO dto = createRiskResilienceDTO(0, record, parreAnalysisId, entityManager);
            atRras.add(dto);
        }
        return rrMap;
    }

    private static RiskResilienceDTO createRiskResilienceDTO(int colNum, Object[] record, Long parreAnalysisId, EntityManager entityManager) {
        RiskResilienceDTO dto = new RiskResilienceDTO();
        dto.setId((Long) record[++colNum]);
        dto.setAssetThreatId((Long) record[++colNum]);
        dto.setAssetId((String) record[++colNum]);
        dto.setAssetName((String) record[++colNum]);
        dto.setThreatName((String) record[++colNum]);
        dto.setThreatDescription((String) record[++colNum]);
        dto.setVulnerability((BigDecimal) record[++colNum]);
        dto.setLot((BigDecimal) record[++colNum]);
        dto.setDurationDays((BigDecimal) record[++colNum]);
        dto.setSeverityMgd((BigDecimal) record[++colNum]);
        dto.setEconomicResilience(((Amount) record[++colNum]).createDTO());
        
        dto.setFinancialTotal(((Amount) record[++colNum]).createDTO());
        
        dto.setFatalities(new BigDecimal((Integer)record[++colNum]));
        dto.setSeriousInjuries(new BigDecimal((Integer)record[++colNum]));

        dto.setOwnersFinancialImpact(((Amount) record[++colNum]).getQuantity());
        
        Amount totalRisk = (Amount)record[++colNum];
        if (ThreatTypeUtil.isNaturalThreat(dto.getThreatName())) {
        	dto.setRisk(totalRisk == null ? BigDecimal.ZERO : totalRisk.getQuantity());
        } else {
	        dto.calculateRisk();
        }
        
        return dto;
    }

    public static List<UriResponseDTO> getUriResponseDTOs(Long riskResilienceAnalysisId, EntityManager entityManager) {
        String queryString = "select uri.id, uri.uriOption.question.id, uri.uriOption.id, uri.uriOption.question.name, uri.uriOption.name, uri.value, uri.weight" +
                " from UriResponse uri where uri.analysis.id = :riskResilienceAnalysisId";
        Query query = entityManager.createQuery(queryString);
        query.setParameter("riskResilienceAnalysisId", riskResilienceAnalysisId);
        List<Object[]> resultList = query.getResultList();
        List<UriResponseDTO> dtos = new ArrayList<UriResponseDTO>(resultList.size());
        for (Object[] record : resultList) {
            UriResponseDTO dto = new UriResponseDTO();
            int index = -1;
            dto.setId((Long) record[++index]);
            dto.setUriQuestionId((Long) record[++index]);
            dto.setUriOptionId((Long) record[++index]);
            dto.setQuestionName((String) record[++index]);
            dto.setOptionName((String) record[++index]);
            dto.setValue((BigDecimal) record[++index]);
            dto.setWeight((BigDecimal) record[++index]);
            dtos.add(dto);
        }
        return dtos;
    }

    public static UriQuestionDTO getUriQuestionDTO(Long questionId, EntityManager entityManager) {
        String queryString = "select uriq from UriQuestion uriq where uriq.id = :questionId";
        Query query = entityManager.createQuery(queryString);
        query.setParameter("questionId", questionId);
        UriQuestion result = (UriQuestion) query.getSingleResult();
        UriQuestionDTO dto = new UriQuestionDTO();
        dto.setId(result.getId());
        dto.setType(result.getType());
        dto.setName(result.getName());
        dto.setDescription(result.getDescription());
        return dto;
    }

    public static Map<Long, List<RiskResilienceDTO>> getRiskResilienceDTOs(List<Long> parreAnalysisIds, EntityManager entityManager) {
        String queryString = "select rr.parreAnalysis.id, rr.id, rr.assetThreat.id" +
                ", rr.assetThreat.asset.assetId, rr.assetThreat.asset.name" +
                ", rr.assetThreat.threat.name,  rr.assetThreat.threat.description" +
                ", ata.vulnerability, ata.lot, ata.durationDays, ata.severityMgd" +
                ", ata.economicImpact, ata.financialTotal, ata.fatalities, ata.seriousInjuries, ata.financialImpact, ata.totalRisk" +
                " from RiskResilience rr, AssetThreatAnalysis ata" +
                " where (ata.assetThreat = rr.assetThreat and ata.parreAnalysis = rr.parreAnalysis)" +
                " and rr.assetThreat.removed = :removed";
        StringBuilder queryBuilder = new StringBuilder().append(queryString).
                append("  and rr.parreAnalysis.id");
        addInClause(parreAnalysisIds, queryBuilder);
        queryString = queryBuilder.toString();
        Query query = entityManager.createQuery(queryString);
        return createParreAnalysisDTOs(query, entityManager);
    }

    public static List<UriQuestion> getUriQuestions(EntityManager entityManager) {
        String queryString = "select uriq from UriQuestion uriq";
        Query query = entityManager.createQuery(queryString);
        List<UriQuestion> resultList = query.getResultList();
        return resultList;
    }

    public static Map<Long, Map<Long, BigDecimal>> getLot(List<Long> parreAnalysisIds, EntityManager entityManager) {
        String queryString = "select rr.parreAnalysis.id, rr.assetThreat.id, avg(ata.lot)" +
                " from RiskResilience rr, AssetThreatAnalysis ata" +
                " where (ata.assetThreat = rr.assetThreat and ata.parreAnalysis = rr.parreAnalysis)" +
                " and not exists (select 1 from Dp dp where dp.id = ata.id and dp.type = 'MAIN_WITH_SUB')" +
                " and rr.assetThreat.removed = :removed";
        queryString = addParreAnalysisIds(parreAnalysisIds, queryString);
        queryString = addGroupBy(queryString);
        Query query = entityManager.createQuery(queryString);
        query.setParameter("removed", Boolean.FALSE);
        return createParreAnalysisAssetThreatQuantities(query);
    }

    public static Map<Long, Map<Long, BigDecimal>> getVulnerability(List<Long> parreAnalysisIds, EntityManager entityManager) {
        String queryString = "select rr.parreAnalysis.id, rr.assetThreat.id, avg(ata.vulnerability)" +
                " from RiskResilience rr, AssetThreatAnalysis ata" +
                " where (ata.assetThreat = rr.assetThreat and ata.parreAnalysis = rr.parreAnalysis)" +
                " and not exists (select 1 from Dp dp where dp.id = ata.id and dp.type = 'MAIN_WITH_SUB')" +
                " and rr.assetThreat.removed = :removed";
        queryString = addParreAnalysisIds(parreAnalysisIds, queryString);
        queryString = addGroupBy(queryString);
        Query query = entityManager.createQuery(queryString);
        query.setParameter("removed", Boolean.FALSE);
        return createParreAnalysisAssetThreatQuantities(query);
    }

    public static Map<Long, Map<Long, BigDecimal>> getFatalities(List<Long> parreAnalysisIds, EntityManager entityManager) {
        String queryString = "select rr.parreAnalysis.id, rr.assetThreat.id, sum(ata.fatalities)" +
                " from RiskResilience rr, AssetThreatAnalysis ata" +
                " where (ata.assetThreat = rr.assetThreat and ata.parreAnalysis = rr.parreAnalysis)" +
                " and rr.assetThreat.removed = :removed";
        queryString = addParreAnalysisIds(parreAnalysisIds, queryString);
        queryString = addGroupBy(queryString);
        Query query = entityManager.createQuery(queryString);
        query.setParameter("removed", Boolean.FALSE);
        return createParreAnalysisAssetThreatQuantities(query);
    }
    
    public static Map<Long, Map<Long, BigDecimal>> getSeriousInjuries(List<Long> parreAnalysisIds, EntityManager entityManager) {
    	String queryString = "select rr.parreAnalysis.id, rr.assetThreat.id, sum(ata.seriousInjuries)" +
    			" from RiskResilience rr, AssetThreatAnalysis ata" +
    			" where (ata.assetThreat = rr.assetThreat and ata.parreAnalysis = rr.parreAnalysis)" +
    			" and rr.assetThreat.removed = :removed";
    	queryString = addParreAnalysisIds(parreAnalysisIds, queryString);
    	queryString = addGroupBy(queryString);
    	Query query = entityManager.createQuery(queryString);
    	query.setParameter("removed", Boolean.FALSE);
    	return createParreAnalysisAssetThreatQuantities(query);
    }
    
    private static String addGroupBy(String queryString) {
    	return queryString + " group by rr.parreAnalysis.id, rr.assetThreat.id";
    }
    
    public static Map<Long, Map<Long, AmountDTO>> getFinancialImpact(List<Long> parreAnalysisIds, EntityManager entityManager) {
        String queryString = "select rr.parreAnalysis.id, rr.assetThreat.id, ata.financialImpact" +
                " from RiskResilience rr, AssetThreatAnalysis ata" +
                " where (ata.assetThreat = rr.assetThreat and ata.parreAnalysis = rr.parreAnalysis)" +
                " and rr.assetThreat.removed = :removed";
        queryString = addParreAnalysisIds(parreAnalysisIds, queryString);
        Query query = entityManager.createQuery(queryString);
        query.setParameter("removed", Boolean.FALSE);
        return createParreAnalysisAssetThreatAmounts(query);
    }

    public static Map<Long, Map<Long, AmountDTO>> getFinancialTotal(List<Long> parreAnalysisIds, EntityManager entityManager) {
        String queryString = "select rr.parreAnalysis.id, rr.assetThreat.id, ata.financialTotal" +
                " from RiskResilience rr, AssetThreatAnalysis ata" +
                " where (ata.assetThreat = rr.assetThreat and ata.parreAnalysis = rr.parreAnalysis)" +
                " and rr.assetThreat.removed = :removed";
        queryString = addParreAnalysisIds(parreAnalysisIds, queryString);
        Query query = entityManager.createQuery(queryString);
        query.setParameter("removed", Boolean.FALSE);
        return createParreAnalysisAssetThreatAmounts(query);
    }

    public static Map<Long, Map<Long, AmountDTO>> getFinancialTotalWithStatValues(List<Long> allIds, EntityManager entityManager) {
        String queryString = "select rr.parreAnalysis.id, rr.assetThreat, ata.fatalities, ata.seriousInjuries, ata.financialImpact" +
                " from RiskResilience rr, AssetThreatAnalysis ata" +
                " where (ata.assetThreat = rr.assetThreat and ata.parreAnalysis = rr.parreAnalysis)" +
                " and rr.assetThreat.removed = :removed";
        queryString = addParreAnalysisIds(allIds, queryString);
        Query query = entityManager.createQuery(queryString);
        query.setParameter("removed", Boolean.FALSE);
        return createParreAnalysisAmountsForFinancialTotal(query);
    }

    /*
        each query record is a tuple {parreAnalysisId, assetThreatId, fatalities, seriousInjuries, financialImpact}
     */
    private static Map<Long, Map<Long, AmountDTO>> createParreAnalysisAmountsForFinancialTotal(Query query) {
        List<Object[]> records = (List<Object[]>) query.getResultList();
        Map<Long, Map<Long, AmountDTO>> optionMetricAmountMap = new HashMap<Long, Map<Long, AmountDTO>>();
        for (Object[] record : records) {
            Long parreAnalysisId = (Long) record[0];
            Map<Long, AmountDTO> amountMap = optionMetricAmountMap.get(parreAnalysisId);
            if (amountMap == null) {
                amountMap = new HashMap<Long, AmountDTO>();
                optionMetricAmountMap.put(parreAnalysisId, amountMap);
            }
            AssetThreat assetThreat = (AssetThreat) record[1];
            Integer fatalities = (Integer) record[2];
            Integer seriousInjuries = (Integer) record[3];
            AmountDTO financialImpact = ((Amount) record[4]).createDTO();
            BigDecimal financialTotal = SharedCalculationUtil.calculateFinancialTotal(fatalities, seriousInjuries,
                    financialImpact.getQuantity(), ParreDatabase.get().getStatisticalValuesDTO());
            
            if (log.isDebugEnabled()) { 
            	log.debug("Fatality value = " + ParreDatabase.get().getStatisticalValuesDTO().getFatalityDTO().getQuantity());
            	log.debug("Serious Injuries value = " + ParreDatabase.get().getStatisticalValuesDTO().getSeriousInjuryQuantity());
            }

            if(amountMap.containsKey(assetThreat.getId())) {
                amountMap.put(assetThreat.getId(), new AmountDTO(amountMap.get(assetThreat.getId()).getQuantity().add(financialTotal)));
            } else {
                amountMap.put(assetThreat.getId(), new AmountDTO(financialTotal));
            }
        }
        return optionMetricAmountMap;
    }

    public static Map<Long, Map<Long, AmountDTO>> getEconomicImpact(List<Long> parreAnalysisIds, EntityManager entityManager) {
        String queryString = "select rr.parreAnalysis.id, rr.assetThreat.id, ata.economicImpact" +
                " from RiskResilience rr, AssetThreatAnalysis ata" +
                " where (ata.assetThreat = rr.assetThreat and ata.parreAnalysis = rr.parreAnalysis)" +
                " and rr.assetThreat.removed = :removed";
        queryString = addParreAnalysisIds(parreAnalysisIds, queryString);
        Query query = entityManager.createQuery(queryString);
        query.setParameter("removed", Boolean.FALSE);
        return createParreAnalysisAssetThreatAmounts(query);
    }

    private static String addParreAnalysisIds(List<Long> parreAnalysisIds, String queryString) {
        StringBuilder queryBuilder = new StringBuilder().append(queryString).append("  and rr.parreAnalysis.id");
        addInClause(parreAnalysisIds, queryBuilder);
        queryString = queryBuilder.toString();
        return queryString;
    }
    
    @SuppressWarnings("unchecked")
    private static Map<Long, Map<Long, BigDecimal>> createParreAnalysisAssetThreatQuantities(Query query) {
        List<Object[]> records = (List<Object[]>) query.getResultList();
        Map<Long, Map<Long, BigDecimal>> optionMetricAmountMap = new HashMap<Long, Map<Long, BigDecimal>>();
        for (Object[] record : records) {
            Long parreAnalysisId = (Long) record[0];
            Map<Long, BigDecimal> quantityMap = optionMetricAmountMap.get(parreAnalysisId);
            if (quantityMap == null) {
                quantityMap = new HashMap<Long, BigDecimal>();
                optionMetricAmountMap.put(parreAnalysisId, quantityMap);
            }
            Long assetThreatId = (Long) record[1];
            Object value = record[2];
            BigDecimal quantity;
            quantity = new BigDecimal(String.valueOf(value));
            quantityMap.put(assetThreatId, quantity);
        }
        return optionMetricAmountMap;
    }

    @SuppressWarnings("unchecked")
    private static Map<Long, Map<Long, AmountDTO>> createParreAnalysisAssetThreatAmounts(Query query) {
        List<Object[]> records = (List<Object[]>) query.getResultList();
        Map<Long, Map<Long, AmountDTO>> optionMetricAmountMap = new HashMap<Long, Map<Long, AmountDTO>>();
        for (Object[] record : records) {
            Long parreAnalysisId = (Long) record[0];
            Map<Long, AmountDTO> amountMap = optionMetricAmountMap.get(parreAnalysisId);
            if (amountMap == null) {
                amountMap = new HashMap<Long, AmountDTO>();
                optionMetricAmountMap.put(parreAnalysisId, amountMap);
            }
            Long assetThreatId = (Long) record[1];
            AmountDTO amountDTO = ((Amount) record[2]).createDTO();
            if(amountMap.containsKey(assetThreatId)) {
                amountMap.put(assetThreatId, new AmountDTO(amountMap.get(assetThreatId).getQuantity().add(amountDTO.getQuantity())));
            }
            else {
                amountMap.put(assetThreatId, amountDTO);
            }

        }
        return optionMetricAmountMap;
    }

    public static Map<Long, UnitPriceDTO> getParreAnalysisUnitPrices(List<Long> allIds, EntityManager entityManager) {
        String queryString = "select rra.parreAnalysis.id, rra.unitPrice from RiskResilienceAnalysis rra";
        StringBuilder queryBuilder = new StringBuilder().append(queryString).append(" where rra.parreAnalysis.id");
        addInClause(allIds, queryBuilder);
        queryString = queryBuilder.toString();
        Query query = entityManager.createQuery(queryString);
        List<Object[]> resultList = query.getResultList();
        Map<Long, UnitPriceDTO> unitPriceMap = new HashMap<Long, UnitPriceDTO>(resultList.size());
        for (Object[] record : resultList) {
            Long id = (Long) record[0];
            UnitPrice unitPrice = (UnitPrice) record[1];
            unitPriceMap.put(id, unitPrice.createDTO());
        }
        return unitPriceMap;
    }

    public static List<UriResponse> getUriResponses(Long parreAnalysisId, EntityManager entityManager) {
        String queryString = "select ur from UriResponse ur" +
                " where ur.analysis.parreAnalysis.id = :parreAnalysisId";
        Query query = entityManager.createQuery(queryString);
        query.setParameter("parreAnalysisId", parreAnalysisId);
        List<UriResponse> resultList = query.getResultList();
        return resultList;
    }

    public static void deleteRiskResilienceFromParreAnalysis(Long parreAnalysisId, EntityManager entityManager) {
        String queryString = "delete from RiskResilience rr where rr.parreAnalysis.id = :parreAnalysisId";
        Query query = entityManager.createQuery(queryString);
        query.setParameter("parreAnalysisId", parreAnalysisId);
        int deleted = query.executeUpdate();
        log.debug("Removed " + deleted  + " RiskResilience");
    }

    public static void deleteURIResponses(Long riskResilienceAnalysisId, EntityManager entityManager) {
        String queryString = "delete from UriResponse urir where urir.analysis.id = :riskResilienceAnalysisId";
        Query query = entityManager.createQuery(queryString);
        query.setParameter("riskResilienceAnalysisId", riskResilienceAnalysisId);
        int deleted = query.executeUpdate();
        log.debug("Removed " + deleted  + " UriResponses");
    }

    public static void deleteRiskResilienceAnalysis(Long riskResilienceAnalysisId, EntityManager entityManager) {
        String queryString = "delete from RiskResilienceAnalysis rra where rra.id = :riskResilienceAnalysisId";
        Query query = entityManager.createQuery(queryString);
        query.setParameter("riskResilienceAnalysisId", riskResilienceAnalysisId);
        int deleted = query.executeUpdate();
        log.debug("Removed " + deleted  + " RiskResilienceAnalysis");
    }
}
