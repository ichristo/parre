/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.server.domain.manager;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.apache.log4j.Logger;
import org.parre.server.domain.Amount;
import org.parre.server.domain.DpAnalysis;
import org.parre.shared.DpDTO;
import org.parre.shared.ThreatDTO;
import org.parre.shared.types.DpType;


/**
 * The Class DpManager.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 11/30/11
 */
public class DpManager extends ParreManager {
    private static transient final Logger log = Logger.getLogger(DpManager.class);

    public static DpAnalysis getCurrentAnalysis(Long parreAnalysisId, EntityManager entityManager) {
        String queryString = "select analysis from DpAnalysis analysis" +
                " where analysis.parreAnalysis.id = :parreAnalysisId";
        Query query = entityManager.createQuery(queryString);
        query.setParameter("parreAnalysisId", parreAnalysisId);
        List<DpAnalysis> resultList = query.getResultList();
        return getSingleResultWithNull(resultList);
    }

    public static List<DpDTO> search(ThreatDTO searchDTO, Long analysisId, EntityManager entityManager) {
        StringBuilder builder = createSearchSelect();
        Query query = createThreatAnalysisSearchQuery(searchDTO, analysisId, builder, entityManager);
        return createDTOs(query);
    }

    private static List<DpDTO> createDTOs(Query query) {
        List<Object[]> resultList = query.getResultList();
        if (log.isDebugEnabled()) { log.debug("Number of results : " + resultList.size()); }
        List<DpDTO> dtos = new ArrayList<DpDTO>(resultList.size());
        for (Object[] objects : resultList) {
            DpDTO dto = new DpDTO();
            int index = 0;
            dto.setId((Long) objects[index++]);
            dto.setParentId((Long) objects[index++]);
            dto.setThreatId((Long) objects[index++]);
            dto.setAssetId((String) objects[index++]);
            dto.setAssetName((String) objects[index++]);
            dto.setName((String) objects[index++]);
            dto.setDescription((String) objects[index++]);
            dto.setType((DpType) objects[index++]);
            dto.setFatalities((Integer) objects[index++]);
            dto.setSeriousInjuries((Integer) objects[index++]);
            Amount amount = (Amount) objects[index++];
            dto.setFinancialImpactDTO(amount.createDTO());
            amount = (Amount) objects[index++];
            dto.setEconomicImpactDTO(amount.createDTO());
            dto.setVulnerability((BigDecimal) objects[index++]);
            dto.setLot((BigDecimal) objects[index++]);
            dto.setDurationDays((BigDecimal) objects[index++]);
            dto.setSeverityMgd((BigDecimal) objects[index++]);
            dtos.add(dto);
        }
        return dtos;
    }

    private static StringBuilder createSearchSelect() {
        StringBuilder queryBuilder = new StringBuilder();
        queryBuilder.append("select e.id, e.parentId, e.assetThreat.threat.id, e.assetThreat.asset.assetId, e.assetThreat.asset.name, e.name, e.description, e.type" +
                    ", e.fatalities, e.seriousInjuries, e.financialImpact, e.economicImpact, e.vulnerability, e.lot, e.durationDays, e.severityMgd" +
                    " from Dp e");
        appendWhereClause(queryBuilder);
        return queryBuilder;
    }

    public static boolean hasSubCategories(Long parentId, EntityManager entityManager) {
        String queryString = "select count(dp) from Dp dp" +
                " where dp.parentId = :parentId";
        Query query = entityManager.createQuery(queryString);
        query.setParameter("parentId", parentId);
        Number count = (Number) query.getSingleResult();
        return count.intValue() > 0;
    }

    public static void deleteDpAnalysisWithParreAnalysis(Long parreAnalysisId, EntityManager entityManager) {
        entityManager.createQuery("delete from DpAnalysis dpa where dpa.parreAnalysis.id = :parreAnalysisId")
                                .setParameter("parreAnalysisId", parreAnalysisId).executeUpdate();
    }
}
