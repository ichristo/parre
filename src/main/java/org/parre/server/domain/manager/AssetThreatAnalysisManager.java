/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.server.domain.manager;

import org.apache.log4j.Logger;
import org.parre.server.domain.AssetThreatAnalysis;
import org.parre.server.domain.DirectedThreat;
import org.parre.server.domain.ProxyIndication;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;

/**
 * The Class AssetThreatAnalysisManager.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 5/1/12
 */
public class AssetThreatAnalysisManager {
    private static transient final Logger log = Logger.getLogger(AssetThreatAnalysisManager.class);


    public static List<ProxyIndication> getParreAnalysisProxies(Long parreAnalysisId, EntityManager entityManager) {
        String queryString = "select distinct proxy from DirectedThreat dt join dt.proxyIndication proxy" +
                " where dt.parreAnalysis.id = :parreAnalysisId";
        Query query = entityManager.createQuery(queryString);
        query.setParameter("parreAnalysisId", parreAnalysisId);
        List<ProxyIndication> resultList = query.getResultList();
        return resultList;
    }
}
