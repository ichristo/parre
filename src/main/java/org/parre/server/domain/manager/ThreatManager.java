/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.server.domain.manager;

import org.apache.log4j.Logger;
import org.parre.server.domain.DetectionLikelihood;
import org.parre.server.domain.InactiveThreat;
import org.parre.server.domain.Threat;
import org.parre.server.domain.ThreatNote;
import org.parre.server.util.GeneralUtil;
import org.parre.server.util.LoggingUtil;
import org.parre.shared.NoteDTO;
import org.parre.shared.ThreatDTO;
import org.parre.shared.types.ThreatCategoryType;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import java.math.BigDecimal;
import java.util.*;

/**
 * The Class ThreatManager.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 11/18/11
 */
public class ThreatManager extends ParreManager {
    private static transient final Logger log = Logger.getLogger(ThreatManager.class);

    public static List<Threat> searchThreats(ThreatDTO searchDTO, EntityManager entityManager) {
        String queryString = "select threat from Threat threat";
        StringBuilder builder = new StringBuilder(queryString);
        boolean hasWhere = false;
        boolean hasThreatName = !GeneralUtil.isNullOrEmpty(searchDTO.getName());
        if (hasThreatName) {
            hasWhere = addCriteria("threat", "name", searchDTO.getName(), hasWhere, builder);
        }

        boolean hasDescription = !GeneralUtil.isNullOrEmpty(searchDTO.getDescription());
        if (hasDescription) {
            hasWhere = addCriteria("threat", "description", searchDTO.getDescription(), hasWhere, builder);
        }

        boolean hasHazardType = !GeneralUtil.isNullOrEmpty(searchDTO.getHazardType());
        if (hasHazardType) {
            hasWhere = addCriteria("threat", "hazardType", searchDTO.getHazardType(), hasWhere, builder);
        }
        boolean hasThreatCategory = searchDTO.getThreatCategory() != null && searchDTO.getThreatCategory() != ThreatCategoryType.NONE;
        if (hasThreatCategory) {
            if (hasWhere) {
                builder.append(" and ");
            }
            else {
                builder.append(" where ");
            }
            builder.append(" threat.threatCategory = :threatCategory");
            hasWhere = true;
        }
        
        // Order by
        builder.append(" order by threat.threatCategory, threat.hazardType, threat.name");

        queryString = builder.toString();
        LoggingUtil.debugLogMessage(log, "Query=", queryString);
        Query query = entityManager.createQuery(queryString);
        if (hasThreatCategory) {
            query.setParameter("threatCategory", searchDTO.getThreatCategory());
        }
        List<Threat> resultList = query.getResultList();
        if (log.isDebugEnabled()) { log.debug("Returning " + resultList.size() + " threats."); }
        return resultList;
    }

    public static Long getActiveThreatCount(Long parreAnalysisId, EntityManager entityManager) {
        List<Long> threatIds = getInactiveThreatIds(parreAnalysisId, entityManager);
        String queryString = "select count(t) from Threat t";
        StringBuilder builder = new StringBuilder().append(queryString);
        if (!threatIds.isEmpty()) {
            builder.append(" where t.id not");
            addInClause(threatIds, builder);
        }
        queryString = builder.toString();
        Query query = entityManager.createQuery(queryString);
        return (Long) query.getSingleResult();
    }
    
    public static Long getActiveThreatCountBaseline(Long baselineId, EntityManager entityManager) {
    	List<Long> threatIds = getInactiveThreatIds(baselineId, entityManager);
    	String queryString = "select count(t) from Threat t where";
    	StringBuilder builder = new StringBuilder().append(queryString);
    	if (!threatIds.isEmpty()) {
    		builder.append(" t.id not");
    		addInClause(threatIds, builder);
    		builder.append(" and ");
    	}
    	builder.append(" (t.baselineId is null or t.baselineId in (:defaultBaselineId, :baselineId))");
    	queryString = builder.toString();
    	Query query = entityManager.createQuery(queryString)
    									.setParameter("defaultBaselineId", -1L)
    									.setParameter("baselineId", baselineId);
    	return (Long) query.getSingleResult();
    }

    public static Long getActiveThreatCount(Long parreAnalysisId, ThreatCategoryType threatCategory, EntityManager entityManager) {
        List<Long> threatIds = getInactiveThreatIds(parreAnalysisId, entityManager);
        String queryString = "select count(t) from Threat t where t.threatCategory = :threatCategory";
        StringBuilder builder = new StringBuilder().append(queryString);
        if (!threatIds.isEmpty()) {
            builder.append(" and t.id not");
            addInClause(threatIds, builder);
        }
        queryString = builder.toString();
        Query query = entityManager.createQuery(queryString);
        query.setParameter("threatCategory", threatCategory);
        return (Long) query.getSingleResult();
    }

    public static List<Threat> getActiveThreats(Long parreAnalysisId, ThreatCategoryType threatCategory, EntityManager entityManager) {
        List<Long> threatIds = getInactiveThreatIds(parreAnalysisId, entityManager);
        String queryString = "select t from Threat t where t.threatCategory = :threatCategory";
        StringBuilder builder = new StringBuilder().append(queryString);
        if (!threatIds.isEmpty()) {
            builder.append(" and t.id not");
            addInClause(threatIds, builder);
        }
        queryString = builder.toString();
        Query query = entityManager.createQuery(queryString);
        query.setParameter("threatCategory", threatCategory);
        List<Threat> resultList;
        resultList = onlyThreatsFrom(query.getResultList(), parreAnalysisId);
        if (log.isDebugEnabled()) { log.debug ("Returning " + resultList.size() + " threats of type: " + threatCategory); }
        return resultList;
    }

    public static List<Threat> getActiveThreats(Long parreAnalysisId, EntityManager entityManager) {
        List<Long> threatIds = getInactiveThreatIds(parreAnalysisId, entityManager);
        String queryString = "select t from Threat t";
        StringBuilder builder = new StringBuilder().append(queryString);
        if (!threatIds.isEmpty()) {
            builder.append(" where t.id not");
            addInClause(threatIds, builder);
        }
        queryString = builder.toString();
        Query query = entityManager.createQuery(queryString);
        List<Threat> resultList = onlyThreatsFrom(query.getResultList(), parreAnalysisId);
        return resultList;
    }

    public static List<InactiveThreat> getInactiveThreats(long parreAnalysisId, EntityManager entityManager) {
        String queryString = "select it from InactiveThreat it" +
                " where it.parreAnalysis.id = :id";
        Query query = entityManager.createQuery(queryString);
        query.setParameter("id", parreAnalysisId);
        List<InactiveThreat> resultList = query.getResultList();
        return resultList;
    }

    public static List<Long> getInactiveThreatIds(Long parreAnalysisId, EntityManager entityManager) {
        String queryString = "select it.threat.id from InactiveThreat it" +
                " where it.parreAnalysis.id = :id";
        Query query = entityManager.createQuery(queryString);
        query.setParameter("id", parreAnalysisId);
        List<Long> resultList = query.getResultList();
        return resultList;
    }

    public static InactiveThreat findInactiveThreat(Long parreAnalysisId, Long threatId, EntityManager entityManager) {
        String queryString = "select it from InactiveThreat it" +
                " where it.parreAnalysis.id = :parreAnalysisId and it.threat.id = :threatId";
        Query query = entityManager.createQuery(queryString);
        query.setParameter("parreAnalysisId", parreAnalysisId);
        query.setParameter("threatId", threatId);
        List<InactiveThreat> resultList = query.getResultList();
        return getSingleResultWithNull(resultList);
    }
    
    @SuppressWarnings("unchecked")
	public static List<InactiveThreat> getInactiveThreats(Long threatId, EntityManager entityManager) {
    	String queryString = "select it from InactiveThreat it where it.threat.id = :threatId";
    	Query query = entityManager.createQuery(queryString);
    	query.setParameter("threatId", threatId);
    	return (List<InactiveThreat>)query.getResultList();
    }

    public static Map<Long, String> getInactiveReasons(Long parreAnalysisId, List<Long> threatIds, EntityManager entityManager) {
        if (threatIds.isEmpty()) {
            return Collections.emptyMap();
        }
        String queryString = "select it.threat.id, it.inactiveReason from InactiveThreat it" +
                " where it.parreAnalysis.id = :parreAnalysisId and it.threat.id";
        StringBuilder queryBuilder = new StringBuilder().append(queryString);
        addInClause(threatIds, queryBuilder);
        queryString = queryBuilder.toString();
        Query query = entityManager.createQuery(queryString);
        query.setParameter("parreAnalysisId", parreAnalysisId);
        List<Object[]> resultList = query.getResultList();
        Map<Long, String> threatReasonMap = new HashMap<Long, String>(resultList.size());
        for (Object[] map : resultList) {
            threatReasonMap.put((Long) map[0], (String) map[1]);
        }
        return threatReasonMap;
    }

    public static Threat findThreat(String name, EntityManager entityManager) {
        String queryString = "select threat from Threat threat" +
                " where threat.name = :name";
        Query query = entityManager.createQuery(queryString);
        query.setParameter("name", name);
        List<Threat> resultList = query.getResultList();
        return getSingleResultWithNull(resultList);
    }

    public static boolean isThreatInUse(Long threatId, EntityManager entityManager) {
        String queryString = "select count(at) from AssetThreat at" +
                " where at.threat.id = :threatId" +
        		" and at.removed = :threatRemoved";
        Query query = entityManager.createQuery(queryString);
        query.setParameter("threatId", threatId);
        query.setParameter("threatRemoved", false);
        return ((Number) query.getSingleResult()).intValue() > 0;
    }

    public static List<Threat> onlyThreatsFrom(List<Threat> threatList, Long baselineId) {
        List<Threat> resultList = new ArrayList<Threat>();
        for(Threat threat:threatList) {
            if(threat.getBaselineId().equals(-1L) || threat.getBaselineId().equals(baselineId)) {
                resultList.add(threat);
            }
        }
        return resultList;
    }

    public static List<NoteDTO> getThreatNotes(Long threatId, EntityManager entityManager) {
        String queryString = "select threatNote from ThreatNote threatNote " +
                "where threatNote.threat.id = :threatId";
        Query query = entityManager.createQuery(queryString);
        query.setParameter("threatId", threatId);
        return convertToDTO(query.getResultList());
    }

    public static List<NoteDTO> convertToDTO(List<ThreatNote> threatNotes) {
        List<NoteDTO> noteDTOs = new ArrayList<NoteDTO>();
        for(ThreatNote threat : threatNotes) {
            noteDTOs.add(threat.copyInto(new NoteDTO()));
        }
        return noteDTOs;
    }
    
    // This is a stop gap until the specification can be updated to include guidance on how to define 
    // detection likelihoods for user defined threat categories
    @SuppressWarnings("unchecked")
	public static void updateUserDefinedThreatLikelihood(Threat threat, BigDecimal likelihood, EntityManager entityManager) {
    	List<DetectionLikelihood> detectionLikelihoods = 
    			entityManager.createQuery("select dl from DetectionLikelihood dl where dl.threat = :threat")
    							.setParameter("threat", threat)
    							.getResultList();
    	
    	if (ThreatCategoryType.MAN_MADE_HAZARD.equals(threat.getThreatCategory())) {
    		if (detectionLikelihoods == null || detectionLikelihoods.size() == 0) {
    			DetectionLikelihood detectionLikelihood = new DetectionLikelihood();
    			detectionLikelihood.setLikelihood(likelihood);
    			detectionLikelihood.setThreat(threat);
    			entityManager.persist(detectionLikelihood);
    			entityManager.flush();
    		}
    	} else {
    		for (DetectionLikelihood detectionLikelihood : detectionLikelihoods) {
    			entityManager.remove(detectionLikelihood);
    		}
    	}
    }

    public static void deleteInactiveThreats(Long parreAnalysisId, EntityManager entityManager) {
        entityManager.createQuery("delete from InactiveThreat it where it.parreAnalysis.id = :parreAnalysisId")
                                .setParameter("parreAnalysisId", parreAnalysisId).executeUpdate();
    }

    public static void deleteUserMadeThreats(Long parreAnalysisId, EntityManager entityManager) {
    	int result = entityManager.createQuery("delete from DetectionLikelihood dl where dl.threat in (select t from Threat t where t.baselineId = :parreAnalysisId and t.userDefined = :true)")
    			.setParameter("parreAnalysisId", parreAnalysisId)
                .setParameter("true", true).executeUpdate();
    	
    	if (log.isDebugEnabled()) { log.debug("Removed " + result + " user defined threat likelihoods."); }
    	
    	result = entityManager.createQuery("delete from ThreatNote tn where tn.threat in (select t from Threat t where t.baselineId = :parreAnalysisId and t.userDefined = :true)")
    			.setParameter("parreAnalysisId", parreAnalysisId)
                .setParameter("true", true).executeUpdate();
    	
    	if (log.isDebugEnabled()) { log.debug("Removed " + result + " threat notes for parre analysis id: " + parreAnalysisId); }
    	
        result = entityManager.createQuery("delete from Threat t where t.baselineId = :parreAnalysisId and t.userDefined = :true")
                .setParameter("parreAnalysisId", parreAnalysisId)
                .setParameter("true", true).executeUpdate();
        
        if (log.isDebugEnabled()) { log.debug("Removed " + result + " user defined threats."); }
    }
}
