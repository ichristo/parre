/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.server.domain.manager;

import org.apache.log4j.Logger;
import org.parre.server.domain.*;
import org.parre.server.util.GeneralUtil;
import org.parre.shared.*;
import org.parre.shared.types.LotAnalysisType;
import org.parre.shared.types.ThreatCategoryType;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * The Class LotManager.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 11/30/11
 */
public class LotManager extends ParreManager {
    private static transient final Logger log = Logger.getLogger(LotManager.class);

    public static ProxyTier findProxyTier(Integer tierNumber, EntityManager entityManager) {
        String queryString = "select pt from ProxyTier pt where pt.tierNumber = :tierNumber";
        Query query = entityManager.createQuery(queryString);
        query.setParameter("tierNumber", tierNumber);
        List<ProxyTier> resultList = query.getResultList();
        return getSingleResultWithNull(resultList);
    }

    public static List<DetectionLikelihoodDTO> getDetectionLikelihoodDTOs(EntityManager entityManager) {
        String queryString = "select dl.threat.name, dl.threat.description, dl.likelihood from DetectionLikelihood dl";
        Query query = entityManager.createQuery(queryString);
        List<Object[]> resultList = query.getResultList();
        List<DetectionLikelihoodDTO> dtos = new ArrayList<DetectionLikelihoodDTO>(resultList.size());
        for (Object[] likelihood : resultList) {
            dtos.add(new DetectionLikelihoodDTO((String) likelihood[0], (String) likelihood[1], (BigDecimal) likelihood[2]));
        }
        return dtos;
    }

    public static BigDecimal getDetectionLikelihood(EntityManager entityManager, Long threatId) {
        String queryString = "select dl.likelihood from DetectionLikelihood dl" +
                " where dl.threat.id = :threatId";
        Query query = entityManager.createQuery(queryString);
        query.setParameter("threatId", threatId);
        return (BigDecimal)getSingleResultWithNull(query.getResultList());
    }

    public static List<ProxyCityDTO> getProxyCityTierDTOs(EntityManager entityManager) {
        String queryString = "select pc.id, pc.city, pc.proxyTier.tierNumber, pc.proxyTier.numberOfCities from ProxyCity pc";
        Query query = entityManager.createQuery(queryString);
        List<Object[]> resultList = query.getResultList();
        List<ProxyCityDTO> dtos = new ArrayList<ProxyCityDTO>(resultList.size());
        for (Object[] pct : resultList) {
            ProxyCityDTO proxyCityDTO = new ProxyCityDTO((Long) pct[0]);
            proxyCityDTO.setCity((String) pct[1]);
            proxyCityDTO.setTierNumber((Integer) pct[2]);
            proxyCityDTO.setNumberOfCities((Integer) pct[3]);
            dtos.add(proxyCityDTO);
        }
        return dtos;
    }

    public static List<ProxyTargetType> getProxyTargetTypes(EntityManager entityManager) {
        String queryString = "select ptt from ProxyTargetType ptt";
        Query query = entityManager.createQuery(queryString);
        List<ProxyTargetType> resultList = query.getResultList();
        return resultList;
    }

    public static List<ProxyTier> getProxyTiers(EntityManager entityManager) {
        String queryString = "select pt from ProxyTier pt";
        Query query = entityManager.createQuery(queryString);
        List<ProxyTier> resultList = query.getResultList();
        return resultList;
    }

    public static ProxyIndication findProxyIndication(Long lotId, EntityManager entityManager) {
        String queryString = "select pi from DirectedThreat lot join lot.proxyIndication pi" +
                " where lot.id = :lotId";
        Query query = entityManager.createQuery(queryString);
        query.setParameter("lotId", lotId);
        List<ProxyIndication> resultList = query.getResultList();
        return getSingleResultWithNull(resultList);
    }

    public static List<DirectedThreat> getLotsWithProxyIndication(ProxyIndication proxyIndication, EntityManager entityManager) {
        String queryString = "select dt from DirectedThreat dt" +
                " where dt.lotAnalysisType = :analysisType and dt.proxyIndication = :proxyIndication";
        Query query = entityManager.createQuery(queryString);
        query.setParameter("analysisType", LotAnalysisType.PROXY_INDICATOR);
        query.setParameter("proxyIndication", proxyIndication);
        List<DirectedThreat> resultList = query.getResultList();
        return resultList;
    }

    public static List<ProxyIndication> getExistingProxyIndications(Long parreAnalysisId, EntityManager entityManager) {
        String queryString = "select pi from ProxyIndication pi" +
                " where pi.parreAnalysis.id = :parreAnalysisId";
        Query query = entityManager.createQuery(queryString);
        query.setParameter("parreAnalysisId", parreAnalysisId);
        List<ProxyIndication> resultList = query.getResultList();
        return resultList;
    }

}
