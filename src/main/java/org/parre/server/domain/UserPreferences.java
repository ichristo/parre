/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.server.domain;


import javax.persistence.*;

import org.parre.shared.UserPreferencesDTO;

/**
 * User: keithjones
 * Date: 6/28/12
 * Time: 1:27 PM
*/
@Entity
@Table(name = "USER_PREFERENCES")
public class UserPreferences extends BaseEntity {
    @Column(name = "USE_SCIENTIFIC_NOTATION")
    private boolean useScientificNotation;
    @OneToOne(targetEntity = User.class)
    @JoinColumn(name = "USER_ID")
    private User user;

    public boolean isUseScientificNotation() {
        return useScientificNotation;
    }

    public void setUseScientificNotation(boolean useScientificNotation) {
        this.useScientificNotation = useScientificNotation;
    }

    public UserPreferencesDTO createDTO() {
        UserPreferencesDTO dto = new UserPreferencesDTO();
        dto.setUseScientificNotation(this.useScientificNotation);
        return dto;
    }

    @Override
    public String toString() {
        return "UserPreferences{" +
                "useScientificNotation=" + useScientificNotation +
                '}';
    }
}
