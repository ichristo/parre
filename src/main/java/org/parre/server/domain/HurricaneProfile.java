/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.server.domain;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * The Class HurricaneProfile.
 */
@Entity
@Table(name = "HURRICANE_PROFILE")
public class HurricaneProfile extends NameDescription {
	@OneToOne(targetEntity = ParreAnalysis.class)
    @JoinColumn(name = "PARRE_ANALYSIS_ID")
    private ParreAnalysis parreAnalysis;
	
	@Column(name = "DEFAULT_DESIGN_SPEED", columnDefinition = "DECIMAL(10,2)")
	private BigDecimal defaultDesignSpeed;
	
	@Column(name = "CAT0_RETURN_PERIOD")
	private BigDecimal cat0ReturnPeriod = BigDecimal.ZERO;
	
	@Column(name = "CAT1_RETURN_PERIOD")
	private BigDecimal cat1ReturnPeriod = BigDecimal.ZERO;
	
	@Column(name = "CAT2_RETURN_PERIOD")
	private BigDecimal cat2ReturnPeriod = BigDecimal.ZERO;

	@Column(name = "CAT3_RETURN_PERIOD")
	private BigDecimal cat3ReturnPeriod = BigDecimal.ZERO;
	
	@Column(name = "CAT4_RETURN_PERIOD")
	private BigDecimal cat4ReturnPeriod = BigDecimal.ZERO;
	
	@Column(name = "CAT5_RETURN_PERIOD")
	private BigDecimal cat5ReturnPeriod = BigDecimal.ZERO;
	
	public ParreAnalysis getParreAnalysis() {
		return parreAnalysis;
	}

	public void setParreAnalysis(ParreAnalysis parreAnalysis) {
		this.parreAnalysis = parreAnalysis;
	}

	public BigDecimal getDefaultDesignSpeed() {
		return defaultDesignSpeed;
	}

	public void setDefaultDesignSpeed(BigDecimal defaultDesignSpeed) {
		this.defaultDesignSpeed = defaultDesignSpeed;
	}

	public BigDecimal getCat0ReturnPeriod() {
		return cat0ReturnPeriod;
	}

	public void setCat0ReturnPeriod(BigDecimal cat0ReturnPeriod) {
		this.cat0ReturnPeriod = cat0ReturnPeriod;
	}

	public BigDecimal getCat1ReturnPeriod() {
		return cat1ReturnPeriod;
	}

	public void setCat1ReturnPeriod(BigDecimal cat1ReturnPeriod) {
		this.cat1ReturnPeriod = cat1ReturnPeriod;
	}

	public BigDecimal getCat2ReturnPeriod() {
		return cat2ReturnPeriod;
	}

	public void setCat2ReturnPeriod(BigDecimal cat2ReturnPeriod) {
		this.cat2ReturnPeriod = cat2ReturnPeriod;
	}

	public BigDecimal getCat3ReturnPeriod() {
		return cat3ReturnPeriod;
	}

	public void setCat3ReturnPeriod(BigDecimal cat3ReturnPeriod) {
		this.cat3ReturnPeriod = cat3ReturnPeriod;
	}

	public BigDecimal getCat4ReturnPeriod() {
		return cat4ReturnPeriod;
	}

	public void setCat4ReturnPeriod(BigDecimal cat4ReturnPeriod) {
		this.cat4ReturnPeriod = cat4ReturnPeriod;
	}

	public BigDecimal getCat5ReturnPeriod() {
		return cat5ReturnPeriod;
	}

	public void setCat5ReturnPeriod(BigDecimal cat5ReturnPeriod) {
		this.cat5ReturnPeriod = cat5ReturnPeriod;
	}
	
	public void copyFrom(HurricaneProfile src) {
		setName(src.getName());
		defaultDesignSpeed = src.getDefaultDesignSpeed();
		cat0ReturnPeriod = src.getCat0ReturnPeriod();
		cat1ReturnPeriod = src.getCat1ReturnPeriod();
		cat2ReturnPeriod = src.getCat2ReturnPeriod();
		cat3ReturnPeriod = src.getCat3ReturnPeriod();
		cat4ReturnPeriod = src.getCat4ReturnPeriod();
		cat5ReturnPeriod = src.getCat5ReturnPeriod();
	}
}
