/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.server.domain;


import javax.persistence.*;

import org.parre.shared.CounterMeasureDTO;
import org.parre.shared.types.CounterMeasureType;

import java.util.*;

/**
 * The Class CounterMeasure.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 1/19/12
 */
@Entity
@Table(name = "COUNTER_MEASURE")
public class CounterMeasure extends NameDescription {
    @OneToOne(targetEntity = ParreAnalysis.class)
    @JoinColumn(name = "PARRE_ANALYSIS_ID")
    private ParreAnalysis parreAnalysis;
    @Column(name = "TYPE") @Enumerated(EnumType.STRING)
    private CounterMeasureType type;
    @ManyToMany(mappedBy = "diagramAnalysisCounterMeasures")
    private Set<DiagramAnalysis> diagramAnalysisSet;
    @ManyToMany(mappedBy = "expertElicitationCounterMeasures")
    private Set<DirectedThreat> directedThreatSet;
    @ManyToMany(mappedBy = "pathAnalysisCounterMeasures")
    private Set<PathAnalysis> pathAnalysisSet;

    public CounterMeasure() {
    }

    public CounterMeasure(String name, String description) {
        super(name, description);
    }

    public ParreAnalysis getParreAnalysis() {
        return parreAnalysis;
    }

    public void setParreAnalysis(ParreAnalysis parreAnalysis) {
        this.parreAnalysis = parreAnalysis;
    }

    public CounterMeasureType getType() {
        return type;
    }

    public void setType(CounterMeasureType type) {
        this.type = type;
    }

    public CounterMeasure createCopy() {
        CounterMeasure copy = copyInto(new CounterMeasure());
        copy.setId(null);
        copy.setParreAnalysis(null);
        copy.setDiagramAnalysisSet(new HashSet<DiagramAnalysis>());
        copy.setDirectedThreatSet(new HashSet<DirectedThreat>());
        copy.setPathAnalysisSet(new HashSet<PathAnalysis>());
        return copy;
    }

    public static List<CounterMeasureDTO> createDTOs(Collection<CounterMeasure> entities) {
        List<CounterMeasureDTO> dtos = new ArrayList<CounterMeasureDTO>(entities.size());
        for (CounterMeasure entity : entities) {
            dtos.add(entity.copyInto(new CounterMeasureDTO()));
        }
        return dtos;
    }

    public static Set<CounterMeasureDTO> createSetOfDTOs(Set<CounterMeasure> counterMeasures) {
        Set<CounterMeasureDTO> dtos = new HashSet<CounterMeasureDTO>();
        for(CounterMeasure counterMeasure : counterMeasures) {
            dtos.add(counterMeasure.copyInto(new CounterMeasureDTO()));
        }
        return dtos;
    }

    public Set<DiagramAnalysis> getDiagramAnalysisSet() {
        return diagramAnalysisSet;
    }

    public void setDiagramAnalysisSet(Set<DiagramAnalysis> diagramAnalysisSet) {
        this.diagramAnalysisSet = diagramAnalysisSet;
    }

    public void addDiagramAnalysis(DiagramAnalysis diagramAnalysis) {
        if(this.diagramAnalysisSet == null) {
            this.diagramAnalysisSet = new HashSet<DiagramAnalysis>();
        }
        this.diagramAnalysisSet.add(diagramAnalysis);
    }

    public Set<DirectedThreat> getDirectedThreatSet() {
        return directedThreatSet;
    }

    public void setDirectedThreatSet(Set<DirectedThreat> directedThreatSet) {
        this.directedThreatSet = directedThreatSet;
    }
    public void addDirectedThreat(DirectedThreat directedThreat) {
        if(this.directedThreatSet == null) {
            this.directedThreatSet = new HashSet<DirectedThreat>();
        }
        this.directedThreatSet.add(directedThreat);
    }

	@Override
	public String toString() {
		return "CounterMeasure [type=" + type + ", getName()=" + getName()
				+ ", getDescription()=" + getDescription() + ", getId()="
				+ getId() + "]";
	}


    public Set<PathAnalysis> getPathAnalysisSet() {
        return pathAnalysisSet;
    }

    public void setPathAnalysisSet(Set<PathAnalysis> pathAnalysisSet) {
        if(this.pathAnalysisSet == null) {
            this.pathAnalysisSet = new HashSet<PathAnalysis>();
        }
        this.pathAnalysisSet = pathAnalysisSet;
    }

    public void addPathAnalysis(PathAnalysis pathAnalysis) {
        if(this.pathAnalysisSet == null) {
            this.pathAnalysisSet = new HashSet<PathAnalysis>();
        }
        this.pathAnalysisSet.add(pathAnalysis);
    }
}
