/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.server.domain;


import javax.persistence.*;

import org.parre.shared.ContactDTO;
import org.parre.shared.LabelValueDTO;
import org.parre.shared.NameDescriptionDTO;

/**
 * The Class Contact.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 11/15/11
 */
@Entity
@Table(name = "CONTACT")
public class Contact extends BaseEntity {
    @Column(name = "CONTACT_NAME")
    private String contactName;
    @Column(name = "PHONE")
    private String phone;
    @Column(name = "WORK_PHONE")
    private String workPhone;
    @Column(name = "MOBILE_PHONE")
    private String mobilePhone;
    @Column(name = "FAX")
    private String fax;
    @Column(name = "email")
    private String email;
    @OneToOne(targetEntity = Address.class)
    @JoinColumn(name = "ADDRESS_ID")
    private Address address;
    @OneToOne(targetEntity = Org.class)
    @JoinColumn(name = "ORG_ID")
    private Org org;

    public String getContactName() {
        return contactName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getWorkPhone() {
        return workPhone;
    }

    public void setWorkPhone(String workPhone) {
        this.workPhone = workPhone;
    }

    public String getMobilePhone() {
        return mobilePhone;
    }

    public void setMobilePhone(String mobilePhone) {
        this.mobilePhone = mobilePhone;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public Org getOrg() {
        return org;
    }

    public void setOrg(Org org) {
        this.org = org;
    }

    public NameDescriptionDTO createNameDescriptionDTO() {
        return copyInto(new NameDescriptionDTO(getContactName(), ""));
    }

    public LabelValueDTO createLabelValueDTO() {
        return new LabelValueDTO(getContactName(), String.valueOf(getId()));
    }

    public ContactDTO createDTO() {
        return this.copyInto(new ContactDTO());
    }
}
