/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.server.domain;


import javax.persistence.*;

import org.parre.shared.AmountDTO;
import org.parre.shared.RiskBenefitDTO;

import java.util.Set;

/**
 * The Class RiskBenefit.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 2/16/12
 */
@Entity
@Table(name = "RISK_BENEFIT")
public class RiskBenefit extends AssetThreatEntity {
    @ManyToOne(targetEntity = RiskBenefitAnalysis.class)
    @JoinColumn(name = "ANALYSIS_ID")
    private RiskBenefitAnalysis analysis;
    @Embedded
    @AttributeOverrides( {
            @AttributeOverride(name="quantity", column = @Column(name="BASELINE_RISK_Q", columnDefinition = "DECIMAL(10,2)") ),
            @AttributeOverride(name="unit", column = @Column(name="BASELINE_RISK_U") )
    } )
    private Amount baselineRisk;

    @OneToMany(mappedBy = "riskBenefit", cascade = {CascadeType.REMOVE})
    private Set<RiskBenefitOption> options;

    public RiskBenefit() {
    }

    public RiskBenefit(AssetThreat assetThreat) {
        super(assetThreat);
    }

    public RiskBenefitAnalysis getAnalysis() {
        return analysis;
    }

    public void setAnalysis(RiskBenefitAnalysis analysis) {
        this.analysis = analysis;
    }

    public Amount getBaselineRisk() {
        return baselineRisk;
    }

    public void setBaselineRisk(Amount baselineRisk) {
        this.baselineRisk = baselineRisk;
    }

    public AmountDTO getBaselineRiskDTO() {
        return baselineRisk.createDTO();
    }

    public Set<RiskBenefitOption> getOptions() {
        return options;
    }

    public void setOptions(Set<RiskBenefitOption> options) {
        this.options = options;
    }

    public void addOption(RiskBenefitOption option) {
        options = add(option, options);
        option.setRiskBenefit(this);
    }

    public RiskBenefitDTO createDTO() {
        RiskBenefitDTO riskBenefitDTO = new RiskBenefitDTO();
        populate(riskBenefitDTO);
        riskBenefitDTO.updateId();
        riskBenefitDTO.setBaselineAmountDTO(getBaselineRiskDTO());
        for (RiskBenefitOption option : options) {
            riskBenefitDTO.addRiskBenefitOptionDTO(option.createDTO());
        }
        riskBenefitDTO.sort();
        return riskBenefitDTO;
    }
}
