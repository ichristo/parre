/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.server.domain;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.parre.shared.HurricaneLookupDataDTO;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * User: keithjones
 * Date: 1/30/12
 * Time: 10:49 AM
*/
@Entity
@Table(name="HURRICANE_LOOKUP_DATA")
public class HurricaneLookupData extends BaseEntity {
    @Column(name = "CATEGORY")
    private String category;
    @Column(name = "LOWER_WIND_SPEED", columnDefinition = "DECIMAL(10,2)")
    private BigDecimal lowerWindSpeed;
    @Column(name = "UPPER_WIND_SPEED", columnDefinition = "DECIMAL(10,2)")
    private BigDecimal upperWindSpeed;

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public BigDecimal getLowerWindSpeed() {
        return lowerWindSpeed;
    }

    public void setLowerWindSpeed(BigDecimal lowerWindSpeed) {
        this.lowerWindSpeed = lowerWindSpeed;
    }

    public BigDecimal getUpperWindSpeed() {
        return upperWindSpeed;
    }

    public void setUpperWindSpeed(BigDecimal upperWindSpeed) {
        this.upperWindSpeed = upperWindSpeed;
    }

    public static List<HurricaneLookupDataDTO> createDTOs(List<HurricaneLookupData> entities) {
        List<HurricaneLookupDataDTO> dtos = new ArrayList<HurricaneLookupDataDTO>(entities.size());
        for (HurricaneLookupData entity : entities) {
            dtos.add(entity.copyInto(new HurricaneLookupDataDTO()));
        }
        return dtos;
    }

    @Override
    public String toString() {
        return "HurricaneCalcDTO{" +
                "category='" + category + '\'' +
                ", lowerWindSpeed=" + lowerWindSpeed +
                ", upperWindSpeed=" + upperWindSpeed +
                '}';
    }


}
