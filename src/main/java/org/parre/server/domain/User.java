/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.server.domain;

import javax.persistence.*;
import java.util.Set;

/**
 * User: Swarn S. Dhaliwal
 * Date: 8/20/11
 * Time: 1:59 PM
*/
@Entity
@Table(name = "USER", uniqueConstraints = @UniqueConstraint(columnNames = {"EMAIL"}))
public class User extends BaseEntity {
    @Column(name = "EMAIL")
    private String email;
    @Column(name = "PASSWORD")
    private String password;
    @Column(name = "NICK_NAME")
    private String nickname;
    @ManyToMany(targetEntity = Role.class)
    @JoinTable(name = "USER_ROLE", joinColumns = {@JoinColumn(name = "USER_ID")},
        inverseJoinColumns = {@JoinColumn(name = "ROLE_ID")})
    private Set<Role> roles;
    @OneToOne(targetEntity = Org.class)
    @JoinColumn(name = "ORG_ID")
    private Org org;
    @OneToOne (targetEntity = ParreAnalysis.class)
    @JoinColumn (name = "PARRE_ANALYSIS_ID")
    private ParreAnalysis currentAnalysis;
    @OneToOne (targetEntity = UserPreferences.class)
    @JoinColumn (name = "PREF_ID")
    private UserPreferences userPreferences;

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public Set<Role> getRoles() {
        return roles;
    }

    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }

    public Org getOrg() {
        return org;
    }

    public void setOrg(Org org) {
        this.org = org;
    }

    public ParreAnalysis getCurrentAnalysis() {
        return currentAnalysis;
    }

    public void setCurrentAnalysis(ParreAnalysis currentAnalysis) {
        this.currentAnalysis = currentAnalysis;
    }

    @Override
    public int hashCode() {
        return email.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        return ((User) obj).email.equals(email);
    }


    @Override
    public String toString() {
        return "User{" +
                "email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", nickname='" + nickname + '\'' +
                ", roles=" + roles +
                ", org=" + org +
                ", currentAnalysis=" + currentAnalysis +
                ", userPreferences=" + userPreferences +
                '}';
    }

    public UserPreferences getUserPreferences() {
        return userPreferences;
    }

    public void setUserPreferences(UserPreferences userPreferences) {
        this.userPreferences = userPreferences;
    }
}
