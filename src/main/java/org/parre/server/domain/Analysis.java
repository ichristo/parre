/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.server.domain;



import javax.persistence.*;

import org.parre.shared.NameDescriptionDTO;

import java.util.Collection;
import java.util.Date;
import java.util.Set;

/**
 * The Class Analysis.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 9/19/11
 */
@MappedSuperclass
public abstract class Analysis extends NameDescription {
    @OneToOne(targetEntity = ParreAnalysis.class)
    @JoinColumn(name = "PARRE_ANALYSIS_ID")
    private ParreAnalysis parreAnalysis;
    @Column(name = "STARTED_DATE") @Temporal(value = TemporalType.DATE)
    private Date startedDate;

    protected Analysis() {
    }

    public Analysis(NameDescriptionDTO dto) {
        super(dto);
    }

    public Analysis(String name, String description) {
        super(name, description);
        this.startedDate = new Date();
    }

    public Date getStartedDate() {
        return startedDate;
    }

    public void setStartedDate(Date startedDate) {
        this.startedDate = startedDate;
    }

    public ParreAnalysis getParreAnalysis() {
        return parreAnalysis;
    }

    public void setParreAnalysis(ParreAnalysis parreAnalysis) {
        this.parreAnalysis = parreAnalysis;
    }

    public void addAssetThreats(Collection<? extends AssetThreatEntity> assetThreats) {
        for (AssetThreatEntity assetThreat : assetThreats) {
            addAssetThreat(assetThreat);
        }
    }
    public abstract Set<? extends AssetThreatEntity> getAssetThreats();
    public abstract void addAssetThreat(AssetThreatEntity assetThreat);
}
