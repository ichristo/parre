/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.server.domain;


import java.math.BigDecimal;

import javax.persistence.AttributeOverrides;
import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 * User: Swarn S. Dhaliwal
 * Date: 7/14/11
 * Time: 11:20 AM
*/
@Entity
@DiscriminatorValue(value = "NATURAL_THREAT")
public class NaturalThreat extends AssetThreatAnalysis {
    @ManyToOne (targetEntity = NaturalThreatAnalysis.class)
    @JoinColumn(name = "NT_ANALYSIS_ID")
    private NaturalThreatAnalysis analysis;
    
    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name = "quantity", column = @Column(name = "OPERATIONAL_LOSS_QUANTITY", columnDefinition = "DECIMAL(19,6)")),
            @AttributeOverride(name = "unit", column = @Column(name = "OPERATIONAL_LOSS_UNIT"))
    })
    private Amount operationalLoss = Amount.ZERO;
    
    @Column(name = "NT_MANUALLY_ENTERED")
    private Boolean manuallyEntered = false;
    
    @Column(name = "NT_USE_DEFAULT_VALUES")
    private Boolean useDefaultValues = true;
    
    @Column(name = "NT_LATITUDE_OVERRIDE", columnDefinition = "DECIMAL(10,5)")
    private BigDecimal latitudeOverride;
    
    @Column(name = "NT_LONGITUDE_OVERRIDE", columnDefinition = "DECIMAL(10,5)")
    private BigDecimal longitudeOverride;
    
    @Column(name = "NT_COUNTY_OVERRIDE")
    private String countyOverride;
    
    @Column(name = "NT_STATE_CODE_OVERRIDE")
    private String stateCodeOverride;
    
    @Column(name = "NT_FLOOD_RISK")
    private Boolean floodRisk;
    
    @ManyToOne (targetEntity = EarthquakeProfile.class, optional=true)
    @JoinColumn(name = "NT_EARTHQUAKE_PROFILE_ID")
    private EarthquakeProfile earthquakeProfile;
    
    @ManyToOne (targetEntity = HurricaneProfile.class, optional=true)
    @JoinColumn(name = "NT_HURRICANE_PROFILE_ID")
    private HurricaneProfile hurricaneProfile;
    
    @Column(name = "DESIGN_SPEED", columnDefinition = "DECIMAL(10,2)")
    private BigDecimal designSpeed;
    
    public NaturalThreat() {
    }

    public NaturalThreat(AssetThreat assetThreat) {
        super(assetThreat);
    }

    public Analysis getAnalysis() {
        return analysis;
    }

    public void setAnalysis(NaturalThreatAnalysis analysis) {
        this.analysis = analysis;
    }
    
	public Amount getOperationalLoss() {
		return operationalLoss;
	}

	public void setOperationalLoss(Amount operationalLoss) {
		this.operationalLoss = operationalLoss;
	}

	public Boolean getManuallyEntered() {
        if(manuallyEntered == null) {
            manuallyEntered = true;
        }
		return manuallyEntered;
	}

	public void setManuallyEntered(Boolean manuallyEntered) {
		this.manuallyEntered = manuallyEntered;
	}

	public Boolean getUseDefaultValues() {
		return useDefaultValues;
	}

	public void setUseDefaultValues(Boolean useDefaultValues) {
		this.useDefaultValues = useDefaultValues;
	}

	public BigDecimal getLatitudeOverride() {
		return latitudeOverride;
	}

	public void setLatitudeOverride(BigDecimal latitudeOverride) {
		this.latitudeOverride = latitudeOverride;
	}

	public BigDecimal getLongitudeOverride() {
		return longitudeOverride;
	}

	public void setLongitudeOverride(BigDecimal longitudeOverride) {
		this.longitudeOverride = longitudeOverride;
	}

	public String getCountyOverride() {
		return countyOverride;
	}

	public void setCountyOverride(String countyOverride) {
		this.countyOverride = countyOverride;
	}

	public String getStateCodeOverride() {
		return stateCodeOverride;
	}

	public void setStateCodeOverride(String stateCodeOverride) {
		this.stateCodeOverride = stateCodeOverride;
	}

	public Boolean getFloodRisk() {
		return floodRisk;
	}

	public void setFloodRisk(Boolean floodRisk) {
		this.floodRisk = floodRisk;
	}

	public EarthquakeProfile getEarthquakeProfile() {
		return earthquakeProfile;
	}

	public void setEarthquakeProfile(EarthquakeProfile earthquakeProfile) {
		this.earthquakeProfile = earthquakeProfile;
	}

	public HurricaneProfile getHurricaneProfile() {
		return hurricaneProfile;
	}

	public void setHurricaneProfile(HurricaneProfile hurricaneProfile) {
		this.hurricaneProfile = hurricaneProfile;
	}

	public BigDecimal getDesignSpeed() {
		return designSpeed;
	}

	public void setDesignSpeed(BigDecimal designSpeed) {
		this.designSpeed = designSpeed;
	}
	

	@Override
	public void updateFinancialTotal() {
		// do nothing
	}

	@Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("NaturalThreat");
        sb.append("{analysis=").append(analysis);
        sb.append(",designSpeed=").append(designSpeed);
        sb.append('}');
        return sb.toString();
    }
	
	

}
