/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.server.domain;


import javax.persistence.*;

import org.parre.shared.NameDescriptionDTO;
import org.parre.shared.RiskResilienceAnalysisDTO;
import org.parre.shared.UnitPriceDTO;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.Set;

/**
 * The Class RiskResilienceAnalysis.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 2/15/12
 */
@Entity
@Table(name = "RISK_RESILIENCE_ANALYSIS")
public class RiskResilienceAnalysis extends Analysis {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@OneToMany(mappedBy = "analysis")
    private Set<RiskResilience> riskResiliences = Collections.emptySet();
	
   
    @AttributeOverrides( {
            @AttributeOverride(name="price", column = @Column(name="UP_PRICE", columnDefinition = "DECIMAL(10,2)") ),
            @AttributeOverride(name="quantity", column = @Column(name="UP_QUANTITY") )
    } )
    @Embedded private UnitPrice unitPrice;

    public RiskResilienceAnalysis() {
    }

    public RiskResilienceAnalysis(NameDescriptionDTO dto) {
        this(dto.getName(), dto.getDescription());
    }

    public RiskResilienceAnalysis(String name, String description) {
        super(name, description);
        unitPrice = new UnitPrice(BigDecimal.ONE, 1000);
    }

    @Override
    public Set<? extends AssetThreatEntity> getAssetThreats() {
        return riskResiliences;
    }

    @Override
    public void addAssetThreat(AssetThreatEntity assetThreat) {
        addRiskResilience((RiskResilience) assetThreat);
    }

    public Set<RiskResilience> getRiskResiliences() {
        return riskResiliences;
    }

    public void setRiskResiliences(Set<RiskResilience> riskResiliences) {
        this.riskResiliences = riskResiliences;
    }

    public void addRiskResilience(RiskResilience riskResilience) {
        riskResiliences = add(riskResilience, riskResiliences);
        riskResilience.setAnalysis(this);
    }

    public UnitPrice getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(UnitPrice unitPrice) {
        this.unitPrice = unitPrice;
    }

    public UnitPriceDTO getUnitPriceDTO() {
        return unitPrice.createDTO();
    }

    public void setUnitPriceDTO(UnitPriceDTO dto) {
        if (dto != null) {
            unitPrice = new UnitPrice(dto);
        }
    }

    public RiskResilienceAnalysisDTO createDTO() {
        return copyInto(new RiskResilienceAnalysisDTO());
    }
    
    public boolean hasRiskResilience(AssetThreat assetThreat) {
    	for (RiskResilience riskResilience : getRiskResiliences()) {
    		if (riskResilience.isFor(assetThreat)) { 
    			return true;
    		}
    	}
    	return false;
    }
}
