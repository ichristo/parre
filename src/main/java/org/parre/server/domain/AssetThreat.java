/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.server.domain;


import javax.persistence.*;

import org.parre.shared.AssetThreatDTO;

/**
 * The Class AssetThreat.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 2/23/12
 */
@Entity
@Table(name = "ASSET_THREAT", uniqueConstraints =
@UniqueConstraint(columnNames = {"PARRE_ANALYSIS_ID", "ASSET_ID", "THREAT_ID"}))
public class AssetThreat extends BaseEntity {
    @ManyToOne(targetEntity = ParreAnalysis.class)
    @JoinColumn(name = "PARRE_ANALYSIS_ID")
    private ParreAnalysis parreAnalysis;
    @OneToOne(targetEntity = Asset.class)
    @JoinColumn(name = "ASSET_ID")
    private Asset asset;
    @OneToOne(targetEntity = Threat.class)
    @JoinColumn(name = "THREAT_ID")
    private Threat threat;
    @Column(name = "REMOVED")
    private Boolean removed = false;

    public AssetThreat() {
    }

    public AssetThreat(ParreAnalysis parreAnalysis, Asset asset, Threat threat) {
        this.parreAnalysis = parreAnalysis;
        this.asset = asset;
        this.threat = threat;
    }

    public ParreAnalysis getParreAnalysis() {
        return parreAnalysis;
    }

    public void setParreAnalysis(ParreAnalysis parreAnalysis) {
        this.parreAnalysis = parreAnalysis;
    }

    public Asset getAsset() {
        return asset;
    }

    public void setAsset(Asset asset) {
        this.asset = asset;
    }

    public Threat getThreat() {
        return threat;
    }

    public void setThreat(Threat threat) {
        this.threat = threat;
    }

    public Boolean getRemoved() {
        return removed;
    }

    public void setRemoved(Boolean removed) {
        this.removed = removed;
    }

    public boolean isFor(Asset asset) {
        return this.asset.equals(asset);
    }

    public boolean isFor(Threat threat) {
        return this.threat.equals(threat);
    }

    public boolean isFor(Asset asset, Threat threat) {
        return isFor(asset) && isFor(threat);
    }

    public boolean matches(AssetThreatDTO assetThreatDTO) {
        return getId().equals(assetThreatDTO.getAssetThreatId());
    }

    public AssetThreat createCopyFor(ParreAnalysis optionAnalysis) {
        return new AssetThreat(optionAnalysis, getAsset(), getThreat());
    }

    public void populate(AssetThreatDTO dto) {
        dto.setAssetThreatId(getId());
        dto.setAssetId(getAsset().getAssetId());
        dto.setAssetName(getAsset().getName());
        dto.setThreatName(getThreat().getName());
        dto.setThreatDescription(getThreat().getDescription());
    }
}
