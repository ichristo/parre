/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.server.domain;



import javax.persistence.*;
import javax.validation.constraints.NotNull;

import org.parre.shared.types.DpType;

import java.math.BigDecimal;

/**
 * User: Swarn S. Dhaliwal
 * Date: 7/14/11
 * Time: 11:20 AM
*/
@Entity
@DiscriminatorValue(value = "DP")
/*@Table(name = "DP")*/
public class Dp extends AssetThreatAnalysis {
    @Column(name = "DP_NAME") @NotNull
    private String name;
    @Column(name = "DP_DESCRIPTION", columnDefinition = "VARCHAR(4000)", length = 4000)
    private String description;
    @ManyToOne (targetEntity = DpAnalysis.class)
    @JoinColumn(name = "DP_ANALYSIS_ID")
    private DpAnalysis analysis;
    @Column(name = "DP_PARENT_ID")
    private Long parentId;
    @Column(name = "DP_TYPE") @Enumerated(EnumType.STRING)
    private DpType type;

    public Dp() {
    }

    public Dp(AssetThreat assetThreat) {
        super(assetThreat);
        setName(assetThreat.getThreat().getName());
        setDescription(assetThreat.getThreat().getDescription());
        this.type = DpType.MAIN;
    }

    public void clearValues() {
        setFatalities(0);
        setSeriousInjuries(0);
        setFinancialImpact(Amount.ZERO);
        setEconomicImpact(Amount.ZERO);
        setFinancialTotal(Amount.ZERO);
        setVulnerability(BigDecimal.ZERO);
        setLot(BigDecimal.ZERO);
        setDurationDays(BigDecimal.ZERO);
        setSeverityMgd(BigDecimal.ZERO);
    }

    public Dp(Dp parent) {
        super(parent.getAssetThreat());
        this.parentId = parent.getId();
        this.analysis = parent.analysis;
        this.type = DpType.SUB;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Analysis getAnalysis() {
        return analysis;
    }

    public void setAnalysis(DpAnalysis analysis) {
        this.analysis = analysis;
    }

    public DpType getType() {
        return type;
    }

    public void setType(DpType type) {
        this.type = type;
    }

    public Long getParentId() {
        return parentId;
    }


    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("Dp");
        sb.append(", analysis=").append(analysis);
        sb.append(", parentId=").append(parentId);
        sb.append(", type=").append(type);
        sb.append('}');
        return sb.toString();
    }
}
