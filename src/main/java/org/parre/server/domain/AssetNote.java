/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.server.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.parre.shared.NoteDTO;


/**
 * User: keithjones
 * Date: 11/13/12
 * Time: 10:20 AM
*/
@Entity
@Table(name = "ASSET_NOTE")
public class AssetNote extends BaseEntity {
    @Column(name = "USERNAME") @NotNull
    private String userName = "";
    @Column(name = "DATE_CREATED")
    private Date dateCreated;
    @Column(name = "MESSAGE", columnDefinition = "VARCHAR(4000)", length = 4000)
    private String message = "";
    @ManyToOne(targetEntity = Asset.class)
    @JoinColumn(name = "ASSETID")
    private Asset asset;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Asset getAsset() {
        return asset;
    }

    public void setAsset(Asset asset) {
        this.asset = asset;
    }

    public NoteDTO createDTO() {
        return this.copyInto(new NoteDTO());
    }
}
