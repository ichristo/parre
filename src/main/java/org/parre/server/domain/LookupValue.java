/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.server.domain;


import javax.persistence.*;

import org.parre.shared.LabelValueDTO;
import org.parre.shared.types.LookupType;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * .
 * User: Swarn S. Dhaliwal
 * Date: 11/6/11
 * Time: 12:02 PM
 */
@Entity
@Table(name = "LOOKUP_VALUE")
public class LookupValue extends NameDescription {
    @Column (name = "LOOKUP_TYPE") @Enumerated (value = EnumType.STRING)
    private LookupType lookupType;

    public LookupValue() {
    }

    public LookupValue(String name, String description, LookupType lookupType) {
        super(name, description);
        this.lookupType = lookupType;
    }

    public LookupType getLookupType() {
        return lookupType;
    }

    public void setLookupType(LookupType lookupType) {
        this.lookupType = lookupType;
    }

    public static List<LabelValueDTO> createDTOs(Collection<LookupValue> entities) {
            List<LabelValueDTO> dtos = new ArrayList<LabelValueDTO>(entities.size());
            for (LookupValue entity : entities) {
                dtos.add(new LabelValueDTO(entity.getDescription(), entity.getName()));
            }
            return dtos;
        }

}
