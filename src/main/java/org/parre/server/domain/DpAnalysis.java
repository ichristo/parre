/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.server.domain;


import javax.persistence.*;

import org.parre.shared.NameDescriptionDTO;

import java.util.Collections;
import java.util.Set;

/**
 * The Class DpAnalysis.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 11/15/11
 */
@Entity
@Table(name = "DP_ANALYSIS")
public class DpAnalysis extends Analysis {
    @OneToMany(mappedBy = "analysis")
    private Set<Dp> dps = Collections.<Dp>emptySet();

    public DpAnalysis() {
    }

    public DpAnalysis(NameDescriptionDTO dto) {
        super(dto);
    }

    public DpAnalysis(String name, String description) {
        super(name, description);
    }

    @Override
    public Set<? extends AssetThreatEntity> getAssetThreats() {
        return dps;
    }

    @Override
    public void addAssetThreat(AssetThreatEntity assetThreat) {
        addDp((Dp) assetThreat);
    }

    public void addDp(Dp dp) {
        dps = add(dp, dps);
        dp.setAnalysis(this);
    }

    public DpAnalysis(DpAnalysis baseline) {
    }

    public Set<Dp> getDps() {
        return dps;
    }

    public void setDps(Set<Dp> dps) {
        this.dps = dps;
    }

}
