/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.server.domain;

import org.apache.log4j.Logger;
import org.parre.shared.AmountDTO;

import java.util.Map;

/**
 * The Class MoneyMap.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 4/18/12
 */
public class MoneyMap implements AmountMap {
    private Map<Long, Map<Long, AmountDTO>> amountMap;

    public MoneyMap(Map<Long, Map<Long, AmountDTO>> amountMap) {
        this.amountMap = amountMap;
    }

    public AmountDTO getAmount(Long id, Long nestedId) {
        return amountMap.get(id).get(nestedId);
    }
}
