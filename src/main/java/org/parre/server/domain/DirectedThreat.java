/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.server.domain;


import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import org.apache.log4j.Logger;
import org.parre.server.messaging.service.logic.ParreAnalysisCopyData;
import org.parre.shared.types.LotAnalysisType;
import org.parre.shared.types.VulnerabilityAnalysisType;


/**
 * User: Swarn S. Dhaliwal
 * Date: 7/14/11
 * Time: 11:20 AM
*/
@Entity
@DiscriminatorValue(value = "DIRECTED_THREAT")
/*@Table(name = "DIRECTED_THREAT")*/
public class DirectedThreat extends AssetThreatAnalysis {
	private final static transient Logger log = Logger.getLogger(DirectedThreat.class);
	
    @ManyToOne (targetEntity = DirectedThreatAnalysis.class)
    @JoinColumn(name = "DT_ANALYSIS_ID")
    private DirectedThreatAnalysis analysis;

    //Vulnerability Analysis Section
    @Column(name = "DT_V_ANALYSIS_TYPE") @Enumerated(EnumType.STRING)
    private VulnerabilityAnalysisType vulnerabilityAnalysisType;

    @OneToOne(targetEntity = PathAnalysis.class, cascade = {CascadeType.REMOVE})
    @JoinColumn(name = "DT_PATH_ANALYSIS_ID")
    private PathAnalysis pathAnalysis;

    @ManyToOne(targetEntity = DiagramAnalysis.class)
    @JoinColumn(name = "DIAGRAM_ANALYSIS_ID")
    private DiagramAnalysis diagramAnalysis;

    @ManyToMany(cascade = {CascadeType.REMOVE}, targetEntity = CounterMeasure.class)
    @JoinTable(name = "DIRECTED_THREAT_COUNTER_MEASURE",
            joinColumns = {@JoinColumn(name = "DIRECTED_THREAT_ID")},
            inverseJoinColumns = {@JoinColumn(name = "COUNTER_MEASURE_ID")})

    private Set<CounterMeasure> expertElicitationCounterMeasures = new HashSet<CounterMeasure>();

    //Likelihood of Threat Analysis section
    @Column(name = "DT_LOT_ANALYSIS_TYPE") @Enumerated(EnumType.STRING)
    private LotAnalysisType lotAnalysisType;
    @ManyToOne(targetEntity = ProxyIndication.class)
    @JoinColumn(name = "DT_PROXY_INDICATION_ID")
    private ProxyIndication proxyIndication;

    public DirectedThreat() {
    }

    public DirectedThreat(AssetThreat assetThreat) {
        super(assetThreat);
        vulnerabilityAnalysisType = VulnerabilityAnalysisType.NONE;
        lotAnalysisType = LotAnalysisType.NONE;
    }

    public DirectedThreat(ParreAnalysis parreAnalysis, AssetThreat assetThreat) {
        this(assetThreat);
        setParreAnalysis(parreAnalysis);
    }

    public Analysis getAnalysis() {
        return analysis;
    }

    public void setAnalysis(DirectedThreatAnalysis consequenceAnalysis) {
        this.analysis = consequenceAnalysis;
    }


    public VulnerabilityAnalysisType getVulnerabilityAnalysisType() {
        return vulnerabilityAnalysisType;
    }

    public void setVulnerabilityAnalysisType(VulnerabilityAnalysisType vulnerabilityAnalysisType) {
        this.vulnerabilityAnalysisType = vulnerabilityAnalysisType;
    }

    public PathAnalysis getPathAnalysis() {
        return pathAnalysis;
    }

    public void setPathAnalysis(PathAnalysis pathAnalysis) {
        this.pathAnalysis = pathAnalysis;
    }

    public Set<CounterMeasure> getExpertElicitationCounterMeasures() {
        return expertElicitationCounterMeasures;
    }

    public void setExpertElicitationCounterMeasures(Set<CounterMeasure> expertElicitationCounterMeasures) {
        this.expertElicitationCounterMeasures = expertElicitationCounterMeasures;
    }

    public void addCounterMeasure(CounterMeasure counterMeasure) {
        if(vulnerabilityAnalysisType.isEventTree()) {
            diagramAnalysis.addCounterMeasure(counterMeasure);
        }
        else if(vulnerabilityAnalysisType.isPathAnalysis()) {
            pathAnalysis.addCounterMeasure(counterMeasure);
        }
        else {
            if (expertElicitationCounterMeasures.isEmpty()) {
                expertElicitationCounterMeasures = new HashSet<CounterMeasure>();
            }
            expertElicitationCounterMeasures.add(counterMeasure);
            counterMeasure.addDirectedThreat(this);
        }
    }
    
    public void addExpertElicitationCounterMeasure(CounterMeasure counterMeasure) {
    	if (expertElicitationCounterMeasures.isEmpty()) {
            expertElicitationCounterMeasures = new HashSet<CounterMeasure>();
        }
        expertElicitationCounterMeasures.add(counterMeasure);
        counterMeasure.addDirectedThreat(this);
    }

    public boolean hasCounterMeasures() {
        return !expertElicitationCounterMeasures.isEmpty();
    }

    public LotAnalysisType getLotAnalysisType() {
        return lotAnalysisType;
    }


    public void setLotAnalysisType(LotAnalysisType lotAnalysisType) {
        this.lotAnalysisType = lotAnalysisType;
    }

    public ProxyIndication getProxyIndication() {
        return proxyIndication;
    }

    public void setProxyIndication(ProxyIndication proxyIndication) {
        this.proxyIndication = proxyIndication;
    }

    public void updateVulnerabilityAnalysisTypeRelationships() {
        if (getVulnerabilityAnalysisType().isExpertElicitation()) {
            setDiagramAnalysis(null);
            setPathAnalysis(null);
        }
        else if(getVulnerabilityAnalysisType().isEventTree()) {
            setPathAnalysis(null);
            setExpertElicitationCounterMeasures(null);
        }
        else {
            setDiagramAnalysis(null);
            setExpertElicitationCounterMeasures(null);
        }
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("DirectedThreat");
        sb.append(", vulnerabilityAnalysisType=").append(vulnerabilityAnalysisType);
        sb.append(", lotAnalysisType=").append(lotAnalysisType);
        sb.append('}');
        return sb.toString();
    }

    public DiagramAnalysis getDiagramAnalysis() {
        return diagramAnalysis;
    }

    public void setDiagramAnalysis(DiagramAnalysis diagramAnalysis) {
        this.diagramAnalysis = diagramAnalysis;
    }

    public Set<CounterMeasure> getCounterMeasures() {
        if(vulnerabilityAnalysisType.isEventTree()) {
            return getDiagramAnalysis().getDiagramAnalysisCounterMeasures();
        }
        else if(vulnerabilityAnalysisType.isPathAnalysis()) {
            return getPathAnalysis().getPathAnalysisCounterMeasures();
        }
        else {
            return getExpertElicitationCounterMeasures();
        }
    }

    public void setCounterMeasures(Set<CounterMeasure> counterMeasures) {
        if(vulnerabilityAnalysisType.isEventTree()) {
        	diagramAnalysis.setDiagramAnalysisCounterMeasures(counterMeasures);
        }
        else if(vulnerabilityAnalysisType.isPathAnalysis()) {
            pathAnalysis.setPathAnalysisCounterMeasures(counterMeasures);
        }
        else {
            setExpertElicitationCounterMeasures(counterMeasures);
        }
    }
    
    public DirectedThreat makeCopy(DirectedThreat copy, ParreAnalysisCopyData copyData) {
    	copy.setFatalities(getFatalities());
    	copy.setSeriousInjuries(getSeriousInjuries());
    	copy.setFinancialImpact(getFinancialImpact());
    	copy.setEconomicImpact(getEconomicImpact());
    	copy.setFinancialTotal(getFinancialTotal());
    	copy.setVulnerability(getVulnerability());
    	copy.setDurationDays(getDurationDays());
    	copy.setSeverityMgd(getSeverityMgd());
    	copy.setLot(getLot());
    	
    	copy.setAssetThreat(getAssetThreat());
    	
    	copy.setVulnerabilityAnalysisType(vulnerabilityAnalysisType);
    	copy.setLotAnalysisType(lotAnalysisType);
    	
    	if (diagramAnalysis != null) {
    		DiagramAnalysis copiedDiagramAnalysis;
            copiedDiagramAnalysis = copyData.getDiagramAnalysisCopies().get(diagramAnalysis.getId());
    		copy.setDiagramAnalysis(copiedDiagramAnalysis);
    	}
    	
    	if (pathAnalysis != null) {
            PathAnalysis copiedPathAnalysis = copyData.getPathAnalysisCopies().get(pathAnalysis.getId());
    		copy.setPathAnalysis(copiedPathAnalysis);
    	}
    	
    	if (expertElicitationCounterMeasures != null) {
    		copy.setExpertElicitationCounterMeasures(new HashSet<CounterMeasure>());
    		for (CounterMeasure counterMeasure : expertElicitationCounterMeasures) {
    			CounterMeasure copyCounterMeasure = copyData.getCounterMeasureCopies().get(counterMeasure.getId());
    			copy.addExpertElicitationCounterMeasure(copyCounterMeasure);
    		}
    	}

        if(copy.getLotAnalysisType().equals(LotAnalysisType.PROXY_INDICATOR)) {
            ProxyIndication copiedProxyIndication = copyData.getProxyIndicationCopies().get(getProxyIndication().getId());
            copy.setProxyIndication(copiedProxyIndication);
        }
    	return copy;
    }
    
}
