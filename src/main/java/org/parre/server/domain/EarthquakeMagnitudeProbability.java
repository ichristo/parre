/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.server.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.math.BigDecimal;

/**
 * User: keithjones
 * Date: 5/7/12
 * Time: 1:11 PM
*/
@Entity
@Table(name = "EARTHQUAKE_MAGNITUDE_PROBABILITY")
public class EarthquakeMagnitudeProbability extends BaseEntity {
    @Column(name = "PROBABILITY", columnDefinition = "DECIMAL(21,20)")
    private BigDecimal probability;
    @Column(name = "MAGNITUDE", columnDefinition = "DECIMAL(2,1)")
    private BigDecimal magnitude;

    public BigDecimal getProbability() {
        return probability;
    }

    public void setProbability(BigDecimal probability) {
        this.probability = probability;
    }

    public BigDecimal getMagnitude() {
        return magnitude;
    }

    public void setMagnitude(BigDecimal magnitude) {
        this.magnitude = magnitude;
    }

    @Override
    public String toString() {
        return "EarthquakeMagnitudeProbabilitiesDTO{" +
                "probability=" + probability +
                ", magnitude=" + magnitude +
                '}';
    }
}