/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.server.domain;

import javax.persistence.*;
import java.math.BigDecimal;

/**
 * The Class RiskResilience.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 2/16/12
 */
@Entity
@Table(name = "RISK_RESILIENCE")
public class RiskResilience extends AssetThreatEntity {
    @ManyToOne(targetEntity = RiskResilienceAnalysis.class)
    @JoinColumn(name = "ANALYSIS_ID")
    private RiskResilienceAnalysis analysis;


    public RiskResilience() {
    }

    public RiskResilience(AssetThreat assetThreat) {
        super(assetThreat);

    }

    public RiskResilienceAnalysis getAnalysis() {
        return analysis;
    }

    public void setAnalysis(RiskResilienceAnalysis analysis) {
        this.analysis = analysis;
    }




}
