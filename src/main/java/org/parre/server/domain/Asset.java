/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.server.domain;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;

import org.parre.shared.AssetDTO;
import org.parre.shared.LabelValueDTO;
import org.parre.shared.types.AssetType;


/**
 * User: Swarn S. Dhaliwal
 * Date: 7/14/11
 * Time: 11:19 AM
*/
@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "ASSET_TYPE", discriminatorType = DiscriminatorType.STRING)
@Table(name = "ASSET", uniqueConstraints = @UniqueConstraint(columnNames = {"PARRE_ANALYSIS_ID", "ASSETID"}))
public abstract class Asset extends NameDescription {
    @ManyToOne(targetEntity = ParreAnalysis.class)
    @JoinColumn(name = "PARRE_ANALYSIS_ID")
    private ParreAnalysis parreAnalysis;
    @Column(name = "IS_REMOVED")
    private Boolean removed = false;
    @Column (name = "ASSETID")
    private String assetId = "";
    @Column (name = "CRITICAL")
    private Boolean critical;
    @Column (name = "HUMAN_PL")
    private Integer humanPl = Integer.valueOf(0);
    @Column (name = "FINANCIAL_PL")
    private Integer financialPl = Integer.valueOf(0);
    @Column (name = "ECONOMIC_PL")
    private Integer economicPl = Integer.valueOf(0);
    @OneToMany(mappedBy = "asset", cascade = CascadeType.ALL)
    private Set<AssetNote> assetNotes;
    
    @Column (name = "SYSTEM_TYPE")
    private String systemType;
    
    public ParreAnalysis getParreAnalysis() {
        return parreAnalysis;
    }

    public void setParreAnalysis(ParreAnalysis parreAnalysis) {
        this.parreAnalysis = parreAnalysis;
    }

    @Transient
    public abstract AssetType getAssetType();

    public Boolean getRemoved() {
        return removed;
    }

    public void setRemoved(Boolean removed) {
        this.removed = removed;
    }

    public String getAssetId() {
        return assetId;
    }

    public void setAssetId(String assetId) {
        this.assetId = assetId;
    }

    public Boolean getCritical() {
        return critical;
    }

    public void setCritical(Boolean critical) {
        this.critical = critical;
    }

    public Integer getHumanPl() {
        return humanPl;
    }

    public void setHumanPl(Integer humanPl) {
        this.humanPl = humanPl;
    }

    public Integer getEconomicPl() {
        return economicPl;
    }

    public void setEconomicPl(Integer economicPl) {
        this.economicPl = economicPl;
    }

    public Integer getFinancialPl() {
        return financialPl;
    }

    public void setFinancialPl(Integer financialPl) {
        this.financialPl = financialPl;
    }

    @Transient
    public Integer getTotalPl() {
        return humanPl + financialPl + economicPl;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append(super.toString());
        sb.append("Asset");
        sb.append("{assetId='").append(assetId).append('\'');
        sb.append(", critical=").append(critical);
        sb.append(", humanPl=").append(humanPl);
        sb.append(", financialPl=").append(financialPl);
        sb.append(", economicPl=").append(economicPl);
        sb.append('}');
        return sb.toString();
    }

    public static List<AssetDTO> createDTOs(List<Asset> entities) {
        List<AssetDTO> dtos = new ArrayList<AssetDTO>(entities.size());
        for (Asset entity : entities) {
            AssetDTO assetDTO = entity.createDTO();


            for(AssetNote note : entity.getAssetNotes()) {
                assetDTO.getNoteDTOs().add(note.createDTO());
            }
            dtos.add(entity.copyInto(assetDTO));
        }
        return dtos;

    }

    protected abstract AssetDTO createDTO();

    public LabelValueDTO createLabelValueDTO() {
        return new LabelValueDTO(getName(), String.valueOf(getId()));
    }

    public static Asset cloneAsset(Asset asset) {
        Asset clone = asset.copyInto(create(asset.getAssetType()));
        clone.setId(null);
        clone.setParreAnalysis(null);
        for(AssetNote note : clone.getAssetNotes()) {
            note.setAsset(clone);
            note.setId(null);
        }
        return clone;
    }

    public static Asset create(AssetType assetType) {
        switch (assetType) {
            case PRODUCT_SERVICE:
                return new ProductService();
            case SCADA:
                return new Scada();
            case EXTERNAL_ASSET:
                return new ExternalResource();
            case HUMAN_ASSET:
                return new HumanResource();
            case PHYSICAL_ASSET:
                return new PhysicalResource();
            default:
                throw new IllegalArgumentException("Unknown assetType=" + assetType);
        }
    }

    public Set<AssetNote> getAssetNotes() {
        return assetNotes;
    }

    public void setAssetNotes(Set<AssetNote> assetNotes) {
        this.assetNotes = assetNotes;
    }

    public void addAssetNote(AssetNote note) {
        if(assetNotes == null) {
            assetNotes = new HashSet<AssetNote>();
        }
        assetNotes.add(note);
    }

	public String getSystemType() {
		return systemType;
	}

	public void setSystemType(String systemType) {
		this.systemType = systemType;
	}
}
