/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.server.domain;



import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.validation.constraints.NotNull;

import org.parre.shared.NameDescriptionDTO;

/**
 * The Class NameDescription.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 9/19/11
 */
@MappedSuperclass
public abstract class NameDescription extends BaseEntity {
    @Column(name = "NAME") @NotNull
    private String name;
    @Column(name = "DESCRIPTION", columnDefinition = "VARCHAR(4000)", length = 4000)
    private String description;

    protected NameDescription() {
    }

    protected NameDescription(NameDescriptionDTO dto) {
        this(dto.getName(), dto.getDescription());
    }
    protected NameDescription(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public NameDescriptionDTO createNameDescriptionDTO() {
        return copyInto(new NameDescriptionDTO());
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("NameDescription");
        sb.append("{name='").append(name).append('\'');
        sb.append(", description='").append(description).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
