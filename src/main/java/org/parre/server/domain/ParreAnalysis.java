/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.server.domain;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.parre.client.util.ClientLoggingUtil;
import org.parre.shared.ParreAnalysisDTO;
import org.parre.shared.StatValuesDTO;
import org.parre.shared.types.ParreAnalysisStatusType;


/**
 * The Class ParreAnalysis.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 11/15/11
 */
@Entity
@Table(name = "PARRE_ANALYSIS")
public class ParreAnalysis extends NameDescription {
    @Column(name = "STATUS") @Enumerated(EnumType.STRING)
    private ParreAnalysisStatusType status;
    @Column(name = "ASSET_THRESHOLD")
    private Integer assetThreshold;
    @Column(name = "ASSET_THREAT_THRESHOLD")
    private Integer assetThreatThreshold;
    @OneToOne(targetEntity = Contact.class)
    @JoinColumn(name = "CONTACT_ID")
    private Contact contact;
    @OneToOne(targetEntity = Org.class)
    @JoinColumn(name = "ORG_ID")
    private Org org;

    @Column(name = "USE_STAT_VALUES")
    private Boolean useStatValues = Boolean.FALSE;
    
    @Embedded
    private StatValues statValues;
    
    @Column(name = "OVERRIDE_FATALITY")
    private Boolean overrideFatality;

    @Column(name = "OVERRIDE_SI")
    private Boolean overrideSeriousInjury;

    @Column(name = "CREATED_TIME", insertable = true, updatable = false) @Temporal(TemporalType.TIMESTAMP)
    private Date createdTime;

    @OneToMany(mappedBy = "parreAnalysis")
    private Set<AssetThreat> assetThreats;

    @OneToMany(mappedBy = "parreAnalysis")
    private Set<Asset> assets;

    @ManyToOne(targetEntity = ParreAnalysis.class)
    private ParreAnalysis baseline;

    @OneToMany(mappedBy = "baseline")
    private Set<ParreAnalysis> optionParreAnalyses = Collections.emptySet();

    @OneToMany(mappedBy = "baselineParreAnalysis")
    private Set<ProposedMeasure> baselineProposedMeasures = Collections.emptySet();

    @ManyToMany(targetEntity = ProposedMeasure.class)
    @JoinTable(name = "PA_PROPOSED_MEASURE",
            joinColumns = {@JoinColumn(name = "PARRE_ANALYSIS_ID")},
            inverseJoinColumns = {@JoinColumn(name = "PROPOSED_MEASURE_ID")})
    private Set<ProposedMeasure> proposedMeasures;

    @Column(name = "DISPLAY_ORDER")
    private Integer displayOrder;

    @OneToOne
    @JoinColumn(name = "SITE_LOCATION")
    private SiteLocation siteLocation;
    
    @Column(name = "USE_DEFAULT_LOCATION")
    private Boolean useDefaultLocation = Boolean.FALSE;
    
    @Column(name = "DEFAULT_POSTAL_CODE")
    private String defaultPostalCode;
    
    @Column (name = "DEFAULT_LATITUDE", columnDefinition = "DECIMAL(10,5)")
    private BigDecimal defaultLatitude;
    
    @Column (name = "DEFAULT_LONGITUDE", columnDefinition = "DECIMAL(10, 5)")
    private BigDecimal defaultLongitude;
    
    @OneToOne
    @JoinColumn(name = "DEFAULT_EARTHQUAKE_PROFILE_ID")
    private EarthquakeProfile defaultEarthquakeProfile;

    @OneToOne
    @JoinColumn(name = "DEFAULT_HURRICANE_PROFILE_ID")
    private HurricaneProfile defaultHurricaneProfile;
    
    @OneToMany(mappedBy = "parreAnalysis")
    private Set<EarthquakeProfile> earthquakeProfiles;
    
    @OneToMany(mappedBy = "parreAnalysis")
    private Set<HurricaneProfile> hurricaneProfiles;
    
    public ParreAnalysis() {
    }

    public ParreAnalysis(ParreAnalysisStatusType status) {
        this.createdTime = new Date();
        this.status = status;
    }

    public ParreAnalysisStatusType getStatus() {
        return status;
    }

    public void setStatus(ParreAnalysisStatusType status) {
        this.status = status;
    }

    public Contact getContact() {
        return contact;
    }

    public void setContact(Contact contact) {
        this.contact = contact;
    }

    public Integer getAssetThreshold() {
        return assetThreshold;
    }

    public void setAssetThreshold(Integer assetThreshold) {
        this.assetThreshold = assetThreshold;
    }

    public Integer getAssetThreatThreshold() {
        return assetThreatThreshold;
    }

    public void setAssetThreatThreshold(Integer assetThreatThreshold) {
        this.assetThreatThreshold = assetThreatThreshold;
    }

    public Org getOrg() {
        return org;
    }

    public void setOrg(Org org) {
        this.org = org;
    }

    public Boolean getUseStatValues() {
        return useStatValues;
    }

    public void setUseStatValues(Boolean useStatValues) {
        this.useStatValues = useStatValues;
    }

    public StatValues getStatValues() {
        return statValues;
    }

    public void setStatValues(StatValues statValues) {
        this.statValues = statValues;
    }

    public StatValuesDTO getStatValuesDTO() {
        return statValues != null ? statValues.createDTO() : null;
     }

    public void setStatValuesDTO(StatValuesDTO statValuesDTO) {
        if (statValuesDTO != null) {
            this.statValues = new StatValues(statValuesDTO);
        }
        else {
            this.statValues = null;
        }
    }

    public Boolean getOverrideFatality() {
        return overrideFatality;
    }

    public void setOverrideFatality(Boolean overrideFatality) {
        this.overrideFatality = overrideFatality;
    }

    public Boolean getOverrideSeriousInjury() {
        return overrideSeriousInjury;
    }

    public void setOverrideSeriousInjury(Boolean overrideSeriousInjury) {
        this.overrideSeriousInjury = overrideSeriousInjury;
    }

    public Date getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(Date createdTime) {
        this.createdTime = createdTime;
    }

    public Set<AssetThreat> getAssetThreats() {
        return assetThreats;
    }

    public List<AssetThreat> getActiveAssetThreats() {
        List<AssetThreat> activeAssetThreats = new ArrayList<AssetThreat>(assetThreats.size());
        for (AssetThreat assetThreat : assetThreats) {
            if (!assetThreat.getRemoved()) {
                activeAssetThreats.add(assetThreat);
            }
        }
        return activeAssetThreats;
    }

    public void setAssetThreats(Set<AssetThreat> assetThreats) {
        this.assetThreats = assetThreats;
    }

    public Set<Asset> getAssets() {
        return assets;
    }

    public void setAssets(Set<Asset> assets) {
        this.assets = assets;
    }

    public List<Asset> getActiveAssets() {
        List<Asset> activeAssets = new ArrayList<Asset>(assets.size());
        for (Asset asset : assets) {
            if (!asset.getRemoved()) {
                activeAssets.add(asset);
            }
        }
        return activeAssets;
    }

    public ParreAnalysis getBaseline() {
        return baseline;
    }

    public void setBaseline(ParreAnalysis baseline) {
        this.baseline = baseline;
    }

    public boolean isBaseline() {
        return /*isLocked() &&  */baseline == null;
    }

    public boolean isLocked() {
        return ParreAnalysisStatusType.LOCKED == status;
    }

    public boolean isOption() {
        return baseline != null;
    }

    public Set<ParreAnalysis> getOptionParreAnalyses() {
        return optionParreAnalyses;
    }

    public void setOptionParreAnalyses(Set<ParreAnalysis> optionParreAnalyses) {
        this.optionParreAnalyses = optionParreAnalyses;
    }

    public void addOptionParreAnalysis(ParreAnalysis optionAnalysis) {
        optionParreAnalyses = add(optionAnalysis, optionParreAnalyses);
        optionAnalysis.setStatus(getStatus());
        optionAnalysis.setBaseline(this);
    }

    public Set<ProposedMeasure> getBaselineProposedMeasures() {
        return baselineProposedMeasures;
    }

    public void setBaselineProposedMeasures(Set<ProposedMeasure> baselineProposedMeasures) {
        this.baselineProposedMeasures = baselineProposedMeasures;
    }

    public void addBaselineProposedMeasure(ProposedMeasure pm) {
        baselineProposedMeasures = add(pm, baselineProposedMeasures);
        pm.setBaselineParreAnalysis(this);
    }

    public Set<ProposedMeasure> getProposedMeasures() {
        return proposedMeasures;
    }

    public void setProposedMeasures(Set<ProposedMeasure> proposedMeasures) {
        this.proposedMeasures = proposedMeasures;
    }
    public void addProposedMeasure(ProposedMeasure pm) {
        proposedMeasures = add(pm, proposedMeasures);
    }

    public Integer getDisplayOrder() {
        return displayOrder;
    }

    public void setDisplayOrder(Integer displayOrder) {
        this.displayOrder = displayOrder;
    }
    
    public Boolean getUseDefaultLocation() {
		return useDefaultLocation;
	}

	public void setUseDefaultLocation(Boolean useDefaultLocation) {
		this.useDefaultLocation = useDefaultLocation;
	}

	public String getDefaultPostalCode() {
		return defaultPostalCode;
	}

	public void setDefaultPostalCode(String defaultPostalCode) {
		this.defaultPostalCode = defaultPostalCode;
	}

	public BigDecimal getDefaultLatitude() {
		return defaultLatitude;
	}

	public void setDefaultLatitude(BigDecimal defaultLatitude) {
		this.defaultLatitude = defaultLatitude;
	}

	public BigDecimal getDefaultLongitude() {
		return defaultLongitude;
	}

	public void setDefaultLongitude(BigDecimal defaultLongitude) {
		this.defaultLongitude = defaultLongitude;
	}

	public EarthquakeProfile getDefaultEarthquakeProfile() {
		return defaultEarthquakeProfile;
	}

	public void setDefaultEarthquakeProfile(
			EarthquakeProfile defaultEarthquakeProfile) {
		this.defaultEarthquakeProfile = defaultEarthquakeProfile;
	}
	
	public HurricaneProfile getDefaultHurricaneProfile() {
		return defaultHurricaneProfile;
	}

	public void setDefaultHurricaneProfile(HurricaneProfile defaultHurricaneProfile) {
		this.defaultHurricaneProfile = defaultHurricaneProfile;
	}
	
	public Set<EarthquakeProfile> getEarthquakeProfiles() {
		return earthquakeProfiles;
	}

	public void setEarthquakeProfiles(Set<EarthquakeProfile> earthquakeProfiles) {
		this.earthquakeProfiles = earthquakeProfiles;
	}

	public Set<HurricaneProfile> getHurricaneProfiles() {
		return hurricaneProfiles;
	}

	public void setHurricaneProfiles(Set<HurricaneProfile> hurricaneProfiles) {
		this.hurricaneProfiles = hurricaneProfiles;
	}

	public ParreAnalysisDTO createDTO() {
        ParreAnalysisDTO analysisDTO = copyInto(new ParreAnalysisDTO());
        try {
            analysisDTO.setContactDTO(getContact().createNameDescriptionDTO());
        } catch (NullPointerException e) {
            ClientLoggingUtil.error("No Contact in database");
            analysisDTO.setContactDTO(null);
        }

        analysisDTO.setOrgName(getOrg().getName());
        if (getStatValues() != null) {
            analysisDTO.setStatValuesDTO(getStatValues().createDTO());
        }
        if (getBaseline() != null) {
            analysisDTO.setBaselineId(getBaseline().getId());
        }
        if (getSiteLocation() != null) {
            analysisDTO.setSiteLocationDTO(getSiteLocation().createNameDescriptionDTO());
        }
        return analysisDTO;
    }

    public boolean statValuesChanged(ParreAnalysisDTO dto) {
        if (useStatValues == null || statValues == null) {
            return true;
        }
        return !useStatValues.equals(dto.getUseStatValues()) || statValues.isChanged(dto.getStatValuesDTO());
    }

    public ParreAnalysisDTO createDTOWithOptions() {
        ParreAnalysisDTO analysisDTO = createDTO();
        analysisDTO.setOptionParreAnalysisDTOs(createOptionDTOs());
        analysisDTO.sort();
        return analysisDTO;
    }

    public List<ParreAnalysisDTO> createOptionDTOs() {
        if (optionParreAnalyses.isEmpty()) {
            Collections.emptyList();
        }
        List<ParreAnalysisDTO> dtos = new ArrayList<ParreAnalysisDTO>();
        for (ParreAnalysis parreAnalysis : optionParreAnalyses) {
            ParreAnalysisDTO dto = parreAnalysis.createDTO();
            dto.setBaselineId(getId());
            dtos.add(dto);
        }
        Collections.sort(dtos);
        return dtos;
    }

    public SiteLocation getSiteLocation() {
        return siteLocation;
    }

    public void setSiteLocation(SiteLocation siteLocation) {
        this.siteLocation = siteLocation;
    }

    public List<Long> getAllIds() {
        List<Long> optionAnalysisIds = BaseEntity.extractIds(getOptionParreAnalyses());
        for(int i = 0; i < optionAnalysisIds.size() - 1; i++) {
            for(int j = i + 1; j < optionAnalysisIds.size(); j++) {
                if(optionAnalysisIds.get(i) > optionAnalysisIds.get(j)) {
                    Long temp = optionAnalysisIds.get(j);
                    optionAnalysisIds.set(j, optionAnalysisIds.get(i));
                    optionAnalysisIds.set(i, temp);
                }
            }
        }
        List<Long> parreAnalysisIds = new ArrayList<Long>(optionAnalysisIds.size() + 1);
        parreAnalysisIds.add(getId());
        parreAnalysisIds.addAll(optionAnalysisIds);
        return parreAnalysisIds;
    }

    public void copyFrom(ParreAnalysis src) {
        super.copyFrom(src);
        setAssets(Collections.<Asset>emptySet());
        Set<Asset> assetSet = new HashSet<Asset>();
        for (Asset asset : src.getAssets()) {
            Asset copiedAsset = Asset.create(asset.getAssetType());
            copiedAsset.copyFrom(asset);
            copiedAsset.setParreAnalysis(this);
            copiedAsset.setId(null);
            assetSet.add(copiedAsset);
        }
        setAssets(assetSet);

        setProposedMeasures(Collections.<ProposedMeasure>emptySet());
        Set<ProposedMeasure>measureSet = new HashSet<ProposedMeasure>();
        for (ProposedMeasure proposedMeasure : src.getProposedMeasures()) {
            ProposedMeasure pm = new ProposedMeasure();
            pm.copyFrom(proposedMeasure);
            pm.setBaselineParreAnalysis(this);
            pm.setId(null);
            measureSet.add(pm);
        }
        setProposedMeasures(measureSet);

        setBaselineProposedMeasures(Collections.<ProposedMeasure>emptySet());
        measureSet = new HashSet<ProposedMeasure>();
        for (ProposedMeasure proposedMeasure : src.getBaselineProposedMeasures()) {
            ProposedMeasure pm = new ProposedMeasure();
            pm.copyFrom(proposedMeasure);
            pm.setBaselineParreAnalysis(this);
            pm.setId(null);
            measureSet.add(pm);
        }
        setBaselineProposedMeasures(measureSet);
    }
    
    @Override
	public String toString() {
		return "ParreAnalysis [status=" + status + ", assetThreshold="
				+ assetThreshold + ", assetThreatThreshold="
				+ assetThreatThreshold + ", contact=" + contact + ", org="
				+ org + ", useStatValues=" + useStatValues + ", statValues="
				+ statValues + ", overrideFatality=" + overrideFatality
				+ ", overrideSeriousInjury=" + overrideSeriousInjury
				+ ", createdTime=" + createdTime + ", assetThreats="
				+ assetThreats + ", assets=" + assets + ", baseline="
				+ baseline + ", optionParreAnalyses count=" + (optionParreAnalyses != null ? optionParreAnalyses.size() : " <none> ")
				+ ", baselineProposedMeasures=" + baselineProposedMeasures
				+ ", proposedMeasures=" + proposedMeasures + ", displayOrder="
				+ displayOrder + ", siteLocation=" + siteLocation 
				+ ", useDefaultLocation=" + useDefaultLocation
				+ ", defaultPostalCode=" + defaultPostalCode
				+ ", defaultLatitude=" + defaultLatitude
				+ ", defaultLongitude=" + defaultLongitude 
				+ ", defaultEarthquakeProfile: " + String.valueOf(defaultEarthquakeProfile)
				+ ", defaultHurricaneProfile: " + String.valueOf(defaultHurricaneProfile)
				+ ", earthquakeProfiles.count: " + (earthquakeProfiles == null ? 0 : earthquakeProfiles.size())
				+ ", hurricaneProfiles.count: " + (hurricaneProfiles == null ? 0 : hurricaneProfiles.size())
				+ "]";
	}
    
    
}
