/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.server.domain;

import org.apache.log4j.Logger;

import javax.persistence.*;

/**
 * The Class InactiveThreat.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 11/18/11
 */
@Entity
@Table(name = "INACTIVE_THREAT",
        uniqueConstraints = @UniqueConstraint(columnNames = {"PARRE_ANALYSIS_ID", "THREAT_ID"}))
public class InactiveThreat extends BaseEntity {
    @OneToOne(targetEntity = ParreAnalysis.class)
    @JoinColumn(name = "PARRE_ANALYSIS_ID")
    private ParreAnalysis parreAnalysis;
    @OneToOne(targetEntity = Threat.class)
    @JoinColumn(name = "THREAT_ID")
    private Threat threat;
    @Column(name = "INACTIVE_REASON")
    private String inactiveReason;

    public InactiveThreat() {
    }

    public InactiveThreat(Threat threat, String inactiveReason) {
        this.threat = threat;
        this.inactiveReason = inactiveReason;
    }

    public ParreAnalysis getParreAnalysis() {
        return parreAnalysis;
    }

    public void setParreAnalysis(ParreAnalysis parreAnalysis) {
        this.parreAnalysis = parreAnalysis;
    }

    public Threat getThreat() {
        return threat;
    }

    public void setThreat(Threat threat) {
        this.threat = threat;
    }

    public String getInactiveReason() {
        return inactiveReason;
    }

    public void setInactiveReason(String inactiveReason) {
        this.inactiveReason = inactiveReason;
    }

    public InactiveThreat createCopy() {
        return new InactiveThreat(threat, inactiveReason);
    }
}
