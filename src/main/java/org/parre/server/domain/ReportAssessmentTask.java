/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.server.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.parre.shared.NameDescriptionDTO;

@Entity
@Table(name = "REPORT_ASSESSMENT_TASK")
public class ReportAssessmentTask extends NameDescription {
	private static final long serialVersionUID = 1091336840849945957L;
	
	@ManyToOne(targetEntity = ParreAnalysisReport.class)
	@JoinColumn(name = "REPORT_ID")
	private ParreAnalysisReport parreAnalysisReport;
		
	public ReportAssessmentTask(){
	}

	public ReportAssessmentTask(NameDescriptionDTO dto) {
		super(dto);
	}

	public ReportAssessmentTask(String name, String description) {
		super(name, description);
	}

	@Column(name = "TASK_NUMBER")
	private Integer taskNumber;
	
	@Column(name = "TITLE")
	private String title;
	
	@Column(name = "COMMENTS", columnDefinition = "VARCHAR(4000)", length = 4000)
	private String comments;
	
	
	
	public ParreAnalysisReport getParreAnalysisReport() {
		return parreAnalysisReport;
	}

	public void setParreAnalysisReport(ParreAnalysisReport parreAnalysisReport) {
		this.parreAnalysisReport = parreAnalysisReport;
	}

	public Integer getTaskNumber() {
		return taskNumber;
	}

	public void setTaskNumber(Integer taskNumber) {
		this.taskNumber = taskNumber;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}


	@Override
	public String toString() {
		return "ReportAssessmentTask [parreAnalysisReport="
				+ parreAnalysisReport + ", taskNumber=" + taskNumber
				+ ", title=" + title + ", comments=" + comments + "]";
	}
	


}
