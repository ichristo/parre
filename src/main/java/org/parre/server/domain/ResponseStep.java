/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.server.domain;


import javax.persistence.*;

import org.parre.shared.types.AnalysisPathStepType;

import java.math.BigDecimal;

/**
 * The Class ResponseStep.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 1/31/12
 */
@Entity
@Table(name = "RESPONSE_STEP")
public class ResponseStep extends NameDescription {
    @Column(name = "TYPE") @Enumerated(EnumType.STRING)
    private AnalysisPathStepType type;
    @Column(name = "DURATION", columnDefinition = "DECIMAL(8,2)")
    private BigDecimal duration;
    @Column(name = "STEP_NUMBER")
    private Integer stepNumber;

    public AnalysisPathStepType getType() {
        return type;
    }

    public void setType(AnalysisPathStepType type) {
        this.type = type;
    }

    public BigDecimal getDuration() {
        return duration;
    }

    public void setDuration(BigDecimal duration) {
        this.duration = duration;
    }

    public Integer getStepNumber() {
        return stepNumber;
    }

    public void setStepNumber(Integer stepNumber) {
        this.stepNumber = stepNumber;
    }

    public ResponseStep makeCopy() {
        ResponseStep copy = new ResponseStep();
        copy.copyFrom(this);
        copy.setId(null);
        return copy;
    }
}
