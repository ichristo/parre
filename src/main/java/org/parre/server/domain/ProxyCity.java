/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.server.domain;

import javax.persistence.*;

/**
 * The Class ProxyCity.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 2/3/12
 */
@Entity
@Table(name = "PROXY_CITY", uniqueConstraints =
        {@UniqueConstraint(columnNames = {"CITY", "PROXY_TIER_ID"})})
public class ProxyCity extends BaseEntity {
    @Column(name = "CITY")
    private String city;
    @OneToOne(targetEntity = ProxyTier.class)
    @JoinColumn(name = "PROXY_TIER_ID")
    private ProxyTier proxyTier;

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public ProxyTier getProxyTier() {
        return proxyTier;
    }

    public void setProxyTier(ProxyTier proxyTier) {
        this.proxyTier = proxyTier;
    }
}
