/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.server.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.parre.shared.NameDescriptionDTO;

@Entity
@Table(name = "REPORT_INVESTMENT_OPTION")
public class ReportInvestmentOption extends NameDescription {

	private static final long serialVersionUID = -5962499286101772095L;

	@ManyToOne(targetEntity = ParreAnalysisReport.class)
	@JoinColumn(name = "REPORT_ID")
	private ParreAnalysisReport parreAnalysisReport;
		
	public ReportInvestmentOption(){
	}

	public ReportInvestmentOption(NameDescriptionDTO dto) {
		super(dto);
	}

	public ReportInvestmentOption(String name, String description) {
		super(name, description);
	}

	@Column(name = "INVEST_OPTION")
	private String investOption;
	
	@Column(name = "COUNTERMEASURE")
	private String countermeasure;

	@Column(name = "SITES_APPLIED")
	private String sitesApplied;
	
	@Column(name = "SITE_COST")
	private String siteCost;
	
	@Column(name = "TOTAL_INVESTMENT")
	private String totalInvestment;
	
	
	public ParreAnalysisReport getParreAnalysisReport() {
		return parreAnalysisReport;
	}

	public void setParreAnalysisReport(ParreAnalysisReport parreAnalysisReport) {
		this.parreAnalysisReport = parreAnalysisReport;
	}

	public String getInvestOption() {
		return investOption;
	}

	public void setInvestOption(String investOption) {
		this.investOption = investOption;
	}

	public String getCountermeasure() {
		return countermeasure;
	}

	public void setCountermeasure(String countermeasure) {
		this.countermeasure = countermeasure;
	}

	public String getSitesApplied() {
		return sitesApplied;
	}

	public void setSitesApplied(String sitesApplied) {
		this.sitesApplied = sitesApplied;
	}

	public String getSiteCost() {
		return siteCost;
	}

	public void setSiteCost(String siteCost) {
		this.siteCost = siteCost;
	}

	public String getTotalInvestment() {
		return totalInvestment;
	}

	public void setTotalInvestment(String totalInvestment) {
		this.totalInvestment = totalInvestment;
	}

	@Override
	public String toString() {
		return "ReportInvestmentOption [parreAnalysisReport="
				+ parreAnalysisReport + ", investOption=" + investOption
				+ ", countermeasure=" + countermeasure + ", sitesApplied="
				+ sitesApplied + ", siteCost=" + siteCost
				+ ", totalInvestment=" + totalInvestment + "]";
	}
	

}
