/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.server.domain;

import java.math.BigDecimal;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Embedded;

import org.parre.server.domain.util.DataObject;
import org.parre.shared.StatValueQuantities;
import org.parre.shared.StatValuesDTO;


/**
 * The Class StatValues.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 11/4/11
 */
@Embeddable
public class StatValues extends DataObject implements StatValueQuantities {
    @Embedded
    @AttributeOverrides( {
            @AttributeOverride(name="quantity", column = @Column(name="FATALITY_QUANTITY", columnDefinition = "DECIMAL(19,6)") ),
            @AttributeOverride(name="unit", column = @Column(name="FATALITY_UNIT") )
    } )
    private Amount fatality;
    @Embedded
    @AttributeOverrides( {
            @AttributeOverride(name="quantity", column = @Column(name="SI_QUANTITY", columnDefinition = "DECIMAL(19,6)") ),
            @AttributeOverride(name="unit", column = @Column(name="SI_UNIT") )
    } )
    private Amount seriousInjury;

    public StatValues() {
        fatality = Amount.ZERO;
        seriousInjury = Amount.ZERO;
    }

    public StatValues(StatValuesDTO statValuesDTO) {
        this.fatality = new Amount(statValuesDTO.getFatalityDTO());
        this.seriousInjury = new Amount(statValuesDTO.getSeriousInjuryDTO());
    }

    public Amount getFatality() {
        return fatality;
    }

    public void setFatality(Amount fatality) {
        this.fatality = fatality;
    }

    public Amount getSeriousInjury() {
        return seriousInjury;
    }

    public void setSeriousInjury(Amount seriousInjury) {
        this.seriousInjury = seriousInjury;
    }

    public StatValuesDTO createDTO() {
        StatValuesDTO dto = new StatValuesDTO();
        dto.setFatalityDTO(getFatality().createDTO());
        dto.setSeriousInjuryDTO(getSeriousInjury().createDTO());
        return dto;
    }

    public BigDecimal getFatalityQuantity() {
        return fatality.getQuantity();
    }

    public BigDecimal getSeriousInjuryQuantity() {
        return seriousInjury.getQuantity();
    }

    public boolean isChanged(StatValuesDTO dto) {
        if (fatality == null || fatality.getQuantity() == null) {
            fatality = new Amount(BigDecimal.ZERO);
        }
        if (seriousInjury == null || fatality.getQuantity() == null) {
            seriousInjury = new Amount(BigDecimal.ZERO);
        }

        if (getFatalityQuantity().doubleValue() != dto.getFatalityQuantity().doubleValue()) {
            return true;
        }
        if (getSeriousInjuryQuantity().doubleValue() != dto.getSeriousInjuryQuantity().doubleValue()) {
            return true;
        }
        return false;
    }

	@Override
	public String toString() {
		return "StatValues [fatality=" + fatality + ", seriousInjury="
				+ seriousInjury + "]";
	}
    
    
}
