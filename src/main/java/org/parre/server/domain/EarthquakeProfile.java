/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.server.domain;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.parre.shared.EarthquakeMagnitudeProbabilityDTO;


/**
 * The Class EarthquakeProfile.
 */
@Entity
@Table(name = "EARTHQUAKE_PROFILE")
public class EarthquakeProfile extends NameDescription {
	
	@OneToOne(targetEntity = ParreAnalysis.class)
    @JoinColumn(name = "PARRE_ANALYSIS_ID")
    private ParreAnalysis parreAnalysis;

	@Column(name = "PERCENT_G", columnDefinition="DECIMAL(19,6)")
	private BigDecimal percentG;
	@Column(name = "RETURN_VALUE", columnDefinition="DECIMAL(19,6)")
	private BigDecimal returnValue;
	@OneToMany(targetEntity = EarthquakeMagnitudeProbability.class, fetch=FetchType.EAGER, cascade=CascadeType.ALL)
	@JoinColumn(name = "EARTHQUAKE_PROFILE_ID")
	private Set<EarthquakeMagnitudeProbability> magnitudeProbabilities = Collections.<EarthquakeMagnitudeProbability>emptySet();
	
	public ParreAnalysis getParreAnalysis() {
		return parreAnalysis;
	}

	public void setParreAnalysis(ParreAnalysis parreAnalysis) {
		this.parreAnalysis = parreAnalysis;
	}

    public BigDecimal getPercentG() {
		return percentG;
	}

	public void setPercentG(BigDecimal percentG) {
		this.percentG = percentG;
	}

	public BigDecimal getReturnValue() {
		return returnValue;
	}

	public void setReturnValue(BigDecimal returnValue) {
		this.returnValue = returnValue;
	}

	public Set<EarthquakeMagnitudeProbability> getMagnitudeProbabilities() {
		return magnitudeProbabilities;
	}

	public void setMagnitudeProbabilities(
			Set<EarthquakeMagnitudeProbability> magnitudeProbabilities) {
		this.magnitudeProbabilities = magnitudeProbabilities;
	}

	public void addEarthquakeMagnitudeProbability(EarthquakeMagnitudeProbabilityDTO dto) {
        EarthquakeMagnitudeProbability newDTO = new EarthquakeMagnitudeProbability();
        newDTO.setMagnitude(dto.getMagnitude());
        newDTO.setProbability(dto.getProbability());
        magnitudeProbabilities.add(newDTO);
    }

	public void copyFrom(EarthquakeProfile src) {
		super.setName(src.getName());
		percentG = src.getPercentG();
		returnValue = src.getReturnValue();
		
		magnitudeProbabilities = new HashSet<EarthquakeMagnitudeProbability>();
		if (src.getMagnitudeProbabilities() != null) {
			for (EarthquakeMagnitudeProbability emp : src.getMagnitudeProbabilities()) {
				EarthquakeMagnitudeProbability copyEmp = new EarthquakeMagnitudeProbability();
				copyEmp.setMagnitude(emp.getMagnitude());
				copyEmp.setProbability(emp.getProbability());
				magnitudeProbabilities.add(copyEmp);
			}
		}
	}
	
}
