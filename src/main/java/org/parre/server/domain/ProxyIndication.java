/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.server.domain;


import javax.persistence.*;

import org.parre.shared.ProxyCityDTO;
import org.parre.shared.ProxyIndicationDTO;
import org.parre.shared.ProxyTargetTypeDTO;

import java.util.ArrayList;
import java.util.List;

/**
 * User: Daniel
 * Date: 1/12/12
 * Time: 4:18 PM
*/
@Entity
@Table(name = "PROXY_INDICATION")
public class ProxyIndication extends NameDescription {
    @OneToOne(targetEntity = ParreAnalysis.class)
    @JoinColumn(name = "PARRE_ANALYSIS_ID")
    private ParreAnalysis parreAnalysis;
    @Column(name = "ATTACKS_PER_YEAR")
    private Integer attacksPerYear;
    @OneToOne(targetEntity = ProxyCity.class)
    @JoinColumn(name = "PROXY_CITY_ID")
    private ProxyCity proxyCity;
    @OneToOne(targetEntity = ProxyTargetType.class)
    @JoinColumn(name = "TARGET_TYPE_ID")
    private ProxyTargetType targetType;
    @Column(name = "TARGET_TYPE_IN_REGION_NUM")
    private Integer targetTypeInRegionNum;
    @Column(name = "FACILITIES_IN_REGION_NUM")
    private Integer facilitiesInRegionNum;
    @Column(name = "FACILITY_CAPACITY")
    private Integer facilityCapacity;
    @Column(name = "REGIONAL_CAPACITY")
    private Integer regionalCapacity;
    @Column(name = "POPULATION")
    private Integer population;

    public ProxyIndication() {
    }

    public ParreAnalysis getParreAnalysis() {
        return parreAnalysis;
    }

    public void setParreAnalysis(ParreAnalysis parreAnalysis) {
        this.parreAnalysis = parreAnalysis;
    }

    public Integer getAttacksPerYear() {
        return attacksPerYear;
    }

    public ProxyCity getProxyCity() {
        return proxyCity;
    }

    public ProxyTargetType getTargetType() {
        return targetType;
    }

    public void setAttacksPerYear(Integer attacksPerYear) {
        this.attacksPerYear = attacksPerYear;
    }

    public void setProxyCity(ProxyCity proxyCity) {
        this.proxyCity = proxyCity;
    }

    public void setTargetType(ProxyTargetType targetType) {
        this.targetType = targetType;
    }

    public void setTargetTypeInRegionNum(Integer targetTypeInRegionNum) {
        this.targetTypeInRegionNum = targetTypeInRegionNum;
    }

    public void setFacilitiesInRegionNum(Integer facilitiesInRegionNum) {
        this.facilitiesInRegionNum = facilitiesInRegionNum;
    }

    public void setFacilityCapacity(Integer facilityCapacity) {
        this.facilityCapacity = facilityCapacity;
    }

    public void setRegionalCapacity(Integer regionalCapacity) {
        this.regionalCapacity = regionalCapacity;
    }

    public void setPopulation(Integer population) {
        this.population = population;
    }

    public Integer getTargetTypeInRegionNum() {
        return targetTypeInRegionNum;
    }

    public Integer getFacilitiesInRegionNum() {
        return facilitiesInRegionNum;
    }

    public Integer getFacilityCapacity() {
        return facilityCapacity;
    }

    public Integer getRegionalCapacity() {
        return regionalCapacity;
    }

    public Integer getPopulation() {
        return population;
    }

    public ProxyIndication makeCopy() {
        ProxyIndication copy = new ProxyIndication();
        copy.copyFrom(this);
        copy.setId(null);
        copy.setName(copy.getName() + " - Copy" );
        return copy;
    }

    public ProxyIndicationDTO createDTO() {
        ProxyIndicationDTO dto = copyInto(new ProxyIndicationDTO());
        dto.setTargetTypeDTO(getTargetType().copyInto(new ProxyTargetTypeDTO()));
        ProxyCity pc = getProxyCity();
        ProxyCityDTO proxyCityDTO = null;
        if (pc != null) {
            proxyCityDTO = pc.getProxyTier().copyInto(new ProxyCityDTO());
            pc.copyInto(proxyCityDTO);

        }
        dto.setProxyCityDTO(proxyCityDTO);
        return dto;
    }

    public static List<ProxyIndicationDTO> createDTOs(List<ProxyIndication> entities) {
        List<ProxyIndicationDTO> dtos = new ArrayList<ProxyIndicationDTO>(entities.size());
        for (ProxyIndication entity : entities) {
            dtos.add(entity.createDTO());
        }
        return dtos;
    }
}
