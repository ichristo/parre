/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.server.domain;

import org.apache.log4j.Logger;

import javax.persistence.*;
import java.util.Set;

/**
 * The Class Role.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 11/18/11
 */
@Entity
@Table(name = "ROLE", uniqueConstraints = @UniqueConstraint(columnNames = {"NAME"}))
public class Role extends NameDescription {
    @ManyToMany(targetEntity = User.class)
    @JoinTable(name = "USER_ROLE", joinColumns = {@JoinColumn(name = "ROLE_ID")},
        inverseJoinColumns = {@JoinColumn(name = "USER_ID")})
    private Set<User> users;

    public Role() {
    }

    public Role(String roleName, String roleDescription) {
        super(roleName, roleDescription);
    }

    public Set<User> getUsers() {
        return users;
    }

    public void setUsers(Set<User> users) {
        this.users = users;
    }
}
