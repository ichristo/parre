/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.server.util;

import org.apache.log4j.Logger;


/**
 * The Class SystemUtil.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 2/21/12
 */
public class SystemUtil {
	private final static transient Logger log = Logger.getLogger(SystemUtil.class);
    public static String getPersistenceUnit() {
        String persistenceUnitName = System.getProperty("parre.persistenceUnit", "parre-dev");
        if (log.isDebugEnabled()) {
        	log.debug("Returning persistence unit name: " + persistenceUnitName);
        }
        return persistenceUnitName;
    }
}
