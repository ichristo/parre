/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.server.util;

import java.math.BigDecimal;

import org.parre.shared.IceStormWorkDTO;
import org.parre.shared.NaturalThreatCalculationDTO;


/**
 * The Interface NaturalThreatCalculator.
 */
public interface NaturalThreatCalculator {
	public static final BigDecimal INDEX3_POWER_OUTAGE_PROBABLILITY = new BigDecimal("0.874");
	public static final BigDecimal INDEX4_POWER_OUTAGE_PROBABLILITY = new BigDecimal("0.0349");
	public static final BigDecimal INDEX5_POWER_OUTAGE_PROBABLILITY = new BigDecimal("0.000000354");
	
	public static final Integer INDEX3_DAYS = 5;
	public static final Integer INDEX4_DAYS = 10;
	public static final Integer INDEX5_DAYS = 21;
	
	void calculateEarthquakeRisk(NaturalThreatCalculationDTO calc);
	void calculateTornadoRisk(NaturalThreatCalculationDTO calc);
	void calculateFloodRisk(NaturalThreatCalculationDTO calc);
	void calculateHurricaneRisk(NaturalThreatCalculationDTO calc);
	void calculateIceStormRisk(NaturalThreatCalculationDTO calc);
	void calculateIceDamageProbabilities(IceStormWorkDTO work);
}
