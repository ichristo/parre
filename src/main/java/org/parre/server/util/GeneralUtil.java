/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.server.util;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;

import org.apache.log4j.Logger;
import org.parre.client.ClientSingleton;
import org.parre.server.messaging.service.ParreServiceImpl;

//import com.google.gwt.i18n.client.NumberFormat;

/**
 * The Class GeneralUtil.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 11/23/11
 */
public class GeneralUtil {
    private static transient final Logger log = Logger.getLogger(GeneralUtil.class);
    private static final DecimalFormat PROBABILITY_FORMAT = new DecimalFormat("'0'.000E0");

    public static boolean isNullOrEmpty(String value) {
        return value == null || value.length() == 0;
    }
    
    public static BigDecimal createBigDecimal(String input) {
    	try {
    		return new BigDecimal(input);
    	} catch (NumberFormatException nfe) {
    		log.error("Could not convert to BigDecimal: " + input, nfe);
    	}
    	return BigDecimal.ZERO;
    }
    
    
    public static String displayFormat(BigDecimal quantity) {
        if(quantity == null || quantity.doubleValue() == 0) {
            return "0";
        }
        if(quantity.doubleValue() < 1 && quantity.doubleValue() > -1) {
            return formatProbability(quantity);
        }
        if(quantity.doubleValue() >= 100  || quantity.doubleValue() <= -1000) {
            return toDisplayValueWithCommas(quantity);
        }
        if(quantity.longValue()%10 == 0) {
            return quantity.stripTrailingZeros().toPlainString();
        }
        return quantity.setScale(5, RoundingMode.HALF_EVEN).stripTrailingZeros().toString();
    }
    
    public static String displayFormatForRisk(BigDecimal quantity) {
        if(quantity == null || quantity.doubleValue() == 0) {
            return "0";
        }
        
        //if((quantity.compareTo(BigDecimal.ZERO) > 0) &&   (quantity.compareTo(BigDecimal.ONE) < 1) ){
        //	return "0";
        //}
        
        if(quantity.doubleValue() < 1 && quantity.doubleValue() > -1) {
            return formatProbability(quantity);
        }
        if(quantity.doubleValue() >= 1  || quantity.doubleValue() <= -1000) {
            return toDisplayValueWithCommas(quantity);
        }
        if(quantity.longValue()%10 == 0) {
            return quantity.stripTrailingZeros().toPlainString();
        }
        return quantity.setScale(5, RoundingMode.HALF_EVEN).stripTrailingZeros().toString();
    }
    
    public static String displayFormatForRatio(BigDecimal quantity) {
        if(quantity == null || quantity.doubleValue() == 0) {
            return "0";
        }
        return quantity.setScale(3, RoundingMode.CEILING).toString();
    }
    
    public static String formatProbability(BigDecimal bd) {
    	ParreServiceImpl parreService = new ParreServiceImpl();
    	
        if(parreService.getAppInitData().getUserPreferencesDTO().getUseScientificNotation()) {
            return PROBABILITY_FORMAT.format(bd);
        }
        return bd.doubleValue() == 0 ? "0" : bd.stripTrailingZeros().toPlainString();

    }
    
    private static String toDisplayValueWithCommas(BigDecimal quantity) {
        boolean isNegative = false;
        if(quantity.longValue() < 0) {
            isNegative = true;
        }
       
        return addCommas(Long.toString(quantity.abs().longValue()), Long.toString(quantity.abs().longValue()).length(), isNegative);
    }
    
    private static String addCommas(String str, int length, boolean isNegative) {
        if(length < 4) {
            if(isNegative) {
                str = "-" + str;
            }
            return str;
        }
        return addCommas(str.subSequence(0, (length - 3)) + "," + str.subSequence(length - 3, str.length()), length-3, isNegative);
    }
    
   

}
