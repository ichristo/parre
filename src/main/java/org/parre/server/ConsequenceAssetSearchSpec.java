/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.server;

import org.parre.server.domain.Asset;
import org.parre.server.domain.DirectedThreat;
import org.parre.shared.AssetSearchDTO;

/**
 * User: Swarn S. Dhaliwal
 * Date: 7/14/11
 * Time: 3:55 PM
*/
public class ConsequenceAssetSearchSpec implements SearchSpec<DirectedThreat> {
    private AssetSearchDTO criteriaAssetDTO;

    public ConsequenceAssetSearchSpec(AssetSearchDTO criteriaAssetDTO) {
        this.criteriaAssetDTO = criteriaAssetDTO;
    }

    public boolean isSatisfiedBy(DirectedThreat consequence) {
        Asset asset = consequence.getAsset();
        return FilterUtil.compareValues(criteriaAssetDTO.getAssetId() + "*", asset.getAssetId()) &&
                FilterUtil.compareValues(criteriaAssetDTO.getAssetName() + "*", asset.getName());
    }

}
