/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.server;

import org.parre.shared.ConsequenceDTO;
import org.parre.shared.ThreatDTO;
import org.parre.shared.types.ThreatCategoryType;

/**
 * User: Swarn S. Dhaliwal
 * Date: 7/14/11
 * Time: 3:55 PM
*/
public class ConsequenceThreatSearchSpec implements SearchSpec<ConsequenceDTO> {
    private ThreatDTO criteriaThreadDTO;

    public ConsequenceThreatSearchSpec(ThreatDTO criteriaThreadDTO) {
        this.criteriaThreadDTO = criteriaThreadDTO;
    }

    public boolean isSatisfiedBy(ConsequenceDTO consequenceDTO) {
        return FilterUtil.compareValues(criteriaThreadDTO.getName() + "*", consequenceDTO.getThreatName()) &&
                FilterUtil.compareValues(criteriaThreadDTO.getDescription() + "*", consequenceDTO.getThreatDescription());

    }

}
