/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.server.messaging.invocation;


import org.apache.log4j.Logger;
import org.parre.server.persistence.EntityManagerRegistry;
import org.parre.server.persistence.PersistenceContextRegistry;

import javax.persistence.EntityManager;

/**
 * The Class PersistenceContextHandler.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 2/21/12
 */
public abstract class PersistenceContextHandler {
    private static transient final Logger log = Logger.getLogger(PersistenceContextHandler.class);

    private String persistenceUnit;

    public PersistenceContextHandler(String persistenceUnit) {
        this.persistenceUnit = persistenceUnit;
    }

    protected String getPersistenceUnit() {
        return persistenceUnit;
    }

    protected void tearDownPersistenceContext(EntityManagerRegistry managerRegistry) {
        try {
            ServiceInvocationContext.getCurrentInstance().setEntityManager(null);
        } finally {
            managerRegistry.closeEntityManager();
        }
    }

    protected void rollbackTransaction(EntityManagerRegistry managerRegistry) {
        try {
            managerRegistry.rollbackTransaction();
        } catch (Throwable e) {
            log.error("Error during transaction rollback", e);
        }
    }

    protected void commitTransaction(EntityManagerRegistry managerRegistry) {
        try {
            managerRegistry.commitTransaction();
        } catch (Throwable e) {
            log.error("Error during transaction commit", e);
        }
    }

    protected EntityManagerRegistry setUpPersistenceContext() {
        EntityManagerRegistry managerRegistry = initEntityManagerRegistry();
        EntityManager entityManager = managerRegistry.entityManager();
        managerRegistry.beginTransaction();
        ServiceInvocationContext.getCurrentInstance().setEntityManager(entityManager);
        return managerRegistry;
    }

    private EntityManagerRegistry initEntityManagerRegistry() {
        try {
            return PersistenceContextRegistry.getEntityManagerAdaptor(getPersistenceUnit());
        }
        catch (RuntimeException t) {
            String message = "Error initialialing persistence context : " + t.getMessage();
            log.error(message, t);
            throw t;
        }
    }
}
