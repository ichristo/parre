/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.server.messaging.invocation;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;

import org.apache.log4j.Logger;
import org.parre.server.persistence.EntityManagerRegistry;
import org.parre.server.util.LoggingUtil;


/**
 * .
 * User: Swarn S. Dhaliwal
 * Date: 9/10/11
 * Time: 7:22 AM
 */
public class ServiceInvocationHandler extends PersistenceContextHandler implements InvocationHandler {
    private static transient final Logger log = Logger.getLogger(ServiceInvocationHandler.class);
    private Object serviceObject;

    public ServiceInvocationHandler(Object serviceObject, String persistenceUnit) {
        super(persistenceUnit);
        this.serviceObject = serviceObject;
        if (log.isDebugEnabled()) { log.debug("ServiceInvocationHandler created with persistenceUnit=" + persistenceUnit); }
    }

    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        LoggingUtil.debugLogMessage(log, "Invoking : ", method.getName(), " on ", serviceObject.getClass().getSimpleName(), " using ", Arrays.toString(args));
        EntityManagerRegistry managerRegistry = setUpPersistenceContext();
        try {
            Object result = method.invoke(serviceObject, args);
            commitTransaction(managerRegistry);
            return result;
        }
        catch (Throwable t) {
            rollbackTransaction(managerRegistry);
            Throwable exception;
            if (t instanceof InvocationTargetException) {
                exception = ((InvocationTargetException) t).getTargetException();
            }
            else {
                exception = t;
            }
            String message = "Unexpected error : " + exception.getMessage();
            log.error(message, exception);
            throw exception;
        }
        finally {
            tearDownPersistenceContext(managerRegistry);
        }
    }
}
