/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.server.messaging.invocation;


import javax.persistence.EntityManager;

import org.parre.shared.LoginInfo;

/**
 * .
 * User: Swarn S. Dhaliwal
 * Date: 10/8/11
 * Time: 9:16 AM
 */
public class ServiceInvocationContext {
    private static ThreadLocal<ServiceInvocationContext> contextContainer = new ThreadLocal<ServiceInvocationContext>();
    private LoginInfo loginInfo;
    private EntityManager entityManager;

    public ServiceInvocationContext(LoginInfo loginInfo) {
        this.loginInfo = loginInfo;
    }

    protected void setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public static void init(ServiceInvocationContext context) {
        if (contextContainer.get() != null) {
            throw new IllegalStateException("ServiceInvocationContext already initialzed");
        }
        contextContainer.set(context);
    }

    public static ServiceInvocationContext getCurrentInstance() {
        ServiceInvocationContext serviceInvocationContext = contextContainer.get();
        if (serviceInvocationContext == null) {
            throw new IllegalStateException("ServiceInvocationContext not initialized");
        }
        return serviceInvocationContext;
    }

    public static void destroy() {
        contextContainer.set(null);
    }

    public LoginInfo getLoginInfo() {
        return loginInfo;
    }

    public EntityManager getEntityManager() {
        return entityManager;
    }
}
