/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.server.messaging.service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.parre.client.messaging.RiskResilienceService;
import org.parre.server.domain.ParreAnalysis;
import org.parre.server.domain.RiskResilience;
import org.parre.server.domain.RiskResilienceAnalysis;
import org.parre.server.domain.UriOption;
import org.parre.server.domain.UriQuestion;
import org.parre.server.domain.UriResponse;
import org.parre.server.domain.manager.RiskResilienceManager;
import org.parre.shared.RiskResilienceAnalysisDTO;
import org.parre.shared.RiskResilienceDTO;
import org.parre.shared.UnitPriceDTO;
import org.parre.shared.UriQuestionDTO;
import org.parre.shared.UriResponseDTO;


/**
 * The Class RiskResilienceServiceImpl.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 2/15/12
 */
public class RiskResilienceServiceImpl extends ParreBaseService implements RiskResilienceService {
    private static transient final Logger log = Logger.getLogger(RiskResilienceServiceImpl.class);

    private static RiskResilienceService instance = new RiskResilienceServiceImpl();

    public static RiskResilienceService getInstance() {
        return instance;
    }

    public RiskResilienceAnalysisDTO getAnalysis(Long parreAnalysisId) {
        RiskResilienceAnalysis rra = RiskResilienceManager.getCurrentAnalysis(parreAnalysisId, entityManager());
        if (rra != null) {
            return rra.createDTO();
        }
        ParreAnalysis parreAnalysis = find(ParreAnalysis.class, parreAnalysisId);
        if (log.isDebugEnabled()) { 
        	log.debug("Risk Resilience does not exist for ParreAnalysis : " + parreAnalysis.getId() + " " + parreAnalysis.getName() + " " + parreAnalysis.isOption()); 
        }

        RiskResilienceAnalysisDTO analysisDTO =
                new RiskResilienceAnalysisDTO(parreAnalysis.createNameDescriptionDTO(), parreAnalysis.getId());
        return startAnalysis(analysisDTO);
    }

    private synchronized RiskResilienceAnalysisDTO startAnalysis(RiskResilienceAnalysisDTO analysisDTO) {
        RiskResilienceAnalysis rra = RiskResilienceManager.getCurrentAnalysis(analysisDTO.getParreAnalysisId(), entityManager());
        if (rra != null) {
            return rra.createDTO();
        }
        RiskResilienceAnalysis analysis = startRiskResilienceAnalysis(analysisDTO);
        return analysis.createDTO();
    }

    public List<RiskResilienceDTO> getRiskResilienceDTOs(Long riskResilienceAnalysisId, Long parreAnalysisId) {
        RiskResilienceAnalysis analysis = find(RiskResilienceAnalysis.class, riskResilienceAnalysisId);
        log.debug("RiskResilience ID = " + riskResilienceAnalysisId);
        log.debug("ParreAnalysis Id = " + parreAnalysisId);
        return createRiskResilienceDTOs(analysis, parreAnalysisId);
    }

    public RiskResilienceDTO saveRiskResilience(RiskResilienceDTO riskResilienceDTO) {
        RiskResilience riskResilience = find(RiskResilience.class, riskResilienceDTO.getId());
        riskResilience.copyFrom(riskResilienceDTO);
        saveEntity(riskResilience);
        return riskResilienceDTO;
    }

    public List<UriQuestionDTO> getUriQuestionDTOs() {
        List<UriQuestion> questions = RiskResilienceManager.getUriQuestions(entityManager());
        return UriQuestion.createDTOs(questions);
    }

    public List<UriResponseDTO> getUriResponseDTOs(Long riskResilienceAnalysisId) {
        List<UriResponseDTO> uriResponseDTOs = RiskResilienceManager.getUriResponseDTOs(riskResilienceAnalysisId, entityManager());
        Set<Long> existingQuestionIds = new HashSet<Long>();
        for (UriResponseDTO uriResponseDTO : uriResponseDTOs) {
            existingQuestionIds.add(uriResponseDTO.getUriQuestionId());
        }
        List<UriQuestionDTO> uriQuestionDTOs = getUriQuestionDTOs();
        for (UriQuestionDTO uriQuestionDTO : uriQuestionDTOs) {
            if (existingQuestionIds.contains(uriQuestionDTO.getId())) {
                continue;
            }
            UriResponseDTO dto = new UriResponseDTO(riskResilienceAnalysisId, uriQuestionDTO);
            dto.setId(saveUriResponse(dto));
            uriResponseDTOs.add(dto);
        }
        return uriResponseDTOs;
    }

    public void saveUriResponses(List<UriResponseDTO> dtos) {
        for (UriResponseDTO dto : dtos) {
            saveUriResponse(dto);
        }
    }

    private Long saveUriResponse(UriResponseDTO dto) {
        UriResponse uriResponse = null;
        Long uriOptionId = dto.getUriOptionId();
        if (dto.isNew()) {
            RiskResilienceAnalysis analysis = find(RiskResilienceAnalysis.class, dto.getRiskResilienceAnalysisId());
            UriOption uriOption = find(UriOption.class, uriOptionId);
            uriResponse = new UriResponse(analysis, uriOption);
        } else {
            uriResponse = find(UriResponse.class, dto.getId());
            if (!uriResponse.getUriOption().hasIdentity(uriOptionId)) {
                UriOption uriOption = find(UriOption.class, uriOptionId);
                uriResponse.setUriOption(uriOption);
            }
        }
        uriResponse.copyFrom(dto);
        saveEntityWithFlush(uriResponse);
        return uriResponse.getId();
    }

    public void updateUnitPrice(Long riskResilienceId, UnitPriceDTO unitPriceDTO) {
        RiskResilienceAnalysis analysis = find(RiskResilienceAnalysis.class, riskResilienceId);
        analysis.setUnitPriceDTO(unitPriceDTO);
        saveEntity(analysis);
    }

}
