/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */

/* TODO: Merge this back into the LGPL version */
package org.parre.server.messaging.service;

import java.io.InputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.parre.client.messaging.ParreService;
import org.parre.server.domain.*;
import org.parre.server.domain.manager.*;
import org.parre.server.messaging.service.async.AssetCountChanged;
import org.parre.server.messaging.service.async.AssetThreatCountChanged;
import org.parre.server.messaging.service.async.ParreCommandExecutor;
import org.parre.server.messaging.service.async.StatValuesChanged;
import org.parre.server.messaging.service.logic.AssetThreatAnalysisState;
import org.parre.shared.*;
import org.parre.shared.exception.DuplicateItemException;
import org.parre.shared.types.AssetThreatAnalysisType;
import org.parre.shared.types.LookupType;
import org.parre.shared.types.ParreAnalysisStatusType;
import org.parre.shared.types.ThreatCategoryType;


/**
 * The server side implementation of the RPC service.
 */
public class ParreServiceImpl extends ParreBaseService implements ParreService {
    private static transient Logger log = Logger.getLogger(ParreServiceImpl.class);

    private static ParreServiceImpl instance = new ParreServiceImpl();

    public static ParreServiceImpl getInstance() {
        return instance;
    }

    public List<LabelValueDTO> getContacts() {
        User user = getCurrentUser();
        List<Contact> contacts = ParreManager.getOrgContacts(user.getOrg().getId(), entityManager());
        List<LabelValueDTO> dtos = new ArrayList<LabelValueDTO>(contacts.size());
        for (Contact contact : contacts) {
            dtos.add(contact.createLabelValueDTO());
        }
        Collections.sort(dtos);
        return dtos;
    }

    public ContactDTO getContact(Long contactId) {
        Contact contact = ParreManager.getContact(contactId, entityManager());
        return contact.createDTO();
    }

    public List<LabelValueDTO> getProductServices() {
        User user = getCurrentUser();
        Set<Asset> productServices = user.getOrg().getAssets();
        List<LabelValueDTO> dtos = new ArrayList<LabelValueDTO>(productServices.size());
        for (Asset productService : productServices) {
            dtos.add(productService.createLabelValueDTO());
        }
        Collections.sort(dtos);
        return dtos;
    }

    public ParreAnalysisDTO saveParreAnalysis(ParreAnalysisDTO dto) throws DuplicateItemException {
        ParreAnalysis existingAnalysis = ParreManager.findParreAnalysis(dto.getName(), getCurrentUser().getOrg(), entityManager());
        if (log.isDebugEnabled()) { log.debug("New Analyis ? : " + dto.isNew()); }
        if (existingAnalysis != null && (dto.isNew() || !existingAnalysis.hasSameIdentity(dto))) {
            throw new DuplicateItemException("A ParreAnalysis already exists with name = " + dto.getName());
        }
        //Asset productServiceAsset = null;
        boolean statValuesChanged = false;
        ParreAnalysis parreAnalysis;
        if (dto.isNew()) {
            dto.setCreatedTime(new Date());
            dto.setStatus(ParreAnalysisStatusType.ACTIVE);
            dto.setAssetThreatThreshold(1);
            dto.setAssetThreshold(1);
            parreAnalysis = new ParreAnalysis();
            parreAnalysis.setOrg(getCurrentUser().getOrg());
        } else {
            parreAnalysis = find(ParreAnalysis.class, dto.getId());
        }
        
        if (parreAnalysis.isLocked()) {
            parreAnalysis.setName(dto.getName());
            parreAnalysis.setDescription(dto.getDescription());
            saveEntityWithFlush(parreAnalysis);
        } else {
        	//log.info("For New & Update Analysis");
            statValuesChanged = parreAnalysis.statValuesChanged(dto);
            parreAnalysis.copyFrom(dto);
           
            if(dto.getContactDTO() != null){
            	parreAnalysis.setContact(entityManager().find(Contact.class, dto.getContactDTO().getId()));
            } else{
            	parreAnalysis.setContact(null);
            }
            
            if(dto.getSiteLocationDTO() != null){
            	parreAnalysis.setSiteLocation(entityManager().find(SiteLocation.class, dto.getSiteLocationDTO().getId()));
            } else{
            	parreAnalysis.setSiteLocation(null);
            }
            
            
            if (log.isDebugEnabled()) { log.debug("Before Saving : " + parreAnalysis.getStatus()); }
            saveEntityWithFlush(parreAnalysis);
            if (statValuesChanged) {
                ParreCommandExecutor.getInstance().submitCommand(new StatValuesChanged(getLoginInfo(), parreAnalysis.getId()));
            }
            /*if (productServiceAsset != null) {
                productServiceAsset.setParreAnalysis(parreAnalysis);
                saveEntityWithFlush(productServiceAsset);
                ParreCommandExecutor.getInstance().submitCommand(new AssetAdded(getLoginInfo(), parreAnalysis.getId(), productServiceAsset.getId()));
            } */
        }
        return parreAnalysis.createDTO();
    }

    /*private Asset addProductServiceAsset(ParreAnalysis parreAnalysis, Asset newProductService) {
        Asset asset = AssetManager.findAsset(newProductService.getAssetId(), parreAnalysis.getId(), entityManager());
        if (asset == null) {
            return Asset.cloneAsset(newProductService);
        }
        return null;
    } */

    public AppInitData getAppInitData() {
        AppInitData data = new AppInitData();
        LookupValue lifeStatValue = ParreManager.getLookupValues(LookupType.LIFE_STAT_VALUE, entityManager()).get(0);
        LookupValue siStatValue = ParreManager.getLookupValues(LookupType.SERIOUS_INJURY_STAT_VALUE, entityManager()).get(0);
        StatValuesDTO statisticalValuesDTO = new StatValuesDTO();
        statisticalValuesDTO.setFatalityDTO(new AmountDTO(new BigDecimal(lifeStatValue.getName())));
        statisticalValuesDTO.setSeriousInjuryDTO(new AmountDTO(new BigDecimal(siStatValue.getName())));
        data.setStatisticalValuesDTO(statisticalValuesDTO);
        User user = getCurrentUser();
        Long orgId = user.getOrg().getId();
        List<ParreAnalysis> parreAnalyses = ParreAnalysisManager.getNotArchivedAnalyses(orgId, entityManager());
        List<ParreAnalysisDTO> activeAnalysisDTOs = new ArrayList<ParreAnalysisDTO>(parreAnalyses.size());
        for (ParreAnalysis parreAnalysis : parreAnalyses) {
            ParreAnalysisDTO parreAnalysisDTO = parreAnalysis.createDTOWithOptions();
            activeAnalysisDTOs.add(parreAnalysisDTO);
        }
        Long currentParreAnalysisId = 0L;
        if (user.getCurrentAnalysis() != null) {
            currentParreAnalysisId = user.getCurrentAnalysis().getId();
            ParreAnalysisDTO analysisDTO = ParreAnalysisDTO.findParreAnalysis(currentParreAnalysisId, activeAnalysisDTOs);
            data.setParreAnalysisDTO(analysisDTO);
        }
        data.setUserPreferencesDTO(user.getUserPreferences().createDTO());

        data.setActiveParreAnalyses(activeAnalysisDTOs);
        data.sortParreAnalysisDTOs();
        List<ThreatDTO> threatDTOs;
        try {
            threatDTOs = getDirectedThreatAnalysisDTOs(data.getParreAnalysisDTO().getBaselineId());
        } catch(NullPointerException e) {
            threatDTOs = getDirectedThreatAnalysisDTOs(currentParreAnalysisId);
        }

        data.setConsequenceAnalysisThreats(threatDTOs);
        
        data.setStateList(LookupValue.createDTOs(ParreManager.getLookupValues(LookupType.STATE_CODES, entityManager())));

		data.setReportSectionDTOs(ParreManager.getReportSectionValues(entityManager()));
		
		boolean reportSubSystemAvailable = false;
		try {
			InputStream rptStream = this.getClass().getResourceAsStream("/jasperreports/ParreRptPDF.jasper");
			if (log.isDebugEnabled()) { log.debug("Found jasper definition: " + (rptStream != null)); }
			reportSubSystemAvailable = !(rptStream == null);
		} catch (Exception ex) {
			reportSubSystemAvailable = false;
		}
		data.setReportSubSystemAvailable(reportSubSystemAvailable);
		
        return data;
    }

    public ParreAnalysisDTO getParreAnalysis(Long parreAnalysisId) {
        return findParreAnalysis(parreAnalysisId).createDTO();
    }

    public void setCurrentParreAnalysis(Long parreAnalysisId) {
        User currentUser = getCurrentUser();
        ParreAnalysis analysis = find(ParreAnalysis.class, parreAnalysisId);
        currentUser.setCurrentAnalysis(analysis);
        saveEntity(currentUser);
    }

    public Integer updateAssetThreshold(Integer threshold, Long parreAnalysisId) {
        ParreAnalysis parreAnalysis = findParreAnalysis(parreAnalysisId);
        parreAnalysis.setAssetThreshold(threshold);
        saveEntityWithFlush(parreAnalysis);
        ParreCommandExecutor.getInstance().submitCommand(new AssetCountChanged(getLoginInfo(), parreAnalysisId));
        return threshold;
    }

    public Integer updateAssetThreatData(Integer thresholdValue, Set<AssetThreatLevelDTO> levelDTOs, Long parreAnalysisId) {
        ParreAnalysis parreAnalysis = findParreAnalysis(parreAnalysisId);
        parreAnalysis.setAssetThreatThreshold(thresholdValue);
        saveEntity(parreAnalysis);
        for (AssetThreatLevelDTO levelDTO : levelDTOs) {
            saveAssetThreatLevel(levelDTO, parreAnalysisId);
        }
        ParreCommandExecutor.getInstance().submitCommand(new AssetThreatCountChanged(getLoginInfo(), parreAnalysisId));
        return thresholdValue;
    }

    public void updateAssetThreatLevel(AssetThreatLevelDTO levelDTO, Long parreAnalysisId) {
        saveAssetThreatLevel(levelDTO, parreAnalysisId);
        ParreCommandExecutor.getInstance().submitCommand(new AssetThreatCountChanged(getLoginInfo(), parreAnalysisId));
    }

    private void saveAssetThreatLevel(AssetThreatLevelDTO levelDTO, Long parreAnalysisId) {
        AssetThreatLevel level = ParreManager.findAssetThreatLevel(parreAnalysisId, levelDTO, entityManager());
        if (level == null) {
            level = new AssetThreatLevel(parreAnalysisId, levelDTO);
        }
        level.setLevel(levelDTO.getLevel());
        saveEntity(level);
    }

    public Integer getAssetThreatAnalysisCount(Long parreAnalysisId, AssetThreatAnalysisType analysisType) {
        List<Asset> assetsForAnalysis = getAssetsForAnalysis(parreAnalysisId);
        ThreatCategoryType threatCategoryType = analysisType.getThreatCategoryType();
        List<Threat> activeThreats;
        if (threatCategoryType.isNone()) {
            activeThreats = ThreatManager.getActiveThreats(parreAnalysisId, entityManager());
        } else {
            activeThreats = ThreatManager.getActiveThreats(parreAnalysisId, threatCategoryType, entityManager());
        }

        AssetThreatAnalysisState analysisState = createAssetThreatAnalysisState(parreAnalysisId);
        int count = 0;
        for (Asset asset : assetsForAnalysis) {
            for (Threat threat : activeThreats) {
                if (!threat.getThreatCategory().isManMadeHazard()) {
                    count++;
                } else if (analysisState.analysisRequired(asset, threat)) {
                    count++;
                }
            }
        }
        return count;
    }

    public void lockCurrentAnalysis(Long parreAnalysisId) {
        ParreAnalysis parreAnalysis = findParreAnalysis(parreAnalysisId);
        parreAnalysis.setStatus(ParreAnalysisStatusType.LOCKED);
        saveEntity(parreAnalysis);
    }

    public void unlockCurrentAnalysis(Long parreAnalysisId) {
        ParreAnalysis parreAnalysis = findParreAnalysis(parreAnalysisId);
        parreAnalysis.setStatus(ParreAnalysisStatusType.ACTIVE);
        saveEntity(parreAnalysis);
    }

    public ContactDTO saveContact(ContactDTO contactDTO) {
        Contact contact = null;
        if (contactDTO.isNew()) {
            contact = new Contact();
            contact.setOrg(getCurrentUser().getOrg());
        }
        else {
            contact = find(Contact.class, contactDTO.getId());
        }
        contact.copyFrom(contactDTO);
        saveEntityWithFlush(contact);
        contactDTO.setId(contact.getId());
        return contactDTO;
    }

    public ProposedMeasureDTO saveProposedMeasure(ProposedMeasureDTO dto) {
        assertNotNew(dto);
        ProposedMeasure proposedMeasure = find(ProposedMeasure.class, dto.getId());
        proposedMeasure.copyFrom(dto);
        saveEntity(proposedMeasure);
        return dto;
    }

    public ParreAnalysisDTO copyParreAnalysis(Long parreAnalysisId) {
        ParreAnalysis parreAnalysis = findParreAnalysis(parreAnalysisId);
        NameDescriptionDTO copyDTO = parreAnalysis.createNameDescriptionDTO();
        copyDTO.setName(copyDTO.getName() + " - " + System.currentTimeMillis());
        ParreAnalysis copiedParreAnalysis = copyParreAnalysis(copyDTO, parreAnalysis, true);

        return copiedParreAnalysis.createDTO();
    }

    public List<ProposedMeasureDTO> getProposedMeasures(Long parreAnalysisId) {
        List<ProposedMeasure> proposedMeasures = ParreManager.getProposedMeasures(parreAnalysisId, entityManager());
        return ProposedMeasure.createDTOs(proposedMeasures);
    }

    public List<ProposedMeasureDTO> getBaselineProposedMeasures(Long baselineParreAnalysisId) {
        List<ProposedMeasure> proposedMeasures = ParreManager.getBaselineProposedMeasures(baselineParreAnalysisId, entityManager());
        return ProposedMeasure.createDTOs(proposedMeasures);
    }

    public void associateProposedMeasure(Long proposedMeasureId, Long parreAnalysisId) {
        ProposedMeasure proposedMeasure = find(ProposedMeasure.class, proposedMeasureId);
        ParreAnalysis parreAnalysis = find(ParreAnalysis.class, parreAnalysisId);
        parreAnalysis.addProposedMeasure(proposedMeasure);
        saveEntity(parreAnalysis);
    }

    public ProposedMeasureDTO createAndAssociateProposedMeasure(ProposedMeasureDTO dto) {
        assertNew(dto);
        ParreAnalysis parreAnalysis = find(ParreAnalysis.class, dto.getParreAnalysisId());
        ProposedMeasure pm = new ProposedMeasure();
        pm.copyFrom(dto);
        pm.setBaselineParreAnalysis(parreAnalysis.getBaseline());
        saveEntityWithFlush(pm);
        parreAnalysis.addProposedMeasure(pm);
        saveEntity(parreAnalysis);
        dto.setId(pm.getId());
        return dto;
    }

    public void disassociateProposedMeasure(Long pmId, Long parreAnalysisId) {
        ProposedMeasure proposedMeasure = find(ProposedMeasure.class, pmId);
        ParreAnalysis parreAnalysis = find(ParreAnalysis.class, parreAnalysisId);
        parreAnalysis.getProposedMeasures().remove(proposedMeasure);
        saveEntity(parreAnalysis);
    }

    public SiteLocationDTO saveSiteLocation(SiteLocationDTO dto) {
        SiteLocation siteLocation;
        if(dto.isNew()) {
            siteLocation = new SiteLocation();
        }
        else {
            siteLocation = find(SiteLocation.class, dto.getId());
        }
        siteLocation.copyFrom(dto);
        saveEntityWithFlush(siteLocation);
        dto.setId(siteLocation.getId());
        return dto;
    }

    public List<LabelValueDTO> getSiteLocationList() {
        List<SiteLocation> siteLocationList = ParreManager.getSiteLocationList(entityManager());
        List<LabelValueDTO> dtos = new ArrayList<LabelValueDTO>(siteLocationList.size());
        for(SiteLocation siteLocation : siteLocationList) {
            dtos.add(siteLocation.createLabelValueDTO());
        }
        Collections.sort(dtos);
        return dtos;
    }

    public SiteLocationDTO getSiteLocation(Long locationId) {
        SiteLocation siteLocation = ParreManager.getSiteLocation(locationId, entityManager());
        return siteLocation.createDTO();
    }

    public void deleteAnalysis(Long parreAnalysisId) {
        ParreAnalysis analysis = find(ParreAnalysis.class, parreAnalysisId);
        //RRM
        RiskBenefitManager.deleteRiskBenefitAnalysisFromParreAnalysis(parreAnalysisId, entityManager());
        //RRA
        RiskResilienceManager.deleteRiskResilienceFromParreAnalysis(parreAnalysisId, entityManager());

        try {
            RiskResilienceAnalysis riskResilienceAnalysis = RiskResilienceManager.getCurrentAnalysis(parreAnalysisId, entityManager());
            RiskResilienceManager.deleteURIResponses(riskResilienceAnalysis.getId(), entityManager());
            RiskResilienceManager.deleteRiskResilienceAnalysis(riskResilienceAnalysis.getId(), entityManager());
            riskResilienceAnalysis = null;
        } catch (NullPointerException e) {
            log.debug("No risk resilience analysis to delete");
        }

        //Vulnerability
        VulnerabilityManager.deleteCounterMeasuresWithParreAnalysis(parreAnalysisId, entityManager());

        AssetManager.deleteAssetThreatPairsWithParreAnalysis(analysis.getId(), entityManager());

        List<PathAnalysis> pathAnalysisList =  VulnerabilityManager.getPathAnalyses(parreAnalysisId, entityManager());
        for(PathAnalysis pathAnalysis : pathAnalysisList) {
            VulnerabilityServiceImpl.getInstance().deletePathAnalysis(pathAnalysis.getId());
        }
        pathAnalysisList = null;

        List<DiagramAnalysis> diagramAnalysisList = VulnerabilityManager.getAllDiagramAnalysis(parreAnalysisId, entityManager());
        for(DiagramAnalysis diagramAnalysis : diagramAnalysisList) {
            VulnerabilityServiceImpl.getInstance().deleteDiagramAnalysis(diagramAnalysis.getId());
        }
        diagramAnalysisList = null;
        List<ProxyIndication> proxyIndicationList = VulnerabilityManager.getProxyIndications(parreAnalysisId, entityManager());
        for(ProxyIndication proxyIndication :proxyIndicationList) {
            VulnerabilityServiceImpl.getInstance().deleteProxyIndication(proxyIndication.getId());
        }
        proxyIndicationList = null;
        analysis.setDefaultEarthquakeProfile(null);
        analysis.setDefaultHurricaneProfile(null);
        saveEntityWithFlush(analysis);
        for(EarthquakeProfile earthquakeProfile : analysis.getEarthquakeProfiles()) {
            NaturalThreatServiceImpl.getInstance().deleteEarthquakeProfile(earthquakeProfile.getId());
        }
        for(HurricaneProfile hurricaneProfile : analysis.getHurricaneProfiles()) {
            NaturalThreatServiceImpl.getInstance().deleteHurricaneProfile(hurricaneProfile.getId());
        }
        analysis.setEarthquakeProfiles(null);
        analysis.setHurricaneProfiles(null);
        analysis.setBaseline(null);
        saveEntityWithFlush(analysis);

        DirectedThreatManager.deleteDirectedThreatAnalysisWithParreAnalysis(parreAnalysisId, entityManager());

        DpManager.deleteDpAnalysisWithParreAnalysis(parreAnalysisId, entityManager());

        NaturalThreatManager.deleteNaturalThreatAnalysisWithParreAnalysis(parreAnalysisId, entityManager());

        ParreManager.removeCurrentParreAnalysisFromUsers(parreAnalysisId, entityManager());

        if(analysis.isBaseline()) {
            AssetManager.deleteAssetWithParreAnalysis(parreAnalysisId, entityManager());
            ThreatManager.deleteInactiveThreats(parreAnalysisId, entityManager());
            ThreatManager.deleteUserMadeThreats(parreAnalysisId, entityManager());
            ParreAnalysisManager.deleteProposedMeasures(parreAnalysisId, entityManager());
        }
        
        ReportsManager.deleteReport(parreAnalysisId, entityManager());

        entityManager().remove(analysis);
        entityManager().flush();
    }

    public List<Long> deleteParreAnalysis(Long parreAnalysisId) {
        ParreAnalysis analysis = find(ParreAnalysis.class, parreAnalysisId);
        if(analysis.isBaseline()) {
            for(Long id : BaseEntity.extractIds(analysis.getOptionParreAnalyses())) {
                deleteAnalysis(id);
            }
            deleteAnalysis(parreAnalysisId);
            return analysis.getAllIds();
        }
        else {
            deleteAnalysis(parreAnalysisId);
            ArrayList<Long> list = new ArrayList<Long>();
            list.add(parreAnalysisId);
            return list;
        }
    }
}
