/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.server.messaging.service;

import org.parre.server.domain.*;

/**
 * A factory for creating Dp objects.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 1/10/12
 */
public class DpFactory implements AssetThreatAnalysisFactory<AssetThreatEntity> {
    private static DpFactory instance = new DpFactory();

    public static DpFactory getInstance() {
        return instance;
    }

    public Dp create(AssetThreat assetThreat) {
        return new Dp(assetThreat);
    }

    public AssetThreatEntity create() {
        return new Dp();
    }
}
