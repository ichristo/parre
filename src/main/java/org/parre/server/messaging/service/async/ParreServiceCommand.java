/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.server.messaging.service.async;

import org.apache.log4j.Logger;
import org.parre.server.messaging.invocation.ServiceInvocationContext;
import org.parre.server.persistence.EntityManagerRegistry;
import org.parre.shared.LoginInfo;

/**
 * The Class ParreServiceCommand.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 2/21/12
 */
public abstract class ParreServiceCommand  implements ParreCommand {
    private static transient final Logger log = Logger.getLogger(ParreServiceCommand.class);

    private LoginInfo loginInfo;
    private Long parreAnalysisId;

    protected ParreServiceCommand(LoginInfo loginInfo, Long parreAnalysisId) {
        this.loginInfo = loginInfo;
        this.parreAnalysisId = parreAnalysisId;
    }

    public final Object call() throws Exception {
    	if (log.isDebugEnabled()) { log.debug("Executing command : " + getClass().getSimpleName()); }
        ServiceInvocationContext context = new ServiceInvocationContext(loginInfo);
        ServiceInvocationContext.init(context);
        try {
            return execute();
        }
        finally {
            ServiceInvocationContext.destroy();
        }

    }
    protected abstract Object execute() throws Exception;

    public LoginInfo getLoginInfo() {
        return loginInfo;
    }

    public Long getParreAnalysisId() {
        return parreAnalysisId;
    }
}
