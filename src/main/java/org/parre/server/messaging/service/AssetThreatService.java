/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.server.messaging.service;

/**
 * The Interface AssetThreatService.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 2/24/12
 */
public interface AssetThreatService {
    void assetCountChanged(Long parreAnalysisId);

    void assetThreatCountChanged(Long parreAnalysisId);

    void assetAdded(Long parreAnalysisId, Long addedAssetId);

    void assetRemoved(Long parreAnalysisId, Long removedAssetId);

    void threatAdded(Long parreAnalysisId, Long addedThreatId);

    void threatRemoved(Long parreAnalysisId, Long removedThreatId);

    void statValuesChanged(Long parreAnalysisId);

    void consequenceUpdated(Long parreAnalysisId);

    void vulnerabilityUpdated(Long parreAnalysisId);
}
