/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.server.messaging.service;


import java.util.*;

import org.parre.client.messaging.DpService;
import org.parre.server.domain.AssetThreatEntity;
import org.parre.server.domain.Dp;
import org.parre.server.domain.DpAnalysis;
import org.parre.server.domain.manager.DpManager;
import org.parre.shared.DpAnalysisDTO;
import org.parre.shared.DpDTO;
import org.parre.shared.ThreatDTO;
import org.parre.shared.types.DpType;
import org.parre.shared.types.ThreatCategoryType;

/**
 * The Class DpServiceImpl.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 12/7/11
 */
public class DpServiceImpl extends ParreBaseService implements DpService {
    private static DpServiceImpl instance = new DpServiceImpl();

    public static DpServiceImpl getInstance() {
        return instance;
    }

    private DpServiceImpl() {

    }

    public DpDTO save(DpDTO dpDTO) {
        Dp dp = entityManager().find(Dp.class, dpDTO.getId());
        dp.copyFrom(dpDTO);
        dp.updateFinancialTotal();
        saveEntityWithFlush(dp);
        return dpDTO;
    }

    public DpAnalysisDTO startAnalysis(DpAnalysisDTO dto) {
        DpAnalysis analysis = startDpAnalysis(dto);
        return analysis.copyInto(new DpAnalysisDTO());
    }

    public DpAnalysisDTO getCurrentAnalysis(Long parreAnalysisId) {
        DpAnalysis analysis = findCurrentDpAnalysis(parreAnalysisId);
        return analysis != null ? analysis.copyInto(new DpAnalysisDTO()) : null;
    }

    public boolean hasAnalysis(Long parreAnalysisId) {
        return findCurrentDpAnalysis(parreAnalysisId) != null;
    }

    public DpDTO addSubCategory(DpDTO subCatDTO) {
        assertNew(subCatDTO);
        Dp dp = find(Dp.class, subCatDTO.getParentId());
        dp.setType(DpType.MAIN_WITH_SUB);
        dp.clearValues();
        saveEntity(dp);
        Dp subCat = new Dp(dp);
        subCat.setParreAnalysis(dp.getParreAnalysis());
        subCat.setName(subCatDTO.getName());
        subCat.setDescription(subCatDTO.getDescription());
        saveEntityWithFlush(subCat);
        subCat.copyInto(subCatDTO);
        subCatDTO.setId(subCat.getId());
        return subCatDTO;
    }

    public void removeCategory(Long dpId) {
        Dp cat = find(Dp.class, dpId);
        Long parentId = cat.getParentId();
        entityManager().remove(cat);
        entityManager().flush();
        boolean hasSubCategories = DpManager.hasSubCategories(parentId, entityManager());
        if (!hasSubCategories) {
            Dp dp = find(Dp.class, parentId);
            dp.setType(DpType.MAIN);
            saveEntity(dp);
        }
    }

    public List<DpDTO> getSearchResults(ThreatDTO dto, Long parreAnalysisId) {
        DpAnalysis analysis = findCurrentDpAnalysis(parreAnalysisId);
        return analysis != null ? DpManager.search(dto, analysis.getId(), entityManager()) :
                Collections.<DpDTO>emptyList();
    }

    private DpAnalysis findCurrentDpAnalysis(Long parreAnalysisId) {
        return DpManager.getCurrentAnalysis(parreAnalysisId, entityManager());
    }

}
