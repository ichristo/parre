/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.server.messaging.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.parre.client.messaging.ThreatService;
import org.parre.server.domain.InactiveThreat;
import org.parre.server.domain.ParreAnalysis;
import org.parre.server.domain.Threat;
import org.parre.server.domain.ThreatNote;
import org.parre.server.domain.manager.LotManager;
import org.parre.server.domain.manager.ThreatManager;
import org.parre.server.messaging.ParreServiceFactory;
import org.parre.server.messaging.service.async.ParreCommandExecutor;
import org.parre.server.messaging.service.async.ThreatAdded;
import org.parre.server.messaging.service.async.ThreatRemoved;
import org.parre.shared.LabelValueDTO;
import org.parre.shared.NoteDTO;
import org.parre.shared.ThreatDTO;
import org.parre.shared.exception.ItemInUseException;
import org.parre.shared.types.ThreatCategoryType;


/*
	Date			Author				Changes
    9/29/11	Swarn S. Dhaliwal	Created
*/
/**
 * The Class ThreatServiceImpl.
 */
public class ThreatServiceImpl extends ParreBaseService implements  ThreatService {
    private static transient final Logger log = Logger.getLogger(ThreatServiceImpl.class);

    private static ThreatService instance = createInstance();

    public static ThreatService getInstance() {
        return instance;
    }

    private static ThreatService createInstance() {
        return (ThreatService) ParreServiceFactory.createServiceProxy(new ThreatServiceImpl());
    }

    public List<ThreatDTO> getThreatSearchResults(ThreatDTO dto, Long parreAnalysisId) {
        parreAnalysisId = getBaselineId(parreAnalysisId);
        List<Threat> threats = ThreatManager.searchThreats(dto, entityManager());
        List<ThreatDTO> dtOs = Threat.createDTOs(threats, parreAnalysisId);
        List<Long> threatIds = new ArrayList<Long>(dtOs.size());
        for (ThreatDTO dtO : dtOs) {
            threatIds.add(dtO.getId());

        }
        Map<Long, String> threatReasonMap = ThreatManager.getInactiveReasons(parreAnalysisId, threatIds, entityManager());
        for (ThreatDTO dtO : dtOs) {
            String reason = threatReasonMap.get(dtO.getId());
            if (reason != null) {
                dtO.setActive(false);
                dtO.setInactiveReason(reason);
            }
            dtO.setNoteDTOs(ThreatManager.getThreatNotes(dtO.getId(), entityManager()));
            dtO.setDetectionLikelihood(LotManager.getDetectionLikelihood(entityManager(), dtO.getId()));
        }
        return dtOs;
    }

    public List<LabelValueDTO> getThreatCategories() {
        List<LabelValueDTO> dtos = new ArrayList<LabelValueDTO>(3);
        dtos.add(new LabelValueDTO("Dependency and Proximity", ThreatCategoryType.DEPENDENCY_PROXIMITY.name()));
        dtos.add(new LabelValueDTO("Man-Made Hazard", ThreatCategoryType.MAN_MADE_HAZARD.name()));
        dtos.add(new LabelValueDTO("Natural Hazard", ThreatCategoryType.NATURAL_HAZARD.name()));
        return dtos;
    }

    public ThreatDTO saveThreat(ThreatDTO dto, Long baselineId) {
        Threat threat = null;
        boolean isNewThreat = dto.isNew();
        if (isNewThreat) {
            threat = new Threat();
            threat.setBaselineId(baselineId);
        }
        else {
            threat = entityManager().find(Threat.class, dto.getId());
        }
        threat.copyFrom(dto);
        if (threat.getUserDefined()) {
        	// Since the copyFrom will overwrite this...
        	threat.setBaselineId(baselineId);
        }
        saveEntityWithFlush(threat);
        
        // If manual threat and 'man made hazard', add a threat likelihood of 50%
        if (threat.getUserDefined()) {
        	ThreatManager.updateUserDefinedThreatLikelihood(threat, dto.getDetectionLikelihood(), entityManager());
        }

        for(NoteDTO noteDTO : dto.getNoteDTOs()) {
            if(noteDTO.isNew()) {
                saveThreatNote(noteDTO, threat);
            }
        }

        Long threatId = threat.getId();
        dto.setId(threatId);
        if (isNewThreat) {
            threatAdded(baselineId, threatId);
        }
        if (log.isDebugEnabled()) { log.debug("new dto id = " + threat.getBaselineId()); }
        return dto;
    }

    private void saveThreatNote(NoteDTO noteDTO, Threat asset) {

        log.debug("ThreatNoteDTO.id = " + noteDTO.toString());
        ThreatNote threatNote = null;
        if(noteDTO.getId() ==  null) {
            threatNote = new ThreatNote();
            threatNote.setThreat(asset);
        } else {
            threatNote = entityManager().find(ThreatNote.class, noteDTO.getId());
        }

        threatNote.copyFrom(noteDTO);
        saveEntityWithFlush(threatNote);

    }

    private void threatAdded(Long baselineId, Long threatId) {
        ParreCommandExecutor.getInstance().submitCommand(new ThreatAdded(getLoginInfo(), baselineId, threatId));
    }

    private void threatRemoved(Long parreAnalysisId, Long threatId) {
        ParreCommandExecutor.getInstance().submitCommand(new ThreatRemoved(getLoginInfo(), parreAnalysisId, threatId));
    }

    public ThreatDTO updateThreatActive(ThreatDTO dto, Long parreAnalysisId) {
        if (dto.getActive()) {
            InactiveThreat it = ThreatManager.findInactiveThreat(parreAnalysisId, dto.getId(), entityManager());
            entityManager().remove(it);
            threatAdded(parreAnalysisId, it.getThreat().getId());
        }
        else {
            ParreAnalysis parreAnalysis = entityManager().find(ParreAnalysis.class, parreAnalysisId);
            Threat threat = entityManager().find(Threat.class, dto.getId());
            InactiveThreat it = new InactiveThreat();
            it.setParreAnalysis(parreAnalysis);
            it.setThreat(threat);
            it.setInactiveReason(dto.getInactiveReason());
            saveEntityWithFlush(it);
            threatRemoved(parreAnalysisId, threat.getId());
        }
        return dto;
    }

    public void deleteThreat(ThreatDTO threatDTO) throws ItemInUseException {
        boolean inUse = ThreatManager.isThreatInUse(threatDTO.getId(), entityManager());
        if (inUse) {
            throw new ItemInUseException("This is in use and cannot be deleted");
        }
        
        List<InactiveThreat> inactiveThreats = ThreatManager.getInactiveThreats(threatDTO.getId(), entityManager());
        for (InactiveThreat inactiveThreat : inactiveThreats) {
        	entityManager().remove(inactiveThreat);
        }
        
        int assetThreatAnalysisCount = 
        		entityManager().createQuery("delete from AssetThreatAnalysis ata where ata.assetThreat in (select at from AssetThreat at where at.threat.id = :threatId)")
        										.setParameter("threatId", threatDTO.getId())
        										.executeUpdate();
        
        int riskResilienceCount = 
        		entityManager().createQuery("delete from RiskResilience rr where rr.assetThreat in (select at from AssetThreat at where at.threat.id = :threatId)")
        										.setParameter("threatId", threatDTO.getId())
        										.executeUpdate();
        
        int riskBenefitCount = 
        		entityManager().createQuery("delete from RiskBenefit rb where rb.assetThreat in (select at from AssetThreat at where at.threat.id = :threatId)")
        										.setParameter("threatId", threatDTO.getId())
        										.executeUpdate();
        
        int assetThreatCount = entityManager().createQuery("delete from AssetThreat at where at.threat.id = :threatId")
        	.setParameter("threatId", threatDTO.getId())
        	.executeUpdate();
        
        int threatNoteCount = entityManager().createQuery("delete from ThreatNote tn where tn.threat.id = :threatId")
        		.setParameter("threatId", threatDTO.getId())
        		.executeUpdate();
        
        int threatLikelihoodCount = entityManager().createQuery("delete from DetectionLikelihood dl where dl.threat.id = :threatId")
        		.setParameter("threatId", threatDTO.getId())
        		.executeUpdate();
        
        if (log.isInfoEnabled()) { log.info("Removed " + assetThreatAnalysisCount + " analyses, " + 
        													riskResilienceCount + " risk resiliences, " +
        													threatNoteCount + " threat notes, " +
        													threatLikelihoodCount + " detection likelihoods, " +
        													riskBenefitCount + " risk benefits and " + 
        													assetThreatCount + " asset threats while deleting threat: " + String.valueOf(threatDTO)); }
        
        Threat threat = find(Threat.class, threatDTO.getId());
        entityManager().remove(threat);
        entityManager().flush();
    }

}