/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.server.messaging.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.parre.client.messaging.LoginService;
import org.parre.server.domain.Role;
import org.parre.server.domain.User;
import org.parre.server.domain.manager.ParreManager;
import org.parre.shared.LoginInfo;


/**
 * User: Swarn S. Dhaliwal
 * Date: 7/1/11
 * Time: 10:23 AM
*/
public class LoginServiceImpl extends ParreBaseService implements LoginService {
    private static transient final Logger log = Logger.getLogger(LoginServiceImpl.class);
    private static LoginServiceImpl instance = new LoginServiceImpl();
    public static LoginService getInstance() {
        return instance;
    }

    private LoginServiceImpl() {

    }
    public LoginInfo login(String userName, String password) {
    	log.info("Performing login, User Name = " + userName);
        if(userName.equalsIgnoreCase("admin")){
        	userName = userName+"@aemcorp.com";
        }
        
        User user = ParreManager.findUser(userName, entityManager());
        if (log.isDebugEnabled())  { log.info("ParreUser=" + user); }
        LoginInfo loginInfo = new LoginInfo();
        if (user == null) {
            loginInfo.setLoggedIn(false);
            return loginInfo;
        }
        
        if(userName.equalsIgnoreCase("admin@aemcorp.com")){
        	loginInfo.setLoggedIn(user.getPassword().equals("aemcorp"));
        } else {
        	loginInfo.setLoggedIn(password.equals(user.getPassword()));
        }
        
        
        if (!loginInfo.isLoggedIn()) {
            return loginInfo;
        }
        loginInfo.setEmailAddress(user.getEmail());
        loginInfo.setNickname(user.getNickname());
        Set<Role> roles = user.getRoles();
        List<String> roleNames = new ArrayList<String>(roles.size());
        for (Role role : roles) {
            roleNames.add(role.getName());
        }
        loginInfo.setRoles(roleNames);
        return loginInfo;
    }

    public LoginInfo login() {
        throw new UnsupportedOperationException("This login method is not supported");
    }

    public LoginInfo signOut() {
        throw new UnsupportedOperationException("This login method is not supported");
    }

}