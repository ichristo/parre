/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.server.messaging.service;

import org.apache.log4j.Logger;
import org.parre.client.messaging.LotService;
import org.parre.server.domain.*;
import org.parre.server.domain.manager.DirectedThreatManager;
import org.parre.server.domain.manager.LookupDataManager;
import org.parre.server.domain.manager.LotManager;
import org.parre.shared.*;
import org.parre.shared.types.LotAnalysisType;

import java.math.BigDecimal;
import java.util.*;

/**
 * The Class LotServiceImpl.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 12/1/11
 */
public class LotServiceImpl extends ParreBaseService implements LotService {
    private static transient final Logger log = Logger.getLogger(LotServiceImpl.class);

    private static final LotServiceImpl instance = new LotServiceImpl();

    public static LotServiceImpl getInstance() {
        return instance;
    }

    private LotServiceImpl() {

    }
    public LotDTO save(LotDTO dto) {
        DirectedThreat lot = entityManager().find(DirectedThreat.class, dto.getId());
        lot.copyFrom(dto);
        saveEntityWithFlush(lot);
        return dto;
    }

    public LotAnalysisDTO startAnalysis(LotAnalysisDTO dto) {
        return startDirectedThreatAnalysis(dto);
    }

    public LotDTO saveWithProxyIndication(LotDTO lotDTO) {
        DirectedThreat lot = find(DirectedThreat.class, lotDTO.getId());
        lot.setLotAnalysisType(LotAnalysisType.PROXY_INDICATOR);
        ProxyIndication proxyIndication = lot.getProxyIndication();
        if (proxyIndication == null) {
            proxyIndication = find(ProxyIndication.class, lotDTO.getProxyIndicationDTO().getId());
            lot.setProxyIndication(proxyIndication);
        }
        BigDecimal bigDecimal = calculateProbability(proxyIndication, lot).getCalculatedProbability();
        lot.setLot(bigDecimal);
        saveEntityWithFlush(lot);
        lotDTO.setLot(bigDecimal);
        lotDTO.setLotAnalysisType(LotAnalysisType.PROXY_INDICATOR);
        return lotDTO;
    }

    public List<LabelValueDTO> getProxyCities() {
        List<ProxyCityDTO> proxyCityTierDTOs = LotManager.getProxyCityTierDTOs(entityManager());
        List<LabelValueDTO> labelValueDTOs = new ArrayList<LabelValueDTO>(proxyCityTierDTOs.size());
        for (ProxyCityDTO proxyCityTierDTO : proxyCityTierDTOs) {
            labelValueDTOs.add(new LabelValueDTO(proxyCityTierDTO.getCity(), String.valueOf(proxyCityTierDTO.getId())));
        }
        labelValueDTOs.add(new LabelValueDTO("Other", "-1"));
        return labelValueDTOs;
    }

    public List<LabelValueDTO> getProxyTargetTypes() {
        List<ProxyTargetType> proxyTargetTypes = LotManager.getProxyTargetTypes(entityManager());
        List<LabelValueDTO> labelValueDTOs = new ArrayList<LabelValueDTO>(proxyTargetTypes.size());
        for (ProxyTargetType targetType : proxyTargetTypes) {
            labelValueDTOs.add(new LabelValueDTO(targetType.getName(), String.valueOf(targetType.getId())));
        }
        return labelValueDTOs;
    }

    public ProxyIndicationDTO getProxyIndication(Long lotId) {
        ProxyIndication proxyIndication = LotManager.findProxyIndication(lotId, entityManager());
        if (proxyIndication == null) {
            return null;
        }

        return calculateProbability(proxyIndication, find(DirectedThreat.class, lotId));
    }

    public ProxyIndicationDTO saveProxyIndication(ProxyIndicationDTO dto) {
        ProxyIndication proxyIndication;
        boolean isNewProxyIndication = dto.isNew();
        if (isNewProxyIndication) {
            proxyIndication = new ProxyIndication();
            proxyIndication.setParreAnalysis(findParreAnalysis(dto.getParreAnalysisId()));
        }
        else {
            proxyIndication = find(ProxyIndication.class, dto.getId());
        }
        proxyIndication.copyFrom(dto);

        ProxyCity proxyCity = dto.getProxyCityDTO().isOther() ? null : find(ProxyCity.class, dto.getProxyCityDTO().getId());
        proxyIndication.setProxyCity(proxyCity);
        proxyIndication.setTargetType(find(ProxyTargetType.class, dto.getTargetTypeDTO().getId()));
        saveEntityWithFlush(proxyIndication);
        if (!dto.isNew()) {
            updateProbabilities(proxyIndication);
        }
        dto.setId(proxyIndication.getId());
        return dto;
    }

    public List<ProxyIndicationDTO> getExistingProxyIndications(Long parreAnalysisId) {
        List<ProxyIndication> pis = LotManager.getExistingProxyIndications(parreAnalysisId, entityManager());
        return ProxyIndication.createDTOs(pis);
    }

    public ProxyIndicationDTO associateProxyIndication(Long lotId, Long proxyIndicationId, boolean makeCopy) {
        DirectedThreat lot = find(DirectedThreat.class, lotId);
        ProxyIndication proxyIndication = find(ProxyIndication.class, proxyIndicationId);
        if (makeCopy) {
            proxyIndication = proxyIndication.makeCopy();
            saveEntityWithFlush(proxyIndication);
        }
        lot.setProxyIndication(proxyIndication);
        saveEntityWithFlush(lot);
        return calculateProbability(proxyIndication, lot);
    }

    public List<LotDTO> getAssetSearchResults(AssetSearchDTO dto, Long parreAnalysisId) {
        DirectedThreatAnalysis analysis = getCurrentDirectedThreatAnalysis(parreAnalysisId);
        return analysis != null ? DirectedThreatManager.searchLots(dto, analysis.getId(), entityManager()) :
                Collections.<LotDTO>emptyList();
    }

    public List<LotDTO> getThreatSearchResults(ThreatDTO dto, Long parreAnalysisId) {
        DirectedThreatAnalysis analysis = getCurrentDirectedThreatAnalysis(parreAnalysisId);
        return analysis != null ? DirectedThreatManager.searchLots(dto, analysis.getId(), entityManager()) :
                Collections.<LotDTO>emptyList();
    }

    public List<LabelValueDTO> getAnalysisTypes() {
        List<LabelValueDTO> types = new ArrayList<LabelValueDTO>();
        types.add(new LabelValueDTO("Best Estimate", LotAnalysisType.BEST_ESTIMATE.name()));
        types.add(new LabelValueDTO("Conditional Risk", LotAnalysisType.CONDITIONAL_RISK.name()));
        types.add(new LabelValueDTO("Proxy Indicator", LotAnalysisType.PROXY_INDICATOR.name()));
        return types;
    }

    public LotAnalysisDTO getCurrentAnalysis(Long parreAnalysisId) {
        return getCurrentDirectedThreatAnalysisDTO(parreAnalysisId, new LotAnalysisDTO());
    }

}
