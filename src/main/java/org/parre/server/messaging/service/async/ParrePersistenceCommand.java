/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.server.messaging.service.async;

import org.apache.log4j.Logger;
import org.parre.server.messaging.invocation.PersistenceContextHandler;
import org.parre.server.messaging.invocation.ServiceInvocationContext;
import org.parre.server.persistence.EntityManagerRegistry;
import org.parre.server.util.SystemUtil;
import org.parre.shared.LoginInfo;

import javax.persistence.EntityManager;

/**
 * The Class ParrePersistenceCommand.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 2/21/12
 */
public abstract class ParrePersistenceCommand extends PersistenceContextHandler implements ParreCommand {
    private static transient final Logger log = Logger.getLogger(ParrePersistenceCommand.class);

    private LoginInfo loginInfo;
    private Long parreAnalysisId;

    protected ParrePersistenceCommand(LoginInfo loginInfo, Long parreAnalysisId) {
        super(SystemUtil.getPersistenceUnit());
        this.loginInfo = loginInfo;
        this.parreAnalysisId = parreAnalysisId;
    }

    protected LoginInfo getLoginInfo() {
        return loginInfo;
    }

    protected Long getParreAnalysisId() {
        return parreAnalysisId;
    }

    public final Object call() throws Exception {
        ServiceInvocationContext context = new ServiceInvocationContext(loginInfo);
        ServiceInvocationContext.init(context);
        EntityManagerRegistry managerRegistry = setUpPersistenceContext();
        try {
            Object result = execute();
            commitTransaction(managerRegistry);
            return result;
        }
        catch (Exception exception) {
            rollbackTransaction(managerRegistry);
            log.error("Error during command execution : " + getClass().getName(), exception);
            throw exception;
        }
        finally {
            tearDownPersistenceContext(managerRegistry);
            ServiceInvocationContext.destroy();
        }

    }

    protected abstract Object execute() throws Exception;

    protected EntityManager entityManager() {
        return ServiceInvocationContext.getCurrentInstance().getEntityManager();
    }
}
