/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.server.messaging.service;

import java.math.BigDecimal;
import java.math.RoundingMode;

import org.apache.log4j.Logger;
import org.parre.shared.ProxyIndicationDTO;
import org.parre.shared.SharedCalculationUtil;


/**
 * The Class CalculationUtil.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 2/6/12
 */
public class CalculationUtil {

    private static transient final Logger log = Logger.getLogger(CalculationUtil.class);

    public static BigDecimal calculateProbability(ProxyIndicationDTO info) {
        BigDecimal node2Probability = new BigDecimal(0);
        BigDecimal node3Probability = new BigDecimal(0);
        BigDecimal node4Probability;
        BigDecimal node5Probability;
        Double totalTiers = new Double(255);      // make constant?


        Integer tierNumber = info.getProxyCityDTO().getTierNumber();

        double RANDTierWeights = Math.pow(2, 8 - tierNumber);
        node2Probability = new BigDecimal((RANDTierWeights / info.getProxyCityDTO().getNumberOfCities()) / totalTiers);


        node3Probability = info.getTargetTypeDTO().getProbability();

        node4Probability =
                new BigDecimal(info.getFacilitiesInRegionNum().doubleValue() / info.getTargetTypeInRegionNum().doubleValue());
        node5Probability = new BigDecimal((info.getFacilityCapacity().doubleValue() / info.getRegionalCapacity().doubleValue()));

        BigDecimal node1 = new BigDecimal(info.getAttacksPerYear());
        
        if (log.isDebugEnabled()) {
	        log.debug("node1: " + node1);
	        log.debug("node2Probability: " + node2Probability);
	        log.debug("node3Probability: " + node3Probability);
	        log.debug("node4Probability: " + node4Probability);
	        log.debug("node5Probability: " + node5Probability);
	        log.debug("info.getNode6Value: " + info.getNode6Value());
        }
        return node1.multiply(node2Probability).multiply(node3Probability).multiply(node4Probability).
                multiply(node5Probability).multiply(info.getNode6Value()).setScale(SharedCalculationUtil.DIVISION_PRECISION, RoundingMode.HALF_EVEN);
    }

    public static Integer calculateTierFromPopulation(Integer population) {
        if (population >= 1000000) {
            return 5;
        } else if (population >= 500000) {
            return 6;
        } else if (population >= 250000) {
            return 7;
        } else {
            return 8;
        }
    }

}
