/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.server.messaging.service.async;

import org.parre.server.messaging.ParreServiceFactory;
import org.parre.server.messaging.service.AssetThreatService;
import org.parre.shared.LoginInfo;

/**
 * The Class AssetRemoved.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 2/21/12
 */
public class AssetRemoved extends ParreServiceCommand {

    private Long assetId;

    public AssetRemoved(LoginInfo loginInfo, Long parreAnalysisId, Long assetId) {
        super(loginInfo, parreAnalysisId);
        this.assetId = assetId;
    }

    @Override
    protected Object execute() throws Exception {
        AssetThreatService service = ParreServiceFactory.createAssetThreatService();
        service.assetRemoved(getParreAnalysisId(), assetId);
        return null;
    }

}
