/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.server.messaging.service;

import java.util.*;

import org.apache.log4j.Logger;
import org.parre.client.messaging.ConsequenceService;
import org.parre.server.domain.*;
import org.parre.server.domain.manager.DirectedThreatManager;
import org.parre.server.messaging.service.async.ConsequenceUpdated;
import org.parre.server.messaging.service.async.ParreCommandExecutor;
import org.parre.server.messaging.service.async.ParreServiceCommand;
import org.parre.shared.*;


/**
 * The Class ConsequenceServiceImpl.
 */
public class ConsequenceServiceImpl extends ParreBaseService implements ConsequenceService {
	
	private static transient Logger log = Logger.getLogger(ConsequenceServiceImpl.class);


    private static ConsequenceServiceImpl instance = new ConsequenceServiceImpl();

    public static ConsequenceService getInstance() {
    	return instance;
    }

	public List<ConsequenceDTO> getAssetSearchResults(AssetSearchDTO searchDTO, Long parreAnalysisId) {
        DirectedThreatAnalysis analysis = getCurrentDirectedThreatAnalysis(parreAnalysisId);
        return analysis != null ? DirectedThreatManager.searchConsequences(searchDTO, analysis.getId(), entityManager()) :
                Collections.<ConsequenceDTO>emptyList();
	}

    public List<ConsequenceDTO> getThreatSearchResults(ThreatDTO searchDTO, Long parreAnalysisId) {
        DirectedThreatAnalysis analysis = getCurrentDirectedThreatAnalysis(parreAnalysisId);
        return analysis != null ? DirectedThreatManager.searchConsequences(searchDTO, analysis.getId(), entityManager()) :
                Collections.<ConsequenceDTO>emptyList();
    }

    public ConsequenceDTO saveConsequence(ConsequenceDTO consequenceDTO) {
        DirectedThreat consequence = entityManager().find(DirectedThreat.class, consequenceDTO.getId());
        consequence.copyFrom(consequenceDTO);
        consequence.updateFinancialTotal();
        saveEntityWithFlush(consequence);
        Long parreAnalysisId = consequence.getParreAnalysis().getId();
        consequenceUpdated(parreAnalysisId);
        return consequenceDTO;
    }

    private void consequenceUpdated(Long parreAnalysisId) {
        ParreCommandExecutor.getInstance().submitCommand(new ConsequenceUpdated(getLoginInfo(), parreAnalysisId));
    }


    public DirectedThreatAnalysisDTO getCurrentAnalysis(Long parreAnalysisId) {
        DirectedThreatAnalysisDTO analysisDTO = new DirectedThreatAnalysisDTO();
        return getCurrentDirectedThreatAnalysisDTO(parreAnalysisId, analysisDTO);
    }

    public DirectedThreatAnalysisDTO startAnalysis(DirectedThreatAnalysisDTO dto) {
        return startDirectedThreatAnalysis(dto);
    }


}
