/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.server.messaging.service.logic;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.parre.server.domain.Asset;
import org.parre.server.domain.AssetThreat;
import org.parre.server.domain.AssetThreatLevel;
import org.parre.server.domain.Threat;


/**
 * The Class AssetThreatAnalysisState.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 2/22/12
 */
public class AssetThreatAnalysisState extends AssetThreatLevelState {

    private Integer assetThreatThreshold;
    private Set<Long> inactiveThreatIds = new HashSet<Long>();

    public AssetThreatAnalysisState(Integer assetThreatThreshold, List<AssetThreatLevel> assetThreatLevels, List<Long> inactiveThreatIds) {
        super(assetThreatLevels);
        this.assetThreatThreshold = assetThreatThreshold;
        this.inactiveThreatIds = new HashSet<Long>(inactiveThreatIds);
    }
    public boolean analysisRequired(AssetThreat assetThreat) {
        return analysisRequired(assetThreat.getAsset(), assetThreat.getThreat());
    }

    public boolean analysisRequired(Asset asset, Threat threat) {
    	if (asset.getRemoved() || inactiveThreatIds.contains(threat.getId())) {
    		return false;
    	}
    	
        if (!threat.getThreatCategory().isManMadeHazard()) {
            return true;
        }
        AssetThreatLevel assetThreatLevel = findAssetThreatLevel(asset.getId(), threat.getId());
        Integer levelValue = assetThreatLevel != null ? assetThreatLevel.getLevel() : 0;
        return levelValue >= assetThreatThreshold;
    }
    
    public Integer getAssetThreatThreshold() {
        return assetThreatThreshold;
    }
    
    public void setInactiveThreaIds(Set<Long> inactiveThreatIds) {
    	this.inactiveThreatIds = inactiveThreatIds;
    }

}
