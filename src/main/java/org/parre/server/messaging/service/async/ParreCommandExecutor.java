/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.server.messaging.service.async;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * The Class ParreCommandExecutor.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 2/21/12
 */
public class ParreCommandExecutor {
    private static ParreCommandExecutor instance = new ParreCommandExecutor();
    private ExecutorService executorService = Executors.newSingleThreadExecutor();

    public static ParreCommandExecutor getInstance() {
        return instance;
    }

    public Future<Object> submitCommand(Callable command) {
        return executorService.submit(command);
    }
}
