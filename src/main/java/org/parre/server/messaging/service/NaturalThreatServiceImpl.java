/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.server.messaging.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.parre.client.messaging.NaturalThreatService;
import org.parre.server.domain.Amount;
import org.parre.server.domain.EarthquakeLookupData;
import org.parre.server.domain.EarthquakeMagnitudeProbability;
import org.parre.server.domain.EarthquakeProfile;
import org.parre.server.domain.HurricaneLookupData;
import org.parre.server.domain.HurricaneProfile;
import org.parre.server.domain.IceStormThreatDetails;
import org.parre.server.domain.NaturalThreat;
import org.parre.server.domain.NaturalThreatAnalysis;
import org.parre.server.domain.ParreAnalysis;
import org.parre.server.domain.manager.NaturalThreatManager;
import org.parre.server.domain.util.DomainUtil;
import org.parre.server.util.NaturalThreatCalculator;
import org.parre.server.util.NaturalThreatCalculatorImpl;
import org.parre.shared.AmountDTO;
import org.parre.shared.AssetSearchDTO;
import org.parre.shared.EarthquakeLookupDataDTO;
import org.parre.shared.EarthquakeMagnitudeProbabilityDTO;
import org.parre.shared.EarthquakeProfileDTO;
import org.parre.shared.HurricaneLookupDataDTO;
import org.parre.shared.HurricaneProfileDTO;
import org.parre.shared.IceStormIndexWorkDTO;
import org.parre.shared.NaturalThreatAnalysisDTO;
import org.parre.shared.NaturalThreatCalculationDTO;
import org.parre.shared.NaturalThreatDTO;
import org.parre.shared.ParreAnalysisDTO;
import org.parre.shared.PhysicalResourceDTO;
import org.parre.shared.ThreatDTO;
import org.parre.shared.types.MoneyUnitType;
import org.parre.shared.types.NaturalThreatType;


/**
 * The Class NaturalThreatServiceImpl.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 12/6/11
 */
public class NaturalThreatServiceImpl extends ParreBaseService implements NaturalThreatService {
    private static NaturalThreatServiceImpl instance = new NaturalThreatServiceImpl();
    private final static transient Logger log = Logger.getLogger(NaturalThreatServiceImpl.class);
    
    private NaturalThreatCalculator calculator = new NaturalThreatCalculatorImpl();

    public static NaturalThreatServiceImpl getInstance() {
        return instance;
    }

    private NaturalThreatServiceImpl() {
    }

    public NaturalThreatDTO save(NaturalThreatDTO dto) {
    	//Recalculate...
    	dto = calculateNaturalThreat(dto);
    	
        NaturalThreat naturalThreat = entityManager().find(NaturalThreat.class, dto.getId());
        naturalThreat.copyFrom(dto);
        naturalThreat.setFinancialTotal(new Amount(dto.getFinancialTotalDTO()));
        naturalThreat.setEconomicImpact(new Amount(dto.getEconomicImpactDTO()));
        naturalThreat.setOperationalLoss(new Amount(dto.getOperationalLossDTO().getQuantity(), dto.getOperationalLossDTO().getUnit()));
        naturalThreat.setTotalRisk(new Amount(dto.getTotalRiskDTO().getQuantity(), dto.getTotalRiskDTO().getUnit()));
        if(!naturalThreat.getManuallyEntered()) {
            if(NaturalThreatType.getNaturalThreatType(dto.getThreatDescription()).equals(NaturalThreatType.HURRICANE) && dto.getHurricaneProfileId() != null) {
                naturalThreat.setHurricaneProfile(entityManager().find(HurricaneProfile.class, dto.getHurricaneProfileId()));
            }
            if(NaturalThreatType.getNaturalThreatType(dto.getThreatDescription()).equals(NaturalThreatType.EARTHQUAKE) && dto.getEarthquakeProfileId() != null) {
                naturalThreat.setEarthquakeProfile(entityManager().find(EarthquakeProfile.class, dto.getEarthquakeProfileId()));
            }
        }
        saveEntityWithFlush(naturalThreat);
        
        // Ice Storm details
        if (NaturalThreatType.getNaturalThreatType(dto.getThreatDescription()).equals(NaturalThreatType.ICE_STORM) && dto.getIceStormWorkDTO() != null) {
        	IceStormThreatDetails details = NaturalThreatManager.getIceStormThreatDetails(naturalThreat, entityManager());
        	
        	details.setLatitude(dto.getIceStormWorkDTO().getLatitude());
        	details.setLongitude(dto.getIceStormWorkDTO().getLongitude());
        	details.setStateCode(dto.getIceStormWorkDTO().getStateCode());
        	
        	details.setDailyOperationalLoss(new Amount(dto.getIceStormWorkDTO().getDailyOperationalLoss(), MoneyUnitType.DOLLAR));
        	details.setDaysOfBackupPower(dto.getIceStormWorkDTO().getDaysOfBackupPower());
        	
        	IceStormIndexWorkDTO index3Work = dto.getIceStormWorkDTO().getIceStormIndex(3) != null ? dto.getIceStormWorkDTO().getIceStormIndex(3) : new IceStormIndexWorkDTO();
        	details.setIndex3Fatalities(index3Work.getFatalities());
        	details.setIndex3SeriousInjuries(index3Work.getSeriousInjuries());
        	details.setIndex3RecoveryTime(index3Work.getRecoveryTime());
        	
        	IceStormIndexWorkDTO index4Work = dto.getIceStormWorkDTO().getIceStormIndex(4) != null ? dto.getIceStormWorkDTO().getIceStormIndex(4) : new IceStormIndexWorkDTO();
        	details.setIndex4Fatalities(index4Work.getFatalities());
        	details.setIndex4SeriousInjuries(index4Work.getSeriousInjuries());
        	details.setIndex4RecoveryTime(index4Work.getRecoveryTime());
        	
        	IceStormIndexWorkDTO index5Work = dto.getIceStormWorkDTO().getIceStormIndex(5) != null ? dto.getIceStormWorkDTO().getIceStormIndex(5) : new IceStormIndexWorkDTO();
        	details.setIndex5Fatalities(index5Work.getFatalities());
        	details.setIndex5SeriousInjuries(index5Work.getSeriousInjuries());
        	details.setIndex5RecoveryTime(index5Work.getRecoveryTime());
        	
        	saveEntityWithFlush(details);
        }
        
        return dto;
    }

    public NaturalThreatAnalysisDTO getCurrentAnalysis(Long parreAnalysisId) {
        NaturalThreatAnalysis analysis = findCurrentNaturalThreatAnalysis(parreAnalysisId);
        return analysis != null ? analysis.copyInto(new NaturalThreatAnalysisDTO()) : null;
    }

    public NaturalThreatAnalysisDTO startAnalysis(NaturalThreatAnalysisDTO dto) {
        dto.setName("Natural Threat Analysis");
        NaturalThreatAnalysis analysis = startNaturalThreatAnalysis(dto);
        return analysis.copyInto(new NaturalThreatAnalysisDTO());
    }

    public EarthquakeLookupDataDTO getEarthquakeLookupData(String earthquakeText) {
        List<EarthquakeLookupData> list = NaturalThreatManager.getEarthquakeLookupData(entityManager());
        int i = 0;
        while(list.size() > i) {
            if(list.get(i).getLowerGBoundry().doubleValue() < Double.parseDouble(earthquakeText) && i < list.size() - 1) {
                i++;
            }
            else {
                break;
            }
        }
        return list.get(i).copyInto(new EarthquakeLookupDataDTO());
    }

    @Override
    public List<HurricaneLookupDataDTO> getHurricaneLookupData() {
    	List<HurricaneLookupDataDTO> results = new ArrayList<HurricaneLookupDataDTO>();
    	
        List<HurricaneLookupData> list = NaturalThreatManager.getHurricaneLookupData(entityManager());
        for (HurricaneLookupData data : list) {
        	results.add(data.copyInto(new HurricaneLookupDataDTO()));
        }
        return results;
    }
    
    @Override
	public NaturalThreatCalculationDTO calculateTornadoData(NaturalThreatCalculationDTO dto) {
    	calculator.calculateTornadoRisk(dto);
    	return dto;
	}
    
	@Override
	public NaturalThreatCalculationDTO calculateFloodData(NaturalThreatCalculationDTO dto) {
		calculator.calculateFloodRisk(dto);
		return dto;
	}

	@Override
	public NaturalThreatCalculationDTO calculateEarthquakeData(NaturalThreatCalculationDTO dto) {
		calculator.calculateEarthquakeRisk(dto);
		return dto;
	}

	@Override
	public NaturalThreatCalculationDTO calculateIceStormData(NaturalThreatCalculationDTO dto) {
		calculator.calculateIceStormRisk(dto);
		return dto;
	}

	@Override
	public PhysicalResourceDTO getPhysicalResourceForNT(Long naturalThreatId) {
		return NaturalThreatManager.getPhysicalResourceForNT(naturalThreatId, entityManager());
	}
	
	@Override
	public EarthquakeProfileDTO getEarthquakeProfile(Long earthquakeProfileId) {
		return NaturalThreatManager.findEarthquakeProfileById(earthquakeProfileId, entityManager());
	}

	@Override
	public EarthquakeProfileDTO saveEarthquakeProfile(EarthquakeProfileDTO dto) {
		return NaturalThreatManager.saveEarthquakeProfile(dto, entityManager());
	}

	public List<NaturalThreatDTO> getSearchResults(ThreatDTO dto, Long parreAnalysisId) {
        NaturalThreatAnalysis analysis = findCurrentNaturalThreatAnalysis(parreAnalysisId);
        return analysis != null ? NaturalThreatManager.search(dto, analysis.getId(), parreAnalysisId, entityManager()) :
                Collections.<NaturalThreatDTO>emptyList();
    }

    private NaturalThreatAnalysis findCurrentNaturalThreatAnalysis(Long parreAnalysisId) {
        return NaturalThreatManager.getCurrentAnalysis(parreAnalysisId, entityManager());
    }

	@Override
	public ParreAnalysisDTO getAnalysisEarthquakeProfiles(Long id) {
		ParreAnalysis analysis = entityManager().find(ParreAnalysis.class, id);
		ParreAnalysisDTO dto = new ParreAnalysisDTO();
		
		dto.setDefaultEarthquakeProfileDTO(createEarthquakeProfileDTO(analysis.getDefaultEarthquakeProfile()));
		
		List<EarthquakeProfileDTO> dtos = new ArrayList<EarthquakeProfileDTO>();
		if (analysis.getEarthquakeProfiles() != null) {
			for (EarthquakeProfile profile : analysis.getEarthquakeProfiles()) {
				dtos.add(createEarthquakeProfileDTO(profile));
			}
		}
		dto.setEarthquakeProfileDTOs(dtos);
		
		return dto;
	}

    @Override
    public List<String> getTornadoCountyData(String stateCodeOverride) {
        return NaturalThreatManager.findCountyDataByState(stateCodeOverride, entityManager());
    }

    private EarthquakeProfileDTO createEarthquakeProfileDTO(EarthquakeProfile profile) {
		if (profile == null) { return null; }
		EarthquakeProfileDTO dto = new EarthquakeProfileDTO();
		DomainUtil.copyProperties(dto, profile);
		Set<EarthquakeMagnitudeProbabilityDTO>probDTOs = new HashSet<EarthquakeMagnitudeProbabilityDTO>();
		for (EarthquakeMagnitudeProbability probability : profile.getMagnitudeProbabilities()) {
			EarthquakeMagnitudeProbabilityDTO probDTO = new EarthquakeMagnitudeProbabilityDTO();
			DomainUtil.copyProperties(probDTO, probability);
			probDTOs.add(probDTO);
		}
		dto.setMagnitudeProbabilityDTOs(probDTOs);
		return dto;
	}

	@Override
	public NaturalThreatCalculationDTO calculateHurricaneData(NaturalThreatCalculationDTO dto) {
		calculator.calculateHurricaneRisk(dto);
		return dto;
	}

	@Override
	public HurricaneProfileDTO getHurricaneProfile(Long hurricaneProfileId) {
		return NaturalThreatManager.findHurricaneProfileById(hurricaneProfileId, entityManager());
	}

	@Override
	public HurricaneProfileDTO saveHurricaneProfile(HurricaneProfileDTO dto) {
		return NaturalThreatManager.saveHurricaneProfile(dto, entityManager());
	}

	@Override
	public ParreAnalysisDTO getAnalysisHurricaneProfiles(Long id) {
		ParreAnalysis analysis = entityManager().find(ParreAnalysis.class, id);
		ParreAnalysisDTO dto = new ParreAnalysisDTO();
		
		dto.setDefaultHurricaneProfileDTO(NaturalThreatManager.createHurricaneProfileDTO(analysis.getDefaultHurricaneProfile()));
		
		List<HurricaneProfileDTO> dtos = new ArrayList<HurricaneProfileDTO>();
		if (analysis.getHurricaneProfiles() != null) {
			for (HurricaneProfile profile : analysis.getHurricaneProfiles()) {
				dtos.add(NaturalThreatManager.createHurricaneProfileDTO(profile));
			}
		}
		dto.setHurricaneProfileDTOs(dtos);
		
		return dto;
	}

    public void deleteEarthquakeProfile(Long earthquakeProfileId) {
        EarthquakeProfile earthquakeProfile = find(EarthquakeProfile.class, earthquakeProfileId);
        for(EarthquakeMagnitudeProbability earthquakeMagnitudeProbability : earthquakeProfile.getMagnitudeProbabilities()) {
            entityManager().remove(earthquakeMagnitudeProbability);
        }
        earthquakeProfile.setMagnitudeProbabilities(null);
        entityManager().remove(earthquakeProfile);
        entityManager().flush();
    }

    public void deleteHurricaneProfile(Long hurricaneProfileId) {
        entityManager().remove(find(HurricaneProfile.class, hurricaneProfileId));
        entityManager().flush();
    }
    
    public NaturalThreatDTO calculateNaturalThreat(NaturalThreatDTO dto) {
    	NaturalThreat naturalThreat = entityManager().find(NaturalThreat.class, dto.getId());
    	
    	if (dto.getManuallyEntered()) {
    		BigDecimal newFinancialTotal = dto.getFinancialImpactDTO() == null ? BigDecimal.ZERO : dto.getFinancialImpactDTO().getQuantity();
            if(naturalThreat.getParreAnalysis().getUseStatValues()) {
            	Integer fatalities = dto.getFatalities() == null ? new Integer(0) : dto.getFatalities();
            	Integer seriousInjuries = dto.getSeriousInjuries() == null ? new Integer(0) : dto.getSeriousInjuries();
                newFinancialTotal = newFinancialTotal.add((new BigDecimal(fatalities))
                        .multiply(naturalThreat.getParreAnalysis().getStatValues().getFatalityQuantity()
                                .multiply(new BigDecimal(1000000))));
                newFinancialTotal = newFinancialTotal.add((new BigDecimal(seriousInjuries))
                        .multiply(naturalThreat.getParreAnalysis().getStatValues().getSeriousInjuryQuantity()
                                .multiply(new BigDecimal(1000000))));
            }
            dto.setFinancialTotalDTO(new AmountDTO(newFinancialTotal, dto.getFinancialImpactDTO().getUnit()));
            BigDecimal totalRisk = newFinancialTotal
                    .multiply(dto.getVulnerability())
                    .multiply(dto.getLot());
            dto.setTotalRiskDTO(new AmountDTO(totalRisk, dto.getFinancialImpactDTO().getUnit()));
    		return dto;
    	}
    	
    	NaturalThreatCalculationDTO calcDTO = new NaturalThreatCalculationDTO();
    	
    	dto.setFinancialImpactDTO(AmountDTO.createZero());
    	dto.setFinancialTotalDTO(AmountDTO.createZero());
    	dto.setLot(BigDecimal.ZERO);
    	dto.setVulnerability(BigDecimal.ZERO);
    	dto.setTotalRiskDTO(AmountDTO.createZero());
    	
    	calcDTO.setAnalysis(dto);
    	PhysicalResourceDTO assetDTO = getPhysicalResourceForNT(dto.getId());
    	calcDTO.setAsset(assetDTO);
    	
    	NaturalThreatType currentType = NaturalThreatType.getNaturalThreatType(dto.getThreatDescription());
    	if(currentType.equals(NaturalThreatType.EARTHQUAKE)) {
    		if (dto.getEarthquakeProfileId() != null) {
    			// Use the one specified
        		calcDTO.setEarthquakeProfile(getEarthquakeProfile(dto.getEarthquakeProfileId()));
        	} else if (naturalThreat.getParreAnalysis().getDefaultEarthquakeProfile() != null) {
        		// Take the default for this parre analyis
        		calcDTO.setEarthquakeProfile(getEarthquakeProfile(naturalThreat.getParreAnalysis().getDefaultEarthquakeProfile().getId()));
        	} else if (naturalThreat.getParreAnalysis().getEarthquakeProfiles() != null && naturalThreat.getParreAnalysis().getEarthquakeProfiles().size() > 0) {
        		// Take the first available...
        		for (EarthquakeProfile earthquakeProfile : naturalThreat.getParreAnalysis().getEarthquakeProfiles()) {
        			calcDTO.setEarthquakeProfile(getEarthquakeProfile(earthquakeProfile.getId()));
        			break;
        		}
        	}
    		
    		calcDTO = calculateEarthquakeData(calcDTO);
    	} else if (currentType.equals(NaturalThreatType.HURRICANE)) {
    		if (dto.getHurricaneProfileId() != null) {
    			// Use the one specified
        		calcDTO.setHurricaneProfile(getHurricaneProfile(dto.getHurricaneProfileId()));
        	} else if (naturalThreat.getParreAnalysis().getDefaultHurricaneProfile() != null) {
        		// Take the default for this parre analyis
        		calcDTO.setHurricaneProfile(getHurricaneProfile(naturalThreat.getParreAnalysis().getDefaultHurricaneProfile().getId()));
        	} else if (naturalThreat.getParreAnalysis().getHurricaneProfiles() != null && naturalThreat.getParreAnalysis().getHurricaneProfiles().size() > 0) {
        		// Take the first available...
        		for (HurricaneProfile hurricaneProfile : naturalThreat.getParreAnalysis().getHurricaneProfiles()) {
        			calcDTO.setHurricaneProfile(getHurricaneProfile(hurricaneProfile.getId()));
        			break;
        		}
        	}
    	
    		// Set Design Speed
    		if ((dto.getDesignSpeed() == null || dto.getDesignSpeed().intValue() == 0) && calcDTO.getHurricaneProfile() != null) {
    			dto.setDesignSpeed(calcDTO.getHurricaneProfile().getDefaultDesignSpeed());
    		}
    		
    		calcDTO = calculateHurricaneData(calcDTO);
    	} else if (currentType.equals(NaturalThreatType.ICE_STORM)) {
    		calcDTO = calculateIceStormData(calcDTO);
    	} else if (currentType.equals(NaturalThreatType.TORNADO)) {
    		calcDTO = calculateTornadoData(calcDTO);
    	} else if (currentType.equals(NaturalThreatType.FLOOD)) {
    		calcDTO = calculateFloodData(calcDTO);
    	} 
    	
    	if (calcDTO.getCalculationSuccess() == false) {
    		log.warn("Could not calculate natural threat id: " + dto.getId());
    		throw new RuntimeException("Could not calculate natural threat...");
    	}
    	return calcDTO.getAnalysis();
    }
    
    public Boolean recalculateAllNaturalThreats(Long parreAnalysisId) {
    	if (log.isInfoEnabled()) { log.info("Recalculating all Natural Threats for parreAnalysisId: " + parreAnalysisId); }
    	Boolean success = false;
    	
    	try {
    		NaturalThreatAnalysis analysis = NaturalThreatManager.getCurrentAnalysis(parreAnalysisId, entityManager());
    		
    		if (analysis != null) {
	    		List<NaturalThreatDTO> dtos = NaturalThreatManager.search(new AssetSearchDTO(), analysis.getId(), parreAnalysisId, entityManager());
	    		for(NaturalThreatDTO dto : dtos) {
	    			save(dto);
	    		}
    		} else {
    			if (log.isInfoEnabled()) { log.info("No natural threat analysis to recalcuate for parreAnalysisId: " + parreAnalysisId); }
    		}
    		
	    	success = true;
    	} catch (Exception ex) {
    		log.error("Could not recalculate all naturals...", ex);
    		success = false;
    	}
    	
    	return success;
    }
}
