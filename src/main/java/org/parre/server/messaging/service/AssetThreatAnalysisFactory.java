/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.server.messaging.service;

import org.parre.server.domain.Asset;
import org.parre.server.domain.AssetThreat;
import org.parre.server.domain.Threat;

/**
 * A factory for creating AssetThreatAnalysis objects.
 *
 * @param <T> the generic type
 * @author Swarn S. Dhaliwal
 * @version 1.0 1/10/12
 */
public interface AssetThreatAnalysisFactory<T> {
    public T create(AssetThreat assetThreat);
    public T create();
}
