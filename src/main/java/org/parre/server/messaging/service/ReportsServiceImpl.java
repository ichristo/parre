/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.server.messaging.service;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.parre.client.messaging.ReportsService;
import org.parre.server.domain.ParreAnalysis;
import org.parre.server.domain.ParreAnalysisReport;
import org.parre.server.domain.ParreAnalysisReportSection;
import org.parre.server.domain.ReportAssessmentTask;
import org.parre.server.domain.ReportInvestmentOption;
import org.parre.server.domain.ReportSectionLookup;
import org.parre.server.domain.RiskResilienceAnalysis;
import org.parre.server.domain.manager.ReportsManager;
import org.parre.server.domain.manager.RiskResilienceManager;
import org.parre.server.domain.util.DomainUtil;
import org.parre.shared.ParreAnalysisReportSectionDTO;
import org.parre.shared.ReportAssessmentTaskDTO;
import org.parre.shared.ReportInvestmentOptionDTO;
import org.parre.shared.RiskResilienceDTO;
import org.parre.shared.RptAssessmentDTO;
import org.parre.shared.RptAssetDTO;
import org.parre.shared.RptConsequenceDTO;
import org.parre.shared.RptCountermeasureDTO;
import org.parre.shared.RptFatalitiesDTO;
import org.parre.shared.RptInjuriesDTO;
import org.parre.shared.RptMainDTO;
import org.parre.shared.RptNaturalThreatsDTO;
import org.parre.shared.RptRankingsDTO;
import org.parre.shared.RptResilienceDTO;
import org.parre.shared.RptRiskDTO;
import org.parre.shared.RptThreatDTO;
import org.parre.shared.RptThreatLikelihoodDTO;
import org.parre.shared.RptTop20Risks;
import org.parre.shared.RptVulnerabilityDTO;
import org.parre.shared.UnitPriceDTO;
import org.parre.shared.exception.InvalidParreAnalysisIdException;

/**
 * The server side implementation of the RPC service.
 */
public class ReportsServiceImpl extends ParreBaseService implements ReportsService {
    private static transient Logger log = Logger.getLogger(ReportsServiceImpl.class);

    private static ReportsServiceImpl instance = new ReportsServiceImpl();

    public static ReportsService getInstance() {
        return instance;
    }
    
    @Override
	public List<RptMainDTO> getReportData(Long parreAnalysisID) throws InvalidParreAnalysisIdException{		
		log.debug("In Reports Service Impl class");
    	RptMainDTO rptData = new RptMainDTO();
		
		//Validate if the User is having permission to View the Report		
		Long paId = getCurrentUser().getCurrentAnalysis().getId();
		if (!paId.equals(parreAnalysisID)) {
			throw new InvalidParreAnalysisIdException(""+parreAnalysisID);
		} 	
		
		//If the Report is for Options.. we have to use the Baseline ParreAnalysisId
		parreAnalysisID = ReportsManager.getParreAnalysisBaselineId(parreAnalysisID, entityManager());
		
		//rptData.setName("Loudoun Water Facility");
		Date currDate = new Date(System.currentTimeMillis());
		rptData.setCreatedTime(currDate);
		
		ParreAnalysis parreAnalysis = entityManager().find(ParreAnalysis.class, parreAnalysisID);
		rptData.setName(parreAnalysis.getName());
		rptData.setDescription(parreAnalysis.getDescription());
		
		//Set All the Report Static Content into RptMainDTO
		ReportsManager.setReportStaticContent(rptData, parreAnalysisID, entityManager());
		
		//Set All the User Input Section into the RptMainDTO
		ReportsManager.setReportUserInputData(rptData, parreAnalysisID, entityManager());
		
		//Set the Data for FRI & ORI
		ReportsManager.setIndexContent(rptData, parreAnalysisID, entityManager());
		
		//Get the Data for User Input Table - Assessment Tasks
		List<ReportAssessmentTaskDTO> tasks = ReportsManager.getReportAssessmentTasks(parreAnalysisID, entityManager());
		rptData.setTasks(tasks);
		
		//Get the Data for User Input Table - Investment Options
		List<ReportInvestmentOptionDTO> investOptions =  ReportsManager.getRptInvOptions(parreAnalysisID, entityManager());
		rptData.setInvestOptions(investOptions);
		
    	//Get the Data for Critical Assets Table in the Report
    	List<RptAssetDTO> criticalAssets = ReportsManager.getCriticalAssets(parreAnalysisID, entityManager());	
    	rptData.setAssets(criticalAssets);
    	
    	//Get the Data for Most Probable Threats Table in the Report
    	List<RptThreatDTO> probableThreats = ReportsManager.getMostProbableThreats(parreAnalysisID, entityManager());	
    	rptData.setThreats(probableThreats);
    	
    	//Get the Data for Existing Countermeasures Table in the Report
    	List<RptCountermeasureDTO> countermeasures = ReportsManager.getExistingCountermeasures(parreAnalysisID, entityManager());	
    	rptData.setCountermeasures(countermeasures);
    	
    	//Get the Data for Financial Consequences Table in the Report
    	List<RptConsequenceDTO> financialConsequences = ReportsManager.getFinancialConsequences(parreAnalysisID, entityManager());	
    	rptData.setConsequences(financialConsequences);
    	
    	//Get the Data for Fatalities Table in the Report
    	List<RptFatalitiesDTO> fatalities = ReportsManager.getFatalities(parreAnalysisID, entityManager());	
    	rptData.setFatalities(fatalities);
    	
    	//Get the Data for Serious Injuries Table in the Report
    	List<RptInjuriesDTO> injuries = ReportsManager.getSeriousInjuries(parreAnalysisID, entityManager());	
    	rptData.setInjuries(injuries);
    	
    	//Get the Data for Vulnerabilities Table in the Report
    	List<RptVulnerabilityDTO> vulnerabilities = ReportsManager.getVulnerabilities(parreAnalysisID, entityManager());	
    	rptData.setVulnerabilities(vulnerabilities);
    	
    	//Get the Data for Threat Likelihood Table in the Report
    	List<RptThreatLikelihoodDTO> likelihoods = ReportsManager.getThreatLikelihoods(parreAnalysisID, entityManager());	
    	rptData.setThreatLikehoods(likelihoods);
    	
    	//Get the Data for Top 50 Risks Table in the Report
    	List<RptRiskDTO> risks = ReportsManager.getRisks(parreAnalysisID, entityManager());	
    	rptData.setRisks(risks);
    	
    	//Get the Data for Summary Ranking Table in the Report
    	List<RptRankingsDTO> rankings = ReportsManager.getSummaryRankings(parreAnalysisID, entityManager());	
    	rptData.setRankings(rankings);
    	
    	//Get the Data for Investment Assessment Results Table in the Report
    	List<RptAssessmentDTO> assessments = ReportsManager.getInvAssessment(parreAnalysisID, entityManager());	
    	rptData.setAssessments(assessments);
    	
    	//Get the Data for Bottom 50 Least Financially Resilient Asset-Threat Pairs Table in the Report
    	List<RptResilienceDTO> resilience = ReportsManager.getResilience(parreAnalysisID, entityManager());	
    	rptData.setResilience(resilience);
    	
    	//Get the Data for Top 20 Risks Table in the Report
    	List<RptTop20Risks> topRisks = ReportsManager.getTop20Risks(parreAnalysisID, entityManager());
    	rptData.setTopRisks(topRisks);
    	
    	//Get the Data for Top 10 Risks to Natural Threats Table in the Report
    	List<RptNaturalThreatsDTO> naturalThreats = ReportsManager.getNaturalThreats(parreAnalysisID, entityManager());
    	rptData.setNaturalThreats(naturalThreats);
    	
    	return Arrays.asList(rptData); 
	}
   
    public List<RiskResilienceDTO> getRptRiskResilienceDTOs(Long parreAnalysisId) {
    	parreAnalysisId = ReportsManager.getParreAnalysisBaselineId(parreAnalysisId, entityManager());
        RiskResilienceAnalysis analysis = RiskResilienceManager.getCurrentAnalysis(parreAnalysisId, entityManager());
        UnitPriceDTO unitPriceDTO = analysis.getUnitPriceDTO();
        List<RiskResilienceDTO> dtos = ReportsManager.getRRDTOsForResilienceRpt(parreAnalysisId, entityManager());
        return updateRiskResilienceDTOs(unitPriceDTO, dtos, parreAnalysisId);
    }
    
    public List<RiskResilienceDTO> getRptTop20RisksDTOs(Long parreAnalysisId) {
    	parreAnalysisId = ReportsManager.getParreAnalysisBaselineId(parreAnalysisId, entityManager());
        RiskResilienceAnalysis analysis = RiskResilienceManager.getCurrentAnalysis(parreAnalysisId, entityManager());        
        UnitPriceDTO unitPriceDTO = analysis.getUnitPriceDTO();
        List<RiskResilienceDTO> dtos = ReportsManager.getRRDTOsForTop20RisksRpt(parreAnalysisId, entityManager());
        return updateRiskResilienceDTOs(unitPriceDTO, dtos, parreAnalysisId);
    }
    
    public List<RiskResilienceDTO> getRptTop10NatThreatDTOs(Long parreAnalysisId) {
    	parreAnalysisId = ReportsManager.getParreAnalysisBaselineId(parreAnalysisId, entityManager());
        RiskResilienceAnalysis analysis = RiskResilienceManager.getCurrentAnalysis(parreAnalysisId, entityManager());           
        UnitPriceDTO unitPriceDTO = analysis.getUnitPriceDTO();
        List<RiskResilienceDTO> dtos = ReportsManager.getRRDTOsForTop10NatThreatsRpt(parreAnalysisId, entityManager());
        return updateRiskResilienceDTOs(unitPriceDTO, dtos, parreAnalysisId);
    }

    @Override
	public RptMainDTO getReportInputData(Long parreAnalysisId) {
		return null;
	}
    
    
	@Override
	public ParreAnalysisReportSectionDTO getReportSection(Long parreAnalysisId, Long reportSectionLookupId) {
		parreAnalysisId = ReportsManager.getParreAnalysisBaselineId(parreAnalysisId, entityManager());
		ParreAnalysisReportSection section = ReportsManager.getReportSection(parreAnalysisId, reportSectionLookupId, entityManager());
		ParreAnalysisReportSectionDTO dto = new ParreAnalysisReportSectionDTO();
		
		DomainUtil.copyProperties(dto, section);
		if (section.getParreAnalysisReport() != null) {
			dto.setReportId(section.getParreAnalysisReport().getId());
		}
		if (dto.getOmitted() == null) { dto.setOmitted(false); }
		dto.setReportSectionLookupId(reportSectionLookupId);
		
		return dto;
	}

	@Override
	public List<RptAssetDTO> getReportDisplayTableAssets(Long parreAnalysisId) {
		parreAnalysisId = ReportsManager.getParreAnalysisBaselineId(parreAnalysisId, entityManager());
		List<RptAssetDTO> criticalAssets = ReportsManager.getCriticalAssets(parreAnalysisId, entityManager());		
		return criticalAssets;
	}
	
	@Override
	public List<RptConsequenceDTO> getReportDisplayTableConsequences(Long parreAnalysisId) {
		parreAnalysisId = ReportsManager.getParreAnalysisBaselineId(parreAnalysisId, entityManager());
		List<RptConsequenceDTO> consequences = ReportsManager.getFinancialConsequences(parreAnalysisId, entityManager());		
		return consequences;
	}
	
	@Override
	public List<RptVulnerabilityDTO> getReportDisplayTableVulnerability(Long parreAnalysisId) {
		parreAnalysisId = ReportsManager.getParreAnalysisBaselineId(parreAnalysisId, entityManager());
		List<RptVulnerabilityDTO> vulnerabilities = ReportsManager.getVulnerabilities(parreAnalysisId, entityManager());		
		return vulnerabilities;
	}
	
	@Override
	public List<RptThreatLikelihoodDTO> getReportDisplayTableThreatLikelihood(Long parreAnalysisId) {
		parreAnalysisId = ReportsManager.getParreAnalysisBaselineId(parreAnalysisId, entityManager());
		List<RptThreatLikelihoodDTO> threatLikelihoods = ReportsManager.getThreatLikelihoods(parreAnalysisId, entityManager());		
		return threatLikelihoods;
	}
	
	@Override
	public List<RptRankingsDTO> getReportDisplayTableRiskManagement(Long parreAnalysisId) {
		parreAnalysisId = ReportsManager.getParreAnalysisBaselineId(parreAnalysisId, entityManager());
		List<RptRankingsDTO> riskManagement = ReportsManager.getSummaryRankings(parreAnalysisId, entityManager());	
		return riskManagement;
	}
	
	@Override
	public List<RptAssessmentDTO> getReportDisplayTableRiskReduction(Long parreAnalysisId) {
		parreAnalysisId = ReportsManager.getParreAnalysisBaselineId(parreAnalysisId, entityManager());
		List<RptAssessmentDTO> riskReduction = ReportsManager.getInvAssessment(parreAnalysisId, entityManager());		
		return riskReduction;
	}
	
	@Override
	public List<ReportAssessmentTaskDTO> getReportAssessmentTasks(Long parreAnalysisId) {
		parreAnalysisId = ReportsManager.getParreAnalysisBaselineId(parreAnalysisId, entityManager());
		return ReportsManager.getReportAssessmentTasks(parreAnalysisId, entityManager());
	}
	
	@Override
	public List<ReportInvestmentOptionDTO> getReportInvestmentOptions(Long parreAnalysisId) {
		parreAnalysisId = ReportsManager.getParreAnalysisBaselineId(parreAnalysisId, entityManager());
		return ReportsManager.getReportInvestmentOptions(parreAnalysisId, entityManager());
	}
	

	@Override
	public ParreAnalysisReportSectionDTO saveReportSection(Long parreAnalysisId, ParreAnalysisReportSectionDTO dto) {
		parreAnalysisId = ReportsManager.getParreAnalysisBaselineId(parreAnalysisId, entityManager());
		
		ParreAnalysisReportSection section = ReportsManager.getReportSection(parreAnalysisId, dto.getReportSectionLookupId(), entityManager());
		
		if (section.getReportSectionLookup() == null) {
			ReportSectionLookup reportSectionLookup = entityManager().find(ReportSectionLookup.class, dto.getReportSectionLookupId());
			section.setReportSectionLookup(reportSectionLookup);
		}
		
		if (section.getParreAnalysisReport() == null) {
			ParreAnalysisReport parreAnalysisReport = ReportsManager.getAnalysisReport(parreAnalysisId, entityManager());
			section.setParreAnalysisReport(parreAnalysisReport);
		}
		
		DomainUtil.copyProperties(section, dto);
		entityManager().persist(section);
		entityManager().flush();
		
		ParreAnalysisReportSectionDTO afterSave = getReportSection(parreAnalysisId, dto.getReportSectionLookupId());
		return afterSave;
	}

	
	@Override
	public List<ReportAssessmentTaskDTO> saveAddTaskPopup(Long parreAnalysisId, ReportAssessmentTaskDTO taskDTO) {
		
		parreAnalysisId = ReportsManager.getParreAnalysisBaselineId(parreAnalysisId, entityManager());
		
		ReportAssessmentTask task = new ReportAssessmentTask();
			if(taskDTO.getTaskId() == null || taskDTO.getTaskId().equals(null)){
				task =  ReportsManager.getAssessmentTask(parreAnalysisId, entityManager());
					if(task.getId()!= null){
						task = new ReportAssessmentTask();
						ParreAnalysisReport parreAnalysisReport = ReportsManager.getAnalysisReport(parreAnalysisId, entityManager());
						task.setParreAnalysisReport(parreAnalysisReport);
						task.setName("Assessment Task " + parreAnalysisReport.getName());			
					}
				DomainUtil.copyProperties(task, taskDTO);
				entityManager().persist(task);
				entityManager().flush();
			} else {
				taskDTO.setId(taskDTO.getTaskId());
				task = ReportsManager.getReportTask(taskDTO.getTaskId(), entityManager());
				DomainUtil.copyProperties(task, taskDTO);
				entityManager().merge(task);
				entityManager().flush();
			}
		List<ReportAssessmentTaskDTO> tasks = getReportAssessmentTasks(parreAnalysisId);
		return tasks;
	}

	
	@Override
	public List<ReportInvestmentOptionDTO> saveEditOptionPopup(Long parreAnalysisId, ReportInvestmentOptionDTO optionDTO) {
		parreAnalysisId = ReportsManager.getParreAnalysisBaselineId(parreAnalysisId, entityManager());
		ReportInvestmentOption invOption = new ReportInvestmentOption();
		optionDTO.setId(optionDTO.getOptionId());
		invOption = ReportsManager.getReportOption(optionDTO.getOptionId(), entityManager());
		DomainUtil.copyProperties(invOption, optionDTO);
		entityManager().merge(invOption);
		entityManager().flush();
		List<ReportInvestmentOptionDTO> invOptions = getReportInvestmentOptions(parreAnalysisId);
		return invOptions;
	}


}
