/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.server.messaging.service;

import org.apache.log4j.Logger;
import org.parre.client.messaging.NaturalThreatService;
import org.parre.client.messaging.ServiceFactory;
import org.parre.server.domain.*;
import org.parre.server.domain.manager.*;
import org.parre.server.messaging.service.logic.AssetThreatAnalysisState;
import org.parre.shared.types.ThreatCategoryType;

import javax.persistence.EntityManager;
import java.util.*;

/**
 * The Class AssetThreatServiceImpl.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 2/24/12
 */
public class AssetThreatServiceImpl extends ParreBaseService implements AssetThreatService {
    private static transient final Logger log = Logger.getLogger(AssetThreatServiceImpl.class);

    private static AssetThreatService instance = new AssetThreatServiceImpl();

    public static AssetThreatService getInstance() {
        return instance;
    }

    public void assetCountChanged(Long parreAnalysisId) {
        List<AssetThreat> assetThreats = ParreManager.getAllAssetThreats(parreAnalysisId, entityManager());
        if (assetThreats.isEmpty()) {
            log.debug("No asset threat yet, no need to update");
            return;
        }
        ParreAnalysis parreAnalysis = findParreAnalysis(parreAnalysisId);
        List<Asset> assets = getAssetsForAnalysis(parreAnalysisId);
        if (log.isDebugEnabled()) { log.debug("Number of analysis assets : " + assets.size()); }
        addAssetThreats(parreAnalysis, assets, assetThreats);
        removeAssetThreats(assets, assetThreats);
    }

    private void removeAssetThreats(List<Asset> assets, List<AssetThreat> assetThreats) {
        for (AssetThreat assetThreat : assetThreats) {
            boolean assetFound = false;
            for (Asset asset : assets) {
                assetFound = assetThreat.isFor(asset);
                if (assetFound) {
                    break;
                }
            }
            if (!assetFound) {
                assetThreat.setRemoved(Boolean.TRUE);
                saveEntity(assetThreat);
            }
        }
    }

    private void addAssetThreats(ParreAnalysis parreAnalysis, List<Asset> assets, List<AssetThreat> assetThreats) {
        AssetThreatAnalysisState analysisState = createAssetThreatAnalysisState(parreAnalysis.getId());
        List<Asset> toAddAssets = new ArrayList<Asset>();
        for (Asset asset : assets) {
            boolean assetFound = false;
            for (AssetThreat assetThreat : assetThreats) {
                if (assetThreat.isFor(asset)) {
                    if (!assetFound) {
                        assetFound = true;
                    }
                    if (assetThreat.getRemoved() && analysisState.analysisRequired(assetThreat)) {
                        assetThreat.setRemoved(Boolean.FALSE);
                        saveEntity(assetThreat);
                    }
                }
            }
            if (!assetFound) {
                toAddAssets.add(asset);
            }
        }
        if (toAddAssets.isEmpty()) {
            return;
        }
        List<Threat> activeThreats = ThreatManager.getActiveThreats(parreAnalysis.getId(), entityManager());
        addAssetThreatAnalyses(parreAnalysis, toAddAssets, activeThreats);
    }

    private void addAssetThreatAnalyses(ParreAnalysis parreAnalysis, List<Asset> toAddAssets, List<Threat> activeThreats) {
        List<AssetThreat> toAddAssetThreats = initAssetThreats(parreAnalysis, toAddAssets, activeThreats);
        updateDirectedThreats(parreAnalysis, toAddAssetThreats);
        updateRiskResiliences(parreAnalysis, toAddAssetThreats);
        updateDps(parreAnalysis, toAddAssetThreats);
        updateNaturalThreats(parreAnalysis, toAddAssetThreats);
    }

    private void updateNaturalThreats(ParreAnalysis parreAnalysis, List<AssetThreat> assetThreats) {
        NaturalThreatAnalysis currentAnalysis = getCurrentNaturalThreatAnalysis(parreAnalysis.getId());
        updateAnalysis(parreAnalysis, currentAnalysis, assetThreats, ThreatCategoryType.NATURAL_HAZARD, NaturalThreatFactory.getInstance());
    }

    private void updateDps(ParreAnalysis parreAnalysis, List<AssetThreat> assetThreats) {
        DpAnalysis currentAnalysis = getCurrentDpAnalysis(parreAnalysis.getId());
        updateAnalysis(parreAnalysis, currentAnalysis, assetThreats, ThreatCategoryType.DEPENDENCY_PROXIMITY, DpFactory.getInstance());
    }

    private void updateRiskResiliences(ParreAnalysis parreAnalysis, List<AssetThreat> assetThreats) {
        RiskResilienceAnalysis currentAnalysis = getCurrentRiskResilienceAnalysis(parreAnalysis);
        updateAnalysis(parreAnalysis, currentAnalysis, assetThreats, ThreatCategoryType.NONE, RiskResilienceFactory.getInstance());
    }


    private void updateDirectedThreats(ParreAnalysis parreAnalysis, List<AssetThreat> assetThreatList) {
        DirectedThreatAnalysis analysis = getCurrentDirectedThreatAnalysis(parreAnalysis.getId());
        updateAnalysis(parreAnalysis, analysis, assetThreatList, ThreatCategoryType.MAN_MADE_HAZARD, DirectedThreatFactory.getInstance());
    }

    private void updateAnalysis(ParreAnalysis parreAnalysis, Analysis analysis, List<AssetThreat> toAddAssetThreats,
                                ThreatCategoryType threatCategory, AssetThreatAnalysisFactory<AssetThreatEntity> factory) {
        if (analysis == null) {
            return;
        }

        Set<? extends AssetThreatEntity> addedAssetThreatAnalyses =
                initAssetThreatAnalyses(parreAnalysis, threatCategory, toAddAssetThreats, factory);
        for (AssetThreatEntity consequence : addedAssetThreatAnalyses) {
            analysis.addAssetThreat(consequence);
        }
        saveEntities(addedAssetThreatAnalyses);
        /*if (!addedAssetThreatAnalyses.isEmpty()) {
            saveEntity(analysis);
        }*/
    }

    public void threatAdded(Long parreAnalysisId, Long addedThreatId) {
        List<AssetThreat> allAssetThreats = ParreManager.getAllAssetThreats(parreAnalysisId, entityManager());
        if (allAssetThreats.isEmpty()) {
            log.debug("No Asset Threats yet, no need to update");
            return;
        }
        ParreAnalysis parreAnalysis = findParreAnalysis(parreAnalysisId);
        Threat addedThreat = find(Threat.class, addedThreatId);
        AssetThreatAnalysisState analysisState = createAssetThreatAnalysisState(parreAnalysisId);
        List<Asset> assets = getAssetsForAnalysis(parreAnalysisId);
        boolean threatFound = false;
        for (AssetThreat assetThreat : allAssetThreats) {
            if (!assetThreat.isFor(addedThreat)) {
                continue;
            }
            threatFound = true;
            if (assetThreat.getRemoved()) {
                log.debug("Get removed");
                if (assetExists(assets, assetThreat) && isRequiredAnalysis(analysisState, assetThreat)) {
                    log.debug("Asset exists and is required");
                    assetThreat.setRemoved(Boolean.FALSE);
                    saveEntity(assetThreat);
                }
            }
        }
        if (!threatFound) {
            addAssetThreatAnalyses(parreAnalysis, assets, Collections.singletonList(addedThreat));
        }
    }

    private boolean isRequiredAnalysis(AssetThreatAnalysisState analysisState, AssetThreat assetThreat) {
        return !assetThreat.getThreat().getThreatCategory().isManMadeHazard() ||
                analysisState.analysisRequired(assetThreat);
    }

    public void threatRemoved(Long parreAnalysisId, Long removedThreatId) {
        List<AssetThreat> allAssetThreats = ParreManager.getAllAssetThreats(parreAnalysisId, entityManager());
        if (allAssetThreats.isEmpty()) {
            log.debug("No Asset Threats yet, no need to update");
            return;
        }
        Threat removedThreat = find(Threat.class, removedThreatId);
        for (AssetThreat assetThreat : allAssetThreats) {
            if (assetThreat.isFor(removedThreat)) {
                assetThreat.setRemoved(Boolean.TRUE);
                saveEntity(assetThreat);
            }
        }
    }

    public void statValuesChanged(Long parreAnalysisId) {
        updateFinancialTotal(ParreManager.getAssetThreatAnalyses(parreAnalysisId, entityManager()));
        
        log.info("Recalculating all Natural Threats for parreAnalysisId: " + parreAnalysisId);
        Boolean success = NaturalThreatServiceImpl.getInstance().recalculateAllNaturalThreats(parreAnalysisId);
        log.info("Completed recalculation, success: " + success);
    }

    public void consequenceUpdated(Long parreAnalysisId) {
        updateParreAnalysisProxies(parreAnalysisId);
    }

    public void vulnerabilityUpdated(Long parreAnalysisId) {
        updateParreAnalysisProxies(parreAnalysisId);
    }

    protected void updateParreAnalysisProxies(Long parreAnalysisId) {
    	if (log.isDebugEnabled()) { log.debug("Updating ProxyIndication Probabilities for ParreAnalysis with id = " + parreAnalysisId); }
        List<ProxyIndication> proxies = AssetThreatAnalysisManager.getParreAnalysisProxies(parreAnalysisId, entityManager());
        if (log.isDebugEnabled()) { log.debug("Number of Proxy Indications to update : " + proxies.size()); }
        for (ProxyIndication proxy : proxies) {
            updateProbabilities(proxy);
        }
    }

    private void updateFinancialTotal(Collection<? extends AssetThreatAnalysis> analyses) {
        for (AssetThreatAnalysis analysis : analyses) {
            if (!analysis.getRemoved()) { // and not Natural Threat...
                analysis.updateFinancialTotal();
                saveEntity(analysis);
            }
        }
    }
    private boolean threatExists(List<Threat> threats, AssetThreat assetThreat) {
        for (Threat threat : threats) {
            if (assetThreat.isFor(threat)) {
                return true;
            }
        }
        return false;
    }

    private boolean assetExists(List<Asset> assets, AssetThreat assetThreat) {
        for (Asset asset : assets) {
            if (assetThreat.isFor(asset)) {
                return true;
            }
        }
        return false;
    }

    public void assetThreatCountChanged(Long parreAnalysisId) {
        DirectedThreatAnalysis currentAnalysis = DirectedThreatManager.getCurrentAnalysis(parreAnalysisId, entityManager());
        RiskResilienceAnalysis currentRRA = RiskResilienceManager.getCurrentAnalysis(parreAnalysisId, entityManager());
        if (currentAnalysis == null) {
        	log.debug("No Current Directed Threat Analysis, no action needed");
            return;
        }
        AssetThreatAnalysisState analysisState = createAssetThreatAnalysisState(parreAnalysisId);
        List<AssetThreat> directedAssetThreats = ThreatManager.getActiveAssetThreats
                (parreAnalysisId, ThreatCategoryType.MAN_MADE_HAZARD, entityManager());
/*
        log.info("Number of Existing DirectedThreatAnalyses = " + directedAssetThreats.size());
        for (AssetThreat directedAssetThreat : directedAssetThreats) {
            boolean analysisRequired = analysisState.analysisRequired(directedAssetThreat);
            if (analysisRequired && directedAssetThreat.getRemoved()) {
                directedAssetThreat.setRemoved(Boolean.FALSE);
                saveEntity(directedAssetThreat);
            } else if (!analysisRequired && !directedAssetThreat.getRemoved()) {
                directedAssetThreat.setRemoved(Boolean.TRUE);
                saveEntity(directedAssetThreat);
            }
        }
*/
        List<Asset> assets = getAssetsForAnalysis(parreAnalysisId);
        //List<Threat> activeThreats = ThreatManager.getActiveThreats(parreAnalysisId, ThreatCategoryType.MAN_MADE_HAZARD, entityManager());
        List<Threat> allThreats = ThreatManager.getActiveThreats(parreAnalysisId, ThreatCategoryType.MAN_MADE_HAZARD, entityManager());
        ParreAnalysis parreAnalysis = findParreAnalysis(parreAnalysisId);
        for (Asset asset : assets) {
            //for (Threat threat : activeThreats) {
            for (Threat threat : allThreats) {
                boolean analysisRequired = analysisState.analysisRequired(asset, threat);
                //log.info("Analysis Required For : " + asset.getAssetId() + ", " + threat.getName());
                AssetThreat assetThreat = findAssetThreat(asset, threat, directedAssetThreats);
                boolean assetThreatExists = assetThreat != null;
                if (!analysisRequired && (assetThreatExists && !assetThreat.getRemoved())) {
                    assetThreat.setRemoved(Boolean.TRUE);
                    saveEntity(assetThreat);
                }
                else if (analysisRequired) {
                    if (assetThreatExists && assetThreat.getRemoved()) {
                        assetThreat.setRemoved(Boolean.FALSE);
                        saveEntity(assetThreat);
                    }
                    else if (!assetThreatExists) {
                        assetThreat = new AssetThreat(parreAnalysis, asset, threat);
                        saveEntityWithFlush(assetThreat);
                    }
                    if (!currentAnalysis.hasDirectedThreatFor(assetThreat)) {
                        DirectedThreat directedThreat = new DirectedThreat(parreAnalysis, assetThreat);
                        currentAnalysis.addDirectedThreat(directedThreat);
                        saveEntityWithFlush(directedThreat);
                    }
                    if (currentRRA != null && !currentRRA.hasRiskResilience(assetThreat)) {
                    	RiskResilience riskResilience = new RiskResilience();
                    	riskResilience.setAssetThreat(assetThreat);
                    	riskResilience.setParreAnalysis(parreAnalysis);
                    	currentRRA.addRiskResilience(riskResilience);
                    	saveEntityWithFlush(riskResilience);
                    }
                }

            }
        }
    }

    private AssetThreat findAssetThreat(Asset asset, Threat threat, List<AssetThreat> directedAssetThreats) {
        for (AssetThreat directedAssetThreat : directedAssetThreats) {
            if (directedAssetThreat.isFor(asset, threat)) {
                return directedAssetThreat;
            }
        }
        return null;
    }

    public void assetAdded(Long parreAnalysisId, Long addedAssetId) {
        List<AssetThreat> allAssetThreats = ParreManager.getAllAssetThreats(parreAnalysisId, entityManager());
        ParreAnalysis parreAnalysis = findParreAnalysis(parreAnalysisId);
        Asset addedAsset = find(Asset.class, addedAssetId);
        AssetThreatAnalysisState analysisState = createAssetThreatAnalysisState(parreAnalysisId);
        List<Threat> activeThreats = ThreatManager.getActiveThreats(parreAnalysisId, entityManager());
        boolean assetFound = false;
        for (AssetThreat assetThreat : allAssetThreats) {
            if (!assetThreat.isFor(addedAsset)) {
                continue;
            }
            assetFound = true;
            if (assetThreat.getRemoved()) {
                if (threatExists(activeThreats, assetThreat) && isRequiredAnalysis(analysisState, assetThreat)) {
                    assetThreat.setRemoved(Boolean.FALSE);
                    saveEntity(assetThreat);
                }
            }
        }
        if (!assetFound) {
            addAssetThreatAnalyses(parreAnalysis, Collections.singletonList(addedAsset), activeThreats);
        }

    }

    public void assetRemoved(Long parreAnalysisId, Long removedAssetId) {
        List<AssetThreat> allAssetThreats = ParreManager.getAllAssetThreats(parreAnalysisId, entityManager());
        if (allAssetThreats.isEmpty()) {
            log.debug("No Asset Threats yet, no need to update");
            return;
        }
        Asset removedAsset = find(Asset.class, removedAssetId);
        for (AssetThreat assetThreat : allAssetThreats) {
            if (assetThreat.isFor(removedAsset)) {
                assetThreat.setRemoved(Boolean.TRUE);
                saveEntity(assetThreat);
            }
        }
    }

}
