/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */

/* TODO: Merge this back into the LGPL version */
package org.parre.server.messaging;



import java.lang.reflect.Proxy;

import org.parre.client.messaging.*;
import org.parre.server.messaging.invocation.ServiceInvocationHandler;
import org.parre.server.messaging.service.*;
import org.parre.server.util.SystemUtil;

/**
 * .
 * User: Swarn S. Dhaliwal
 * Date: 9/10/11
 * Time: 8:12 AM
 */
public class ParreServiceFactory {
    public static <T> T createServiceProxy(T serviceObject) {
        String persistenceUnit = SystemUtil.getPersistenceUnit();
        return (T)Proxy.newProxyInstance(Thread.currentThread().getContextClassLoader(), serviceObject.getClass().getInterfaces(),
                new ServiceInvocationHandler(serviceObject, persistenceUnit));

    }

    public static AssetService createAssetService() {
        return createServiceProxy(AssetServiceImpl.getInstance());
    }

    public static ConsequenceService createConsequenceService() {
        return createServiceProxy(ConsequenceServiceImpl.getInstance());
    }

    public static LoginService createLoginService() {
        return  createServiceProxy(LoginServiceImpl.getInstance());
    }

    public static ParreService createParreService() {
        return  createServiceProxy(ParreServiceImpl.getInstance());
    }

    public static VulnerabilityService createVulnerabilityService() {
        return createServiceProxy(VulnerabilityServiceImpl.getInstance());
    }

    public static LotService createLotService() {
        return  createServiceProxy(LotServiceImpl.getInstance());
    }
    public static NaturalThreatService createNaturalThreatService() {
        return  createServiceProxy(NaturalThreatServiceImpl.getInstance());
    }
    public static DpService createDpService() {
        return  createServiceProxy(DpServiceImpl.getInstance());
    }
    
    public static RiskResilienceService createRraService() {
        return createServiceProxy(RiskResilienceServiceImpl.getInstance());
    }

    public static RiskBenefitService createRrmService() {
        return createServiceProxy(RiskBenefitServiceImpl.getInstance());
    }

    public static AssetThreatService createAssetThreatService() {
        return createServiceProxy(AssetThreatServiceImpl.getInstance());
    }

    public static DbInitService createDbInitService() {
        return createServiceProxy(DbInitServiceImpl.getInstance());
    }
    
    public static ReportsService createReportsService() {
        return createServiceProxy(ReportsServiceImpl.getInstance());
    }
}
