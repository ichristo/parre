/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.server.messaging.servlet;

import java.util.List;

import org.apache.log4j.Logger;
import org.parre.client.messaging.ConsequenceService;
import org.parre.server.messaging.ParreServiceFactory;
import org.parre.shared.*;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;

/**
 * The Class ConsequenceServlet.
 */
public class ConsequenceServlet extends RemoteServiceServlet implements ConsequenceService {

	private static transient Logger log = Logger.getLogger(ConsequenceServlet.class);
    private ConsequenceService service = ParreServiceFactory.createConsequenceService();

	public List<ConsequenceDTO> getAssetSearchResults(AssetSearchDTO searchDTO, Long parreAnalysisId)
			throws IllegalArgumentException {
		return service.getAssetSearchResults(searchDTO, parreAnalysisId);
	}

	public List<ConsequenceDTO> getThreatSearchResults(ThreatDTO searchDTO, Long parreAnalysisId)
			throws IllegalArgumentException {
		return service.getThreatSearchResults(searchDTO, parreAnalysisId);
	}

    public ConsequenceDTO saveConsequence(ConsequenceDTO consequenceDTO) {
        return service.saveConsequence(consequenceDTO);
    }

    public DirectedThreatAnalysisDTO getCurrentAnalysis(Long parreAnalysisId) {
        return service.getCurrentAnalysis(parreAnalysisId);
    }

    public DirectedThreatAnalysisDTO startAnalysis(DirectedThreatAnalysisDTO dto) {
        return service.startAnalysis(dto);
    }

}
