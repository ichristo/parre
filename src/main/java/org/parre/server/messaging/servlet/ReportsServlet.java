/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.server.messaging.servlet;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperRunManager;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

import org.apache.log4j.Logger;
import org.parre.client.messaging.ReportsService;
import org.parre.server.domain.manager.ReportsManager;
import org.parre.server.messaging.ParreServiceFactory;
import org.parre.shared.ParreAnalysisReportSectionDTO;
import org.parre.shared.ReportAssessmentTaskDTO;
import org.parre.shared.ReportInvestmentOptionDTO;
import org.parre.shared.RiskResilienceDTO;
import org.parre.shared.RptAssessmentDTO;
import org.parre.shared.RptAssetDTO;
import org.parre.shared.RptConsequenceDTO;
import org.parre.shared.RptMainDTO;
import org.parre.shared.RptRankingsDTO;
import org.parre.shared.RptThreatLikelihoodDTO;
import org.parre.shared.RptVulnerabilityDTO;
import org.parre.shared.exception.InvalidParreAnalysisIdException;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;



public class ReportsServlet extends RemoteServiceServlet implements ReportsService {
	private static final long serialVersionUID = 1L;
	private final static transient Logger log = Logger.getLogger(ReportsServlet.class);
	private ReportsService service = ParreServiceFactory.createReportsService();

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
        byte[] bytes = null;
        List<RptMainDTO> rptData = new ArrayList<RptMainDTO>();
        //Get the ParreAnalysisID from the Client
    	Long parreAnalysisId =  (Long.parseLong(request.getParameter("paId")));
    	log.debug("Generate Report for ParreAnalysisId : " + parreAnalysisId);
    	
		try {
			rptData = getReportData(parreAnalysisId);
		} catch (InvalidParreAnalysisIdException e1) {
			e1.printStackTrace();
		}
			
        ServletOutputStream servletOutputStream = response.getOutputStream();         
        InputStream rptStream = this.getClass().getResourceAsStream("/jasperreports/ParreRptPDF.jasper");     
    	Map<String, Object> parameters = new HashMap<String, Object>();
    	
    	
    	
	    try
	    {		    	      	      
	      bytes = JasperRunManager.runReportToPdf(rptStream, parameters, new JRBeanCollectionDataSource(rptData));	      
	      response.setContentType("application/pdf");
	      response.setContentLength(bytes.length);
	      servletOutputStream.write(bytes, 0, bytes.length);
	      servletOutputStream.flush();
	      servletOutputStream.close();
	    }
	    catch (JRException e)
	    {	      
	      StringWriter stringWriter = new StringWriter();
	      PrintWriter printWriter = new PrintWriter(stringWriter);
	      e.printStackTrace(printWriter);
	      response.setContentType("text/plain");
	      response.getOutputStream().print(stringWriter.toString());
	    } catch (Exception e) { 
	    	e.printStackTrace(); 
	    } 
	  }
	

	@Override
	public List<RptMainDTO> getReportData(Long parreAnalysisID) throws InvalidParreAnalysisIdException {
		return service.getReportData(parreAnalysisID);
	}


	@Override
	public List<RiskResilienceDTO> getRptRiskResilienceDTOs(Long parreAnalysisId) {
		return service.getRptRiskResilienceDTOs(parreAnalysisId);
	}
	
	@Override
	public List<RiskResilienceDTO> getRptTop20RisksDTOs(Long parreAnalysisId) {
		return service.getRptTop20RisksDTOs(parreAnalysisId);
	}


	@Override
	public List<RiskResilienceDTO> getRptTop10NatThreatDTOs(Long parreAnalysisId) {
		return service.getRptTop10NatThreatDTOs(parreAnalysisId);
	}


	@Override
	public RptMainDTO getReportInputData(Long parreAnalysisId) {
		return service.getReportInputData(parreAnalysisId);
	}

	@Override
	public ParreAnalysisReportSectionDTO getReportSection(Long parreAnalysisId, Long reportSectionLookupId) {
		return service.getReportSection(parreAnalysisId, reportSectionLookupId);
	}


	@Override
	public ParreAnalysisReportSectionDTO saveReportSection(Long parreAnalysisId, ParreAnalysisReportSectionDTO dto) {
		return service.saveReportSection(parreAnalysisId, dto);
	}


	@Override
	public List<RptAssetDTO> getReportDisplayTableAssets(Long parreAnalysisId) {
		return service.getReportDisplayTableAssets(parreAnalysisId);
	}


	@Override
	public List<RptConsequenceDTO> getReportDisplayTableConsequences(Long parreAnalysisId) {
		return service.getReportDisplayTableConsequences(parreAnalysisId);
	}


	@Override
	public List<RptVulnerabilityDTO> getReportDisplayTableVulnerability(Long parreAnalysisId) {
		return service.getReportDisplayTableVulnerability(parreAnalysisId);
	}


	@Override
	public List<RptThreatLikelihoodDTO> getReportDisplayTableThreatLikelihood(Long parreAnalysisId) {
		return service.getReportDisplayTableThreatLikelihood(parreAnalysisId);
	}


	@Override
	public List<RptRankingsDTO> getReportDisplayTableRiskManagement(Long parreAnalysisId) {		
		return service.getReportDisplayTableRiskManagement(parreAnalysisId);
	}


	@Override
	public List<RptAssessmentDTO> getReportDisplayTableRiskReduction(Long parreAnalysisId) {		
		return service.getReportDisplayTableRiskReduction(parreAnalysisId);
	}


	@Override
	public List<ReportAssessmentTaskDTO> getReportAssessmentTasks(Long parreAnalysisId) {
		return service.getReportAssessmentTasks(parreAnalysisId);
	}


	@Override
	public List<ReportAssessmentTaskDTO> saveAddTaskPopup(Long parreAnalysisId, ReportAssessmentTaskDTO taskDTO) {
		return service.saveAddTaskPopup(parreAnalysisId, taskDTO);
		
	}


	@Override
	public List<ReportInvestmentOptionDTO> getReportInvestmentOptions(Long parreAnalysisId) {
		return service.getReportInvestmentOptions(parreAnalysisId);
	}


	@Override
	public List<ReportInvestmentOptionDTO> saveEditOptionPopup(Long parreAnalysisId, ReportInvestmentOptionDTO optionDTO) {
		return service.saveEditOptionPopup(parreAnalysisId, optionDTO);
	}

}
