/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.server.messaging.servlet;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;

import java.util.List;

import org.parre.client.messaging.RiskBenefitService;
import org.parre.server.messaging.ParreServiceFactory;
import org.parre.shared.*;
import org.parre.shared.types.RiskBenefitMetricType;

/*
	Date			Author				Changes
    2/15/12	Swarn S. Dhaliwal	Created
*/
/**
 * The Class RiskBenefitServlet.
 */
public class RiskBenefitServlet extends RemoteServiceServlet implements RiskBenefitService {
    private RiskBenefitService service = ParreServiceFactory.createRrmService();
    public RiskBenefitAnalysisDTO getAnalysis(RiskBenefitMetricType metricType) {
        return service.getAnalysis(metricType);
    }

    public void updateBudget(Long riskBenefitAnalysisId, AmountDTO budgetAmount) {
        service.updateBudget(riskBenefitAnalysisId, budgetAmount);
    }

    public ParreAnalysisDTO startOptionAnalysis(Long riskBenefitAnalysisId, NameDescriptionDTO dto) {
        return service.startOptionAnalysis(riskBenefitAnalysisId, dto);
    }

    public List<RiskBenefitDTO> getMetricAnalysis(RiskBenefitMetricType metricType) {
        return service.getMetricAnalysis(metricType);
    }

    public List<BenefitCostDTO> getBenefitCostAnalysis() {
        return service.getBenefitCostAnalysis();
    }
}