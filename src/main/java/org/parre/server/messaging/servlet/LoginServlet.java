/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.server.messaging.servlet;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.parre.client.messaging.LoginService;
import org.parre.server.messaging.ParreServiceFactory;
import org.parre.shared.LoginInfo;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;

/**
 * The Class LoginServlet.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 9/21/11
 */
public class LoginServlet extends RemoteServiceServlet implements LoginService {
    private static transient final Logger log = Logger.getLogger(LoginServlet.class);

    private LoginService loginService = ParreServiceFactory.createLoginService();

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        //initDb(config.getServletContext());
    }

    public LoginInfo login(String userName, String password) {
        LoginInfo login = loginService.login(userName, password);
        HttpServletRequest request = getThreadLocalRequest();
        request.getSession().setAttribute("loginInfo", login);
        return login;
    }

    public LoginInfo login() {
        HttpSession session = getThreadLocalRequest().getSession();
        if (log.isDebugEnabled()) { log.debug("Checking Login session Id = " + session.getId()); }
        LoginInfo loginInfo = (LoginInfo) session.getAttribute("loginInfo");
        if (loginInfo != null) {
            return loginInfo;
        }
        loginInfo = new LoginInfo();
        loginInfo.setLoggedIn(false);
        setDevMode(loginInfo);
        return loginInfo;
    }

    private void setDevMode(LoginInfo loginInfo) {
        loginInfo.setDevMode("true".equals(System.getProperty("dev.mode", "false")));
    }
    public LoginInfo signOut() {
        getThreadLocalRequest().getSession().invalidate();
        return new LoginInfo();
    }

}
