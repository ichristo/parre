/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.server.messaging.servlet;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;

import java.util.List;

import org.parre.client.messaging.RiskResilienceService;
import org.parre.server.messaging.ParreServiceFactory;
import org.parre.shared.*;

/*
	Date			Author				Changes
    2/15/12	Swarn S. Dhaliwal	Created
*/
/**
 * The Class RiskResilienceServlet.
 */
public class RiskResilienceServlet extends RemoteServiceServlet implements RiskResilienceService {
    private RiskResilienceService service = ParreServiceFactory.createRraService();
    public RiskResilienceAnalysisDTO getAnalysis(Long parreAnalysisId) {
        return service.getAnalysis(parreAnalysisId);
    }

    public List<RiskResilienceDTO> getRiskResilienceDTOs(Long riskResilienceAnalysisId, Long baselineId) {
        return service.getRiskResilienceDTOs(riskResilienceAnalysisId, baselineId);
    }

    public RiskResilienceDTO saveRiskResilience(RiskResilienceDTO riskResilienceDTO) {
        return service.saveRiskResilience(riskResilienceDTO);
    }

    public List<UriResponseDTO> getUriResponseDTOs(Long riskResilienceAnalysisId) {
        return service.getUriResponseDTOs(riskResilienceAnalysisId);
    }

    public void saveUriResponses(List<UriResponseDTO> uriResponseDTOs) {
        service.saveUriResponses(uriResponseDTOs);
    }

    public List<UriQuestionDTO> getUriQuestionDTOs() {
        return service.getUriQuestionDTOs();
    }

    public void updateUnitPrice(Long riskResilienceId, UnitPriceDTO unitPriceDTO) {
        service.updateUnitPrice(riskResilienceId, unitPriceDTO);
    }
}