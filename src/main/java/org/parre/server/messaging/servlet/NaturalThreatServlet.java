/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.server.messaging.servlet;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;

import java.util.List;

import org.parre.client.messaging.NaturalThreatService;
import org.parre.server.messaging.ParreServiceFactory;
import org.parre.shared.*;

/*
	Date			Author				Changes
    12/6/11	Swarn S. Dhaliwal	Created
*/
/**
 * The Class NaturalThreatServlet.
 */
public class NaturalThreatServlet extends RemoteServiceServlet implements NaturalThreatService {
	private static final long serialVersionUID = 1L;
	private NaturalThreatService service = ParreServiceFactory.createNaturalThreatService();
    public NaturalThreatDTO save(NaturalThreatDTO naturalThreatDTO) {
        return service.save(naturalThreatDTO);
    }

    public List<NaturalThreatDTO> getSearchResults(ThreatDTO dto, Long parreAnalysisId) {
        return service.getSearchResults(dto, parreAnalysisId);
    }

    public NaturalThreatAnalysisDTO getCurrentAnalysis(Long parreAnalysisId) {
        return service.getCurrentAnalysis(parreAnalysisId);
    }

    public NaturalThreatAnalysisDTO startAnalysis(NaturalThreatAnalysisDTO dto) {
        return service.startAnalysis(dto);
    }

    public EarthquakeLookupDataDTO getEarthquakeLookupData(String earthquakeText) {
        return service.getEarthquakeLookupData(earthquakeText);
    }

    public List<HurricaneLookupDataDTO> getHurricaneLookupData() {
        return service.getHurricaneLookupData();
    }

	@Override
	public NaturalThreatCalculationDTO calculateTornadoData(NaturalThreatCalculationDTO dto) {
		return service.calculateTornadoData(dto);
	}
	
	@Override
	public NaturalThreatCalculationDTO calculateEarthquakeData(NaturalThreatCalculationDTO dto) {
		return service.calculateEarthquakeData(dto);
	}

	@Override
	public PhysicalResourceDTO getPhysicalResourceForNT(Long naturalThreatId) {
		return service.getPhysicalResourceForNT(naturalThreatId);
	}

	@Override
	public NaturalThreatCalculationDTO calculateFloodData(NaturalThreatCalculationDTO dto) {
		return service.calculateFloodData(dto);
	}

	@Override
	public NaturalThreatCalculationDTO calculateIceStormData(NaturalThreatCalculationDTO dto) {
		return service.calculateIceStormData(dto);
	}

	@Override
	public EarthquakeProfileDTO getEarthquakeProfile(Long earthquakeProfileId) {
		return service.getEarthquakeProfile(earthquakeProfileId);
	}

	@Override
	public EarthquakeProfileDTO saveEarthquakeProfile(EarthquakeProfileDTO dto) {
		return service.saveEarthquakeProfile(dto);
	}

	@Override
	public ParreAnalysisDTO getAnalysisEarthquakeProfiles(Long id) {
		return service.getAnalysisEarthquakeProfiles(id);
	}

    @Override
    public List<String> getTornadoCountyData(String stateCodeOverride) {
        return service.getTornadoCountyData(stateCodeOverride);
    }

    @Override
	public NaturalThreatCalculationDTO calculateHurricaneData(NaturalThreatCalculationDTO dto) {
		return service.calculateHurricaneData(dto);
	}

	@Override
	public HurricaneProfileDTO getHurricaneProfile(Long hurricaneProfileId) {
		return service.getHurricaneProfile(hurricaneProfileId);
	}

	@Override
	public HurricaneProfileDTO saveHurricaneProfile(HurricaneProfileDTO dto) {
		return service.saveHurricaneProfile(dto);
	}

	@Override
	public ParreAnalysisDTO getAnalysisHurricaneProfiles(Long id) {
		return service.getAnalysisHurricaneProfiles(id);
	}
	
	@Override
	public NaturalThreatDTO calculateNaturalThreat(NaturalThreatDTO dto) {
		return service.calculateNaturalThreat(dto);
	}
	
	@Override
	public Boolean recalculateAllNaturalThreats(Long parreAnalysisId) {
		return service.recalculateAllNaturalThreats(parreAnalysisId);
	}

}