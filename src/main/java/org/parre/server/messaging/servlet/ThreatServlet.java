/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.server.messaging.servlet;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import org.apache.log4j.Logger;
import org.parre.client.messaging.ThreatService;
import org.parre.server.messaging.service.ThreatServiceImpl;
import org.parre.shared.LabelValueDTO;
import org.parre.shared.ThreatDTO;
import org.parre.shared.exception.ItemInUseException;

import java.util.List;

/*
	Date			Author				Changes
    9/29/11	Swarn S. Dhaliwal	Created
*/
/**
 * The Class ThreatServlet.
 */
public class ThreatServlet extends RemoteServiceServlet implements ThreatService {
    private static transient final Logger log = Logger.getLogger(ThreatServlet.class);
    private ThreatService service = ThreatServiceImpl.getInstance();

    public List<ThreatDTO> getThreatSearchResults(ThreatDTO dto, Long parreAnalysisId) {
        return service.getThreatSearchResults(dto, parreAnalysisId);
    }

    public List<LabelValueDTO> getThreatCategories() {
        return service.getThreatCategories();
    }

    public ThreatDTO saveThreat(ThreatDTO model, Long parreAnalysisId) {
        return service.saveThreat(model, parreAnalysisId);
    }

    public ThreatDTO updateThreatActive(ThreatDTO threatDTO, Long parreAnalysisId) {
        return service.updateThreatActive(threatDTO, parreAnalysisId);
    }

    public void deleteThreat(ThreatDTO threatDTO) throws ItemInUseException {
        service.deleteThreat(threatDTO);
    }
}