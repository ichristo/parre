/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.server.messaging.servlet;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;

import java.util.List;

import org.parre.client.messaging.DpService;
import org.parre.server.messaging.ParreServiceFactory;
import org.parre.shared.DpAnalysisDTO;
import org.parre.shared.DpDTO;
import org.parre.shared.ThreatDTO;

/*
	Date			Author				Changes
    12/7/11	Swarn S. Dhaliwal	Created
*/
/**
 * The Class DpServlet.
 */
public class DpServlet extends RemoteServiceServlet implements DpService {
    private DpService service = ParreServiceFactory.createDpService();
    public List<DpDTO> getSearchResults(ThreatDTO dto, Long parreAnalysisId) {
        return service.getSearchResults(dto, parreAnalysisId);
    }

    public DpDTO save(DpDTO dpDTO) {
        return service.save(dpDTO);
    }

    public DpAnalysisDTO startAnalysis(DpAnalysisDTO dto) {
        return service.startAnalysis(dto);
    }

    public DpAnalysisDTO getCurrentAnalysis(Long parreAnalysisId) {
        return service.getCurrentAnalysis(parreAnalysisId);
    }

    public DpDTO addSubCategory(DpDTO subCat) {
        return service.addSubCategory(subCat);
    }

    public void removeCategory(Long dpId) {
        service.removeCategory(dpId);
    }

    @Override
    public boolean hasAnalysis(Long parreAnalysisId) {
        return service.hasAnalysis(parreAnalysisId);
    }
}