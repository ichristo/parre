/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.server.persistence;


import org.apache.log4j.Logger;
import org.parre.server.util.LoggingUtil;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import java.util.HashMap;

/**
 * This class a threadsafe registry for the entity manager for a particular
 * persistence-unit.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 Feb 20, 2006
 */
public class EntityManagerRegistry {
    private transient static final Logger log = Logger
            .getLogger(EntityManagerRegistry.class);
    private EntityManagerFactory entityManagerFactory;
    private final ThreadLocal<EntityManager> entityManagerContainer
            = new ThreadLocal<EntityManager>();
    private final ThreadLocal<EntityTransaction> transactionContainer
            = new ThreadLocal<EntityTransaction>();
    private String persistenceUnit;

    public EntityManagerRegistry(String persistenceUnit) {
        this.persistenceUnit = persistenceUnit;
        entityManagerFactory = Persistence
                .createEntityManagerFactory(persistenceUnit, new HashMap());
    }

    public String getPersistenceUnit() {
        return persistenceUnit;
    }

    public EntityManager entityManager() {
        EntityManager entityManager = entityManagerContainer.get();
        if (entityManager == null) {
            entityManager = entityManagerFactory.createEntityManager();
            entityManagerContainer.set(entityManager);
        }
        return entityManager;
    }

    public void closeEntityManager() {
        EntityManager entityManager = entityManagerContainer.get();
        if (entityManager == null) {
            log.warn("Attempt to close a non existent entity manager ignored : "
                    + getPersistenceUnit());
            return;
        }
        EntityTransaction entityTransaction = transactionContainer.get();
        if (entityTransaction != null) {
            throw new IllegalStateException(
                    "Attempt to close entity manager with active transaction : "
                            + getPersistenceUnit());
        }
        entityManager.close();
        entityManagerContainer.set(null);
    }

    public boolean isTransactionActive() {
        EntityTransaction entityTransaction = transactionContainer.get();
        return entityTransaction != null && entityTransaction.isActive();
    }

    /**
     * begin a transaction in the context of current thread.
     *
     * @return returns true if a new transaction was started, returns false if a transaction was already in progress
     */
    public boolean beginTransaction() {
        EntityTransaction transaction = transactionContainer.get();
        if (transaction == null) {
            transaction = entityManager().getTransaction();
            transactionContainer.set(transaction);
        }
        if (transaction.isActive()) {
            log.debug("Transaction active");
            return false;
        }
        transaction.begin();
        log.debug("Transaction started");
        return true;
    }

    public void commitTransaction() {
        EntityTransaction transaction = transactionContainer.get();
        LoggingUtil.debugLogMessage(log, "Committing transaction :", transaction);
        if (transaction == null) {
            log.warn(
                    "commitTransaction was called when there was not active transaction for : "
                            + getPersistenceUnit());
            return;
        }
        LoggingUtil.debugLogMessage(log, "Attempting to commit transaction :", transaction);
        transaction.commit();

        LoggingUtil.debugLogMessage(log, "Transaction committed");
        closeTransaction();
        LoggingUtil.debugLogMessage(log, "Transaction closed");
    }

    public void rollbackTransactionIfActive() {
        EntityTransaction transaction = transactionContainer.get();
        if (transaction != null && transaction.isActive()) {
            transaction.rollback();
        }
        closeTransaction();
    }

    public void rollbackTransaction() {
        EntityTransaction transaction = transactionContainer.get();
        if (transaction == null) {
            log.warn(
                    "rollbackTransaction was called when there was not active transaction for : "
                            + getPersistenceUnit());
            return;
        }
        if (transaction.isActive()) {
            transaction.rollback();
        }
        closeTransaction();
        log.debug("Transation rolledback and closed");
    }

    private void closeTransaction() {
        transactionContainer.set(null);
    }

    public void shutdown() {
        log.info("Shutting down EntityManager factory for " + persistenceUnit);
        entityManagerFactory.close();
        entityManagerFactory = null;
        log.info("EntityManger factory shutdown for " + persistenceUnit);
    }

}
