/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.server.servlet;


import org.apache.log4j.Logger;
import org.parre.server.messaging.ParreServiceFactory;
import org.parre.server.messaging.invocation.ServiceInvocationContext;
import org.parre.server.messaging.service.DbInitService;
import org.parre.shared.LoginInfo;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;

/**
 * .
 * User: Swarn S. Dhaliwal
 * Date: 10/8/11
 * Time: 9:29 AM
 */
public class ServerSecurityFilter implements Filter {
    private static transient final Logger log = Logger.getLogger(ServerSecurityFilter.class);

    private boolean enforceLogin;
    public void init(FilterConfig filterConfig) throws ServletException {
        enforceLogin = Boolean.valueOf(filterConfig.getInitParameter("enforceLogin"));
        log.info("initialized, enforceLogin=" + enforceLogin);
    }

    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        LoginInfo loginInfo = (LoginInfo)request.getSession().getAttribute("loginInfo");
        if (loginInfo == null && enforceLogin) {
            HttpServletResponse response = (HttpServletResponse) servletResponse;
            response.setStatus(HttpServletResponse.SC_FORBIDDEN);
            return;
        }
        if (loginInfo == null) {
            loginInfo = new LoginInfo();
        }
        ServiceInvocationContext context = new ServiceInvocationContext(loginInfo);
        ServiceInvocationContext.init(context);
        try {
            filterChain.doFilter(servletRequest, servletResponse);
        }
        finally {
            ServiceInvocationContext.destroy();
        }
    }


    public void destroy() {
        log.info("destroyed");
    }
}
