PARRE Community Software is an OpenSource which is available for any one to modify and enhance for the betterment of the Application under the Terms and conditions of GNU General Public Livense GPLv3.

* The development environment for PARRE is based on common practices used by most of the popular Java open source community projects.
* PARRE uses the Google Web Toolkit (GWT) for the User Interface.
* The business logic and middleware functionality is hosted on the JBoss Application Server.
* The Hibernate “Object to Relational Mapping” (ORM) framework is used to manage PARRE data that is stored in a relational database.
* Hibernate is the market leading ORM framework and supports all major relational databases including Oracle, MS SQL Server, Postgres, MySQL and H2.
* The PARRE development environment uses two very important and commonly used development tools, Git and Maven.
* The PARRE community will interact with the PARRE online forums, feature tracking system, product roadmap, and source code.
* The community may submit feature requests and code patches to PARRE for consideration. AEM will determine if these are functional or technical in nature and then manage the review and incorporation of this community input. 
* Code contributed to the open source community will be reviewed by the CCB for technical soundness and contribution to the utility risk and resiliency community. Both of these criteria must be satisfied before the community code will be considered for inclusion in new releases.


Persistence Unit Setup on Jboss Server:

The goal is to support both H2 and Mysql databases. The following approach allows a deployed artifact (EAR) to be capable of supporting either without having to rebuild it.  

* The JNDI is accessible via java:jboss/datasources/parre-dev
* Persistence unit name is "parre-dev", can override with System property "parre.persistenceUnit"
* The class that handles persistence name resolution is org.parre.server.util.SystemUtil. This functionality is for unit testing
* The Hibernate dialect by default will be "org.hibernate.dialect.H2Dialect"
* Can override with the system property "parre.hibernate.dialect" for MySql as (-Dparre.hibernate.dialect=org.hibernate.dialect.MySQLDialect)
	
Unit Testing:

* The persistence unit name used during testing by default is "parre-testing" which refers to a H2 specific unit in the test version of persistence.xml (in src/test/resources/META-INF).  For persistence unit tests, we need to set this as a System property so services will pick up the correct persistence unit (since they utilize SystemUtil to derive the name).
* Unit tests that are mechanisms to load seed data should derive from the base class DataloadBase.
* Regular unit tests that require database access should derive from the base class PersistenceTest.
* The persistence.xml on the classpath for running tests includes two persistence units, one for H2 ("parre-testing") and one for Mysql ("parre-testing-mysql").  Running a unit test will by default use the H2 backed persistence unit.  To run a unit test against the Mysql instance, include the following system parameter (-Dparre.persistenceUnit=parre-testing-mysql)

	
Create MySQL Database:	

* Create new database parre with username/password as parre/parre, use the following:
	CREATE database parre;
	GRANT ALL PRIVILEGES ON parre.* TO 'parre'@'localhost' IDENTIFIED BY 'parre' WITH GRANT OPTION;

MySQL Database Setup:

* Configure Jboss's standalone.conf file with MySQL Dialect (JAVA_OPTS="$JAVA_OPTS -Ddev.mode=true -Dtest.mode=true -Dparre.hibernate.dialect=org.hibernate.dialect.MySQLDialect")
* Configure Test Cases by configuring persistence.xml at src/test/resources (persistence-unit name="parre-testing" for MySql property)
	
Default Dataload into the Databse:	  

* Use Maven to load the default data (mvn clean -PcreateSchemaWithData test)
	
Create MySql Datasource in Jboss:	

* Install MySql Drivers and Create new datasource:
  1. Download MySql Drivers (Type-4 JDBC) and deploy them from the Management Console of JBoss
  2. Add a new Datasource (Name:parre-dev-mysql, JNDI Name: java:jboss/datasources/parre-dev, Driver: mysql-connector-java-commercial-5.1.26-bin.jar, Connection URL: jdbc:mysql://localhost:3306/parre)
  3. Test the new datasource connection for success
  
	  
Deploy and Run the Application:	  

* While the Jboss server is running deploy the application using: mvn clean deploy -DskipTests  
* Login into the application using: localhost:8080/parre and click on Get started. Welcome to PARRE!!
